import {
	requestNoAuth,
	request
} from '../../lib/java110/java110Request.js';
import url from '../../constant/url.js';
/**
 * 重大事件分级
 * @param {Object} _data 
 */
export function queryGovEventsType(_data){
	return new Promise((resolve, reject) => {
		requestNoAuth({
			url: url.queryGovEventsType,
			method: "GET",
			data: _data, //动态数据
			success: function(res) {
				let _data = res.data;
				if (_data.code == 0) {
					resolve(_data);
					return ;
				}
				reject(_data.msg);
			},
			fail: function(e) {
				reject(e);
			}
		});
	})
}

/**
 * add by wuxw
 * @param {Object} _data 保存重大事件
 */
export function savePhoneGovMajorEvents(_data){
	return new Promise((resolve, reject) => {
		requestNoAuth({
			url: url.savePhoneGovMajorEvents,
			method: "POST",
			data: _data, //动态数据
			success: function(res) {
				let _data = res.data;
				if (_data.code == 0) {
					resolve(_data);
					return ;
				}
				reject(_data.msg);
			},
			fail: function(e) {
				reject(e);
			}
		});
	})
}

/**
 * add by wuxw
 * @param {Object} _data 保存安全事件（路案、校园安全）
 */
export function savePhoneGovProtectionCase(_data){
	return new Promise((resolve, reject) => {
		requestNoAuth({
			url: url.savePhoneGovProtectionCase,
			method: "POST",
			data: _data, //动态数据
			success: function(res) {
				let _data = res.data;
				if (_data.code == 0) {
					resolve(_data);
					return ;
				}
				reject(_data.msg);
			},
			fail: function(e) {
				reject(e);
			}
		});
	})
}

/**
 * 线路事件 路线
 * @param {Object} _data 
 */
export function queryGovRoadProtection(_data){
	return new Promise((resolve, reject) => {
		requestNoAuth({
			url: url.queryGovRoadProtection,
			method: "GET",
			data: _data, //动态数据
			success: function(res) {
				let _data = res.data;
				if (_data.code == 0) {
					resolve(_data);
					return ;
				}
				reject(_data.msg);
			},
			fail: function(e) {
				reject(e);
			}
		});
	})
}
/**
 * 线路事件 学校
 * @param {Object} _data 
 */
export function queryGovSchool(_data){
	return new Promise((resolve, reject) => {
		requestNoAuth({
			url: url.queryGovSchool,
			method: "GET",
			data: _data, //动态数据
			success: function(res) {
				let _data = res.data;
				if (_data.code == 0) {
					resolve(_data);
					return ;
				}
				reject(_data.msg);
			},
			fail: function(e) {
				reject(e);
			}
		});
	})
}

/**
 * 数据字典
 * @param {Object} _data 
 */
export function queryDicts(_data){
	return new Promise((resolve, reject) => {
		requestNoAuth({
			url: url.queryDicts,
			method: "GET",
			data: _data, //动态数据
			success: function(res) {
				resolve(res);
				return ;
			},
			fail: function(e) {
				reject(e);
			}
		});
	})
}