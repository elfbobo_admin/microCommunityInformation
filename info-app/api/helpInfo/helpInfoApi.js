import {
	requestNoAuth,
	request
} from '../../lib/java110/java110Request.js';
import url from '../../constant/url.js';
/**
 * 查询特殊人员
 * @param {Object} _data 
 */

export function queryGovHelpPolicyList(_data){
	return new Promise((resolve, reject) => {
		requestNoAuth({
			url: url.queryGovHelpPolicyList,
			method: "GET",
			data: _data, //动态数据
			success: function(res) {
				let _data = res.data;
				if (_data.code == 0) {
					resolve(_data);
					return ;
				}
				reject(_data.msg);
			},
			fail: function(e) {
				reject(e);
			}
		});
	})
}

