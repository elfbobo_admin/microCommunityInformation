import {
	requestNoAuth,
	request
} from '../../lib/java110/java110Request.js';
import url from '../../constant/url.js';


/**
 * 政务宣传查询
 * @param {Object} _data 
 */
export function queryGovCiviladmin(_data){
	return new Promise((resolve, reject) => {
		requestNoAuth({
			url: url.queryGovCiviladmin,
			method: "GET",
			data: _data, //动态数据
			success: function(res) {
				let _data = res.data;
				if (_data.code == 0) {
					resolve(_data);
					return ;
				}
				reject(_data.msg);
			},
			fail: function(e) {
				reject(e);
			}
		});
	})
}