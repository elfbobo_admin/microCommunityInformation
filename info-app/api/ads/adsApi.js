import {
	requestNoAuth,
	request
} from '../../lib/java110/java110Request.js';
import url from '../../constant/url.js';


/**
 * 查询拼团产品
 */
export function loadAdverts(dataObj) {
	return new Promise(
		(resolve, reject) => {
			requestNoAuth({
				url: url.listAdvertPhoto,
				method: "GET",
				data: dataObj,
				//动态数据
				success: function(res) {
					if (res.statusCode == 200) {
						let _products = res.data;
						resolve(_products);
						return;
					}
					reject();
				},
				fail: function(e) {
					reject();
				}
			});
		})
}

