import conf from '../conf/config.js'
const baseUrl = conf.baseUrl;
export default {
	NEED_NOT_LOGIN_PAGE: [
		'/pages/login/login',
		'/pages/register/register',
		'/pages/my/my',
		'/pages/index/index',
	],
	NEED_NOT_LOGIN_URL: [
		// this.listActivitiess,
		// this.listAdvertPhoto,
		// this.queryAppUserBindingOwner,
		// this.listJunkRequirements
	],
	baseUrl: baseUrl,
	
	expeditingDelivery: baseUrl + 'app/storeOrder/expeditingDelivery',
	//发送验证码
	userSendSms: baseUrl + "app/user.userSendSms",
	register: baseUrl + "app/userAuth/register",
	login: baseUrl + "app/userAuth/login",
	queryStoreAds: baseUrl + "app/storeAds/queryStoreAds",
	listAreas: baseUrl + "app/govCommunityArea/queryGovCommunityArea",//查询所有区域
	listAdvertPhoto: baseUrl + "app/govAdvert/queryGovAdvertItems",//查询社区广告
	queryActivitiesType: baseUrl + 'app/govActivitiesType/queryGovActivitiesType',
	listActivitiess: baseUrl + 'app/govActivities/queryGovActivities',
	queryReportSetting: baseUrl + 'app/govReportSetting/queryGovReportSetting',
	queryReports: baseUrl + 'app/reportPool/queryReportPool',
	saveReportInfo: baseUrl + 'app/reportPool/savePhoneReportPool',
	deleteReportInfo: baseUrl + 'app/reportPool/deleteReportPool',
	queryGovReportUser: baseUrl + 'app/govReportUser/queryGovReportUser',
	
	//活动报名
	queryGovActivityType: baseUrl + 'app/govActivityType/queryGovActivityType',
	queryGovActivity: baseUrl + 'app/govActivity/queryGovActivity',
	queryGovActivityPerson: baseUrl + 'app/govActivityPerson/queryGovActivityPerson',
	deleteGovActivityPerson: baseUrl + 'app/govActivityPerson/deleteGovActivityPerson',
	saveGovActivityPerson: baseUrl + 'app/govActivityPerson/saveGovActivityPerson',
	
	//办事指南、预约
	queryGovWorkGuide: baseUrl + 'app/govWorkGuide/queryGovWorkGuide',
	queryGovGuideSubscribe: baseUrl + 'app/govGuideSubscribe/queryGovGuideSubscribe',
	saveGovGuideSubscribe: baseUrl + 'app/govGuideSubscribe/saveGovGuideSubscribe',
	deleteGovGuideSubscribe: baseUrl + 'app/govGuideSubscribe/deleteGovGuideSubscribe',
	
	/**企业信息**/
	queryGovCompany: baseUrl + 'app/govCompany/queryGovCompany',
	saveGovCompany: baseUrl + 'app/govCompany/saveGovCompany',
	
	/**查询村、小区**/
	queryGovCommunity: baseUrl + 'app/govCommunity/queryGovCommunity',
	
	/**保存老人死亡登记**/
	saveGovPersonDie: baseUrl + 'app/govPersonDie/savePhonePersonDie',
	
	/**查询政务宣传**/
	queryGovCiviladmin: baseUrl + 'app/govCiviladmin/queryGovCiviladmin',
	
	/**查询事件类型**/
	queryGovEventsType: baseUrl + 'app/govEventsType/queryGovEventsType',
	savePhoneGovMajorEvents: baseUrl + 'app/govMajorEvents/savePhoneGovMajorEvents',
	savePhoneGovProtectionCase: baseUrl + 'app/govProtectionCase/savePhoneGovProtectionCase',
	queryGovRoadProtection: baseUrl + 'app/govRoadProtection/queryGovRoadProtection',//路线
	queryGovSchool: baseUrl + 'app/govSchool/queryGovSchool',//学校
	queryDicts: baseUrl + 'callComponent/core/list',//字典查询
	/**特殊人员跟进**/
	savePhoneGovSpecialFollow: baseUrl + 'app/govSpecialFollow/savePhoneGovSpecialFollow',
	queryLabelGovPerson: baseUrl + 'app/govPerson/queryLabelGovPerson',//人员标签查询
	
	/**评论点赞**/
	saveGovActivityLikes: baseUrl + 'app/govActivityLikes/saveGovActivityLikes',
	deleteGovActivityLikes: baseUrl + 'app/govActivityLikes/deleteGovActivityLikes',
	queryGovActivityLikes: baseUrl + 'app/govActivityLikes/queryGovActivityLikes',
	saveGovActivityReply: baseUrl + 'app/govActivityReply/saveGovActivityReply',
	deleteGovActivityReply: baseUrl + 'app/govActivityReply/deleteGovActivityReply',
	queryGovActivityReply: baseUrl + 'app/govActivityReply/queryGovActivityReply',
	
	/**帮扶记录查询**/
	queryGovPerson: baseUrl + 'app/govPerson/queryGovPerson',//人员信息查询
	queryGovHelpPolicyList: baseUrl + 'app/govHelpPolicyList/queryGovHelpPolicyList',//人员帮扶记录
	
	
}
