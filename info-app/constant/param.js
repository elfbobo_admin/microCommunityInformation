/**
 * 一些参数常量
 */
export default {
	// 默认头像
	headImg: '/static/images/missing-face.png',
	// 出错填充图片
	errorImage: '/static/images/errorImage.jpg',
}
