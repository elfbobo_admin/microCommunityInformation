package com.java110.api.front.components;

import com.java110.api.front.busi.INavServiceSMO;
import com.java110.core.context.IPageData;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

/**
 * 导航栏
 * Created by wuxw on 2019/3/19.
 */

@Component("nav")
public class NavComponent {

    @Autowired
    private INavServiceSMO navServiceSMOImpl;


    /**
     * 查询通知信息
     *
     * @param pd 页面封装数据
     * @return 通知信息
     */
    public ResponseEntity<String> getNavData(IPageData pd) {

        return ResultVo.success();
    }

    /**
     * 阅读消息
     *
     * @param pd
     * @return
     */
    public ResponseEntity<String> readMsg(IPageData pd) {
        return ResultVo.success();
    }


    /**
     * 退出登录
     *
     * @param pd 页面封装对象
     * @return 页面对象ResponseEntity
     */
    public ResponseEntity<String> logout(IPageData pd) {
        ResponseEntity<String> responseEntity = null;
        try {
            responseEntity = navServiceSMOImpl.doExit(pd);
        } catch (Exception e) {
            responseEntity = new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    /**
     * 获取用户信息
     *
     * @param pd 页面封装对象
     * @return 页面对象ResponseEntity
     */
    public ResponseEntity<String> getUserInfo(IPageData pd) {
        ResponseEntity<String> responseEntity = null;
        try {
            responseEntity = navServiceSMOImpl.getUserInfo(pd);
        } catch (Exception e) {
            responseEntity = new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }


    public INavServiceSMO getNavServiceSMOImpl() {
        return navServiceSMOImpl;
    }

    public void setNavServiceSMOImpl(INavServiceSMO navServiceSMOImpl) {
        this.navServiceSMOImpl = navServiceSMOImpl;
    }


}
