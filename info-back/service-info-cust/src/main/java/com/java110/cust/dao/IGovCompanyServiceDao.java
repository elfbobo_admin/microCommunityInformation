package com.java110.cust.dao;


import com.java110.utils.exception.DAOException;


import java.util.List;
import java.util.Map;

/**
 * 公司组织组件内部之间使用，没有给外围系统提供服务能力
 * 公司组织服务接口类，要求全部以字符串传输，方便微服务化
 * 新建客户，修改客户，删除客户，查询客户等功能
 *
 * Created by wuxw on 2016/12/27.
 */
public interface IGovCompanyServiceDao {


    /**
     * 保存 公司组织信息
     * @param info
     * @throws DAOException DAO异常
     */
    void saveGovCompanyInfo(Map info) throws DAOException;




    /**
     * 查询公司组织信息（instance过程）
     * 根据bId 查询公司组织信息
     * @param info bId 信息
     * @return 公司组织信息
     * @throws DAOException DAO异常
     */
    List<Map> getGovCompanyInfo(Map info) throws DAOException;



    /**
     * 修改公司组织信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    void updateGovCompanyInfo(Map info) throws DAOException;


    /**
     * 查询公司组织总数
     *
     * @param info 公司组织信息
     * @return 公司组织数量
     */
    int queryGovCompanysCount(Map info);

}
