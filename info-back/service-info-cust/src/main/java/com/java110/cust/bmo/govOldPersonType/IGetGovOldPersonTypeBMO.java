package com.java110.cust.bmo.govOldPersonType;
import org.springframework.http.ResponseEntity;
import com.java110.dto.govOldPersonType.GovOldPersonTypeDto;
public interface IGetGovOldPersonTypeBMO {


    /**
     * 查询老人类型
     * add by wuxw
     * @param  govOldPersonTypeDto
     * @return
     */
    ResponseEntity<String> get(GovOldPersonTypeDto govOldPersonTypeDto);


}
