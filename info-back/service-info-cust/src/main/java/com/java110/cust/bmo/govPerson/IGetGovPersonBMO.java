package com.java110.cust.bmo.govPerson;
import com.java110.dto.govPerson.GovPersonDto;
import org.springframework.http.ResponseEntity;
public interface IGetGovPersonBMO {


    /**
     * 查询人口管理
     * add by wuxw
     * @param  govPersonDto
     * @return
     */
    ResponseEntity<String> get(GovPersonDto govPersonDto);
    /**
     * 查询人口管理
     * add by wuxw
     * @param  govPersonDto
     * @return
     */
    ResponseEntity<String> getGovPersonHomicide(GovPersonDto govPersonDto);
    /**
     * 查询贫困、帮扶干部
     * add by wuxw
     * @param  govPersonDto
     * @return
     */
    ResponseEntity<String> getGovPersonHelpPolicy(GovPersonDto govPersonDto);


    /**
     * 查询标签人口管理
     * add by wuxw
     * @param  govPersonDto
     * @return
     */
    ResponseEntity<String> getLabelPersons(GovPersonDto govPersonDto,String labelCd,String labelCds);

    /**
     * 查询人口管理
     * add by wuxw
     * @param  govPersonDto
     * @return
     */
    ResponseEntity<String> getGovPersonCompany(GovPersonDto govPersonDto);
    /**
     * 查询人口管理
     * add by wuxw
     * @param  govPersonDto
     * @return
     */
    ResponseEntity<String> queryLargeGovPersonCoun(GovPersonDto govPersonDto);



}
