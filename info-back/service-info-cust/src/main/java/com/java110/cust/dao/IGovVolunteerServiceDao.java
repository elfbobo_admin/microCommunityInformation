package com.java110.cust.dao;


import com.java110.utils.exception.DAOException;


import java.util.List;
import java.util.Map;

/**
 * 志愿者管理组件内部之间使用，没有给外围系统提供服务能力
 * 志愿者管理服务接口类，要求全部以字符串传输，方便微服务化
 * 新建客户，修改客户，删除客户，查询客户等功能
 *
 * Created by wuxw on 2016/12/27.
 */
public interface IGovVolunteerServiceDao {


    /**
     * 保存 志愿者管理信息
     * @param info
     * @throws DAOException DAO异常
     */
    void saveGovVolunteerInfo(Map info) throws DAOException;




    /**
     * 查询志愿者管理信息（instance过程）
     * 根据bId 查询志愿者管理信息
     * @param info bId 信息
     * @return 志愿者管理信息
     * @throws DAOException DAO异常
     */
    List<Map> getGovVolunteerInfo(Map info) throws DAOException;

    /**
     * 查询志愿者管理信息（instance过程）
     * 根据bId 查询志愿者管理信息
     * @param info bId 信息
     * @return 志愿者管理信息
     * @throws DAOException DAO异常
     */
    List<Map> getGovStateVolunteerCount(Map info) throws DAOException;



    /**
     * 修改志愿者管理信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    void updateGovVolunteerInfo(Map info) throws DAOException;


    /**
     * 查询志愿者管理总数
     *
     * @param info 志愿者管理信息
     * @return 志愿者管理数量
     */
    int queryGovVolunteersCount(Map info);

}
