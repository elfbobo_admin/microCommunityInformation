package com.java110.cust.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.cust.dao.IGovGridObjRelServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 网格对象关系服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govGridObjRelServiceDaoImpl")
//@Transactional
public class GovGridObjRelServiceDaoImpl extends BaseServiceDao implements IGovGridObjRelServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovGridObjRelServiceDaoImpl.class);





    /**
     * 保存网格对象关系信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovGridObjRelInfo(Map info) throws DAOException {
        logger.debug("保存网格对象关系信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govGridObjRelServiceDaoImpl.saveGovGridObjRelInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存网格对象关系信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询网格对象关系信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovGridObjRelInfo(Map info) throws DAOException {
        logger.debug("查询网格对象关系信息 入参 info : {}",info);

        List<Map> businessGovGridObjRelInfos = sqlSessionTemplate.selectList("govGridObjRelServiceDaoImpl.getGovGridObjRelInfo",info);

        return businessGovGridObjRelInfos;
    }


    /**
     * 修改网格对象关系信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovGridObjRelInfo(Map info) throws DAOException {
        logger.debug("修改网格对象关系信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govGridObjRelServiceDaoImpl.updateGovGridObjRelInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改网格对象关系信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询网格对象关系数量
     * @param info 网格对象关系信息
     * @return 网格对象关系数量
     */
    @Override
    public int queryGovGridObjRelsCount(Map info) {
        logger.debug("查询网格对象关系数据 入参 info : {}",info);

        List<Map> businessGovGridObjRelInfos = sqlSessionTemplate.selectList("govGridObjRelServiceDaoImpl.queryGovGridObjRelsCount", info);
        if (businessGovGridObjRelInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovGridObjRelInfos.get(0).get("count").toString());
    }


}
