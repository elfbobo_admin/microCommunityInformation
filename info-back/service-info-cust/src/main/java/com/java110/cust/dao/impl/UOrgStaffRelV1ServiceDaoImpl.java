package com.java110.cust.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.cust.dao.IUOrgStaffRelV1ServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 员工角色关系服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("uOrgStaffRelV1ServiceDaoImpl")
//@Transactional
public class UOrgStaffRelV1ServiceDaoImpl extends BaseServiceDao implements IUOrgStaffRelV1ServiceDao {

    private static Logger logger = LoggerFactory.getLogger(UOrgStaffRelV1ServiceDaoImpl.class);





    /**
     * 保存员工角色关系信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveUOrgStaffRelV1Info(Map info) throws DAOException {
        logger.debug("保存员工角色关系信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("uOrgStaffRelV1ServiceDaoImpl.saveUOrgStaffRelV1Info",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存员工角色关系信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询员工角色关系信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getUOrgStaffRelV1Info(Map info) throws DAOException {
        logger.debug("查询员工角色关系信息 入参 info : {}",info);

        List<Map> businessUOrgStaffRelV1Infos = sqlSessionTemplate.selectList("uOrgStaffRelV1ServiceDaoImpl.getUOrgStaffRelV1Info",info);

        return businessUOrgStaffRelV1Infos;
    }


    /**
     * 修改员工角色关系信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateUOrgStaffRelV1Info(Map info) throws DAOException {
        logger.debug("修改员工角色关系信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("uOrgStaffRelV1ServiceDaoImpl.updateUOrgStaffRelV1Info",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改员工角色关系信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询员工角色关系数量
     * @param info 员工角色关系信息
     * @return 员工角色关系数量
     */
    @Override
    public int queryUOrgStaffRelV1sCount(Map info) {
        logger.debug("查询员工角色关系数据 入参 info : {}",info);

        List<Map> businessUOrgStaffRelV1Infos = sqlSessionTemplate.selectList("uOrgStaffRelV1ServiceDaoImpl.queryUOrgStaffRelV1sCount", info);
        if (businessUOrgStaffRelV1Infos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessUOrgStaffRelV1Infos.get(0).get("count").toString());
    }


}
