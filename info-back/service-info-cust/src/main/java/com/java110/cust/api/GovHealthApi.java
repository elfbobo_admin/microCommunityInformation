package com.java110.cust.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.cust.bmo.govHealth.IDeleteGovHealthBMO;
import com.java110.cust.bmo.govHealth.IGetGovHealthBMO;
import com.java110.cust.bmo.govHealth.ISaveGovHealthBMO;
import com.java110.cust.bmo.govHealth.IUpdateGovHealthBMO;
import com.java110.dto.govHealth.GovHealthDto;
import com.java110.po.govHealth.GovHealthPo;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govHealth")
public class GovHealthApi {

    @Autowired
    private ISaveGovHealthBMO saveGovHealthBMOImpl;
    @Autowired
    private IUpdateGovHealthBMO updateGovHealthBMOImpl;
    @Autowired
    private IDeleteGovHealthBMO deleteGovHealthBMOImpl;

    @Autowired
    private IGetGovHealthBMO getGovHealthBMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govHealth/saveGovHealth
     * @path /app/govHealth/saveGovHealth
     */
    @RequestMapping(value = "/saveGovHealth", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovHealth(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "name", "请求报文中未包含name");


        GovHealthPo govHealthPo = BeanConvertUtil.covertBean(reqJson, GovHealthPo.class);
        return saveGovHealthBMOImpl.save(govHealthPo);
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govHealth/updateGovHealth
     * @path /app/govHealth/updateGovHealth
     */
    @RequestMapping(value = "/updateGovHealth", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovHealth(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "healthId", "请求报文中未包含healthId");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "name", "请求报文中未包含name");
        Assert.hasKeyAndValue(reqJson, "healthId", "healthId不能为空");


        GovHealthPo govHealthPo = BeanConvertUtil.covertBean(reqJson, GovHealthPo.class);
        return updateGovHealthBMOImpl.update(govHealthPo);
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govHealth/deleteGovHealth
     * @path /app/govHealth/deleteGovHealth
     */
    @RequestMapping(value = "/deleteGovHealth", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovHealth(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "caId", "caId不能为空");

        Assert.hasKeyAndValue(reqJson, "healthId", "healthId不能为空");


        GovHealthPo govHealthPo = BeanConvertUtil.covertBean(reqJson, GovHealthPo.class);
        return deleteGovHealthBMOImpl.delete(govHealthPo);
    }

    /**
     * 微信删除消息模板
     *
     * @param caId 小区ID
     * @return
     * @serviceCode /govHealth/queryGovHealth
     * @path /app/govHealth/queryGovHealth
     */
    @RequestMapping(value = "/queryGovHealth", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovHealth(@RequestParam(value = "caId") String caId,
                                                 @RequestParam(value = "page") int page,
                                                 @RequestParam(value = "row") int row) {
        GovHealthDto govHealthDto = new GovHealthDto();
        govHealthDto.setPage(page);
        govHealthDto.setRow(row);
        govHealthDto.setCaId(caId);
        return getGovHealthBMOImpl.get(govHealthDto);
    }
}
