package com.java110.cust.bmo.govHealthAnswerValue;
import org.springframework.http.ResponseEntity;
import com.java110.po.govHealthAnswerValue.GovHealthAnswerValuePo;

public interface IDeleteGovHealthAnswerValueBMO {


    /**
     * 修改体检项目答案
     * add by wuxw
     * @param govHealthAnswerValuePo
     * @return
     */
    ResponseEntity<String> delete(GovHealthAnswerValuePo govHealthAnswerValuePo);


}
