package com.java110.cust.bmo.govServField.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.cust.bmo.govServField.IGetGovServFieldBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.cust.IGovServFieldInnerServiceSMO;
import com.java110.dto.govServField.GovServFieldDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import org.springframework.http.HttpStatus;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service("getGovServFieldBMOImpl")
public class GetGovServFieldBMOImpl implements IGetGovServFieldBMO {

    @Autowired
    private IGovServFieldInnerServiceSMO govServFieldInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govServFieldDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovServFieldDto govServFieldDto) {


        int count = govServFieldInnerServiceSMOImpl.queryGovServFieldsCount(govServFieldDto);

        List<GovServFieldDto> govServFieldDtos = null;
        if (count > 0) {
            govServFieldDtos = govServFieldInnerServiceSMOImpl.queryGovServFields(govServFieldDto);
        } else {
            govServFieldDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govServFieldDto.getRow()), count, govServFieldDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
