package com.java110.cust.bmo.govMedicalGrade;

import org.springframework.http.ResponseEntity;
import com.java110.po.govMedicalGrade.GovMedicalGradePo;
public interface ISaveGovMedicalGradeBMO {


    /**
     * 添加医疗分级
     * add by wuxw
     * @param govMedicalGradePo
     * @return
     */
    ResponseEntity<String> save(GovMedicalGradePo govMedicalGradePo);


}
