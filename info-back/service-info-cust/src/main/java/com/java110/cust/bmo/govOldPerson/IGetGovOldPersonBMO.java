package com.java110.cust.bmo.govOldPerson;
import com.java110.dto.govOldPerson.GovOldPersonDto;
import org.springframework.http.ResponseEntity;
import com.java110.dto.store.StoreDto;
public interface IGetGovOldPersonBMO {


    /**
     * 查询老人管理
     * add by wuxw
     * @param  govOldPersonDto
     * @return
     */
    ResponseEntity<String> get(GovOldPersonDto govOldPersonDto);

    /**
     * 查询老人管理
     * add by wuxw
     * @param  govOldPersonDto
     * @return
     */
    ResponseEntity<String> getGovOldTypePersonCount(GovOldPersonDto govOldPersonDto);

    /**
     * 查询老人管理
     * add by wuxw
     * @param  govOldPersonDto
     * @return
     */
    ResponseEntity<String> queryGovOldAccountDetail(GovOldPersonDto govOldPersonDto);

    /**
     * 查询老人管理
     * add by wuxw
     * @param
     * @return
     */
    ResponseEntity<String> queryAccountAmount();
    /**
     * 查询老人管理
     * add by wuxw
     * @param
     * @return
     */
    ResponseEntity<String> queryStoreOrderCart(int page,int row);


}
