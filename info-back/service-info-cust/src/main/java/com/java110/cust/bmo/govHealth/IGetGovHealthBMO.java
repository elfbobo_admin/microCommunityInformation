package com.java110.cust.bmo.govHealth;
import com.java110.dto.govHealth.GovHealthDto;
import org.springframework.http.ResponseEntity;
import com.java110.dto.store.StoreDto;
public interface IGetGovHealthBMO {


    /**
     * 查询体检项目
     * add by wuxw
     * @param  govHealthDto
     * @return
     */
    ResponseEntity<String> get(GovHealthDto govHealthDto);


}
