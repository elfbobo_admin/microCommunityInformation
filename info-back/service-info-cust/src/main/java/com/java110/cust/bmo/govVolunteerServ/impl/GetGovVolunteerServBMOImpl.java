package com.java110.cust.bmo.govVolunteerServ.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.cust.bmo.govVolunteerServ.IGetGovVolunteerServBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.cust.IGovVolunteerServInnerServiceSMO;
import com.java110.dto.govVolunteerServ.GovVolunteerServDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import org.springframework.http.HttpStatus;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service("getGovVolunteerServBMOImpl")
public class GetGovVolunteerServBMOImpl implements IGetGovVolunteerServBMO {

    @Autowired
    private IGovVolunteerServInnerServiceSMO govVolunteerServInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govVolunteerServDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovVolunteerServDto govVolunteerServDto) {


        int count = govVolunteerServInnerServiceSMOImpl.queryGovVolunteerServsCount(govVolunteerServDto);

        List<GovVolunteerServDto> govVolunteerServDtos = null;
        if (count > 0) {
            govVolunteerServDtos = govVolunteerServInnerServiceSMOImpl.queryGovVolunteerServs(govVolunteerServDto);
        } else {
            govVolunteerServDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govVolunteerServDto.getRow()), count, govVolunteerServDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
