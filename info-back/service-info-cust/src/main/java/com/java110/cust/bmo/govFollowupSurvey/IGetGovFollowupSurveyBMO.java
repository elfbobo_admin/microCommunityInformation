package com.java110.cust.bmo.govFollowupSurvey;
import org.springframework.http.ResponseEntity;
import com.java110.dto.govFollowupSurvey.GovFollowupSurveyDto;
public interface IGetGovFollowupSurveyBMO {


    /**
     * 查询随访登记
     * add by wuxw
     * @param  govFollowupSurveyDto
     * @return
     */
    ResponseEntity<String> get(GovFollowupSurveyDto govFollowupSurveyDto);


}
