package com.java110.cust.bmo.govGridType;
import com.java110.dto.govGridType.GovGridTypeDto;
import org.springframework.http.ResponseEntity;
public interface IGetGovGridTypeBMO {


    /**
     * 查询网格类型
     * add by wuxw
     * @param  govGridTypeDto
     * @return
     */
    ResponseEntity<String> get(GovGridTypeDto govGridTypeDto);


}
