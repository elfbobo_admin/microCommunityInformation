package com.java110.cust.bmo.govGridType;

import com.java110.po.govGridType.GovGridTypePo;
import org.springframework.http.ResponseEntity;
public interface ISaveGovGridTypeBMO {


    /**
     * 添加网格类型
     * add by wuxw
     * @param govGridTypePo
     * @return
     */
    ResponseEntity<String> save(GovGridTypePo govGridTypePo,String storeId);


}
