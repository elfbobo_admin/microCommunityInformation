package com.java110.cust.bmo.govOwner;

import com.java110.po.govOwner.GovOwnerPo;
import org.springframework.http.ResponseEntity;
public interface ISaveGovOwnerBMO {


    /**
     * 添加户籍管理
     * add by wuxw
     * @param govOwnerPo
     * @return
     */
    ResponseEntity<String> save(GovOwnerPo govOwnerPo);


}
