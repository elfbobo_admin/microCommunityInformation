package com.java110.cust.bmo.govMedicalClassify.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.cust.bmo.govMedicalClassify.IGetGovMedicalClassifyBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.cust.IGovMedicalClassifyInnerServiceSMO;
import com.java110.dto.govMedicalClassify.GovMedicalClassifyDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import org.springframework.http.HttpStatus;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service("getGovMedicalClassifyBMOImpl")
public class GetGovMedicalClassifyBMOImpl implements IGetGovMedicalClassifyBMO {

    @Autowired
    private IGovMedicalClassifyInnerServiceSMO govMedicalClassifyInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govMedicalClassifyDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovMedicalClassifyDto govMedicalClassifyDto) {


        int count = govMedicalClassifyInnerServiceSMOImpl.queryGovMedicalClassifysCount(govMedicalClassifyDto);

        List<GovMedicalClassifyDto> govMedicalClassifyDtos = null;
        if (count > 0) {
            govMedicalClassifyDtos = govMedicalClassifyInnerServiceSMOImpl.queryGovMedicalClassifys(govMedicalClassifyDto);
        } else {
            govMedicalClassifyDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govMedicalClassifyDto.getRow()), count, govMedicalClassifyDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
