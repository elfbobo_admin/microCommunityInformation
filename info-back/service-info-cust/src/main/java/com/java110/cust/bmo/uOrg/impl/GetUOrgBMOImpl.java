package com.java110.cust.bmo.uOrg.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.cust.bmo.uOrg.IGetUOrgBMO;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.cust.IUOrgInnerServiceSMO;
import com.java110.dto.uOrg.UOrgDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import org.springframework.http.HttpStatus;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service("getUOrgBMOImpl")
public class GetUOrgBMOImpl implements IGetUOrgBMO {

    @Autowired
    private IUOrgInnerServiceSMO uOrgInnerServiceSMOImpl;

    /**
     *
     *
     * @param  uOrgDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(UOrgDto uOrgDto) {


        int count = uOrgInnerServiceSMOImpl.queryUOrgsCount(uOrgDto);

        List<UOrgDto> uOrgDtos = null;
        if (count > 0) {
            uOrgDtos = uOrgInnerServiceSMOImpl.queryUOrgs(uOrgDto);
        } else {
            uOrgDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) uOrgDto.getRow()), count, uOrgDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
