package com.java110.cust.bmo.UserV1;
import com.java110.po.UserV1.UserV1Po;
import org.springframework.http.ResponseEntity;
import com.java110.po.store.StorePo;

public interface IDeleteUserV1BMO {


    /**
     * 修改用戶管理
     * add by wuxw
     * @param UserV1Po
     * @return
     */
    ResponseEntity<String> delete(UserV1Po UserV1Po);


}
