package com.java110.cust.smo.impl;


import com.java110.cust.dao.IStoreUserV1ServiceDao;
import com.java110.intf.cust.IStoreUserV1InnerServiceSMO;
import com.java110.dto.StoreUserV1.StoreUserV1Dto;
import com.java110.po.StoreUserV1.StoreUserV1Po;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 用戶管理内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class StoreUserV1InnerServiceSMOImpl extends BaseServiceSMO implements IStoreUserV1InnerServiceSMO {

    @Autowired
    private IStoreUserV1ServiceDao storeUserV1ServiceDaoImpl;


    @Override
    public int saveStoreUserV1(@RequestBody  StoreUserV1Po storeUserV1Po) {
        int saveFlag = 1;
        storeUserV1ServiceDaoImpl.saveStoreUserV1Info(BeanConvertUtil.beanCovertMap(storeUserV1Po));
        return saveFlag;
    }

     @Override
    public int updateStoreUserV1(@RequestBody  StoreUserV1Po storeUserV1Po) {
        int saveFlag = 1;
         storeUserV1ServiceDaoImpl.updateStoreUserV1Info(BeanConvertUtil.beanCovertMap(storeUserV1Po));
        return saveFlag;
    }

     @Override
    public int deleteStoreUserV1(@RequestBody  StoreUserV1Po storeUserV1Po) {
        int saveFlag = 1;
        storeUserV1Po.setStatusCd("1");
        storeUserV1ServiceDaoImpl.updateStoreUserV1Info(BeanConvertUtil.beanCovertMap(storeUserV1Po));
        return saveFlag;
    }

    @Override
    public List<StoreUserV1Dto> queryStoreUserV1s(@RequestBody  StoreUserV1Dto storeUserV1Dto) {

        //校验是否传了 分页信息

        int page = storeUserV1Dto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            storeUserV1Dto.setPage((page - 1) * storeUserV1Dto.getRow());
        }

        List<StoreUserV1Dto> storeUserV1s = BeanConvertUtil.covertBeanList(storeUserV1ServiceDaoImpl.getStoreUserV1Info(BeanConvertUtil.beanCovertMap(storeUserV1Dto)), StoreUserV1Dto.class);

        return storeUserV1s;
    }


    @Override
    public int queryStoreUserV1sCount(@RequestBody StoreUserV1Dto storeUserV1Dto) {
        return storeUserV1ServiceDaoImpl.queryStoreUserV1sCount(BeanConvertUtil.beanCovertMap(storeUserV1Dto));    }

    public IStoreUserV1ServiceDao getStoreUserV1ServiceDaoImpl() {
        return storeUserV1ServiceDaoImpl;
    }

    public void setStoreUserV1ServiceDaoImpl(IStoreUserV1ServiceDao storeUserV1ServiceDaoImpl) {
        this.storeUserV1ServiceDaoImpl = storeUserV1ServiceDaoImpl;
    }
}
