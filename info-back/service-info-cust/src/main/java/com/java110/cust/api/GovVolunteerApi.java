package com.java110.cust.api;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govVolunteer.GovVolunteerDto;
import com.java110.po.govPerson.GovPersonPo;
import com.java110.po.govVolunteer.GovVolunteerPo;
import com.java110.cust.bmo.govVolunteer.IDeleteGovVolunteerBMO;
import com.java110.cust.bmo.govVolunteer.IGetGovVolunteerBMO;
import com.java110.cust.bmo.govVolunteer.ISaveGovVolunteerBMO;
import com.java110.cust.bmo.govVolunteer.IUpdateGovVolunteerBMO;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govVolunteer")
public class GovVolunteerApi {

    @Autowired
    private ISaveGovVolunteerBMO saveGovVolunteerBMOImpl;
    @Autowired
    private IUpdateGovVolunteerBMO updateGovVolunteerBMOImpl;
    @Autowired
    private IDeleteGovVolunteerBMO deleteGovVolunteerBMOImpl;

    @Autowired
    private IGetGovVolunteerBMO getGovVolunteerBMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govVolunteer/saveGovVolunteer
     * @path /app/govVolunteer/saveGovVolunteer
     */
    @RequestMapping(value = "/saveGovVolunteer", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovVolunteer(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "name", "请求报文中未包含name");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "address", "请求报文中未包含address");
        Assert.hasKeyAndValue(reqJson, "tel", "请求报文中未包含tel");
        Assert.hasKeyAndValue(reqJson, "student", "请求报文中未包含student");
        Assert.hasKeyAndValue(reqJson, "company", "请求报文中未包含company");
        Assert.hasKeyAndValue(reqJson, "volWork", "请求报文中未包含volWork");
        Assert.hasKeyAndValue(reqJson, "edu", "请求报文中未包含edu");

        Assert.isNotNull(reqJson.getJSONArray("goodAtSkills"), "请求报文中未包含goodAtSkills");
        Assert.isNotNull(reqJson.getJSONArray("freeTime"), "请求报文中未包含freeTime");
        Assert.isNotNull(reqJson.getJSONArray("volunteerServ"), "请求报文中未包含volunteerServ");

        Assert.hasKeyAndValue(reqJson.getJSONObject("persons"), "idCard", "请求报文中未包含idCard");
        Assert.hasKeyAndValue(reqJson.getJSONObject("persons"), "idType", "请求报文中未包含idType");
        Assert.hasKeyAndValue(reqJson.getJSONObject("persons"), "nation", "请求报文中未包含nation");
        Assert.hasKeyAndValue(reqJson.getJSONObject("persons"), "nativePlace", "请求报文中未包含nativePlace");
        Assert.hasKeyAndValue(reqJson.getJSONObject("persons"), "personSex", "请求报文中未包含personSex");
        Assert.hasKeyAndValue(reqJson.getJSONObject("persons"), "politicalOutlook", "请求报文中未包含politicalOutlook");


        return saveGovVolunteerBMOImpl.save(reqJson);
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govVolunteer/updateGovVolunteer
     * @path /app/govVolunteer/updateGovVolunteer
     */
    @RequestMapping(value = "/updateGovVolunteer", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovVolunteer(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "volunteerId", "请求报文中未包含volunteerId");
        Assert.hasKeyAndValue(reqJson, "name", "请求报文中未包含name");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "address", "请求报文中未包含address");
        Assert.hasKeyAndValue(reqJson, "tel", "请求报文中未包含tel");
        Assert.hasKeyAndValue(reqJson, "student", "请求报文中未包含student");
        Assert.hasKeyAndValue(reqJson, "company", "请求报文中未包含company");
        Assert.hasKeyAndValue(reqJson, "volWork", "请求报文中未包含volWork");
        Assert.hasKeyAndValue(reqJson, "edu", "请求报文中未包含edu");
        Assert.hasKeyAndValue(reqJson, "govPersonId", "请求报文中未包含govPersonId");

        Assert.isNotNull(reqJson.getJSONArray("goodAtSkills"), "请求报文中未包含goodAtSkills");
        Assert.isNotNull(reqJson.getJSONArray("freeTime"), "请求报文中未包含freeTime");
        Assert.isNotNull(reqJson.getJSONArray("volunteerServ"), "请求报文中未包含volunteerServ");

        Assert.hasKeyAndValue(reqJson.getJSONObject("persons"), "idCard", "请求报文中未包含idCard");
        Assert.hasKeyAndValue(reqJson.getJSONObject("persons"), "idType", "请求报文中未包含idType");
        Assert.hasKeyAndValue(reqJson.getJSONObject("persons"), "nation", "请求报文中未包含nation");
        Assert.hasKeyAndValue(reqJson.getJSONObject("persons"), "nativePlace", "请求报文中未包含nativePlace");
        Assert.hasKeyAndValue(reqJson.getJSONObject("persons"), "personSex", "请求报文中未包含personSex");
        Assert.hasKeyAndValue(reqJson.getJSONObject("persons"), "politicalOutlook", "请求报文中未包含politicalOutlook");

        return updateGovVolunteerBMOImpl.update(reqJson);
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govVolunteer/reviewGovVolunteer
     * @path /app/govVolunteer/reviewGovVolunteer
     */
    @RequestMapping(value = "/reviewGovVolunteer", method = RequestMethod.POST)
    public ResponseEntity<String> reviewGovVolunteer(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "volunteerId", "请求报文中未包含volunteerId");
        Assert.hasKeyAndValue(reqJson, "state", "请求报文中未包含state");

        return updateGovVolunteerBMOImpl.review(reqJson);
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govVolunteer/deleteGovVolunteer
     * @path /app/govVolunteer/deleteGovVolunteer
     */
    @RequestMapping(value = "/deleteGovVolunteer", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovVolunteer(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "caId", "caId不能为空");
        Assert.hasKeyAndValue(reqJson, "govPersonId", "caId不能为空");
        Assert.hasKeyAndValue(reqJson, "volunteerId", "volunteerId不能为空");


        GovVolunteerPo govVolunteerPo = BeanConvertUtil.covertBean(reqJson, GovVolunteerPo.class);
        GovPersonPo govPersonPo = BeanConvertUtil.covertBean(reqJson, GovPersonPo.class);
        govPersonPo.setIsWeb("F");
        return deleteGovVolunteerBMOImpl.delete(govVolunteerPo, govPersonPo);
    }

    /**
     * 微信删除消息模板
     *
     * @param caId 小区ID
     * @return
     * @serviceCode /govVolunteer/queryGovVolunteer
     * @path /app/govVolunteer/queryGovVolunteer
     */
    @RequestMapping(value = "/queryGovVolunteer", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovVolunteer(@RequestParam(value = "caId", required = false) String caId,
                                                    @RequestParam(value = "name", required = false) String name,
                                                    @RequestParam(value = "volunteerId", required = false) String volunteerId,
                                                    @RequestParam(value = "state", required = false) String state,
                                                    @RequestParam(value = "tel", required = false) String tel,
                                                    @RequestParam(value = "page") int page,
                                                    @RequestParam(value = "row") int row) {
        GovVolunteerDto govVolunteerDto = new GovVolunteerDto();
        govVolunteerDto.setPage(page);
        govVolunteerDto.setRow(row);
        govVolunteerDto.setCaId(caId);
        govVolunteerDto.setName(name);
        govVolunteerDto.setTel(tel);
        govVolunteerDto.setVolunteerId(volunteerId);
        govVolunteerDto.setState(state);
        return getGovVolunteerBMOImpl.get(govVolunteerDto);
    }
    /**
     * 微信删除消息模板
     *
     * @param caId 小区ID
     * @return
     * @serviceCode /govVolunteer/getGovStateVolunteerCount
     * @path /app/govVolunteer/queryGovVolunteer
     */
    @RequestMapping(value = "/getGovStateVolunteerCount", method = RequestMethod.GET)
    public ResponseEntity<String> getGovStateVolunteerCount(@RequestParam(value = "caId", required = false) String caId,
                                                    @RequestParam(value = "page") int page,
                                                    @RequestParam(value = "row") int row) {
        GovVolunteerDto govVolunteerDto = new GovVolunteerDto();
        govVolunteerDto.setPage(page);
        govVolunteerDto.setRow(row);
        govVolunteerDto.setCaId(caId);
        return getGovVolunteerBMOImpl.getGovStateVolunteerCount(govVolunteerDto);
    }

    /**
     * 微信删除消息模板
     *
     * @param caId 小区ID
     * @return
     * @serviceCode /govVolunteer/getVolunteerAccountDetail
     * @path /app/govVolunteer/getVolunteerAccountDetail
     */
    @RequestMapping(value = "/getVolunteerAccountDetail", method = RequestMethod.GET)
    public ResponseEntity<String> getVolunteerAccountDetail(@RequestParam(value = "caId", required = false) String caId,
                                                            @RequestParam(value = "volunteerId", required = false) String volunteerId,
                                                            @RequestParam(value = "detailType", required = false) String detailType,
                                                            @RequestParam(value = "orderId", required = false) String orderId,
                                                            @RequestParam(value = "page") int page,
                                                            @RequestParam(value = "row") int row) {
        GovVolunteerDto govVolunteerDto = new GovVolunteerDto();
        govVolunteerDto.setPage(page);
        govVolunteerDto.setRow(row);
        govVolunteerDto.setCaId(caId);
        govVolunteerDto.setVolunteerId(volunteerId);
        govVolunteerDto.setDetailType(detailType);
        govVolunteerDto.setOrderId(orderId);
        return getGovVolunteerBMOImpl.getVolunteerAccountDetail(govVolunteerDto);
    }
}
