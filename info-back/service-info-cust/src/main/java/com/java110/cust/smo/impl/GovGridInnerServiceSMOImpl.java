package com.java110.cust.smo.impl;


import com.java110.cust.dao.IGovGridServiceDao;
import com.java110.intf.cust.IGovGridInnerServiceSMO;
import com.java110.dto.govGrid.GovGridDto;
import com.java110.po.govGrid.GovGridPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 网格人员内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovGridInnerServiceSMOImpl extends BaseServiceSMO implements IGovGridInnerServiceSMO {

    @Autowired
    private IGovGridServiceDao govGridServiceDaoImpl;


    @Override
    public int saveGovGrid(@RequestBody  GovGridPo govGridPo) {
        int saveFlag = 1;
        govGridServiceDaoImpl.saveGovGridInfo(BeanConvertUtil.beanCovertMap(govGridPo));
        return saveFlag;
    }

     @Override
    public int updateGovGrid(@RequestBody  GovGridPo govGridPo) {
        int saveFlag = 1;
         govGridServiceDaoImpl.updateGovGridInfo(BeanConvertUtil.beanCovertMap(govGridPo));
        return saveFlag;
    }

     @Override
    public int deleteGovGrid(@RequestBody  GovGridPo govGridPo) {
        int saveFlag = 1;
        govGridPo.setStatusCd("1");
        govGridServiceDaoImpl.updateGovGridInfo(BeanConvertUtil.beanCovertMap(govGridPo));
        return saveFlag;
    }

    @Override
    public List<GovGridDto> queryGovGrids(@RequestBody  GovGridDto govGridDto) {

        //校验是否传了 分页信息

        int page = govGridDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govGridDto.setPage((page - 1) * govGridDto.getRow());
        }

        List<GovGridDto> govGrids = BeanConvertUtil.covertBeanList(govGridServiceDaoImpl.getGovGridInfo(BeanConvertUtil.beanCovertMap(govGridDto)), GovGridDto.class);

        return govGrids;
    }


    @Override
    public int queryGovGridsCount(@RequestBody GovGridDto govGridDto) {
        return govGridServiceDaoImpl.queryGovGridsCount(BeanConvertUtil.beanCovertMap(govGridDto));    }

    public IGovGridServiceDao getGovGridServiceDaoImpl() {
        return govGridServiceDaoImpl;
    }

    public void setGovGridServiceDaoImpl(IGovGridServiceDao govGridServiceDaoImpl) {
        this.govGridServiceDaoImpl = govGridServiceDaoImpl;
    }
}
