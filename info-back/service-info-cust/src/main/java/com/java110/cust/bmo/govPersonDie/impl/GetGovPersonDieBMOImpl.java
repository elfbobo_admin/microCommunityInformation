package com.java110.cust.bmo.govPersonDie.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.cust.bmo.govPersonDie.IGetGovPersonDieBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.cust.IGovPersonDieInnerServiceSMO;
import com.java110.dto.govPersonDie.GovPersonDieDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import org.springframework.http.HttpStatus;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service("getGovPersonDieBMOImpl")
public class GetGovPersonDieBMOImpl implements IGetGovPersonDieBMO {

    @Autowired
    private IGovPersonDieInnerServiceSMO govPersonDieInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govPersonDieDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovPersonDieDto govPersonDieDto) {


        int count = govPersonDieInnerServiceSMOImpl.queryGovPersonDiesCount(govPersonDieDto);

        List<GovPersonDieDto> govPersonDieDtos = null;
        if (count > 0) {
            govPersonDieDtos = govPersonDieInnerServiceSMOImpl.queryGovPersonDies(govPersonDieDto);
        } else {
            govPersonDieDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govPersonDieDto.getRow()), count, govPersonDieDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
