package com.java110.cust.bmo.govOwnerPerson;
import com.java110.dto.govOwnerPerson.GovOwnerPersonDto;
import org.springframework.http.ResponseEntity;
public interface IGetGovOwnerPersonBMO {


    /**
     * 查询人口户籍关系
     * add by wuxw
     * @param  govOwnerPersonDto
     * @return
     */
    ResponseEntity<String> get(GovOwnerPersonDto govOwnerPersonDto);


}
