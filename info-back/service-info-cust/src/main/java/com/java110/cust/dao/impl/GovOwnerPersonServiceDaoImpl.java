package com.java110.cust.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.cust.dao.IGovOwnerPersonServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 人口户籍关系服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govOwnerPersonServiceDaoImpl")
//@Transactional
public class GovOwnerPersonServiceDaoImpl extends BaseServiceDao implements IGovOwnerPersonServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovOwnerPersonServiceDaoImpl.class);





    /**
     * 保存人口户籍关系信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovOwnerPersonInfo(Map info) throws DAOException {
        logger.debug("保存人口户籍关系信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govOwnerPersonServiceDaoImpl.saveGovOwnerPersonInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存人口户籍关系信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询人口户籍关系信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovOwnerPersonInfo(Map info) throws DAOException {
        logger.debug("查询人口户籍关系信息 入参 info : {}",info);

        List<Map> businessGovOwnerPersonInfos = sqlSessionTemplate.selectList("govOwnerPersonServiceDaoImpl.getGovOwnerPersonInfo",info);

        return businessGovOwnerPersonInfos;
    }


    /**
     * 修改人口户籍关系信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovOwnerPersonInfo(Map info) throws DAOException {
        logger.debug("修改人口户籍关系信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govOwnerPersonServiceDaoImpl.updateGovOwnerPersonInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改人口户籍关系信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询人口户籍关系数量
     * @param info 人口户籍关系信息
     * @return 人口户籍关系数量
     */
    @Override
    public int queryGovOwnerPersonsCount(Map info) {
        logger.debug("查询人口户籍关系数据 入参 info : {}",info);

        List<Map> businessGovOwnerPersonInfos = sqlSessionTemplate.selectList("govOwnerPersonServiceDaoImpl.queryGovOwnerPersonsCount", info);
        if (businessGovOwnerPersonInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovOwnerPersonInfos.get(0).get("count").toString());
    }


}
