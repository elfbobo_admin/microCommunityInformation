package com.java110.cust.dao;


import com.java110.utils.exception.DAOException;


import java.util.List;
import java.util.Map;

/**
 * 老人属性组件内部之间使用，没有给外围系统提供服务能力
 * 老人属性服务接口类，要求全部以字符串传输，方便微服务化
 * 新建客户，修改客户，删除客户，查询客户等功能
 *
 * Created by wuxw on 2016/12/27.
 */
public interface IGovOldPersonAttrServiceDao {


    /**
     * 保存 老人属性信息
     * @param info
     * @throws DAOException DAO异常
     */
    void saveGovOldPersonAttrInfo(Map info) throws DAOException;




    /**
     * 查询老人属性信息（instance过程）
     * 根据bId 查询老人属性信息
     * @param info bId 信息
     * @return 老人属性信息
     * @throws DAOException DAO异常
     */
    List<Map> getGovOldPersonAttrInfo(Map info) throws DAOException;



    /**
     * 修改老人属性信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    void updateGovOldPersonAttrInfo(Map info) throws DAOException;


    /**
     * 查询老人属性总数
     *
     * @param info 老人属性信息
     * @return 老人属性数量
     */
    int queryGovOldPersonAttrsCount(Map info);

}
