package com.java110.cust.dao;


import com.java110.utils.exception.DAOException;


import java.util.List;
import java.util.Map;

/**
 * 网格类型组件内部之间使用，没有给外围系统提供服务能力
 * 网格类型服务接口类，要求全部以字符串传输，方便微服务化
 * 新建客户，修改客户，删除客户，查询客户等功能
 *
 * Created by wuxw on 2016/12/27.
 */
public interface IGovGridTypeServiceDao {


    /**
     * 保存 网格类型信息
     * @param info
     * @throws DAOException DAO异常
     */
    void saveGovGridTypeInfo(Map info) throws DAOException;




    /**
     * 查询网格类型信息（instance过程）
     * 根据bId 查询网格类型信息
     * @param info bId 信息
     * @return 网格类型信息
     * @throws DAOException DAO异常
     */
    List<Map> getGovGridTypeInfo(Map info) throws DAOException;



    /**
     * 修改网格类型信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    void updateGovGridTypeInfo(Map info) throws DAOException;


    /**
     * 查询网格类型总数
     *
     * @param info 网格类型信息
     * @return 网格类型数量
     */
    int queryGovGridTypesCount(Map info);

}
