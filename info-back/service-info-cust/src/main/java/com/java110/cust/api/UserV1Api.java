package com.java110.cust.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.UserV1.UserV1Dto;
import com.java110.po.UserV1.UserV1Po;
import com.java110.cust.bmo.UserV1.IDeleteUserV1BMO;
import com.java110.cust.bmo.UserV1.IGetUserV1BMO;
import com.java110.cust.bmo.UserV1.ISaveUserV1BMO;
import com.java110.cust.bmo.UserV1.IUpdateUserV1BMO;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/UserV1")
public class UserV1Api {

    @Autowired
    private ISaveUserV1BMO saveUserV1BMOImpl;
    @Autowired
    private IUpdateUserV1BMO updateUserV1BMOImpl;
    @Autowired
    private IDeleteUserV1BMO deleteUserV1BMOImpl;

    @Autowired
    private IGetUserV1BMO getUserV1BMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /UserV1/saveUserV1
     * @path /app/UserV1/saveUserV1
     */
    @RequestMapping(value = "/saveUserV1", method = RequestMethod.POST)
    public ResponseEntity<String> saveUserV1(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "id", "请求报文中未包含id");
        Assert.hasKeyAndValue(reqJson, "userId", "请求报文中未包含userId");
        Assert.hasKeyAndValue(reqJson, "name", "请求报文中未包含name");
        Assert.hasKeyAndValue(reqJson, "tel", "请求报文中未包含tel");
        Assert.hasKeyAndValue(reqJson, "statusCd", "请求报文中未包含statusCd");


        UserV1Po UserV1Po = BeanConvertUtil.covertBean(reqJson, UserV1Po.class);
        return saveUserV1BMOImpl.save(UserV1Po);
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /UserV1/updateUserV1
     * @path /app/UserV1/updateUserV1
     */
    @RequestMapping(value = "/updateUserV1", method = RequestMethod.POST)
    public ResponseEntity<String> updateUserV1(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "id", "请求报文中未包含id");
        Assert.hasKeyAndValue(reqJson, "userId", "请求报文中未包含userId");
        Assert.hasKeyAndValue(reqJson, "name", "请求报文中未包含name");
        Assert.hasKeyAndValue(reqJson, "tel", "请求报文中未包含tel");
        Assert.hasKeyAndValue(reqJson, "statusCd", "请求报文中未包含statusCd");
        Assert.hasKeyAndValue(reqJson, "id", "id不能为空");


        UserV1Po UserV1Po = BeanConvertUtil.covertBean(reqJson, UserV1Po.class);
        return updateUserV1BMOImpl.update(UserV1Po);
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /UserV1/deleteUserV1
     * @path /app/UserV1/deleteUserV1
     */
    @RequestMapping(value = "/deleteUserV1", method = RequestMethod.POST)
    public ResponseEntity<String> deleteUserV1(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "communityId", "小区ID不能为空");

        Assert.hasKeyAndValue(reqJson, "id", "id不能为空");


        UserV1Po UserV1Po = BeanConvertUtil.covertBean(reqJson, UserV1Po.class);
        return deleteUserV1BMOImpl.delete(UserV1Po);
    }

    /**
     * 微信删除消息模板
     *
     * @param
     * @return
     * @serviceCode /UserV1/queryUserV1
     * @path /app/UserV1/queryUserV1
     */
    @RequestMapping(value = "/queryUserV1", method = RequestMethod.GET)
    public ResponseEntity<String> queryUserV1(@RequestParam(value = "page") int page,
                                              @RequestParam(value = "row") int row) {
        UserV1Dto UserV1Dto = new UserV1Dto();
        UserV1Dto.setPage(page);
        UserV1Dto.setRow(row);
        return getUserV1BMOImpl.get(UserV1Dto);
    }
}
