package com.java110.cust.smo.impl;


import com.java110.cust.dao.IGovVolunteerServServiceDao;
import com.java110.intf.cust.IGovVolunteerServInnerServiceSMO;
import com.java110.dto.govVolunteerServ.GovVolunteerServDto;
import com.java110.po.govVolunteerServ.GovVolunteerServPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 服务领域内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovVolunteerServInnerServiceSMOImpl extends BaseServiceSMO implements IGovVolunteerServInnerServiceSMO {

    @Autowired
    private IGovVolunteerServServiceDao govVolunteerServServiceDaoImpl;


    @Override
    public int saveGovVolunteerServ(@RequestBody  GovVolunteerServPo govVolunteerServPo) {
        int saveFlag = 1;
        govVolunteerServServiceDaoImpl.saveGovVolunteerServInfo(BeanConvertUtil.beanCovertMap(govVolunteerServPo));
        return saveFlag;
    }

     @Override
    public int updateGovVolunteerServ(@RequestBody  GovVolunteerServPo govVolunteerServPo) {
        int saveFlag = 1;
         govVolunteerServServiceDaoImpl.updateGovVolunteerServInfo(BeanConvertUtil.beanCovertMap(govVolunteerServPo));
        return saveFlag;
    }

     @Override
    public int deleteGovVolunteerServ(@RequestBody  GovVolunteerServPo govVolunteerServPo) {
        int saveFlag = 1;
        govVolunteerServPo.setStatusCd("1");
        govVolunteerServServiceDaoImpl.updateGovVolunteerServInfo(BeanConvertUtil.beanCovertMap(govVolunteerServPo));
        return saveFlag;
    }

    @Override
    public List<GovVolunteerServDto> queryGovVolunteerServs(@RequestBody  GovVolunteerServDto govVolunteerServDto) {

        //校验是否传了 分页信息

        int page = govVolunteerServDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govVolunteerServDto.setPage((page - 1) * govVolunteerServDto.getRow());
        }

        List<GovVolunteerServDto> govVolunteerServs = BeanConvertUtil.covertBeanList(govVolunteerServServiceDaoImpl.getGovVolunteerServInfo(BeanConvertUtil.beanCovertMap(govVolunteerServDto)), GovVolunteerServDto.class);

        return govVolunteerServs;
    }


    @Override
    public int queryGovVolunteerServsCount(@RequestBody GovVolunteerServDto govVolunteerServDto) {
        return govVolunteerServServiceDaoImpl.queryGovVolunteerServsCount(BeanConvertUtil.beanCovertMap(govVolunteerServDto));    }

    public IGovVolunteerServServiceDao getGovVolunteerServServiceDaoImpl() {
        return govVolunteerServServiceDaoImpl;
    }

    public void setGovVolunteerServServiceDaoImpl(IGovVolunteerServServiceDao govVolunteerServServiceDaoImpl) {
        this.govVolunteerServServiceDaoImpl = govVolunteerServServiceDaoImpl;
    }
}
