package com.java110.cust.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.cust.dao.IGovVolunteerServServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 服务领域服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govVolunteerServServiceDaoImpl")
//@Transactional
public class GovVolunteerServServiceDaoImpl extends BaseServiceDao implements IGovVolunteerServServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovVolunteerServServiceDaoImpl.class);





    /**
     * 保存服务领域信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovVolunteerServInfo(Map info) throws DAOException {
        logger.debug("保存服务领域信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govVolunteerServServiceDaoImpl.saveGovVolunteerServInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存服务领域信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询服务领域信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovVolunteerServInfo(Map info) throws DAOException {
        logger.debug("查询服务领域信息 入参 info : {}",info);

        List<Map> businessGovVolunteerServInfos = sqlSessionTemplate.selectList("govVolunteerServServiceDaoImpl.getGovVolunteerServInfo",info);

        return businessGovVolunteerServInfos;
    }


    /**
     * 修改服务领域信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovVolunteerServInfo(Map info) throws DAOException {
        logger.debug("修改服务领域信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govVolunteerServServiceDaoImpl.updateGovVolunteerServInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改服务领域信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询服务领域数量
     * @param info 服务领域信息
     * @return 服务领域数量
     */
    @Override
    public int queryGovVolunteerServsCount(Map info) {
        logger.debug("查询服务领域数据 入参 info : {}",info);

        List<Map> businessGovVolunteerServInfos = sqlSessionTemplate.selectList("govVolunteerServServiceDaoImpl.queryGovVolunteerServsCount", info);
        if (businessGovVolunteerServInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovVolunteerServInfos.get(0).get("count").toString());
    }


}
