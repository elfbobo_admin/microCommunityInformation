package com.java110.cust.smo.impl;


import com.java110.cust.dao.IGovCompanyServiceDao;
import com.java110.dto.govCompany.GovCompanyDto;
import com.java110.intf.cust.IGovCompanyInnerServiceSMO;
import com.java110.po.govCompany.GovCompanyPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 公司组织内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovCompanyInnerServiceSMOImpl extends BaseServiceSMO implements IGovCompanyInnerServiceSMO {

    @Autowired
    private IGovCompanyServiceDao govCompanyServiceDaoImpl;


    @Override
    public int saveGovCompany(@RequestBody GovCompanyPo govCompanyPo) {
        int saveFlag = 1;
        govCompanyServiceDaoImpl.saveGovCompanyInfo(BeanConvertUtil.beanCovertMap(govCompanyPo));
        return saveFlag;
    }

     @Override
    public int updateGovCompany(@RequestBody  GovCompanyPo govCompanyPo) {
        int saveFlag = 1;
         govCompanyServiceDaoImpl.updateGovCompanyInfo(BeanConvertUtil.beanCovertMap(govCompanyPo));
        return saveFlag;
    }

     @Override
    public int deleteGovCompany(@RequestBody  GovCompanyPo govCompanyPo) {
        int saveFlag = 1;
        govCompanyPo.setStatusCd("1");
        govCompanyServiceDaoImpl.updateGovCompanyInfo(BeanConvertUtil.beanCovertMap(govCompanyPo));
        return saveFlag;
    }

    @Override
    public List<GovCompanyDto> queryGovCompanys(@RequestBody  GovCompanyDto govCompanyDto) {

        //校验是否传了 分页信息

        int page = govCompanyDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govCompanyDto.setPage((page - 1) * govCompanyDto.getRow());
        }

        List<GovCompanyDto> govCompanys = BeanConvertUtil.covertBeanList(govCompanyServiceDaoImpl.getGovCompanyInfo(BeanConvertUtil.beanCovertMap(govCompanyDto)), GovCompanyDto.class);

        return govCompanys;
    }


    @Override
    public int queryGovCompanysCount(@RequestBody GovCompanyDto govCompanyDto) {
        return govCompanyServiceDaoImpl.queryGovCompanysCount(BeanConvertUtil.beanCovertMap(govCompanyDto));    }

    public IGovCompanyServiceDao getGovCompanyServiceDaoImpl() {
        return govCompanyServiceDaoImpl;
    }

    public void setGovCompanyServiceDaoImpl(IGovCompanyServiceDao govCompanyServiceDaoImpl) {
        this.govCompanyServiceDaoImpl = govCompanyServiceDaoImpl;
    }
}
