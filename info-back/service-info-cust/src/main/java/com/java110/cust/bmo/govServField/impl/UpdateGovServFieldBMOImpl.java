package com.java110.cust.bmo.govServField.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.cust.bmo.govServField.IUpdateGovServFieldBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.cust.IGovServFieldInnerServiceSMO;
import com.java110.dto.govServField.GovServFieldDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.java110.po.govServField.GovServFieldPo;
import java.util.List;

@Service("updateGovServFieldBMOImpl")
public class UpdateGovServFieldBMOImpl implements IUpdateGovServFieldBMO {

    @Autowired
    private IGovServFieldInnerServiceSMO govServFieldInnerServiceSMOImpl;

    /**
     *
     *
     * @param govServFieldPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> update(GovServFieldPo govServFieldPo) {

        int flag = govServFieldInnerServiceSMOImpl.updateGovServField(govServFieldPo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
