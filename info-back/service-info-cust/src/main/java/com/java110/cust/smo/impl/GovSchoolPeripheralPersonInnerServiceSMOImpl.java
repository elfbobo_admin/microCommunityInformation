package com.java110.cust.smo.impl;


import com.java110.cust.dao.IGovSchoolPeripheralPersonServiceDao;
import com.java110.intf.cust.IGovSchoolPeripheralPersonInnerServiceSMO;
import com.java110.dto.govSchoolPeripheralPerson.GovSchoolPeripheralPersonDto;
import com.java110.po.govSchoolPeripheralPerson.GovSchoolPeripheralPersonPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 校园周边重点人员内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovSchoolPeripheralPersonInnerServiceSMOImpl extends BaseServiceSMO implements IGovSchoolPeripheralPersonInnerServiceSMO {

    @Autowired
    private IGovSchoolPeripheralPersonServiceDao govSchoolPeripheralPersonServiceDaoImpl;


    @Override
    public int saveGovSchoolPeripheralPerson(@RequestBody  GovSchoolPeripheralPersonPo govSchoolPeripheralPersonPo) {
        int saveFlag = 1;
        govSchoolPeripheralPersonServiceDaoImpl.saveGovSchoolPeripheralPersonInfo(BeanConvertUtil.beanCovertMap(govSchoolPeripheralPersonPo));
        return saveFlag;
    }

     @Override
    public int updateGovSchoolPeripheralPerson(@RequestBody  GovSchoolPeripheralPersonPo govSchoolPeripheralPersonPo) {
        int saveFlag = 1;
         govSchoolPeripheralPersonServiceDaoImpl.updateGovSchoolPeripheralPersonInfo(BeanConvertUtil.beanCovertMap(govSchoolPeripheralPersonPo));
        return saveFlag;
    }

     @Override
    public int deleteGovSchoolPeripheralPerson(@RequestBody  GovSchoolPeripheralPersonPo govSchoolPeripheralPersonPo) {
        int saveFlag = 1;
        govSchoolPeripheralPersonPo.setStatusCd("1");
        govSchoolPeripheralPersonServiceDaoImpl.updateGovSchoolPeripheralPersonInfo(BeanConvertUtil.beanCovertMap(govSchoolPeripheralPersonPo));
        return saveFlag;
    }

    @Override
    public List<GovSchoolPeripheralPersonDto> queryGovSchoolPeripheralPersons(@RequestBody  GovSchoolPeripheralPersonDto govSchoolPeripheralPersonDto) {

        //校验是否传了 分页信息

        int page = govSchoolPeripheralPersonDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govSchoolPeripheralPersonDto.setPage((page - 1) * govSchoolPeripheralPersonDto.getRow());
        }

        List<GovSchoolPeripheralPersonDto> govSchoolPeripheralPersons = BeanConvertUtil.covertBeanList(govSchoolPeripheralPersonServiceDaoImpl.getGovSchoolPeripheralPersonInfo(BeanConvertUtil.beanCovertMap(govSchoolPeripheralPersonDto)), GovSchoolPeripheralPersonDto.class);

        return govSchoolPeripheralPersons;
    }


    @Override
    public int queryGovSchoolPeripheralPersonsCount(@RequestBody GovSchoolPeripheralPersonDto govSchoolPeripheralPersonDto) {
        return govSchoolPeripheralPersonServiceDaoImpl.queryGovSchoolPeripheralPersonsCount(BeanConvertUtil.beanCovertMap(govSchoolPeripheralPersonDto));    }

    public IGovSchoolPeripheralPersonServiceDao getGovSchoolPeripheralPersonServiceDaoImpl() {
        return govSchoolPeripheralPersonServiceDaoImpl;
    }

    public void setGovSchoolPeripheralPersonServiceDaoImpl(IGovSchoolPeripheralPersonServiceDao govSchoolPeripheralPersonServiceDaoImpl) {
        this.govSchoolPeripheralPersonServiceDaoImpl = govSchoolPeripheralPersonServiceDaoImpl;
    }
}
