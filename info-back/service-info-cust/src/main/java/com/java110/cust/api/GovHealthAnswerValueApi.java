package com.java110.cust.api;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govHealthAnswerValue.GovHealthAnswerValueDto;
import com.java110.po.govHealthAnswerValue.GovHealthAnswerValuePo;
import com.java110.cust.bmo.govHealthAnswerValue.IDeleteGovHealthAnswerValueBMO;
import com.java110.cust.bmo.govHealthAnswerValue.IGetGovHealthAnswerValueBMO;
import com.java110.cust.bmo.govHealthAnswerValue.ISaveGovHealthAnswerValueBMO;
import com.java110.cust.bmo.govHealthAnswerValue.IUpdateGovHealthAnswerValueBMO;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govHealthAnswerValue")
public class GovHealthAnswerValueApi {

    @Autowired
    private ISaveGovHealthAnswerValueBMO saveGovHealthAnswerValueBMOImpl;
    @Autowired
    private IUpdateGovHealthAnswerValueBMO updateGovHealthAnswerValueBMOImpl;
    @Autowired
    private IDeleteGovHealthAnswerValueBMO deleteGovHealthAnswerValueBMOImpl;

    @Autowired
    private IGetGovHealthAnswerValueBMO getGovHealthAnswerValueBMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govHealthAnswerValue/saveGovHealthAnswerValue
     * @path /app/govHealthAnswerValue/saveGovHealthAnswerValue
     */
    @RequestMapping(value = "/saveGovHealthAnswerValue", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovHealthAnswerValue(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "healthId", "请求报文中未包含healthId");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "personId", "请求报文中未包含personId");
        Assert.hasKeyAndValue(reqJson, "doctor", "请求报文中未包含doctor");
        Assert.hasKeyAndValue(reqJson, "doctorRemark", "请求报文中未包含doctorRemark");
        Assert.hasKeyAndValue(reqJson, "questionAnswerTitles", "请求报文中未包含回答项");

        JSONArray questionAnswerTitles = reqJson.getJSONArray("questionAnswerTitles");

        if (questionAnswerTitles == null || questionAnswerTitles.size() < 1) {
            throw new IllegalArgumentException("未包含题目及答案");
        }
        JSONObject titleObj = null;
        for (int questionAnswerTitleIndex = 0; questionAnswerTitleIndex < questionAnswerTitles.size(); questionAnswerTitleIndex++) {
            titleObj = questionAnswerTitles.getJSONObject(questionAnswerTitleIndex);
            Assert.hasKeyAndValue(titleObj, "valueContent", titleObj.getString("title") + ",未填写答案");
        }

        return saveGovHealthAnswerValueBMOImpl.save(reqJson);
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govHealthAnswerValue/updateGovHealthAnswerValue
     * @path /app/govHealthAnswerValue/updateGovHealthAnswerValue
     */
    @RequestMapping(value = "/updateGovHealthAnswerValue", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovHealthAnswerValue(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "personTitleId", "请求报文中未包含personTitleId");
        Assert.hasKeyAndValue(reqJson, "healthPersonId", "请求报文中未包含healthPersonId");
        Assert.hasKeyAndValue(reqJson, "healthId", "请求报文中未包含healthId");
        Assert.hasKeyAndValue(reqJson, "titleId", "请求报文中未包含titleId");
        Assert.hasKeyAndValue(reqJson, "valueId", "请求报文中未包含valueId");
        Assert.hasKeyAndValue(reqJson, "valueContent", "请求报文中未包含valueContent");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "personId", "请求报文中未包含personId");
        Assert.hasKeyAndValue(reqJson, "doctor", "请求报文中未包含doctor");


        GovHealthAnswerValuePo govHealthAnswerValuePo = BeanConvertUtil.covertBean(reqJson, GovHealthAnswerValuePo.class);
        return updateGovHealthAnswerValueBMOImpl.update(govHealthAnswerValuePo);
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govHealthAnswerValue/deleteGovHealthAnswerValue
     * @path /app/govHealthAnswerValue/deleteGovHealthAnswerValue
     */
    @RequestMapping(value = "/deleteGovHealthAnswerValue", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovHealthAnswerValue(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caId", "caId不能为空");

        Assert.hasKeyAndValue(reqJson, "personTitleId", "personTitleId不能为空");


        GovHealthAnswerValuePo govHealthAnswerValuePo = BeanConvertUtil.covertBean(reqJson, GovHealthAnswerValuePo.class);
        return deleteGovHealthAnswerValueBMOImpl.delete(govHealthAnswerValuePo);
    }

    /**
     * 微信删除消息模板
     *
     * @param caId 小区ID
     * @return
     * @serviceCode /govHealthAnswerValue/queryGovHealthAnswerValue
     * @path /app/govHealthAnswerValue/queryGovHealthAnswerValue
     */
    @RequestMapping(value = "/queryGovHealthAnswerValue", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovHealthAnswerValue(@RequestParam(value = "caId") String caId,
                                                            @RequestParam(value = "healthPersonId",required = false) String healthPersonId,
                                                            @RequestParam(value = "healthId",required = false) String healthId,
                                                            @RequestParam(value = "personId",required = false) String personId,
                                                            @RequestParam(value = "page") int page,
                                                            @RequestParam(value = "row") int row) {
        GovHealthAnswerValueDto govHealthAnswerValueDto = new GovHealthAnswerValueDto();
        govHealthAnswerValueDto.setPage(page);
        govHealthAnswerValueDto.setRow(row);
        govHealthAnswerValueDto.setCaId(caId);
        govHealthAnswerValueDto.setHealthPersonId(healthPersonId);
        govHealthAnswerValueDto.setHealthId(healthId);
        govHealthAnswerValueDto.setPersonId(personId);
        return getGovHealthAnswerValueBMOImpl.get(govHealthAnswerValueDto);
    }
}
