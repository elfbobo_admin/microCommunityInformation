package com.java110.cust.bmo.govHealthTitleValue;

import com.java110.po.govHealthTitleValue.GovHealthTitleValuePo;
import org.springframework.http.ResponseEntity;
import com.java110.po.store.StorePo;
public interface ISaveGovHealthTitleValueBMO {


    /**
     * 添加体检项值
     * add by wuxw
     * @param govHealthTitleValuePo
     * @return
     */
    ResponseEntity<String> save(GovHealthTitleValuePo govHealthTitleValuePo);


}
