package com.java110.cust.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govServField.GovServFieldDto;
import com.java110.po.govServField.GovServFieldPo;
import com.java110.cust.bmo.govServField.IDeleteGovServFieldBMO;
import com.java110.cust.bmo.govServField.IGetGovServFieldBMO;
import com.java110.cust.bmo.govServField.ISaveGovServFieldBMO;
import com.java110.cust.bmo.govServField.IUpdateGovServFieldBMO;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govServField")
public class GovServFieldApi {

    @Autowired
    private ISaveGovServFieldBMO saveGovServFieldBMOImpl;
    @Autowired
    private IUpdateGovServFieldBMO updateGovServFieldBMOImpl;
    @Autowired
    private IDeleteGovServFieldBMO deleteGovServFieldBMOImpl;

    @Autowired
    private IGetGovServFieldBMO getGovServFieldBMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govServField/saveGovServField
     * @path /app/govServField/saveGovServField
     */
    @RequestMapping(value = "/saveGovServField", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovServField(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "name", "请求报文中未包含name");
        Assert.hasKeyAndValue(reqJson, "isShow", "请求报文中未包含isShow");


        GovServFieldPo govServFieldPo = BeanConvertUtil.covertBean(reqJson, GovServFieldPo.class);
        return saveGovServFieldBMOImpl.save(govServFieldPo);
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govServField/updateGovServField
     * @path /app/govServField/updateGovServField
     */
    @RequestMapping(value = "/updateGovServField", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovServField(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "servId", "请求报文中未包含servId");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "name", "请求报文中未包含name");
        Assert.hasKeyAndValue(reqJson, "isShow", "请求报文中未包含isShow");


        GovServFieldPo govServFieldPo = BeanConvertUtil.covertBean(reqJson, GovServFieldPo.class);
        return updateGovServFieldBMOImpl.update(govServFieldPo);
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govServField/deleteGovServField
     * @path /app/govServField/deleteGovServField
     */
    @RequestMapping(value = "/deleteGovServField", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovServField(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "caId", "caId不能为空");

        Assert.hasKeyAndValue(reqJson, "servId", "servId不能为空");


        GovServFieldPo govServFieldPo = BeanConvertUtil.covertBean(reqJson, GovServFieldPo.class);
        return deleteGovServFieldBMOImpl.delete(govServFieldPo);
    }

    /**
     * 微信删除消息模板
     *
     * @param caId 小区ID
     * @return
     * @serviceCode /govServField/queryGovServField
     * @path /app/govServField/queryGovServField
     */
    @RequestMapping(value = "/queryGovServField", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovServField(@RequestParam(value = "caId") String caId,
                                                    @RequestParam(value = "page") int page,
                                                    @RequestParam(value = "name",required = false) String name,
                                                    @RequestParam(value = "isShow",required = false) String isShow,
                                                    @RequestParam(value = "row") int row) {
        GovServFieldDto govServFieldDto = new GovServFieldDto();
        govServFieldDto.setPage(page);
        govServFieldDto.setRow(row);
        govServFieldDto.setCaId(caId);
        govServFieldDto.setIsShow(isShow);
        govServFieldDto.setName(name);
        return getGovServFieldBMOImpl.get(govServFieldDto);
    }
}
