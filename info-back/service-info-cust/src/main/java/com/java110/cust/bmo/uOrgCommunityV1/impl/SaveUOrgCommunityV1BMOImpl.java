package com.java110.cust.bmo.uOrgCommunityV1.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.cust.bmo.uOrgCommunityV1.ISaveUOrgCommunityV1BMO;
import com.java110.po.uOrgCommunityV1.UOrgCommunityV1Po;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.po.store.StorePo;
import com.java110.intf.cust.IUOrgCommunityV1InnerServiceSMO;
import com.java110.dto.uOrgCommunityV1.UOrgCommunityV1Dto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("saveUOrgCommunityV1BMOImpl")
public class SaveUOrgCommunityV1BMOImpl implements ISaveUOrgCommunityV1BMO {

    @Autowired
    private IUOrgCommunityV1InnerServiceSMO uOrgCommunityV1InnerServiceSMOImpl;

    /**
     * 添加小区信息
     *
     * @param uOrgCommunityV1Po
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> save(UOrgCommunityV1Po uOrgCommunityV1Po,JSONArray communitys) {


        for(int communitysIndex = 0;communitysIndex < communitys.size();communitysIndex++){
            uOrgCommunityV1Po.setOrgCommunityId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_orgCommunityId));
            uOrgCommunityV1Po.setCommunityId(communitys.getJSONObject(communitysIndex).getString("communityId"));
            uOrgCommunityV1Po.setCommunityName(communitys.getJSONObject(communitysIndex).getString("communityName"));
            uOrgCommunityV1Po.setbId("-1");
            int flag = uOrgCommunityV1InnerServiceSMOImpl.saveUOrgCommunityV1(uOrgCommunityV1Po);
            if (flag < 1) {
                throw new IllegalArgumentException("保存关系失败");
            }
        }
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
    }

}
