package com.java110.cust.bmo.govGridObjRel;

import org.springframework.http.ResponseEntity;
import com.java110.po.govGridObjRel.GovGridObjRelPo;
public interface ISaveGovGridObjRelBMO {


    /**
     * 添加网格对象关系
     * add by wuxw
     * @param govGridObjRelPo
     * @return
     */
    ResponseEntity<String> save(GovGridObjRelPo govGridObjRelPo);


}
