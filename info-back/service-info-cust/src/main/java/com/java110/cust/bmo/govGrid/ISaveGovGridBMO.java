package com.java110.cust.bmo.govGrid;

import com.alibaba.fastjson.JSONObject;
import com.java110.po.govGrid.GovGridPo;
import com.java110.po.govPerson.GovPersonPo;
import org.springframework.http.ResponseEntity;
import com.java110.po.store.StorePo;
public interface ISaveGovGridBMO {


    /**
     * 添加网格人员
     * add by wuxw
     * @param govGridPo
     * @return
     */
    ResponseEntity<String> save(GovGridPo govGridPo, String storeId, GovPersonPo govPersonPo, JSONObject reqJson);


}
