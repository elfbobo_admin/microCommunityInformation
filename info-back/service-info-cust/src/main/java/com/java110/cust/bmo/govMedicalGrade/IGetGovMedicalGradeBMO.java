package com.java110.cust.bmo.govMedicalGrade;
import org.springframework.http.ResponseEntity;
import com.java110.dto.govMedicalGrade.GovMedicalGradeDto;
public interface IGetGovMedicalGradeBMO {


    /**
     * 查询医疗分级
     * add by wuxw
     * @param  govMedicalGradeDto
     * @return
     */
    ResponseEntity<String> get(GovMedicalGradeDto govMedicalGradeDto);


}
