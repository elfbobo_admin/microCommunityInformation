package com.java110.cust.bmo.UserV1;
import com.java110.dto.UserV1.UserV1Dto;
import org.springframework.http.ResponseEntity;
import com.java110.dto.store.StoreDto;
public interface IGetUserV1BMO {


    /**
     * 查询用戶管理
     * add by wuxw
     * @param  UserV1Dto
     * @return
     */
    ResponseEntity<String> get(UserV1Dto UserV1Dto);


}
