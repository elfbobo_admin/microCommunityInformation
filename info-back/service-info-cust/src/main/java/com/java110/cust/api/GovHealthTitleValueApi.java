package com.java110.cust.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.cust.bmo.govHealthTitleValue.IDeleteGovHealthTitleValueBMO;
import com.java110.cust.bmo.govHealthTitleValue.IGetGovHealthTitleValueBMO;
import com.java110.cust.bmo.govHealthTitleValue.ISaveGovHealthTitleValueBMO;
import com.java110.cust.bmo.govHealthTitleValue.IUpdateGovHealthTitleValueBMO;
import com.java110.dto.govHealthTitleValue.GovHealthTitleValueDto;
import com.java110.po.govHealthTitleValue.GovHealthTitleValuePo;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govHealthTitleValue")
public class GovHealthTitleValueApi {

    @Autowired
    private ISaveGovHealthTitleValueBMO saveGovHealthTitleValueBMOImpl;
    @Autowired
    private IUpdateGovHealthTitleValueBMO updateGovHealthTitleValueBMOImpl;
    @Autowired
    private IDeleteGovHealthTitleValueBMO deleteGovHealthTitleValueBMOImpl;

    @Autowired
    private IGetGovHealthTitleValueBMO getGovHealthTitleValueBMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govHealthTitleValue/saveGovHealthTitleValue
     * @path /app/govHealthTitleValue/saveGovHealthTitleValue
     */
    @RequestMapping(value = "/saveGovHealthTitleValue", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovHealthTitleValue(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "titleId", "请求报文中未包含titleId");
        Assert.hasKeyAndValue(reqJson, "value", "请求报文中未包含value");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "seq", "请求报文中未包含seq");


        GovHealthTitleValuePo govHealthTitleValuePo = BeanConvertUtil.covertBean(reqJson, GovHealthTitleValuePo.class);
        return saveGovHealthTitleValueBMOImpl.save(govHealthTitleValuePo);
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govHealthTitleValue/updateGovHealthTitleValue
     * @path /app/govHealthTitleValue/updateGovHealthTitleValue
     */
    @RequestMapping(value = "/updateGovHealthTitleValue", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovHealthTitleValue(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "valueId", "请求报文中未包含valueId");
        Assert.hasKeyAndValue(reqJson, "titleId", "请求报文中未包含titleId");
        Assert.hasKeyAndValue(reqJson, "value", "请求报文中未包含value");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "seq", "请求报文中未包含seq");


        GovHealthTitleValuePo govHealthTitleValuePo = BeanConvertUtil.covertBean(reqJson, GovHealthTitleValuePo.class);
        return updateGovHealthTitleValueBMOImpl.update(govHealthTitleValuePo);
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govHealthTitleValue/deleteGovHealthTitleValue
     * @path /app/govHealthTitleValue/deleteGovHealthTitleValue
     */
    @RequestMapping(value = "/deleteGovHealthTitleValue", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovHealthTitleValue(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "caId", "caId不能为空");

        Assert.hasKeyAndValue(reqJson, "valueId", "valueId不能为空");


        GovHealthTitleValuePo govHealthTitleValuePo = BeanConvertUtil.covertBean(reqJson, GovHealthTitleValuePo.class);
        return deleteGovHealthTitleValueBMOImpl.delete(govHealthTitleValuePo);
    }

    /**
     * 微信删除消息模板
     *
     * @param caId 小区ID
     * @return
     * @serviceCode /govHealthTitleValue/queryGovHealthTitleValue
     * @path /app/govHealthTitleValue/queryGovHealthTitleValue
     */
    @RequestMapping(value = "/queryGovHealthTitleValue", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovHealthTitleValue(@RequestParam(value = "caId") String caId,
                                                           @RequestParam(value = "titleId",required = false) String titleId,
                                                           @RequestParam(value = "page") int page,
                                                           @RequestParam(value = "row") int row) {
        GovHealthTitleValueDto govHealthTitleValueDto = new GovHealthTitleValueDto();
        govHealthTitleValueDto.setPage(page);
        govHealthTitleValueDto.setRow(row);
        govHealthTitleValueDto.setCaId(caId);
        govHealthTitleValueDto.setTitleId(titleId);
        return getGovHealthTitleValueBMOImpl.get(govHealthTitleValueDto);
    }
}
