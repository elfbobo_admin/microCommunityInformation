package com.java110.cust.bmo.govPersonDieRecord.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.cust.bmo.govPersonDieRecord.IGetGovPersonDieRecordBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.cust.IGovPersonDieRecordInnerServiceSMO;
import com.java110.dto.govPersonDieRecord.GovPersonDieRecordDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import org.springframework.http.HttpStatus;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service("getGovPersonDieRecordBMOImpl")
public class GetGovPersonDieRecordBMOImpl implements IGetGovPersonDieRecordBMO {

    @Autowired
    private IGovPersonDieRecordInnerServiceSMO govPersonDieRecordInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govPersonDieRecordDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovPersonDieRecordDto govPersonDieRecordDto) {


        int count = govPersonDieRecordInnerServiceSMOImpl.queryGovPersonDieRecordsCount(govPersonDieRecordDto);

        List<GovPersonDieRecordDto> govPersonDieRecordDtos = null;
        if (count > 0) {
            govPersonDieRecordDtos = govPersonDieRecordInnerServiceSMOImpl.queryGovPersonDieRecords(govPersonDieRecordDto);
        } else {
            govPersonDieRecordDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govPersonDieRecordDto.getRow()), count, govPersonDieRecordDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
