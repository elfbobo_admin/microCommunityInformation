package com.java110.cust.bmo.govVolunteer;
import com.alibaba.fastjson.JSONObject;
import org.springframework.http.ResponseEntity;
import com.java110.po.govVolunteer.GovVolunteerPo;

public interface IUpdateGovVolunteerBMO {


    /**
     * 修改志愿者管理
     * add by wuxw
     * @param
     * @return
     */
    ResponseEntity<String> update(JSONObject reqJson);
    /**
     * 修改志愿者管理
     * add by wuxw
     * @param
     * @return
     */
    ResponseEntity<String> review(JSONObject reqJson);


}
