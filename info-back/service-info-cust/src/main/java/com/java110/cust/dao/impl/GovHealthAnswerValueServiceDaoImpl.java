package com.java110.cust.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.cust.dao.IGovHealthAnswerValueServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 体检项目答案服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govHealthAnswerValueServiceDaoImpl")
//@Transactional
public class GovHealthAnswerValueServiceDaoImpl extends BaseServiceDao implements IGovHealthAnswerValueServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovHealthAnswerValueServiceDaoImpl.class);





    /**
     * 保存体检项目答案信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovHealthAnswerValueInfo(Map info) throws DAOException {
        logger.debug("保存体检项目答案信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govHealthAnswerValueServiceDaoImpl.saveGovHealthAnswerValueInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存体检项目答案信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询体检项目答案信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovHealthAnswerValueInfo(Map info) throws DAOException {
        logger.debug("查询体检项目答案信息 入参 info : {}",info);

        List<Map> businessGovHealthAnswerValueInfos = sqlSessionTemplate.selectList("govHealthAnswerValueServiceDaoImpl.getGovHealthAnswerValueInfo",info);

        return businessGovHealthAnswerValueInfos;
    }


    /**
     * 修改体检项目答案信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovHealthAnswerValueInfo(Map info) throws DAOException {
        logger.debug("修改体检项目答案信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govHealthAnswerValueServiceDaoImpl.updateGovHealthAnswerValueInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改体检项目答案信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询体检项目答案数量
     * @param info 体检项目答案信息
     * @return 体检项目答案数量
     */
    @Override
    public int queryGovHealthAnswerValuesCount(Map info) {
        logger.debug("查询体检项目答案数据 入参 info : {}",info);

        List<Map> businessGovHealthAnswerValueInfos = sqlSessionTemplate.selectList("govHealthAnswerValueServiceDaoImpl.queryGovHealthAnswerValuesCount", info);
        if (businessGovHealthAnswerValueInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovHealthAnswerValueInfos.get(0).get("count").toString());
    }


}
