package com.java110.cust.smo.impl;


import com.java110.cust.dao.IGovDoctorHealthyServiceDao;
import com.java110.intf.cust.IGovDoctorHealthyInnerServiceSMO;
import com.java110.dto.govDoctorHealthy.GovDoctorHealthyDto;
import com.java110.po.govDoctorHealthy.GovDoctorHealthyPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 档案医生内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovDoctorHealthyInnerServiceSMOImpl extends BaseServiceSMO implements IGovDoctorHealthyInnerServiceSMO {

    @Autowired
    private IGovDoctorHealthyServiceDao govDoctorHealthyServiceDaoImpl;


    @Override
    public int saveGovDoctorHealthy(@RequestBody  GovDoctorHealthyPo govDoctorHealthyPo) {
        int saveFlag = 1;
        govDoctorHealthyServiceDaoImpl.saveGovDoctorHealthyInfo(BeanConvertUtil.beanCovertMap(govDoctorHealthyPo));
        return saveFlag;
    }

     @Override
    public int updateGovDoctorHealthy(@RequestBody  GovDoctorHealthyPo govDoctorHealthyPo) {
        int saveFlag = 1;
         govDoctorHealthyServiceDaoImpl.updateGovDoctorHealthyInfo(BeanConvertUtil.beanCovertMap(govDoctorHealthyPo));
        return saveFlag;
    }

     @Override
    public int deleteGovDoctorHealthy(@RequestBody  GovDoctorHealthyPo govDoctorHealthyPo) {
        int saveFlag = 1;
        govDoctorHealthyPo.setStatusCd("1");
        govDoctorHealthyServiceDaoImpl.updateGovDoctorHealthyInfo(BeanConvertUtil.beanCovertMap(govDoctorHealthyPo));
        return saveFlag;
    }

    @Override
    public List<GovDoctorHealthyDto> queryGovDoctorHealthys(@RequestBody  GovDoctorHealthyDto govDoctorHealthyDto) {

        //校验是否传了 分页信息

        int page = govDoctorHealthyDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govDoctorHealthyDto.setPage((page - 1) * govDoctorHealthyDto.getRow());
        }

        List<GovDoctorHealthyDto> govDoctorHealthys = BeanConvertUtil.covertBeanList(govDoctorHealthyServiceDaoImpl.getGovDoctorHealthyInfo(BeanConvertUtil.beanCovertMap(govDoctorHealthyDto)), GovDoctorHealthyDto.class);

        return govDoctorHealthys;
    }


    @Override
    public int queryGovDoctorHealthysCount(@RequestBody GovDoctorHealthyDto govDoctorHealthyDto) {
        return govDoctorHealthyServiceDaoImpl.queryGovDoctorHealthysCount(BeanConvertUtil.beanCovertMap(govDoctorHealthyDto));    }

    public IGovDoctorHealthyServiceDao getGovDoctorHealthyServiceDaoImpl() {
        return govDoctorHealthyServiceDaoImpl;
    }

    public void setGovDoctorHealthyServiceDaoImpl(IGovDoctorHealthyServiceDao govDoctorHealthyServiceDaoImpl) {
        this.govDoctorHealthyServiceDaoImpl = govDoctorHealthyServiceDaoImpl;
    }
}
