package com.java110.cust.bmo.govEventsType;

import org.springframework.http.ResponseEntity;
import com.java110.po.govEventsType.GovEventsTypePo;
public interface ISaveGovEventsTypeBMO {


    /**
     * 添加事件类型
     * add by wuxw
     * @param govEventsTypePo
     * @return
     */
    ResponseEntity<String> save(GovEventsTypePo govEventsTypePo);


}
