package com.java110.cust.bmo.uOrgCommunityV1;

import com.alibaba.fastjson.JSONArray;
import com.java110.po.uOrgCommunityV1.UOrgCommunityV1Po;
import org.springframework.http.ResponseEntity;
import com.java110.po.store.StorePo;
public interface ISaveUOrgCommunityV1BMO {


    /**
     * 添加组织区域关系
     * add by wuxw
     * @param uOrgCommunityV1Po
     * @return
     */
    ResponseEntity<String> save(UOrgCommunityV1Po uOrgCommunityV1Po,JSONArray communitys);


}
