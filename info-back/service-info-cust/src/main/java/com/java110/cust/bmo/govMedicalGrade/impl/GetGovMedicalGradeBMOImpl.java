package com.java110.cust.bmo.govMedicalGrade.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.cust.bmo.govMedicalGrade.IGetGovMedicalGradeBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.cust.IGovMedicalGradeInnerServiceSMO;
import com.java110.dto.govMedicalGrade.GovMedicalGradeDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import org.springframework.http.HttpStatus;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service("getGovMedicalGradeBMOImpl")
public class GetGovMedicalGradeBMOImpl implements IGetGovMedicalGradeBMO {

    @Autowired
    private IGovMedicalGradeInnerServiceSMO govMedicalGradeInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govMedicalGradeDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovMedicalGradeDto govMedicalGradeDto) {


        int count = govMedicalGradeInnerServiceSMOImpl.queryGovMedicalGradesCount(govMedicalGradeDto);

        List<GovMedicalGradeDto> govMedicalGradeDtos = null;
        if (count > 0) {
            govMedicalGradeDtos = govMedicalGradeInnerServiceSMOImpl.queryGovMedicalGrades(govMedicalGradeDto);
        } else {
            govMedicalGradeDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govMedicalGradeDto.getRow()), count, govMedicalGradeDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
