package com.java110.cust.smo.impl;


import com.java110.cust.dao.IUOrgCommunityV1ServiceDao;
import com.java110.intf.cust.IUOrgCommunityV1InnerServiceSMO;
import com.java110.dto.uOrgCommunityV1.UOrgCommunityV1Dto;
import com.java110.po.uOrgCommunityV1.UOrgCommunityV1Po;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 组织区域关系内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class UOrgCommunityV1InnerServiceSMOImpl extends BaseServiceSMO implements IUOrgCommunityV1InnerServiceSMO {

    @Autowired
    private IUOrgCommunityV1ServiceDao uOrgCommunityV1ServiceDaoImpl;


    @Override
    public int saveUOrgCommunityV1(@RequestBody  UOrgCommunityV1Po uOrgCommunityV1Po) {
        int saveFlag = 1;
        uOrgCommunityV1ServiceDaoImpl.saveUOrgCommunityV1Info(BeanConvertUtil.beanCovertMap(uOrgCommunityV1Po));
        return saveFlag;
    }

     @Override
    public int updateUOrgCommunityV1(@RequestBody  UOrgCommunityV1Po uOrgCommunityV1Po) {
        int saveFlag = 1;
         uOrgCommunityV1ServiceDaoImpl.updateUOrgCommunityV1Info(BeanConvertUtil.beanCovertMap(uOrgCommunityV1Po));
        return saveFlag;
    }

     @Override
    public int deleteUOrgCommunityV1(@RequestBody  UOrgCommunityV1Po uOrgCommunityV1Po) {
        int saveFlag = 1;
        uOrgCommunityV1Po.setStatusCd("1");
        uOrgCommunityV1ServiceDaoImpl.updateUOrgCommunityV1Info(BeanConvertUtil.beanCovertMap(uOrgCommunityV1Po));
        return saveFlag;
    }

    @Override
    public List<UOrgCommunityV1Dto> queryUOrgCommunityV1s(@RequestBody  UOrgCommunityV1Dto uOrgCommunityV1Dto) {

        //校验是否传了 分页信息

        int page = uOrgCommunityV1Dto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            uOrgCommunityV1Dto.setPage((page - 1) * uOrgCommunityV1Dto.getRow());
        }

        List<UOrgCommunityV1Dto> uOrgCommunityV1s = BeanConvertUtil.covertBeanList(uOrgCommunityV1ServiceDaoImpl.getUOrgCommunityV1Info(BeanConvertUtil.beanCovertMap(uOrgCommunityV1Dto)), UOrgCommunityV1Dto.class);

        return uOrgCommunityV1s;
    }


    @Override
    public int queryUOrgCommunityV1sCount(@RequestBody UOrgCommunityV1Dto uOrgCommunityV1Dto) {
        return uOrgCommunityV1ServiceDaoImpl.queryUOrgCommunityV1sCount(BeanConvertUtil.beanCovertMap(uOrgCommunityV1Dto));    }

    public IUOrgCommunityV1ServiceDao getUOrgCommunityV1ServiceDaoImpl() {
        return uOrgCommunityV1ServiceDaoImpl;
    }

    public void setUOrgCommunityV1ServiceDaoImpl(IUOrgCommunityV1ServiceDao uOrgCommunityV1ServiceDaoImpl) {
        this.uOrgCommunityV1ServiceDaoImpl = uOrgCommunityV1ServiceDaoImpl;
    }
}
