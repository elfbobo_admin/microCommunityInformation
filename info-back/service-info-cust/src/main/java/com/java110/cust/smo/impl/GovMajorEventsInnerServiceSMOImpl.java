package com.java110.cust.smo.impl;


import com.java110.cust.dao.IGovMajorEventsServiceDao;
import com.java110.intf.cust.IGovMajorEventsInnerServiceSMO;
import com.java110.dto.govMajorEvents.GovMajorEventsDto;
import com.java110.po.govMajorEvents.GovMajorEventsPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 重特大事件内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovMajorEventsInnerServiceSMOImpl extends BaseServiceSMO implements IGovMajorEventsInnerServiceSMO {

    @Autowired
    private IGovMajorEventsServiceDao govMajorEventsServiceDaoImpl;


    @Override
    public int saveGovMajorEvents(@RequestBody  GovMajorEventsPo govMajorEventsPo) {
        int saveFlag = 1;
        govMajorEventsServiceDaoImpl.saveGovMajorEventsInfo(BeanConvertUtil.beanCovertMap(govMajorEventsPo));
        return saveFlag;
    }

     @Override
    public int updateGovMajorEvents(@RequestBody  GovMajorEventsPo govMajorEventsPo) {
        int saveFlag = 1;
         govMajorEventsServiceDaoImpl.updateGovMajorEventsInfo(BeanConvertUtil.beanCovertMap(govMajorEventsPo));
        return saveFlag;
    }

     @Override
    public int deleteGovMajorEvents(@RequestBody  GovMajorEventsPo govMajorEventsPo) {
        int saveFlag = 1;
        govMajorEventsPo.setStatusCd("1");
        govMajorEventsServiceDaoImpl.updateGovMajorEventsInfo(BeanConvertUtil.beanCovertMap(govMajorEventsPo));
        return saveFlag;
    }

    @Override
    public List<GovMajorEventsDto> queryGovMajorEventss(@RequestBody  GovMajorEventsDto govMajorEventsDto) {

        //校验是否传了 分页信息

        int page = govMajorEventsDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govMajorEventsDto.setPage((page - 1) * govMajorEventsDto.getRow());
        }

        List<GovMajorEventsDto> govMajorEventss = BeanConvertUtil.covertBeanList(govMajorEventsServiceDaoImpl.getGovMajorEventsInfo(BeanConvertUtil.beanCovertMap(govMajorEventsDto)), GovMajorEventsDto.class);

        return govMajorEventss;
    }


    @Override
    public int queryGovMajorEventssCount(@RequestBody GovMajorEventsDto govMajorEventsDto) {
        return govMajorEventsServiceDaoImpl.queryGovMajorEventssCount(BeanConvertUtil.beanCovertMap(govMajorEventsDto));    }

    public IGovMajorEventsServiceDao getGovMajorEventsServiceDaoImpl() {
        return govMajorEventsServiceDaoImpl;
    }

    public void setGovMajorEventsServiceDaoImpl(IGovMajorEventsServiceDao govMajorEventsServiceDaoImpl) {
        this.govMajorEventsServiceDaoImpl = govMajorEventsServiceDaoImpl;
    }
}
