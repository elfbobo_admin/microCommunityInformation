package com.java110.cust.bmo.govDoctorHealthy.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.cust.bmo.govDoctorHealthy.ISaveGovDoctorHealthyBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.dto.govLabel.GovLabelDto;
import com.java110.dto.govPerson.GovPersonDto;
import com.java110.dto.govPersonLabelRel.GovPersonLabelRelDto;
import com.java110.intf.cust.IGovMedicalDoctorRelInnerServiceSMO;
import com.java110.intf.cust.IGovPersonInnerServiceSMO;
import com.java110.intf.gov.IGovLabelInnerServiceSMO;
import com.java110.intf.gov.IGovPersonLabelRelInnerServiceSMO;
import com.java110.po.govLabel.GovLabelPo;
import com.java110.po.govMedicalDoctorRel.GovMedicalDoctorRelPo;
import com.java110.po.govPerson.GovPersonPo;
import com.java110.po.govPersonLabelRel.GovPersonLabelRelPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.po.govDoctorHealthy.GovDoctorHealthyPo;
import com.java110.intf.cust.IGovDoctorHealthyInnerServiceSMO;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("saveGovDoctorHealthyBMOImpl")
public class SaveGovDoctorHealthyBMOImpl implements ISaveGovDoctorHealthyBMO {

    @Autowired
    private IGovDoctorHealthyInnerServiceSMO govDoctorHealthyInnerServiceSMOImpl;
    @Autowired
    private IGovPersonInnerServiceSMO govPersonInnerServiceSMOImpl;
    @Autowired
    private IGovLabelInnerServiceSMO govLabelInnerServiceSMOImpl;
    @Autowired
    private IGovPersonLabelRelInnerServiceSMO govPersonLabelRelInnerServiceSMOImpl;
    @Autowired
    private IGovMedicalDoctorRelInnerServiceSMO govMedicalDoctorRelInnerServiceSMOImpl;
    /**
     * 添加小区信息
     *
     * @param govDoctorHealthyPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> save(GovDoctorHealthyPo govDoctorHealthyPo, GovPersonPo govPersonPo, GovMedicalDoctorRelPo govMedicalDoctorRelPo) {

        govDoctorHealthyPo.setGovDoctorId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_PUBLIC_indexId));
        GovPersonPo govPersonPo1 = getGovPersonPo(govPersonPo);
        govDoctorHealthyPo.setGovPersonId( govPersonPo1.getGovPersonId() );
        int flag = govDoctorHealthyInnerServiceSMOImpl.saveGovDoctorHealthy(govDoctorHealthyPo);
        String lableName = "";
        if (flag > 0) {
            if(GovLabelDto.LABLE_DOCTOR_CD.equals( govDoctorHealthyPo.getLabelCd() )){
                lableName = "档案医生";
            }else if(GovLabelDto.LABLE_HEALTHY_CD.equals( govDoctorHealthyPo.getLabelCd() )){
                lableName = "健康管理员";
            }else if(GovLabelDto.LABLE_DOCTOR_OL_CD.equals( govDoctorHealthyPo.getLabelCd() )){
                lableName = "医生";
            }
            //处理标签信息
            isGovLabelNull(govDoctorHealthyPo.getLabelCd(),"P",govDoctorHealthyPo.getCaId(),lableName);
            //处理人员与标签关系
            isGovLabelRelNull(govDoctorHealthyPo.getGovPersonId(),govDoctorHealthyPo.getLabelCd(),govDoctorHealthyPo.getCaId());

            //人员和团队关系
            govMedicalDoctorRelPo.setRelId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_PUBLIC_indexId));
            govMedicalDoctorRelPo.setGovDoctorId( govDoctorHealthyPo.getGovDoctorId() );
            flag = govMedicalDoctorRelInnerServiceSMOImpl.saveGovMedicalDoctorRel(govMedicalDoctorRelPo);
            if (flag < 1) {
                return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
            }
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

    private GovPersonPo getGovPersonPo(GovPersonPo govPersonPo) {
        govPersonPo.setGovPersonId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_govGridId));
        govPersonPo.setPersonType("2002");
        govPersonPo.setRamark("系統自动生成");
        govPersonPo.setReligiousBelief("无");
        govPersonPo.setNativePlace("中国");
        govPersonPo.setPrePersonName(govPersonPo.getPersonName());
        govPersonPo.setPoliticalOutlook("5012");

        GovPersonDto govPersonDto = new GovPersonDto();
        govPersonDto.setPersonTel(govPersonPo.getPersonTel());
        govPersonDto.setPersonName(govPersonPo.getPersonName());
        govPersonDto.setCaId(govPersonPo.getCaId());
        List<GovPersonDto> govPersonDtos = govPersonInnerServiceSMOImpl.queryGovPersons(govPersonDto);
        if (govPersonDtos == null || govPersonDtos.size() < 1) {
            int flag = govPersonInnerServiceSMOImpl.saveGovPerson(govPersonPo);
            if (flag < 1) {
                throw new IllegalArgumentException("保存固定人口失败");
            }
        } else {
            govPersonPo.setGovPersonId(govPersonDtos.get(0).getGovPersonId());
            int flag = govPersonInnerServiceSMOImpl.updateGovPerson(govPersonPo);
            if (flag < 1) {
                throw new IllegalArgumentException("修改固定人口失败");
            }
        }
        return govPersonPo;
    }
    protected void isGovLabelNull(String lableCd,String labelType,String cdId,String lableName){
        //处理标签信息
        GovLabelDto govLabelDto = new GovLabelDto();
        govLabelDto.setLabelCd( lableCd);
        govLabelDto.setLabelType( labelType );
        govLabelDto.setCaId( cdId );
        List<GovLabelDto> govLabelDtos = govLabelInnerServiceSMOImpl.queryGovLabels( govLabelDto );
        if (govLabelDtos == null || govLabelDtos.size() < 1) {
            GovLabelPo govLabelPo = new GovLabelPo();
            govLabelPo.setGovLabelId( GenerateCodeFactory.getGeneratorId( GenerateCodeFactory.CODE_PREFIX_govHomicideId ) );
            govLabelPo.setLabelCd( lableCd );
            govLabelPo.setLabelType( labelType );
            govLabelPo.setCaId( cdId );
            govLabelPo.setLabelName( lableName );
            int flag = govLabelInnerServiceSMOImpl.saveGovLabel( govLabelPo );
            if (flag < 1) {
                throw new IllegalArgumentException( "保存"+lableName+"信息失败" );
            }
        }
    }

    protected void isGovLabelRelNull(String govPersonId,String lableCd,String cdId){
        //处理人员与标签关系
        GovPersonLabelRelDto govPersonLabelRelDto = new GovPersonLabelRelDto();
        govPersonLabelRelDto.setGovPersonId( govPersonId );
        govPersonLabelRelDto.setLabelCd( lableCd );
        govPersonLabelRelDto.setCaId( cdId );
        List<GovPersonLabelRelDto> govPersonLabelRelDtos = govPersonLabelRelInnerServiceSMOImpl.queryGovPersonLabelRels( govPersonLabelRelDto );
        if (govPersonLabelRelDtos == null || govPersonLabelRelDtos.size() < 1) {
            GovPersonLabelRelPo govPersonLabelRelPo = new GovPersonLabelRelPo();
            govPersonLabelRelPo.setLabelRelId( GenerateCodeFactory.getGeneratorId( GenerateCodeFactory.CODE_PREFIX_govHomicideId ) );
            govPersonLabelRelPo.setGovPersonId( govPersonId );
            govPersonLabelRelPo.setLabelCd( lableCd );
            govPersonLabelRelPo.setCaId( cdId );
            int flag = govPersonLabelRelInnerServiceSMOImpl.saveGovPersonLabelRel( govPersonLabelRelPo );
            if (flag < 1) {
                throw new IllegalArgumentException( "打标签嫌疑人失败" );
            }
        }
    }
}
