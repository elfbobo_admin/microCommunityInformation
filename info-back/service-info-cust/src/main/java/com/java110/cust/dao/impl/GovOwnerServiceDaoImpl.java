package com.java110.cust.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.cust.dao.IGovOwnerServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 户籍管理服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govOwnerServiceDaoImpl")
//@Transactional
public class GovOwnerServiceDaoImpl extends BaseServiceDao implements IGovOwnerServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovOwnerServiceDaoImpl.class);





    /**
     * 保存户籍管理信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovOwnerInfo(Map info) throws DAOException {
        logger.debug("保存户籍管理信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govOwnerServiceDaoImpl.saveGovOwnerInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存户籍管理信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询户籍管理信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovOwnerInfo(Map info) throws DAOException {
        logger.debug("查询户籍管理信息 入参 info : {}",info);

        List<Map> businessGovOwnerInfos = sqlSessionTemplate.selectList("govOwnerServiceDaoImpl.getGovOwnerInfo",info);

        return businessGovOwnerInfos;
    }
    /**
     * 查询户籍管理信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovOwnerPersonRel(Map info) throws DAOException {
        logger.debug("查询户籍管理信息 入参 info : {}",info);

        List<Map> businessGovOwnerInfos = sqlSessionTemplate.selectList("govOwnerServiceDaoImpl.getGovOwnerPersonRel",info);

        return businessGovOwnerInfos;
    }


    /**
     * 修改户籍管理信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovOwnerInfo(Map info) throws DAOException {
        logger.debug("修改户籍管理信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govOwnerServiceDaoImpl.updateGovOwnerInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改户籍管理信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询户籍管理数量
     * @param info 户籍管理信息
     * @return 户籍管理数量
     */
    @Override
    public int queryGovOwnersCount(Map info) {
        logger.debug("查询户籍管理数据 入参 info : {}",info);

        List<Map> businessGovOwnerInfos = sqlSessionTemplate.selectList("govOwnerServiceDaoImpl.queryGovOwnersCount", info);
        if (businessGovOwnerInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovOwnerInfos.get(0).get("count").toString());
    }
     /**
     * 查询户籍管理数量
     * @param info 户籍管理信息
     * @return 户籍管理数量
     */
    @Override
    public int getGovOwnerPersonRelCount(Map info) {
        logger.debug("查询户籍管理数据 入参 info : {}",info);

        List<Map> businessGovOwnerInfos = sqlSessionTemplate.selectList("govOwnerServiceDaoImpl.getGovOwnerPersonRelCount", info);
        if (businessGovOwnerInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovOwnerInfos.get(0).get("count").toString());
    }


}
