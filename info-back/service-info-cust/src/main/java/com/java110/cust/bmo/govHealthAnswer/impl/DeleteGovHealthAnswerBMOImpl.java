package com.java110.cust.bmo.govHealthAnswer.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.cust.bmo.govHealthAnswer.IDeleteGovHealthAnswerBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.vo.ResultVo;
import com.java110.po.govHealthAnswer.GovHealthAnswerPo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.cust.IGovHealthAnswerInnerServiceSMO;
import com.java110.dto.govHealthAnswer.GovHealthAnswerDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("deleteGovHealthAnswerBMOImpl")
public class DeleteGovHealthAnswerBMOImpl implements IDeleteGovHealthAnswerBMO {

    @Autowired
    private IGovHealthAnswerInnerServiceSMO govHealthAnswerInnerServiceSMOImpl;

    /**
     * @param govHealthAnswerPo 数据
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> delete(GovHealthAnswerPo govHealthAnswerPo) {

        int flag = govHealthAnswerInnerServiceSMOImpl.deleteGovHealthAnswer(govHealthAnswerPo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
