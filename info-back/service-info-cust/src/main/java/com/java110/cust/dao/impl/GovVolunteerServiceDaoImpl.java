package com.java110.cust.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.cust.dao.IGovVolunteerServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 志愿者管理服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govVolunteerServiceDaoImpl")
//@Transactional
public class GovVolunteerServiceDaoImpl extends BaseServiceDao implements IGovVolunteerServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovVolunteerServiceDaoImpl.class);





    /**
     * 保存志愿者管理信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovVolunteerInfo(Map info) throws DAOException {
        logger.debug("保存志愿者管理信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govVolunteerServiceDaoImpl.saveGovVolunteerInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存志愿者管理信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询志愿者管理信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovVolunteerInfo(Map info) throws DAOException {
        logger.debug("查询志愿者管理信息 入参 info : {}",info);

        List<Map> businessGovVolunteerInfos = sqlSessionTemplate.selectList("govVolunteerServiceDaoImpl.getGovVolunteerInfo",info);

        return businessGovVolunteerInfos;
    }
    /**
     * 查询志愿者管理信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovStateVolunteerCount(Map info) throws DAOException {
        logger.debug("查询志愿者管理信息 入参 info : {}",info);

        List<Map> businessGovVolunteerInfos = sqlSessionTemplate.selectList("govVolunteerServiceDaoImpl.getGovStateVolunteerCount",info);

        return businessGovVolunteerInfos;
    }


    /**
     * 修改志愿者管理信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovVolunteerInfo(Map info) throws DAOException {
        logger.debug("修改志愿者管理信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govVolunteerServiceDaoImpl.updateGovVolunteerInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改志愿者管理信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询志愿者管理数量
     * @param info 志愿者管理信息
     * @return 志愿者管理数量
     */
    @Override
    public int queryGovVolunteersCount(Map info) {
        logger.debug("查询志愿者管理数据 入参 info : {}",info);

        List<Map> businessGovVolunteerInfos = sqlSessionTemplate.selectList("govVolunteerServiceDaoImpl.queryGovVolunteersCount", info);
        if (businessGovVolunteerInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovVolunteerInfos.get(0).get("count").toString());
    }


}
