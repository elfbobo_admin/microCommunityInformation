package com.java110.cust.bmo.govFollowupSurvey;

import org.springframework.http.ResponseEntity;
import com.java110.po.govFollowupSurvey.GovFollowupSurveyPo;
public interface ISaveGovFollowupSurveyBMO {


    /**
     * 添加随访登记
     * add by wuxw
     * @param govFollowupSurveyPo
     * @return
     */
    ResponseEntity<String> save(GovFollowupSurveyPo govFollowupSurveyPo);


}
