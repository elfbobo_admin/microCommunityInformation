package com.java110.cust.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.cust.dao.IGovSchoolPeripheralPersonServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 校园周边重点人员服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govSchoolPeripheralPersonServiceDaoImpl")
//@Transactional
public class GovSchoolPeripheralPersonServiceDaoImpl extends BaseServiceDao implements IGovSchoolPeripheralPersonServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovSchoolPeripheralPersonServiceDaoImpl.class);





    /**
     * 保存校园周边重点人员信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovSchoolPeripheralPersonInfo(Map info) throws DAOException {
        logger.debug("保存校园周边重点人员信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govSchoolPeripheralPersonServiceDaoImpl.saveGovSchoolPeripheralPersonInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存校园周边重点人员信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询校园周边重点人员信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovSchoolPeripheralPersonInfo(Map info) throws DAOException {
        logger.debug("查询校园周边重点人员信息 入参 info : {}",info);

        List<Map> businessGovSchoolPeripheralPersonInfos = sqlSessionTemplate.selectList("govSchoolPeripheralPersonServiceDaoImpl.getGovSchoolPeripheralPersonInfo",info);

        return businessGovSchoolPeripheralPersonInfos;
    }


    /**
     * 修改校园周边重点人员信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovSchoolPeripheralPersonInfo(Map info) throws DAOException {
        logger.debug("修改校园周边重点人员信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govSchoolPeripheralPersonServiceDaoImpl.updateGovSchoolPeripheralPersonInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改校园周边重点人员信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询校园周边重点人员数量
     * @param info 校园周边重点人员信息
     * @return 校园周边重点人员数量
     */
    @Override
    public int queryGovSchoolPeripheralPersonsCount(Map info) {
        logger.debug("查询校园周边重点人员数据 入参 info : {}",info);

        List<Map> businessGovSchoolPeripheralPersonInfos = sqlSessionTemplate.selectList("govSchoolPeripheralPersonServiceDaoImpl.queryGovSchoolPeripheralPersonsCount", info);
        if (businessGovSchoolPeripheralPersonInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovSchoolPeripheralPersonInfos.get(0).get("count").toString());
    }


}
