package com.java110.cust.bmo.govOldPerson.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.client.RestTemplate;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.core.factory.RestTemplateFactory;
import com.java110.cust.bmo.govOldPerson.IDeleteGovOldPersonBMO;
import com.java110.intf.cust.IGovPersonInnerServiceSMO;
import com.java110.po.govOldPerson.GovOldPersonPo;
import com.java110.po.govPerson.GovPersonPo;
import com.java110.utils.constant.MallConstant;
import com.java110.vo.ResultVo;
import com.java110.po.store.StorePo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.cust.IGovOldPersonInnerServiceSMO;
import com.java110.dto.govOldPerson.GovOldPersonDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("deleteGovOldPersonBMOImpl")
public class DeleteGovOldPersonBMOImpl implements IDeleteGovOldPersonBMO {

    @Autowired
    private IGovOldPersonInnerServiceSMO govOldPersonInnerServiceSMOImpl;
    @Autowired
    private IGovPersonInnerServiceSMO govPersonInnerServiceSMOImpl;
    @Autowired
    private RestTemplate outRestTemplate;
    /**
     * @param govOldPersonPo 数据
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> delete(GovOldPersonPo govOldPersonPo, GovPersonPo govPersonPo ) {

        int flag = govOldPersonInnerServiceSMOImpl.deleteGovOldPerson(govOldPersonPo);
        if (flag < 1) {
            throw new IllegalArgumentException("删除网格人员失败");
        }
        flag = govPersonInnerServiceSMOImpl.deleteGovPerson(govPersonPo);
        if (flag < 1) {
            throw new IllegalArgumentException("删除对应人口失败");
        }
        JSONObject reqJson = new JSONObject();
        reqJson.put("govCommunityId",govOldPersonPo.getGovCommunityId());
        reqJson.put("oldId",govOldPersonPo.getOldId());
        ResponseEntity<String> responseEntity = RestTemplateFactory.restOutMallTemplate(reqJson, outRestTemplate, MallConstant.DELETE_OLD_PERSON_MALL, HttpMethod.POST);
        if (responseEntity != null) {
            if (responseEntity.getStatusCode() != HttpStatus.OK) {
                throw new IllegalArgumentException(responseEntity.getBody());
            }
        }
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
    }

}
