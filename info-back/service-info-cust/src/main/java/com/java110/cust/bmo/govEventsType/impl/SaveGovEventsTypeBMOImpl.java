package com.java110.cust.bmo.govEventsType.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.cust.bmo.govEventsType.ISaveGovEventsTypeBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.po.govEventsType.GovEventsTypePo;
import com.java110.intf.cust.IGovEventsTypeInnerServiceSMO;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("saveGovEventsTypeBMOImpl")
public class SaveGovEventsTypeBMOImpl implements ISaveGovEventsTypeBMO {

    @Autowired
    private IGovEventsTypeInnerServiceSMO govEventsTypeInnerServiceSMOImpl;

    /**
     * 添加小区信息
     *
     * @param govEventsTypePo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> save(GovEventsTypePo govEventsTypePo) {

        govEventsTypePo.setTypeId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_PUBLIC_indexId));
        int flag = govEventsTypeInnerServiceSMOImpl.saveGovEventsType(govEventsTypePo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
