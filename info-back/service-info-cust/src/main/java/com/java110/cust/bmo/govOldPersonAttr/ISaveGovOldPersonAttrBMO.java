package com.java110.cust.bmo.govOldPersonAttr;

import org.springframework.http.ResponseEntity;
import com.java110.po.govOldPersonAttr.GovOldPersonAttrPo;
public interface ISaveGovOldPersonAttrBMO {


    /**
     * 添加老人属性
     * add by wuxw
     * @param govOldPersonAttrPo
     * @return
     */
    ResponseEntity<String> save(GovOldPersonAttrPo govOldPersonAttrPo);


}
