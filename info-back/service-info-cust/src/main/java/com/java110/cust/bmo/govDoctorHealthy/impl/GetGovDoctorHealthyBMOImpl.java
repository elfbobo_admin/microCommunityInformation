package com.java110.cust.bmo.govDoctorHealthy.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.cust.bmo.govDoctorHealthy.IGetGovDoctorHealthyBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.cust.IGovDoctorHealthyInnerServiceSMO;
import com.java110.dto.govDoctorHealthy.GovDoctorHealthyDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import org.springframework.http.HttpStatus;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service("getGovDoctorHealthyBMOImpl")
public class GetGovDoctorHealthyBMOImpl implements IGetGovDoctorHealthyBMO {

    @Autowired
    private IGovDoctorHealthyInnerServiceSMO govDoctorHealthyInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govDoctorHealthyDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovDoctorHealthyDto govDoctorHealthyDto) {


        int count = govDoctorHealthyInnerServiceSMOImpl.queryGovDoctorHealthysCount(govDoctorHealthyDto);

        List<GovDoctorHealthyDto> govDoctorHealthyDtos = null;
        if (count > 0) {
            govDoctorHealthyDtos = govDoctorHealthyInnerServiceSMOImpl.queryGovDoctorHealthys(govDoctorHealthyDto);
        } else {
            govDoctorHealthyDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govDoctorHealthyDto.getRow()), count, govDoctorHealthyDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
