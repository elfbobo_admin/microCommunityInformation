package com.java110.cust.smo.impl;


import com.java110.cust.dao.IGovCompanyPersonServiceDao;
import com.java110.dto.govCompanyPerson.GovCompanyPersonDto;
import com.java110.intf.cust.IGovCompanyPersonInnerServiceSMO;
import com.java110.po.govCompanyPerson.GovCompanyPersonPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 小区位置内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovCompanyPersonInnerServiceSMOImpl extends BaseServiceSMO implements IGovCompanyPersonInnerServiceSMO {

    @Autowired
    private IGovCompanyPersonServiceDao govCompanyPersonServiceDaoImpl;


    @Override
    public int saveGovCompanyPerson(@RequestBody GovCompanyPersonPo govCompanyPersonPo) {
        int saveFlag = 1;
        govCompanyPersonServiceDaoImpl.saveGovCompanyPersonInfo(BeanConvertUtil.beanCovertMap(govCompanyPersonPo));
        return saveFlag;
    }

     @Override
    public int updateGovCompanyPerson(@RequestBody  GovCompanyPersonPo govCompanyPersonPo) {
        int saveFlag = 1;
         govCompanyPersonServiceDaoImpl.updateGovCompanyPersonInfo(BeanConvertUtil.beanCovertMap(govCompanyPersonPo));
        return saveFlag;
    }

     @Override
    public int deleteGovCompanyPerson(@RequestBody  GovCompanyPersonPo govCompanyPersonPo) {
        int saveFlag = 1;
        govCompanyPersonPo.setStatusCd("1");
        govCompanyPersonServiceDaoImpl.updateGovCompanyPersonInfo(BeanConvertUtil.beanCovertMap(govCompanyPersonPo));
        return saveFlag;
    }

    @Override
    public List<GovCompanyPersonDto> queryGovCompanyPersons(@RequestBody  GovCompanyPersonDto govCompanyPersonDto) {

        //校验是否传了 分页信息

        int page = govCompanyPersonDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govCompanyPersonDto.setPage((page - 1) * govCompanyPersonDto.getRow());
        }

        List<GovCompanyPersonDto> govCompanyPersons = BeanConvertUtil.covertBeanList(govCompanyPersonServiceDaoImpl.getGovCompanyPersonInfo(BeanConvertUtil.beanCovertMap(govCompanyPersonDto)), GovCompanyPersonDto.class);

        return govCompanyPersons;
    }


    @Override
    public int queryGovCompanyPersonsCount(@RequestBody GovCompanyPersonDto govCompanyPersonDto) {
        return govCompanyPersonServiceDaoImpl.queryGovCompanyPersonsCount(BeanConvertUtil.beanCovertMap(govCompanyPersonDto));    }

    public IGovCompanyPersonServiceDao getGovCompanyPersonServiceDaoImpl() {
        return govCompanyPersonServiceDaoImpl;
    }

    public void setGovCompanyPersonServiceDaoImpl(IGovCompanyPersonServiceDao govCompanyPersonServiceDaoImpl) {
        this.govCompanyPersonServiceDaoImpl = govCompanyPersonServiceDaoImpl;
    }
}
