package com.java110.cust.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.cust.bmo.govPerson.IDeleteGovPersonBMO;
import com.java110.cust.bmo.govPerson.IGetGovPersonBMO;
import com.java110.cust.bmo.govPerson.ISaveGovPersonBMO;
import com.java110.cust.bmo.govPerson.IUpdateGovPersonBMO;
import com.java110.dto.govPerson.GovPersonDto;
import com.java110.po.govPerson.GovPersonPo;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govPersonCompany")
public class GovPersonCompanyApi {

    @Autowired
    private ISaveGovPersonBMO saveGovPersonBMOImpl;
    @Autowired
    private IUpdateGovPersonBMO updateGovPersonBMOImpl;
    @Autowired
    private IDeleteGovPersonBMO deleteGovPersonBMOImpl;

    @Autowired
    private IGetGovPersonBMO getGovPersonBMOImpl;

    /**
     * 微信保存消息模板
     * @serviceCode /govPersonCompany/saveGovPersonCompany
     * @path /app/govPerson/saveGovPerson
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/saveGovPersonCompany", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovPersonCompany(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "personType", "请求报文中未包含personType");
        Assert.hasKeyAndValue(reqJson, "idType", "请求报文中未包含idType");
        Assert.hasKeyAndValue(reqJson, "idCard", "请求报文中未包含idCard");
        Assert.hasKeyAndValue(reqJson, "personName", "请求报文中未包含personName");
        Assert.hasKeyAndValue(reqJson, "personTel", "请求报文中未包含personTel");
        Assert.hasKeyAndValue(reqJson, "personSex", "请求报文中未包含personSex");
        Assert.hasKeyAndValue(reqJson, "birthday", "请求报文中未包含birthday");
        Assert.hasKeyAndValue(reqJson, "nation", "请求报文中未包含nation");
        Assert.hasKeyAndValue(reqJson, "nativePlace", "请求报文中未包含nativePlace");
        Assert.hasKeyAndValue(reqJson, "politicalOutlook", "请求报文中未包含politicalOutlook");
        Assert.hasKeyAndValue(reqJson, "maritalStatus", "请求报文中未包含maritalStatus");
        Assert.hasKeyAndValue(reqJson, "govCompanyId", "请求报文中未包含govCompanyId");
        Assert.hasKeyAndValue(reqJson, "state", "请求报文中未包含state");
        Assert.hasKeyAndValue(reqJson, "relCd", "请求报文中未包含relCd");
        Assert.hasKeyAndValue(reqJson, "govOrgName", "请求报文中未包含govOrgName");


        GovPersonPo govPersonPo = BeanConvertUtil.covertBean(reqJson, GovPersonPo.class);
        return saveGovPersonBMOImpl.saveGovPersonCompany(govPersonPo);
    }

    /**
     * 微信修改消息模板
     * @serviceCode /govPersonCompany/updateGovPersonCompany
     * @path /app/govPerson/updateGovPersonCompany
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/updateGovPersonCompany", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovPersonCompany(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "personType", "请求报文中未包含personType");
        Assert.hasKeyAndValue(reqJson, "idType", "请求报文中未包含idType");
        Assert.hasKeyAndValue(reqJson, "idCard", "请求报文中未包含idCard");
        Assert.hasKeyAndValue(reqJson, "personName", "请求报文中未包含personName");
        Assert.hasKeyAndValue(reqJson, "personTel", "请求报文中未包含personTel");
        Assert.hasKeyAndValue(reqJson, "personSex", "请求报文中未包含personSex");
        Assert.hasKeyAndValue(reqJson, "birthday", "请求报文中未包含birthday");
        Assert.hasKeyAndValue(reqJson, "nation", "请求报文中未包含nation");
        Assert.hasKeyAndValue(reqJson, "nativePlace", "请求报文中未包含nativePlace");
        Assert.hasKeyAndValue(reqJson, "politicalOutlook", "请求报文中未包含politicalOutlook");
        Assert.hasKeyAndValue(reqJson, "maritalStatus", "请求报文中未包含maritalStatus");
        Assert.hasKeyAndValue(reqJson, "govPersonId", "govPersonId不能为空");
        Assert.hasKeyAndValue(reqJson, "govCompanyId", "请求报文中未包含govCompanyId");
        Assert.hasKeyAndValue(reqJson, "state", "请求报文中未包含state");
        Assert.hasKeyAndValue(reqJson, "relCd", "请求报文中未包含relCd");
        Assert.hasKeyAndValue(reqJson, "govOrgName", "请求报文中未包含govOrgName");

        GovPersonPo govPersonPo = BeanConvertUtil.covertBean(reqJson, GovPersonPo.class);
        return updateGovPersonBMOImpl.updateGovPersonCompany(govPersonPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govPersonCompany/queryGovPersonCompany
     * @path /app/govPerson/queryGovPerson
     * @param
     * @return
     */
    @RequestMapping(value = "/queryGovPersonCompany", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovPersonCompany(@RequestParam(value = "caId" , required = false) String caId,
                                                      @RequestParam(value = "govPersonId" , required = false) String govPersonId,
                                                      @RequestParam(value = "govCompanyId" , required = false) String govCompanyId,
                                                      @RequestParam(value = "state" , required = false) String state,
                                                      @RequestParam(value = "personType" , required = false) String personType,
                                                      @RequestParam(value = "personSex" , required = false) String personSex,
                                                      @RequestParam(value = "idCard" , required = false) String idCard,
                                                      @RequestParam(value = "isWeb" , required = false) String isWeb,
                                                      @RequestParam(value = "page") int page,
                                                      @RequestParam(value = "row") int row) {
        GovPersonDto govPersonDto = new GovPersonDto();
        govPersonDto.setPage(page);
        govPersonDto.setRow(row);
        govPersonDto.setCaId(caId);
        govPersonDto.setGovPersonId(govPersonId);
        govPersonDto.setPersonType(personType);
        govPersonDto.setPersonSex(personSex);
        govPersonDto.setIdCard(idCard);
        govPersonDto.setGovCompanyId(govCompanyId);
        govPersonDto.setState(state);
        govPersonDto.setIsWeb(isWeb);
        return getGovPersonBMOImpl.getGovPersonCompany(govPersonDto);
    }
}
