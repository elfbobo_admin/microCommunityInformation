package com.java110.cust.bmo.govMajorEvents;
import org.springframework.http.ResponseEntity;
import com.java110.dto.govMajorEvents.GovMajorEventsDto;
public interface IGetGovMajorEventsBMO {


    /**
     * 查询重特大事件
     * add by wuxw
     * @param  govMajorEventsDto
     * @return
     */
    ResponseEntity<String> get(GovMajorEventsDto govMajorEventsDto);


}
