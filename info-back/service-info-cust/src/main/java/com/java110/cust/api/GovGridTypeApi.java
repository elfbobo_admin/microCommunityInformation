package com.java110.cust.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.cust.bmo.govGridType.IDeleteGovGridTypeBMO;
import com.java110.cust.bmo.govGridType.IGetGovGridTypeBMO;
import com.java110.cust.bmo.govGridType.ISaveGovGridTypeBMO;
import com.java110.cust.bmo.govGridType.IUpdateGovGridTypeBMO;
import com.java110.dto.govGridType.GovGridTypeDto;
import com.java110.po.govGridType.GovGridTypePo;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govGridType")
public class GovGridTypeApi {

    @Autowired
    private ISaveGovGridTypeBMO saveGovGridTypeBMOImpl;
    @Autowired
    private IUpdateGovGridTypeBMO updateGovGridTypeBMOImpl;
    @Autowired
    private IDeleteGovGridTypeBMO deleteGovGridTypeBMOImpl;

    @Autowired
    private IGetGovGridTypeBMO getGovGridTypeBMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govGridType/saveGovGridType
     * @path /app/govGridType/saveGovGridType
     */
    @RequestMapping(value = "/saveGovGridType", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovGridType(@RequestBody JSONObject reqJson,@RequestHeader(value = "store-id") String storeId) {

        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "typeName", "请求报文中未包含typeName");


        GovGridTypePo govGridTypePo = BeanConvertUtil.covertBean(reqJson, GovGridTypePo.class);
        return saveGovGridTypeBMOImpl.save(govGridTypePo,storeId);
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govGridType/updateGovGridType
     * @path /app/govGridType/updateGovGridType
     */
    @RequestMapping(value = "/updateGovGridType", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovGridType(@RequestBody JSONObject reqJson,@RequestHeader(value = "store-id") String storeId) {

        Assert.hasKeyAndValue(reqJson, "govTypeId", "请求报文中未包含govTypeId");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "typeName", "请求报文中未包含typeName");


        GovGridTypePo govGridTypePo = BeanConvertUtil.covertBean(reqJson, GovGridTypePo.class);
        return updateGovGridTypeBMOImpl.update(govGridTypePo,storeId);
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govGridType/deleteGovGridType
     * @path /app/govGridType/deleteGovGridType
     */
    @RequestMapping(value = "/deleteGovGridType", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovGridType(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "caId", "caId不能为空");

        Assert.hasKeyAndValue(reqJson, "govTypeId", "govTypeId不能为空");


        GovGridTypePo govGridTypePo = BeanConvertUtil.covertBean(reqJson, GovGridTypePo.class);
        return deleteGovGridTypeBMOImpl.delete(govGridTypePo);
    }

    /**
     * 微信删除消息模板
     *
     * @param caId 小区ID
     * @return
     * @serviceCode /govGridType/queryGovGridType
     * @path /app/govGridType/queryGovGridType
     */
    @RequestMapping(value = "/queryGovGridType", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovGridType(@RequestParam(value = "caId") String caId,
                                                   @RequestParam(value = "page") int page,
                                                   @RequestParam(value = "row") int row) {
        GovGridTypeDto govGridTypeDto = new GovGridTypeDto();
        govGridTypeDto.setPage(page);
        govGridTypeDto.setRow(row);
        govGridTypeDto.setCaId(caId);
        return getGovGridTypeBMOImpl.get(govGridTypeDto);
    }
}
