package com.java110.cust.bmo.govGridObjRel;
import org.springframework.http.ResponseEntity;
import com.java110.po.govGridObjRel.GovGridObjRelPo;

public interface IUpdateGovGridObjRelBMO {


    /**
     * 修改网格对象关系
     * add by wuxw
     * @param govGridObjRelPo
     * @return
     */
    ResponseEntity<String> update(GovGridObjRelPo govGridObjRelPo);


}
