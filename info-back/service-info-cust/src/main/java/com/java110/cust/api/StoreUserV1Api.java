package com.java110.cust.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.StoreUserV1.StoreUserV1Dto;
import com.java110.po.StoreUserV1.StoreUserV1Po;
import com.java110.cust.bmo.StoreUserV1.IDeleteStoreUserV1BMO;
import com.java110.cust.bmo.StoreUserV1.IGetStoreUserV1BMO;
import com.java110.cust.bmo.StoreUserV1.ISaveStoreUserV1BMO;
import com.java110.cust.bmo.StoreUserV1.IUpdateStoreUserV1BMO;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/StoreUserV1")
public class StoreUserV1Api {

    @Autowired
    private ISaveStoreUserV1BMO saveStoreUserV1BMOImpl;
    @Autowired
    private IUpdateStoreUserV1BMO updateStoreUserV1BMOImpl;
    @Autowired
    private IDeleteStoreUserV1BMO deleteStoreUserV1BMOImpl;

    @Autowired
    private IGetStoreUserV1BMO getStoreUserV1BMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /StoreUserV1/saveStoreUserV1
     * @path /app/StoreUserV1/saveStoreUserV1
     */
    @RequestMapping(value = "/saveStoreUserV1", method = RequestMethod.POST)
    public ResponseEntity<String> saveStoreUserV1(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "storeUserId", "请求报文中未包含storeUserId");
        Assert.hasKeyAndValue(reqJson, "storeId", "请求报文中未包含storeId");
        Assert.hasKeyAndValue(reqJson, "bId", "请求报文中未包含bId");
        Assert.hasKeyAndValue(reqJson, "userId", "请求报文中未包含userId");
        Assert.hasKeyAndValue(reqJson, "relCd", "请求报文中未包含relCd");
        Assert.hasKeyAndValue(reqJson, "statusCd", "请求报文中未包含statusCd");


        StoreUserV1Po StoreUserV1Po = BeanConvertUtil.covertBean(reqJson, StoreUserV1Po.class);
        return saveStoreUserV1BMOImpl.save(StoreUserV1Po);
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /StoreUserV1/updateStoreUserV1
     * @path /app/StoreUserV1/updateStoreUserV1
     */
    @RequestMapping(value = "/updateStoreUserV1", method = RequestMethod.POST)
    public ResponseEntity<String> updateStoreUserV1(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "storeUserId", "请求报文中未包含storeUserId");
        Assert.hasKeyAndValue(reqJson, "storeId", "请求报文中未包含storeId");
        Assert.hasKeyAndValue(reqJson, "bId", "请求报文中未包含bId");
        Assert.hasKeyAndValue(reqJson, "userId", "请求报文中未包含userId");
        Assert.hasKeyAndValue(reqJson, "relCd", "请求报文中未包含relCd");
        Assert.hasKeyAndValue(reqJson, "statusCd", "请求报文中未包含statusCd");
        Assert.hasKeyAndValue(reqJson, "storeUserId", "storeUserId不能为空");


        StoreUserV1Po StoreUserV1Po = BeanConvertUtil.covertBean(reqJson, StoreUserV1Po.class);
        return updateStoreUserV1BMOImpl.update(StoreUserV1Po);
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /StoreUserV1/deleteStoreUserV1
     * @path /app/StoreUserV1/deleteStoreUserV1
     */
    @RequestMapping(value = "/deleteStoreUserV1", method = RequestMethod.POST)
    public ResponseEntity<String> deleteStoreUserV1(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "communityId", "小区ID不能为空");

        Assert.hasKeyAndValue(reqJson, "storeUserId", "storeUserId不能为空");


        StoreUserV1Po StoreUserV1Po = BeanConvertUtil.covertBean(reqJson, StoreUserV1Po.class);
        return deleteStoreUserV1BMOImpl.delete(StoreUserV1Po);
    }

    /**
     * 微信删除消息模板
     *
     * @param
     * @return
     * @serviceCode /StoreUserV1/queryStoreUserV1
     * @path /app/StoreUserV1/queryStoreUserV1
     */
    @RequestMapping(value = "/queryStoreUserV1", method = RequestMethod.GET)
    public ResponseEntity<String> queryStoreUserV1(@RequestParam(value = "page") int page,
                                                   @RequestParam(value = "row") int row) {
        StoreUserV1Dto StoreUserV1Dto = new StoreUserV1Dto();
        StoreUserV1Dto.setPage(page);
        StoreUserV1Dto.setRow(row);
        return getStoreUserV1BMOImpl.get(StoreUserV1Dto);
    }
}
