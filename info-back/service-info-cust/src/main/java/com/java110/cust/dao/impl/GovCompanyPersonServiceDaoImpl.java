package com.java110.cust.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.cust.dao.IGovCompanyPersonServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 小区位置服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govCompanyPersonServiceDaoImpl")
//@Transactional
public class GovCompanyPersonServiceDaoImpl extends BaseServiceDao implements IGovCompanyPersonServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovCompanyPersonServiceDaoImpl.class);





    /**
     * 保存小区位置信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovCompanyPersonInfo(Map info) throws DAOException {
        logger.debug("保存小区位置信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govCompanyPersonServiceDaoImpl.saveGovCompanyPersonInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存小区位置信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询小区位置信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovCompanyPersonInfo(Map info) throws DAOException {
        logger.debug("查询小区位置信息 入参 info : {}",info);

        List<Map> businessGovCompanyPersonInfos = sqlSessionTemplate.selectList("govCompanyPersonServiceDaoImpl.getGovCompanyPersonInfo",info);

        return businessGovCompanyPersonInfos;
    }


    /**
     * 修改小区位置信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovCompanyPersonInfo(Map info) throws DAOException {
        logger.debug("修改小区位置信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govCompanyPersonServiceDaoImpl.updateGovCompanyPersonInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改小区位置信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询小区位置数量
     * @param info 小区位置信息
     * @return 小区位置数量
     */
    @Override
    public int queryGovCompanyPersonsCount(Map info) {
        logger.debug("查询小区位置数据 入参 info : {}",info);

        List<Map> businessGovCompanyPersonInfos = sqlSessionTemplate.selectList("govCompanyPersonServiceDaoImpl.queryGovCompanyPersonsCount", info);
        if (businessGovCompanyPersonInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovCompanyPersonInfos.get(0).get("count").toString());
    }


}
