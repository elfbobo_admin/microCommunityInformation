package com.java110.cust.smo.impl;


import com.java110.cust.dao.IGovMedicalClassifyServiceDao;
import com.java110.intf.cust.IGovMedicalClassifyInnerServiceSMO;
import com.java110.dto.govMedicalClassify.GovMedicalClassifyDto;
import com.java110.po.govMedicalClassify.GovMedicalClassifyPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 医疗分类内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovMedicalClassifyInnerServiceSMOImpl extends BaseServiceSMO implements IGovMedicalClassifyInnerServiceSMO {

    @Autowired
    private IGovMedicalClassifyServiceDao govMedicalClassifyServiceDaoImpl;


    @Override
    public int saveGovMedicalClassify(@RequestBody  GovMedicalClassifyPo govMedicalClassifyPo) {
        int saveFlag = 1;
        govMedicalClassifyServiceDaoImpl.saveGovMedicalClassifyInfo(BeanConvertUtil.beanCovertMap(govMedicalClassifyPo));
        return saveFlag;
    }

     @Override
    public int updateGovMedicalClassify(@RequestBody  GovMedicalClassifyPo govMedicalClassifyPo) {
        int saveFlag = 1;
         govMedicalClassifyServiceDaoImpl.updateGovMedicalClassifyInfo(BeanConvertUtil.beanCovertMap(govMedicalClassifyPo));
        return saveFlag;
    }

     @Override
    public int deleteGovMedicalClassify(@RequestBody  GovMedicalClassifyPo govMedicalClassifyPo) {
        int saveFlag = 1;
        govMedicalClassifyPo.setStatusCd("1");
        govMedicalClassifyServiceDaoImpl.updateGovMedicalClassifyInfo(BeanConvertUtil.beanCovertMap(govMedicalClassifyPo));
        return saveFlag;
    }

    @Override
    public List<GovMedicalClassifyDto> queryGovMedicalClassifys(@RequestBody  GovMedicalClassifyDto govMedicalClassifyDto) {

        //校验是否传了 分页信息

        int page = govMedicalClassifyDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govMedicalClassifyDto.setPage((page - 1) * govMedicalClassifyDto.getRow());
        }

        List<GovMedicalClassifyDto> govMedicalClassifys = BeanConvertUtil.covertBeanList(govMedicalClassifyServiceDaoImpl.getGovMedicalClassifyInfo(BeanConvertUtil.beanCovertMap(govMedicalClassifyDto)), GovMedicalClassifyDto.class);

        return govMedicalClassifys;
    }


    @Override
    public int queryGovMedicalClassifysCount(@RequestBody GovMedicalClassifyDto govMedicalClassifyDto) {
        return govMedicalClassifyServiceDaoImpl.queryGovMedicalClassifysCount(BeanConvertUtil.beanCovertMap(govMedicalClassifyDto));    }

    public IGovMedicalClassifyServiceDao getGovMedicalClassifyServiceDaoImpl() {
        return govMedicalClassifyServiceDaoImpl;
    }

    public void setGovMedicalClassifyServiceDaoImpl(IGovMedicalClassifyServiceDao govMedicalClassifyServiceDaoImpl) {
        this.govMedicalClassifyServiceDaoImpl = govMedicalClassifyServiceDaoImpl;
    }
}
