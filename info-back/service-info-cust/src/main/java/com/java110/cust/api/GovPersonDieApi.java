package com.java110.cust.api;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govPersonDie.GovPersonDieDto;
import com.java110.po.govPersonDie.GovPersonDiePo;
import com.java110.cust.bmo.govPersonDie.IDeleteGovPersonDieBMO;
import com.java110.cust.bmo.govPersonDie.IGetGovPersonDieBMO;
import com.java110.cust.bmo.govPersonDie.ISaveGovPersonDieBMO;
import com.java110.cust.bmo.govPersonDie.IUpdateGovPersonDieBMO;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govPersonDie")
public class GovPersonDieApi {

    @Autowired
    private ISaveGovPersonDieBMO saveGovPersonDieBMOImpl;
    @Autowired
    private IUpdateGovPersonDieBMO updateGovPersonDieBMOImpl;
    @Autowired
    private IDeleteGovPersonDieBMO deleteGovPersonDieBMOImpl;

    @Autowired
    private IGetGovPersonDieBMO getGovPersonDieBMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govPersonDie/saveGovPersonDie
     * @path /app/govPersonDie/saveGovPersonDie
     */
    @RequestMapping(value = "/saveGovPersonDie", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovPersonDie(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "govPersonId", "请求报文中未包含govPersonId");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "diePlace", "请求报文中未包含diePlace");
        Assert.hasKeyAndValue(reqJson, "dieTime", "请求报文中未包含dieTime");
        Assert.hasKeyAndValue(reqJson, "contactPerson", "请求报文中未包含contactPerson");
        Assert.hasKeyAndValue(reqJson, "contactTel", "请求报文中未包含contactTel");

        GovPersonDiePo govPersonDiePo = BeanConvertUtil.covertBean(reqJson, GovPersonDiePo.class);
        return saveGovPersonDieBMOImpl.save(govPersonDiePo);
    }

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govPersonDie/savePhonePersonDie
     * @path /app/govPersonDie/savePhonePersonDie
     */
    @RequestMapping(value = "/savePhonePersonDie", method = RequestMethod.POST)
    public ResponseEntity<String> savePhonePersonDie(@RequestHeader(value = "user-id") String userId,@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "diePlace", "请求报文中未包含diePlace");
        Assert.hasKeyAndValue(reqJson, "dieTime", "请求报文中未包含dieTime");
        Assert.hasKeyAndValue(reqJson, "contactPerson", "请求报文中未包含contactPerson");
        Assert.hasKeyAndValue(reqJson, "contactTel", "请求报文中未包含contactTel");
        reqJson.put( "userId",userId );
        GovPersonDiePo govPersonDiePo = BeanConvertUtil.covertBean(reqJson, GovPersonDiePo.class);
        JSONArray photos = reqJson.getJSONArray("photos");
        return saveGovPersonDieBMOImpl.savePhone(govPersonDiePo,photos);
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govPersonDie/updateGovPersonDie
     * @path /app/govPersonDie/updateGovPersonDie
     */
    @RequestMapping(value = "/updateGovPersonDie", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovPersonDie(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "dieId", "请求报文中未包含dieId");
        Assert.hasKeyAndValue(reqJson, "govPersonId", "请求报文中未包含govPersonId");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "diePlace", "请求报文中未包含diePlace");
        Assert.hasKeyAndValue(reqJson, "dieTime", "请求报文中未包含dieTime");
        Assert.hasKeyAndValue(reqJson, "contactPerson", "请求报文中未包含contactPerson");
        Assert.hasKeyAndValue(reqJson, "contactTel", "请求报文中未包含contactTel");

        GovPersonDiePo govPersonDiePo = BeanConvertUtil.covertBean(reqJson, GovPersonDiePo.class);
        return updateGovPersonDieBMOImpl.update(govPersonDiePo);
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govPersonDie/deleteGovPersonDie
     * @path /app/govPersonDie/deleteGovPersonDie
     */
    @RequestMapping(value = "/deleteGovPersonDie", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovPersonDie(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "caId", "caId不能为空");
        Assert.hasKeyAndValue(reqJson, "dieId", "dieId不能为空");
        GovPersonDiePo govPersonDiePo = BeanConvertUtil.covertBean(reqJson, GovPersonDiePo.class);
        return deleteGovPersonDieBMOImpl.delete(govPersonDiePo);
    }

    /**
     * 微信删除消息模板
     *
     * @param caId 小区ID
     * @return
     * @serviceCode /govPersonDie/queryGovPersonDie
     * @path /app/govPersonDie/queryGovPersonDie
     */
    @RequestMapping(value = "/queryGovPersonDie", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovPersonDie(@RequestParam(value = "caId") String caId,
                                                    @RequestParam(value = "personName" , required = false) String personName,
                                                    @RequestParam(value = "contactPerson" , required = false) String contactPerson,
                                                    @RequestParam(value = "contactTel" , required = false) String contactTel,
                                                    @RequestParam(value = "dieTime" , required = false) String dieTime,
                                                    @RequestParam(value = "diePlace" , required = false) String diePlace,
                                                    @RequestParam(value = "communityName" , required = false) String communityName,
                                                    @RequestParam(value = "page") int page,
                                                    @RequestParam(value = "row") int row) {
        GovPersonDieDto govPersonDieDto = new GovPersonDieDto();
        govPersonDieDto.setPage(page);
        govPersonDieDto.setRow(row);
        govPersonDieDto.setCaId(caId);
        govPersonDieDto.setPersonName(personName);
        govPersonDieDto.setContactTel(contactTel);
        govPersonDieDto.setDieTime(dieTime);
        govPersonDieDto.setDiePlace(diePlace);
        govPersonDieDto.setCommunityName(communityName);
        return getGovPersonDieBMOImpl.get(govPersonDieDto);
    }
}
