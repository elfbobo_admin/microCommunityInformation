package com.java110.cust.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.cust.dao.IGovOldPersonServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 老人管理服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govOldPersonServiceDaoImpl")
//@Transactional
public class GovOldPersonServiceDaoImpl extends BaseServiceDao implements IGovOldPersonServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovOldPersonServiceDaoImpl.class);





    /**
     * 保存老人管理信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovOldPersonInfo(Map info) throws DAOException {
        logger.debug("保存老人管理信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govOldPersonServiceDaoImpl.saveGovOldPersonInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存老人管理信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询老人管理信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovOldPersonInfo(Map info) throws DAOException {
        logger.debug("查询老人管理信息 入参 info : {}",info);

        List<Map> businessGovOldPersonInfos = sqlSessionTemplate.selectList("govOldPersonServiceDaoImpl.getGovOldPersonInfo",info);

        return businessGovOldPersonInfos;
    }

    /**
     * 查询老人管理信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovOldTypePersonCount(Map info) throws DAOException {
        logger.debug("查询老人管理信息 入参 info : {}",info);

        List<Map> businessGovOldPersonInfos = sqlSessionTemplate.selectList("govOldPersonServiceDaoImpl.getGovOldTypePersonCount",info);

        return businessGovOldPersonInfos;
    }


    /**
     * 修改老人管理信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovOldPersonInfo(Map info) throws DAOException {
        logger.debug("修改老人管理信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govOldPersonServiceDaoImpl.updateGovOldPersonInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改老人管理信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询老人管理数量
     * @param info 老人管理信息
     * @return 老人管理数量
     */
    @Override
    public int queryGovOldPersonsCount(Map info) {
        logger.debug("查询老人管理数据 入参 info : {}",info);

        List<Map> businessGovOldPersonInfos = sqlSessionTemplate.selectList("govOldPersonServiceDaoImpl.queryGovOldPersonsCount", info);
        if (businessGovOldPersonInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovOldPersonInfos.get(0).get("count").toString());
    }


}
