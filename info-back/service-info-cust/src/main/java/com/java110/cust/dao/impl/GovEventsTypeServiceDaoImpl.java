package com.java110.cust.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.cust.dao.IGovEventsTypeServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 事件类型服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govEventsTypeServiceDaoImpl")
//@Transactional
public class GovEventsTypeServiceDaoImpl extends BaseServiceDao implements IGovEventsTypeServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovEventsTypeServiceDaoImpl.class);





    /**
     * 保存事件类型信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovEventsTypeInfo(Map info) throws DAOException {
        logger.debug("保存事件类型信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govEventsTypeServiceDaoImpl.saveGovEventsTypeInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存事件类型信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询事件类型信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovEventsTypeInfo(Map info) throws DAOException {
        logger.debug("查询事件类型信息 入参 info : {}",info);

        List<Map> businessGovEventsTypeInfos = sqlSessionTemplate.selectList("govEventsTypeServiceDaoImpl.getGovEventsTypeInfo",info);

        return businessGovEventsTypeInfos;
    }


    /**
     * 修改事件类型信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovEventsTypeInfo(Map info) throws DAOException {
        logger.debug("修改事件类型信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govEventsTypeServiceDaoImpl.updateGovEventsTypeInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改事件类型信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询事件类型数量
     * @param info 事件类型信息
     * @return 事件类型数量
     */
    @Override
    public int queryGovEventsTypesCount(Map info) {
        logger.debug("查询事件类型数据 入参 info : {}",info);

        List<Map> businessGovEventsTypeInfos = sqlSessionTemplate.selectList("govEventsTypeServiceDaoImpl.queryGovEventsTypesCount", info);
        if (businessGovEventsTypeInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovEventsTypeInfos.get(0).get("count").toString());
    }


}
