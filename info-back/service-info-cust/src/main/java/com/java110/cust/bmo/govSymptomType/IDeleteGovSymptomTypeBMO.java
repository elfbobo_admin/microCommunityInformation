package com.java110.cust.bmo.govSymptomType;
import org.springframework.http.ResponseEntity;
import com.java110.po.govSymptomType.GovSymptomTypePo;

public interface IDeleteGovSymptomTypeBMO {


    /**
     * 修改症状类型
     * add by wuxw
     * @param govSymptomTypePo
     * @return
     */
    ResponseEntity<String> delete(GovSymptomTypePo govSymptomTypePo);


}
