package com.java110.cust.dao;


import com.java110.utils.exception.DAOException;


import java.util.List;
import java.util.Map;

/**
 * 随访登记组件内部之间使用，没有给外围系统提供服务能力
 * 随访登记服务接口类，要求全部以字符串传输，方便微服务化
 * 新建客户，修改客户，删除客户，查询客户等功能
 *
 * Created by wuxw on 2016/12/27.
 */
public interface IGovFollowupSurveyServiceDao {


    /**
     * 保存 随访登记信息
     * @param info
     * @throws DAOException DAO异常
     */
    void saveGovFollowupSurveyInfo(Map info) throws DAOException;




    /**
     * 查询随访登记信息（instance过程）
     * 根据bId 查询随访登记信息
     * @param info bId 信息
     * @return 随访登记信息
     * @throws DAOException DAO异常
     */
    List<Map> getGovFollowupSurveyInfo(Map info) throws DAOException;



    /**
     * 修改随访登记信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    void updateGovFollowupSurveyInfo(Map info) throws DAOException;


    /**
     * 查询随访登记总数
     *
     * @param info 随访登记信息
     * @return 随访登记数量
     */
    int queryGovFollowupSurveysCount(Map info);

}
