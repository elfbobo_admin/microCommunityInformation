package com.java110.cust.bmo.govMedicalClassify;
import org.springframework.http.ResponseEntity;
import com.java110.po.govMedicalClassify.GovMedicalClassifyPo;

public interface IDeleteGovMedicalClassifyBMO {


    /**
     * 修改医疗分类
     * add by wuxw
     * @param govMedicalClassifyPo
     * @return
     */
    ResponseEntity<String> delete(GovMedicalClassifyPo govMedicalClassifyPo);


}
