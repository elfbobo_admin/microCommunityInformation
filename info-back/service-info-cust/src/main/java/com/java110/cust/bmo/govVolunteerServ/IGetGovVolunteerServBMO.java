package com.java110.cust.bmo.govVolunteerServ;
import org.springframework.http.ResponseEntity;
import com.java110.dto.govVolunteerServ.GovVolunteerServDto;
public interface IGetGovVolunteerServBMO {


    /**
     * 查询服务领域
     * add by wuxw
     * @param  govVolunteerServDto
     * @return
     */
    ResponseEntity<String> get(GovVolunteerServDto govVolunteerServDto);


}
