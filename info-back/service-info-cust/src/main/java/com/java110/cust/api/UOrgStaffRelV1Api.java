package com.java110.cust.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.cust.bmo.uOrgStaffRelV1.IDeleteUOrgStaffRelV1BMO;
import com.java110.cust.bmo.uOrgStaffRelV1.IGetUOrgStaffRelV1BMO;
import com.java110.cust.bmo.uOrgStaffRelV1.ISaveUOrgStaffRelV1BMO;
import com.java110.cust.bmo.uOrgStaffRelV1.IUpdateUOrgStaffRelV1BMO;
import com.java110.dto.uOrgStaffRelV1.UOrgStaffRelV1Dto;
import com.java110.po.uOrgStaffRelV1.UOrgStaffRelV1Po;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/uOrgStaffRelV1")
public class UOrgStaffRelV1Api {

    @Autowired
    private ISaveUOrgStaffRelV1BMO saveUOrgStaffRelV1BMOImpl;
    @Autowired
    private IUpdateUOrgStaffRelV1BMO updateUOrgStaffRelV1BMOImpl;
    @Autowired
    private IDeleteUOrgStaffRelV1BMO deleteUOrgStaffRelV1BMOImpl;

    @Autowired
    private IGetUOrgStaffRelV1BMO getUOrgStaffRelV1BMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /uOrgStaffRelV1/saveUOrgStaffRelV1
     * @path /app/uOrgStaffRelV1/saveUOrgStaffRelV1
     */
    @RequestMapping(value = "/saveUOrgStaffRelV1", method = RequestMethod.POST)
    public ResponseEntity<String> saveUOrgStaffRelV1(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "relId", "请求报文中未包含relId");
        Assert.hasKeyAndValue(reqJson, "bId", "请求报文中未包含bId");
        Assert.hasKeyAndValue(reqJson, "orgId", "请求报文中未包含orgId");
        Assert.hasKeyAndValue(reqJson, "staffId", "请求报文中未包含staffId");
        Assert.hasKeyAndValue(reqJson, "relCd", "请求报文中未包含relCd");
        Assert.hasKeyAndValue(reqJson, "statusCd", "请求报文中未包含statusCd");
        Assert.hasKeyAndValue(reqJson, "storeId", "请求报文中未包含storeId");


        UOrgStaffRelV1Po uOrgStaffRelV1Po = BeanConvertUtil.covertBean(reqJson, UOrgStaffRelV1Po.class);
        return saveUOrgStaffRelV1BMOImpl.save(uOrgStaffRelV1Po);
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /uOrgStaffRelV1/updateUOrgStaffRelV1
     * @path /app/uOrgStaffRelV1/updateUOrgStaffRelV1
     */
    @RequestMapping(value = "/updateUOrgStaffRelV1", method = RequestMethod.POST)
    public ResponseEntity<String> updateUOrgStaffRelV1(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "relId", "请求报文中未包含relId");
        Assert.hasKeyAndValue(reqJson, "bId", "请求报文中未包含bId");
        Assert.hasKeyAndValue(reqJson, "orgId", "请求报文中未包含orgId");
        Assert.hasKeyAndValue(reqJson, "staffId", "请求报文中未包含staffId");
        Assert.hasKeyAndValue(reqJson, "relCd", "请求报文中未包含relCd");
        Assert.hasKeyAndValue(reqJson, "statusCd", "请求报文中未包含statusCd");
        Assert.hasKeyAndValue(reqJson, "storeId", "请求报文中未包含storeId");
        Assert.hasKeyAndValue(reqJson, "relId", "relId不能为空");


        UOrgStaffRelV1Po uOrgStaffRelV1Po = BeanConvertUtil.covertBean(reqJson, UOrgStaffRelV1Po.class);
        return updateUOrgStaffRelV1BMOImpl.update(uOrgStaffRelV1Po);
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /uOrgStaffRelV1/deleteUOrgStaffRelV1
     * @path /app/uOrgStaffRelV1/deleteUOrgStaffRelV1
     */
    @RequestMapping(value = "/deleteUOrgStaffRelV1", method = RequestMethod.POST)
    public ResponseEntity<String> deleteUOrgStaffRelV1(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "communityId", "小区ID不能为空");

        Assert.hasKeyAndValue(reqJson, "relId", "relId不能为空");


        UOrgStaffRelV1Po uOrgStaffRelV1Po = BeanConvertUtil.covertBean(reqJson, UOrgStaffRelV1Po.class);
        return deleteUOrgStaffRelV1BMOImpl.delete(uOrgStaffRelV1Po);
    }

    /**
     * 微信删除消息模板
     *
     * @param
     * @return
     * @serviceCode /uOrgStaffRelV1/queryUOrgStaffRelV1
     * @path /app/uOrgStaffRelV1/queryUOrgStaffRelV1
     */
    @RequestMapping(value = "/queryUOrgStaffRelV1", method = RequestMethod.GET)
    public ResponseEntity<String> queryUOrgStaffRelV1(@RequestParam(value = "page") int page,
                                                      @RequestParam(value = "row") int row) {
        UOrgStaffRelV1Dto uOrgStaffRelV1Dto = new UOrgStaffRelV1Dto();
        uOrgStaffRelV1Dto.setPage(page);
        uOrgStaffRelV1Dto.setRow(row);

        return getUOrgStaffRelV1BMOImpl.get(uOrgStaffRelV1Dto);
    }
}
