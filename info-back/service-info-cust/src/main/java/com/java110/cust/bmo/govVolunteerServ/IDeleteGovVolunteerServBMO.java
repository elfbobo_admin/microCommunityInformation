package com.java110.cust.bmo.govVolunteerServ;
import org.springframework.http.ResponseEntity;
import com.java110.po.govVolunteerServ.GovVolunteerServPo;

public interface IDeleteGovVolunteerServBMO {


    /**
     * 修改服务领域
     * add by wuxw
     * @param govVolunteerServPo
     * @return
     */
    ResponseEntity<String> delete(GovVolunteerServPo govVolunteerServPo);


}
