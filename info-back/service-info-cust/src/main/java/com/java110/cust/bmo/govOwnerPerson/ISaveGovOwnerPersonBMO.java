package com.java110.cust.bmo.govOwnerPerson;

import com.java110.po.govOwnerPerson.GovOwnerPersonPo;
import org.springframework.http.ResponseEntity;
public interface ISaveGovOwnerPersonBMO {


    /**
     * 添加人口户籍关系
     * add by wuxw
     * @param govOwnerPersonPo
     * @return
     */
    ResponseEntity<String> save(GovOwnerPersonPo govOwnerPersonPo);


}
