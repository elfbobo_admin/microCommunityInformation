package com.java110.cust.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.cust.dao.IUOrgServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 组织管理服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("uOrgServiceDaoImpl")
//@Transactional
public class UOrgServiceDaoImpl extends BaseServiceDao implements IUOrgServiceDao {

    private static Logger logger = LoggerFactory.getLogger(UOrgServiceDaoImpl.class);





    /**
     * 保存组织管理信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveUOrgInfo(Map info) throws DAOException {
        logger.debug("保存组织管理信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("uOrgServiceDaoImpl.saveUOrgInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存组织管理信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询组织管理信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getUOrgInfo(Map info) throws DAOException {
        logger.debug("查询组织管理信息 入参 info : {}",info);

        List<Map> businessUOrgInfos = sqlSessionTemplate.selectList("uOrgServiceDaoImpl.getUOrgInfo",info);

        return businessUOrgInfos;
    }


    /**
     * 修改组织管理信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateUOrgInfo(Map info) throws DAOException {
        logger.debug("修改组织管理信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("uOrgServiceDaoImpl.updateUOrgInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改组织管理信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询组织管理数量
     * @param info 组织管理信息
     * @return 组织管理数量
     */
    @Override
    public int queryUOrgsCount(Map info) {
        logger.debug("查询组织管理数据 入参 info : {}",info);

        List<Map> businessUOrgInfos = sqlSessionTemplate.selectList("uOrgServiceDaoImpl.queryUOrgsCount", info);
        if (businessUOrgInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessUOrgInfos.get(0).get("count").toString());
    }


}
