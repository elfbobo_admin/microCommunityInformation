package com.java110.cust.bmo.govOldPersonType.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.cust.bmo.govOldPersonType.IDeleteGovOldPersonTypeBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.vo.ResultVo;
import com.java110.po.govOldPersonType.GovOldPersonTypePo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.cust.IGovOldPersonTypeInnerServiceSMO;
import com.java110.dto.govOldPersonType.GovOldPersonTypeDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("deleteGovOldPersonTypeBMOImpl")
public class DeleteGovOldPersonTypeBMOImpl implements IDeleteGovOldPersonTypeBMO {

    @Autowired
    private IGovOldPersonTypeInnerServiceSMO govOldPersonTypeInnerServiceSMOImpl;

    /**
     * @param govOldPersonTypePo 数据
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> delete(GovOldPersonTypePo govOldPersonTypePo) {

        int flag = govOldPersonTypeInnerServiceSMOImpl.deleteGovOldPersonType(govOldPersonTypePo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
