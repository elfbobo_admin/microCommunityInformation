package com.java110.cust.dao;


import com.java110.utils.exception.DAOException;


import java.util.List;
import java.util.Map;

/**
 * 校园周边重点人员组件内部之间使用，没有给外围系统提供服务能力
 * 校园周边重点人员服务接口类，要求全部以字符串传输，方便微服务化
 * 新建客户，修改客户，删除客户，查询客户等功能
 *
 * Created by wuxw on 2016/12/27.
 */
public interface IGovSchoolPeripheralPersonServiceDao {


    /**
     * 保存 校园周边重点人员信息
     * @param info
     * @throws DAOException DAO异常
     */
    void saveGovSchoolPeripheralPersonInfo(Map info) throws DAOException;




    /**
     * 查询校园周边重点人员信息（instance过程）
     * 根据bId 查询校园周边重点人员信息
     * @param info bId 信息
     * @return 校园周边重点人员信息
     * @throws DAOException DAO异常
     */
    List<Map> getGovSchoolPeripheralPersonInfo(Map info) throws DAOException;



    /**
     * 修改校园周边重点人员信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    void updateGovSchoolPeripheralPersonInfo(Map info) throws DAOException;


    /**
     * 查询校园周边重点人员总数
     *
     * @param info 校园周边重点人员信息
     * @return 校园周边重点人员数量
     */
    int queryGovSchoolPeripheralPersonsCount(Map info);

}
