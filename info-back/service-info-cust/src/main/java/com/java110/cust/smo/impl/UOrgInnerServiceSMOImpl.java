package com.java110.cust.smo.impl;


import com.java110.cust.dao.IUOrgServiceDao;
import com.java110.intf.cust.IUOrgInnerServiceSMO;
import com.java110.dto.uOrg.UOrgDto;
import com.java110.po.uOrg.UOrgPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 组织管理内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class UOrgInnerServiceSMOImpl extends BaseServiceSMO implements IUOrgInnerServiceSMO {

    @Autowired
    private IUOrgServiceDao uOrgServiceDaoImpl;


    @Override
    public int saveUOrg(@RequestBody  UOrgPo uOrgPo) {
        int saveFlag = 1;
        uOrgServiceDaoImpl.saveUOrgInfo(BeanConvertUtil.beanCovertMap(uOrgPo));
        return saveFlag;
    }

     @Override
    public int updateUOrg(@RequestBody  UOrgPo uOrgPo) {
        int saveFlag = 1;
         uOrgServiceDaoImpl.updateUOrgInfo(BeanConvertUtil.beanCovertMap(uOrgPo));
        return saveFlag;
    }

     @Override
    public int deleteUOrg(@RequestBody  UOrgPo uOrgPo) {
        int saveFlag = 1;
        uOrgPo.setStatusCd("1");
        uOrgServiceDaoImpl.updateUOrgInfo(BeanConvertUtil.beanCovertMap(uOrgPo));
        return saveFlag;
    }

    @Override
    public List<UOrgDto> queryUOrgs(@RequestBody  UOrgDto uOrgDto) {

        //校验是否传了 分页信息

        int page = uOrgDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            uOrgDto.setPage((page - 1) * uOrgDto.getRow());
        }

        List<UOrgDto> uOrgs = BeanConvertUtil.covertBeanList(uOrgServiceDaoImpl.getUOrgInfo(BeanConvertUtil.beanCovertMap(uOrgDto)), UOrgDto.class);

        return uOrgs;
    }


    @Override
    public int queryUOrgsCount(@RequestBody UOrgDto uOrgDto) {
        return uOrgServiceDaoImpl.queryUOrgsCount(BeanConvertUtil.beanCovertMap(uOrgDto));    }

    public IUOrgServiceDao getUOrgServiceDaoImpl() {
        return uOrgServiceDaoImpl;
    }

    public void setUOrgServiceDaoImpl(IUOrgServiceDao uOrgServiceDaoImpl) {
        this.uOrgServiceDaoImpl = uOrgServiceDaoImpl;
    }
}
