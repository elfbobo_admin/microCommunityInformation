package com.java110.cust.bmo.govHomicideBasic;
import org.springframework.http.ResponseEntity;
import com.java110.dto.govHomicideBasic.GovHomicideBasicDto;
public interface IGetGovHomicideBasicBMO {


    /**
     * 查询命案基本信息
     * add by wuxw
     * @param  govHomicideBasicDto
     * @return
     */
    ResponseEntity<String> get(GovHomicideBasicDto govHomicideBasicDto);


}
