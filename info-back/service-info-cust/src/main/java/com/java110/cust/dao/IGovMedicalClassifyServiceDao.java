package com.java110.cust.dao;


import com.java110.utils.exception.DAOException;


import java.util.List;
import java.util.Map;

/**
 * 医疗分类组件内部之间使用，没有给外围系统提供服务能力
 * 医疗分类服务接口类，要求全部以字符串传输，方便微服务化
 * 新建客户，修改客户，删除客户，查询客户等功能
 *
 * Created by wuxw on 2016/12/27.
 */
public interface IGovMedicalClassifyServiceDao {


    /**
     * 保存 医疗分类信息
     * @param info
     * @throws DAOException DAO异常
     */
    void saveGovMedicalClassifyInfo(Map info) throws DAOException;




    /**
     * 查询医疗分类信息（instance过程）
     * 根据bId 查询医疗分类信息
     * @param info bId 信息
     * @return 医疗分类信息
     * @throws DAOException DAO异常
     */
    List<Map> getGovMedicalClassifyInfo(Map info) throws DAOException;



    /**
     * 修改医疗分类信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    void updateGovMedicalClassifyInfo(Map info) throws DAOException;


    /**
     * 查询医疗分类总数
     *
     * @param info 医疗分类信息
     * @return 医疗分类数量
     */
    int queryGovMedicalClassifysCount(Map info);

}
