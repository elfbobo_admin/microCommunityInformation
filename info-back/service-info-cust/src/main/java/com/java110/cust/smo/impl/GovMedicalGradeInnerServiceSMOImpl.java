package com.java110.cust.smo.impl;


import com.java110.cust.dao.IGovMedicalGradeServiceDao;
import com.java110.intf.cust.IGovMedicalGradeInnerServiceSMO;
import com.java110.dto.govMedicalGrade.GovMedicalGradeDto;
import com.java110.po.govMedicalGrade.GovMedicalGradePo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 医疗分级内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovMedicalGradeInnerServiceSMOImpl extends BaseServiceSMO implements IGovMedicalGradeInnerServiceSMO {

    @Autowired
    private IGovMedicalGradeServiceDao govMedicalGradeServiceDaoImpl;


    @Override
    public int saveGovMedicalGrade(@RequestBody  GovMedicalGradePo govMedicalGradePo) {
        int saveFlag = 1;
        govMedicalGradeServiceDaoImpl.saveGovMedicalGradeInfo(BeanConvertUtil.beanCovertMap(govMedicalGradePo));
        return saveFlag;
    }

     @Override
    public int updateGovMedicalGrade(@RequestBody  GovMedicalGradePo govMedicalGradePo) {
        int saveFlag = 1;
         govMedicalGradeServiceDaoImpl.updateGovMedicalGradeInfo(BeanConvertUtil.beanCovertMap(govMedicalGradePo));
        return saveFlag;
    }

     @Override
    public int deleteGovMedicalGrade(@RequestBody  GovMedicalGradePo govMedicalGradePo) {
        int saveFlag = 1;
        govMedicalGradePo.setStatusCd("1");
        govMedicalGradeServiceDaoImpl.updateGovMedicalGradeInfo(BeanConvertUtil.beanCovertMap(govMedicalGradePo));
        return saveFlag;
    }

    @Override
    public List<GovMedicalGradeDto> queryGovMedicalGrades(@RequestBody  GovMedicalGradeDto govMedicalGradeDto) {

        //校验是否传了 分页信息

        int page = govMedicalGradeDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govMedicalGradeDto.setPage((page - 1) * govMedicalGradeDto.getRow());
        }

        List<GovMedicalGradeDto> govMedicalGrades = BeanConvertUtil.covertBeanList(govMedicalGradeServiceDaoImpl.getGovMedicalGradeInfo(BeanConvertUtil.beanCovertMap(govMedicalGradeDto)), GovMedicalGradeDto.class);

        return govMedicalGrades;
    }


    @Override
    public int queryGovMedicalGradesCount(@RequestBody GovMedicalGradeDto govMedicalGradeDto) {
        return govMedicalGradeServiceDaoImpl.queryGovMedicalGradesCount(BeanConvertUtil.beanCovertMap(govMedicalGradeDto));    }

    public IGovMedicalGradeServiceDao getGovMedicalGradeServiceDaoImpl() {
        return govMedicalGradeServiceDaoImpl;
    }

    public void setGovMedicalGradeServiceDaoImpl(IGovMedicalGradeServiceDao govMedicalGradeServiceDaoImpl) {
        this.govMedicalGradeServiceDaoImpl = govMedicalGradeServiceDaoImpl;
    }
}
