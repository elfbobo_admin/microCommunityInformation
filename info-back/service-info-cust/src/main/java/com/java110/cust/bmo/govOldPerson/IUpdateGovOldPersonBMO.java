package com.java110.cust.bmo.govOldPerson;
import com.alibaba.fastjson.JSONObject;
import com.java110.po.govOldPerson.GovOldPersonPo;
import com.java110.po.govPerson.GovPersonPo;
import org.springframework.http.ResponseEntity;
import com.java110.po.store.StorePo;

public interface IUpdateGovOldPersonBMO {


    /**
     * 修改老人管理
     * add by wuxw
     * @param govOldPersonPo
     * @return
     */
    ResponseEntity<String> update(GovOldPersonPo govOldPersonPo, GovPersonPo govPersonPo,JSONObject reqJson);


}
