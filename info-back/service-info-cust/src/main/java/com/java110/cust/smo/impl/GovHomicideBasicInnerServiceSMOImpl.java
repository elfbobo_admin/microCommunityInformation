package com.java110.cust.smo.impl;


import com.java110.cust.dao.IGovHomicideBasicServiceDao;
import com.java110.intf.cust.IGovHomicideBasicInnerServiceSMO;
import com.java110.dto.govHomicideBasic.GovHomicideBasicDto;
import com.java110.po.govHomicideBasic.GovHomicideBasicPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 命案基本信息内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovHomicideBasicInnerServiceSMOImpl extends BaseServiceSMO implements IGovHomicideBasicInnerServiceSMO {

    @Autowired
    private IGovHomicideBasicServiceDao govHomicideBasicServiceDaoImpl;


    @Override
    public int saveGovHomicideBasic(@RequestBody  GovHomicideBasicPo govHomicideBasicPo) {
        int saveFlag = 1;
        govHomicideBasicServiceDaoImpl.saveGovHomicideBasicInfo(BeanConvertUtil.beanCovertMap(govHomicideBasicPo));
        return saveFlag;
    }

     @Override
    public int updateGovHomicideBasic(@RequestBody  GovHomicideBasicPo govHomicideBasicPo) {
        int saveFlag = 1;
         govHomicideBasicServiceDaoImpl.updateGovHomicideBasicInfo(BeanConvertUtil.beanCovertMap(govHomicideBasicPo));
        return saveFlag;
    }

     @Override
    public int deleteGovHomicideBasic(@RequestBody  GovHomicideBasicPo govHomicideBasicPo) {
        int saveFlag = 1;
        govHomicideBasicPo.setStatusCd("1");
        govHomicideBasicServiceDaoImpl.updateGovHomicideBasicInfo(BeanConvertUtil.beanCovertMap(govHomicideBasicPo));
        return saveFlag;
    }

    @Override
    public List<GovHomicideBasicDto> queryGovHomicideBasics(@RequestBody  GovHomicideBasicDto govHomicideBasicDto) {

        //校验是否传了 分页信息

        int page = govHomicideBasicDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govHomicideBasicDto.setPage((page - 1) * govHomicideBasicDto.getRow());
        }

        List<GovHomicideBasicDto> govHomicideBasics = BeanConvertUtil.covertBeanList(govHomicideBasicServiceDaoImpl.getGovHomicideBasicInfo(BeanConvertUtil.beanCovertMap(govHomicideBasicDto)), GovHomicideBasicDto.class);

        return govHomicideBasics;
    }


    @Override
    public int queryGovHomicideBasicsCount(@RequestBody GovHomicideBasicDto govHomicideBasicDto) {
        return govHomicideBasicServiceDaoImpl.queryGovHomicideBasicsCount(BeanConvertUtil.beanCovertMap(govHomicideBasicDto));    }

    public IGovHomicideBasicServiceDao getGovHomicideBasicServiceDaoImpl() {
        return govHomicideBasicServiceDaoImpl;
    }

    public void setGovHomicideBasicServiceDaoImpl(IGovHomicideBasicServiceDao govHomicideBasicServiceDaoImpl) {
        this.govHomicideBasicServiceDaoImpl = govHomicideBasicServiceDaoImpl;
    }
}
