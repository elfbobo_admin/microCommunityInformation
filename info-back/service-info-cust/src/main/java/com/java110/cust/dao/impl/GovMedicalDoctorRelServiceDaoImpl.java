package com.java110.cust.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.cust.dao.IGovMedicalDoctorRelServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 档案医生服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govMedicalDoctorRelServiceDaoImpl")
//@Transactional
public class GovMedicalDoctorRelServiceDaoImpl extends BaseServiceDao implements IGovMedicalDoctorRelServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovMedicalDoctorRelServiceDaoImpl.class);





    /**
     * 保存档案医生信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovMedicalDoctorRelInfo(Map info) throws DAOException {
        logger.debug("保存档案医生信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govMedicalDoctorRelServiceDaoImpl.saveGovMedicalDoctorRelInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存档案医生信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询档案医生信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovMedicalDoctorRelInfo(Map info) throws DAOException {
        logger.debug("查询档案医生信息 入参 info : {}",info);

        List<Map> businessGovMedicalDoctorRelInfos = sqlSessionTemplate.selectList("govMedicalDoctorRelServiceDaoImpl.getGovMedicalDoctorRelInfo",info);

        return businessGovMedicalDoctorRelInfos;
    }


    /**
     * 修改档案医生信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovMedicalDoctorRelInfo(Map info) throws DAOException {
        logger.debug("修改档案医生信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govMedicalDoctorRelServiceDaoImpl.updateGovMedicalDoctorRelInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改档案医生信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询档案医生数量
     * @param info 档案医生信息
     * @return 档案医生数量
     */
    @Override
    public int queryGovMedicalDoctorRelsCount(Map info) {
        logger.debug("查询档案医生数据 入参 info : {}",info);

        List<Map> businessGovMedicalDoctorRelInfos = sqlSessionTemplate.selectList("govMedicalDoctorRelServiceDaoImpl.queryGovMedicalDoctorRelsCount", info);
        if (businessGovMedicalDoctorRelInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovMedicalDoctorRelInfos.get(0).get("count").toString());
    }


}
