package com.java110.cust.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govHealthAnswer.GovHealthAnswerDto;
import com.java110.po.govHealthAnswer.GovHealthAnswerPo;
import com.java110.cust.bmo.govHealthAnswer.IDeleteGovHealthAnswerBMO;
import com.java110.cust.bmo.govHealthAnswer.IGetGovHealthAnswerBMO;
import com.java110.cust.bmo.govHealthAnswer.ISaveGovHealthAnswerBMO;
import com.java110.cust.bmo.govHealthAnswer.IUpdateGovHealthAnswerBMO;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govHealthAnswer")
public class GovHealthAnswerApi {

    @Autowired
    private ISaveGovHealthAnswerBMO saveGovHealthAnswerBMOImpl;
    @Autowired
    private IUpdateGovHealthAnswerBMO updateGovHealthAnswerBMOImpl;
    @Autowired
    private IDeleteGovHealthAnswerBMO deleteGovHealthAnswerBMOImpl;

    @Autowired
    private IGetGovHealthAnswerBMO getGovHealthAnswerBMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govHealthAnswer/saveGovHealthAnswer
     * @path /app/govHealthAnswer/saveGovHealthAnswer
     */
    @RequestMapping(value = "/saveGovHealthAnswer", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovHealthAnswer(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "healthId", "请求报文中未包含healthId");
        Assert.hasKeyAndValue(reqJson, "personId", "请求报文中未包含personId");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");


        GovHealthAnswerPo govHealthAnswerPo = BeanConvertUtil.covertBean(reqJson, GovHealthAnswerPo.class);
        return saveGovHealthAnswerBMOImpl.save(govHealthAnswerPo);
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govHealthAnswer/updateGovHealthAnswer
     * @path /app/govHealthAnswer/updateGovHealthAnswer
     */
    @RequestMapping(value = "/updateGovHealthAnswer", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovHealthAnswer(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "healthPersonId", "请求报文中未包含healthPersonId");
        Assert.hasKeyAndValue(reqJson, "healthId", "请求报文中未包含healthId");
        Assert.hasKeyAndValue(reqJson, "personId", "请求报文中未包含personId");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");


        GovHealthAnswerPo govHealthAnswerPo = BeanConvertUtil.covertBean(reqJson, GovHealthAnswerPo.class);
        return updateGovHealthAnswerBMOImpl.update(govHealthAnswerPo);
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govHealthAnswer/deleteGovHealthAnswer
     * @path /app/govHealthAnswer/deleteGovHealthAnswer
     */
    @RequestMapping(value = "/deleteGovHealthAnswer", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovHealthAnswer(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "communityId", "小区ID不能为空");

        Assert.hasKeyAndValue(reqJson, "healthPersonId", "healthPersonId不能为空");


        GovHealthAnswerPo govHealthAnswerPo = BeanConvertUtil.covertBean(reqJson, GovHealthAnswerPo.class);
        return deleteGovHealthAnswerBMOImpl.delete(govHealthAnswerPo);
    }

    /**
     * 微信删除消息模板
     *
     * @param caId 小区ID
     * @return
     * @serviceCode /govHealthAnswer/queryGovHealthAnswer
     * @path /app/govHealthAnswer/queryGovHealthAnswer
     */
    @RequestMapping(value = "/queryGovHealthAnswer", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovHealthAnswer(@RequestParam(value = "caId") String caId,
                                                       @RequestParam(value = "page") int page,
                                                       @RequestParam(value = "personId",required = false) String personId,
                                                       @RequestParam(value = "healthId",required = false) String healthId,
                                                       @RequestParam(value = "row") int row) {
        GovHealthAnswerDto govHealthAnswerDto = new GovHealthAnswerDto();
        govHealthAnswerDto.setPage(page);
        govHealthAnswerDto.setRow(row);
        govHealthAnswerDto.setCaId(caId);
        govHealthAnswerDto.setHealthId(healthId);
        govHealthAnswerDto.setPersonId(personId);
        return getGovHealthAnswerBMOImpl.get(govHealthAnswerDto);
    }
}
