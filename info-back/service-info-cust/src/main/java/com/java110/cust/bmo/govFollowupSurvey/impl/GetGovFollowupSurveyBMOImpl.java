package com.java110.cust.bmo.govFollowupSurvey.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.cust.bmo.govFollowupSurvey.IGetGovFollowupSurveyBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.cust.IGovFollowupSurveyInnerServiceSMO;
import com.java110.dto.govFollowupSurvey.GovFollowupSurveyDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import org.springframework.http.HttpStatus;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service("getGovFollowupSurveyBMOImpl")
public class GetGovFollowupSurveyBMOImpl implements IGetGovFollowupSurveyBMO {

    @Autowired
    private IGovFollowupSurveyInnerServiceSMO govFollowupSurveyInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govFollowupSurveyDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovFollowupSurveyDto govFollowupSurveyDto) {


        int count = govFollowupSurveyInnerServiceSMOImpl.queryGovFollowupSurveysCount(govFollowupSurveyDto);

        List<GovFollowupSurveyDto> govFollowupSurveyDtos = null;
        if (count > 0) {
            govFollowupSurveyDtos = govFollowupSurveyInnerServiceSMOImpl.queryGovFollowupSurveys(govFollowupSurveyDto);
        } else {
            govFollowupSurveyDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govFollowupSurveyDto.getRow()), count, govFollowupSurveyDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
