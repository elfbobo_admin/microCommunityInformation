package com.java110.cust.bmo.govHealthAnswerValue.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.java110.cust.bmo.govHealthAnswerValue.ISaveGovHealthAnswerValueBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.dto.govHealthAnswerValue.GovHealthAnswerValueDto;
import com.java110.dto.govHealthTitleValue.GovHealthTitleValueDto;
import com.java110.intf.cust.IGovHealthAnswerInnerServiceSMO;
import com.java110.intf.cust.IGovHealthTitleValueInnerServiceSMO;
import com.java110.po.govHealthAnswer.GovHealthAnswerPo;
import com.java110.utils.util.StringUtil;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.po.govHealthAnswerValue.GovHealthAnswerValuePo;
import com.java110.intf.cust.IGovHealthAnswerValueInnerServiceSMO;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("saveGovHealthAnswerValueBMOImpl")
public class SaveGovHealthAnswerValueBMOImpl implements ISaveGovHealthAnswerValueBMO {

    @Autowired
    private IGovHealthAnswerValueInnerServiceSMO govHealthAnswerValueInnerServiceSMOImpl;
    @Autowired
    private IGovHealthAnswerInnerServiceSMO govHealthAnswerInnerServiceSMOImpl;
    @Autowired
    private IGovHealthTitleValueInnerServiceSMO govHealthTitleValueInnerServiceSMOImpl;
    /**
     * 添加小区信息
     *
     * @param
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> save(JSONObject reqJson) {

        GovHealthAnswerPo govInfoAnswerPo = new GovHealthAnswerPo();
        govInfoAnswerPo.setHealthPersonId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_healthPersonId));
        govInfoAnswerPo.setHealthId(reqJson.getString("healthId"));
        govInfoAnswerPo.setPersonId(reqJson.getString("personId"));
        govInfoAnswerPo.setCaId(reqJson.getString("caId"));
        govInfoAnswerPo.setState("1202");
        int flag = govHealthAnswerInnerServiceSMOImpl.saveGovHealthAnswer(govInfoAnswerPo);
        if (flag < 1) {
            return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存失败");
        }
        JSONArray questionAnswerTitles = reqJson.getJSONArray("questionAnswerTitles");
        String doctor = reqJson.getString("doctor");
        String doctorRemark = reqJson.getString("doctorRemark");

        if (questionAnswerTitles == null || questionAnswerTitles.size() < 1) {
            throw new IllegalArgumentException("未包含题目及答案");
        }
        JSONObject titleObj = null;
        for (int questionAnswerTitleIndex = 0; questionAnswerTitleIndex < questionAnswerTitles.size(); questionAnswerTitleIndex++) {
            titleObj = questionAnswerTitles.getJSONObject(questionAnswerTitleIndex);
            String titleType = titleObj.getString("titleType");
            if(GovHealthAnswerValueDto.TITLETYPE_CHECKBOX.equals(titleType)){
                JSONArray valueContent = titleObj.getJSONArray("valueContent");
                if(valueContent == null || valueContent.size() < 1){
                    throw new IllegalArgumentException("多选题目未包含答案");
                }
                for(int checkBoxIndex = 0;checkBoxIndex<valueContent.size();checkBoxIndex++){
                    GovHealthAnswerValuePo govInfoAnswerValuePo = new GovHealthAnswerValuePo();
                    govInfoAnswerValuePo.setPersonTitleId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_personTitleId));
                    govInfoAnswerValuePo.setPersonId(govInfoAnswerPo.getPersonId());
                    govInfoAnswerValuePo.setValueId(valueContent.getString(checkBoxIndex));
                    govInfoAnswerValuePo.setHealthId(govInfoAnswerPo.getHealthId());
                    govInfoAnswerValuePo.setHealthPersonId(govInfoAnswerPo.getHealthPersonId());
                    govInfoAnswerValuePo.setCaId(titleObj.getString("caId"));
                    govInfoAnswerValuePo.setDoctor( doctor );
                    govInfoAnswerValuePo.setDoctorRemark( doctorRemark );
                    govInfoAnswerValuePo.setTitleId(titleObj.getString("titleId"));

                    GovHealthTitleValueDto govInfoSettingTitleValueDto = new GovHealthTitleValueDto();


                    govInfoSettingTitleValueDto.setCaId(titleObj.getString("caId"));
                    govInfoSettingTitleValueDto.setTitleId(titleObj.getString("titleId"));
                    govInfoSettingTitleValueDto.setValueId(valueContent.getString(checkBoxIndex));
                    List<GovHealthTitleValueDto> govInfoSettingTitleValueDtos = govHealthTitleValueInnerServiceSMOImpl.queryGovHealthTitleValues(govInfoSettingTitleValueDto);
                    if(govInfoSettingTitleValueDtos == null || govInfoSettingTitleValueDtos.size() < 1){
                        throw new IllegalArgumentException("多选题目未查询到答案信息");
                    }
                    if (valueContent.getString(checkBoxIndex).equals(govInfoSettingTitleValueDtos.get(0).getValueId())) {
                        govInfoAnswerValuePo.setValueContent(govInfoSettingTitleValueDtos.get(0).getValue());
                    }
                    flag = govHealthAnswerValueInnerServiceSMOImpl.saveGovHealthAnswerValue(govInfoAnswerValuePo);
                    if (flag < 1) {
                        throw new IllegalArgumentException("保存多选题目答案失败");
                    }
                }
            }
            if(GovHealthAnswerValueDto.TITLETYPE_RADIO.equals(titleType)){
                String valueContent = titleObj.getString("valueContent");
                if(valueContent == null || "".equals(valueContent)){
                    throw new IllegalArgumentException("单选题目未包含答案");
                }
                GovHealthAnswerValuePo govInfoAnswerValuePo = new GovHealthAnswerValuePo();

                govInfoAnswerValuePo.setPersonTitleId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_personTitleId));
                govInfoAnswerValuePo.setPersonId(govInfoAnswerPo.getPersonId());
                govInfoAnswerValuePo.setValueId(valueContent);
                govInfoAnswerValuePo.setHealthId(govInfoAnswerPo.getHealthId());
                govInfoAnswerValuePo.setHealthPersonId(govInfoAnswerPo.getHealthPersonId());
                govInfoAnswerValuePo.setCaId(titleObj.getString("caId"));
                govInfoAnswerValuePo.setTitleId(titleObj.getString("titleId"));
                govInfoAnswerValuePo.setDoctor( doctor );
                govInfoAnswerValuePo.setDoctorRemark( doctorRemark );

                GovHealthTitleValueDto govInfoSettingTitleValueDto = new GovHealthTitleValueDto();
                govInfoSettingTitleValueDto.setCaId(titleObj.getString("caId"));
                govInfoSettingTitleValueDto.setTitleId(titleObj.getString("titleId"));
                govInfoSettingTitleValueDto.setValueId(valueContent);
                List<GovHealthTitleValueDto> govInfoSettingTitleValueDtos = govHealthTitleValueInnerServiceSMOImpl.queryGovHealthTitleValues(govInfoSettingTitleValueDto);
                if(govInfoSettingTitleValueDtos == null || govInfoSettingTitleValueDtos.size() < 1){
                    throw new IllegalArgumentException("单选题目未查询到答案信息");
                }
                if (valueContent.equals(govInfoSettingTitleValueDtos.get(0).getValueId())) {
                    govInfoAnswerValuePo.setValueContent(govInfoSettingTitleValueDtos.get(0).getValue());
                }
                flag = govHealthAnswerValueInnerServiceSMOImpl.saveGovHealthAnswerValue(govInfoAnswerValuePo);
                if (flag < 1) {
                    throw new IllegalArgumentException("单选题目保存失败");
                }
            }
            if(GovHealthAnswerValueDto.ITLETYPE_TEXTAREA.equals(titleType)){
                String valueContent = titleObj.getString("valueContent");
                if(valueContent == null || "".equals(valueContent)){
                    throw new IllegalArgumentException("简答题未包含答案");
                }
                GovHealthAnswerValuePo govInfoAnswerValuePo = new GovHealthAnswerValuePo();

                govInfoAnswerValuePo.setPersonTitleId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_personTitleId));
                govInfoAnswerValuePo.setPersonId(govInfoAnswerPo.getPersonId());
                govInfoAnswerValuePo.setHealthId(govInfoAnswerPo.getHealthId());
                govInfoAnswerValuePo.setHealthPersonId(govInfoAnswerPo.getHealthPersonId());
                govInfoAnswerValuePo.setCaId(titleObj.getString("caId"));
                govInfoAnswerValuePo.setTitleId(titleObj.getString("titleId"));
                govInfoAnswerValuePo.setValueId("-1");
                govInfoAnswerValuePo.setValueContent(valueContent);
                govInfoAnswerValuePo.setDoctor( doctor );
                govInfoAnswerValuePo.setDoctorRemark( doctorRemark );

                flag = govHealthAnswerValueInnerServiceSMOImpl.saveGovHealthAnswerValue(govInfoAnswerValuePo);
                if (flag < 1) {
                    throw new IllegalArgumentException("简答题保存失败");
                }
            }
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
    }

}
