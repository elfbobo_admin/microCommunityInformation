package com.java110.cust.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.cust.dao.IMachineStaffShowServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 摄像头员工关系服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("machineStaffShowServiceDaoImpl")
//@Transactional
public class MachineStaffShowServiceDaoImpl extends BaseServiceDao implements IMachineStaffShowServiceDao {

    private static Logger logger = LoggerFactory.getLogger(MachineStaffShowServiceDaoImpl.class);





    /**
     * 保存摄像头员工关系信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveMachineStaffShowInfo(Map info) throws DAOException {
        logger.debug("保存摄像头员工关系信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("machineStaffShowServiceDaoImpl.saveMachineStaffShowInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存摄像头员工关系信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询摄像头员工关系信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getMachineStaffShowInfo(Map info) throws DAOException {
        logger.debug("查询摄像头员工关系信息 入参 info : {}",info);

        List<Map> businessMachineStaffShowInfos = sqlSessionTemplate.selectList("machineStaffShowServiceDaoImpl.getMachineStaffShowInfo",info);

        return businessMachineStaffShowInfos;
    }


    /**
     * 修改摄像头员工关系信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateMachineStaffShowInfo(Map info) throws DAOException {
        logger.debug("修改摄像头员工关系信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("machineStaffShowServiceDaoImpl.updateMachineStaffShowInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改摄像头员工关系信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询摄像头员工关系数量
     * @param info 摄像头员工关系信息
     * @return 摄像头员工关系数量
     */
    @Override
    public int queryMachineStaffShowsCount(Map info) {
        logger.debug("查询摄像头员工关系数据 入参 info : {}",info);

        List<Map> businessMachineStaffShowInfos = sqlSessionTemplate.selectList("machineStaffShowServiceDaoImpl.queryMachineStaffShowsCount", info);
        if (businessMachineStaffShowInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessMachineStaffShowInfos.get(0).get("count").toString());
    }


}
