package com.java110.cust.bmo.govGrid.impl;

import com.java110.cust.bmo.govGrid.IGetGovGridBMO;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.cust.IGovGridInnerServiceSMO;
import com.java110.dto.govGrid.GovGridDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import org.springframework.http.HttpStatus;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service("getGovGridBMOImpl")
public class GetGovGridBMOImpl implements IGetGovGridBMO {

    @Autowired
    private IGovGridInnerServiceSMO govGridInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govGridDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovGridDto govGridDto) {


        int count = govGridInnerServiceSMOImpl.queryGovGridsCount(govGridDto);

        List<GovGridDto> govGridDtos = null;
        if (count > 0) {
            govGridDtos = govGridInnerServiceSMOImpl.queryGovGrids(govGridDto);
        } else {
            govGridDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govGridDto.getRow()), count, govGridDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
