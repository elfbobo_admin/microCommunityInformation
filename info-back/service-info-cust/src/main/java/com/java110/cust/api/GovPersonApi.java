package com.java110.cust.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.cust.bmo.govPerson.IDeleteGovPersonBMO;
import com.java110.cust.bmo.govPerson.IGetGovPersonBMO;
import com.java110.cust.bmo.govPerson.ISaveGovPersonBMO;
import com.java110.cust.bmo.govPerson.IUpdateGovPersonBMO;
import com.java110.dto.govPerson.GovPersonDto;
import com.java110.po.govPerson.GovPersonPo;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govPerson")
public class GovPersonApi {

    @Autowired
    private ISaveGovPersonBMO saveGovPersonBMOImpl;
    @Autowired
    private IUpdateGovPersonBMO updateGovPersonBMOImpl;
    @Autowired
    private IDeleteGovPersonBMO deleteGovPersonBMOImpl;

    @Autowired
    private IGetGovPersonBMO getGovPersonBMOImpl;

    /**
     * 微信保存消息模板
     * @serviceCode /govPerson/saveGovPerson
     * @path /app/govPerson/saveGovPerson
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/saveGovPerson", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovPerson(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "personType", "请求报文中未包含personType");
        Assert.hasKeyAndValue(reqJson, "idType", "请求报文中未包含idType");
        Assert.hasKeyAndValue(reqJson, "idCard", "请求报文中未包含idCard");
        Assert.hasKeyAndValue(reqJson, "personName", "请求报文中未包含personName");
        Assert.hasKeyAndValue(reqJson, "personTel", "请求报文中未包含personTel");
        Assert.hasKeyAndValue(reqJson, "personSex", "请求报文中未包含personSex");
        Assert.hasKeyAndValue(reqJson, "birthday", "请求报文中未包含birthday");
        Assert.hasKeyAndValue(reqJson, "nation", "请求报文中未包含nation");
        Assert.hasKeyAndValue(reqJson, "nativePlace", "请求报文中未包含nativePlace");
        Assert.hasKeyAndValue(reqJson, "politicalOutlook", "请求报文中未包含politicalOutlook");
        Assert.hasKeyAndValue(reqJson, "maritalStatus", "请求报文中未包含maritalStatus");


        GovPersonPo govPersonPo = BeanConvertUtil.covertBean(reqJson, GovPersonPo.class);
        return saveGovPersonBMOImpl.save(govPersonPo);
    }



    /**
     * 微信保存消息模板
     * @serviceCode /govPerson/saveGovPerson
     * @path /app/govPerson/saveGovPerson
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/saveGovPetitionPerson", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovPetitionPerson(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "personType", "请求报文中未包含personType");
        Assert.hasKeyAndValue(reqJson, "idType", "请求报文中未包含idType");
        Assert.hasKeyAndValue(reqJson, "idCard", "请求报文中未包含idCard");
        Assert.hasKeyAndValue(reqJson, "personName", "请求报文中未包含personName");
        Assert.hasKeyAndValue(reqJson, "personTel", "请求报文中未包含personTel");
        Assert.hasKeyAndValue(reqJson, "personSex", "请求报文中未包含personSex");
        Assert.hasKeyAndValue(reqJson, "birthday", "请求报文中未包含birthday");
        Assert.hasKeyAndValue(reqJson, "nation", "请求报文中未包含nation");
        Assert.hasKeyAndValue(reqJson, "nativePlace", "请求报文中未包含nativePlace");
        Assert.hasKeyAndValue(reqJson, "politicalOutlook", "请求报文中未包含politicalOutlook");
        Assert.hasKeyAndValue(reqJson, "maritalStatus", "请求报文中未包含maritalStatus");


        GovPersonPo govPersonPo = BeanConvertUtil.covertBean(reqJson, GovPersonPo.class);
        return saveGovPersonBMOImpl.saveGovPetition(govPersonPo);
    }
    /**
     * 微信修改消息模板
     * @serviceCode /govPerson/updateGovPerson
     * @path /app/govPerson/updateGovPerson
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/updateGovPerson", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovPerson(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "personType", "请求报文中未包含personType");
        Assert.hasKeyAndValue(reqJson, "idType", "请求报文中未包含idType");
        Assert.hasKeyAndValue(reqJson, "idCard", "请求报文中未包含idCard");
        Assert.hasKeyAndValue(reqJson, "personName", "请求报文中未包含personName");
        Assert.hasKeyAndValue(reqJson, "personTel", "请求报文中未包含personTel");
        Assert.hasKeyAndValue(reqJson, "personSex", "请求报文中未包含personSex");
        Assert.hasKeyAndValue(reqJson, "birthday", "请求报文中未包含birthday");
        Assert.hasKeyAndValue(reqJson, "nation", "请求报文中未包含nation");
        Assert.hasKeyAndValue(reqJson, "nativePlace", "请求报文中未包含nativePlace");
        Assert.hasKeyAndValue(reqJson, "politicalOutlook", "请求报文中未包含politicalOutlook");
        Assert.hasKeyAndValue(reqJson, "maritalStatus", "请求报文中未包含maritalStatus");
        Assert.hasKeyAndValue(reqJson, "govPersonId", "govPersonId不能为空");


        GovPersonPo govPersonPo = BeanConvertUtil.covertBean(reqJson, GovPersonPo.class);
        return updateGovPersonBMOImpl.update(govPersonPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govPerson/deleteGovPerson
     * @path /app/govPerson/deleteGovPerson
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/deleteGovPerson", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovPerson(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "govPersonId", "govPersonId不能为空");


        GovPersonPo govPersonPo = BeanConvertUtil.covertBean(reqJson, GovPersonPo.class);
        return deleteGovPersonBMOImpl.delete(govPersonPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govPerson/queryGovPerson
     * @path /app/govPerson/queryGovPerson
     * @param
     * @return
     */
    @RequestMapping(value = "/queryGovPerson", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovPerson(@RequestParam(value = "caId" , required = false) String caId,
                                                      @RequestParam(value = "govPersonId" , required = false) String govPersonId,
                                                      @RequestParam(value = "personType" , required = false) String personType,
                                                      @RequestParam(value = "personName" , required = false) String personName,
                                                      @RequestParam(value = "personTel" , required = false) String personTel,
                                                      @RequestParam(value = "personSex" , required = false) String personSex,
                                                      @RequestParam(value = "personNameLike" , required = false) String personNameLike,
                                                      @RequestParam(value = "idCard" , required = false) String idCard,
                                                      @RequestParam(value = "isWeb" , required = false) String isWeb,
                                                      @RequestParam(value = "page") int page,
                                                      @RequestParam(value = "row") int row) {
        GovPersonDto govPersonDto = new GovPersonDto();
        govPersonDto.setPage(page);
        govPersonDto.setRow(row);
        govPersonDto.setCaId(caId);
        govPersonDto.setGovPersonId(govPersonId);
        govPersonDto.setPersonType(personType);
        govPersonDto.setPersonSex(personSex);
        govPersonDto.setIdCard(idCard);
        govPersonDto.setPersonName(personName);
        govPersonDto.setPersonTel(personTel);
        govPersonDto.setIsWeb(isWeb);
        govPersonDto.setPersonName( personName );
        govPersonDto.setPersonNameLike( personNameLike );
        return getGovPersonBMOImpl.get(govPersonDto);
    }

    /**
     * 获取标签人员
     * @serviceCode /govPerson/queryLabelGovPerson
     * @path /app/govPerson/queryLabelGovPerson
     * @param
     * @return
     */
    @RequestMapping(value = "/queryLabelGovPerson", method = RequestMethod.GET)
    public ResponseEntity<String> queryLabelGovPerson(@RequestParam(value = "caId" , required = false) String caId,
                                                 @RequestParam(value = "govPersonId" , required = false) String govPersonId,
                                                 @RequestParam(value = "personName" , required = false) String personName,
                                                 @RequestParam(value = "idCard" , required = false) String idCard,
                                                 @RequestParam(value = "tel" , required = false) String tel,
                                                 @RequestParam(value = "personSex" , required = false) String personSex,
                                                 @RequestParam(value = "isWeb" , required = false) String isWeb,
                                                 @RequestParam(value = "labelCd" , required = false) String labelCd,
                                                 @RequestParam(value = "labelCds" , required = false) String labelCds,
                                                 @RequestParam(value = "page") int page,
                                                 @RequestParam(value = "row") int row) {
        GovPersonDto govPersonDto = new GovPersonDto();
        govPersonDto.setPage(page);
        govPersonDto.setRow(row);
        govPersonDto.setCaId(caId);
        govPersonDto.setGovPersonId(govPersonId);
        govPersonDto.setPersonTel(tel);
        govPersonDto.setPersonSex(personSex);
        govPersonDto.setIdCard(idCard);
        govPersonDto.setIsWeb(isWeb);
        govPersonDto.setPersonName(personName);
        return getGovPersonBMOImpl.getLabelPersons(govPersonDto,labelCd,labelCds);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govPerson/queryLargeGovPerson
     * @path /app/govPerson/queryLargeGovPerson
     * @param
     * @return
     */
    @RequestMapping(value = "/queryLargeGovPerson", method = RequestMethod.GET)
    public ResponseEntity<String> queryLargeGovPersonCoun(@RequestParam(value = "caId" , required = false) String caId,
                                                      @RequestParam(value = "page") int page,
                                                      @RequestParam(value = "row") int row) {
        GovPersonDto govPersonDto = new GovPersonDto();
        govPersonDto.setPage(page);
        govPersonDto.setRow(row);
        govPersonDto.setCaId(caId);
        return getGovPersonBMOImpl.queryLargeGovPersonCoun(govPersonDto);
    }





    /**
     * 人员画像
     * @param govPersonId 人员ID
     * @return
     * @service /ownerApi/comprehensiveQuery
     * @path /app/ownerApi/comprehensiveQuery
     */
    @RequestMapping(value = "/userProfile", method = RequestMethod.GET)
    public ResponseEntity<String> userProfile(@RequestParam(value = "caId" , required = false) String caId,
                                              @RequestParam(value = "govPersonId" , required = false) String govPersonId,
                                              @RequestParam(value = "personName" , required = false) String personName,
                                              @RequestParam(value = "personType" , required = false) String personType,
                                              @RequestParam(value = "personTel" , required = false) String personTel,
                                              @RequestParam(value = "idCard" , required = false) String idCard,
                                              @RequestParam(value = "isWeb" , required = false) String isWeb,
                                              @RequestParam(value = "page") int page,
                                              @RequestParam(value = "row") int row) {
        GovPersonDto govPersonDto = new GovPersonDto();
        govPersonDto.setPage(page);
        govPersonDto.setRow(row);
        govPersonDto.setCaId(caId);
        govPersonDto.setGovPersonId(govPersonId);
        govPersonDto.setPersonType(personType);
        govPersonDto.setIdCard(idCard);
        govPersonDto.setIsWeb(isWeb);
        govPersonDto.setPersonName(personName);
        govPersonDto.setPersonTel( personTel );
        return getGovPersonBMOImpl.get(govPersonDto);
    }
    /**
     * 人员画像
     * @param
     * @return
     * @service /govPerson/queryGovPersonHomicide
     * @path
     */
    @RequestMapping(value = "/queryGovPersonHomicide", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovPersonHomicide(@RequestParam(value = "caId" , required = false) String caId,
                                              @RequestParam(value = "idCardLike" , required = false) String idCardLike,
                                              @RequestParam(value = "personNameLike" , required = false) String personNameLike,
                                              @RequestParam(value = "personTelLike" , required = false) String personTelLike,
                                              @RequestParam(value = "page") int page,
                                              @RequestParam(value = "row") int row) {
        GovPersonDto govPersonDto = new GovPersonDto();
        govPersonDto.setPage(page);
        govPersonDto.setRow(row);
        govPersonDto.setCaId(caId);
        govPersonDto.setPersonNameLike( personNameLike );
        govPersonDto.setIdCardLike( idCardLike );
        govPersonDto.setPersonTelLike( personTelLike );
        return getGovPersonBMOImpl.getGovPersonHomicide(govPersonDto);
    }
    /**
     * 人员帮扶、扶贫干部
     * @param
     * @return
     * @service /govPerson/getGovPersonHelpPolicy
     * @path
     */
    @RequestMapping(value = "/getGovPersonHelpPolicy", method = RequestMethod.GET)
    public ResponseEntity<String> getGovPersonHelpPolicy(@RequestParam(value = "caId" , required = false) String caId,
                                              @RequestParam(value = "idCardLike" , required = false) String idCardLike,
                                              @RequestParam(value = "personNameLike" , required = false) String personNameLike,
                                              @RequestParam(value = "personTelLike" , required = false) String personTelLike,
                                              @RequestParam(value = "labelCd" , required = false) String labelCd,
                                              @RequestParam(value = "labelCds" , required = false) String labelCds,
                                              @RequestParam(value = "page") int page,
                                              @RequestParam(value = "row") int row) {
        GovPersonDto govPersonDto = new GovPersonDto();
        govPersonDto.setPage(page);
        govPersonDto.setRow(row);
        govPersonDto.setCaId(caId);
        govPersonDto.setPersonNameLike( personNameLike );
        govPersonDto.setLabelCd( labelCd );
        govPersonDto.setIdCardLike( idCardLike );
        govPersonDto.setPersonTelLike( personTelLike );
        if (labelCds != null && "".equals( labelCds )) {
            govPersonDto.setLabelCds( labelCds.split( "," ) );
        }
        return getGovPersonBMOImpl.getGovPersonHelpPolicy(govPersonDto);
    }

}
