package com.java110.databus.reportData;

import com.alibaba.fastjson.JSONObject;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.dto.govCommunity.GovCommunityDto;
import com.java110.dto.govCommunityLocation.GovCommunityLocationDto;
import com.java110.dto.reportData.ReportDataDto;
import com.java110.dto.reportData.ReportDataHeaderDto;
import com.java110.intf.assets.IGovCommunityLocationInnerServiceSMO;
import com.java110.po.govCommunityLocation.GovCommunityLocationPo;
import com.java110.utils.util.Assert;
import com.java110.utils.util.DateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 保存位置 小区位置
 * <p>
 * add by  wuxw 2021-09-05
 * <p>
 * ReportAddLocationDataAdapt
 */
@Service(value = "ADD_LOCATION")
public class ReportAddLocationDataAdapt implements IReportDataAdapt {
    private final static Logger logger = LoggerFactory.getLogger(ReportAddLocationDataAdapt.class);
    @Autowired
    private IGovCommunityLocationInnerServiceSMO govCommunityLocationInnerServiceSMOImpl;

    @Override
    public ReportDataDto report(ReportDataDto reportDataDto, GovCommunityDto govCommunity) {
        logger.info(JSONObject.toJSONString(reportDataDto));

        JSONObject reqJson = reportDataDto.getReportDataBodyDto();

        Assert.hasKeyAndValue(reqJson, "name", "请求报文中未包含位置名称");

        GovCommunityLocationPo govCommunityLocationPo = new GovCommunityLocationPo();
        if (!reqJson.containsKey("extLocationId")) {
            govCommunityLocationPo.setLocationId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_govFloorId));
        } else {
            govCommunityLocationPo.setLocationId(reqJson.getString("extLocationId"));
        }
        govCommunityLocationPo.setGovCommunityId(govCommunity.getGovCommunityId());
        govCommunityLocationPo.setName(reqJson.getString("name"));
        govCommunityLocationPo.setDatasourceType(ReportDataHeaderDto.SYSTEM_COMMUNITY_TYPE);
        govCommunityLocationPo.setCaId(govCommunity.getCaId());

        GovCommunityLocationDto govCommunityLocationDto = new GovCommunityLocationDto();
        govCommunityLocationDto.setName(reqJson.getString("name"));
        govCommunityLocationDto.setCaId(govCommunity.getCaId());
        govCommunityLocationDto.setGovCommunityId(govCommunity.getGovCommunityId());
        List<GovCommunityLocationDto> govCommunityLocationDtos = govCommunityLocationInnerServiceSMOImpl.queryGovCommunityLocations(govCommunityLocationDto);
        if (govCommunityLocationDtos == null || govCommunityLocationDtos.size() < 1) {
            int flag = govCommunityLocationInnerServiceSMOImpl.saveGovCommunityLocation(govCommunityLocationPo);
            if (flag < 1) {
                throw new IllegalArgumentException("保存楼栋信息失败");
            }
        } else {
            govCommunityLocationPo.setLocationId(govCommunityLocationDtos.get(0).getLocationId());
            int flag = govCommunityLocationInnerServiceSMOImpl.updateGovCommunityLocation(govCommunityLocationPo);
            if (flag < 1) {
                throw new IllegalArgumentException("修改楼栋信息失败");
            }
        }

        ReportDataDto returnReportData = new ReportDataDto();
        ReportDataHeaderDto returnReportHeader = reportDataDto.getReportDataHeaderDto();
        returnReportHeader.setCode(ReportDataHeaderDto.CODE_SUCCESS);
        returnReportHeader.setMsg(ReportDataHeaderDto.MSG_SUCCESS);
        returnReportHeader.setResTime(DateUtil.getNow(DateUtil.DATE_FORMATE_STRING_DEFAULT));
        returnReportHeader.setReqTime(null);
        JSONObject body = new JSONObject();
        body.put("extLocationId", govCommunityLocationPo.getLocationId());
        returnReportData.setReportDataHeaderDto(returnReportHeader);
        returnReportData.setReportDataBodyDto(body);
        return returnReportData;
    }
}
