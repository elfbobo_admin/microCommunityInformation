package com.java110.intf.gov;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govActivityReply.GovActivityReplyDto;
import com.java110.po.govActivityReply.GovActivityReplyPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovActivityReplyInnerServiceSMO
 * @Description 活动评论接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-gov", configuration = {FeignConfiguration.class})
@RequestMapping("/govActivityReplyApi")
public interface IGovActivityReplyInnerServiceSMO {


    @RequestMapping(value = "/saveGovActivityReply", method = RequestMethod.POST)
    public int saveGovActivityReply(@RequestBody  GovActivityReplyPo govActivityReplyPo);

    @RequestMapping(value = "/updateGovActivityReply", method = RequestMethod.POST)
    public int updateGovActivityReply(@RequestBody  GovActivityReplyPo govActivityReplyPo);

    @RequestMapping(value = "/deleteGovActivityReply", method = RequestMethod.POST)
    public int deleteGovActivityReply(@RequestBody  GovActivityReplyPo govActivityReplyPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govActivityReplyDto 数据对象分享
     * @return GovActivityReplyDto 对象数据
     */
    @RequestMapping(value = "/queryGovActivityReplys", method = RequestMethod.POST)
    List<GovActivityReplyDto> queryGovActivityReplys(@RequestBody GovActivityReplyDto govActivityReplyDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govActivityReplyDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovActivityReplysCount", method = RequestMethod.POST)
    int queryGovActivityReplysCount(@RequestBody GovActivityReplyDto govActivityReplyDto);
}
