package com.java110.intf.cust;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govCompany.GovCompanyDto;
import com.java110.po.govCompany.GovCompanyPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovCompanyInnerServiceSMO
 * @Description 公司组织接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-cust", configuration = {FeignConfiguration.class})
@RequestMapping("/govCompanyApi")
public interface IGovCompanyInnerServiceSMO {


    @RequestMapping(value = "/saveGovCompany", method = RequestMethod.POST)
    public int saveGovCompany(@RequestBody GovCompanyPo govCompanyPo);

    @RequestMapping(value = "/updateGovCompany", method = RequestMethod.POST)
    public int updateGovCompany(@RequestBody  GovCompanyPo govCompanyPo);

    @RequestMapping(value = "/deleteGovCompany", method = RequestMethod.POST)
    public int deleteGovCompany(@RequestBody  GovCompanyPo govCompanyPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govCompanyDto 数据对象分享
     * @return GovCompanyDto 对象数据
     */
    @RequestMapping(value = "/queryGovCompanys", method = RequestMethod.POST)
    List<GovCompanyDto> queryGovCompanys(@RequestBody GovCompanyDto govCompanyDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govCompanyDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovCompanysCount", method = RequestMethod.POST)
    int queryGovCompanysCount(@RequestBody GovCompanyDto govCompanyDto);
}
