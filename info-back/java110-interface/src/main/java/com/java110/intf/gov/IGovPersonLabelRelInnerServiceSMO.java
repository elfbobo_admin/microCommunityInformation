package com.java110.intf.gov;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govPersonLabelRel.GovPersonLabelRelDto;
import com.java110.po.govPersonLabelRel.GovPersonLabelRelPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovPersonLabelRelInnerServiceSMO
 * @Description 标签与人口关系接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-gov", configuration = {FeignConfiguration.class})
@RequestMapping("/govPersonLabelRelApi")
public interface IGovPersonLabelRelInnerServiceSMO {


    @RequestMapping(value = "/saveGovPersonLabelRel", method = RequestMethod.POST)
    public int saveGovPersonLabelRel(@RequestBody  GovPersonLabelRelPo govPersonLabelRelPo);

    @RequestMapping(value = "/updateGovPersonLabelRel", method = RequestMethod.POST)
    public int updateGovPersonLabelRel(@RequestBody  GovPersonLabelRelPo govPersonLabelRelPo);

    @RequestMapping(value = "/deleteGovPersonLabelRel", method = RequestMethod.POST)
    public int deleteGovPersonLabelRel(@RequestBody  GovPersonLabelRelPo govPersonLabelRelPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govPersonLabelRelDto 数据对象分享
     * @return GovPersonLabelRelDto 对象数据
     */
    @RequestMapping(value = "/queryGovPersonLabelRels", method = RequestMethod.POST)
    List<GovPersonLabelRelDto> queryGovPersonLabelRels(@RequestBody GovPersonLabelRelDto govPersonLabelRelDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govPersonLabelRelDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovPersonLabelRelsCount", method = RequestMethod.POST)
    int queryGovPersonLabelRelsCount(@RequestBody GovPersonLabelRelDto govPersonLabelRelDto);
}
