package com.java110.intf.assets;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.cityArea.CityAreaDto;
import com.java110.po.cityArea.CityAreaPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName ICityAreaInnerServiceSMO
 * @Description 选择省市区接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-assets", configuration = {FeignConfiguration.class})
@RequestMapping("/cityAreaApi")
public interface ICityAreaInnerServiceSMO {


    @RequestMapping(value = "/saveCityArea", method = RequestMethod.POST)
    public int saveCityArea(@RequestBody  CityAreaPo cityAreaPo);

    @RequestMapping(value = "/updateCityArea", method = RequestMethod.POST)
    public int updateCityArea(@RequestBody  CityAreaPo cityAreaPo);

    @RequestMapping(value = "/deleteCityArea", method = RequestMethod.POST)
    public int deleteCityArea(@RequestBody  CityAreaPo cityAreaPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param cityAreaDto 数据对象分享
     * @return CityAreaDto 对象数据
     */
    @RequestMapping(value = "/queryCityAreas", method = RequestMethod.POST)
    List<CityAreaDto> queryCityAreas(@RequestBody CityAreaDto cityAreaDto);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param cityAreaDto 数据对象分享
     * @return CityAreaDto 对象数据
     */
    @RequestMapping(value = "/getAreas", method = RequestMethod.POST)
    List<CityAreaDto> getAreas(@RequestBody CityAreaDto cityAreaDto);


    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param cityAreaDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryCityAreasCount", method = RequestMethod.POST)
    int queryCityAreasCount(@RequestBody CityAreaDto cityAreaDto);
}
