package com.java110.intf.cust;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govOldPersonAttr.GovOldPersonAttrDto;
import com.java110.po.govOldPersonAttr.GovOldPersonAttrPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovOldPersonAttrInnerServiceSMO
 * @Description 老人属性接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-cust", configuration = {FeignConfiguration.class})
@RequestMapping("/govOldPersonAttrApi")
public interface IGovOldPersonAttrInnerServiceSMO {


    @RequestMapping(value = "/saveGovOldPersonAttr", method = RequestMethod.POST)
    public int saveGovOldPersonAttr(@RequestBody  GovOldPersonAttrPo govOldPersonAttrPo);

    @RequestMapping(value = "/updateGovOldPersonAttr", method = RequestMethod.POST)
    public int updateGovOldPersonAttr(@RequestBody  GovOldPersonAttrPo govOldPersonAttrPo);

    @RequestMapping(value = "/deleteGovOldPersonAttr", method = RequestMethod.POST)
    public int deleteGovOldPersonAttr(@RequestBody  GovOldPersonAttrPo govOldPersonAttrPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govOldPersonAttrDto 数据对象分享
     * @return GovOldPersonAttrDto 对象数据
     */
    @RequestMapping(value = "/queryGovOldPersonAttrs", method = RequestMethod.POST)
    List<GovOldPersonAttrDto> queryGovOldPersonAttrs(@RequestBody GovOldPersonAttrDto govOldPersonAttrDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govOldPersonAttrDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovOldPersonAttrsCount", method = RequestMethod.POST)
    int queryGovOldPersonAttrsCount(@RequestBody GovOldPersonAttrDto govOldPersonAttrDto);
}
