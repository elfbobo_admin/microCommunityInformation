package com.java110.intf.assets;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.machineType.MachineTypeDto;
import com.java110.po.machineType.MachineTypePo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IMachineTypeInnerServiceSMO
 * @Description 设备类型接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-assets", configuration = {FeignConfiguration.class})
@RequestMapping("/machineTypeApi")
public interface IMachineTypeInnerServiceSMO {


    @RequestMapping(value = "/saveMachineType", method = RequestMethod.POST)
    public int saveMachineType(@RequestBody  MachineTypePo machineTypePo);

    @RequestMapping(value = "/updateMachineType", method = RequestMethod.POST)
    public int updateMachineType(@RequestBody  MachineTypePo machineTypePo);

    @RequestMapping(value = "/deleteMachineType", method = RequestMethod.POST)
    public int deleteMachineType(@RequestBody  MachineTypePo machineTypePo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param machineTypeDto 数据对象分享
     * @return MachineTypeDto 对象数据
     */
    @RequestMapping(value = "/queryMachineTypes", method = RequestMethod.POST)
    List<MachineTypeDto> queryMachineTypes(@RequestBody MachineTypeDto machineTypeDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param machineTypeDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryMachineTypesCount", method = RequestMethod.POST)
    int queryMachineTypesCount(@RequestBody MachineTypeDto machineTypeDto);
}
