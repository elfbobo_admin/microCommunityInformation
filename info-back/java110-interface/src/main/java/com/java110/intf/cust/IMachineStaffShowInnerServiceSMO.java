package com.java110.intf.cust;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.machineStaffShow.MachineStaffShowDto;
import com.java110.po.machineStaffShow.MachineStaffShowPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IMachineStaffShowInnerServiceSMO
 * @Description 摄像头员工关系接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-cust", configuration = {FeignConfiguration.class})
@RequestMapping("/machineStaffShowApi")
public interface IMachineStaffShowInnerServiceSMO {


    @RequestMapping(value = "/saveMachineStaffShow", method = RequestMethod.POST)
    public int saveMachineStaffShow(@RequestBody  MachineStaffShowPo machineStaffShowPo);

    @RequestMapping(value = "/updateMachineStaffShow", method = RequestMethod.POST)
    public int updateMachineStaffShow(@RequestBody  MachineStaffShowPo machineStaffShowPo);

    @RequestMapping(value = "/deleteMachineStaffShow", method = RequestMethod.POST)
    public int deleteMachineStaffShow(@RequestBody  MachineStaffShowPo machineStaffShowPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param machineStaffShowDto 数据对象分享
     * @return MachineStaffShowDto 对象数据
     */
    @RequestMapping(value = "/queryMachineStaffShows", method = RequestMethod.POST)
    List<MachineStaffShowDto> queryMachineStaffShows(@RequestBody MachineStaffShowDto machineStaffShowDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param machineStaffShowDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryMachineStaffShowsCount", method = RequestMethod.POST)
    int queryMachineStaffShowsCount(@RequestBody MachineStaffShowDto machineStaffShowDto);
}
