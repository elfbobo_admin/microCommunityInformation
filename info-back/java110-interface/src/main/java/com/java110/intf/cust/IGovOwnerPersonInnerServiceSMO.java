package com.java110.intf.cust;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govOwnerPerson.GovOwnerPersonDto;
import com.java110.po.govOwnerPerson.GovOwnerPersonPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovOwnerPersonInnerServiceSMO
 * @Description 人口户籍关系接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-cust", configuration = {FeignConfiguration.class})
@RequestMapping("/govOwnerPersonApi")
public interface IGovOwnerPersonInnerServiceSMO {


    @RequestMapping(value = "/saveGovOwnerPerson", method = RequestMethod.POST)
    public int saveGovOwnerPerson(@RequestBody GovOwnerPersonPo govOwnerPersonPo);

    @RequestMapping(value = "/updateGovOwnerPerson", method = RequestMethod.POST)
    public int updateGovOwnerPerson(@RequestBody  GovOwnerPersonPo govOwnerPersonPo);

    @RequestMapping(value = "/deleteGovOwnerPerson", method = RequestMethod.POST)
    public int deleteGovOwnerPerson(@RequestBody  GovOwnerPersonPo govOwnerPersonPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govOwnerPersonDto 数据对象分享
     * @return GovOwnerPersonDto 对象数据
     */
    @RequestMapping(value = "/queryGovOwnerPersons", method = RequestMethod.POST)
    List<GovOwnerPersonDto> queryGovOwnerPersons(@RequestBody GovOwnerPersonDto govOwnerPersonDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govOwnerPersonDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovOwnerPersonsCount", method = RequestMethod.POST)
    int queryGovOwnerPersonsCount(@RequestBody GovOwnerPersonDto govOwnerPersonDto);
}
