package com.java110.intf.cust;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govSchoolPeripheralPerson.GovSchoolPeripheralPersonDto;
import com.java110.po.govSchoolPeripheralPerson.GovSchoolPeripheralPersonPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovSchoolPeripheralPersonInnerServiceSMO
 * @Description 校园周边重点人员接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-cust", configuration = {FeignConfiguration.class})
@RequestMapping("/govSchoolPeripheralPersonApi")
public interface IGovSchoolPeripheralPersonInnerServiceSMO {


    @RequestMapping(value = "/saveGovSchoolPeripheralPerson", method = RequestMethod.POST)
    public int saveGovSchoolPeripheralPerson(@RequestBody  GovSchoolPeripheralPersonPo govSchoolPeripheralPersonPo);

    @RequestMapping(value = "/updateGovSchoolPeripheralPerson", method = RequestMethod.POST)
    public int updateGovSchoolPeripheralPerson(@RequestBody  GovSchoolPeripheralPersonPo govSchoolPeripheralPersonPo);

    @RequestMapping(value = "/deleteGovSchoolPeripheralPerson", method = RequestMethod.POST)
    public int deleteGovSchoolPeripheralPerson(@RequestBody  GovSchoolPeripheralPersonPo govSchoolPeripheralPersonPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govSchoolPeripheralPersonDto 数据对象分享
     * @return GovSchoolPeripheralPersonDto 对象数据
     */
    @RequestMapping(value = "/queryGovSchoolPeripheralPersons", method = RequestMethod.POST)
    List<GovSchoolPeripheralPersonDto> queryGovSchoolPeripheralPersons(@RequestBody GovSchoolPeripheralPersonDto govSchoolPeripheralPersonDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govSchoolPeripheralPersonDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovSchoolPeripheralPersonsCount", method = RequestMethod.POST)
    int queryGovSchoolPeripheralPersonsCount(@RequestBody GovSchoolPeripheralPersonDto govSchoolPeripheralPersonDto);
}
