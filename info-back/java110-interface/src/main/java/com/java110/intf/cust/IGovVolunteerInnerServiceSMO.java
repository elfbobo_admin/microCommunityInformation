package com.java110.intf.cust;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govVolunteer.GovVolunteerDto;
import com.java110.po.govVolunteer.GovVolunteerPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovVolunteerInnerServiceSMO
 * @Description 志愿者管理接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-cust", configuration = {FeignConfiguration.class})
@RequestMapping("/govVolunteerApi")
public interface IGovVolunteerInnerServiceSMO {


    @RequestMapping(value = "/saveGovVolunteer", method = RequestMethod.POST)
    public int saveGovVolunteer(@RequestBody  GovVolunteerPo govVolunteerPo);

    @RequestMapping(value = "/updateGovVolunteer", method = RequestMethod.POST)
    public int updateGovVolunteer(@RequestBody  GovVolunteerPo govVolunteerPo);

    @RequestMapping(value = "/deleteGovVolunteer", method = RequestMethod.POST)
    public int deleteGovVolunteer(@RequestBody  GovVolunteerPo govVolunteerPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govVolunteerDto 数据对象分享
     * @return GovVolunteerDto 对象数据
     */
    @RequestMapping(value = "/queryGovVolunteers", method = RequestMethod.POST)
    List<GovVolunteerDto> queryGovVolunteers(@RequestBody GovVolunteerDto govVolunteerDto);
    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govVolunteerDto 数据对象分享
     * @return GovVolunteerDto 对象数据
     */
    @RequestMapping(value = "/getGovStateVolunteerCount", method = RequestMethod.POST)
    List<GovVolunteerDto> getGovStateVolunteerCount(@RequestBody GovVolunteerDto govVolunteerDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govVolunteerDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovVolunteersCount", method = RequestMethod.POST)
    int queryGovVolunteersCount(@RequestBody GovVolunteerDto govVolunteerDto);
}
