package com.java110.intf.cust;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govMedicalDoctorRel.GovMedicalDoctorRelDto;
import com.java110.po.govMedicalDoctorRel.GovMedicalDoctorRelPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovMedicalDoctorRelInnerServiceSMO
 * @Description 档案医生接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-cust", configuration = {FeignConfiguration.class})
@RequestMapping("/govMedicalDoctorRelApi")
public interface IGovMedicalDoctorRelInnerServiceSMO {


    @RequestMapping(value = "/saveGovMedicalDoctorRel", method = RequestMethod.POST)
    public int saveGovMedicalDoctorRel(@RequestBody  GovMedicalDoctorRelPo govMedicalDoctorRelPo);

    @RequestMapping(value = "/updateGovMedicalDoctorRel", method = RequestMethod.POST)
    public int updateGovMedicalDoctorRel(@RequestBody  GovMedicalDoctorRelPo govMedicalDoctorRelPo);

    @RequestMapping(value = "/deleteGovMedicalDoctorRel", method = RequestMethod.POST)
    public int deleteGovMedicalDoctorRel(@RequestBody  GovMedicalDoctorRelPo govMedicalDoctorRelPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govMedicalDoctorRelDto 数据对象分享
     * @return GovMedicalDoctorRelDto 对象数据
     */
    @RequestMapping(value = "/queryGovMedicalDoctorRels", method = RequestMethod.POST)
    List<GovMedicalDoctorRelDto> queryGovMedicalDoctorRels(@RequestBody GovMedicalDoctorRelDto govMedicalDoctorRelDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govMedicalDoctorRelDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovMedicalDoctorRelsCount", method = RequestMethod.POST)
    int queryGovMedicalDoctorRelsCount(@RequestBody GovMedicalDoctorRelDto govMedicalDoctorRelDto);
}
