package com.java110.intf.gov;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govCiviladminType.GovCiviladminTypeDto;
import com.java110.po.govCiviladminType.GovCiviladminTypePo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovCiviladminTypeInnerServiceSMO
 * @Description 民政服务宣传类型接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-gov", configuration = {FeignConfiguration.class})
@RequestMapping("/govCiviladminTypeApi")
public interface IGovCiviladminTypeInnerServiceSMO {


    @RequestMapping(value = "/saveGovCiviladminType", method = RequestMethod.POST)
    public int saveGovCiviladminType(@RequestBody  GovCiviladminTypePo govCiviladminTypePo);

    @RequestMapping(value = "/updateGovCiviladminType", method = RequestMethod.POST)
    public int updateGovCiviladminType(@RequestBody  GovCiviladminTypePo govCiviladminTypePo);

    @RequestMapping(value = "/deleteGovCiviladminType", method = RequestMethod.POST)
    public int deleteGovCiviladminType(@RequestBody  GovCiviladminTypePo govCiviladminTypePo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govCiviladminTypeDto 数据对象分享
     * @return GovCiviladminTypeDto 对象数据
     */
    @RequestMapping(value = "/queryGovCiviladminTypes", method = RequestMethod.POST)
    List<GovCiviladminTypeDto> queryGovCiviladminTypes(@RequestBody GovCiviladminTypeDto govCiviladminTypeDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govCiviladminTypeDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovCiviladminTypesCount", method = RequestMethod.POST)
    int queryGovCiviladminTypesCount(@RequestBody GovCiviladminTypeDto govCiviladminTypeDto);
}
