package com.java110.intf.gov;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govMeetingMemberRel.GovMeetingMemberRelDto;
import com.java110.po.govMeetingMemberRel.GovMeetingMemberRelPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovMeetingMemberRelInnerServiceSMO
 * @Description 会议与参会人关系接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-gov", configuration = {FeignConfiguration.class})
@RequestMapping("/govMeetingMemberRelApi")
public interface IGovMeetingMemberRelInnerServiceSMO {


    @RequestMapping(value = "/saveGovMeetingMemberRel", method = RequestMethod.POST)
    public int saveGovMeetingMemberRel(@RequestBody  GovMeetingMemberRelPo govMeetingMemberRelPo);

    @RequestMapping(value = "/updateGovMeetingMemberRel", method = RequestMethod.POST)
    public int updateGovMeetingMemberRel(@RequestBody  GovMeetingMemberRelPo govMeetingMemberRelPo);

    @RequestMapping(value = "/deleteGovMeetingMemberRel", method = RequestMethod.POST)
    public int deleteGovMeetingMemberRel(@RequestBody  GovMeetingMemberRelPo govMeetingMemberRelPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govMeetingMemberRelDto 数据对象分享
     * @return GovMeetingMemberRelDto 对象数据
     */
    @RequestMapping(value = "/queryGovMeetingMemberRels", method = RequestMethod.POST)
    List<GovMeetingMemberRelDto> queryGovMeetingMemberRels(@RequestBody GovMeetingMemberRelDto govMeetingMemberRelDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govMeetingMemberRelDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovMeetingMemberRelsCount", method = RequestMethod.POST)
    int queryGovMeetingMemberRelsCount(@RequestBody GovMeetingMemberRelDto govMeetingMemberRelDto);
}
