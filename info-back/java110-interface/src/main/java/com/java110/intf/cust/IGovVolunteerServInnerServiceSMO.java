package com.java110.intf.cust;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govVolunteerServ.GovVolunteerServDto;
import com.java110.po.govVolunteerServ.GovVolunteerServPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovVolunteerServInnerServiceSMO
 * @Description 服务领域接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-cust", configuration = {FeignConfiguration.class})
@RequestMapping("/govVolunteerServApi")
public interface IGovVolunteerServInnerServiceSMO {


    @RequestMapping(value = "/saveGovVolunteerServ", method = RequestMethod.POST)
    public int saveGovVolunteerServ(@RequestBody  GovVolunteerServPo govVolunteerServPo);

    @RequestMapping(value = "/updateGovVolunteerServ", method = RequestMethod.POST)
    public int updateGovVolunteerServ(@RequestBody  GovVolunteerServPo govVolunteerServPo);

    @RequestMapping(value = "/deleteGovVolunteerServ", method = RequestMethod.POST)
    public int deleteGovVolunteerServ(@RequestBody  GovVolunteerServPo govVolunteerServPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govVolunteerServDto 数据对象分享
     * @return GovVolunteerServDto 对象数据
     */
    @RequestMapping(value = "/queryGovVolunteerServs", method = RequestMethod.POST)
    List<GovVolunteerServDto> queryGovVolunteerServs(@RequestBody GovVolunteerServDto govVolunteerServDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govVolunteerServDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovVolunteerServsCount", method = RequestMethod.POST)
    int queryGovVolunteerServsCount(@RequestBody GovVolunteerServDto govVolunteerServDto);
}
