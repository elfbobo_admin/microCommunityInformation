package com.java110.intf.gov;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govAdvertItem.GovAdvertItemDto;
import com.java110.po.govAdvertItem.GovAdvertItemPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovAdvertItemInnerServiceSMO
 * @Description 广告明细接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-gov", configuration = {FeignConfiguration.class})
@RequestMapping("/govAdvertItemApi")
public interface IGovAdvertItemInnerServiceSMO {


    @RequestMapping(value = "/saveGovAdvertItem", method = RequestMethod.POST)
    public int saveGovAdvertItem(@RequestBody GovAdvertItemPo govAdvertItemPo);

    @RequestMapping(value = "/updateGovAdvertItem", method = RequestMethod.POST)
    public int updateGovAdvertItem(@RequestBody  GovAdvertItemPo govAdvertItemPo);

    @RequestMapping(value = "/deleteGovAdvertItem", method = RequestMethod.POST)
    public int deleteGovAdvertItem(@RequestBody  GovAdvertItemPo govAdvertItemPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govAdvertItemDto 数据对象分享
     * @return GovAdvertItemDto 对象数据
     */
    @RequestMapping(value = "/queryGovAdvertItems", method = RequestMethod.POST)
    List<GovAdvertItemDto> queryGovAdvertItems(@RequestBody GovAdvertItemDto govAdvertItemDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govAdvertItemDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovAdvertItemsCount", method = RequestMethod.POST)
    int queryGovAdvertItemsCount(@RequestBody GovAdvertItemDto govAdvertItemDto);
}
