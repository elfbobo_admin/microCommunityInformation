package com.java110.intf.assets;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govCommunityLocation.GovCommunityLocationDto;
import com.java110.po.govCommunityLocation.GovCommunityLocationPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovCommunityLocationInnerServiceSMO
 * @Description 小区位置接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-assets", configuration = {FeignConfiguration.class})
@RequestMapping("/govCommunityLocationApi")
public interface IGovCommunityLocationInnerServiceSMO {


    @RequestMapping(value = "/saveGovCommunityLocation", method = RequestMethod.POST)
    public int saveGovCommunityLocation(@RequestBody GovCommunityLocationPo govCommunityLocationPo);

    @RequestMapping(value = "/updateGovCommunityLocation", method = RequestMethod.POST)
    public int updateGovCommunityLocation(@RequestBody  GovCommunityLocationPo govCommunityLocationPo);

    @RequestMapping(value = "/deleteGovCommunityLocation", method = RequestMethod.POST)
    public int deleteGovCommunityLocation(@RequestBody  GovCommunityLocationPo govCommunityLocationPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govCommunityLocationDto 数据对象分享
     * @return GovCommunityLocationDto 对象数据
     */
    @RequestMapping(value = "/queryGovCommunityLocations", method = RequestMethod.POST)
    List<GovCommunityLocationDto> queryGovCommunityLocations(@RequestBody GovCommunityLocationDto govCommunityLocationDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govCommunityLocationDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovCommunityLocationsCount", method = RequestMethod.POST)
    int queryGovCommunityLocationsCount(@RequestBody GovCommunityLocationDto govCommunityLocationDto);
}
