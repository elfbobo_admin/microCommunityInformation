package com.java110.intf.cust;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.StoreUserV1.StoreUserV1Dto;
import com.java110.po.StoreUserV1.StoreUserV1Po;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IStoreUserV1InnerServiceSMO
 * @Description 用戶管理接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-cust", configuration = {FeignConfiguration.class})
@RequestMapping("/storeUserV1Api")
public interface IStoreUserV1InnerServiceSMO {


    @RequestMapping(value = "/saveStoreUserV1", method = RequestMethod.POST)
    public int saveStoreUserV1(@RequestBody StoreUserV1Po storeUserV1Po);

    @RequestMapping(value = "/updateStoreUserV1", method = RequestMethod.POST)
    public int updateStoreUserV1(@RequestBody  StoreUserV1Po storeUserV1Po);

    @RequestMapping(value = "/deleteStoreUserV1", method = RequestMethod.POST)
    public int deleteStoreUserV1(@RequestBody  StoreUserV1Po storeUserV1Po);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param storeUserV1Dto 数据对象分享
     * @return StoreUserV1Dto 对象数据
     */
    @RequestMapping(value = "/queryStoreUserV1s", method = RequestMethod.POST)
    List<StoreUserV1Dto> queryStoreUserV1s(@RequestBody StoreUserV1Dto storeUserV1Dto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param storeUserV1Dto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryStoreUserV1sCount", method = RequestMethod.POST)
    int queryStoreUserV1sCount(@RequestBody StoreUserV1Dto storeUserV1Dto);
}
