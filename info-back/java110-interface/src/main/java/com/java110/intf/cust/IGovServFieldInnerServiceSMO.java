package com.java110.intf.cust;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govServField.GovServFieldDto;
import com.java110.po.govServField.GovServFieldPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovServFieldInnerServiceSMO
 * @Description 服务领域接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-cust", configuration = {FeignConfiguration.class})
@RequestMapping("/govServFieldApi")
public interface IGovServFieldInnerServiceSMO {


    @RequestMapping(value = "/saveGovServField", method = RequestMethod.POST)
    public int saveGovServField(@RequestBody  GovServFieldPo govServFieldPo);

    @RequestMapping(value = "/updateGovServField", method = RequestMethod.POST)
    public int updateGovServField(@RequestBody  GovServFieldPo govServFieldPo);

    @RequestMapping(value = "/deleteGovServField", method = RequestMethod.POST)
    public int deleteGovServField(@RequestBody  GovServFieldPo govServFieldPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govServFieldDto 数据对象分享
     * @return GovServFieldDto 对象数据
     */
    @RequestMapping(value = "/queryGovServFields", method = RequestMethod.POST)
    List<GovServFieldDto> queryGovServFields(@RequestBody GovServFieldDto govServFieldDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govServFieldDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovServFieldsCount", method = RequestMethod.POST)
    int queryGovServFieldsCount(@RequestBody GovServFieldDto govServFieldDto);
}
