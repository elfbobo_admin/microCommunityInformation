package com.java110.intf.gov;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govRoadProtectionCase.GovRoadProtectionCaseDto;
import com.java110.po.govRoadProtectionCase.GovRoadProtectionCasePo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovRoadProtectionCaseInnerServiceSMO
 * @Description 路案事件接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-gov", configuration = {FeignConfiguration.class})
@RequestMapping("/govRoadProtectionCaseApi")
public interface IGovRoadProtectionCaseInnerServiceSMO {


    @RequestMapping(value = "/saveGovRoadProtectionCase", method = RequestMethod.POST)
    public int saveGovRoadProtectionCase(@RequestBody  GovRoadProtectionCasePo govRoadProtectionCasePo);

    @RequestMapping(value = "/updateGovRoadProtectionCase", method = RequestMethod.POST)
    public int updateGovRoadProtectionCase(@RequestBody  GovRoadProtectionCasePo govRoadProtectionCasePo);

    @RequestMapping(value = "/deleteGovRoadProtectionCase", method = RequestMethod.POST)
    public int deleteGovRoadProtectionCase(@RequestBody  GovRoadProtectionCasePo govRoadProtectionCasePo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govRoadProtectionCaseDto 数据对象分享
     * @return GovRoadProtectionCaseDto 对象数据
     */
    @RequestMapping(value = "/queryGovRoadProtectionCases", method = RequestMethod.POST)
    List<GovRoadProtectionCaseDto> queryGovRoadProtectionCases(@RequestBody GovRoadProtectionCaseDto govRoadProtectionCaseDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govRoadProtectionCaseDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovRoadProtectionCasesCount", method = RequestMethod.POST)
    int queryGovRoadProtectionCasesCount(@RequestBody GovRoadProtectionCaseDto govRoadProtectionCaseDto);
}
