package com.java110.intf.gov;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govPersonInoutRecord.GovPersonInoutRecordDto;
import com.java110.po.govPersonInoutRecord.GovPersonInoutRecordPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovPersonInoutRecordInnerServiceSMO
 * @Description 开门记录接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-gov", configuration = {FeignConfiguration.class})
@RequestMapping("/govPersonInoutRecordApi")
public interface IGovPersonInoutRecordInnerServiceSMO {


    @RequestMapping(value = "/saveGovPersonInoutRecord", method = RequestMethod.POST)
    public int saveGovPersonInoutRecord(@RequestBody GovPersonInoutRecordPo govPersonInoutRecordPo);

    @RequestMapping(value = "/updateGovPersonInoutRecord", method = RequestMethod.POST)
    public int updateGovPersonInoutRecord(@RequestBody  GovPersonInoutRecordPo govPersonInoutRecordPo);

    @RequestMapping(value = "/deleteGovPersonInoutRecord", method = RequestMethod.POST)
    public int deleteGovPersonInoutRecord(@RequestBody  GovPersonInoutRecordPo govPersonInoutRecordPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govPersonInoutRecordDto 数据对象分享
     * @return GovPersonInoutRecordDto 对象数据
     */
    @RequestMapping(value = "/queryGovPersonInoutRecords", method = RequestMethod.POST)
    List<GovPersonInoutRecordDto> queryGovPersonInoutRecords(@RequestBody GovPersonInoutRecordDto govPersonInoutRecordDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govPersonInoutRecordDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovPersonInoutRecordsCount", method = RequestMethod.POST)
    int queryGovPersonInoutRecordsCount(@RequestBody GovPersonInoutRecordDto govPersonInoutRecordDto);
}
