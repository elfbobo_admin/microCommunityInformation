package com.java110.intf.gov;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govPartyOrgContext.GovPartyOrgContextDto;
import com.java110.po.govPartyOrgContext.GovPartyOrgContextPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovPartyOrgContextInnerServiceSMO
 * @Description 党组织简介接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-gov", configuration = {FeignConfiguration.class})
@RequestMapping("/govPartyOrgContextApi")
public interface IGovPartyOrgContextInnerServiceSMO {


    @RequestMapping(value = "/saveGovPartyOrgContext", method = RequestMethod.POST)
    public int saveGovPartyOrgContext(@RequestBody GovPartyOrgContextPo govPartyOrgContextPo);

    @RequestMapping(value = "/updateGovPartyOrgContext", method = RequestMethod.POST)
    public int updateGovPartyOrgContext(@RequestBody  GovPartyOrgContextPo govPartyOrgContextPo);

    @RequestMapping(value = "/deleteGovPartyOrgContext", method = RequestMethod.POST)
    public int deleteGovPartyOrgContext(@RequestBody  GovPartyOrgContextPo govPartyOrgContextPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govPartyOrgContextDto 数据对象分享
     * @return GovPartyOrgContextDto 对象数据
     */
    @RequestMapping(value = "/queryGovPartyOrgContexts", method = RequestMethod.POST)
    List<GovPartyOrgContextDto> queryGovPartyOrgContexts(@RequestBody GovPartyOrgContextDto govPartyOrgContextDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govPartyOrgContextDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovPartyOrgContextsCount", method = RequestMethod.POST)
    int queryGovPartyOrgContextsCount(@RequestBody GovPartyOrgContextDto govPartyOrgContextDto);
}
