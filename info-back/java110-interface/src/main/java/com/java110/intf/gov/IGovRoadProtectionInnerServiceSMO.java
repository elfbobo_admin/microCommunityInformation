package com.java110.intf.gov;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govRoadProtection.GovRoadProtectionDto;
import com.java110.po.govRoadProtection.GovRoadProtectionPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovRoadProtectionInnerServiceSMO
 * @Description 护路护线接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-gov", configuration = {FeignConfiguration.class})
@RequestMapping("/govRoadProtectionApi")
public interface IGovRoadProtectionInnerServiceSMO {


    @RequestMapping(value = "/saveGovRoadProtection", method = RequestMethod.POST)
    public int saveGovRoadProtection(@RequestBody  GovRoadProtectionPo govRoadProtectionPo);

    @RequestMapping(value = "/updateGovRoadProtection", method = RequestMethod.POST)
    public int updateGovRoadProtection(@RequestBody  GovRoadProtectionPo govRoadProtectionPo);

    @RequestMapping(value = "/deleteGovRoadProtection", method = RequestMethod.POST)
    public int deleteGovRoadProtection(@RequestBody  GovRoadProtectionPo govRoadProtectionPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govRoadProtectionDto 数据对象分享
     * @return GovRoadProtectionDto 对象数据
     */
    @RequestMapping(value = "/queryGovRoadProtections", method = RequestMethod.POST)
    List<GovRoadProtectionDto> queryGovRoadProtections(@RequestBody GovRoadProtectionDto govRoadProtectionDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govRoadProtectionDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovRoadProtectionsCount", method = RequestMethod.POST)
    int queryGovRoadProtectionsCount(@RequestBody GovRoadProtectionDto govRoadProtectionDto);
}
