package com.java110.intf.gov;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govReportReturnVisit.GovReportReturnVisitDto;
import com.java110.po.govReportReturnVisit.GovReportReturnVisitPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovReportReturnVisitInnerServiceSMO
 * @Description 回访管理接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "community-service", configuration = {FeignConfiguration.class})
@RequestMapping("/govReportReturnVisitApi")
public interface IGovReportReturnVisitInnerServiceSMO {


    @RequestMapping(value = "/saveGovReportReturnVisit", method = RequestMethod.POST)
    public int saveGovReportReturnVisit(@RequestBody GovReportReturnVisitPo govReportReturnVisitPo);

    @RequestMapping(value = "/updateGovReportReturnVisit", method = RequestMethod.POST)
    public int updateGovReportReturnVisit(@RequestBody  GovReportReturnVisitPo govReportReturnVisitPo);

    @RequestMapping(value = "/deleteGovReportReturnVisit", method = RequestMethod.POST)
    public int deleteGovReportReturnVisit(@RequestBody  GovReportReturnVisitPo govReportReturnVisitPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govReportReturnVisitDto 数据对象分享
     * @return GovReportReturnVisitDto 对象数据
     */
    @RequestMapping(value = "/queryGovReportReturnVisits", method = RequestMethod.POST)
    List<GovReportReturnVisitDto> queryGovReportReturnVisits(@RequestBody GovReportReturnVisitDto govReportReturnVisitDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govReportReturnVisitDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovReportReturnVisitsCount", method = RequestMethod.POST)
    int queryGovReportReturnVisitsCount(@RequestBody GovReportReturnVisitDto govReportReturnVisitDto);
}
