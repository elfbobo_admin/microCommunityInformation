package com.java110.intf.cust;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govFollowupSurvey.GovFollowupSurveyDto;
import com.java110.po.govFollowupSurvey.GovFollowupSurveyPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovFollowupSurveyInnerServiceSMO
 * @Description 随访登记接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-cust", configuration = {FeignConfiguration.class})
@RequestMapping("/govFollowupSurveyApi")
public interface IGovFollowupSurveyInnerServiceSMO {


    @RequestMapping(value = "/saveGovFollowupSurvey", method = RequestMethod.POST)
    public int saveGovFollowupSurvey(@RequestBody  GovFollowupSurveyPo govFollowupSurveyPo);

    @RequestMapping(value = "/updateGovFollowupSurvey", method = RequestMethod.POST)
    public int updateGovFollowupSurvey(@RequestBody  GovFollowupSurveyPo govFollowupSurveyPo);

    @RequestMapping(value = "/deleteGovFollowupSurvey", method = RequestMethod.POST)
    public int deleteGovFollowupSurvey(@RequestBody  GovFollowupSurveyPo govFollowupSurveyPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govFollowupSurveyDto 数据对象分享
     * @return GovFollowupSurveyDto 对象数据
     */
    @RequestMapping(value = "/queryGovFollowupSurveys", method = RequestMethod.POST)
    List<GovFollowupSurveyDto> queryGovFollowupSurveys(@RequestBody GovFollowupSurveyDto govFollowupSurveyDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govFollowupSurveyDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovFollowupSurveysCount", method = RequestMethod.POST)
    int queryGovFollowupSurveysCount(@RequestBody GovFollowupSurveyDto govFollowupSurveyDto);
}
