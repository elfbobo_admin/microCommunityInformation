package com.java110.intf.gov;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govPartyMemberChange.GovPartyMemberChangeDto;
import com.java110.po.govPartyMemberChange.GovPartyMemberChangePo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovPartyMemberChangeInnerServiceSMO
 * @Description 党关系管理接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-gov", configuration = {FeignConfiguration.class})
@RequestMapping("/govPartyMemberChangeApi")
public interface IGovPartyMemberChangeInnerServiceSMO {


    @RequestMapping(value = "/saveGovPartyMemberChange", method = RequestMethod.POST)
    public int saveGovPartyMemberChange(@RequestBody GovPartyMemberChangePo govPartyMemberChangePo);

    @RequestMapping(value = "/updateGovPartyMemberChange", method = RequestMethod.POST)
    public int updateGovPartyMemberChange(@RequestBody  GovPartyMemberChangePo govPartyMemberChangePo);

    @RequestMapping(value = "/deleteGovPartyMemberChange", method = RequestMethod.POST)
    public int deleteGovPartyMemberChange(@RequestBody  GovPartyMemberChangePo govPartyMemberChangePo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govPartyMemberChangeDto 数据对象分享
     * @return GovPartyMemberChangeDto 对象数据
     */
    @RequestMapping(value = "/queryGovPartyMemberChanges", method = RequestMethod.POST)
    List<GovPartyMemberChangeDto> queryGovPartyMemberChanges(@RequestBody GovPartyMemberChangeDto govPartyMemberChangeDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govPartyMemberChangeDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovPartyMemberChangesCount", method = RequestMethod.POST)
    int queryGovPartyMemberChangesCount(@RequestBody GovPartyMemberChangeDto govPartyMemberChangeDto);
}
