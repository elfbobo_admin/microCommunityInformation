package com.java110.intf.gov;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govAreaType.GovAreaTypeDto;
import com.java110.po.govAreaType.GovAreaTypePo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovAreaTypeInnerServiceSMO
 * @Description 涉及区域类型接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-gov", configuration = {FeignConfiguration.class})
@RequestMapping("/govAreaTypeApi")
public interface IGovAreaTypeInnerServiceSMO {


    @RequestMapping(value = "/saveGovAreaType", method = RequestMethod.POST)
    public int saveGovAreaType(@RequestBody  GovAreaTypePo govAreaTypePo);

    @RequestMapping(value = "/updateGovAreaType", method = RequestMethod.POST)
    public int updateGovAreaType(@RequestBody  GovAreaTypePo govAreaTypePo);

    @RequestMapping(value = "/deleteGovAreaType", method = RequestMethod.POST)
    public int deleteGovAreaType(@RequestBody  GovAreaTypePo govAreaTypePo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govAreaTypeDto 数据对象分享
     * @return GovAreaTypeDto 对象数据
     */
    @RequestMapping(value = "/queryGovAreaTypes", method = RequestMethod.POST)
    List<GovAreaTypeDto> queryGovAreaTypes(@RequestBody GovAreaTypeDto govAreaTypeDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govAreaTypeDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovAreaTypesCount", method = RequestMethod.POST)
    int queryGovAreaTypesCount(@RequestBody GovAreaTypeDto govAreaTypeDto);
}
