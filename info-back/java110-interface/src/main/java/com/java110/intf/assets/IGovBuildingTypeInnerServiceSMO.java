package com.java110.intf.assets;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govBuildingType.GovBuildingTypeDto;
import com.java110.po.govBuildingType.GovBuildingTypePo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovBuildingTypeInnerServiceSMO
 * @Description 建筑分类接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-assets", configuration = {FeignConfiguration.class})
@RequestMapping("/govBuildingTypeApi")
public interface IGovBuildingTypeInnerServiceSMO {


    @RequestMapping(value = "/saveGovBuildingType", method = RequestMethod.POST)
    public int saveGovBuildingType(@RequestBody GovBuildingTypePo govBuildingTypePo);

    @RequestMapping(value = "/updateGovBuildingType", method = RequestMethod.POST)
    public int updateGovBuildingType(@RequestBody  GovBuildingTypePo govBuildingTypePo);

    @RequestMapping(value = "/deleteGovBuildingType", method = RequestMethod.POST)
    public int deleteGovBuildingType(@RequestBody  GovBuildingTypePo govBuildingTypePo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govBuildingTypeDto 数据对象分享
     * @return GovBuildingTypeDto 对象数据
     */
    @RequestMapping(value = "/queryGovBuildingTypes", method = RequestMethod.POST)
    List<GovBuildingTypeDto> queryGovBuildingTypes(@RequestBody GovBuildingTypeDto govBuildingTypeDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govBuildingTypeDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovBuildingTypesCount", method = RequestMethod.POST)
    int queryGovBuildingTypesCount(@RequestBody GovBuildingTypeDto govBuildingTypeDto);
}
