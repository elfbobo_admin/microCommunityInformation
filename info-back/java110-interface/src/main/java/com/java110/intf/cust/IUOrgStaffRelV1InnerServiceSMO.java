package com.java110.intf.cust;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.uOrgStaffRelV1.UOrgStaffRelV1Dto;
import com.java110.po.uOrgStaffRelV1.UOrgStaffRelV1Po;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IUOrgStaffRelV1InnerServiceSMO
 * @Description 员工角色关系接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-cust", configuration = {FeignConfiguration.class})
@RequestMapping("/uOrgStaffRelV1Api")
public interface IUOrgStaffRelV1InnerServiceSMO {


    @RequestMapping(value = "/saveUOrgStaffRelV1", method = RequestMethod.POST)
    public int saveUOrgStaffRelV1(@RequestBody  UOrgStaffRelV1Po uOrgStaffRelV1Po);

    @RequestMapping(value = "/updateUOrgStaffRelV1", method = RequestMethod.POST)
    public int updateUOrgStaffRelV1(@RequestBody  UOrgStaffRelV1Po uOrgStaffRelV1Po);

    @RequestMapping(value = "/deleteUOrgStaffRelV1", method = RequestMethod.POST)
    public int deleteUOrgStaffRelV1(@RequestBody  UOrgStaffRelV1Po uOrgStaffRelV1Po);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param uOrgStaffRelV1Dto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryUOrgStaffRelV1sCount", method = RequestMethod.POST)
    int queryUOrgStaffRelV1sCount(@RequestBody UOrgStaffRelV1Dto uOrgStaffRelV1Dto);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param uOrgStaffRelV1Dto 数据对象分享
     * @return UOrgStaffRelV1Dto 对象数据
     */
    @RequestMapping(value = "/queryUOrgStaffRelV1s", method = RequestMethod.POST)
    List<UOrgStaffRelV1Dto> queryUOrgStaffRelV1s(@RequestBody UOrgStaffRelV1Dto uOrgStaffRelV1Dto);
}
