package com.java110.intf.cust;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govOwner.GovOwnerDto;
import com.java110.po.govOwner.GovOwnerPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovOwnerInnerServiceSMO
 * @Description 户籍管理接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-cust", configuration = {FeignConfiguration.class})
@RequestMapping("/govOwnerApi")
public interface IGovOwnerInnerServiceSMO {


    @RequestMapping(value = "/saveGovOwner", method = RequestMethod.POST)
    public int saveGovOwner(@RequestBody GovOwnerPo govOwnerPo);

    @RequestMapping(value = "/updateGovOwner", method = RequestMethod.POST)
    public int updateGovOwner(@RequestBody  GovOwnerPo govOwnerPo);

    @RequestMapping(value = "/deleteGovOwner", method = RequestMethod.POST)
    public int deleteGovOwner(@RequestBody  GovOwnerPo govOwnerPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govOwnerDto 数据对象分享
     * @return GovOwnerDto 对象数据
     */
    @RequestMapping(value = "/queryGovOwners", method = RequestMethod.POST)
    List<GovOwnerDto> queryGovOwners(@RequestBody GovOwnerDto govOwnerDto);
    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govOwnerDto 数据对象分享
     * @return GovOwnerDto 对象数据
     */
    @RequestMapping(value = "/getGovOwnerPersonRel", method = RequestMethod.POST)
    List<GovOwnerDto> getGovOwnerPersonRel(@RequestBody GovOwnerDto govOwnerDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govOwnerDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovOwnersCount", method = RequestMethod.POST)
    int queryGovOwnersCount(@RequestBody GovOwnerDto govOwnerDto);
    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govOwnerDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/getGovOwnerPersonRelCount", method = RequestMethod.POST)
    int getGovOwnerPersonRelCount(@RequestBody GovOwnerDto govOwnerDto);
}
