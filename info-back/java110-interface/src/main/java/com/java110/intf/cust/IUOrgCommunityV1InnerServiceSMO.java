package com.java110.intf.cust;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.uOrgCommunityV1.UOrgCommunityV1Dto;
import com.java110.po.uOrgCommunityV1.UOrgCommunityV1Po;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IUOrgCommunityV1InnerServiceSMO
 * @Description 组织区域关系接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-cust", configuration = {FeignConfiguration.class})
@RequestMapping("/uOrgCommunityV1Api")
public interface IUOrgCommunityV1InnerServiceSMO {


    @RequestMapping(value = "/saveUOrgCommunityV1", method = RequestMethod.POST)
    public int saveUOrgCommunityV1(@RequestBody  UOrgCommunityV1Po uOrgCommunityV1Po);

    @RequestMapping(value = "/updateUOrgCommunityV1", method = RequestMethod.POST)
    public int updateUOrgCommunityV1(@RequestBody  UOrgCommunityV1Po uOrgCommunityV1Po);

    @RequestMapping(value = "/deleteUOrgCommunityV1", method = RequestMethod.POST)
    public int deleteUOrgCommunityV1(@RequestBody  UOrgCommunityV1Po uOrgCommunityV1Po);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param uOrgCommunityV1Dto 数据对象分享
     * @return UOrgCommunityV1Dto 对象数据
     */
    @RequestMapping(value = "/queryUOrgCommunityV1s", method = RequestMethod.POST)
    List<UOrgCommunityV1Dto> queryUOrgCommunityV1s(@RequestBody UOrgCommunityV1Dto uOrgCommunityV1Dto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param uOrgCommunityV1Dto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryUOrgCommunityV1sCount", method = RequestMethod.POST)
    int queryUOrgCommunityV1sCount(@RequestBody UOrgCommunityV1Dto uOrgCommunityV1Dto);
}
