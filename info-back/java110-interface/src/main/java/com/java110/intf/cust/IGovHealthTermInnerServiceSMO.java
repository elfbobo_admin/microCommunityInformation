package com.java110.intf.cust;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govHealthTerm.GovHealthTermDto;
import com.java110.po.govHealthTerm.GovHealthTermPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovHealthTermInnerServiceSMO
 * @Description 体检项接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-cust", configuration = {FeignConfiguration.class})
@RequestMapping("/govHealthTermApi")
public interface IGovHealthTermInnerServiceSMO {


    @RequestMapping(value = "/saveGovHealthTerm", method = RequestMethod.POST)
    public int saveGovHealthTerm(@RequestBody  GovHealthTermPo govHealthTermPo);

    @RequestMapping(value = "/updateGovHealthTerm", method = RequestMethod.POST)
    public int updateGovHealthTerm(@RequestBody  GovHealthTermPo govHealthTermPo);

    @RequestMapping(value = "/deleteGovHealthTerm", method = RequestMethod.POST)
    public int deleteGovHealthTerm(@RequestBody  GovHealthTermPo govHealthTermPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govHealthTermDto 数据对象分享
     * @return GovHealthTermDto 对象数据
     */
    @RequestMapping(value = "/queryGovHealthTerms", method = RequestMethod.POST)
    List<GovHealthTermDto> queryGovHealthTerms(@RequestBody GovHealthTermDto govHealthTermDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govHealthTermDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovHealthTermsCount", method = RequestMethod.POST)
    int queryGovHealthTermsCount(@RequestBody GovHealthTermDto govHealthTermDto);
}
