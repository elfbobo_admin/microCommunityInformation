package com.java110.intf.gov;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.reportPool.ReportPoolDto;
import com.java110.po.reportPool.ReportPoolPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IReportPoolInnerServiceSMO
 * @Description 报事管理接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-gov", configuration = {FeignConfiguration.class})
@RequestMapping("/reportPoolApi")
public interface IReportPoolInnerServiceSMO {


    @RequestMapping(value = "/saveReportPool", method = RequestMethod.POST)
    public int saveReportPool(@RequestBody ReportPoolPo reportPoolPo);

    @RequestMapping(value = "/updateReportPool", method = RequestMethod.POST)
    public int updateReportPool(@RequestBody  ReportPoolPo reportPoolPo);

    @RequestMapping(value = "/deleteReportPool", method = RequestMethod.POST)
    public int deleteReportPool(@RequestBody  ReportPoolPo reportPoolPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param reportPoolDto 数据对象分享
     * @return ReportPoolDto 对象数据
     */
    @RequestMapping(value = "/queryReportPools", method = RequestMethod.POST)
    List<ReportPoolDto> queryReportPools(@RequestBody ReportPoolDto reportPoolDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param reportPoolDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryReportPoolsCount", method = RequestMethod.POST)
    int queryReportPoolsCount(@RequestBody ReportPoolDto reportPoolDto);
    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param reportPoolDto 数据对象分享
     * @return ReportPoolDto 对象数据
     */
    @RequestMapping(value = "/quseryReportStaffs", method = RequestMethod.POST)
    List<ReportPoolDto> quseryReportStaffs(@RequestBody ReportPoolDto reportPoolDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param reportPoolDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/quseryReportStaffCount", method = RequestMethod.POST)
    int quseryReportStaffCount(@RequestBody ReportPoolDto reportPoolDto);
    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param reportPoolDto 数据对象分享
     * @return ReportPoolDto 对象数据
     */
    @RequestMapping(value = "/queryStaffFinishReports", method = RequestMethod.POST)
    List<ReportPoolDto> queryStaffFinishReports(@RequestBody ReportPoolDto reportPoolDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param reportPoolDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryStaffFinishReportCount", method = RequestMethod.POST)
    int queryStaffFinishReportCount(@RequestBody ReportPoolDto reportPoolDto);
    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param reportPoolDto 数据对象分享
     * @return ReportPoolDto 对象数据
     */
    @RequestMapping(value = "/quseryReportFinishStaffs", method = RequestMethod.POST)
    List<ReportPoolDto> quseryReportFinishStaffs(@RequestBody ReportPoolDto reportPoolDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param reportPoolDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/quseryReportFinishStaffCount", method = RequestMethod.POST)
    int quseryReportFinishStaffCount(@RequestBody ReportPoolDto reportPoolDto);
}
