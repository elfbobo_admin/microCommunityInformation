package com.java110.intf.cust;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.UserV1.UserV1Dto;
import com.java110.po.UserV1.UserV1Po;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IUserV1InnerServiceSMO
 * @Description 用戶管理接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-cust", configuration = {FeignConfiguration.class})
@RequestMapping("/userV1Api")
public interface IUserV1InnerServiceSMO {


    @RequestMapping(value = "/saveUserV1", method = RequestMethod.POST)
    public int saveUserV1(@RequestBody UserV1Po userV1Po);

    @RequestMapping(value = "/updateUserV1", method = RequestMethod.POST)
    public int updateUserV1(@RequestBody  UserV1Po userV1Po);

    @RequestMapping(value = "/deleteUserV1", method = RequestMethod.POST)
    public int deleteUserV1(@RequestBody  UserV1Po userV1Po);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param userV1Dto 数据对象分享
     * @return UserV1Dto 对象数据
     */
    @RequestMapping(value = "/queryUserV1s", method = RequestMethod.POST)
    List<UserV1Dto> queryUserV1s(@RequestBody UserV1Dto userV1Dto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param userV1Dto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryUserV1sCount", method = RequestMethod.POST)
    int queryUserV1sCount(@RequestBody UserV1Dto userV1Dto);
}
