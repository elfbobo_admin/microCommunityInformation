package com.java110.intf.gov;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govMeetingList.GovMeetingListDto;
import com.java110.po.govMeetingList.GovMeetingListPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovMeetingListInnerServiceSMO
 * @Description 会议列表接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-gov", configuration = {FeignConfiguration.class})
@RequestMapping("/govMeetingListApi")
public interface IGovMeetingListInnerServiceSMO {


    @RequestMapping(value = "/saveGovMeetingList", method = RequestMethod.POST)
    public int saveGovMeetingList(@RequestBody  GovMeetingListPo govMeetingListPo);

    @RequestMapping(value = "/updateGovMeetingList", method = RequestMethod.POST)
    public int updateGovMeetingList(@RequestBody  GovMeetingListPo govMeetingListPo);

    @RequestMapping(value = "/deleteGovMeetingList", method = RequestMethod.POST)
    public int deleteGovMeetingList(@RequestBody  GovMeetingListPo govMeetingListPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govMeetingListDto 数据对象分享
     * @return GovMeetingListDto 对象数据
     */
    @RequestMapping(value = "/queryGovMeetingLists", method = RequestMethod.POST)
    List<GovMeetingListDto> queryGovMeetingLists(@RequestBody GovMeetingListDto govMeetingListDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govMeetingListDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovMeetingListsCount", method = RequestMethod.POST)
    int queryGovMeetingListsCount(@RequestBody GovMeetingListDto govMeetingListDto);
}
