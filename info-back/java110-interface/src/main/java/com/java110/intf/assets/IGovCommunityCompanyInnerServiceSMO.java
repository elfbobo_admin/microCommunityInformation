package com.java110.intf.assets;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govCommunityCompany.GovCommunityCompanyDto;
import com.java110.po.govCommunityCompany.GovCommunityCompanyPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovCommunityCompanyInnerServiceSMO
 * @Description 小区位置接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-assets", configuration = {FeignConfiguration.class})
@RequestMapping("/govCommunityCompanyApi")
public interface IGovCommunityCompanyInnerServiceSMO {


    @RequestMapping(value = "/saveGovCommunityCompany", method = RequestMethod.POST)
    public int saveGovCommunityCompany(@RequestBody GovCommunityCompanyPo govCommunityCompanyPo);

    @RequestMapping(value = "/updateGovCommunityCompany", method = RequestMethod.POST)
    public int updateGovCommunityCompany(@RequestBody  GovCommunityCompanyPo govCommunityCompanyPo);

    @RequestMapping(value = "/deleteGovCommunityCompany", method = RequestMethod.POST)
    public int deleteGovCommunityCompany(@RequestBody  GovCommunityCompanyPo govCommunityCompanyPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govCommunityCompanyDto 数据对象分享
     * @return GovCommunityCompanyDto 对象数据
     */
    @RequestMapping(value = "/queryGovCommunityCompanys", method = RequestMethod.POST)
    List<GovCommunityCompanyDto> queryGovCommunityCompanys(@RequestBody GovCommunityCompanyDto govCommunityCompanyDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govCommunityCompanyDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovCommunityCompanysCount", method = RequestMethod.POST)
    int queryGovCommunityCompanysCount(@RequestBody GovCommunityCompanyDto govCommunityCompanyDto);
}
