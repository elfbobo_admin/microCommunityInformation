package com.java110.intf.cust;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govGridObjRel.GovGridObjRelDto;
import com.java110.po.govGridObjRel.GovGridObjRelPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovGridObjRelInnerServiceSMO
 * @Description 网格对象关系接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-cust", configuration = {FeignConfiguration.class})
@RequestMapping("/govGridObjRelApi")
public interface IGovGridObjRelInnerServiceSMO {


    @RequestMapping(value = "/saveGovGridObjRel", method = RequestMethod.POST)
    public int saveGovGridObjRel(@RequestBody  GovGridObjRelPo govGridObjRelPo);

    @RequestMapping(value = "/updateGovGridObjRel", method = RequestMethod.POST)
    public int updateGovGridObjRel(@RequestBody  GovGridObjRelPo govGridObjRelPo);

    @RequestMapping(value = "/deleteGovGridObjRel", method = RequestMethod.POST)
    public int deleteGovGridObjRel(@RequestBody  GovGridObjRelPo govGridObjRelPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govGridObjRelDto 数据对象分享
     * @return GovGridObjRelDto 对象数据
     */
    @RequestMapping(value = "/queryGovGridObjRels", method = RequestMethod.POST)
    List<GovGridObjRelDto> queryGovGridObjRels(@RequestBody GovGridObjRelDto govGridObjRelDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govGridObjRelDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovGridObjRelsCount", method = RequestMethod.POST)
    int queryGovGridObjRelsCount(@RequestBody GovGridObjRelDto govGridObjRelDto);
}
