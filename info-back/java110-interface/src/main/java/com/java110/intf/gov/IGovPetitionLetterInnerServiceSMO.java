package com.java110.intf.gov;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govPetitionLetter.GovPetitionLetterDto;
import com.java110.po.govPetitionLetter.GovPetitionLetterPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovPetitionLetterInnerServiceSMO
 * @Description 信访管理接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-gov", configuration = {FeignConfiguration.class})
@RequestMapping("/govPetitionLetterApi")
public interface IGovPetitionLetterInnerServiceSMO {


    @RequestMapping(value = "/saveGovPetitionLetter", method = RequestMethod.POST)
    public int saveGovPetitionLetter(@RequestBody  GovPetitionLetterPo govPetitionLetterPo);

    @RequestMapping(value = "/updateGovPetitionLetter", method = RequestMethod.POST)
    public int updateGovPetitionLetter(@RequestBody  GovPetitionLetterPo govPetitionLetterPo);

    @RequestMapping(value = "/deleteGovPetitionLetter", method = RequestMethod.POST)
    public int deleteGovPetitionLetter(@RequestBody  GovPetitionLetterPo govPetitionLetterPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govPetitionLetterDto 数据对象分享
     * @return GovPetitionLetterDto 对象数据
     */
    @RequestMapping(value = "/queryGovPetitionLetters", method = RequestMethod.POST)
    List<GovPetitionLetterDto> queryGovPetitionLetters(@RequestBody GovPetitionLetterDto govPetitionLetterDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govPetitionLetterDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovPetitionLettersCount", method = RequestMethod.POST)
    int queryGovPetitionLettersCount(@RequestBody GovPetitionLetterDto govPetitionLetterDto);
}
