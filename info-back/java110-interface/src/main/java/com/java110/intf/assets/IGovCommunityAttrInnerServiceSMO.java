package com.java110.intf.assets;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govCommunityAttr.GovCommunityAttrDto;
import com.java110.po.govCommunityAttr.GovCommunityAttrPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovCommunityAttrInnerServiceSMO
 * @Description 小区属性接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-assets", configuration = {FeignConfiguration.class})
@RequestMapping("/govCommunityAttrApi")
public interface IGovCommunityAttrInnerServiceSMO {


    @RequestMapping(value = "/saveGovCommunityAttr", method = RequestMethod.POST)
    public int saveGovCommunityAttr(@RequestBody  GovCommunityAttrPo govCommunityAttrPo);

    @RequestMapping(value = "/updateGovCommunityAttr", method = RequestMethod.POST)
    public int updateGovCommunityAttr(@RequestBody  GovCommunityAttrPo govCommunityAttrPo);

    @RequestMapping(value = "/deleteGovCommunityAttr", method = RequestMethod.POST)
    public int deleteGovCommunityAttr(@RequestBody  GovCommunityAttrPo govCommunityAttrPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govCommunityAttrDto 数据对象分享
     * @return GovCommunityAttrDto 对象数据
     */
    @RequestMapping(value = "/queryGovCommunityAttrs", method = RequestMethod.POST)
    List<GovCommunityAttrDto> queryGovCommunityAttrs(@RequestBody GovCommunityAttrDto govCommunityAttrDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govCommunityAttrDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovCommunityAttrsCount", method = RequestMethod.POST)
    int queryGovCommunityAttrsCount(@RequestBody GovCommunityAttrDto govCommunityAttrDto);
}
