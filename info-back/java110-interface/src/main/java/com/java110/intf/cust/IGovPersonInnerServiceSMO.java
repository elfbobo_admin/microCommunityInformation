package com.java110.intf.cust;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govPerson.GovPersonDto;
import com.java110.po.govPerson.GovPersonPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovPersonInnerServiceSMO
 * @Description 人口管理接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-cust", configuration = {FeignConfiguration.class})
@RequestMapping("/govPersonApi")
public interface IGovPersonInnerServiceSMO {


    @RequestMapping(value = "/saveGovPerson", method = RequestMethod.POST)
    public int saveGovPerson(@RequestBody GovPersonPo govPersonPo);

    @RequestMapping(value = "/updateGovPerson", method = RequestMethod.POST)
    public int updateGovPerson(@RequestBody  GovPersonPo govPersonPo);

    @RequestMapping(value = "/deleteGovPerson", method = RequestMethod.POST)
    public int deleteGovPerson(@RequestBody  GovPersonPo govPersonPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govPersonDto 数据对象分享
     * @return GovPersonDto 对象数据
     */
    @RequestMapping(value = "/queryGovPersons", method = RequestMethod.POST)
    List<GovPersonDto> queryGovPersons(@RequestBody GovPersonDto govPersonDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govPersonDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovPersonsCount", method = RequestMethod.POST)
    int queryGovPersonsCount(@RequestBody GovPersonDto govPersonDto);
    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govPersonDto 数据对象分享
     * @return GovPersonDto 对象数据
     */
    @RequestMapping(value = "/getGovPersonCompany", method = RequestMethod.POST)
    List<GovPersonDto> getGovPersonCompany(@RequestBody GovPersonDto govPersonDto);
    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govPersonDto 数据对象分享
     * @return GovPersonDto 对象数据
     */
    @RequestMapping(value = "/getGovPersonHomicide", method = RequestMethod.POST)
    List<GovPersonDto> getGovPersonHomicide(@RequestBody GovPersonDto govPersonDto);
    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govPersonDto 数据对象分享
     * @return GovPersonDto 对象数据
     */
    @RequestMapping(value = "/getGovPersonHelpPolicy", method = RequestMethod.POST)
    List<GovPersonDto> getGovPersonHelpPolicy(@RequestBody GovPersonDto govPersonDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govPersonDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/getGovPersonCompanyCount", method = RequestMethod.POST)
    int getGovPersonCompanyCount(@RequestBody GovPersonDto govPersonDto);
    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govPersonDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/getGovPersonHelpPolicyCount", method = RequestMethod.POST)
    int getGovPersonHelpPolicyCount(@RequestBody GovPersonDto govPersonDto);
    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govPersonDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/getGovPersonHomicideCount", method = RequestMethod.POST)
    int getGovPersonHomicideCount(@RequestBody GovPersonDto govPersonDto);
}
