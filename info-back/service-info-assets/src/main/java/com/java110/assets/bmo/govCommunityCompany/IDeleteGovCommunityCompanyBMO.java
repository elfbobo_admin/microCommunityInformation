package com.java110.assets.bmo.govCommunityCompany;
import com.java110.po.govCommunityCompany.GovCommunityCompanyPo;
import org.springframework.http.ResponseEntity;

public interface IDeleteGovCommunityCompanyBMO {


    /**
     * 修改小区位置
     * add by wuxw
     * @param govCommunityCompanyPo
     * @return
     */
    ResponseEntity<String> delete(GovCommunityCompanyPo govCommunityCompanyPo);


}
