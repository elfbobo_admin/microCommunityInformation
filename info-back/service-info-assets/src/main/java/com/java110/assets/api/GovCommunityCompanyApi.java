package com.java110.assets.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.assets.bmo.govCommunityCompany.IDeleteGovCommunityCompanyBMO;
import com.java110.assets.bmo.govCommunityCompany.IGetGovCommunityCompanyBMO;
import com.java110.assets.bmo.govCommunityCompany.ISaveGovCommunityCompanyBMO;
import com.java110.assets.bmo.govCommunityCompany.IUpdateGovCommunityCompanyBMO;
import com.java110.dto.govCommunityCompany.GovCommunityCompanyDto;
import com.java110.po.govCommunityCompany.GovCommunityCompanyPo;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govCommunityCompany")
public class GovCommunityCompanyApi {

    @Autowired
    private ISaveGovCommunityCompanyBMO saveGovCommunityCompanyBMOImpl;
    @Autowired
    private IUpdateGovCommunityCompanyBMO updateGovCommunityCompanyBMOImpl;
    @Autowired
    private IDeleteGovCommunityCompanyBMO deleteGovCommunityCompanyBMOImpl;

    @Autowired
    private IGetGovCommunityCompanyBMO getGovCommunityCompanyBMOImpl;

    /**
     * 微信保存消息模板
     * @serviceCode /govCommunityCompany/saveGovCommunityCompany
     * @path /app/govCommunityCompany/saveGovCommunityCompany
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/saveGovCommunityCompany", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovCommunityCompany(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "govOpId", "请求报文中未包含govOpId");
        Assert.hasKeyAndValue(reqJson, "govCommunityId", "请求报文中未包含govCommunityId");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "govCompanyId", "请求报文中未包含govCompanyId");


        GovCommunityCompanyPo govCommunityCompanyPo = BeanConvertUtil.covertBean(reqJson, GovCommunityCompanyPo.class);
        return saveGovCommunityCompanyBMOImpl.save(govCommunityCompanyPo);
    }

    /**
     * 微信修改消息模板
     * @serviceCode /govCommunityCompany/updateGovCommunityCompany
     * @path /app/govCommunityCompany/updateGovCommunityCompany
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/updateGovCommunityCompany", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovCommunityCompany(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "govOpId", "请求报文中未包含govOpId");
        Assert.hasKeyAndValue(reqJson, "govCommunityId", "请求报文中未包含govCommunityId");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "govCompanyId", "请求报文中未包含govCompanyId");


        GovCommunityCompanyPo govCommunityCompanyPo = BeanConvertUtil.covertBean(reqJson, GovCommunityCompanyPo.class);
        return updateGovCommunityCompanyBMOImpl.update(govCommunityCompanyPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govCommunityCompany/deleteGovCommunityCompany
     * @path /app/govCommunityCompany/deleteGovCommunityCompany
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/deleteGovCommunityCompany", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovCommunityCompany(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "govCommunityId", "小区ID不能为空");
        Assert.hasKeyAndValue(reqJson, "govCompanyId", "小区ID不能为空");
        Assert.hasKeyAndValue(reqJson, "govOpId", "govOpId不能为空");


        GovCommunityCompanyPo govCommunityCompanyPo = BeanConvertUtil.covertBean(reqJson, GovCommunityCompanyPo.class);
        return deleteGovCommunityCompanyBMOImpl.delete(govCommunityCompanyPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govCommunityCompany/queryGovCommunityCompany
     * @path /app/govCommunityCompany/queryGovCommunityCompany
     * @param govCommunityId 小区ID
     * @return
     */
    @RequestMapping(value = "/queryGovCommunityCompany", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovCommunityCompany(@RequestParam(value = "govCommunityId") String govCommunityId,
                                                      @RequestParam(value = "page") int page,
                                                      @RequestParam(value = "row") int row) {
        GovCommunityCompanyDto govCommunityCompanyDto = new GovCommunityCompanyDto();
        govCommunityCompanyDto.setPage(page);
        govCommunityCompanyDto.setRow(row);
        govCommunityCompanyDto.setGovCommunityId(govCommunityId);
        return getGovCommunityCompanyBMOImpl.get(govCommunityCompanyDto);
    }
}
