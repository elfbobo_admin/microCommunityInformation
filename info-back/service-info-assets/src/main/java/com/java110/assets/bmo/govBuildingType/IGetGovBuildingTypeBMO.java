package com.java110.assets.bmo.govBuildingType;
import com.java110.dto.govBuildingType.GovBuildingTypeDto;
import org.springframework.http.ResponseEntity;
public interface IGetGovBuildingTypeBMO {


    /**
     * 查询建筑分类
     * add by wuxw
     * @param  govBuildingTypeDto
     * @return
     */
    ResponseEntity<String> get(GovBuildingTypeDto govBuildingTypeDto);


}
