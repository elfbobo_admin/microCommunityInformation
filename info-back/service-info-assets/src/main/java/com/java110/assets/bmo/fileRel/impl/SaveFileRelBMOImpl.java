package com.java110.assets.bmo.fileRel.impl;

import com.java110.assets.bmo.fileRel.ISaveFileRelBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.intf.assets.IFileRelInnerServiceSMO;
import com.java110.po.fileRel.FileRelPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("saveFileRelBMOImpl")
public class SaveFileRelBMOImpl implements ISaveFileRelBMO {

    @Autowired
    private IFileRelInnerServiceSMO fileRelInnerServiceSMOImpl;

    /**
     * 添加小区信息
     *
     * @param fileRelPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> save(FileRelPo fileRelPo) {

        fileRelPo.setFileRelId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_fileRelId));
        int flag = fileRelInnerServiceSMOImpl.saveFileRel(fileRelPo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
