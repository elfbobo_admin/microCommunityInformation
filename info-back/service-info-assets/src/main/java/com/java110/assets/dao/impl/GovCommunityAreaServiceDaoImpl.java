package com.java110.assets.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.assets.dao.IGovCommunityAreaServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 区域管理服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govCommunityAreaServiceDaoImpl")
//@Transactional
public class GovCommunityAreaServiceDaoImpl extends BaseServiceDao implements IGovCommunityAreaServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovCommunityAreaServiceDaoImpl.class);





    /**
     * 保存区域管理信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovCommunityAreaInfo(Map info) throws DAOException {
        logger.debug("保存区域管理信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govCommunityAreaServiceDaoImpl.saveGovCommunityAreaInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存区域管理信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询区域管理信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovCommunityAreaInfo(Map info) throws DAOException {
        logger.debug("查询区域管理信息 入参 info : {}",info);

        List<Map> businessGovCommunityAreaInfos = sqlSessionTemplate.selectList("govCommunityAreaServiceDaoImpl.getGovCommunityAreaInfo",info);

        return businessGovCommunityAreaInfos;
    }
    /**
     * 查询区域管理信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovCommunityAreaStaff(Map info) throws DAOException {
        logger.debug("查询区域管理信息 入参 info : {}",info);

        List<Map> businessGovCommunityAreaInfos = sqlSessionTemplate.selectList("govCommunityAreaServiceDaoImpl.getGovCommunityAreaStaff",info);

        return businessGovCommunityAreaInfos;
    }


    /**
     * 修改区域管理信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovCommunityAreaInfo(Map info) throws DAOException {
        logger.debug("修改区域管理信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govCommunityAreaServiceDaoImpl.updateGovCommunityAreaInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改区域管理信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询区域管理数量
     * @param info 区域管理信息
     * @return 区域管理数量
     */
    @Override
    public int queryGovCommunityAreasCount(Map info) {
        logger.debug("查询区域管理数据 入参 info : {}",info);

        List<Map> businessGovCommunityAreaInfos = sqlSessionTemplate.selectList("govCommunityAreaServiceDaoImpl.queryGovCommunityAreasCount", info);
        if (businessGovCommunityAreaInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovCommunityAreaInfos.get(0).get("count").toString());
    }
     /**
     * 查询区域管理数量
     * @param info 区域管理信息
     * @return 区域管理数量
     */
    @Override
    public int getGovCommunityAreaStaffCount(Map info) {
        logger.debug("查询区域管理数据 入参 info : {}",info);

        List<Map> businessGovCommunityAreaInfos = sqlSessionTemplate.selectList("govCommunityAreaServiceDaoImpl.getGovCommunityAreaStaffCount", info);
        if (businessGovCommunityAreaInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovCommunityAreaInfos.get(0).get("count").toString());
    }


}
