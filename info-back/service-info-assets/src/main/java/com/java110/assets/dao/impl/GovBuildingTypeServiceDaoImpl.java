package com.java110.assets.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.assets.dao.IGovBuildingTypeServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 建筑分类服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govBuildingTypeServiceDaoImpl")
//@Transactional
public class GovBuildingTypeServiceDaoImpl extends BaseServiceDao implements IGovBuildingTypeServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovBuildingTypeServiceDaoImpl.class);





    /**
     * 保存建筑分类信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovBuildingTypeInfo(Map info) throws DAOException {
        logger.debug("保存建筑分类信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govBuildingTypeServiceDaoImpl.saveGovBuildingTypeInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存建筑分类信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询建筑分类信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovBuildingTypeInfo(Map info) throws DAOException {
        logger.debug("查询建筑分类信息 入参 info : {}",info);

        List<Map> businessGovBuildingTypeInfos = sqlSessionTemplate.selectList("govBuildingTypeServiceDaoImpl.getGovBuildingTypeInfo",info);

        return businessGovBuildingTypeInfos;
    }


    /**
     * 修改建筑分类信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovBuildingTypeInfo(Map info) throws DAOException {
        logger.debug("修改建筑分类信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govBuildingTypeServiceDaoImpl.updateGovBuildingTypeInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改建筑分类信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询建筑分类数量
     * @param info 建筑分类信息
     * @return 建筑分类数量
     */
    @Override
    public int queryGovBuildingTypesCount(Map info) {
        logger.debug("查询建筑分类数据 入参 info : {}",info);

        List<Map> businessGovBuildingTypeInfos = sqlSessionTemplate.selectList("govBuildingTypeServiceDaoImpl.queryGovBuildingTypesCount", info);
        if (businessGovBuildingTypeInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovBuildingTypeInfos.get(0).get("count").toString());
    }


}
