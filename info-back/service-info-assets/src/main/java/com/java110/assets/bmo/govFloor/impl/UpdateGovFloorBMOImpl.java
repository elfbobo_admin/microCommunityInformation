package com.java110.assets.bmo.govFloor.impl;


import com.java110.assets.bmo.govFloor.IUpdateGovFloorBMO;
import com.java110.core.annotation.Java110Transactional;

import com.java110.core.factory.GenerateCodeFactory;
import com.java110.dto.file.FileDto;
import com.java110.intf.assets.IFileRelInnerServiceSMO;
import com.java110.intf.assets.IGovFloorInnerServiceSMO;
import com.java110.intf.engine.IFileInnerServiceSMO;
import com.java110.po.fileRel.FileRelPo;
import com.java110.po.govFloor.GovFloorPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


@Service("updateGovFloorBMOImpl")
public class UpdateGovFloorBMOImpl implements IUpdateGovFloorBMO {

    @Autowired
    private IGovFloorInnerServiceSMO govFloorInnerServiceSMOImpl;
    @Autowired
    private IFileInnerServiceSMO fileInnerServiceSMOImpl;
    @Autowired
    private IFileRelInnerServiceSMO fileRelInnerServiceSMOImpl;
    /**
     *
     *
     * @param govFloorPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> update(GovFloorPo govFloorPo) {
        //删除原文件关系
        FileRelPo fileRelPo = new FileRelPo();
        fileRelPo.setObjId( govFloorPo.getGovFloorId()  );
        fileRelPo.setRelType("90200");
        fileRelInnerServiceSMOImpl.deleteFileRel( fileRelPo );

        fileRelPo = new FileRelPo();
        fileRelPo.setFileRelId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_fileRelId));
        fileRelPo.setFileName(govFloorPo.getFloorIcon());
        fileRelPo.setRelType("90200");
        fileRelPo.setObjId(govFloorPo.getGovFloorId());
        int flag = fileRelInnerServiceSMOImpl.saveFileRel(fileRelPo);
        if (flag < 1) {
            return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
        }

        govFloorPo.setFloorIcon( fileRelPo.getFileRelId() );
        flag = govFloorInnerServiceSMOImpl.updateGovFloor(govFloorPo);
        if (flag < 1) {
            return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
        }


        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
    }

}
