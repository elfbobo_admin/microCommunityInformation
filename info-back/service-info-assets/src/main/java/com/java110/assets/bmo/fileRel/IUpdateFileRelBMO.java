package com.java110.assets.bmo.fileRel;
import com.java110.po.fileRel.FileRelPo;
import org.springframework.http.ResponseEntity;

public interface IUpdateFileRelBMO {


    /**
     * 修改文件关系管理
     * add by wuxw
     * @param fileRelPo
     * @return
     */
    ResponseEntity<String> update(FileRelPo fileRelPo);


}
