package com.java110.assets.bmo.govCommunityAttr;
import org.springframework.http.ResponseEntity;
import com.java110.dto.govCommunityAttr.GovCommunityAttrDto;
public interface IGetGovCommunityAttrBMO {


    /**
     * 查询小区属性
     * add by wuxw
     * @param  govCommunityAttrDto
     * @return
     */
    ResponseEntity<String> get(GovCommunityAttrDto govCommunityAttrDto);


}
