package com.java110.assets.bmo.machine;
import com.alibaba.fastjson.JSONObject;
import org.springframework.http.ResponseEntity;
import com.java110.dto.machine.MachineDto;
public interface IGetMachineBMO {


    /**
     * 查询设备管理
     * add by wuxw
     * @param  machineDto
     * @return
     */
    ResponseEntity<String> get(MachineDto machineDto);
    /**
     * 查询设备管理
     * add by wuxw
     * @param  machineDto
     * @return
     */
    ResponseEntity<String> checkMachine(MachineDto machineDto);
    /**
     * 查询设备管理
     * add by wuxw
     * @param
     * @return
     */
    ResponseEntity<String> outMachine(JSONObject reqJson);
    /**
     * 查询设备管理
     * add by wuxw
     * @param
     * @return
     */
    ResponseEntity<String> clearMachine();


}
