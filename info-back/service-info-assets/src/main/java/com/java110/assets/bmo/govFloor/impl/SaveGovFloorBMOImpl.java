package com.java110.assets.bmo.govFloor.impl;

import com.java110.assets.bmo.govFloor.ISaveGovFloorBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.dto.file.FileDto;
import com.java110.intf.assets.IFileRelInnerServiceSMO;
import com.java110.intf.assets.IGovFloorInnerServiceSMO;
import com.java110.intf.engine.IFileInnerServiceSMO;
import com.java110.po.fileRel.FileRelPo;
import com.java110.po.govFloor.GovFloorPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


@Service("saveGovFloorBMOImpl")
public class SaveGovFloorBMOImpl implements ISaveGovFloorBMO {

    @Autowired
    private IGovFloorInnerServiceSMO govFloorInnerServiceSMOImpl;
    @Autowired
    private IFileInnerServiceSMO fileInnerServiceSMOImpl;
    @Autowired
    private IFileRelInnerServiceSMO fileRelInnerServiceSMOImpl;
    /**
     * 添加小区信息
     *
     * @param govFloorPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> save(GovFloorPo govFloorPo) {

        govFloorPo.setGovFloorId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_govFloorId));
        FileRelPo fileRelPo = new FileRelPo();
        fileRelPo.setFileRelId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_fileRelId));
        fileRelPo.setFileName(govFloorPo.getFloorIcon());
        fileRelPo.setRelType("90200");
        fileRelPo.setObjId(govFloorPo.getGovFloorId());
        int flag = fileRelInnerServiceSMOImpl.saveFileRel(fileRelPo);
        if (flag < 1) {
            return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
        }
        govFloorPo.setFloorIcon(fileRelPo.getFileRelId());

        flag = govFloorInnerServiceSMOImpl.saveGovFloor(govFloorPo);
        if (flag < 1) {
            return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
    }

}
