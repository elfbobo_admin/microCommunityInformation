package com.java110.assets.bmo.govCommunityArea.impl;

import com.java110.assets.bmo.govCommunityArea.IDeleteGovCommunityAreaBMO;
import com.java110.core.annotation.Java110Transactional;

import com.java110.intf.assets.IGovCommunityAreaInnerServiceSMO;
import com.java110.po.govCommunityArea.GovCommunityAreaPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("deleteGovCommunityAreaBMOImpl")
public class DeleteGovCommunityAreaBMOImpl implements IDeleteGovCommunityAreaBMO {

    @Autowired
    private IGovCommunityAreaInnerServiceSMO govCommunityAreaInnerServiceSMOImpl;

    /**
     * @param govCommunityAreaPo 数据
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> delete(GovCommunityAreaPo govCommunityAreaPo) {

        int flag = govCommunityAreaInnerServiceSMOImpl.deleteGovCommunityArea(govCommunityAreaPo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
