package com.java110.assets.bmo.govCommunityArea.impl;

import com.java110.assets.bmo.govCommunityArea.IGetGovCommunityAreaBMO;
import com.java110.intf.assets.IGovCommunityAreaInnerServiceSMO;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.dto.govCommunityArea.GovCommunityAreaDto;

import java.util.ArrayList;
import java.util.List;

@Service("getGovCommunityAreaBMOImpl")
public class GetGovCommunityAreaBMOImpl implements IGetGovCommunityAreaBMO {

    @Autowired
    private IGovCommunityAreaInnerServiceSMO govCommunityAreaInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govCommunityAreaDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovCommunityAreaDto govCommunityAreaDto) {


        int count = govCommunityAreaInnerServiceSMOImpl.queryGovCommunityAreasCount(govCommunityAreaDto);

        List<GovCommunityAreaDto> govCommunityAreaDtos = null;
        if (count > 0) {
            govCommunityAreaDtos = govCommunityAreaInnerServiceSMOImpl.queryGovCommunityAreas(govCommunityAreaDto);
        } else {
            govCommunityAreaDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govCommunityAreaDto.getRow()), count, govCommunityAreaDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }
    /**
     *
     *
     * @param  govCommunityAreaDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> getGovCommunityAreaStaff(GovCommunityAreaDto govCommunityAreaDto) {


        int count = govCommunityAreaInnerServiceSMOImpl.getGovCommunityAreaStaffCount(govCommunityAreaDto);

        List<GovCommunityAreaDto> govCommunityAreaDtos = null;
        if (count > 0) {
            govCommunityAreaDtos = govCommunityAreaInnerServiceSMOImpl.getGovCommunityAreaStaff(govCommunityAreaDto);
        } else {
            govCommunityAreaDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govCommunityAreaDto.getRow()), count, govCommunityAreaDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
