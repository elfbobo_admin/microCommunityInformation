package com.java110.assets.bmo.govFloor;
import com.java110.po.govFloor.GovFloorPo;
import org.springframework.http.ResponseEntity;

public interface IUpdateGovFloorBMO {


    /**
     * 修改建筑物管理
     * add by wuxw
     * @param govFloorPo
     * @return
     */
    ResponseEntity<String> update(GovFloorPo govFloorPo);


}
