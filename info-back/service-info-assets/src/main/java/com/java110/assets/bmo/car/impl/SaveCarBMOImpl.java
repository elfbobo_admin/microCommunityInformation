package com.java110.assets.bmo.car.impl;

import com.java110.assets.bmo.car.ISaveCarBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.dto.car.CarDto;
import com.java110.intf.assets.ICarInnerServiceSMO;
import com.java110.po.car.CarPo;
import com.java110.utils.util.StringUtil;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("saveCarBMOImpl")
public class SaveCarBMOImpl implements ISaveCarBMO {

    @Autowired
    private ICarInnerServiceSMO carInnerServiceSMOImpl;

    /**
     * 添加小区信息
     *
     * @param carPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> save(CarPo carPo) {

        carPo.setCarId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_carId));
        if (StringUtil.isEmpty(carPo.getDatasourceType())) {
            carPo.setDatasourceType(CarDto.SYSTEM_GOV_TYPE);
        }
        int flag = carInnerServiceSMOImpl.saveCar(carPo);

        if (flag > 0) {
            return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
