package com.java110.assets.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.assets.dao.IGovCommunityServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 小区信息服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govCommunityServiceDaoImpl")
//@Transactional
public class GovCommunityServiceDaoImpl extends BaseServiceDao implements IGovCommunityServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovCommunityServiceDaoImpl.class);





    /**
     * 保存小区信息信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovCommunityInfo(Map info) throws DAOException {
        logger.debug("保存小区信息信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govCommunityServiceDaoImpl.saveGovCommunityInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存小区信息信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询小区信息信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovCommunityInfo(Map info) throws DAOException {
        logger.debug("查询小区信息信息 入参 info : {}",info);

        List<Map> businessGovCommunityInfos = sqlSessionTemplate.selectList("govCommunityServiceDaoImpl.getGovCommunityInfo",info);

        return businessGovCommunityInfos;
    }
    /**
     * 查询小区信息信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovCommunityYm(Map info) throws DAOException {
        logger.debug("查询小区信息信息 入参 info : {}",info);

        List<Map> businessGovCommunityInfos = sqlSessionTemplate.selectList("govCommunityServiceDaoImpl.getGovCommunityYm",info);

        return businessGovCommunityInfos;
    }


    /**
     * 修改小区信息信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovCommunityInfo(Map info) throws DAOException {
        logger.debug("修改小区信息信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govCommunityServiceDaoImpl.updateGovCommunityInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改小区信息信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询小区信息数量
     * @param info 小区信息信息
     * @return 小区信息数量
     */
    @Override
    public int queryGovCommunitysCount(Map info) {
        logger.debug("查询小区信息数据 入参 info : {}",info);

        List<Map> businessGovCommunityInfos = sqlSessionTemplate.selectList("govCommunityServiceDaoImpl.queryGovCommunitysCount", info);
        if (businessGovCommunityInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovCommunityInfos.get(0).get("count").toString());
    }


}
