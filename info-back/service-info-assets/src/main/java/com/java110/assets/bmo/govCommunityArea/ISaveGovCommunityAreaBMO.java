package com.java110.assets.bmo.govCommunityArea;

import com.java110.po.govCommunityArea.GovCommunityAreaPo;
import org.springframework.http.ResponseEntity;
public interface ISaveGovCommunityAreaBMO {


    /**
     * 添加区域管理
     * add by wuxw
     * @param govCommunityAreaPo
     * @return
     */
    ResponseEntity<String> save(GovCommunityAreaPo govCommunityAreaPo);


}
