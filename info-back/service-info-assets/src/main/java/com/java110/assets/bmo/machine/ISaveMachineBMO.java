package com.java110.assets.bmo.machine;

import com.alibaba.fastjson.JSONObject;
import org.springframework.http.ResponseEntity;
import com.java110.po.machine.MachinePo;
public interface ISaveMachineBMO {


    /**
     * 添加设备管理
     * add by wuxw
     * @param
     * @return
     */
    ResponseEntity<String> save(JSONObject reqJson);


}
