package com.java110.assets.bmo.parkingArea.impl;

import com.java110.assets.bmo.parkingArea.IUpdateParkingAreaBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.intf.assets.IParkingAreaInnerServiceSMO;
import com.java110.po.parkingArea.ParkingAreaPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("updateParkingAreaBMOImpl")
public class UpdateParkingAreaBMOImpl implements IUpdateParkingAreaBMO {

    @Autowired
    private IParkingAreaInnerServiceSMO parkingAreaInnerServiceSMOImpl;

    /**
     * @param parkingAreaPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> update(ParkingAreaPo parkingAreaPo) {

        int flag = parkingAreaInnerServiceSMOImpl.updateParkingArea(parkingAreaPo);

        if (flag > 0) {
            return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
