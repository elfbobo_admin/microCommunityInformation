package com.java110.assets.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.assets.dao.ICityAreaServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 选择省市区服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("cityAreaServiceDaoImpl")
//@Transactional
public class CityAreaServiceDaoImpl extends BaseServiceDao implements ICityAreaServiceDao {

    private static Logger logger = LoggerFactory.getLogger(CityAreaServiceDaoImpl.class);





    /**
     * 保存选择省市区信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveCityAreaInfo(Map info) throws DAOException {
        logger.debug("保存选择省市区信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("cityAreaServiceDaoImpl.saveCityAreaInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存选择省市区信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询选择省市区信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getCityAreaInfo(Map info) throws DAOException {
        logger.debug("查询选择省市区信息 入参 info : {}",info);

        List<Map> businessCityAreaInfos = sqlSessionTemplate.selectList("cityAreaServiceDaoImpl.getCityAreaInfo",info);

        return businessCityAreaInfos;
    }

    /**
     * 查询选择省市区信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getAreas(Map info) throws DAOException {
        logger.debug("查询选择省市区信息 入参 info : {}",info);

        List<Map> businessCityAreaInfos = sqlSessionTemplate.selectList("cityAreaServiceDaoImpl.getAreas",info);

        return businessCityAreaInfos;
    }

    /**
     * 修改选择省市区信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateCityAreaInfo(Map info) throws DAOException {
        logger.debug("修改选择省市区信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("cityAreaServiceDaoImpl.updateCityAreaInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改选择省市区信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询选择省市区数量
     * @param info 选择省市区信息
     * @return 选择省市区数量
     */
    @Override
    public int queryCityAreasCount(Map info) {
        logger.debug("查询选择省市区数据 入参 info : {}",info);

        List<Map> businessCityAreaInfos = sqlSessionTemplate.selectList("cityAreaServiceDaoImpl.queryCityAreasCount", info);
        if (businessCityAreaInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessCityAreaInfos.get(0).get("count").toString());
    }


}
