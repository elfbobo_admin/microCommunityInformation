package com.java110.assets.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.machineType.MachineTypeDto;
import com.java110.po.machineType.MachineTypePo;
import com.java110.assets.bmo.machineType.IDeleteMachineTypeBMO;
import com.java110.assets.bmo.machineType.IGetMachineTypeBMO;
import com.java110.assets.bmo.machineType.ISaveMachineTypeBMO;
import com.java110.assets.bmo.machineType.IUpdateMachineTypeBMO;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/machineType")
public class MachineTypeApi {

    @Autowired
    private ISaveMachineTypeBMO saveMachineTypeBMOImpl;
    @Autowired
    private IUpdateMachineTypeBMO updateMachineTypeBMOImpl;
    @Autowired
    private IDeleteMachineTypeBMO deleteMachineTypeBMOImpl;

    @Autowired
    private IGetMachineTypeBMO getMachineTypeBMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /machineType/saveMachineType
     * @path /app/machineType/saveMachineType
     */
    @RequestMapping(value = "/saveMachineType", method = RequestMethod.POST)
    public ResponseEntity<String> saveMachineType(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "machineTypeCd", "请求报文中未包含machineTypeCd");
        Assert.hasKeyAndValue(reqJson, "machineTypeName", "请求报文中未包含machineTypeName");


        MachineTypePo machineTypePo = BeanConvertUtil.covertBean(reqJson, MachineTypePo.class);
        return saveMachineTypeBMOImpl.save(machineTypePo);
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /machineType/updateMachineType
     * @path /app/machineType/updateMachineType
     */
    @RequestMapping(value = "/updateMachineType", method = RequestMethod.POST)
    public ResponseEntity<String> updateMachineType(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "typeId", "请求报文中未包含typeId");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "machineTypeCd", "请求报文中未包含machineTypeCd");
        Assert.hasKeyAndValue(reqJson, "machineTypeName", "请求报文中未包含machineTypeName");


        MachineTypePo machineTypePo = BeanConvertUtil.covertBean(reqJson, MachineTypePo.class);
        return updateMachineTypeBMOImpl.update(machineTypePo);
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /machineType/deleteMachineType
     * @path /app/machineType/deleteMachineType
     */
    @RequestMapping(value = "/deleteMachineType", method = RequestMethod.POST)
    public ResponseEntity<String> deleteMachineType(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "caId", "caId不能为空");

        Assert.hasKeyAndValue(reqJson, "typeId", "typeId不能为空");


        MachineTypePo machineTypePo = BeanConvertUtil.covertBean(reqJson, MachineTypePo.class);
        return deleteMachineTypeBMOImpl.delete(machineTypePo);
    }

    /**
     * 微信删除消息模板
     *
     * @param caId 小区ID
     * @return
     * @serviceCode /machineType/queryMachineType
     * @path /app/machineType/queryMachineType
     */
    @RequestMapping(value = "/queryMachineType", method = RequestMethod.GET)
    public ResponseEntity<String> queryMachineType(@RequestParam(value = "caId") String caId,
                                                   @RequestParam(value = "machineTypeName" ,required = false) String machineTypeName,
                                                   @RequestParam(value = "machineTypeCd",required = false) String machineTypeCd,
                                                   @RequestParam(value = "page") int page,
                                                   @RequestParam(value = "row") int row) {
        MachineTypeDto machineTypeDto = new MachineTypeDto();
        machineTypeDto.setPage(page);
        machineTypeDto.setRow(row);
        machineTypeDto.setCaId(caId);
        machineTypeDto.setMachineTypeName(machineTypeName);
        machineTypeDto.setMachineTypeCd(machineTypeCd);
        return getMachineTypeBMOImpl.get(machineTypeDto);
    }
}
