package com.java110.assets.bmo.govBuildingType;
import com.java110.po.govBuildingType.GovBuildingTypePo;
import org.springframework.http.ResponseEntity;

public interface IDeleteGovBuildingTypeBMO {


    /**
     * 修改建筑分类
     * add by wuxw
     * @param govBuildingTypePo
     * @return
     */
    ResponseEntity<String> delete(GovBuildingTypePo govBuildingTypePo);


}
