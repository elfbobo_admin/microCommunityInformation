package com.java110.assets.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.assets.dao.IGovCommunityCompanyServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 小区位置服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govCommunityCompanyServiceDaoImpl")
//@Transactional
public class GovCommunityCompanyServiceDaoImpl extends BaseServiceDao implements IGovCommunityCompanyServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovCommunityCompanyServiceDaoImpl.class);





    /**
     * 保存小区位置信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovCommunityCompanyInfo(Map info) throws DAOException {
        logger.debug("保存小区位置信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govCommunityCompanyServiceDaoImpl.saveGovCommunityCompanyInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存小区位置信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询小区位置信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovCommunityCompanyInfo(Map info) throws DAOException {
        logger.debug("查询小区位置信息 入参 info : {}",info);

        List<Map> businessGovCommunityCompanyInfos = sqlSessionTemplate.selectList("govCommunityCompanyServiceDaoImpl.getGovCommunityCompanyInfo",info);

        return businessGovCommunityCompanyInfos;
    }


    /**
     * 修改小区位置信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovCommunityCompanyInfo(Map info) throws DAOException {
        logger.debug("修改小区位置信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govCommunityCompanyServiceDaoImpl.updateGovCommunityCompanyInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改小区位置信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询小区位置数量
     * @param info 小区位置信息
     * @return 小区位置数量
     */
    @Override
    public int queryGovCommunityCompanysCount(Map info) {
        logger.debug("查询小区位置数据 入参 info : {}",info);

        List<Map> businessGovCommunityCompanyInfos = sqlSessionTemplate.selectList("govCommunityCompanyServiceDaoImpl.queryGovCommunityCompanysCount", info);
        if (businessGovCommunityCompanyInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovCommunityCompanyInfos.get(0).get("count").toString());
    }


}
