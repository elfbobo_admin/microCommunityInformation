package com.java110.assets.bmo.govCommunityArea;
import com.java110.po.govCommunityArea.GovCommunityAreaPo;
import org.springframework.http.ResponseEntity;

public interface IUpdateGovCommunityAreaBMO {


    /**
     * 修改区域管理
     * add by wuxw
     * @param govCommunityAreaPo
     * @return
     */
    ResponseEntity<String> update(GovCommunityAreaPo govCommunityAreaPo);


}
