package com.java110.assets.bmo.parkingArea;
import com.java110.po.parkingArea.ParkingAreaPo;
import org.springframework.http.ResponseEntity;

public interface IDeleteParkingAreaBMO {


    /**
     * 修改停車場
     * add by wuxw
     * @param parkingAreaPo
     * @return
     */
    ResponseEntity<String> delete(ParkingAreaPo parkingAreaPo);


}
