package com.java110.assets.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.assets.bmo.parkingArea.IDeleteParkingAreaBMO;
import com.java110.assets.bmo.parkingArea.IGetParkingAreaBMO;
import com.java110.assets.bmo.parkingArea.ISaveParkingAreaBMO;
import com.java110.assets.bmo.parkingArea.IUpdateParkingAreaBMO;
import com.java110.dto.parkingArea.ParkingAreaDto;
import com.java110.po.parkingArea.ParkingAreaPo;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(value = "/parkingArea")
public class ParkingAreaApi {

    @Autowired
    private ISaveParkingAreaBMO saveParkingAreaBMOImpl;
    @Autowired
    private IUpdateParkingAreaBMO updateParkingAreaBMOImpl;
    @Autowired
    private IDeleteParkingAreaBMO deleteParkingAreaBMOImpl;

    @Autowired
    private IGetParkingAreaBMO getParkingAreaBMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /parkingArea/saveParkingArea
     * @path /app/parkingArea/saveParkingArea
     */
    @RequestMapping(value = "/saveParkingArea", method = RequestMethod.POST)
    public ResponseEntity<String> saveParkingArea(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "govCommunityId", "请求报文中未包含govCommunityId");
        Assert.hasKeyAndValue(reqJson, "num", "请求报文中未包含num");
        Assert.hasKeyAndValue(reqJson, "parkingCount", "请求报文中未包含parkingCount");
        Assert.hasKeyAndValue(reqJson, "typeCd", "请求报文中未包含typeCd");


        ParkingAreaPo parkingAreaPo = BeanConvertUtil.covertBean(reqJson, ParkingAreaPo.class);
        return saveParkingAreaBMOImpl.save(parkingAreaPo);
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /parkingArea/updateParkingArea
     * @path /app/parkingArea/updateParkingArea
     */
    @RequestMapping(value = "/updateParkingArea", method = RequestMethod.POST)
    public ResponseEntity<String> updateParkingArea(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "paId", "请求报文中未包含paId");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "govCommunityId", "请求报文中未包含govCommunityId");
        Assert.hasKeyAndValue(reqJson, "num", "请求报文中未包含num");
        Assert.hasKeyAndValue(reqJson, "parkingCount", "请求报文中未包含parkingCount");
        Assert.hasKeyAndValue(reqJson, "typeCd", "请求报文中未包含typeCd");


        ParkingAreaPo parkingAreaPo = BeanConvertUtil.covertBean(reqJson, ParkingAreaPo.class);
        return updateParkingAreaBMOImpl.update(parkingAreaPo);
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /parkingArea/deleteParkingArea
     * @path /app/parkingArea/deleteParkingArea
     */
    @RequestMapping(value = "/deleteParkingArea", method = RequestMethod.POST)
    public ResponseEntity<String> deleteParkingArea(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "caId", "区域ID不能为空");

        Assert.hasKeyAndValue(reqJson, "paId", "paId不能为空");


        ParkingAreaPo parkingAreaPo = BeanConvertUtil.covertBean(reqJson, ParkingAreaPo.class);
        return deleteParkingAreaBMOImpl.delete(parkingAreaPo);
    }

    /**
     * 微信删除消息模板
     *
     * @param caId 小区ID
     * @return
     * @serviceCode /parkingArea/queryParkingArea
     * @path /app/parkingArea/queryParkingArea
     */
    @RequestMapping(value = "/queryParkingArea", method = RequestMethod.GET)
    public ResponseEntity<String> queryParkingArea(@RequestParam(value = "caId") String caId,
                                                   @RequestParam(value = "page") int page,
                                                   @RequestParam(value = "row") int row) {
        ParkingAreaDto parkingAreaDto = new ParkingAreaDto();
        parkingAreaDto.setPage(page);
        parkingAreaDto.setRow(row);
        parkingAreaDto.setCaId(caId);
        return getParkingAreaBMOImpl.get(parkingAreaDto);
    }
}
