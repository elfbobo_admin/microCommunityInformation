package com.java110.assets.bmo.machineType;
import org.springframework.http.ResponseEntity;
import com.java110.dto.machineType.MachineTypeDto;
public interface IGetMachineTypeBMO {


    /**
     * 查询设备类型
     * add by wuxw
     * @param  machineTypeDto
     * @return
     */
    ResponseEntity<String> get(MachineTypeDto machineTypeDto);


}
