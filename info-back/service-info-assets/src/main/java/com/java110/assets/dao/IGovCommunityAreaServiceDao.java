package com.java110.assets.dao;


import com.java110.utils.exception.DAOException;


import java.util.List;
import java.util.Map;

/**
 * 区域管理组件内部之间使用，没有给外围系统提供服务能力
 * 区域管理服务接口类，要求全部以字符串传输，方便微服务化
 * 新建客户，修改客户，删除客户，查询客户等功能
 *
 * Created by wuxw on 2016/12/27.
 */
public interface IGovCommunityAreaServiceDao {


    /**
     * 保存 区域管理信息
     * @param info
     * @throws DAOException DAO异常
     */
    void saveGovCommunityAreaInfo(Map info) throws DAOException;




    /**
     * 查询区域管理信息（instance过程）
     * 根据bId 查询区域管理信息
     * @param info bId 信息
     * @return 区域管理信息
     * @throws DAOException DAO异常
     */
    List<Map> getGovCommunityAreaInfo(Map info) throws DAOException;
    /**
     * 查询区域管理信息（instance过程）
     * 根据bId 查询区域管理信息
     * @param info bId 信息
     * @return 区域管理信息
     * @throws DAOException DAO异常
     */
    List<Map> getGovCommunityAreaStaff(Map info) throws DAOException;



    /**
     * 修改区域管理信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    void updateGovCommunityAreaInfo(Map info) throws DAOException;


    /**
     * 查询区域管理总数
     *
     * @param info 区域管理信息
     * @return 区域管理数量
     */
    int queryGovCommunityAreasCount(Map info);

    /**
     * 查询区域管理总数
     *
     * @param info 区域管理信息
     * @return 区域管理数量
     */
    int getGovCommunityAreaStaffCount(Map info);

}
