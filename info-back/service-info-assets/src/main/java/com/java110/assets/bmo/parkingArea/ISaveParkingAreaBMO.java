package com.java110.assets.bmo.parkingArea;

import com.java110.po.parkingArea.ParkingAreaPo;
import org.springframework.http.ResponseEntity;
public interface ISaveParkingAreaBMO {


    /**
     * 添加停車場
     * add by wuxw
     * @param parkingAreaPo
     * @return
     */
    ResponseEntity<String> save(ParkingAreaPo parkingAreaPo);


}
