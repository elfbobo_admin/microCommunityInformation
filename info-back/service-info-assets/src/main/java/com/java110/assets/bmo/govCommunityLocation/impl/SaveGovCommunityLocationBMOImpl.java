package com.java110.assets.bmo.govCommunityLocation.impl;

import com.java110.assets.bmo.govCommunityLocation.ISaveGovCommunityLocationBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.dto.reportData.ReportDataHeaderDto;
import com.java110.intf.assets.IGovCommunityLocationInnerServiceSMO;
import com.java110.po.govCommunityLocation.GovCommunityLocationPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("saveGovCommunityLocationBMOImpl")
public class SaveGovCommunityLocationBMOImpl implements ISaveGovCommunityLocationBMO {

    @Autowired
    private IGovCommunityLocationInnerServiceSMO govCommunityLocationInnerServiceSMOImpl;

    /**
     * 添加小区信息
     *
     * @param govCommunityLocationPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> save(GovCommunityLocationPo govCommunityLocationPo) {

        govCommunityLocationPo.setLocationId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_locationId));
        govCommunityLocationPo.setDatasourceType(ReportDataHeaderDto.SYSTEM_GOV_TYPE);
        int flag = govCommunityLocationInnerServiceSMOImpl.saveGovCommunityLocation(govCommunityLocationPo);

        if (flag > 0) {
            return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
