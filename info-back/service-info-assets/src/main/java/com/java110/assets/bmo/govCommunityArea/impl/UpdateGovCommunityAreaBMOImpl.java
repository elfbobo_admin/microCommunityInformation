package com.java110.assets.bmo.govCommunityArea.impl;

import com.java110.assets.bmo.govCommunityArea.IUpdateGovCommunityAreaBMO;
import com.java110.core.annotation.Java110Transactional;

import com.java110.intf.assets.IGovCommunityAreaInnerServiceSMO;
import com.java110.po.govCommunityArea.GovCommunityAreaPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("updateGovCommunityAreaBMOImpl")
public class UpdateGovCommunityAreaBMOImpl implements IUpdateGovCommunityAreaBMO {

    @Autowired
    private IGovCommunityAreaInnerServiceSMO govCommunityAreaInnerServiceSMOImpl;

    /**
     *
     *
     * @param govCommunityAreaPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> update(GovCommunityAreaPo govCommunityAreaPo) {

        int flag = govCommunityAreaInnerServiceSMOImpl.updateGovCommunityArea(govCommunityAreaPo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
