package com.java110.assets.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.assets.dao.IFileRelServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 文件关系管理服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("fileRelServiceDaoImpl")
//@Transactional
public class FileRelServiceDaoImpl extends BaseServiceDao implements IFileRelServiceDao {

    private static Logger logger = LoggerFactory.getLogger(FileRelServiceDaoImpl.class);





    /**
     * 保存文件关系管理信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveFileRelInfo(Map info) throws DAOException {
        logger.debug("保存文件关系管理信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("fileRelServiceDaoImpl.saveFileRelInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存文件关系管理信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询文件关系管理信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getFileRelInfo(Map info) throws DAOException {
        logger.debug("查询文件关系管理信息 入参 info : {}",info);

        List<Map> businessFileRelInfos = sqlSessionTemplate.selectList("fileRelServiceDaoImpl.getFileRelInfo",info);

        return businessFileRelInfos;
    }


    /**
     * 修改文件关系管理信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateFileRelInfo(Map info) throws DAOException {
        logger.debug("修改文件关系管理信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("fileRelServiceDaoImpl.updateFileRelInfo",info);

    }

     /**
     * 查询文件关系管理数量
     * @param info 文件关系管理信息
     * @return 文件关系管理数量
     */
    @Override
    public int queryFileRelsCount(Map info) {
        logger.debug("查询文件关系管理数据 入参 info : {}",info);

        List<Map> businessFileRelInfos = sqlSessionTemplate.selectList("fileRelServiceDaoImpl.queryFileRelsCount", info);
        if (businessFileRelInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessFileRelInfos.get(0).get("count").toString());
    }


}
