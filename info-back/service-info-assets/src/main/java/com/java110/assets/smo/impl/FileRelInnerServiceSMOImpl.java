package com.java110.assets.smo.impl;


import com.java110.assets.dao.IFileRelServiceDao;
import com.java110.dto.fileRel.FileRelDto;
import com.java110.intf.assets.IFileRelInnerServiceSMO;
import com.java110.po.fileRel.FileRelPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 文件关系管理内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class FileRelInnerServiceSMOImpl extends BaseServiceSMO implements IFileRelInnerServiceSMO {

    @Autowired
    private IFileRelServiceDao fileRelServiceDaoImpl;


    @Override
    public int saveFileRel(@RequestBody FileRelPo fileRelPo) {
        int saveFlag = 1;
        fileRelServiceDaoImpl.saveFileRelInfo(BeanConvertUtil.beanCovertMap(fileRelPo));
        return saveFlag;
    }

     @Override
    public int updateFileRel(@RequestBody  FileRelPo fileRelPo) {
        int saveFlag = 1;
         fileRelServiceDaoImpl.updateFileRelInfo(BeanConvertUtil.beanCovertMap(fileRelPo));
        return saveFlag;
    }

     @Override
    public int deleteFileRel(@RequestBody  FileRelPo fileRelPo) {
        int saveFlag = 1;
        fileRelPo.setStatusCd("1");
        fileRelServiceDaoImpl.updateFileRelInfo(BeanConvertUtil.beanCovertMap(fileRelPo));
        return saveFlag;
    }

    @Override
    public List<FileRelDto> queryFileRels(@RequestBody  FileRelDto fileRelDto) {

        //校验是否传了 分页信息

        int page = fileRelDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            fileRelDto.setPage((page - 1) * fileRelDto.getRow());
        }

        List<FileRelDto> fileRels = BeanConvertUtil.covertBeanList(fileRelServiceDaoImpl.getFileRelInfo(BeanConvertUtil.beanCovertMap(fileRelDto)), FileRelDto.class);

        return fileRels;
    }


    @Override
    public int queryFileRelsCount(@RequestBody FileRelDto fileRelDto) {
        return fileRelServiceDaoImpl.queryFileRelsCount(BeanConvertUtil.beanCovertMap(fileRelDto));    }

    public IFileRelServiceDao getFileRelServiceDaoImpl() {
        return fileRelServiceDaoImpl;
    }

    public void setFileRelServiceDaoImpl(IFileRelServiceDao fileRelServiceDaoImpl) {
        this.fileRelServiceDaoImpl = fileRelServiceDaoImpl;
    }
}
