package com.java110.assets.bmo.govCommunityAttr.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.assets.bmo.govCommunityAttr.IGetGovCommunityAttrBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.assets.IGovCommunityAttrInnerServiceSMO;
import com.java110.dto.govCommunityAttr.GovCommunityAttrDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import org.springframework.http.HttpStatus;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service("getGovCommunityAttrBMOImpl")
public class GetGovCommunityAttrBMOImpl implements IGetGovCommunityAttrBMO {

    @Autowired
    private IGovCommunityAttrInnerServiceSMO govCommunityAttrInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govCommunityAttrDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovCommunityAttrDto govCommunityAttrDto) {


        int count = govCommunityAttrInnerServiceSMOImpl.queryGovCommunityAttrsCount(govCommunityAttrDto);

        List<GovCommunityAttrDto> govCommunityAttrDtos = null;
        if (count > 0) {
            govCommunityAttrDtos = govCommunityAttrInnerServiceSMOImpl.queryGovCommunityAttrs(govCommunityAttrDto);
        } else {
            govCommunityAttrDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govCommunityAttrDto.getRow()), count, govCommunityAttrDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
