package com.java110.assets.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.assets.dao.IMachineTypeServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 设备类型服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("machineTypeServiceDaoImpl")
//@Transactional
public class MachineTypeServiceDaoImpl extends BaseServiceDao implements IMachineTypeServiceDao {

    private static Logger logger = LoggerFactory.getLogger(MachineTypeServiceDaoImpl.class);





    /**
     * 保存设备类型信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveMachineTypeInfo(Map info) throws DAOException {
        logger.debug("保存设备类型信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("machineTypeServiceDaoImpl.saveMachineTypeInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存设备类型信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询设备类型信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getMachineTypeInfo(Map info) throws DAOException {
        logger.debug("查询设备类型信息 入参 info : {}",info);

        List<Map> businessMachineTypeInfos = sqlSessionTemplate.selectList("machineTypeServiceDaoImpl.getMachineTypeInfo",info);

        return businessMachineTypeInfos;
    }


    /**
     * 修改设备类型信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateMachineTypeInfo(Map info) throws DAOException {
        logger.debug("修改设备类型信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("machineTypeServiceDaoImpl.updateMachineTypeInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改设备类型信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询设备类型数量
     * @param info 设备类型信息
     * @return 设备类型数量
     */
    @Override
    public int queryMachineTypesCount(Map info) {
        logger.debug("查询设备类型数据 入参 info : {}",info);

        List<Map> businessMachineTypeInfos = sqlSessionTemplate.selectList("machineTypeServiceDaoImpl.queryMachineTypesCount", info);
        if (businessMachineTypeInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessMachineTypeInfos.get(0).get("count").toString());
    }


}
