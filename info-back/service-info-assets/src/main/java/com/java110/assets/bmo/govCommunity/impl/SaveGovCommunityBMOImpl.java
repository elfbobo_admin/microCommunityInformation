package com.java110.assets.bmo.govCommunity.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.assets.bmo.govCommunity.ISaveGovCommunityBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.client.RestTemplate;
import com.java110.core.factory.GenerateCodeFactory;

import com.java110.core.factory.RestTemplateFactory;
import com.java110.dto.fileRel.FileRelDto;
import com.java110.dto.govCommunityAttr.GovCommunityAttrDto;
import com.java110.intf.assets.IFileRelInnerServiceSMO;
import com.java110.intf.assets.IGovCommunityAttrInnerServiceSMO;
import com.java110.intf.assets.IGovCommunityInnerServiceSMO;
import com.java110.intf.engine.IFileInnerServiceSMO;
import com.java110.po.fileRel.FileRelPo;
import com.java110.po.govCommunity.GovCommunityPo;
import com.java110.po.govCommunityAttr.GovCommunityAttrPo;
import com.java110.utils.cache.MappingCache;
import com.java110.utils.constant.MallConstant;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.utils.util.PayUtil;
import com.java110.utils.util.PinYinUtil;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;

import java.util.List;


@Service("saveGovCommunityBMOImpl")
public class SaveGovCommunityBMOImpl implements ISaveGovCommunityBMO {

    @Autowired
    private IGovCommunityInnerServiceSMO govCommunityInnerServiceSMOImpl;
    @Autowired
    private IFileInnerServiceSMO fileInnerServiceSMOImpl;
    @Autowired
    private IFileRelInnerServiceSMO fileRelInnerServiceSMOImpl;
    @Autowired
    private IGovCommunityAttrInnerServiceSMO govCommunityAttrInnerServiceSMOImpl;
    @Autowired
    private RestTemplate outRestTemplate;

    /**
     * 添加小区信息
     *
     * @param
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> save(JSONObject reqJson) {

        GovCommunityPo govCommunityPo = BeanConvertUtil.covertBean(reqJson, GovCommunityPo.class);
        govCommunityPo.setGovCommunityId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_govCommunityId));
        FileRelPo fileRelPo = new FileRelPo();
        fileRelPo.setFileRelId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_fileRelId));
        fileRelPo.setFileName(govCommunityPo.getCommunityIcon());
        fileRelPo.setRelType("10000");
        fileRelPo.setObjId(govCommunityPo.getGovCommunityId());
        int flag = fileRelInnerServiceSMOImpl.saveFileRel(fileRelPo);
        if (flag < 1) {
            throw new IllegalArgumentException("保存文件关系失败");
        }
        govCommunityPo.setCommunityIcon(fileRelPo.getFileRelId());
        govCommunityPo.setCommunitySecure(PayUtil.makeUUID(15));
        govCommunityPo.setTopic(PinYinUtil.getFirstSpell(govCommunityPo.getCommunityName()));
        flag = govCommunityInnerServiceSMOImpl.saveGovCommunity(govCommunityPo);
        if (flag < 1) {
            throw new IllegalArgumentException("保存小区失败");
        }
        saveCommunityAttrs(govCommunityPo, GovCommunityAttrPo.COMMUNITY_ATTR_MAPX, govCommunityPo.getMapX());
        saveCommunityAttrs(govCommunityPo, GovCommunityAttrPo.COMMUNITY_ATTR_MAPY, govCommunityPo.getMapY());

        reqJson.put("govCommunityId", govCommunityPo.getGovCommunityId());
        ResponseEntity<String> responseEntity = RestTemplateFactory.restOutMallTemplate(reqJson, outRestTemplate, MallConstant.ADD_COMMUNITY_MALL, HttpMethod.POST);
        if (responseEntity != null) {
            if (responseEntity.getStatusCode() != HttpStatus.OK) {
                throw new IllegalArgumentException( responseEntity.getBody());
            }
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
    }

    private void saveCommunityAttrs(GovCommunityPo govCommunityPo, String attrCdType, String attrValue) {
        int flag;
        GovCommunityAttrPo govCommunityAttrPo = new GovCommunityAttrPo();
        govCommunityAttrPo.setAttrId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_attrId));
        govCommunityAttrPo.setGovCommunityId(govCommunityPo.getGovCommunityId());
        govCommunityAttrPo.setCaId(govCommunityPo.getCaId());
        govCommunityAttrPo.setSpecCd(attrCdType);
        govCommunityAttrPo.setValue(attrValue);

        GovCommunityAttrDto govCommunityAttrDto = new GovCommunityAttrDto();
        govCommunityAttrDto.setGovCommunityId(govCommunityPo.getGovCommunityId());
        govCommunityAttrDto.setCaId(govCommunityPo.getCaId());
        govCommunityAttrDto.setSpecCd(attrCdType);
        List<GovCommunityAttrDto> govCommunityAttrDtos = govCommunityAttrInnerServiceSMOImpl.queryGovCommunityAttrs(govCommunityAttrDto);
        if (govCommunityAttrDtos != null && govCommunityAttrDtos.size() > 0) {
            govCommunityAttrPo.setAttrId(govCommunityAttrDtos.get(0).getAttrId());
            flag = govCommunityAttrInnerServiceSMOImpl.deleteGovCommunityAttr(govCommunityAttrPo);
            if (flag < 1) {
                throw new IllegalArgumentException("清除小区坐标属性失败");
            }
        }
        flag = govCommunityAttrInnerServiceSMOImpl.saveGovCommunityAttr(govCommunityAttrPo);
        if (flag < 1) {
            throw new IllegalArgumentException("保存小区坐标属性失败");
        }
    }

}
