package com.java110.assets.api;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.java110.dto.machine.MachineDto;
import com.java110.po.machine.MachinePo;
import com.java110.assets.bmo.machine.IDeleteMachineBMO;
import com.java110.assets.bmo.machine.IGetMachineBMO;
import com.java110.assets.bmo.machine.ISaveMachineBMO;
import com.java110.assets.bmo.machine.IUpdateMachineBMO;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.utils.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/machine")
public class MachineApi {

    @Autowired
    private ISaveMachineBMO saveMachineBMOImpl;
    @Autowired
    private IUpdateMachineBMO updateMachineBMOImpl;
    @Autowired
    private IDeleteMachineBMO deleteMachineBMOImpl;

    @Autowired
    private IGetMachineBMO getMachineBMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /machine/saveMachine
     * @path /app/machine/saveMachine
     */
    @RequestMapping(value = "/saveMachine", method = RequestMethod.POST)
    public ResponseEntity<String> saveMachine(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "machineCode", "请求报文中未包含machineCode");
        Assert.hasKeyAndValue(reqJson, "machineVersion", "请求报文中未包含machineVersion");
        Assert.hasKeyAndValue(reqJson, "machineTypeCd", "请求报文中未包含machineTypeCd");
        Assert.hasKeyAndValue(reqJson, "govCommunityId", "请求报文中未包含govCommunityId");
        Assert.hasKeyAndValue(reqJson, "machineName", "请求报文中未包含machineName");
        Assert.hasKeyAndValue(reqJson, "authCode", "请求报文中未包含authCode");
        Assert.hasKeyAndValue(reqJson, "locationTypeCd", "请求报文中未包含locationTypeCd");
        Assert.hasKeyAndValue(reqJson, "locationObjId", "请求报文中未包含locationObjId");
        Assert.hasKeyAndValue(reqJson, "state", "请求报文中未包含state");
        Assert.hasKeyAndValue(reqJson, "isShow", "请求报文中未包含isShow");


        return saveMachineBMOImpl.save(reqJson);
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /machine/updateMachine
     * @path /app/machine/updateMachine
     */
    @RequestMapping(value = "/updateMachine", method = RequestMethod.POST)
    public ResponseEntity<String> updateMachine(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "machineId", "请求报文中未包含machineId");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "machineCode", "请求报文中未包含machineCode");
        Assert.hasKeyAndValue(reqJson, "machineVersion", "请求报文中未包含machineVersion");
        Assert.hasKeyAndValue(reqJson, "machineTypeCd", "请求报文中未包含machineTypeCd");
        Assert.hasKeyAndValue(reqJson, "govCommunityId", "请求报文中未包含govCommunityId");
        Assert.hasKeyAndValue(reqJson, "machineName", "请求报文中未包含machineName");
        Assert.hasKeyAndValue(reqJson, "authCode", "请求报文中未包含authCode");
        Assert.hasKeyAndValue(reqJson, "isShow", "请求报文中未包含isShow");

        return updateMachineBMOImpl.update(reqJson);
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /machine/deleteMachine
     * @path /app/machine/deleteMachine
     */
    @RequestMapping(value = "/deleteMachine", method = RequestMethod.POST)
    public ResponseEntity<String> deleteMachine(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "caId", "caId不能为空");

        Assert.hasKeyAndValue(reqJson, "machineId", "machineId不能为空");

        return deleteMachineBMOImpl.delete(reqJson);
    }

    /**
     * 微信删除消息模板
     *
     * @param caId 小区ID
     * @return
     * @serviceCode /machine/queryMachine
     * @path /app/machine/queryMachine
     */
    @RequestMapping(value = "/queryMachine", method = RequestMethod.GET)
    public ResponseEntity<String> queryMachine(@RequestParam(value = "caId") String caId,
                                               @RequestParam(value = "machineName",required = false) String machineName,
                                               @RequestParam(value = "machineCode",required = false) String machineCode,
                                               @RequestParam(value = "machineTypeCd",required = false) String machineTypeCd,
                                               @RequestParam(value = "govCommunityId",required = false) String govCommunityId,
                                               @RequestParam(value = "typeId",required = false) String typeId,
                                               @RequestParam(value = "isShow",required = false) String isShow,
                                               @RequestParam(value = "page") int page,
                                               @RequestParam(value = "row") int row) {

        MachineDto machineDto = new MachineDto();
        machineDto.setPage(page);
        machineDto.setRow(row);
        machineDto.setCaId(caId);
        machineDto.setMachineName(machineName);
        machineDto.setMachineCode(machineCode);
        machineDto.setMachineTypeCd(machineTypeCd);
        machineDto.setTypeId(typeId);
        machineDto.setGovCommunityId(govCommunityId);
        machineDto.setIsShow( isShow );
        return getMachineBMOImpl.get(machineDto);
    }
    /**
     * 微信删除消息模板
     *
     * @param caId 小区ID
     * @return
     * @serviceCode /machine/checkMachine
     * @path /app/machine/queryMachine
     */
    @RequestMapping(value = "/checkMachine", method = RequestMethod.GET)
    public ResponseEntity<String> checkMachine(@RequestParam(value = "caId") String caId,
                                               @RequestParam(value = "machineId",required = false) String machineId,
                                               @RequestParam(value = "clientId",required = false) String clientId,
                                               @RequestParam(value = "page") int page,
                                               @RequestParam(value = "row") int row) {

        MachineDto machineDto = new MachineDto();
        machineDto.setPage(page);
        machineDto.setRow(row);
        machineDto.setCaId(caId);
        machineDto.setMachineId(machineId);
        machineDto.setClientId( clientId );
        return getMachineBMOImpl.checkMachine(machineDto);
    }
    /**
     * 微信删除消息模板
     *
     * @param
     * @return
     * @serviceCode /machine/outMachine
     * @path /app/machine/queryMachine
     */
    @RequestMapping(value = "/outMachine", method = RequestMethod.POST)
    public ResponseEntity<String> outMachine(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "clientId", "clientId不能为空");
        JSONArray machines = reqJson.getJSONArray( "machines" );
        if(machines == null || machines.size() < 1){
            throw new IllegalArgumentException("未传递 设备对象，machines");
        }
        return getMachineBMOImpl.outMachine(reqJson);
    }
    /**
     * 微信删除消息模板
     *
     * @param
     * @return
     * @serviceCode /machine/clearMachine
     * @path /app/machine/clearMachine
     */
    @RequestMapping(value = "/clearMachine", method = RequestMethod.POST)
    public ResponseEntity<String> clearMachine(@RequestBody JSONObject reqJson) {

        return getMachineBMOImpl.clearMachine();
    }
}
