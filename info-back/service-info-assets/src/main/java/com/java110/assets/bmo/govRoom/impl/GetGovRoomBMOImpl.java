package com.java110.assets.bmo.govRoom.impl;

import com.java110.assets.bmo.govRoom.IGetGovRoomBMO;
import com.java110.dto.govOwnerPerson.GovOwnerPersonDto;
import com.java110.intf.assets.IGovRoomInnerServiceSMO;
import com.java110.intf.cust.IGovOwnerPersonInnerServiceSMO;
import com.java110.utils.util.Assert;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.dto.govRoom.GovRoomDto;

import java.util.ArrayList;
import java.util.List;

@Service("getGovRoomBMOImpl")
public class GetGovRoomBMOImpl implements IGetGovRoomBMO {

    @Autowired
    private IGovRoomInnerServiceSMO govRoomInnerServiceSMOImpl;
    @Autowired
    private IGovOwnerPersonInnerServiceSMO govOwnerPersonInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govRoomDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovRoomDto govRoomDto) {


        int count = govRoomInnerServiceSMOImpl.queryGovRoomsCount(govRoomDto);

        List<GovRoomDto> govRoomDtos = null;
        if (count > 0) {
            govRoomDtos = govRoomInnerServiceSMOImpl.queryGovRooms(govRoomDto);
        } else {
            govRoomDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govRoomDto.getRow()), count, govRoomDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }
    /**
     *
     *
     * @param  govRoomDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> getPersonOwner(GovRoomDto govRoomDto,String govPersonId) {

        GovOwnerPersonDto govOwnerPersonDto = new GovOwnerPersonDto();
        govOwnerPersonDto.setCaId( govRoomDto.getCaId() );
        govOwnerPersonDto.setGovPersonId( govPersonId );
        List<GovOwnerPersonDto> govOwnerPersonDtoList = govOwnerPersonInnerServiceSMOImpl.queryGovOwnerPersons( govOwnerPersonDto );
        Assert.isNotNull( govOwnerPersonDtoList,"未查询到户籍信息" );
        govRoomDto.setOwnerId( govOwnerPersonDtoList.get( 0 ).getGovOwnerId() );
        int count = govRoomInnerServiceSMOImpl.queryGovRoomsCount(govRoomDto);

        List<GovRoomDto> govRoomDtos = null;
        if (count > 0) {
            govRoomDtos = govRoomInnerServiceSMOImpl.queryGovRooms(govRoomDto);
        } else {
            govRoomDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govRoomDto.getRow()), count, govRoomDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
