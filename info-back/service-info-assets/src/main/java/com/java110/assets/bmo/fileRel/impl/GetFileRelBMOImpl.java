package com.java110.assets.bmo.fileRel.impl;

import com.java110.assets.bmo.fileRel.IGetFileRelBMO;
import com.java110.intf.assets.IFileRelInnerServiceSMO;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.dto.fileRel.FileRelDto;

import java.util.ArrayList;
import java.util.List;

@Service("getFileRelBMOImpl")
public class GetFileRelBMOImpl implements IGetFileRelBMO {

    @Autowired
    private IFileRelInnerServiceSMO fileRelInnerServiceSMOImpl;

    /**
     *
     *
     * @param  fileRelDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(FileRelDto fileRelDto) {


        int count = fileRelInnerServiceSMOImpl.queryFileRelsCount(fileRelDto);

        List<FileRelDto> fileRelDtos = null;
        if (count > 0) {
            fileRelDtos = fileRelInnerServiceSMOImpl.queryFileRels(fileRelDto);
        } else {
            fileRelDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) fileRelDto.getRow()), count, fileRelDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
