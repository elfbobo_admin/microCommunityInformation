package com.java110.assets.bmo.govBuildingType.impl;

import com.java110.assets.bmo.govBuildingType.IDeleteGovBuildingTypeBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.intf.assets.IGovBuildingTypeInnerServiceSMO;
import com.java110.po.govBuildingType.GovBuildingTypePo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("deleteGovBuildingTypeBMOImpl")
public class DeleteGovBuildingTypeBMOImpl implements IDeleteGovBuildingTypeBMO {

    @Autowired
    private IGovBuildingTypeInnerServiceSMO govBuildingTypeInnerServiceSMOImpl;

    /**
     * @param govBuildingTypePo 数据
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> delete(GovBuildingTypePo govBuildingTypePo) {

        int flag = govBuildingTypeInnerServiceSMOImpl.deleteGovBuildingType(govBuildingTypePo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
