package com.java110.core.factory;

import com.alibaba.fastjson.JSONObject;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.utils.cache.MappingCache;
import com.java110.utils.constant.MallConstant;
import com.java110.utils.util.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.util.MultiValueMap;
import com.java110.core.client.RestTemplate;

import java.util.Map;

public class RestTemplateFactory {
    private static final Logger logger = LoggerFactory.getLogger(RestTemplateFactory.class);

    public static ResponseEntity restOutMallTemplate(JSONObject reqJson, RestTemplate outRestTemplate, String patch, HttpMethod httpMethod) {

        ResponseEntity<String> responseEntity = null;
        String isSend = MappingCache.getValue("MALL", MallConstant.MALL_IS_SEND);
        String url = MallConstant.getUrl(patch);
        if(httpMethod == HttpMethod.GET || httpMethod == HttpMethod.DELETE){
           url+=  mapToUrlParam(reqJson);
        }
        if ("true".equals(isSend)) {
            HttpEntity<MultiValueMap<String, Object>> httpEntity = new HttpEntity(reqJson.toJSONString(), getHeaders(outRestTemplate));
            logger.debug("RestTemplateFactory url:{}：请求参数:{}", url, httpEntity.toString());
            responseEntity = outRestTemplate.exchange(url, httpMethod, httpEntity, String.class);
        }
        return responseEntity;
    }
    public static ResponseEntity restOutSipTemplate(JSONObject reqJson, RestTemplate outRestTemplate, String patch, HttpMethod httpMethod) {

        ResponseEntity<String> responseEntity = null;
        String isSend = MappingCache.getValue("MALL", MallConstant.MALL_IS_SEND);
        String url = MallConstant.getSipUrl(patch);
        if(httpMethod == HttpMethod.GET || httpMethod == HttpMethod.DELETE){
           url+=  mapToUrlParam(reqJson);
        }
        if ("true".equals(isSend)) {
            HttpEntity<MultiValueMap<String, Object>> httpEntity = new HttpEntity(reqJson.toJSONString(), getHeaders(outRestTemplate));
            logger.debug("RestTemplateFactory url:{}：请求参数:{}", url, httpEntity.toString());
            responseEntity = outRestTemplate.exchange(url, httpMethod, httpEntity, String.class);
        }
        return responseEntity;
    }

    /**
     * map 参数转 url get 参数 非空值转为get参数 空值忽略
     *
     * @param info map数据
     * @return url get 参数 带？
     */
    protected static String mapToUrlParam(Map info) {
        String urlParam = "";
        if (info == null || info.isEmpty()) {
            return urlParam;
        }

        urlParam += "?";

        for (Object key : info.keySet()) {
            if (StringUtil.isNullOrNone(info.get(key))) {
                continue;
            }

            urlParam += (key + "=" + info.get(key) + "&");
        }

        urlParam = urlParam.endsWith("&") ? urlParam.substring(0, urlParam.length() - 1) : urlParam;

        return urlParam;
    }

    /**
     * 封装头信息
     *
     * @return
     */
    protected static HttpHeaders getHeaders(RestTemplate outRestTemplate) {
        HttpHeaders httpHeaders = new HttpHeaders();
        //httpHeaders.add("access_token", GetToken.get(outRestTemplate, false));
        //httpHeaders.add("Content-Type", "application/x-www-form-urlencoded");
        httpHeaders.add("Content-Type", "application/json");
        return httpHeaders;
    }
}
