package com.java110.po.govAreaRenovation;

import java.io.Serializable;
import java.util.Date;

public class GovAreaRenovationPo implements Serializable {

    private String leadParticipate;
private String securityProblem;
private String crackSecurity;
private String statusCd = "0";
private String leadRamark;
private String datasourceType;
private String govRenovationId;
private String caId;
private String name;
private String leadCompany;
private String typeId;
private String securityRange;
private String tel;
private String startTime;
private String securityKey;
private String endTime;
private String crackCriminal;
public String getLeadParticipate() {
        return leadParticipate;
    }
public void setLeadParticipate(String leadParticipate) {
        this.leadParticipate = leadParticipate;
    }
public String getSecurityProblem() {
        return securityProblem;
    }
public void setSecurityProblem(String securityProblem) {
        this.securityProblem = securityProblem;
    }
public String getCrackSecurity() {
        return crackSecurity;
    }
public void setCrackSecurity(String crackSecurity) {
        this.crackSecurity = crackSecurity;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getLeadRamark() {
        return leadRamark;
    }
public void setLeadRamark(String leadRamark) {
        this.leadRamark = leadRamark;
    }
public String getDatasourceType() {
        return datasourceType;
    }
public void setDatasourceType(String datasourceType) {
        this.datasourceType = datasourceType;
    }
public String getGovRenovationId() {
        return govRenovationId;
    }
public void setGovRenovationId(String govRenovationId) {
        this.govRenovationId = govRenovationId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getName() {
        return name;
    }
public void setName(String name) {
        this.name = name;
    }
public String getLeadCompany() {
        return leadCompany;
    }
public void setLeadCompany(String leadCompany) {
        this.leadCompany = leadCompany;
    }
public String getTypeId() {
        return typeId;
    }
public void setTypeId(String typeId) {
        this.typeId = typeId;
    }
public String getSecurityRange() {
        return securityRange;
    }
public void setSecurityRange(String securityRange) {
        this.securityRange = securityRange;
    }
public String getTel() {
        return tel;
    }
public void setTel(String tel) {
        this.tel = tel;
    }
public String getStartTime() {
        return startTime;
    }
public void setStartTime(String startTime) {
        this.startTime = startTime;
    }
public String getSecurityKey() {
        return securityKey;
    }
public void setSecurityKey(String securityKey) {
        this.securityKey = securityKey;
    }
public String getEndTime() {
        return endTime;
    }
public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
public String getCrackCriminal() {
        return crackCriminal;
    }
public void setCrackCriminal(String crackCriminal) {
        this.crackCriminal = crackCriminal;
    }

    private Date createTime;

     public Date getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Date createTime) {
            this.createTime = createTime;
        }


}
