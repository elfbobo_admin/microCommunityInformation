package com.java110.po.govPersonLabelRel;

import java.io.Serializable;
import java.util.Date;

public class GovPersonLabelRelPo implements Serializable {

    private String labelRelId;
private String govPersonId;
private String caId;
private String statusCd = "0";
private String labelCd;
private String [] labelCds;
public String getLabelRelId() {
        return labelRelId;
    }
public void setLabelRelId(String labelRelId) {
        this.labelRelId = labelRelId;
    }
public String getGovPersonId() {
        return govPersonId;
    }
public void setGovPersonId(String govPersonId) {
        this.govPersonId = govPersonId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getLabelCd() {
        return labelCd;
    }
public void setLabelCd(String labelCd) {
        this.labelCd = labelCd;
    }

    private Date createTime;

     public Date getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Date createTime) {
            this.createTime = createTime;
        }

    public String[] getLabelCds() {
        return labelCds;
    }

    public void setLabelCds(String[] labelCds) {
        this.labelCds = labelCds;
    }
}
