package com.java110.po.govOldPersonAttr;

import java.io.Serializable;
import java.util.Date;

public class GovOldPersonAttrPo implements Serializable {

    public static  final String OLD_TIME_AMOUNT = "time_amount";

    private String attrId;
private String specCd;
private String oldId;
private String value;
public String getAttrId() {
        return attrId;
    }
public void setAttrId(String attrId) {
        this.attrId = attrId;
    }
public String getSpecCd() {
        return specCd;
    }
public void setSpecCd(String specCd) {
        this.specCd = specCd;
    }
public String getOldId() {
        return oldId;
    }
public void setOldId(String oldId) {
        this.oldId = oldId;
    }
public String getValue() {
        return value;
    }
public void setValue(String value) {
        this.value = value;
    }

    private Date createTime;

    private String statusCd = "0";

     public Date getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Date createTime) {
            this.createTime = createTime;
        }

        public String getStatusCd() {
            return statusCd;
        }

        public void setStatusCd(String statusCd) {
            this.statusCd = statusCd;
        }

}
