package com.java110.po.govHealthAnswerValue;

import java.io.Serializable;
import java.util.Date;

public class GovHealthAnswerValuePo implements Serializable {

    private String doctor;
private String doctorRemark;
private String valueId;
private String healthPersonId;
private String personTitleId;
private String titleId;
private String caId;
private String healthId;
private String valueContent;
private String personId;
private String statusCd = "0";
public String getDoctor() {
        return doctor;
    }
public void setDoctor(String doctor) {
        this.doctor = doctor;
    }
public String getDoctorRemark() {
        return doctorRemark;
    }
public void setDoctorRemark(String doctorRemark) {
        this.doctorRemark = doctorRemark;
    }
public String getValueId() {
        return valueId;
    }
public void setValueId(String valueId) {
        this.valueId = valueId;
    }
public String getHealthPersonId() {
        return healthPersonId;
    }
public void setHealthPersonId(String healthPersonId) {
        this.healthPersonId = healthPersonId;
    }
public String getPersonTitleId() {
        return personTitleId;
    }
public void setPersonTitleId(String personTitleId) {
        this.personTitleId = personTitleId;
    }
public String getTitleId() {
        return titleId;
    }
public void setTitleId(String titleId) {
        this.titleId = titleId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getHealthId() {
        return healthId;
    }
public void setHealthId(String healthId) {
        this.healthId = healthId;
    }
public String getValueContent() {
        return valueContent;
    }
public void setValueContent(String valueContent) {
        this.valueContent = valueContent;
    }
public String getPersonId() {
        return personId;
    }
public void setPersonId(String personId) {
        this.personId = personId;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }



}
