package com.java110.po.govReportReturnVisit;

import java.io.Serializable;
import java.util.Date;

public class GovReportReturnVisitPo implements Serializable {

    private String visitId;
private String reportId;
private String caId;
private String context;
private String statusCd = "0";
private String visitPersonName;
private String visitPersonId;
private String visitType;
public String getVisitId() {
        return visitId;
    }
public void setVisitId(String visitId) {
        this.visitId = visitId;
    }
public String getReportId() {
        return reportId;
    }
public void setReportId(String reportId) {
        this.reportId = reportId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getContext() {
        return context;
    }
public void setContext(String context) {
        this.context = context;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getVisitPersonName() {
        return visitPersonName;
    }
public void setVisitPersonName(String visitPersonName) {
        this.visitPersonName = visitPersonName;
    }
public String getVisitPersonId() {
        return visitPersonId;
    }
public void setVisitPersonId(String visitPersonId) {
        this.visitPersonId = visitPersonId;
    }
public String getVisitType() {
        return visitType;
    }
public void setVisitType(String visitType) {
        this.visitType = visitType;
    }



}
