package com.java110.po.uOrgCommunityV1;

import java.io.Serializable;
import java.util.Date;

public class UOrgCommunityV1Po implements Serializable {

    private String orgName;
private String communityName;
private String statusCd = "0";
private String communityId;
private String storeId;
private String orgId;
private String orgCommunityId;
private String bId;

public String getOrgName() {
        return orgName;
    }
public void setOrgName(String orgName) {
        this.orgName = orgName;
    }
public String getCommunityName() {
        return communityName;
    }
public void setCommunityName(String communityName) {
        this.communityName = communityName;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getCommunityId() {
        return communityId;
    }
public void setCommunityId(String communityId) {
        this.communityId = communityId;
    }
public String getStoreId() {
        return storeId;
    }
public void setStoreId(String storeId) {
        this.storeId = storeId;
    }
public String getOrgId() {
        return orgId;
    }
public void setOrgId(String orgId) {
        this.orgId = orgId;
    }
public String getOrgCommunityId() {
        return orgCommunityId;
    }
public void setOrgCommunityId(String orgCommunityId) {
        this.orgCommunityId = orgCommunityId;
    }

    public void setbId(String bId) {
        this.bId = bId;
    }

    public String getbId() {
        return bId;
    }
}
