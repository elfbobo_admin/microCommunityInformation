package com.java110.po.govMedicalDoctorRel;

import java.io.Serializable;
import java.util.Date;

public class GovMedicalDoctorRelPo implements Serializable {

    private String relId;
private String caId;
private String groupId;
private String name;
private String govDoctorId;
private String statusCd = "0";
private String ramark;
public String getRelId() {
        return relId;
    }
public void setRelId(String relId) {
        this.relId = relId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getGroupId() {
        return groupId;
    }
public void setGroupId(String groupId) {
        this.groupId = groupId;
    }
public String getName() {
        return name;
    }
public void setName(String name) {
        this.name = name;
    }
public String getGovDoctorId() {
        return govDoctorId;
    }
public void setGovDoctorId(String govDoctorId) {
        this.govDoctorId = govDoctorId;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getRamark() {
        return ramark;
    }
public void setRamark(String ramark) {
        this.ramark = ramark;
    }

    private Date createTime;

     public Date getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Date createTime) {
            this.createTime = createTime;
        }


}
