package com.java110.po.govSchool;

import java.io.Serializable;
import java.util.Date;

public class GovSchoolPo implements Serializable {

    private String masterTel;
private String safetyLeader;
private String schoolmaster;
private String schoolType;
private String leaderTel;
private String schoolAddress;
private String statusCd = "0";
private String partSafetyLeader;
private String securityLeader;
private String partLeaderTel;
private String areaCode;
private String securityPersonnelNum;
private String schoolId;
private String caId;
private String schoolName;
private String studentNum;
private String securityLeaderTel;
public String getMasterTel() {
        return masterTel;
    }
public void setMasterTel(String masterTel) {
        this.masterTel = masterTel;
    }
public String getSafetyLeader() {
        return safetyLeader;
    }
public void setSafetyLeader(String safetyLeader) {
        this.safetyLeader = safetyLeader;
    }
public String getSchoolmaster() {
        return schoolmaster;
    }
public void setSchoolmaster(String schoolmaster) {
        this.schoolmaster = schoolmaster;
    }
public String getSchoolType() {
        return schoolType;
    }
public void setSchoolType(String schoolType) {
        this.schoolType = schoolType;
    }
public String getLeaderTel() {
        return leaderTel;
    }
public void setLeaderTel(String leaderTel) {
        this.leaderTel = leaderTel;
    }
public String getSchoolAddress() {
        return schoolAddress;
    }
public void setSchoolAddress(String schoolAddress) {
        this.schoolAddress = schoolAddress;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getPartSafetyLeader() {
        return partSafetyLeader;
    }
public void setPartSafetyLeader(String partSafetyLeader) {
        this.partSafetyLeader = partSafetyLeader;
    }
public String getSecurityLeader() {
        return securityLeader;
    }
public void setSecurityLeader(String securityLeader) {
        this.securityLeader = securityLeader;
    }
public String getPartLeaderTel() {
        return partLeaderTel;
    }
public void setPartLeaderTel(String partLeaderTel) {
        this.partLeaderTel = partLeaderTel;
    }
public String getAreaCode() {
        return areaCode;
    }
public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }
public String getSecurityPersonnelNum() {
        return securityPersonnelNum;
    }
public void setSecurityPersonnelNum(String securityPersonnelNum) {
        this.securityPersonnelNum = securityPersonnelNum;
    }
public String getSchoolId() {
        return schoolId;
    }
public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getSchoolName() {
        return schoolName;
    }
public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }
public String getStudentNum() {
        return studentNum;
    }
public void setStudentNum(String studentNum) {
        this.studentNum = studentNum;
    }
public String getSecurityLeaderTel() {
        return securityLeaderTel;
    }
public void setSecurityLeaderTel(String securityLeaderTel) {
        this.securityLeaderTel = securityLeaderTel;
    }

    private Date createTime;

     public Date getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Date createTime) {
            this.createTime = createTime;
        }


}
