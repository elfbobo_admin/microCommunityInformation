package com.java110.po.car;

import java.io.Serializable;
import java.util.Date;

public class CarPo implements Serializable {

    private String carBrand;
private String govCommunityId;
private String carNum;
private String remark;
private String statusCd = "0";
private String carTypeCd;
private String carId;
private String datasourceType;
private String carColor;
private String carType;
private String govPersonId;
private String caId;
private String paId;
private String startTime;
private String endTime;
public String getCarBrand() {
        return carBrand;
    }
public void setCarBrand(String carBrand) {
        this.carBrand = carBrand;
    }
public String getGovCommunityId() {
        return govCommunityId;
    }
public void setGovCommunityId(String govCommunityId) {
        this.govCommunityId = govCommunityId;
    }
public String getCarNum() {
        return carNum;
    }
public void setCarNum(String carNum) {
        this.carNum = carNum;
    }
public String getRemark() {
        return remark;
    }
public void setRemark(String remark) {
        this.remark = remark;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getCarTypeCd() {
        return carTypeCd;
    }
public void setCarTypeCd(String carTypeCd) {
        this.carTypeCd = carTypeCd;
    }
public String getCarId() {
        return carId;
    }
public void setCarId(String carId) {
        this.carId = carId;
    }
public String getDatasourceType() {
        return datasourceType;
    }
public void setDatasourceType(String datasourceType) {
        this.datasourceType = datasourceType;
    }
public String getCarColor() {
        return carColor;
    }
public void setCarColor(String carColor) {
        this.carColor = carColor;
    }
public String getCarType() {
        return carType;
    }
public void setCarType(String carType) {
        this.carType = carType;
    }
public String getGovPersonId() {
        return govPersonId;
    }
public void setGovPersonId(String govPersonId) {
        this.govPersonId = govPersonId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getPaId() {
        return paId;
    }
public void setPaId(String paId) {
        this.paId = paId;
    }
public String getStartTime() {
        return startTime;
    }
public void setStartTime(String startTime) {
        this.startTime = startTime;
    }
public String getEndTime() {
        return endTime;
    }
public void setEndTime(String endTime) {
        this.endTime = endTime;
    }



}
