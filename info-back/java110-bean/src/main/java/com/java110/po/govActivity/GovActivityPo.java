package com.java110.po.govActivity;

import java.io.Serializable;
import java.util.Date;

public class GovActivityPo implements Serializable {

    private String actAddress;
private String contactName;
private String contactLink;
private String caId;
private String actId;
private String typeId;
private String context;
private String actTime;
private String statusCd = "0";
private String actName;
private String personCount;
public String getActAddress() {
        return actAddress;
    }
public void setActAddress(String actAddress) {
        this.actAddress = actAddress;
    }
public String getContactName() {
        return contactName;
    }
public void setContactName(String contactName) {
        this.contactName = contactName;
    }
public String getContactLink() {
        return contactLink;
    }
public void setContactLink(String contactLink) {
        this.contactLink = contactLink;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getActId() {
        return actId;
    }
public void setActId(String actId) {
        this.actId = actId;
    }
public String getTypeId() {
    return typeId;
}
public void setTypeId(String typeId) {
    this.typeId = typeId;
}
public String getContext() {
        return context;
    }
public void setContext(String context) {
        this.context = context;
    }
public String getActTime() {
        return actTime;
    }
public void setActTime(String actTime) {
        this.actTime = actTime;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getActName() {
        return actName;
    }
public void setActName(String actName) {
        this.actName = actName;
    }
public String getPersonCount() {
        return personCount;
    }
public void setPersonCount(String personCount) {
        this.personCount = personCount;
    }



}
