package com.java110.po.govActivityPerson;

import java.io.Serializable;
import java.util.Date;

public class GovActivityPersonPo implements Serializable {

    private String personName;
private String personAge;
private String actPerId;
private String personAddress;
private String caId;
private String actId;
private String remark;
private String statusCd = "0";
private String personLink;
public String getPersonName() {
        return personName;
    }
public void setPersonName(String personName) {
        this.personName = personName;
    }
public String getPersonAge() {
        return personAge;
    }
public void setPersonAge(String personAge) {
        this.personAge = personAge;
    }
public String getActPerId() {
        return actPerId;
    }
public void setActPerId(String actPerId) {
        this.actPerId = actPerId;
    }
public String getPersonAddress() {
        return personAddress;
    }
public void setPersonAddress(String personAddress) {
        this.personAddress = personAddress;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getActId() {
        return actId;
    }
public void setActId(String actId) {
        this.actId = actId;
    }
public String getRemark() {
        return remark;
    }
public void setRemark(String remark) {
        this.remark = remark;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getPersonLink() {
        return personLink;
    }
public void setPersonLink(String personLink) {
        this.personLink = personLink;
    }



}
