package com.java110.po.govReportSetting;

import java.io.Serializable;
import java.util.Date;

public class GovReportSettingPo implements Serializable {

    private String reportType;
private String returnVisitFlag;
private String caId;
private String remark;
private String statusCd = "0";
private String reportWay;
private String reportTypeName;
private String settingId;
public String getReportType() {
        return reportType;
    }
public void setReportType(String reportType) {
        this.reportType = reportType;
    }
public String getReturnVisitFlag() {
        return returnVisitFlag;
    }
public void setReturnVisitFlag(String returnVisitFlag) {
        this.returnVisitFlag = returnVisitFlag;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getRemark() {
        return remark;
    }
public void setRemark(String remark) {
        this.remark = remark;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getReportWay() {
        return reportWay;
    }
public void setReportWay(String reportWay) {
        this.reportWay = reportWay;
    }
public String getReportTypeName() {
        return reportTypeName;
    }
public void setReportTypeName(String reportTypeName) {
        this.reportTypeName = reportTypeName;
    }
public String getSettingId() {
        return settingId;
    }
public void setSettingId(String settingId) {
        this.settingId = settingId;
    }



}
