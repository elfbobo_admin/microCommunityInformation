package com.java110.po.govMeetingList;

import java.io.Serializable;
import java.util.Date;

public class GovMeetingListPo implements Serializable {

    private String meetingBrief;
private String govMeetingId;
private String govMemberId;
private String statusCd = "0";
private String orgId;
private String meetingTime;
private String meetingAddress;
private String meetingImg;
private String meetingName;
    private String memberName;
private String caId;
private String meetingPoints;
private String meetingContent;
private String typeId;
public String getMeetingBrief() {
        return meetingBrief;
    }
public void setMeetingBrief(String meetingBrief) {
        this.meetingBrief = meetingBrief;
    }
public String getGovMeetingId() {
        return govMeetingId;
    }
public void setGovMeetingId(String govMeetingId) {
        this.govMeetingId = govMeetingId;
    }
public String getGovMemberId() {
        return govMemberId;
    }
public void setGovMemberId(String govMemberId) {
        this.govMemberId = govMemberId;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getOrgId() {
        return orgId;
    }
public void setOrgId(String orgId) {
        this.orgId = orgId;
    }
public String getMeetingTime() {
        return meetingTime;
    }
public void setMeetingTime(String meetingTime) {
        this.meetingTime = meetingTime;
    }
public String getMeetingAddress() {
        return meetingAddress;
    }
public void setMeetingAddress(String meetingAddress) {
        this.meetingAddress = meetingAddress;
    }
public String getMeetingImg() {
        return meetingImg;
    }
public void setMeetingImg(String meetingImg) {
        this.meetingImg = meetingImg;
    }
public String getMeetingName() {
        return meetingName;
    }
public void setMeetingName(String meetingName) {
        this.meetingName = meetingName;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getMeetingPoints() {
        return meetingPoints;
    }
public void setMeetingPoints(String meetingPoints) {
        this.meetingPoints = meetingPoints;
    }
public String getMeetingContent() {
        return meetingContent;
    }
public void setMeetingContent(String meetingContent) {
        this.meetingContent = meetingContent;
    }
public String getTypeId() {
        return typeId;
    }
public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    private Date createTime;

     public Date getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Date createTime) {
            this.createTime = createTime;
        }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }
}
