package com.java110.po.govAdvert;

import java.io.Serializable;
import java.util.Date;

public class GovAdvertPo implements Serializable {

    private String adName;
private String statusCd = "0";
private String adTypeCd;
private String advertType;
private String advertId;
private String caId;
private String viewType;
private String startTime;
private String pageUrl;
private String state;
private String endTime;
private String seq;
public String getAdName() {
        return adName;
    }
public void setAdName(String adName) {
        this.adName = adName;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getAdTypeCd() {
        return adTypeCd;
    }
public void setAdTypeCd(String adTypeCd) {
        this.adTypeCd = adTypeCd;
    }
public String getAdvertType() {
        return advertType;
    }
public void setAdvertType(String advertType) {
        this.advertType = advertType;
    }
public String getAdvertId() {
        return advertId;
    }
public void setAdvertId(String advertId) {
        this.advertId = advertId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getViewType() {
        return viewType;
    }
public void setViewType(String viewType) {
        this.viewType = viewType;
    }
public String getStartTime() {
        return startTime;
    }
public void setStartTime(String startTime) {
        this.startTime = startTime;
    }
public String getPageUrl() {
        return pageUrl;
    }
public void setPageUrl(String pageUrl) {
        this.pageUrl = pageUrl;
    }
public String getState() {
        return state;
    }
public void setState(String state) {
        this.state = state;
    }
public String getEndTime() {
        return endTime;
    }
public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
public String getSeq() {
        return seq;
    }
public void setSeq(String seq) {
        this.seq = seq;
    }



}
