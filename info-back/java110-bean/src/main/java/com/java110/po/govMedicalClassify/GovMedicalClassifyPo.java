package com.java110.po.govMedicalClassify;

import java.io.Serializable;
import java.util.Date;

public class GovMedicalClassifyPo implements Serializable {

    private String caId;
private String classifyType;
private String statusCd = "0";
private String classifyName;
private String seq;
private String ramark;
private String medicalClassifyId;
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getClassifyType() {
        return classifyType;
    }
public void setClassifyType(String classifyType) {
        this.classifyType = classifyType;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getClassifyName() {
        return classifyName;
    }
public void setClassifyName(String classifyName) {
        this.classifyName = classifyName;
    }
public String getSeq() {
        return seq;
    }
public void setSeq(String seq) {
        this.seq = seq;
    }
public String getRamark() {
        return ramark;
    }
public void setRamark(String ramark) {
        this.ramark = ramark;
    }
public String getMedicalClassifyId() {
        return medicalClassifyId;
    }
public void setMedicalClassifyId(String medicalClassifyId) {
        this.medicalClassifyId = medicalClassifyId;
    }

    private Date createTime;

     public Date getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Date createTime) {
            this.createTime = createTime;
        }


}
