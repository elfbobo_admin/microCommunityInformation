package com.java110.po.fileRel;

import java.io.Serializable;
import java.util.Date;

public class FileRelPo implements Serializable {

    private String fileName;
private String fileRelId;
private String objId;
private String relType;
private String statusCd = "0";
public String getFileName() {
        return fileName;
    }
public void setFileName(String fileName) {
        this.fileName = fileName;
    }
public String getFileRelId() {
        return fileRelId;
    }
public void setFileRelId(String fileRelId) {
        this.fileRelId = fileRelId;
    }
public String getObjId() {
        return objId;
    }
public void setObjId(String objId) {
        this.objId = objId;
    }
public String getRelType() {
        return relType;
    }
public void setRelType(String relType) {
        this.relType = relType;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }



}
