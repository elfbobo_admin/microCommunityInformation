package com.java110.po.govLabel;

import java.io.Serializable;
import java.util.Date;

public class GovLabelPo implements Serializable {

    private String govLabelId;
private String caId;
private String labelType;
private String statusCd = "0";
private String labelName;
private String labelCd;
private String ramark;
public String getGovLabelId() {
        return govLabelId;
    }
public void setGovLabelId(String govLabelId) {
        this.govLabelId = govLabelId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getLabelType() {
        return labelType;
    }
public void setLabelType(String labelType) {
        this.labelType = labelType;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getLabelName() {
        return labelName;
    }
public void setLabelName(String labelName) {
        this.labelName = labelName;
    }
public String getLabelCd() {
        return labelCd;
    }
public void setLabelCd(String labelCd) {
        this.labelCd = labelCd;
    }
public String getRamark() {
        return ramark;
    }
public void setRamark(String ramark) {
        this.ramark = ramark;
    }

    private Date createTime;

     public Date getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Date createTime) {
            this.createTime = createTime;
        }


}
