package com.java110.po.govAids;

import java.io.Serializable;
import java.util.Date;

public class GovAidsPo implements Serializable {

    private String emergencyTel;
private String address;
private String aidsStartTime;
private String caId;
private String emergencyPerson;
private String name;
private String aidsReason;
private String tel;
private String aidsId;
private String ramark;
public String getEmergencyTel() {
        return emergencyTel;
    }
public void setEmergencyTel(String emergencyTel) {
        this.emergencyTel = emergencyTel;
    }
public String getAddress() {
        return address;
    }
public void setAddress(String address) {
        this.address = address;
    }
public String getAidsStartTime() {
        return aidsStartTime;
    }
public void setAidsStartTime(String aidsStartTime) {
        this.aidsStartTime = aidsStartTime;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getEmergencyPerson() {
        return emergencyPerson;
    }
public void setEmergencyPerson(String emergencyPerson) {
        this.emergencyPerson = emergencyPerson;
    }
public String getName() {
        return name;
    }
public void setName(String name) {
        this.name = name;
    }
public String getAidsReason() {
        return aidsReason;
    }
public void setAidsReason(String aidsReason) {
        this.aidsReason = aidsReason;
    }
public String getTel() {
        return tel;
    }
public void setTel(String tel) {
        this.tel = tel;
    }
public String getAidsId() {
        return aidsId;
    }
public void setAidsId(String aidsId) {
        this.aidsId = aidsId;
    }
    public String getRamark() {
        return ramark;
    }
    public void setRamark(String ramark) {
        this.ramark = ramark;
    }
    private Date createTime;

    private String statusCd = "0";

     public Date getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Date createTime) {
            this.createTime = createTime;
        }

        public String getStatusCd() {
            return statusCd;
        }

        public void setStatusCd(String statusCd) {
            this.statusCd = statusCd;
        }

}
