package com.java110.po.govPetitionType;

import java.io.Serializable;
import java.util.Date;

public class GovPetitionTypePo implements Serializable {

    private String typeDesc;
private String caId;
private String typeName;
private String typeId;
private String statusCd = "0";
private String seq;
public String getTypeDesc() {
        return typeDesc;
    }
public void setTypeDesc(String typeDesc) {
        this.typeDesc = typeDesc;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getTypeName() {
        return typeName;
    }
public void setTypeName(String typeName) {
        this.typeName = typeName;
    }
public String getTypeId() {
        return typeId;
    }
public void setTypeId(String typeId) {
        this.typeId = typeId;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getSeq() {
        return seq;
    }
public void setSeq(String seq) {
        this.seq = seq;
    }

    private Date createTime;

     public Date getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Date createTime) {
            this.createTime = createTime;
        }


}
