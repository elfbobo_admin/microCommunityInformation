package com.java110.po.govDoctorHealthy;

import java.io.Serializable;
import java.util.Date;

public class GovDoctorHealthyPo implements Serializable {

    private String birthday;
private String jobName;
private String idType;
private String idCard;
private String statusCd = "0";
private String personSex;
private String doctorNum;
private String personName;
private String datasourceType;
private String titleName;
private String govPersonId;
private String caId;
private String businessExpertise;
private String govDoctorId;
private String personTel;
private String labelCd;
public String getBirthday() {
        return birthday;
    }
public void setBirthday(String birthday) {
        this.birthday = birthday;
    }
public String getJobName() {
        return jobName;
    }
public void setJobName(String jobName) {
        this.jobName = jobName;
    }
public String getIdType() {
        return idType;
    }
public void setIdType(String idType) {
        this.idType = idType;
    }
public String getIdCard() {
        return idCard;
    }
public void setIdCard(String idCard) {
        this.idCard = idCard;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getPersonSex() {
        return personSex;
    }
public void setPersonSex(String personSex) {
        this.personSex = personSex;
    }
public String getDoctorNum() {
        return doctorNum;
    }
public void setDoctorNum(String doctorNum) {
        this.doctorNum = doctorNum;
    }
public String getPersonName() {
        return personName;
    }
public void setPersonName(String personName) {
        this.personName = personName;
    }
public String getDatasourceType() {
        return datasourceType;
    }
public void setDatasourceType(String datasourceType) {
        this.datasourceType = datasourceType;
    }
public String getTitleName() {
        return titleName;
    }
public void setTitleName(String titleName) {
        this.titleName = titleName;
    }
public String getGovPersonId() {
        return govPersonId;
    }
public void setGovPersonId(String govPersonId) {
        this.govPersonId = govPersonId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getBusinessExpertise() {
        return businessExpertise;
    }
public void setBusinessExpertise(String businessExpertise) {
        this.businessExpertise = businessExpertise;
    }
public String getGovDoctorId() {
        return govDoctorId;
    }
public void setGovDoctorId(String govDoctorId) {
        this.govDoctorId = govDoctorId;
    }
public String getPersonTel() {
        return personTel;
    }
public void setPersonTel(String personTel) {
        this.personTel = personTel;
    }

    private Date createTime;

     public Date getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Date createTime) {
            this.createTime = createTime;
        }

    public String getLabelCd() {
        return labelCd;
    }

    public void setLabelCd(String labelCd) {
        this.labelCd = labelCd;
    }
}
