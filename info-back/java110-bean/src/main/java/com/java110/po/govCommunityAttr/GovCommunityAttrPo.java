package com.java110.po.govCommunityAttr;

import java.io.Serializable;
import java.util.Date;

public class GovCommunityAttrPo implements Serializable {

public static final String COMMUNITY_ATTR_MAPX = "map_x";
public static final String COMMUNITY_ATTR_MAPY = "map_y";

    private String attrId;
private String govCommunityId;
private String caId;
private String specCd;
private String statusCd = "0";
private String value;
public String getAttrId() {
        return attrId;
    }
public void setAttrId(String attrId) {
        this.attrId = attrId;
    }
public String getGovCommunityId() {
        return govCommunityId;
    }
public void setGovCommunityId(String govCommunityId) {
        this.govCommunityId = govCommunityId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getSpecCd() {
        return specCd;
    }
public void setSpecCd(String specCd) {
        this.specCd = specCd;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getValue() {
        return value;
    }
public void setValue(String value) {
        this.value = value;
    }

    private Date createTime;

     public Date getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Date createTime) {
            this.createTime = createTime;
        }


}
