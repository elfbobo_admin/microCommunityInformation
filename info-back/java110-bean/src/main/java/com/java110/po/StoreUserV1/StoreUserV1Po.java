package com.java110.po.StoreUserV1;

import java.io.Serializable;
import java.util.Date;

public class StoreUserV1Po implements Serializable {

    private String statusCd = "0";
private String storeUserId;
private String storeId;
private String userId;
private String relCd;
private String bId;
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getStoreUserId() {
        return storeUserId;
    }
public void setStoreUserId(String storeUserId) {
        this.storeUserId = storeUserId;
    }
public String getStoreId() {
        return storeId;
    }
public void setStoreId(String storeId) {
        this.storeId = storeId;
    }
public String getUserId() {
        return userId;
    }
public void setUserId(String userId) {
        this.userId = userId;
    }
public String getRelCd() {
        return relCd;
    }
public void setRelCd(String relCd) {
        this.relCd = relCd;
    }

    public String getbId() {
        return bId;
    }

    public void setbId(String bId) {
        this.bId = bId;
    }
}
