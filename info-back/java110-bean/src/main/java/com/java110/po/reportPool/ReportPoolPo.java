package com.java110.po.reportPool;

import java.io.Serializable;
import java.util.Date;

public class ReportPoolPo implements Serializable {

    private String reportType;
private String createUserId;
private String reportId;
private String reportName;
private String appointmentTime;
private String caId;
private String context;
private String tel;
private String repairChannel;
private String statusCd = "0";
private String state;
public String getReportType() {
        return reportType;
    }
public void setReportType(String reportType) {
        this.reportType = reportType;
    }
public String getCreateUserId() {
        return createUserId;
    }
public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }
public String getReportId() {
        return reportId;
    }
public void setReportId(String reportId) {
        this.reportId = reportId;
    }
public String getReportName() {
        return reportName;
    }
public void setReportName(String reportName) {
        this.reportName = reportName;
    }
public String getAppointmentTime() {
        return appointmentTime;
    }
public void setAppointmentTime(String appointmentTime) {
        this.appointmentTime = appointmentTime;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getContext() {
        return context;
    }
public void setContext(String context) {
        this.context = context;
    }
public String getTel() {
        return tel;
    }
public void setTel(String tel) {
        this.tel = tel;
    }
public String getRepairChannel() {
        return repairChannel;
    }
public void setRepairChannel(String repairChannel) {
        this.repairChannel = repairChannel;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getState() {
        return state;
    }
public void setState(String state) {
        this.state = state;
    }



}
