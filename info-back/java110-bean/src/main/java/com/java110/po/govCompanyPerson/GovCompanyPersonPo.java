package com.java110.po.govCompanyPerson;

import java.io.Serializable;
import java.util.Date;

public class GovCompanyPersonPo implements Serializable {

    private String relId;
private String govCompanyId;
private String govPersonId;
private String caId;
private String govOrgName;
private String statusCd = "0";
private String state;
private String relCd;
public String getRelId() {
        return relId;
    }
public void setRelId(String relId) {
        this.relId = relId;
    }
public String getGovCompanyId() {
        return govCompanyId;
    }
public void setGovCompanyId(String govCompanyId) {
        this.govCompanyId = govCompanyId;
    }
public String getGovPersonId() {
        return govPersonId;
    }
public void setGovPersonId(String govPersonId) {
        this.govPersonId = govPersonId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getGovOrgName() {
        return govOrgName;
    }
public void setGovOrgName(String govOrgName) {
        this.govOrgName = govOrgName;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getState() {
        return state;
    }
public void setState(String state) {
        this.state = state;
    }
public String getRelCd() {
        return relCd;
    }
public void setRelCd(String relCd) {
        this.relCd = relCd;
    }



}
