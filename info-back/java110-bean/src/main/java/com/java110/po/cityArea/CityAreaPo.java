package com.java110.po.cityArea;

import java.io.Serializable;
import java.util.Date;

public class CityAreaPo implements Serializable {

    private String areaCode;
private String areaName;
private String areaLevel;
private String lon;
private String statusCd = "0";
private String id;
private String parentAreaCode;
private String parentAreaName;
private String lat;
public String getAreaCode() {
        return areaCode;
    }
public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }
public String getAreaName() {
        return areaName;
    }
public void setAreaName(String areaName) {
        this.areaName = areaName;
    }
public String getAreaLevel() {
        return areaLevel;
    }
public void setAreaLevel(String areaLevel) {
        this.areaLevel = areaLevel;
    }
public String getLon() {
        return lon;
    }
public void setLon(String lon) {
        this.lon = lon;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getId() {
        return id;
    }
public void setId(String id) {
        this.id = id;
    }
public String getParentAreaCode() {
        return parentAreaCode;
    }
public void setParentAreaCode(String parentAreaCode) {
        this.parentAreaCode = parentAreaCode;
    }
public String getParentAreaName() {
        return parentAreaName;
    }
public void setParentAreaName(String parentAreaName) {
        this.parentAreaName = parentAreaName;
    }
public String getLat() {
        return lat;
    }
public void setLat(String lat) {
        this.lat = lat;
    }

    private Date createTime;

     public Date getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Date createTime) {
            this.createTime = createTime;
        }


}
