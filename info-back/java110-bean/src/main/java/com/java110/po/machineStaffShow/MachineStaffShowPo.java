package com.java110.po.machineStaffShow;

import java.io.Serializable;
import java.util.Date;

public class MachineStaffShowPo implements Serializable {

    private String machineId;
private String caId;
private String staffId;
private String key;
private String machineStaffId;
private String isShow;
    private String statusCd = "0";
public String getMachineId() {
        return machineId;
    }
public void setMachineId(String machineId) {
        this.machineId = machineId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getStaffId() {
        return staffId;
    }
public void setStaffId(String staffId) {
        this.staffId = staffId;
    }
public String getKey() {
        return key;
    }
public void setKey(String key) {
        this.key = key;
    }
public String getMachineStaffId() {
        return machineStaffId;
    }
public void setMachineStaffId(String machineStaffId) {
        this.machineStaffId = machineStaffId;
    }
public String getIsShow() {
        return isShow;
    }
public void setIsShow(String isShow) {
        this.isShow = isShow;
    }

    private Date createTime;

     public Date getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Date createTime) {
            this.createTime = createTime;
        }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
}
