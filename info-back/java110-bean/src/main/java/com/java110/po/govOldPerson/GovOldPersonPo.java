package com.java110.po.govOldPerson;

import java.io.Serializable;
import java.util.Date;

public class GovOldPersonPo implements Serializable {

    private String servPerson;
private String govCommunityId;
private String servTel;
private String contactPerson;
private String statusCd = "0";
private String oldId;
private String contactTel;
private String ramark;
private String personName;
private String personAge;
private String datasourceType;
private String govPersonId;
private String caId;
private String personTel;
private String timeAmount;
private String typeId;
public String getServPerson() {
        return servPerson;
    }
public void setServPerson(String servPerson) {
        this.servPerson = servPerson;
    }
public String getGovCommunityId() {
        return govCommunityId;
    }
public void setGovCommunityId(String govCommunityId) {
        this.govCommunityId = govCommunityId;
    }
public String getServTel() {
        return servTel;
    }
public void setServTel(String servTel) {
        this.servTel = servTel;
    }
public String getContactPerson() {
        return contactPerson;
    }
public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getOldId() {
        return oldId;
    }
public void setOldId(String oldId) {
        this.oldId = oldId;
    }
public String getContactTel() {
        return contactTel;
    }
public void setContactTel(String contactTel) {
        this.contactTel = contactTel;
    }
public String getRamark() {
        return ramark;
    }
public void setRamark(String ramark) {
        this.ramark = ramark;
    }
public String getPersonName() {
        return personName;
    }
public void setPersonName(String personName) {
        this.personName = personName;
    }
public String getPersonAge() {
        return personAge;
    }
public void setPersonAge(String personAge) {
        this.personAge = personAge;
    }
public String getDatasourceType() {
        return datasourceType;
    }
public void setDatasourceType(String datasourceType) {
        this.datasourceType = datasourceType;
    }
public String getGovPersonId() {
        return govPersonId;
    }
public void setGovPersonId(String govPersonId) {
        this.govPersonId = govPersonId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getPersonTel() {
        return personTel;
    }
public void setPersonTel(String personTel) {
        this.personTel = personTel;
    }

    public String getTimeAmount() {
        return timeAmount;
    }

    public void setTimeAmount(String timeAmount) {
        this.timeAmount = timeAmount;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }
}
