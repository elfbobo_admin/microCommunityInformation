package com.java110.dto.govGrid;

import com.java110.dto.PageDto;
import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName FloorDto
 * @Description 网格人员数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class GovGridDto extends PageDto implements Serializable {

    private String personName;
private String datasourceType;
private String govGridId;
private String govPersonId;
private String caId;
private String govTypeId;
private String personTel;
private String ramark;
private String idCard;
private String nation;
private String maritalStatus;
private String personSex;
private String typeName;


    private Date createTime;

    private String statusCd = "0";


    public String getPersonName() {
        return personName;
    }
public void setPersonName(String personName) {
        this.personName = personName;
    }
public String getDatasourceType() {
        return datasourceType;
    }
public void setDatasourceType(String datasourceType) {
        this.datasourceType = datasourceType;
    }
public String getGovGridId() {
        return govGridId;
    }
public void setGovGridId(String govGridId) {
        this.govGridId = govGridId;
    }
public String getGovPersonId() {
        return govPersonId;
    }
public void setGovPersonId(String govPersonId) {
        this.govPersonId = govPersonId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getGovTypeId() {
        return govTypeId;
    }
public void setGovTypeId(String govTypeId) {
        this.govTypeId = govTypeId;
    }
public String getPersonTel() {
        return personTel;
    }
public void setPersonTel(String personTel) {
        this.personTel = personTel;
    }
public String getRamark() {
        return ramark;
    }
public void setRamark(String ramark) {
        this.ramark = ramark;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getPersonSex() {
        return personSex;
    }

    public void setPersonSex(String personSex) {
        this.personSex = personSex;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }
}
