package com.java110.dto.govHealthTitle;

import com.java110.dto.PageDto;
import com.java110.dto.govHealthTitleValue.GovHealthTitleValueDto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @ClassName FloorDto
 * @Description 体检项数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class GovHealthTitleDto extends PageDto implements Serializable {
    public static final String TITLE_TYPE_SINGLE = "1001"; // 单选题
    public static final String TITLE_TYPE_MULTIPLE = "2002"; // 多选题
    public static final String TITLE_TYPE_QUESTIONS = "3003"; // 简答题
    private String titleType;
    private String titleId;
    private String termId;
    private String termName;
    private String caId;
    private String healthId;
    private String title;
    private String seq;
    private List<GovHealthTitleValueDto> govHealthTitleValueDtos;

    private Date createTime;

    private String statusCd = "0";


    public String getTitleType() {
        return titleType;
    }

    public void setTitleType(String titleType) {
        this.titleType = titleType;
    }

    public String getTitleId() {
        return titleId;
    }

    public void setTitleId(String titleId) {
        this.titleId = titleId;
    }

    public String getCaId() {
        return caId;
    }

    public void setCaId(String caId) {
        this.caId = caId;
    }

    public String getHealthId() {
        return healthId;
    }

    public void setHealthId(String healthId) {
        this.healthId = healthId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSeq() {
        return seq;
    }

    public void setSeq(String seq) {
        this.seq = seq;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }

    public List<GovHealthTitleValueDto> getGovHealthTitleValueDtos() {
        return govHealthTitleValueDtos;
    }

    public void setGovHealthTitleValueDtos(List<GovHealthTitleValueDto> govHealthTitleValueDtos) {
        this.govHealthTitleValueDtos = govHealthTitleValueDtos;
    }

    public String getTermId() {
        return termId;
    }

    public void setTermId(String termId) {
        this.termId = termId;
    }

    public String getTermName() {
        return termName;
    }

    public void setTermName(String termName) {
        this.termName = termName;
    }
}
