package com.java110.dto.govActivityLikes;

import com.java110.dto.PageDto;
import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName FloorDto
 * @Description 活动点赞数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class GovActivityLikesDto extends PageDto implements Serializable {

    private String flag;
private String likeId;
private String actId;
private String userName;
private String userId;


    private Date createTime;

    private String statusCd = "0";


    public String getFlag() {
        return flag;
    }
public void setFlag(String flag) {
        this.flag = flag;
    }
public String getLikeId() {
        return likeId;
    }
public void setLikeId(String likeId) {
        this.likeId = likeId;
    }
public String getActId() {
        return actId;
    }
public void setActId(String actId) {
        this.actId = actId;
    }
public String getUserName() {
        return userName;
    }
public void setUserName(String userName) {
        this.userName = userName;
    }
public String getUserId() {
        return userId;
    }
public void setUserId(String userId) {
        this.userId = userId;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
}
