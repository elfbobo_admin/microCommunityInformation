package com.java110.dto.govSchoolPeripheralPerson;

import com.java110.dto.PageDto;
import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName FloorDto
 * @Description 校园周边重点人员数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class GovSchoolPeripheralPersonDto extends PageDto implements Serializable {

    private String isAttention;
private String govPersonId;
private String extentInjury;
private String schoolId;
private String caId;
private String remark;
private String schPersonId;
private String schoolName;
private String idCard;
private String personName;
private String personSex;
private String personTel;


    private Date createTime;

    private String statusCd = "0";


    public String getIsAttention() {
        return isAttention;
    }
public void setIsAttention(String isAttention) {
        this.isAttention = isAttention;
    }
public String getGovPersonId() {
        return govPersonId;
    }
public void setGovPersonId(String govPersonId) {
        this.govPersonId = govPersonId;
    }
public String getExtentInjury() {
        return extentInjury;
    }
public void setExtentInjury(String extentInjury) {
        this.extentInjury = extentInjury;
    }
public String getSchoolId() {
        return schoolId;
    }
public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getRemark() {
        return remark;
    }
public void setRemark(String remark) {
        this.remark = remark;
    }
public String getSchPersonId() {
        return schPersonId;
    }
public void setSchPersonId(String schPersonId) {
        this.schPersonId = schPersonId;
    }
public String getSchoolName() {
        return schoolName;
    }
public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getPersonSex() {
        return personSex;
    }

    public void setPersonSex(String personSex) {
        this.personSex = personSex;
    }

    public String getPersonTel() {
        return personTel;
    }

    public void setPersonTel(String personTel) {
        this.personTel = personTel;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
}
