package com.java110.dto.perGovActivities;

import com.java110.dto.PageDto;
import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName FloorDto
 * @Description 生日记录数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class PerGovActivitiesDto extends PageDto implements Serializable {

    private String activityTime;
private String title;
private String userName;
private String userId;
private String typeCd;
private String caId;
private String context;
private String perActivitiesId;
private String startTime;
private String endTime;
private String state;
private String headerImg;


    private Date createTime;

    private String statusCd = "0";


    public String getActivityTime() {
        return activityTime;
    }
public void setActivityTime(String activityTime) {
        this.activityTime = activityTime;
    }
public String getTitle() {
        return title;
    }
public void setTitle(String title) {
        this.title = title;
    }
public String getUserName() {
        return userName;
    }
public void setUserName(String userName) {
        this.userName = userName;
    }
public String getUserId() {
        return userId;
    }
public void setUserId(String userId) {
        this.userId = userId;
    }
public String getTypeCd() {
        return typeCd;
    }
public void setTypeCd(String typeCd) {
        this.typeCd = typeCd;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getContext() {
        return context;
    }
public void setContext(String context) {
        this.context = context;
    }
public String getPerActivitiesId() {
        return perActivitiesId;
    }
public void setPerActivitiesId(String perActivitiesId) {
        this.perActivitiesId = perActivitiesId;
    }
public String getStartTime() {
        return startTime;
    }
public void setStartTime(String startTime) {
        this.startTime = startTime;
    }
public String getEndTime() {
        return endTime;
    }
public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
public String getState() {
        return state;
    }
public void setState(String state) {
        this.state = state;
    }
public String getHeaderImg() {
        return headerImg;
    }
public void setHeaderImg(String headerImg) {
        this.headerImg = headerImg;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
}
