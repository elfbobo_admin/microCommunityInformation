package com.java110.dto.govPersonDie;

import com.java110.dto.PageDto;

import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName FloorDto
 * @Description 死亡登记数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class GovPersonDieDto extends PageDto implements Serializable {

    private String govCommunityId;
    private String dieId;
    private String dieType;
    private String contactPerson;
    private String contactTel;
    private String ramark;
    private String diePlace;
    private String funeralPlace;
    private String govPersonId;
    private String caId;
    private String startTime;
    private String imgProve;
    private String endTime;
    private String personType;
    private String dieTime;
    private String personName;
    private String personTel;
    private String personAge;
    private String birthday;
    private String communityName;


    private Date createTime;

    private String statusCd = "0";


    public String getGovCommunityId() {
        return govCommunityId;
    }

    public void setGovCommunityId(String govCommunityId) {
        this.govCommunityId = govCommunityId;
    }

    public String getDieId() {
        return dieId;
    }

    public void setDieId(String dieId) {
        this.dieId = dieId;
    }

    public String getDieType() {
        return dieType;
    }

    public void setDieType(String dieType) {
        this.dieType = dieType;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getContactTel() {
        return contactTel;
    }

    public void setContactTel(String contactTel) {
        this.contactTel = contactTel;
    }

    public String getRamark() {
        return ramark;
    }

    public void setRamark(String ramark) {
        this.ramark = ramark;
    }

    public String getDiePlace() {
        return diePlace;
    }

    public void setDiePlace(String diePlace) {
        this.diePlace = diePlace;
    }

    public String getFuneralPlace() {
        return funeralPlace;
    }

    public void setFuneralPlace(String funeralPlace) {
        this.funeralPlace = funeralPlace;
    }

    public String getGovPersonId() {
        return govPersonId;
    }

    public void setGovPersonId(String govPersonId) {
        this.govPersonId = govPersonId;
    }

    public String getCaId() {
        return caId;
    }

    public void setCaId(String caId) {
        this.caId = caId;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getImgProve() {
        return imgProve;
    }

    public void setImgProve(String imgProve) {
        this.imgProve = imgProve;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getPersonType() {
        return personType;
    }

    public void setPersonType(String personType) {
        this.personType = personType;
    }

    public String getDieTime() {
        return dieTime;
    }

    public void setDieTime(String dieTime) {
        this.dieTime = dieTime;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getPersonTel() {
        return personTel;
    }

    public void setPersonTel(String personTel) {
        this.personTel = personTel;
    }

    public String getPersonAge() {
        return personAge;
    }

    public void setPersonAge(String personAge) {
        this.personAge = personAge;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getCommunityName() {
        return communityName;
    }

    public void setCommunityName(String communityName) {
        this.communityName = communityName;
    }
}
