package com.java110.dto.govAids;

import com.java110.dto.PageDto;
import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName FloorDto
 * @Description 艾滋病者数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class GovAidsDto extends PageDto implements Serializable {

    private String emergencyTel;
private String address;
private String aidsStartTime;
private String caId;
private String emergencyPerson;
private String name;
private String aidsReason;
private String tel;
private String aidsId;
private String ramark;


    private Date createTime;

    private String statusCd = "0";


    public String getEmergencyTel() {
        return emergencyTel;
    }
public void setEmergencyTel(String emergencyTel) {
        this.emergencyTel = emergencyTel;
    }
public String getAddress() {
        return address;
    }
public void setAddress(String address) {
        this.address = address;
    }
public String getAidsStartTime() {
        return aidsStartTime;
    }
public void setAidsStartTime(String aidsStartTime) {
        this.aidsStartTime = aidsStartTime;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getEmergencyPerson() {
        return emergencyPerson;
    }
public void setEmergencyPerson(String emergencyPerson) {
        this.emergencyPerson = emergencyPerson;
    }
public String getName() {
        return name;
    }
public void setName(String name) {
        this.name = name;
    }
public String getAidsReason() {
        return aidsReason;
    }
public void setAidsReason(String aidsReason) {
        this.aidsReason = aidsReason;
    }
public String getTel() {
        return tel;
    }
public void setTel(String tel) {
        this.tel = tel;
    }
public String getAidsId() {
        return aidsId;
    }
public void setAidsId(String aidsId) {
        this.aidsId = aidsId;
    }
    public String getRamark() {
        return ramark;
    }
    public void setRamark(String ramark) {
        this.ramark = ramark;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
}
