package com.java110.dto.govMeetingMemberRel;

import com.java110.dto.PageDto;
import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName FloorDto
 * @Description 会议与参会人关系数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class GovMeetingMemberRelDto extends PageDto implements Serializable {

    private String personName;
private String govMeetingId;
private String caId;
private String govMemberId;
private String memberRelId;
private String [] govMeetingIds;
    private String workTypeName;
    private String orgName;


    private Date createTime;

    private String statusCd = "0";


    public String getPersonName() {
        return personName;
    }
public void setPersonName(String personName) {
        this.personName = personName;
    }
public String getGovMeetingId() {
        return govMeetingId;
    }
public void setGovMeetingId(String govMeetingId) {
        this.govMeetingId = govMeetingId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getGovMemberId() {
        return govMemberId;
    }
public void setGovMemberId(String govMemberId) {
        this.govMemberId = govMemberId;
    }
public String getMemberRelId() {
        return memberRelId;
    }
public void setMemberRelId(String memberRelId) {
        this.memberRelId = memberRelId;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }

    public String[] getGovMeetingIds() {
        return govMeetingIds;
    }

    public void setGovMeetingIds(String[] govMeetingIds) {
        this.govMeetingIds = govMeetingIds;
    }

    public String getWorkTypeName() {
        return workTypeName;
    }

    public void setWorkTypeName(String workTypeName) {
        this.workTypeName = workTypeName;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }
}
