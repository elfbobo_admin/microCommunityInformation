package com.java110.dto.govPersonLabelRel;

import com.java110.dto.PageDto;

import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName FloorDto
 * @Description 标签与人口关系数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class GovPersonLabelRelDto extends PageDto implements Serializable {

    private String labelRelId;
    private String govPersonId;
    private String[] govPersonIds;
    private String caId;
    private String labelCd;
    private String labelName;

    private String[] labelCds;
    private Date createTime;

    private String statusCd = "0";


    public String getLabelRelId() {
        return labelRelId;
    }

    public void setLabelRelId(String labelRelId) {
        this.labelRelId = labelRelId;
    }

    public String getGovPersonId() {
        return govPersonId;
    }

    public void setGovPersonId(String govPersonId) {
        this.govPersonId = govPersonId;
    }

    public String getCaId() {
        return caId;
    }

    public void setCaId(String caId) {
        this.caId = caId;
    }

    public String getLabelCd() {
        return labelCd;
    }

    public void setLabelCd(String labelCd) {
        this.labelCd = labelCd;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }

    public String getLabelName() {
        return labelName;
    }

    public void setLabelName(String labelName) {
        this.labelName = labelName;
    }

    public String[] getGovPersonIds() {
        return govPersonIds;
    }

    public void setGovPersonIds(String[] govPersonIds) {
        this.govPersonIds = govPersonIds;
    }

    public String[] getLabelCds() {
        return labelCds;
    }

    public void setLabelCds(String[] labelCds) {
        this.labelCds = labelCds;
    }
}
