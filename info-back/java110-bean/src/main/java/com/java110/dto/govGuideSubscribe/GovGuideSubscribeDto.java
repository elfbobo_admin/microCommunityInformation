package com.java110.dto.govGuideSubscribe;

import com.java110.dto.PageDto;
import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName FloorDto
 * @Description 办事预约数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class GovGuideSubscribeDto extends PageDto implements Serializable {

    private String wgsId;
private String person;
private String caId;
private String link;
private String startTime;
private String state;
private String endTime;
private String wgId;
private String guideName;


    private Date createTime;

    private String statusCd = "0";


    public String getWgsId() {
        return wgsId;
    }
public void setWgsId(String wgsId) {
        this.wgsId = wgsId;
    }
public String getPerson() {
        return person;
    }
public void setPerson(String person) {
        this.person = person;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getLink() {
        return link;
    }
public void setLink(String link) {
        this.link = link;
    }
public String getStartTime() {
        return startTime;
    }
public void setStartTime(String startTime) {
        this.startTime = startTime;
    }
public String getState() {
        return state;
    }
public void setState(String state) {
        this.state = state;
    }
public String getEndTime() {
        return endTime;
    }
public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
public String getWgId() {
        return wgId;
    }
public void setWgId(String wgId) {
        this.wgId = wgId;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }

    public String getGuideName() {
        return guideName;
    }

    public void setGuideName(String guideName) {
        this.guideName = guideName;
    }
}
