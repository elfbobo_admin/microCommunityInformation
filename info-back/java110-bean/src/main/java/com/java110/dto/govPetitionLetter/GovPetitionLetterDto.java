package com.java110.dto.govPetitionLetter;

import com.java110.dto.PageDto;
import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName FloorDto
 * @Description 信访管理数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class GovPetitionLetterDto extends PageDto implements Serializable {

    private String petitionPersonId;
private String dealUserName;
private String idCard;
private String receiveDepartmentName;
private String petitionTime;
private String remark;
private String queryCode;
private String receiveUserId;
private String dealOpinion;
private String context;
private String state;
private String personLink;
private String petitionLetterId;
private String dealUserId;
private String letterCode;
private String govCommunityId;
private String petitionPersonName;
private String dealDepartmentId;
private String receiveUserName;
private String imgUrl;
private String dealTime;
private String dealDepartmentName;
private String caId;
private String petitionType;
private String petitionForm;
private String receiveDepartmentId;
private String petitionPurpose;


    private Date createTime;

    private String statusCd = "0";


    public String getPetitionPersonId() {
        return petitionPersonId;
    }
public void setPetitionPersonId(String petitionPersonId) {
        this.petitionPersonId = petitionPersonId;
    }
public String getDealUserName() {
        return dealUserName;
    }
public void setDealUserName(String dealUserName) {
        this.dealUserName = dealUserName;
    }
public String getIdCard() {
        return idCard;
    }
public void setIdCard(String idCard) {
        this.idCard = idCard;
    }
public String getReceiveDepartmentName() {
        return receiveDepartmentName;
    }
public void setReceiveDepartmentName(String receiveDepartmentName) {
        this.receiveDepartmentName = receiveDepartmentName;
    }
public String getPetitionTime() {
        return petitionTime;
    }
public void setPetitionTime(String petitionTime) {
        this.petitionTime = petitionTime;
    }
public String getRemark() {
        return remark;
    }
public void setRemark(String remark) {
        this.remark = remark;
    }
public String getQueryCode() {
        return queryCode;
    }
public void setQueryCode(String queryCode) {
        this.queryCode = queryCode;
    }
public String getReceiveUserId() {
        return receiveUserId;
    }
public void setReceiveUserId(String receiveUserId) {
        this.receiveUserId = receiveUserId;
    }
public String getDealOpinion() {
        return dealOpinion;
    }
public void setDealOpinion(String dealOpinion) {
        this.dealOpinion = dealOpinion;
    }
public String getContext() {
        return context;
    }
public void setContext(String context) {
        this.context = context;
    }
public String getState() {
        return state;
    }
public void setState(String state) {
        this.state = state;
    }
public String getPersonLink() {
        return personLink;
    }
public void setPersonLink(String personLink) {
        this.personLink = personLink;
    }
public String getPetitionLetterId() {
        return petitionLetterId;
    }
public void setPetitionLetterId(String petitionLetterId) {
        this.petitionLetterId = petitionLetterId;
    }
public String getDealUserId() {
        return dealUserId;
    }
public void setDealUserId(String dealUserId) {
        this.dealUserId = dealUserId;
    }
public String getLetterCode() {
        return letterCode;
    }
public void setLetterCode(String letterCode) {
        this.letterCode = letterCode;
    }
public String getGovCommunityId() {
        return govCommunityId;
    }
public void setGovCommunityId(String govCommunityId) {
        this.govCommunityId = govCommunityId;
    }
public String getPetitionPersonName() {
        return petitionPersonName;
    }
public void setPetitionPersonName(String petitionPersonName) {
        this.petitionPersonName = petitionPersonName;
    }
public String getDealDepartmentId() {
        return dealDepartmentId;
    }
public void setDealDepartmentId(String dealDepartmentId) {
        this.dealDepartmentId = dealDepartmentId;
    }
public String getReceiveUserName() {
        return receiveUserName;
    }
public void setReceiveUserName(String receiveUserName) {
        this.receiveUserName = receiveUserName;
    }
public String getImgUrl() {
        return imgUrl;
    }
public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }
public String getDealTime() {
        return dealTime;
    }
public void setDealTime(String dealTime) {
        this.dealTime = dealTime;
    }
public String getDealDepartmentName() {
        return dealDepartmentName;
    }
public void setDealDepartmentName(String dealDepartmentName) {
        this.dealDepartmentName = dealDepartmentName;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getPetitionType() {
        return petitionType;
    }
public void setPetitionType(String petitionType) {
        this.petitionType = petitionType;
    }
public String getReceiveDepartmentId() {
        return receiveDepartmentId;
    }
public void setReceiveDepartmentId(String receiveDepartmentId) {
        this.receiveDepartmentId = receiveDepartmentId;
    }
public String getPetitionPurpose() {
        return petitionPurpose;
    }
public void setPetitionPurpose(String petitionPurpose) {
        this.petitionPurpose = petitionPurpose;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }

    public String getPetitionForm() {
        return petitionForm;
    }

    public void setPetitionForm(String petitionForm) {
        this.petitionForm = petitionForm;
    }
}
