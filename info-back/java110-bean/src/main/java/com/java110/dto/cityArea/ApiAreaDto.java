package com.java110.dto.cityArea;

import com.java110.vo.MorePageVo;

import java.io.Serializable;
import java.util.List;

public class ApiAreaDto extends MorePageVo implements Serializable {
    List<CityAreaDto> areas;

    public List<CityAreaDto> getAreas() {
        return areas;
    }

    public void setAreas(List<CityAreaDto> areas) {
        this.areas = areas;
    }
}
