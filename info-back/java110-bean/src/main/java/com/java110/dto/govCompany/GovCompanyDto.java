package com.java110.dto.govCompany;

import com.java110.dto.PageDto;
import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName FloorDto
 * @Description 公司组织数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class GovCompanyDto extends PageDto implements Serializable {

    private String companyType;
private String registerTime;
private String idCard;
private String companyName;
private String personIdCard;
private String ramark;
private String artificialPerson;
private String personName;
private String datasourceType;
private String govCompanyId;
private String caId;
private String companyAddress;
private String personTel;
private String caName;
private String govCommunityId;
private String communityName;


    private Date createTime;

    private String statusCd = "0";


    public String getCompanyType() {
        return companyType;
    }
public void setCompanyType(String companyType) {
        this.companyType = companyType;
    }
public String getRegisterTime() {
        return registerTime;
    }
public void setRegisterTime(String registerTime) {
        this.registerTime = registerTime;
    }
public String getIdCard() {
        return idCard;
    }
public void setIdCard(String idCard) {
        this.idCard = idCard;
    }
public String getCompanyName() {
        return companyName;
    }
public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
public String getPersonIdCard() {
        return personIdCard;
    }
public void setPersonIdCard(String personIdCard) {
        this.personIdCard = personIdCard;
    }
public String getRamark() {
        return ramark;
    }
public void setRamark(String ramark) {
        this.ramark = ramark;
    }
public String getArtificialPerson() {
        return artificialPerson;
    }
public void setArtificialPerson(String artificialPerson) {
        this.artificialPerson = artificialPerson;
    }
public String getPersonName() {
        return personName;
    }
public void setPersonName(String personName) {
        this.personName = personName;
    }
public String getDatasourceType() {
        return datasourceType;
    }
public void setDatasourceType(String datasourceType) {
        this.datasourceType = datasourceType;
    }
public String getGovCompanyId() {
        return govCompanyId;
    }
public void setGovCompanyId(String govCompanyId) {
        this.govCompanyId = govCompanyId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getCompanyAddress() {
        return companyAddress;
    }
public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }
public String getPersonTel() {
        return personTel;
    }
public void setPersonTel(String personTel) {
        this.personTel = personTel;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }

    public String getCaName() {
        return caName;
    }

    public void setCaName(String caName) {
        this.caName = caName;
    }

    public String getGovCommunityId() {
        return govCommunityId;
    }

    public void setGovCommunityId(String govCommunityId) {
        this.govCommunityId = govCommunityId;
    }

    public String getCommunityName() {
        return communityName;
    }

    public void setCommunityName(String communityName) {
        this.communityName = communityName;
    }
}
