package com.java110.dto.govMajorEvents;

import com.java110.dto.PageDto;
import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName FloorDto
 * @Description 重特大事件数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class GovMajorEventsDto extends PageDto implements Serializable {

    private String eventsName;
private String areaCode;
private String govEventsId;
private String caId;
private String typeId;
private String levelCd;
private String eventsAddress;
private String ramark;
private String happenTime;
private String typeName;
private String levelName;


    private Date createTime;

    private String statusCd = "0";


    public String getEventsName() {
        return eventsName;
    }
public void setEventsName(String eventsName) {
        this.eventsName = eventsName;
    }
public String getAreaCode() {
        return areaCode;
    }
public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }
public String getGovEventsId() {
        return govEventsId;
    }
public void setGovEventsId(String govEventsId) {
        this.govEventsId = govEventsId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getTypeId() {
        return typeId;
    }
public void setTypeId(String typeId) {
        this.typeId = typeId;
    }
public String getLevelCd() {
        return levelCd;
    }
public void setLevelCd(String levelCd) {
        this.levelCd = levelCd;
    }
public String getEventsAddress() {
        return eventsAddress;
    }
public void setEventsAddress(String eventsAddress) {
        this.eventsAddress = eventsAddress;
    }
public String getRamark() {
        return ramark;
    }
public void setRamark(String ramark) {
        this.ramark = ramark;
    }
public String getHappenTime() {
        return happenTime;
    }
public void setHappenTime(String happenTime) {
        this.happenTime = happenTime;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getLevelName() {
        return levelName;
    }

    public void setLevelName(String levelName) {
        this.levelName = levelName;
    }
}
