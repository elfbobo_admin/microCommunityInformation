package com.java110.dto.govHealthAnswer;

import com.java110.dto.PageDto;
import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName FloorDto
 * @Description 体检单提交者数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class GovHealthAnswerDto extends PageDto implements Serializable {

    private String healthPersonId;
private String caId;
private String healthId;
private String personId;
private String state;
private String healthName;
private String personName;


    private Date createTime;

    private String statusCd = "0";


    public String getHealthPersonId() {
        return healthPersonId;
    }
public void setHealthPersonId(String healthPersonId) {
        this.healthPersonId = healthPersonId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getHealthId() {
        return healthId;
    }
public void setHealthId(String healthId) {
        this.healthId = healthId;
    }
public String getPersonId() {
        return personId;
    }
public void setPersonId(String personId) {
        this.personId = personId;
    }
public String getState() {
        return state;
    }
public void setState(String state) {
        this.state = state;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }

    public String getHealthName() {
        return healthName;
    }

    public void setHealthName(String healthName) {
        this.healthName = healthName;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }
}
