package com.java110.dto.govWorkType;

import com.java110.dto.PageDto;
import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName FloorDto
 * @Description 党内职务数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class GovWorkTypeDto extends PageDto implements Serializable {

    private String workTypeName;
private String state;
private String govTypeId;
private String ramark;


    private Date createTime;

    private String statusCd = "0";


    public String getWorkTypeName() {
        return workTypeName;
    }
public void setWorkTypeName(String workTypeName) {
        this.workTypeName = workTypeName;
    }
public String getState() {
        return state;
    }
public void setState(String state) {
        this.state = state;
    }
public String getGovTypeId() {
        return govTypeId;
    }
public void setGovTypeId(String govTypeId) {
        this.govTypeId = govTypeId;
    }
public String getRamark() {
        return ramark;
    }
public void setRamark(String ramark) {
        this.ramark = ramark;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
}
