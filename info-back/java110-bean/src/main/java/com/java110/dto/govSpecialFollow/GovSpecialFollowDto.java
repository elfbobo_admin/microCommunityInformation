package com.java110.dto.govSpecialFollow;

import com.java110.dto.PageDto;
import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName FloorDto
 * @Description 特殊人员跟进记录数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class GovSpecialFollowDto extends PageDto implements Serializable {

    private String workPersonTel;
private String followId;
private String isWork;
private String caId;
private String specialPersonId;
private String name;
private String specialReason;
private String workAddress;
private String workPerson;
private String ramark;
private String specialStartTime;


    private Date createTime;

    private String statusCd = "0";


    public String getWorkPersonTel() {
        return workPersonTel;
    }
public void setWorkPersonTel(String workPersonTel) {
        this.workPersonTel = workPersonTel;
    }
public String getFollowId() {
        return followId;
    }
public void setFollowId(String followId) {
        this.followId = followId;
    }
public String getIsWork() {
        return isWork;
    }
public void setIsWork(String isWork) {
        this.isWork = isWork;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getSpecialPersonId() {
        return specialPersonId;
    }
public void setSpecialPersonId(String specialPersonId) {
        this.specialPersonId = specialPersonId;
    }
public String getName() {
        return name;
    }
public void setName(String name) {
        this.name = name;
    }
public String getSpecialReason() {
        return specialReason;
    }
public void setSpecialReason(String specialReason) {
        this.specialReason = specialReason;
    }
public String getWorkAddress() {
        return workAddress;
    }
public void setWorkAddress(String workAddress) {
        this.workAddress = workAddress;
    }
public String getWorkPerson() {
        return workPerson;
    }
public void setWorkPerson(String workPerson) {
        this.workPerson = workPerson;
    }
public String getRamark() {
        return ramark;
    }
public void setRamark(String ramark) {
        this.ramark = ramark;
    }
public String getSpecialStartTime() {
        return specialStartTime;
    }
public void setSpecialStartTime(String specialStartTime) {
        this.specialStartTime = specialStartTime;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
}
