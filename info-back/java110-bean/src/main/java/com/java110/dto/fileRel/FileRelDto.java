package com.java110.dto.fileRel;

import com.java110.dto.PageDto;
import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName FloorDto
 * @Description 文件关系管理数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class FileRelDto extends PageDto implements Serializable {

    public static final String REL_TYPE = "20000";
    public static final String REL_MEETING_TYPE = "90400"; //会议图片

    private String fileName;
private String fileRelId;
private String objId;
private String [] objIds;
private String relType;


    private Date createTime;

    private String statusCd = "0";


    public String getFileName() {
        return fileName;
    }
public void setFileName(String fileName) {
        this.fileName = fileName;
    }
public String getFileRelId() {
        return fileRelId;
    }
public void setFileRelId(String fileRelId) {
        this.fileRelId = fileRelId;
    }
public String getObjId() {
        return objId;
    }
public void setObjId(String objId) {
        this.objId = objId;
    }
public String getRelType() {
        return relType;
    }
public void setRelType(String relType) {
        this.relType = relType;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }

    public String[] getObjIds() {
        return objIds;
    }

    public void setObjIds(String[] objIds) {
        this.objIds = objIds;
    }
}
