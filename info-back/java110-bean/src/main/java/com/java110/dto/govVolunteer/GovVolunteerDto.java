package com.java110.dto.govVolunteer;

import com.java110.dto.PageDto;

import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName FloorDto
 * @Description 志愿者管理数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class GovVolunteerDto extends PageDto implements Serializable {
    public static final String ACCT_TYPE = "2007";
    public static final String EXT_USER_SPEC_CD = "97000001";
    public static final String OBJ_TYPE = "6006";
    private String volWork;
    private String address;
    private String student;
    private String volunteerId;
    private String ramark;
    private String caId;
    private String edu;
    private String freeTime;
    private String name;
    private String tel;
    private String company;
    private String goodAtSkills;
    private String volunteerServ;
    private String govPersonId;
    private String idType;
    private String idCard;
    private String politicalOutlook;
    private String nativePlace;
    private String nation;
    private String personSex;
    private String state;
    private String stateName;
    private String reviewComm;
    private String detailType;
    private String orderId;
    private String num;

    private Date createTime;

    private String statusCd = "0";


    public String getVolWork() {
        return volWork;
    }

    public void setVolWork(String volWork) {
        this.volWork = volWork;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getStudent() {
        return student;
    }

    public void setStudent(String student) {
        this.student = student;
    }

    public String getVolunteerId() {
        return volunteerId;
    }

    public void setVolunteerId(String volunteerId) {
        this.volunteerId = volunteerId;
    }

    public String getRamark() {
        return ramark;
    }

    public void setRamark(String ramark) {
        this.ramark = ramark;
    }

    public String getCaId() {
        return caId;
    }

    public void setCaId(String caId) {
        this.caId = caId;
    }

    public String getEdu() {
        return edu;
    }

    public void setEdu(String edu) {
        this.edu = edu;
    }

    public String getFreeTime() {
        return freeTime;
    }

    public void setFreeTime(String freeTime) {
        this.freeTime = freeTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getGoodAtSkills() {
        return goodAtSkills;
    }

    public void setGoodAtSkills(String goodAtSkills) {
        this.goodAtSkills = goodAtSkills;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }

    public String getVolunteerServ() {
        return volunteerServ;
    }

    public void setVolunteerServ(String volunteerServ) {
        this.volunteerServ = volunteerServ;
    }

    public String getGovPersonId() {
        return govPersonId;
    }

    public void setGovPersonId(String govPersonId) {
        this.govPersonId = govPersonId;
    }

    public String getIdType() {
        return idType;
    }

    public void setIdType(String idType) {
        this.idType = idType;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getPoliticalOutlook() {
        return politicalOutlook;
    }

    public void setPoliticalOutlook(String politicalOutlook) {
        this.politicalOutlook = politicalOutlook;
    }

    public String getNativePlace() {
        return nativePlace;
    }

    public void setNativePlace(String nativePlace) {
        this.nativePlace = nativePlace;
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    public String getPersonSex() {
        return personSex;
    }

    public void setPersonSex(String personSex) {
        this.personSex = personSex;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getReviewComm() {
        return reviewComm;
    }

    public void setReviewComm(String reviewComm) {
        this.reviewComm = reviewComm;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getDetailType() {
        return detailType;
    }

    public void setDetailType(String detailType) {
        this.detailType = detailType;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }
}
