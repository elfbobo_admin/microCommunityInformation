package com.java110.dto.govHelpPolicyList;

import com.java110.dto.PageDto;
import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName FloorDto
 * @Description 帮扶记录数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class GovHelpPolicyListDto extends PageDto implements Serializable {

    private String cadrePersonId;
private String cadrePersonName;
private String cadrePersonNameLike;
private String poorPersonId;
private String govHelpId;
private String helpName;
private String listName;
private String listNameLike;
private String caId;
private String startTime;
private String poorPersonName;
private String poorPersonNameLike;
private String endTime;
private String listBrief;
private String govHelpListId;


    private Date createTime;

    private String statusCd = "0";


    public String getCadrePersonId() {
        return cadrePersonId;
    }
public void setCadrePersonId(String cadrePersonId) {
        this.cadrePersonId = cadrePersonId;
    }
public String getCadrePersonName() {
        return cadrePersonName;
    }
public void setCadrePersonName(String cadrePersonName) {
        this.cadrePersonName = cadrePersonName;
    }
public String getPoorPersonId() {
        return poorPersonId;
    }
public void setPoorPersonId(String poorPersonId) {
        this.poorPersonId = poorPersonId;
    }
public String getHelpName() {
        return helpName;
    }
public void setHelpName(String helpName) {
        this.helpName = helpName;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getStartTime() {
        return startTime;
    }
public void setStartTime(String startTime) {
        this.startTime = startTime;
    }
public String getPoorPersonName() {
        return poorPersonName;
    }
public void setPoorPersonName(String poorPersonName) {
        this.poorPersonName = poorPersonName;
    }
public String getEndTime() {
        return endTime;
    }
public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
public String getListBrief() {
        return listBrief;
    }
public void setListBrief(String listBrief) {
        this.listBrief = listBrief;
    }
public String getGovHelpListId() {
        return govHelpListId;
    }
public void setGovHelpListId(String govHelpListId) {
        this.govHelpListId = govHelpListId;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }

    public String getCadrePersonNameLike() {
        return cadrePersonNameLike;
    }

    public void setCadrePersonNameLike(String cadrePersonNameLike) {
        this.cadrePersonNameLike = cadrePersonNameLike;
    }

    public String getPoorPersonNameLike() {
        return poorPersonNameLike;
    }

    public void setPoorPersonNameLike(String poorPersonNameLike) {
        this.poorPersonNameLike = poorPersonNameLike;
    }

    public String getListName() {
        return listName;
    }

    public void setListName(String listName) {
        this.listName = listName;
    }

    public String getListNameLike() {
        return listNameLike;
    }

    public void setListNameLike(String listNameLike) {
        this.listNameLike = listNameLike;
    }

    public String getGovHelpId() {
        return govHelpId;
    }

    public void setGovHelpId(String govHelpId) {
        this.govHelpId = govHelpId;
    }
}
