package com.java110.dto.govPerson;

import com.java110.dto.PageDto;
import com.java110.dto.govLabel.GovLabelDto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * @ClassName FloorDto
 * @Description 人口管理数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class GovPersonDto extends PageDto implements Serializable {

    private String birthday;
    private String idType;
    private String nation;
    private String idCard;
    private String prePersonName;
    private String personSex;
    private String ramark;
    private String personName;
    private String personNameLike;
    private String govPersonId;
    private String caId;
    private String nativePlace;
    private String politicalOutlook;
    private String personType;
    private String personTel;
    private String religiousBelief;
    private String maritalStatus;
    private String caName;
    private String personTypeName;
    private String govCompanyId;
    private String state;
    private String stateName;
    private String govOrgName;
    private String companyName;
    private String relCd;
    private String relName;
    private String isWeb;

    private String labelCd;
    private String labelRelId;

    private String govOwnerId;
    private String labelName;


    private String homicideName;
    private String govHomicideId;

    private List<GovLabelDto> govLabelDtoList;

    private String[] govPersonIds;

    private Date createTime;

    private String statusCd = "0";

    private String personTelLike;
    private String idCardLike;

    private String [] labelCds;

    private String helpName;
    private String govHelpId;
    private String govHelpListId;
    private String listName;

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getIdType() {
        return idType;
    }

    public void setIdType(String idType) {
        this.idType = idType;
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getPrePersonName() {
        return prePersonName;
    }

    public void setPrePersonName(String prePersonName) {
        this.prePersonName = prePersonName;
    }

    public String getPersonSex() {
        return personSex;
    }

    public void setPersonSex(String personSex) {
        this.personSex = personSex;
    }

    public String getRamark() {
        return ramark;
    }

    public void setRamark(String ramark) {
        this.ramark = ramark;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getGovPersonId() {
        return govPersonId;
    }

    public void setGovPersonId(String govPersonId) {
        this.govPersonId = govPersonId;
    }

    public String getCaId() {
        return caId;
    }

    public void setCaId(String caId) {
        this.caId = caId;
    }

    public String getNativePlace() {
        return nativePlace;
    }

    public void setNativePlace(String nativePlace) {
        this.nativePlace = nativePlace;
    }

    public String getPoliticalOutlook() {
        return politicalOutlook;
    }

    public void setPoliticalOutlook(String politicalOutlook) {
        this.politicalOutlook = politicalOutlook;
    }

    public String getPersonType() {
        return personType;
    }

    public void setPersonType(String personType) {
        this.personType = personType;
    }

    public String getPersonTel() {
        return personTel;
    }

    public void setPersonTel(String personTel) {
        this.personTel = personTel;
    }

    public String getReligiousBelief() {
        return religiousBelief;
    }

    public void setReligiousBelief(String religiousBelief) {
        this.religiousBelief = religiousBelief;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }

    public String getCaName() {
        return caName;
    }

    public void setCaName(String caName) {
        this.caName = caName;
    }

    public String getPersonTypeName() {
        return personTypeName;
    }

    public void setPersonTypeName(String personTypeName) {
        this.personTypeName = personTypeName;
    }

    private String datasourceType;

    public String getDatasourceType() {
        return datasourceType;
    }

    public void setDatasourceType(String datasourceType) {
        this.datasourceType = datasourceType;
    }

    public String getGovCompanyId() {
        return govCompanyId;
    }

    public void setGovCompanyId(String govCompanyId) {
        this.govCompanyId = govCompanyId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getGovOrgName() {
        return govOrgName;
    }

    public void setGovOrgName(String govOrgName) {
        this.govOrgName = govOrgName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getRelCd() {
        return relCd;
    }

    public void setRelCd(String relCd) {
        this.relCd = relCd;
    }

    public String getRelName() {
        return relName;
    }

    public void setRelName(String relName) {
        this.relName = relName;
    }

    public String getIsWeb() {
        return isWeb;
    }

    public void setIsWeb(String isWeb) {
        this.isWeb = isWeb;
    }

    public String getPersonNameLike() {
        return personNameLike;
    }

    public void setPersonNameLike(String personNameLike) {
        this.personNameLike = personNameLike;
    }

    public List<GovLabelDto> getGovLabelDtoList() {
        return govLabelDtoList;
    }

    public void setGovLabelDtoList(List<GovLabelDto> govLabelDtoList) {
        this.govLabelDtoList = govLabelDtoList;
    }

    public String getGovOwnerId() {
        return govOwnerId;
    }

    public void setGovOwnerId(String govOwnerId) {
        this.govOwnerId = govOwnerId;
    }

    public String getLabelName() {
        return labelName;
    }

    public void setLabelName(String labelName) {
        this.labelName = labelName;
    }

    public String getPersonTelLike() {
        return personTelLike;
    }

    public void setPersonTelLike(String personTelLike) {
        this.personTelLike = personTelLike;
    }

    public String getIdCardLike() {
        return idCardLike;
    }

    public void setIdCardLike(String idCardLike) {
        this.idCardLike = idCardLike;
    }

    public String getHomicideName() {
        return homicideName;
    }

    public void setHomicideName(String homicideName) {
        this.homicideName = homicideName;
    }

    public String getGovHomicideId() {
        return govHomicideId;
    }

    public void setGovHomicideId(String govHomicideId) {
        this.govHomicideId = govHomicideId;

    }

    public String[] getGovPersonIds() {
        return govPersonIds;
    }

    public void setGovPersonIds(String[] govPersonIds) {
        this.govPersonIds = govPersonIds;
    }

    public String[] getLabelCds() {
        return labelCds;
    }

    public void setLabelCds(String[] labelCds) {
        this.labelCds = labelCds;
    }

    public String getHelpName() {
        return helpName;
    }

    public void setHelpName(String helpName) {
        this.helpName = helpName;
    }

    public String getGovHelpId() {
        return govHelpId;
    }

    public void setGovHelpId(String govHelpId) {
        this.govHelpId = govHelpId;
    }

    public String getLabelCd() {
        return labelCd;
    }

    public void setLabelCd(String labelCd) {
        this.labelCd = labelCd;
    }

    public String getGovHelpListId() {
        return govHelpListId;
    }

    public void setGovHelpListId(String govHelpListId) {
        this.govHelpListId = govHelpListId;
    }

    public String getListName() {
        return listName;
    }

    public void setListName(String listName) {
        this.listName = listName;
    }

    public String getLabelRelId() {
        return labelRelId;
    }

    public void setLabelRelId(String labelRelId) {
        this.labelRelId = labelRelId;
    }
}