package com.java110.dto.UserV1;

import com.java110.dto.PageDto;
import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName FloorDto
 * @Description 用戶管理数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class UserV1Dto extends PageDto implements Serializable {

    private String locationCd;
private String password;
private String address;
private String sex;
private String name;
private String tel;
private String id;
private String userId;
private String email;
private String age;
private String bId;
private String levelCd;


    private Date createTime;

    private String statusCd = "0";


    public String getLocationCd() {
        return locationCd;
    }
public void setLocationCd(String locationCd) {
        this.locationCd = locationCd;
    }
public String getPassword() {
        return password;
    }
public void setPassword(String password) {
        this.password = password;
    }
public String getAddress() {
        return address;
    }
public void setAddress(String address) {
        this.address = address;
    }
public String getSex() {
        return sex;
    }
public void setSex(String sex) {
        this.sex = sex;
    }
public String getName() {
        return name;
    }
public void setName(String name) {
        this.name = name;
    }
public String getTel() {
        return tel;
    }
public void setTel(String tel) {
        this.tel = tel;
    }
public String getId() {
        return id;
    }
public void setId(String id) {
        this.id = id;
    }
public String getUserId() {
        return userId;
    }
public void setUserId(String userId) {
        this.userId = userId;
    }
public String getEmail() {
        return email;
    }
public void setEmail(String email) {
        this.email = email;
    }
public String getAge() {
        return age;
    }
public void setAge(String age) {
        this.age = age;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }

    public String getbId() {
        return bId;
    }

    public void setbId(String bId) {
        this.bId = bId;
    }

    public String getLevelCd() {
        return levelCd;
    }

    public void setLevelCd(String levelCd) {
        this.levelCd = levelCd;
    }
}
