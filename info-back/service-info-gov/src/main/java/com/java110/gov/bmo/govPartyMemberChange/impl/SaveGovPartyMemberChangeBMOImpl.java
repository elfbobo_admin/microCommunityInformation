package com.java110.gov.bmo.govPartyMemberChange.impl;

import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.gov.bmo.govPartyMemberChange.ISaveGovPartyMemberChangeBMO;
import com.java110.intf.gov.IGovPartyMemberChangeInnerServiceSMO;
import com.java110.po.govPartyMemberChange.GovPartyMemberChangePo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


@Service("saveGovPartyMemberChangeBMOImpl")
public class SaveGovPartyMemberChangeBMOImpl implements ISaveGovPartyMemberChangeBMO {

    @Autowired
    private IGovPartyMemberChangeInnerServiceSMO govPartyMemberChangeInnerServiceSMOImpl;

    /**
     * 添加小区信息
     *
     * @param govPartyMemberChangePo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> save(GovPartyMemberChangePo govPartyMemberChangePo) {

        govPartyMemberChangePo.setGovChangeId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_govChangeId));
        int flag = govPartyMemberChangeInnerServiceSMOImpl.saveGovPartyMemberChange(govPartyMemberChangePo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
