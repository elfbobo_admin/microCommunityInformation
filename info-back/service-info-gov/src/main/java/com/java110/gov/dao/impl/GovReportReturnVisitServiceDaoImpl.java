package com.java110.gov.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.gov.dao.IGovReportReturnVisitServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 回访管理服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govReportReturnVisitServiceDaoImpl")
//@Transactional
public class GovReportReturnVisitServiceDaoImpl extends BaseServiceDao implements IGovReportReturnVisitServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovReportReturnVisitServiceDaoImpl.class);





    /**
     * 保存回访管理信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovReportReturnVisitInfo(Map info) throws DAOException {
        logger.debug("保存回访管理信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govReportReturnVisitServiceDaoImpl.saveGovReportReturnVisitInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存回访管理信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询回访管理信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovReportReturnVisitInfo(Map info) throws DAOException {
        logger.debug("查询回访管理信息 入参 info : {}",info);

        List<Map> businessGovReportReturnVisitInfos = sqlSessionTemplate.selectList("govReportReturnVisitServiceDaoImpl.getGovReportReturnVisitInfo",info);

        return businessGovReportReturnVisitInfos;
    }


    /**
     * 修改回访管理信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovReportReturnVisitInfo(Map info) throws DAOException {
        logger.debug("修改回访管理信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govReportReturnVisitServiceDaoImpl.updateGovReportReturnVisitInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改回访管理信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询回访管理数量
     * @param info 回访管理信息
     * @return 回访管理数量
     */
    @Override
    public int queryGovReportReturnVisitsCount(Map info) {
        logger.debug("查询回访管理数据 入参 info : {}",info);

        List<Map> businessGovReportReturnVisitInfos = sqlSessionTemplate.selectList("govReportReturnVisitServiceDaoImpl.queryGovReportReturnVisitsCount", info);
        if (businessGovReportReturnVisitInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovReportReturnVisitInfos.get(0).get("count").toString());
    }


}
