package com.java110.gov.smo.impl;


import com.java110.gov.dao.IGovCaseServiceDao;
import com.java110.intf.gov.IGovCaseInnerServiceSMO;
import com.java110.dto.govCase.GovCaseDto;
import com.java110.po.govCase.GovCasePo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 命案基本信息内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovCaseInnerServiceSMOImpl extends BaseServiceSMO implements IGovCaseInnerServiceSMO {

    @Autowired
    private IGovCaseServiceDao govCaseServiceDaoImpl;


    @Override
    public int saveGovCase(@RequestBody  GovCasePo govCasePo) {
        int saveFlag = 1;
        govCaseServiceDaoImpl.saveGovCaseInfo(BeanConvertUtil.beanCovertMap(govCasePo));
        return saveFlag;
    }

     @Override
    public int updateGovCase(@RequestBody  GovCasePo govCasePo) {
        int saveFlag = 1;
         govCaseServiceDaoImpl.updateGovCaseInfo(BeanConvertUtil.beanCovertMap(govCasePo));
        return saveFlag;
    }

     @Override
    public int deleteGovCase(@RequestBody  GovCasePo govCasePo) {
        int saveFlag = 1;
        govCasePo.setStatusCd("1");
        govCaseServiceDaoImpl.updateGovCaseInfo(BeanConvertUtil.beanCovertMap(govCasePo));
        return saveFlag;
    }

    @Override
    public List<GovCaseDto> queryGovCases(@RequestBody  GovCaseDto govCaseDto) {

        //校验是否传了 分页信息

        int page = govCaseDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govCaseDto.setPage((page - 1) * govCaseDto.getRow());
        }

        List<GovCaseDto> govCases = BeanConvertUtil.covertBeanList(govCaseServiceDaoImpl.getGovCaseInfo(BeanConvertUtil.beanCovertMap(govCaseDto)), GovCaseDto.class);

        return govCases;
    }


    @Override
    public int queryGovCasesCount(@RequestBody GovCaseDto govCaseDto) {
        return govCaseServiceDaoImpl.queryGovCasesCount(BeanConvertUtil.beanCovertMap(govCaseDto));    }

    public IGovCaseServiceDao getGovCaseServiceDaoImpl() {
        return govCaseServiceDaoImpl;
    }

    public void setGovCaseServiceDaoImpl(IGovCaseServiceDao govCaseServiceDaoImpl) {
        this.govCaseServiceDaoImpl = govCaseServiceDaoImpl;
    }
}
