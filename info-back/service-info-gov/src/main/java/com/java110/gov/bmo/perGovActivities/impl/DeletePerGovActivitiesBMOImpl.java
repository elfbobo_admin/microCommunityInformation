package com.java110.gov.bmo.perGovActivities.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.gov.bmo.perGovActivities.IDeletePerGovActivitiesBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.vo.ResultVo;
import com.java110.po.perGovActivities.PerGovActivitiesPo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.gov.IPerGovActivitiesInnerServiceSMO;
import com.java110.dto.perGovActivities.PerGovActivitiesDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("deletePerGovActivitiesBMOImpl")
public class DeletePerGovActivitiesBMOImpl implements IDeletePerGovActivitiesBMO {

    @Autowired
    private IPerGovActivitiesInnerServiceSMO perGovActivitiesInnerServiceSMOImpl;

    /**
     * @param perGovActivitiesPo 数据
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> delete(PerGovActivitiesPo perGovActivitiesPo) {

        int flag = perGovActivitiesInnerServiceSMOImpl.deletePerGovActivities(perGovActivitiesPo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
