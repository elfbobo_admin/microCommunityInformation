package com.java110.gov.bmo.govPartyMember;
import com.java110.dto.govPartyMember.GovPartyMemberDto;
import org.springframework.http.ResponseEntity;
public interface IGetGovPartyMemberBMO {


    /**
     * 查询党员管理
     * add by wuxw
     * @param  govPartyMemberDto
     * @return
     */
    ResponseEntity<String> get(GovPartyMemberDto govPartyMemberDto);


}
