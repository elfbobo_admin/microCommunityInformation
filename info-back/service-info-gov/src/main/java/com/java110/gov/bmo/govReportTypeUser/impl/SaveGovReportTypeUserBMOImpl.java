package com.java110.gov.bmo.govReportTypeUser.impl;


import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.gov.bmo.govReportTypeUser.ISaveGovReportTypeUserBMO;
import com.java110.intf.gov.IGovReportTypeUserInnerServiceSMO;
import com.java110.po.govReportTypeUser.GovReportTypeUserPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


@Service("saveGovReportTypeUserBMOImpl")
public class SaveGovReportTypeUserBMOImpl implements ISaveGovReportTypeUserBMO {

    @Autowired
    private IGovReportTypeUserInnerServiceSMO govReportTypeUserInnerServiceSMOImpl;

    /**
     * 添加小区信息
     *
     * @param govReportTypeUserPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> save(GovReportTypeUserPo govReportTypeUserPo) {

        govReportTypeUserPo.setTypeUserId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_typeUserId));
        int flag = govReportTypeUserInnerServiceSMOImpl.saveGovReportTypeUser(govReportTypeUserPo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
