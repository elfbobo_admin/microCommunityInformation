package com.java110.gov.bmo.govPersonInoutRecord.impl;

import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.gov.bmo.govPersonInoutRecord.ISaveGovPersonInoutRecordBMO;
import com.java110.intf.gov.IGovPersonInoutRecordInnerServiceSMO;
import com.java110.po.govPersonInoutRecord.GovPersonInoutRecordPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("saveGovPersonInoutRecordBMOImpl")
public class SaveGovPersonInoutRecordBMOImpl implements ISaveGovPersonInoutRecordBMO {

    @Autowired
    private IGovPersonInoutRecordInnerServiceSMO govPersonInoutRecordInnerServiceSMOImpl;

    /**
     * 添加小区信息
     *
     * @param govPersonInoutRecordPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> save(GovPersonInoutRecordPo govPersonInoutRecordPo) {

        govPersonInoutRecordPo.setRecordId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_recordId));
        int flag = govPersonInoutRecordInnerServiceSMOImpl.saveGovPersonInoutRecord(govPersonInoutRecordPo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
