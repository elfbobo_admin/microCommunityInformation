package com.java110.gov.bmo.govActivity;
import com.java110.po.govActivity.GovActivityPo;
import org.springframework.http.ResponseEntity;

public interface IDeleteGovActivityBMO {


    /**
     * 修改活动
     * add by wuxw
     * @param govActivityPo
     * @return
     */
    ResponseEntity<String> delete(GovActivityPo govActivityPo);


}
