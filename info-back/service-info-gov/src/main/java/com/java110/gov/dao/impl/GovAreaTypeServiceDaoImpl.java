package com.java110.gov.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.gov.dao.IGovAreaTypeServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 涉及区域类型服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govAreaTypeServiceDaoImpl")
//@Transactional
public class GovAreaTypeServiceDaoImpl extends BaseServiceDao implements IGovAreaTypeServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovAreaTypeServiceDaoImpl.class);





    /**
     * 保存涉及区域类型信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovAreaTypeInfo(Map info) throws DAOException {
        logger.debug("保存涉及区域类型信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govAreaTypeServiceDaoImpl.saveGovAreaTypeInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存涉及区域类型信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询涉及区域类型信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovAreaTypeInfo(Map info) throws DAOException {
        logger.debug("查询涉及区域类型信息 入参 info : {}",info);

        List<Map> businessGovAreaTypeInfos = sqlSessionTemplate.selectList("govAreaTypeServiceDaoImpl.getGovAreaTypeInfo",info);

        return businessGovAreaTypeInfos;
    }


    /**
     * 修改涉及区域类型信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovAreaTypeInfo(Map info) throws DAOException {
        logger.debug("修改涉及区域类型信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govAreaTypeServiceDaoImpl.updateGovAreaTypeInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改涉及区域类型信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询涉及区域类型数量
     * @param info 涉及区域类型信息
     * @return 涉及区域类型数量
     */
    @Override
    public int queryGovAreaTypesCount(Map info) {
        logger.debug("查询涉及区域类型数据 入参 info : {}",info);

        List<Map> businessGovAreaTypeInfos = sqlSessionTemplate.selectList("govAreaTypeServiceDaoImpl.queryGovAreaTypesCount", info);
        if (businessGovAreaTypeInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovAreaTypeInfos.get(0).get("count").toString());
    }


}
