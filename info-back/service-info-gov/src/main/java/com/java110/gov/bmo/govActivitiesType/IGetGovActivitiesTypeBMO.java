package com.java110.gov.bmo.govActivitiesType;
import com.java110.dto.govActivitiesType.GovActivitiesTypeDto;
import org.springframework.http.ResponseEntity;
public interface IGetGovActivitiesTypeBMO {


    /**
     * 查询公告类型
     * add by wuxw
     * @param  govActivitiesTypeDto
     * @return
     */
    ResponseEntity<String> get(GovActivitiesTypeDto govActivitiesTypeDto);


}
