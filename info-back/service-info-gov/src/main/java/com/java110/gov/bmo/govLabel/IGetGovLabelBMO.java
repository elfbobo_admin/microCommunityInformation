package com.java110.gov.bmo.govLabel;
import org.springframework.http.ResponseEntity;
import com.java110.dto.govLabel.GovLabelDto;
public interface IGetGovLabelBMO {


    /**
     * 查询标签管理
     * add by wuxw
     * @param  govLabelDto
     * @return
     */
    ResponseEntity<String> get(GovLabelDto govLabelDto);


}
