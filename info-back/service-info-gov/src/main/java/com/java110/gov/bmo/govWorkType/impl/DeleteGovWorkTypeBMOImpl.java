package com.java110.gov.bmo.govWorkType.impl;


import com.java110.core.annotation.Java110Transactional;
import com.java110.gov.bmo.govWorkType.IDeleteGovWorkTypeBMO;
import com.java110.intf.gov.IGovWorkTypeInnerServiceSMO;
import com.java110.po.govWorkType.GovWorkTypePo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("deleteGovWorkTypeBMOImpl")
public class DeleteGovWorkTypeBMOImpl implements IDeleteGovWorkTypeBMO {

    @Autowired
    private IGovWorkTypeInnerServiceSMO govWorkTypeInnerServiceSMOImpl;

    /**
     * @param govWorkTypePo 数据
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> delete(GovWorkTypePo govWorkTypePo) {

        int flag = govWorkTypeInnerServiceSMOImpl.deleteGovWorkType(govWorkTypePo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
