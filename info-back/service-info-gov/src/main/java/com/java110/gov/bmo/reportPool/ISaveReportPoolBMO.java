package com.java110.gov.bmo.reportPool;

import com.alibaba.fastjson.JSONArray;
import com.java110.po.reportPool.ReportPoolPo;
import org.springframework.http.ResponseEntity;
public interface ISaveReportPoolBMO {


    /**
     * 添加报事管理
     * add by wuxw
     * @param reportPoolPo
     * @return
     */
    ResponseEntity<String> save(ReportPoolPo reportPoolPo);

    /**
     * 添加报事管理
     * add by wuxw
     * @param reportPoolPo
     * @return
     */
    ResponseEntity<String> saveTel(ReportPoolPo reportPoolPo, String userId);


    /**
     * 添加报事管理
     * add by wuxw
     * @param reportPoolPo
     * @return
     */
    ResponseEntity<String> savePhone(ReportPoolPo reportPoolPo, String userId, JSONArray photos);

}
