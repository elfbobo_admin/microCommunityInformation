package com.java110.gov.bmo.govPartyOrgContext.impl;


import com.java110.gov.bmo.govPartyOrgContext.IGetGovPartyOrgContextBMO;
import com.java110.intf.gov.IGovPartyOrgContextInnerServiceSMO;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.dto.govPartyOrgContext.GovPartyOrgContextDto;

import java.util.ArrayList;
import java.util.List;

@Service("getGovPartyOrgContextBMOImpl")
public class GetGovPartyOrgContextBMOImpl implements IGetGovPartyOrgContextBMO {

    @Autowired
    private IGovPartyOrgContextInnerServiceSMO govPartyOrgContextInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govPartyOrgContextDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovPartyOrgContextDto govPartyOrgContextDto) {


        int count = govPartyOrgContextInnerServiceSMOImpl.queryGovPartyOrgContextsCount(govPartyOrgContextDto);

        List<GovPartyOrgContextDto> govPartyOrgContextDtos = null;
        if (count > 0) {
            govPartyOrgContextDtos = govPartyOrgContextInnerServiceSMOImpl.queryGovPartyOrgContexts(govPartyOrgContextDto);
        } else {
            govPartyOrgContextDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govPartyOrgContextDto.getRow()), count, govPartyOrgContextDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
