package com.java110.gov.bmo.govCarInout.impl;

import com.java110.core.annotation.Java110Transactional;
import com.java110.gov.bmo.govCarInout.IUpdateGovCarInoutBMO;
import com.java110.intf.gov.IGovCarInoutInnerServiceSMO;
import com.java110.po.govCarInout.GovCarInoutPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("updateGovCarInoutBMOImpl")
public class UpdateGovCarInoutBMOImpl implements IUpdateGovCarInoutBMO {

    @Autowired
    private IGovCarInoutInnerServiceSMO govCarInoutInnerServiceSMOImpl;

    /**
     *
     *
     * @param govCarInoutPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> update(GovCarInoutPo govCarInoutPo) {

        int flag = govCarInoutInnerServiceSMOImpl.updateGovCarInout(govCarInoutPo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
