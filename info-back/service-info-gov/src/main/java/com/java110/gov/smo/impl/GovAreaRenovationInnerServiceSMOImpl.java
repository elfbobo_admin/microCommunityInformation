package com.java110.gov.smo.impl;


import com.java110.gov.dao.IGovAreaRenovationServiceDao;
import com.java110.intf.gov.IGovAreaRenovationInnerServiceSMO;
import com.java110.dto.govAreaRenovation.GovAreaRenovationDto;
import com.java110.po.govAreaRenovation.GovAreaRenovationPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 重点地区排查整治内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovAreaRenovationInnerServiceSMOImpl extends BaseServiceSMO implements IGovAreaRenovationInnerServiceSMO {

    @Autowired
    private IGovAreaRenovationServiceDao govAreaRenovationServiceDaoImpl;


    @Override
    public int saveGovAreaRenovation(@RequestBody  GovAreaRenovationPo govAreaRenovationPo) {
        int saveFlag = 1;
        govAreaRenovationServiceDaoImpl.saveGovAreaRenovationInfo(BeanConvertUtil.beanCovertMap(govAreaRenovationPo));
        return saveFlag;
    }

     @Override
    public int updateGovAreaRenovation(@RequestBody  GovAreaRenovationPo govAreaRenovationPo) {
        int saveFlag = 1;
         govAreaRenovationServiceDaoImpl.updateGovAreaRenovationInfo(BeanConvertUtil.beanCovertMap(govAreaRenovationPo));
        return saveFlag;
    }

     @Override
    public int deleteGovAreaRenovation(@RequestBody  GovAreaRenovationPo govAreaRenovationPo) {
        int saveFlag = 1;
        govAreaRenovationPo.setStatusCd("1");
        govAreaRenovationServiceDaoImpl.updateGovAreaRenovationInfo(BeanConvertUtil.beanCovertMap(govAreaRenovationPo));
        return saveFlag;
    }

    @Override
    public List<GovAreaRenovationDto> queryGovAreaRenovations(@RequestBody  GovAreaRenovationDto govAreaRenovationDto) {

        //校验是否传了 分页信息

        int page = govAreaRenovationDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govAreaRenovationDto.setPage((page - 1) * govAreaRenovationDto.getRow());
        }

        List<GovAreaRenovationDto> govAreaRenovations = BeanConvertUtil.covertBeanList(govAreaRenovationServiceDaoImpl.getGovAreaRenovationInfo(BeanConvertUtil.beanCovertMap(govAreaRenovationDto)), GovAreaRenovationDto.class);

        return govAreaRenovations;
    }


    @Override
    public int queryGovAreaRenovationsCount(@RequestBody GovAreaRenovationDto govAreaRenovationDto) {
        return govAreaRenovationServiceDaoImpl.queryGovAreaRenovationsCount(BeanConvertUtil.beanCovertMap(govAreaRenovationDto));    }

    public IGovAreaRenovationServiceDao getGovAreaRenovationServiceDaoImpl() {
        return govAreaRenovationServiceDaoImpl;
    }

    public void setGovAreaRenovationServiceDaoImpl(IGovAreaRenovationServiceDao govAreaRenovationServiceDaoImpl) {
        this.govAreaRenovationServiceDaoImpl = govAreaRenovationServiceDaoImpl;
    }
}
