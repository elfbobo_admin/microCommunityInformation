package com.java110.gov.smo.impl;


import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.PageDto;
import com.java110.dto.govActivity.GovActivityDto;
import com.java110.gov.dao.IGovActivityServiceDao;
import com.java110.intf.gov.IGovActivityInnerServiceSMO;
import com.java110.po.govActivity.GovActivityPo;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 活动内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovActivityInnerServiceSMOImpl extends BaseServiceSMO implements IGovActivityInnerServiceSMO {

    @Autowired
    private IGovActivityServiceDao govActivityServiceDaoImpl;


    @Override
    public int saveGovActivity(@RequestBody GovActivityPo govActivityPo) {
        int saveFlag = 1;
        govActivityServiceDaoImpl.saveGovActivityInfo(BeanConvertUtil.beanCovertMap(govActivityPo));
        return saveFlag;
    }

     @Override
    public int updateGovActivity(@RequestBody  GovActivityPo govActivityPo) {
        int saveFlag = 1;
         govActivityServiceDaoImpl.updateGovActivityInfo(BeanConvertUtil.beanCovertMap(govActivityPo));
        return saveFlag;
    }

     @Override
    public int deleteGovActivity(@RequestBody  GovActivityPo govActivityPo) {
        int saveFlag = 1;
        govActivityPo.setStatusCd("1");
        govActivityServiceDaoImpl.updateGovActivityInfo(BeanConvertUtil.beanCovertMap(govActivityPo));
        return saveFlag;
    }

    @Override
    public List<GovActivityDto> queryGovActivitys(@RequestBody  GovActivityDto govActivityDto) {

        //校验是否传了 分页信息

        int page = govActivityDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govActivityDto.setPage((page - 1) * govActivityDto.getRow());
        }

        List<GovActivityDto> govActivitys = BeanConvertUtil.covertBeanList(govActivityServiceDaoImpl.getGovActivityInfo(BeanConvertUtil.beanCovertMap(govActivityDto)), GovActivityDto.class);

        return govActivitys;
    }


    @Override
    public int queryGovActivitysCount(@RequestBody GovActivityDto govActivityDto) {
        return govActivityServiceDaoImpl.queryGovActivitysCount(BeanConvertUtil.beanCovertMap(govActivityDto));    }

    public IGovActivityServiceDao getGovActivityServiceDaoImpl() {
        return govActivityServiceDaoImpl;
    }

    public void setGovActivityServiceDaoImpl(IGovActivityServiceDao govActivityServiceDaoImpl) {
        this.govActivityServiceDaoImpl = govActivityServiceDaoImpl;
    }
}
