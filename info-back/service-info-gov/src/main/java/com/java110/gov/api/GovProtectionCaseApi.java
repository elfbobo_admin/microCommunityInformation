package com.java110.gov.api;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govProtectionCase.GovProtectionCaseDto;
import com.java110.po.govProtectionCase.GovProtectionCasePo;
import com.java110.gov.bmo.govProtectionCase.IDeleteGovProtectionCaseBMO;
import com.java110.gov.bmo.govProtectionCase.IGetGovProtectionCaseBMO;
import com.java110.gov.bmo.govProtectionCase.ISaveGovProtectionCaseBMO;
import com.java110.gov.bmo.govProtectionCase.IUpdateGovProtectionCaseBMO;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govProtectionCase")
public class GovProtectionCaseApi {

    @Autowired
    private ISaveGovProtectionCaseBMO saveGovProtectionCaseBMOImpl;
    @Autowired
    private IUpdateGovProtectionCaseBMO updateGovProtectionCaseBMOImpl;
    @Autowired
    private IDeleteGovProtectionCaseBMO deleteGovProtectionCaseBMOImpl;

    @Autowired
    private IGetGovProtectionCaseBMO getGovProtectionCaseBMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govProtectionCase/saveGovProtectionCase
     * @path /app/govProtectionCase/saveGovProtectionCase
     */
    @RequestMapping(value = "/saveGovProtectionCase", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovProtectionCase(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "objType", "请求报文中未包含objType");
        Assert.hasKeyAndValue(reqJson, "objId", "请求报文中未包含objId");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "caseName", "请求报文中未包含caseName");
        Assert.hasKeyAndValue(reqJson, "caseCode", "请求报文中未包含caseCode");
        Assert.hasKeyAndValue(reqJson, "areaCode", "请求报文中未包含areaCode");
        Assert.hasKeyAndValue(reqJson, "happenedTime", "请求报文中未包含happenedTime");
        Assert.hasKeyAndValue(reqJson, "happenedPlace", "请求报文中未包含happenedPlace");
        Assert.hasKeyAndValue(reqJson, "caseType", "请求报文中未包含caseType");
        Assert.hasKeyAndValue(reqJson, "idType", "请求报文中未包含idType");
        Assert.hasKeyAndValue(reqJson, "idCard", "请求报文中未包含idCard");
        Assert.hasKeyAndValue(reqJson, "personName", "请求报文中未包含personName");
        Assert.hasKeyAndValue(reqJson, "isSolved", "请求报文中未包含isSolved");
        Assert.hasKeyAndValue(reqJson, "personNum", "请求报文中未包含personNum");
        Assert.hasKeyAndValue(reqJson, "fleeingNum", "请求报文中未包含fleeingNum");
        Assert.hasKeyAndValue(reqJson, "arrestsNum", "请求报文中未包含arrestsNum");
        Assert.hasKeyAndValue(reqJson, "detectionContent", "请求报文中未包含detectionContent");
        Assert.hasKeyAndValue(reqJson, "caseContent", "请求报文中未包含caseContent");


        GovProtectionCasePo govProtectionCasePo = BeanConvertUtil.covertBean(reqJson, GovProtectionCasePo.class);
        return saveGovProtectionCaseBMOImpl.save(govProtectionCasePo);
    }

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govProtectionCase/savePhoneGovProtectionCase
     * @path /app/govProtectionCase/savePhoneGovProtectionCase
     */
    @RequestMapping(value = "/savePhoneGovProtectionCase", method = RequestMethod.POST)
    public ResponseEntity<String> savePhoneGovProtectionCase(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "objType", "请求报文中未包含objType");
        Assert.hasKeyAndValue(reqJson, "objId", "请求报文中未包含objId");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "caseName", "请求报文中未包含caseName");
        Assert.hasKeyAndValue(reqJson, "caseCode", "请求报文中未包含caseCode");
        Assert.hasKeyAndValue(reqJson, "happenedTime", "请求报文中未包含happenedTime");
        Assert.hasKeyAndValue(reqJson, "happenedPlace", "请求报文中未包含happenedPlace");
        Assert.hasKeyAndValue(reqJson, "caseType", "请求报文中未包含caseType");
        Assert.hasKeyAndValue(reqJson, "idType", "请求报文中未包含idType");
        Assert.hasKeyAndValue(reqJson, "idCard", "请求报文中未包含idCard");
        Assert.hasKeyAndValue(reqJson, "personName", "请求报文中未包含personName");
        Assert.hasKeyAndValue(reqJson, "isSolved", "请求报文中未包含isSolved");
        Assert.hasKeyAndValue(reqJson, "personNum", "请求报文中未包含personNum");
        Assert.hasKeyAndValue(reqJson, "fleeingNum", "请求报文中未包含fleeingNum");
        Assert.hasKeyAndValue(reqJson, "arrestsNum", "请求报文中未包含arrestsNum");
        Assert.hasKeyAndValue(reqJson, "detectionContent", "请求报文中未包含detectionContent");
        Assert.hasKeyAndValue(reqJson, "caseContent", "请求报文中未包含caseContent");

        GovProtectionCasePo govProtectionCasePo = BeanConvertUtil.covertBean(reqJson, GovProtectionCasePo.class);
        JSONArray photos = reqJson.getJSONArray("photos");
        return saveGovProtectionCaseBMOImpl.savePhone(govProtectionCasePo,photos);
    }
    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govProtectionCase/updateGovProtectionCase
     * @path /app/govProtectionCase/updateGovProtectionCase
     */
    @RequestMapping(value = "/updateGovProtectionCase", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovProtectionCase(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caseId", "请求报文中未包含caseId");
        Assert.hasKeyAndValue(reqJson, "objType", "请求报文中未包含objType");
        Assert.hasKeyAndValue(reqJson, "objId", "请求报文中未包含objId");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "caseName", "请求报文中未包含caseName");
        Assert.hasKeyAndValue(reqJson, "caseCode", "请求报文中未包含caseCode");
        Assert.hasKeyAndValue(reqJson, "areaCode", "请求报文中未包含areaCode");
        Assert.hasKeyAndValue(reqJson, "happenedTime", "请求报文中未包含happenedTime");
        Assert.hasKeyAndValue(reqJson, "happenedPlace", "请求报文中未包含happenedPlace");
        Assert.hasKeyAndValue(reqJson, "caseType", "请求报文中未包含caseType");
        Assert.hasKeyAndValue(reqJson, "idType", "请求报文中未包含idType");
        Assert.hasKeyAndValue(reqJson, "idCard", "请求报文中未包含idCard");
        Assert.hasKeyAndValue(reqJson, "personName", "请求报文中未包含personName");
        Assert.hasKeyAndValue(reqJson, "isSolved", "请求报文中未包含isSolved");
        Assert.hasKeyAndValue(reqJson, "personNum", "请求报文中未包含personNum");
        Assert.hasKeyAndValue(reqJson, "fleeingNum", "请求报文中未包含fleeingNum");
        Assert.hasKeyAndValue(reqJson, "arrestsNum", "请求报文中未包含arrestsNum");
        Assert.hasKeyAndValue(reqJson, "detectionContent", "请求报文中未包含detectionContent");
        Assert.hasKeyAndValue(reqJson, "caseContent", "请求报文中未包含caseContent");

        GovProtectionCasePo govProtectionCasePo = BeanConvertUtil.covertBean(reqJson, GovProtectionCasePo.class);
        return updateGovProtectionCaseBMOImpl.update(govProtectionCasePo);
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govProtectionCase/deleteGovProtectionCase
     * @path /app/govProtectionCase/deleteGovProtectionCase
     */
    @RequestMapping(value = "/deleteGovProtectionCase", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovProtectionCase(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caseId", "caseId不能为空");


        GovProtectionCasePo govProtectionCasePo = BeanConvertUtil.covertBean(reqJson, GovProtectionCasePo.class);
        return deleteGovProtectionCaseBMOImpl.delete(govProtectionCasePo);
    }

    /**
     * 微信删除消息模板
     *
     * @param caId 小区ID
     * @return
     * @serviceCode /govProtectionCase/queryGovProtectionCase
     * @path /app/govProtectionCase/queryGovProtectionCase
     */
    @RequestMapping(value = "/queryGovProtectionCase", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovProtectionCase(@RequestParam(value = "caId") String caId,
                                                         @RequestParam(value = "objType",required = false) String objType,
                                                         @RequestParam(value = "objId",required = false) String objId,
                                                         @RequestParam(value = "objName",required = false) String objName,
                                                         @RequestParam(value = "happenedPlace",required = false) String happenedPlace,
                                                         @RequestParam(value = "happenedTime",required = false) String happenedTime,
                                                         @RequestParam(value = "personName",required = false) String personName,
                                                         @RequestParam(value = "isSolved",required = false) String isSolved,
                                                         @RequestParam(value = "idCard",required = false) String idCard,
                                                         @RequestParam(value = "caseName",required = false) String caseName,
                                                         @RequestParam(value = "caseCode",required = false) String caseCode,
                                                         @RequestParam(value = "page") int page,
                                                         @RequestParam(value = "row") int row) {
        GovProtectionCaseDto govProtectionCaseDto = new GovProtectionCaseDto();
        govProtectionCaseDto.setPage(page);
        govProtectionCaseDto.setRow(row);
        govProtectionCaseDto.setCaId(caId);
        govProtectionCaseDto.setHappenedPlace(happenedPlace);
        govProtectionCaseDto.setHappenedTime(happenedTime);
        govProtectionCaseDto.setPersonName(personName);
        govProtectionCaseDto.setIsSolved(isSolved);
        govProtectionCaseDto.setIdCard(idCard);
        govProtectionCaseDto.setCaseName(caseName);
        govProtectionCaseDto.setObjType(objType);
        govProtectionCaseDto.setObjId(objId);
        govProtectionCaseDto.setObjName(objName);
        return getGovProtectionCaseBMOImpl.get(govProtectionCaseDto);
    }
}
