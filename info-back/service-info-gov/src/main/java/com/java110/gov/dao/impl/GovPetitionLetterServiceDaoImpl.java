package com.java110.gov.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.gov.dao.IGovPetitionLetterServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 信访管理服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govPetitionLetterServiceDaoImpl")
//@Transactional
public class GovPetitionLetterServiceDaoImpl extends BaseServiceDao implements IGovPetitionLetterServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovPetitionLetterServiceDaoImpl.class);





    /**
     * 保存信访管理信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovPetitionLetterInfo(Map info) throws DAOException {
        logger.debug("保存信访管理信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govPetitionLetterServiceDaoImpl.saveGovPetitionLetterInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存信访管理信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询信访管理信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovPetitionLetterInfo(Map info) throws DAOException {
        logger.debug("查询信访管理信息 入参 info : {}",info);

        List<Map> businessGovPetitionLetterInfos = sqlSessionTemplate.selectList("govPetitionLetterServiceDaoImpl.getGovPetitionLetterInfo",info);

        return businessGovPetitionLetterInfos;
    }


    /**
     * 修改信访管理信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovPetitionLetterInfo(Map info) throws DAOException {
        logger.debug("修改信访管理信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govPetitionLetterServiceDaoImpl.updateGovPetitionLetterInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改信访管理信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询信访管理数量
     * @param info 信访管理信息
     * @return 信访管理数量
     */
    @Override
    public int queryGovPetitionLettersCount(Map info) {
        logger.debug("查询信访管理数据 入参 info : {}",info);

        List<Map> businessGovPetitionLetterInfos = sqlSessionTemplate.selectList("govPetitionLetterServiceDaoImpl.queryGovPetitionLettersCount", info);
        if (businessGovPetitionLetterInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovPetitionLetterInfos.get(0).get("count").toString());
    }


}
