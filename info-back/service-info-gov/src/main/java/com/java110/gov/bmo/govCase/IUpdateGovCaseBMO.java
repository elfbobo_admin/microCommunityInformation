package com.java110.gov.bmo.govCase;
import org.springframework.http.ResponseEntity;
import com.java110.po.govCase.GovCasePo;

public interface IUpdateGovCaseBMO {


    /**
     * 修改命案基本信息
     * add by wuxw
     * @param govCasePo
     * @return
     */
    ResponseEntity<String> update(GovCasePo govCasePo);


}
