package com.java110.gov.dao;


import com.java110.utils.exception.DAOException;


import java.util.List;
import java.util.Map;

/**
 * 吸毒者组件内部之间使用，没有给外围系统提供服务能力
 * 吸毒者服务接口类，要求全部以字符串传输，方便微服务化
 * 新建客户，修改客户，删除客户，查询客户等功能
 *
 * Created by wuxw on 2016/12/27.
 */
public interface IGovDrugServiceDao {


    /**
     * 保存 吸毒者信息
     * @param info
     * @throws DAOException DAO异常
     */
    void saveGovDrugInfo(Map info) throws DAOException;




    /**
     * 查询吸毒者信息（instance过程）
     * 根据bId 查询吸毒者信息
     * @param info bId 信息
     * @return 吸毒者信息
     * @throws DAOException DAO异常
     */
    List<Map> getGovDrugInfo(Map info) throws DAOException;



    /**
     * 修改吸毒者信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    void updateGovDrugInfo(Map info) throws DAOException;


    /**
     * 查询吸毒者总数
     *
     * @param info 吸毒者信息
     * @return 吸毒者数量
     */
    int queryGovDrugsCount(Map info);

}
