package com.java110.gov.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govAdvert.GovAdvertDto;
import com.java110.gov.bmo.govAdvert.IDeleteGovAdvertBMO;
import com.java110.gov.bmo.govAdvert.IGetGovAdvertBMO;
import com.java110.gov.bmo.govAdvert.ISaveGovAdvertBMO;
import com.java110.gov.bmo.govAdvert.IUpdateGovAdvertBMO;
import com.java110.po.govAdvert.GovAdvertPo;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.utils.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(value = "/govAdvert")
public class GovAdvertApi {

    @Autowired
    private ISaveGovAdvertBMO saveGovAdvertBMOImpl;
    @Autowired
    private IUpdateGovAdvertBMO updateGovAdvertBMOImpl;
    @Autowired
    private IDeleteGovAdvertBMO deleteGovAdvertBMOImpl;

    @Autowired
    private IGetGovAdvertBMO getGovAdvertBMOImpl;

    /**
     * 微信保存消息模板
     * @serviceCode /govAdvert/saveGovAdvert
     * @path /app/govAdvert/saveGovAdvert
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/saveGovAdvert", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovAdvert(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "adName", "请求报文中未包含adName");
        Assert.hasKeyAndValue(reqJson, "adTypeCd", "请求报文中未包含adTypeCd");
        Assert.hasKeyAndValue(reqJson, "seq", "请求报文中未包含seq");
        Assert.hasKeyAndValue(reqJson, "startTime", "请求报文中未包含startTime");
        Assert.hasKeyAndValue(reqJson, "endTime", "请求报文中未包含endTime");
        Assert.hasKeyAndValue(reqJson, "viewType", "请求报文中未包含viewType");
        Assert.hasKeyAndValue(reqJson, "advertType", "请求报文中未包含advertType");
        if (!hasKeyAndValue(reqJson, "photos") && !hasKeyAndValue(reqJson, "vedioName")) {
            throw new IllegalArgumentException("请求报文中没有包含视频或图片");
        }
        return saveGovAdvertBMOImpl.save(reqJson);
    }
    private boolean hasKeyAndValue(JSONObject paramIn, String key) {
        if (!paramIn.containsKey(key)) {
            return false;
        }

        if (StringUtil.isEmpty(paramIn.getString(key))) {
            return false;
        }

        return true;
    }

    /**
     * 微信修改消息模板
     * @serviceCode /govAdvert/updateGovAdvert
     * @path /app/govAdvert/updateGovAdvert
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/updateGovAdvert", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovAdvert(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "adName", "请求报文中未包含adName");
        Assert.hasKeyAndValue(reqJson, "adTypeCd", "请求报文中未包含adTypeCd");
        Assert.hasKeyAndValue(reqJson, "seq", "请求报文中未包含seq");
        Assert.hasKeyAndValue(reqJson, "startTime", "请求报文中未包含startTime");
        Assert.hasKeyAndValue(reqJson, "endTime", "请求报文中未包含endTime");
        Assert.hasKeyAndValue(reqJson, "viewType", "请求报文中未包含viewType");
        Assert.hasKeyAndValue(reqJson, "advertType", "请求报文中未包含advertType");
        Assert.hasKeyAndValue(reqJson, "advertId", "advertId不能为空");

        return updateGovAdvertBMOImpl.update(reqJson);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govAdvert/deleteGovAdvert
     * @path /app/govAdvert/deleteGovAdvert
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/deleteGovAdvert", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovAdvert(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "advertId", "advertId不能为空");


        GovAdvertPo govAdvertPo = BeanConvertUtil.covertBean(reqJson, GovAdvertPo.class);
        return deleteGovAdvertBMOImpl.delete(govAdvertPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govAdvert/queryGovAdvert
     * @path /app/govAdvert/queryGovAdvert
     * @param caId 小区ID
     * @return
     */
    @RequestMapping(value = "/queryGovAdvert", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovAdvert(@RequestParam(value = "caId") String caId,
                                                 @RequestParam(value = "adName" , required = false) String adName,
                                                 @RequestParam(value = "adTypeCd" , required = false) String adTypeCd,
                                                 @RequestParam(value = "viewType" , required = false) String viewType,
                                                 @RequestParam(value = "advertType" , required = false) String advertType,
                                                 @RequestParam(value = "state" , required = false) String state,
                                                 @RequestParam(value = "page") int page,
                                                 @RequestParam(value = "row") int row) {
        GovAdvertDto govAdvertDto = new GovAdvertDto();
        govAdvertDto.setPage(page);
        govAdvertDto.setRow(row);
        govAdvertDto.setCaId(caId);
        govAdvertDto.setAdName(adName);
        govAdvertDto.setAdTypeCd(adTypeCd);
        govAdvertDto.setViewType(viewType);
        govAdvertDto.setAdvertType(advertType);
        govAdvertDto.setState(state);
        return getGovAdvertBMOImpl.get(govAdvertDto);
    }


    /**
     * 微信删除消息模板
     * @serviceCode /govAdvert/queryGovAdvert
     * @path /app/govAdvert/queryGovAdvert
     * @param caId 小区ID
     * @return
     */
    @RequestMapping(value = "/queryGovAdvertItems", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovAdvertItems(@RequestParam(value = "caId") String caId,
                                                 @RequestParam(value = "adName" , required = false) String adName,
                                                 @RequestParam(value = "adTypeCd" , required = false) String adTypeCd,
                                                 @RequestParam(value = "viewType" , required = false) String viewType,
                                                 @RequestParam(value = "advertType" , required = false) String advertType,
                                                 @RequestParam(value = "state" , required = false) String state,
                                                 @RequestParam(value = "page") int page,
                                                 @RequestParam(value = "row") int row) {
        GovAdvertDto govAdvertDto = new GovAdvertDto();
        govAdvertDto.setPage(page);
        govAdvertDto.setRow(row);
        govAdvertDto.setCaId(caId);
        govAdvertDto.setAdName(adName);
        govAdvertDto.setAdTypeCd(adTypeCd);
        govAdvertDto.setViewType(viewType);
        govAdvertDto.setAdvertType(advertType);
        govAdvertDto.setState(state);
        return getGovAdvertBMOImpl.getAdvertItmes(govAdvertDto);
    }
}
