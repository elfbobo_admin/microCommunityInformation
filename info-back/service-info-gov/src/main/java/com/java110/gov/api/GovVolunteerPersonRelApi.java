package com.java110.gov.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govVolunteerPersonRel.GovVolunteerPersonRelDto;
import com.java110.po.govVolunteerPersonRel.GovVolunteerPersonRelPo;
import com.java110.gov.bmo.govVolunteerPersonRel.IDeleteGovVolunteerPersonRelBMO;
import com.java110.gov.bmo.govVolunteerPersonRel.IGetGovVolunteerPersonRelBMO;
import com.java110.gov.bmo.govVolunteerPersonRel.ISaveGovVolunteerPersonRelBMO;
import com.java110.gov.bmo.govVolunteerPersonRel.IUpdateGovVolunteerPersonRelBMO;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govVolunteerPersonRel")
public class GovVolunteerPersonRelApi {

    @Autowired
    private ISaveGovVolunteerPersonRelBMO saveGovVolunteerPersonRelBMOImpl;
    @Autowired
    private IUpdateGovVolunteerPersonRelBMO updateGovVolunteerPersonRelBMOImpl;
    @Autowired
    private IDeleteGovVolunteerPersonRelBMO deleteGovVolunteerPersonRelBMOImpl;

    @Autowired
    private IGetGovVolunteerPersonRelBMO getGovVolunteerPersonRelBMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govVolunteerPersonRel/saveGovVolunteerPersonRel
     * @path /app/govVolunteerPersonRel/saveGovVolunteerPersonRel
     */
    @RequestMapping(value = "/saveGovVolunteerPersonRel", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovVolunteerPersonRel(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "volunteerPersonRelId", "请求报文中未包含volunteerPersonRelId");
        Assert.hasKeyAndValue(reqJson, "govPersonId", "请求报文中未包含govPersonId");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "serviceRecordId", "请求报文中未包含serviceRecordId");
        Assert.hasKeyAndValue(reqJson, "govCommunityId", "请求报文中未包含govCommunityId");
        Assert.hasKeyAndValue(reqJson, "volunteerId", "请求报文中未包含volunteerId");


        GovVolunteerPersonRelPo govVolunteerPersonRelPo = BeanConvertUtil.covertBean(reqJson, GovVolunteerPersonRelPo.class);
        return saveGovVolunteerPersonRelBMOImpl.save(govVolunteerPersonRelPo);
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govVolunteerPersonRel/updateGovVolunteerPersonRel
     * @path /app/govVolunteerPersonRel/updateGovVolunteerPersonRel
     */
    @RequestMapping(value = "/updateGovVolunteerPersonRel", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovVolunteerPersonRel(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "volunteerPersonRelId", "请求报文中未包含volunteerPersonRelId");
        Assert.hasKeyAndValue(reqJson, "govPersonId", "请求报文中未包含govPersonId");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "serviceRecordId", "请求报文中未包含serviceRecordId");
        Assert.hasKeyAndValue(reqJson, "govCommunityId", "请求报文中未包含govCommunityId");
        Assert.hasKeyAndValue(reqJson, "volunteerId", "请求报文中未包含volunteerId");
        Assert.hasKeyAndValue(reqJson, "volunteerPersonRelId", "volunteerPersonRelId不能为空");


        GovVolunteerPersonRelPo govVolunteerPersonRelPo = BeanConvertUtil.covertBean(reqJson, GovVolunteerPersonRelPo.class);
        return updateGovVolunteerPersonRelBMOImpl.update(govVolunteerPersonRelPo);
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govVolunteerPersonRel/deleteGovVolunteerPersonRel
     * @path /app/govVolunteerPersonRel/deleteGovVolunteerPersonRel
     */
    @RequestMapping(value = "/deleteGovVolunteerPersonRel", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovVolunteerPersonRel(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "caId", "caId不能为空");

        Assert.hasKeyAndValue(reqJson, "volunteerPersonRelId", "volunteerPersonRelId不能为空");


        GovVolunteerPersonRelPo govVolunteerPersonRelPo = BeanConvertUtil.covertBean(reqJson, GovVolunteerPersonRelPo.class);
        return deleteGovVolunteerPersonRelBMOImpl.delete(govVolunteerPersonRelPo);
    }

    /**
     * 微信删除消息模板
     *
     * @param caId 小区ID
     * @return
     * @serviceCode /govVolunteerPersonRel/queryGovVolunteerPersonRel
     * @path /app/govVolunteerPersonRel/queryGovVolunteerPersonRel
     */
    @RequestMapping(value = "/queryGovVolunteerPersonRel", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovVolunteerPersonRel(@RequestParam(value = "caId") String caId,
                                                             @RequestParam(value = "page") int page,
                                                             @RequestParam(value = "row") int row) {
        GovVolunteerPersonRelDto govVolunteerPersonRelDto = new GovVolunteerPersonRelDto();
        govVolunteerPersonRelDto.setPage(page);
        govVolunteerPersonRelDto.setRow(row);
        govVolunteerPersonRelDto.setCaId(caId);
        return getGovVolunteerPersonRelBMOImpl.get(govVolunteerPersonRelDto);
    }
}
