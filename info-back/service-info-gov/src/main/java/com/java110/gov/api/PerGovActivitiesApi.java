package com.java110.gov.api;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.java110.dto.perGovActivities.PerGovActivitiesDto;
import com.java110.po.perGovActivities.PerGovActivitiesPo;
import com.java110.gov.bmo.perGovActivities.IDeletePerGovActivitiesBMO;
import com.java110.gov.bmo.perGovActivities.IGetPerGovActivitiesBMO;
import com.java110.gov.bmo.perGovActivities.ISavePerGovActivitiesBMO;
import com.java110.gov.bmo.perGovActivities.IUpdatePerGovActivitiesBMO;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/perGovActivities")
public class PerGovActivitiesApi {

    @Autowired
    private ISavePerGovActivitiesBMO savePerGovActivitiesBMOImpl;
    @Autowired
    private IUpdatePerGovActivitiesBMO updatePerGovActivitiesBMOImpl;
    @Autowired
    private IDeletePerGovActivitiesBMO deletePerGovActivitiesBMOImpl;

    @Autowired
    private IGetPerGovActivitiesBMO getPerGovActivitiesBMOImpl;

    /**
     * 微信保存消息模板
     * @serviceCode /perGovActivities/savePerGovActivities
     * @path /app/perGovActivities/savePerGovActivities
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/savePerGovActivities", method = RequestMethod.POST)
    public ResponseEntity<String> savePerGovActivities(@RequestHeader(value = "user-id") String userId,
                                                       @RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "title", "请求报文中未包含title");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "context", "请求报文中未包含context");
        Assert.hasKeyAndValue(reqJson, "startTime", "请求报文中未包含startTime");
        Assert.hasKeyAndValue(reqJson, "endTime", "请求报文中未包含endTime");
        Assert.hasKeyAndValue(reqJson, "activityTime", "请求报文中未包含activityTime");
        JSONArray oldPersons = reqJson.getJSONArray("oldPersons");
        if(oldPersons == null || oldPersons.size() < 1){
            throw new IllegalArgumentException("未选择老人，请添加本月过生日的老人");
        }
        PerGovActivitiesPo perGovActivitiesPo = BeanConvertUtil.covertBean(reqJson, PerGovActivitiesPo.class);
        perGovActivitiesPo.setUserId(userId);
        return savePerGovActivitiesBMOImpl.save(perGovActivitiesPo,oldPersons);
    }

    /**
     * 微信修改消息模板
     * @serviceCode /perGovActivities/updatePerGovActivities
     * @path /app/perGovActivities/updatePerGovActivities
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/updatePerGovActivities", method = RequestMethod.POST)
    public ResponseEntity<String> updatePerGovActivities(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "perActivitiesId", "请求报文中未包含perActivitiesId");
        Assert.hasKeyAndValue(reqJson, "title", "请求报文中未包含title");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "context", "请求报文中未包含context");
        Assert.hasKeyAndValue(reqJson, "startTime", "请求报文中未包含startTime");
        Assert.hasKeyAndValue(reqJson, "endTime", "请求报文中未包含endTime");
        Assert.hasKeyAndValue(reqJson, "activityTime", "请求报文中未包含activityTime");
        JSONArray oldPersons = reqJson.getJSONArray("oldPersons");
        if(oldPersons == null || oldPersons.size() < 1){
            throw new IllegalArgumentException("未选择老人，请添加本月过生日的老人");
        }
        PerGovActivitiesPo perGovActivitiesPo = BeanConvertUtil.covertBean(reqJson, PerGovActivitiesPo.class);
        return updatePerGovActivitiesBMOImpl.update(perGovActivitiesPo,oldPersons);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /perGovActivities/deletePerGovActivities
     * @path /app/perGovActivities/deletePerGovActivities
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/deletePerGovActivities", method = RequestMethod.POST)
    public ResponseEntity<String> deletePerGovActivities(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "perActivitiesId", "perActivitiesId不能为空");


        PerGovActivitiesPo perGovActivitiesPo = BeanConvertUtil.covertBean(reqJson, PerGovActivitiesPo.class);
        return deletePerGovActivitiesBMOImpl.delete(perGovActivitiesPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /perGovActivities/queryPerGovActivities
     * @path /app/perGovActivities/queryPerGovActivities
     * @param caId 小区ID
     * @return
     */
    @RequestMapping(value = "/queryPerGovActivities", method = RequestMethod.GET)
    public ResponseEntity<String> queryPerGovActivities(@RequestParam(value = "caId") String caId,
                                                        @RequestParam(value = "perActivitiesId", required = false) String perActivitiesId,
                                                        @RequestParam(value = "title", required = false) String title,
                                                        @RequestParam(value = "activityTime", required = false) String activityTime,
                                                      @RequestParam(value = "page") int page,
                                                      @RequestParam(value = "row") int row) {
        PerGovActivitiesDto perGovActivitiesDto = new PerGovActivitiesDto();
        perGovActivitiesDto.setPage(page);
        perGovActivitiesDto.setRow(row);
        perGovActivitiesDto.setCaId(caId);
        perGovActivitiesDto.setActivityTime(activityTime);
        perGovActivitiesDto.setTitle(title);
        perGovActivitiesDto.setPerActivitiesId(perActivitiesId);
        return getPerGovActivitiesBMOImpl.get(perGovActivitiesDto);
    }
}
