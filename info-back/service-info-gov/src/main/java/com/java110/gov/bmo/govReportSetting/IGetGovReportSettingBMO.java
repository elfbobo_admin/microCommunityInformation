package com.java110.gov.bmo.govReportSetting;
import com.java110.dto.govReportSetting.GovReportSettingDto;
import org.springframework.http.ResponseEntity;
public interface IGetGovReportSettingBMO {


    /**
     * 查询报事设置
     * add by wuxw
     * @param  govReportSettingDto
     * @return
     */
    ResponseEntity<String> get(GovReportSettingDto govReportSettingDto);


}
