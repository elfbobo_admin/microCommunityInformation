package com.java110.gov.bmo.govReportTypeUser.impl;

import com.java110.gov.bmo.govReportTypeUser.IGetGovReportTypeUserBMO;
import com.java110.intf.gov.IGovReportTypeUserInnerServiceSMO;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.dto.govReportTypeUser.GovReportTypeUserDto;

import java.util.ArrayList;
import java.util.List;

@Service("getGovReportTypeUserBMOImpl")
public class GetGovReportTypeUserBMOImpl implements IGetGovReportTypeUserBMO {

    @Autowired
    private IGovReportTypeUserInnerServiceSMO govReportTypeUserInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govReportTypeUserDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovReportTypeUserDto govReportTypeUserDto) {


        int count = govReportTypeUserInnerServiceSMOImpl.queryGovReportTypeUsersCount(govReportTypeUserDto);

        List<GovReportTypeUserDto> govReportTypeUserDtos = null;
        if (count > 0) {
            govReportTypeUserDtos = govReportTypeUserInnerServiceSMOImpl.queryGovReportTypeUsers(govReportTypeUserDto);
        } else {
            govReportTypeUserDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govReportTypeUserDto.getRow()), count, govReportTypeUserDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
