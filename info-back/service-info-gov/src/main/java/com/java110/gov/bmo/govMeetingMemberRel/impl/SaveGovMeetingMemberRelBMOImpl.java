package com.java110.gov.bmo.govMeetingMemberRel.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.gov.bmo.govMeetingMemberRel.ISaveGovMeetingMemberRelBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.po.govMeetingMemberRel.GovMeetingMemberRelPo;
import com.java110.intf.gov.IGovMeetingMemberRelInnerServiceSMO;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("saveGovMeetingMemberRelBMOImpl")
public class SaveGovMeetingMemberRelBMOImpl implements ISaveGovMeetingMemberRelBMO {

    @Autowired
    private IGovMeetingMemberRelInnerServiceSMO govMeetingMemberRelInnerServiceSMOImpl;

    /**
     * 添加小区信息
     *
     * @param govMeetingMemberRelPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> save(GovMeetingMemberRelPo govMeetingMemberRelPo) {

        govMeetingMemberRelPo.setMemberRelId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_memberRelId));
        int flag = govMeetingMemberRelInnerServiceSMOImpl.saveGovMeetingMemberRel(govMeetingMemberRelPo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
