package com.java110.gov.bmo.govMeetingList;
import org.springframework.http.ResponseEntity;
import com.java110.po.govMeetingList.GovMeetingListPo;

public interface IDeleteGovMeetingListBMO {


    /**
     * 修改会议列表
     * add by wuxw
     * @param govMeetingListPo
     * @return
     */
    ResponseEntity<String> delete(GovMeetingListPo govMeetingListPo);


}
