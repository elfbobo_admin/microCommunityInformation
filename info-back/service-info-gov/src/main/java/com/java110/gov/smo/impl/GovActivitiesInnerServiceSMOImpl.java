package com.java110.gov.smo.impl;


import com.java110.gov.dao.IGovActivitiesServiceDao;
import com.java110.dto.govActivities.GovActivitiesDto;
import com.java110.intf.gov.IGovActivitiesInnerServiceSMO;
import com.java110.po.govActivities.GovActivitiesPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 公告管理内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovActivitiesInnerServiceSMOImpl extends BaseServiceSMO implements IGovActivitiesInnerServiceSMO {

    @Autowired
    private IGovActivitiesServiceDao govActivitiesServiceDaoImpl;


    @Override
    public int saveGovActivities(@RequestBody GovActivitiesPo govActivitiesPo) {
        int saveFlag = 1;
        govActivitiesServiceDaoImpl.saveGovActivitiesInfo(BeanConvertUtil.beanCovertMap(govActivitiesPo));
        return saveFlag;
    }

     @Override
    public int updateGovActivities(@RequestBody  GovActivitiesPo govActivitiesPo) {
        int saveFlag = 1;
         govActivitiesServiceDaoImpl.updateGovActivitiesInfo(BeanConvertUtil.beanCovertMap(govActivitiesPo));
        return saveFlag;
    }

     @Override
    public int deleteGovActivities(@RequestBody  GovActivitiesPo govActivitiesPo) {
        int saveFlag = 1;
        govActivitiesPo.setStatusCd("1");
        govActivitiesServiceDaoImpl.updateGovActivitiesInfo(BeanConvertUtil.beanCovertMap(govActivitiesPo));
        return saveFlag;
    }

    @Override
    public List<GovActivitiesDto> queryGovActivitiess(@RequestBody  GovActivitiesDto govActivitiesDto) {

        //校验是否传了 分页信息

        int page = govActivitiesDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govActivitiesDto.setPage((page - 1) * govActivitiesDto.getRow());
        }

        List<GovActivitiesDto> govActivitiess = BeanConvertUtil.covertBeanList(govActivitiesServiceDaoImpl.getGovActivitiesInfo(BeanConvertUtil.beanCovertMap(govActivitiesDto)), GovActivitiesDto.class);

        return govActivitiess;
    }


    @Override
    public int queryGovActivitiessCount(@RequestBody GovActivitiesDto govActivitiesDto) {
        return govActivitiesServiceDaoImpl.queryGovActivitiessCount(BeanConvertUtil.beanCovertMap(govActivitiesDto));    }

    public IGovActivitiesServiceDao getGovActivitiesServiceDaoImpl() {
        return govActivitiesServiceDaoImpl;
    }

    public void setGovActivitiesServiceDaoImpl(IGovActivitiesServiceDao govActivitiesServiceDaoImpl) {
        this.govActivitiesServiceDaoImpl = govActivitiesServiceDaoImpl;
    }
}
