package com.java110.gov.bmo.govReportTypeUser.impl;

import com.java110.core.annotation.Java110Transactional;
import com.java110.gov.bmo.govReportTypeUser.IUpdateGovReportTypeUserBMO;
import com.java110.intf.gov.IGovReportTypeUserInnerServiceSMO;
import com.java110.po.govReportTypeUser.GovReportTypeUserPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("updateGovReportTypeUserBMOImpl")
public class UpdateGovReportTypeUserBMOImpl implements IUpdateGovReportTypeUserBMO {

    @Autowired
    private IGovReportTypeUserInnerServiceSMO govReportTypeUserInnerServiceSMOImpl;

    /**
     *
     *
     * @param govReportTypeUserPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> update(GovReportTypeUserPo govReportTypeUserPo) {

        int flag = govReportTypeUserInnerServiceSMOImpl.updateGovReportTypeUser(govReportTypeUserPo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
