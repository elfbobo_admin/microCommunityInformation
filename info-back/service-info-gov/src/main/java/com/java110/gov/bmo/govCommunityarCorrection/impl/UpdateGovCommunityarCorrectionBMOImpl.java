package com.java110.gov.bmo.govCommunityarCorrection.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.gov.bmo.govCommunityarCorrection.IUpdateGovCommunityarCorrectionBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.gov.IGovCommunityarCorrectionInnerServiceSMO;
import com.java110.dto.govCommunityarCorrection.GovCommunityarCorrectionDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.java110.po.govCommunityarCorrection.GovCommunityarCorrectionPo;
import java.util.List;

@Service("updateGovCommunityarCorrectionBMOImpl")
public class UpdateGovCommunityarCorrectionBMOImpl implements IUpdateGovCommunityarCorrectionBMO {

    @Autowired
    private IGovCommunityarCorrectionInnerServiceSMO govCommunityarCorrectionInnerServiceSMOImpl;

    /**
     *
     *
     * @param govCommunityarCorrectionPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> update(GovCommunityarCorrectionPo govCommunityarCorrectionPo) {

        int flag = govCommunityarCorrectionInnerServiceSMOImpl.updateGovCommunityarCorrection(govCommunityarCorrectionPo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
