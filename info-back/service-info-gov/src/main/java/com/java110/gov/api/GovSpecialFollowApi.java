package com.java110.gov.api;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govSpecialFollow.GovSpecialFollowDto;
import com.java110.po.govSpecialFollow.GovSpecialFollowPo;
import com.java110.gov.bmo.govSpecialFollow.IDeleteGovSpecialFollowBMO;
import com.java110.gov.bmo.govSpecialFollow.IGetGovSpecialFollowBMO;
import com.java110.gov.bmo.govSpecialFollow.ISaveGovSpecialFollowBMO;
import com.java110.gov.bmo.govSpecialFollow.IUpdateGovSpecialFollowBMO;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govSpecialFollow")
public class GovSpecialFollowApi {

    @Autowired
    private ISaveGovSpecialFollowBMO saveGovSpecialFollowBMOImpl;
    @Autowired
    private IUpdateGovSpecialFollowBMO updateGovSpecialFollowBMOImpl;
    @Autowired
    private IDeleteGovSpecialFollowBMO deleteGovSpecialFollowBMOImpl;

    @Autowired
    private IGetGovSpecialFollowBMO getGovSpecialFollowBMOImpl;

    /**
     * 微信保存消息模板
     * @serviceCode /govSpecialFollow/saveGovSpecialFollow
     * @path /app/govSpecialFollow/saveGovSpecialFollow
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/saveGovSpecialFollow", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovSpecialFollow(@RequestBody JSONObject reqJson) {


     Assert.hasKeyAndValue(reqJson, "specialPersonId", "请求报文中未包含specialPersonId");
    Assert.hasKeyAndValue(reqJson, "name", "请求报文中未包含name");
    Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
    Assert.hasKeyAndValue(reqJson, "specialStartTime", "请求报文中未包含specialStartTime");
    Assert.hasKeyAndValue(reqJson, "specialReason", "请求报文中未包含specialReason");
    Assert.hasKeyAndValue(reqJson, "isWork", "请求报文中未包含isWork");


        GovSpecialFollowPo govSpecialFollowPo = BeanConvertUtil.covertBean(reqJson, GovSpecialFollowPo.class);
        return saveGovSpecialFollowBMOImpl.save(govSpecialFollowPo);
    }

    /**
     * 微信保存消息模板
     * @serviceCode /govSpecialFollow/saveGovSpecialFollow
     * @path /app/govSpecialFollow/saveGovSpecialFollow
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/savePhoneGovSpecialFollow", method = RequestMethod.POST)
    public ResponseEntity<String> savePhoneGovSpecialFollow(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "specialPersonId", "请求报文中未包含specialPersonId");
        Assert.hasKeyAndValue(reqJson, "name", "请求报文中未包含name");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "specialStartTime", "请求报文中未包含specialStartTime");
        Assert.hasKeyAndValue(reqJson, "specialReason", "请求报文中未包含specialReason");
        Assert.hasKeyAndValue(reqJson, "isWork", "请求报文中未包含isWork");

        GovSpecialFollowPo govSpecialFollowPo = BeanConvertUtil.covertBean(reqJson, GovSpecialFollowPo.class);
        JSONArray photos = reqJson.getJSONArray("photos");
        return saveGovSpecialFollowBMOImpl.savePhone(govSpecialFollowPo,photos);
    }
    /**
     * 微信修改消息模板
     * @serviceCode /govSpecialFollow/updateGovSpecialFollow
     * @path /app/govSpecialFollow/updateGovSpecialFollow
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/updateGovSpecialFollow", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovSpecialFollow(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "followId", "请求报文中未包含followId");
        Assert.hasKeyAndValue(reqJson, "specialPersonId", "请求报文中未包含specialPersonId");
        Assert.hasKeyAndValue(reqJson, "name", "请求报文中未包含name");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "specialStartTime", "请求报文中未包含specialStartTime");
        Assert.hasKeyAndValue(reqJson, "specialReason", "请求报文中未包含specialReason");
        Assert.hasKeyAndValue(reqJson, "isWork", "请求报文中未包含isWork");


        GovSpecialFollowPo govSpecialFollowPo = BeanConvertUtil.covertBean(reqJson, GovSpecialFollowPo.class);
        return updateGovSpecialFollowBMOImpl.update(govSpecialFollowPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govSpecialFollow/deleteGovSpecialFollow
     * @path /app/govSpecialFollow/deleteGovSpecialFollow
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/deleteGovSpecialFollow", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovSpecialFollow(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "caId", "caId不能为空");
        Assert.hasKeyAndValue(reqJson, "followId", "followId不能为空");
        GovSpecialFollowPo govSpecialFollowPo = BeanConvertUtil.covertBean(reqJson, GovSpecialFollowPo.class);
        return deleteGovSpecialFollowBMOImpl.delete(govSpecialFollowPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govSpecialFollow/queryGovSpecialFollow
     * @path /app/govSpecialFollow/queryGovSpecialFollow
     * @param caId 小区ID
     * @return
     */
    @RequestMapping(value = "/queryGovSpecialFollow", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovSpecialFollow(@RequestParam(value = "caId") String caId,
                                                        @RequestParam(value = "specialPersonId" , required = false) String specialPersonId,
                                                        @RequestParam(value = "followId" , required = false) String followId,
                                                      @RequestParam(value = "page") int page,
                                                      @RequestParam(value = "row") int row) {
        GovSpecialFollowDto govSpecialFollowDto = new GovSpecialFollowDto();
        govSpecialFollowDto.setPage(page);
        govSpecialFollowDto.setRow(row);
        govSpecialFollowDto.setCaId(caId);
        govSpecialFollowDto.setSpecialPersonId(specialPersonId);
        govSpecialFollowDto.setFollowId(followId);
        return getGovSpecialFollowBMOImpl.get(govSpecialFollowDto);
    }
}
