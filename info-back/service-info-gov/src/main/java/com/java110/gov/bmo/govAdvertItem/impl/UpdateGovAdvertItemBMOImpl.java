package com.java110.gov.bmo.govAdvertItem.impl;

import com.java110.core.annotation.Java110Transactional;
import com.java110.gov.bmo.govAdvertItem.IUpdateGovAdvertItemBMO;
import com.java110.intf.gov.IGovAdvertItemInnerServiceSMO;
import com.java110.po.govAdvertItem.GovAdvertItemPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("updateGovAdvertItemBMOImpl")
public class UpdateGovAdvertItemBMOImpl implements IUpdateGovAdvertItemBMO {

    @Autowired
    private IGovAdvertItemInnerServiceSMO govAdvertItemInnerServiceSMOImpl;

    /**
     *
     *
     * @param govAdvertItemPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> update(GovAdvertItemPo govAdvertItemPo) {

        int flag = govAdvertItemInnerServiceSMOImpl.updateGovAdvertItem(govAdvertItemPo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
