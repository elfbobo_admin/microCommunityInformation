package com.java110.gov.smo.impl;


import com.java110.gov.dao.IGovRenovationCheckServiceDao;
import com.java110.intf.gov.IGovRenovationCheckInnerServiceSMO;
import com.java110.dto.govRenovationCheck.GovRenovationCheckDto;
import com.java110.po.govRenovationCheck.GovRenovationCheckPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 重点地区整治情况内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovRenovationCheckInnerServiceSMOImpl extends BaseServiceSMO implements IGovRenovationCheckInnerServiceSMO {

    @Autowired
    private IGovRenovationCheckServiceDao govRenovationCheckServiceDaoImpl;


    @Override
    public int saveGovRenovationCheck(@RequestBody  GovRenovationCheckPo govRenovationCheckPo) {
        int saveFlag = 1;
        govRenovationCheckServiceDaoImpl.saveGovRenovationCheckInfo(BeanConvertUtil.beanCovertMap(govRenovationCheckPo));
        return saveFlag;
    }

     @Override
    public int updateGovRenovationCheck(@RequestBody  GovRenovationCheckPo govRenovationCheckPo) {
        int saveFlag = 1;
         govRenovationCheckServiceDaoImpl.updateGovRenovationCheckInfo(BeanConvertUtil.beanCovertMap(govRenovationCheckPo));
        return saveFlag;
    }

     @Override
    public int deleteGovRenovationCheck(@RequestBody  GovRenovationCheckPo govRenovationCheckPo) {
        int saveFlag = 1;
        govRenovationCheckPo.setStatusCd("1");
        govRenovationCheckServiceDaoImpl.updateGovRenovationCheckInfo(BeanConvertUtil.beanCovertMap(govRenovationCheckPo));
        return saveFlag;
    }

    @Override
    public List<GovRenovationCheckDto> queryGovRenovationChecks(@RequestBody  GovRenovationCheckDto govRenovationCheckDto) {

        //校验是否传了 分页信息

        int page = govRenovationCheckDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govRenovationCheckDto.setPage((page - 1) * govRenovationCheckDto.getRow());
        }

        List<GovRenovationCheckDto> govRenovationChecks = BeanConvertUtil.covertBeanList(govRenovationCheckServiceDaoImpl.getGovRenovationCheckInfo(BeanConvertUtil.beanCovertMap(govRenovationCheckDto)), GovRenovationCheckDto.class);

        return govRenovationChecks;
    }


    @Override
    public int queryGovRenovationChecksCount(@RequestBody GovRenovationCheckDto govRenovationCheckDto) {
        return govRenovationCheckServiceDaoImpl.queryGovRenovationChecksCount(BeanConvertUtil.beanCovertMap(govRenovationCheckDto));    }

    public IGovRenovationCheckServiceDao getGovRenovationCheckServiceDaoImpl() {
        return govRenovationCheckServiceDaoImpl;
    }

    public void setGovRenovationCheckServiceDaoImpl(IGovRenovationCheckServiceDao govRenovationCheckServiceDaoImpl) {
        this.govRenovationCheckServiceDaoImpl = govRenovationCheckServiceDaoImpl;
    }
}
