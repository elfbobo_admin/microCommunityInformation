package com.java110.gov.bmo.govRoadProtection.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.gov.bmo.govRoadProtection.IGetGovRoadProtectionBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.gov.IGovRoadProtectionInnerServiceSMO;
import com.java110.dto.govRoadProtection.GovRoadProtectionDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import org.springframework.http.HttpStatus;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service("getGovRoadProtectionBMOImpl")
public class GetGovRoadProtectionBMOImpl implements IGetGovRoadProtectionBMO {

    @Autowired
    private IGovRoadProtectionInnerServiceSMO govRoadProtectionInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govRoadProtectionDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovRoadProtectionDto govRoadProtectionDto) {


        int count = govRoadProtectionInnerServiceSMOImpl.queryGovRoadProtectionsCount(govRoadProtectionDto);

        List<GovRoadProtectionDto> govRoadProtectionDtos = null;
        if (count > 0) {
            govRoadProtectionDtos = govRoadProtectionInnerServiceSMOImpl.queryGovRoadProtections(govRoadProtectionDto);
        } else {
            govRoadProtectionDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govRoadProtectionDto.getRow()), count, govRoadProtectionDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
