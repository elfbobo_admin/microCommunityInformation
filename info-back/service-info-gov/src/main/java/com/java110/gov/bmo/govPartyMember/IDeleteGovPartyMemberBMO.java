package com.java110.gov.bmo.govPartyMember;
import com.java110.po.govPartyMember.GovPartyMemberPo;
import org.springframework.http.ResponseEntity;

public interface IDeleteGovPartyMemberBMO {


    /**
     * 修改党员管理
     * add by wuxw
     * @param govPartyMemberPo
     * @return
     */
    ResponseEntity<String> delete(GovPartyMemberPo govPartyMemberPo);


}
