package com.java110.gov.bmo.govReportUser.impl;

import com.java110.core.annotation.Java110Transactional;
import com.java110.gov.bmo.govReportUser.IDeleteGovReportUserBMO;
import com.java110.intf.gov.IGovReportUserInnerServiceSMO;
import com.java110.po.govReportUser.GovReportUserPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("deleteGovReportUserBMOImpl")
public class DeleteGovReportUserBMOImpl implements IDeleteGovReportUserBMO {

    @Autowired
    private IGovReportUserInnerServiceSMO govReportUserInnerServiceSMOImpl;

    /**
     * @param govReportUserPo 数据
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> delete(GovReportUserPo govReportUserPo) {

        int flag = govReportUserInnerServiceSMOImpl.deleteGovReportUser(govReportUserPo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
