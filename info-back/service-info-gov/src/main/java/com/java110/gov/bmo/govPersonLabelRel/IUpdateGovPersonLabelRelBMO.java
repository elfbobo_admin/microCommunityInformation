package com.java110.gov.bmo.govPersonLabelRel;
import org.springframework.http.ResponseEntity;
import com.java110.po.govPersonLabelRel.GovPersonLabelRelPo;

public interface IUpdateGovPersonLabelRelBMO {


    /**
     * 修改标签与人口关系
     * add by wuxw
     * @param govPersonLabelRelPo
     * @return
     */
    ResponseEntity<String> update(GovPersonLabelRelPo govPersonLabelRelPo,String labels);


}
