package com.java110.gov.bmo.govMeetingType;
import org.springframework.http.ResponseEntity;
import com.java110.dto.govMeetingType.GovMeetingTypeDto;
public interface IGetGovMeetingTypeBMO {


    /**
     * 查询会议类型
     * add by wuxw
     * @param  govMeetingTypeDto
     * @return
     */
    ResponseEntity<String> get(GovMeetingTypeDto govMeetingTypeDto);


}
