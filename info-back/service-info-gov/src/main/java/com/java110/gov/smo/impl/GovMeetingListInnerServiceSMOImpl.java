package com.java110.gov.smo.impl;


import com.java110.gov.dao.IGovMeetingListServiceDao;
import com.java110.intf.gov.IGovMeetingListInnerServiceSMO;
import com.java110.dto.govMeetingList.GovMeetingListDto;
import com.java110.po.govMeetingList.GovMeetingListPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 会议列表内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovMeetingListInnerServiceSMOImpl extends BaseServiceSMO implements IGovMeetingListInnerServiceSMO {

    @Autowired
    private IGovMeetingListServiceDao govMeetingListServiceDaoImpl;


    @Override
    public int saveGovMeetingList(@RequestBody  GovMeetingListPo govMeetingListPo) {
        int saveFlag = 1;
        govMeetingListServiceDaoImpl.saveGovMeetingListInfo(BeanConvertUtil.beanCovertMap(govMeetingListPo));
        return saveFlag;
    }

     @Override
    public int updateGovMeetingList(@RequestBody  GovMeetingListPo govMeetingListPo) {
        int saveFlag = 1;
         govMeetingListServiceDaoImpl.updateGovMeetingListInfo(BeanConvertUtil.beanCovertMap(govMeetingListPo));
        return saveFlag;
    }

     @Override
    public int deleteGovMeetingList(@RequestBody  GovMeetingListPo govMeetingListPo) {
        int saveFlag = 1;
        govMeetingListPo.setStatusCd("1");
        govMeetingListServiceDaoImpl.updateGovMeetingListInfo(BeanConvertUtil.beanCovertMap(govMeetingListPo));
        return saveFlag;
    }

    @Override
    public List<GovMeetingListDto> queryGovMeetingLists(@RequestBody  GovMeetingListDto govMeetingListDto) {

        //校验是否传了 分页信息

        int page = govMeetingListDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govMeetingListDto.setPage((page - 1) * govMeetingListDto.getRow());
        }

        List<GovMeetingListDto> govMeetingLists = BeanConvertUtil.covertBeanList(govMeetingListServiceDaoImpl.getGovMeetingListInfo(BeanConvertUtil.beanCovertMap(govMeetingListDto)), GovMeetingListDto.class);

        return govMeetingLists;
    }


    @Override
    public int queryGovMeetingListsCount(@RequestBody GovMeetingListDto govMeetingListDto) {
        return govMeetingListServiceDaoImpl.queryGovMeetingListsCount(BeanConvertUtil.beanCovertMap(govMeetingListDto));    }

    public IGovMeetingListServiceDao getGovMeetingListServiceDaoImpl() {
        return govMeetingListServiceDaoImpl;
    }

    public void setGovMeetingListServiceDaoImpl(IGovMeetingListServiceDao govMeetingListServiceDaoImpl) {
        this.govMeetingListServiceDaoImpl = govMeetingListServiceDaoImpl;
    }
}
