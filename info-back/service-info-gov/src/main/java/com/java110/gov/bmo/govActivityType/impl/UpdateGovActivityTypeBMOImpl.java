package com.java110.gov.bmo.govActivityType.impl;

import com.java110.core.annotation.Java110Transactional;
import com.java110.gov.bmo.govActivityType.IUpdateGovActivityTypeBMO;
import com.java110.intf.gov.IGovActivityTypeInnerServiceSMO;
import com.java110.po.govActivityType.GovActivityTypePo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("updateGovActivityTypeBMOImpl")
public class UpdateGovActivityTypeBMOImpl implements IUpdateGovActivityTypeBMO {

    @Autowired
    private IGovActivityTypeInnerServiceSMO govActivityTypeInnerServiceSMOImpl;

    /**
     *
     *
     * @param govActivityTypePo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> update(GovActivityTypePo govActivityTypePo) {

        int flag = govActivityTypeInnerServiceSMOImpl.updateGovActivityType(govActivityTypePo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
