package com.java110.gov.bmo.govAreaType;
import org.springframework.http.ResponseEntity;
import com.java110.dto.govAreaType.GovAreaTypeDto;
public interface IGetGovAreaTypeBMO {


    /**
     * 查询涉及区域类型
     * add by wuxw
     * @param  govAreaTypeDto
     * @return
     */
    ResponseEntity<String> get(GovAreaTypeDto govAreaTypeDto);


}
