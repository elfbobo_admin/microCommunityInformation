package com.java110.gov.bmo.govActivityPerson.impl;

import com.java110.dto.govActivityPerson.GovActivityPersonDto;
import com.java110.gov.bmo.govActivityPerson.IGetGovActivityPersonBMO;
import com.java110.intf.gov.IGovActivityPersonInnerServiceSMO;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("getGovActivityPersonBMOImpl")
public class GetGovActivityPersonBMOImpl implements IGetGovActivityPersonBMO {

    @Autowired
    private IGovActivityPersonInnerServiceSMO govActivityPersonInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govActivityPersonDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovActivityPersonDto govActivityPersonDto) {


        int count = govActivityPersonInnerServiceSMOImpl.queryGovActivityPersonsCount(govActivityPersonDto);

        List<GovActivityPersonDto> govActivityPersonDtos = null;
        if (count > 0) {
            govActivityPersonDtos = govActivityPersonInnerServiceSMOImpl.queryGovActivityPersons(govActivityPersonDto);
        } else {
            govActivityPersonDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govActivityPersonDto.getRow()), count, govActivityPersonDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
