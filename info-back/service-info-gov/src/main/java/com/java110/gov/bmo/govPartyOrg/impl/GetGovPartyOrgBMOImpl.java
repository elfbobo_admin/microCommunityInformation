package com.java110.gov.bmo.govPartyOrg.impl;


import com.java110.dto.govPartyOrgContext.GovPartyOrgContextDto;
import com.java110.gov.bmo.govPartyOrg.IGetGovPartyOrgBMO;
import com.java110.intf.gov.IGovPartyOrgContextInnerServiceSMO;
import com.java110.intf.gov.IGovPartyOrgInnerServiceSMO;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.dto.govPartyOrg.GovPartyOrgDto;


import java.util.ArrayList;
import java.util.List;

@Service("getGovPartyOrgBMOImpl")
public class GetGovPartyOrgBMOImpl implements IGetGovPartyOrgBMO {

    @Autowired
    private IGovPartyOrgInnerServiceSMO govPartyOrgInnerServiceSMOImpl;
    @Autowired
    private IGovPartyOrgContextInnerServiceSMO govPartyOrgContextInnerServiceSMOImpl;
    /**
     *
     *
     * @param  govPartyOrgDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovPartyOrgDto govPartyOrgDto) {


        int count = govPartyOrgInnerServiceSMOImpl.queryGovPartyOrgsCount(govPartyOrgDto);

        List<GovPartyOrgDto> govPartyOrgDtos = null;
        if (count > 0) {
            govPartyOrgDtos = govPartyOrgInnerServiceSMOImpl.queryGovPartyOrgs(govPartyOrgDto);
            for(GovPartyOrgDto govPartyOrg : govPartyOrgDtos){
                GovPartyOrgContextDto govPartyOrgContextDto = new GovPartyOrgContextDto();
                govPartyOrgContextDto.setOrgId( govPartyOrg.getOrgId() );
                List<GovPartyOrgContextDto> govPartyOrgContextDtoList=  govPartyOrgContextInnerServiceSMOImpl.queryGovPartyOrgContexts( govPartyOrgContextDto );
                if(null != govPartyOrgContextDtoList && govPartyOrgContextDtoList.size() > 0) {
                    govPartyOrg.setContext( govPartyOrgContextDtoList.get( 0 ).getContext() );
                }
            }
        } else {
            govPartyOrgDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govPartyOrgDto.getRow()), count, govPartyOrgDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

    public ResponseEntity<String> queryNotGovPartyOrg(GovPartyOrgDto govPartyOrgDto) {


        List<GovPartyOrgDto> govPartyOrgDtos = govPartyOrgInnerServiceSMOImpl.queryNotGovPartyOrg(govPartyOrgDto);



        ResultVo resultVo = new ResultVo(govPartyOrgDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
