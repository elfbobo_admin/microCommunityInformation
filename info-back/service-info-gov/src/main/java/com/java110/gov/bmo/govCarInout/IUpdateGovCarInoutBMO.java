package com.java110.gov.bmo.govCarInout;
import com.java110.po.govCarInout.GovCarInoutPo;
import org.springframework.http.ResponseEntity;

public interface IUpdateGovCarInoutBMO {


    /**
     * 修改车辆进出记录
     * add by wuxw
     * @param govCarInoutPo
     * @return
     */
    ResponseEntity<String> update(GovCarInoutPo govCarInoutPo);


}
