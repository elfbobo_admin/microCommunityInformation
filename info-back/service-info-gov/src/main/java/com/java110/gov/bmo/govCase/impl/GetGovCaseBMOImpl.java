package com.java110.gov.bmo.govCase.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.gov.bmo.govCase.IGetGovCaseBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.gov.IGovCaseInnerServiceSMO;
import com.java110.dto.govCase.GovCaseDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import org.springframework.http.HttpStatus;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service("getGovCaseBMOImpl")
public class GetGovCaseBMOImpl implements IGetGovCaseBMO {

    @Autowired
    private IGovCaseInnerServiceSMO govCaseInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govCaseDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovCaseDto govCaseDto) {


        int count = govCaseInnerServiceSMOImpl.queryGovCasesCount(govCaseDto);

        List<GovCaseDto> govCaseDtos = null;
        if (count > 0) {
            govCaseDtos = govCaseInnerServiceSMOImpl.queryGovCases(govCaseDto);
        } else {
            govCaseDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govCaseDto.getRow()), count, govCaseDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
