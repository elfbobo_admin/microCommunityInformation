package com.java110.gov.bmo.govActivityType;
import com.java110.po.govActivityType.GovActivityTypePo;
import org.springframework.http.ResponseEntity;

public interface IUpdateGovActivityTypeBMO {


    /**
     * 修改活动类型
     * add by wuxw
     * @param govActivityTypePo
     * @return
     */
    ResponseEntity<String> update(GovActivityTypePo govActivityTypePo);


}
