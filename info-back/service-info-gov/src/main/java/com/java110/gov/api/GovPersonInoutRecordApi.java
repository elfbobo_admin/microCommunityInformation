package com.java110.gov.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govPersonInoutRecord.GovPersonInoutRecordDto;
import com.java110.gov.bmo.govPersonInoutRecord.IDeleteGovPersonInoutRecordBMO;
import com.java110.gov.bmo.govPersonInoutRecord.IGetGovPersonInoutRecordBMO;
import com.java110.gov.bmo.govPersonInoutRecord.ISaveGovPersonInoutRecordBMO;
import com.java110.gov.bmo.govPersonInoutRecord.IUpdateGovPersonInoutRecordBMO;
import com.java110.po.govPersonInoutRecord.GovPersonInoutRecordPo;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govPersonInoutRecord")
public class GovPersonInoutRecordApi {

    @Autowired
    private ISaveGovPersonInoutRecordBMO saveGovPersonInoutRecordBMOImpl;
    @Autowired
    private IUpdateGovPersonInoutRecordBMO updateGovPersonInoutRecordBMOImpl;
    @Autowired
    private IDeleteGovPersonInoutRecordBMO deleteGovPersonInoutRecordBMOImpl;

    @Autowired
    private IGetGovPersonInoutRecordBMO getGovPersonInoutRecordBMOImpl;

    /**
     * 微信保存消息模板
     * @serviceCode /govPersonInoutRecord/saveGovPersonInoutRecord
     * @path /app/govPersonInoutRecord/saveGovPersonInoutRecord
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/saveGovPersonInoutRecord", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovPersonInoutRecord(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "recordId", "请求报文中未包含recordId");


        GovPersonInoutRecordPo govPersonInoutRecordPo = BeanConvertUtil.covertBean(reqJson, GovPersonInoutRecordPo.class);
        return saveGovPersonInoutRecordBMOImpl.save(govPersonInoutRecordPo);
    }

    /**
     * 微信修改消息模板
     * @serviceCode /govPersonInoutRecord/updateGovPersonInoutRecord
     * @path /app/govPersonInoutRecord/updateGovPersonInoutRecord
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/updateGovPersonInoutRecord", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovPersonInoutRecord(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "recordId", "请求报文中未包含recordId");
        Assert.hasKeyAndValue(reqJson, "recordId", "recordId不能为空");


        GovPersonInoutRecordPo govPersonInoutRecordPo = BeanConvertUtil.covertBean(reqJson, GovPersonInoutRecordPo.class);
        return updateGovPersonInoutRecordBMOImpl.update(govPersonInoutRecordPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govPersonInoutRecord/deleteGovPersonInoutRecord
     * @path /app/govPersonInoutRecord/deleteGovPersonInoutRecord
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/deleteGovPersonInoutRecord", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovPersonInoutRecord(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "communityId", "小区ID不能为空");

        Assert.hasKeyAndValue(reqJson, "recordId", "recordId不能为空");


        GovPersonInoutRecordPo govPersonInoutRecordPo = BeanConvertUtil.covertBean(reqJson, GovPersonInoutRecordPo.class);
        return deleteGovPersonInoutRecordBMOImpl.delete(govPersonInoutRecordPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govPersonInoutRecord/queryGovPersonInoutRecord
     * @path /app/govPersonInoutRecord/queryGovPersonInoutRecord
     * @param caId 小区ID
     * @return
     */
    @RequestMapping(value = "/queryGovPersonInoutRecord", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovPersonInoutRecord(@RequestParam(value = "caId") String caId,
                                                            @RequestParam(value = "govCommunityId" , required = false) String govCommunityId,
                                                            @RequestParam(value = "name" , required = false) String name,
                                                            @RequestParam(value = "openTypeCd" , required = false) String openTypeCd,
                                                            @RequestParam(value = "tel" , required = false) String tel,
                                                            @RequestParam(value = "idCard" , required = false) String idCard,
                                                            @RequestParam(value = "recordTypeCd" , required = false) String recordTypeCd,
                                                            @RequestParam(value = "page") int page,
                                                            @RequestParam(value = "row") int row) {
        GovPersonInoutRecordDto govPersonInoutRecordDto = new GovPersonInoutRecordDto();
        govPersonInoutRecordDto.setPage(page);
        govPersonInoutRecordDto.setRow(row);
        govPersonInoutRecordDto.setGovCommunityId(govCommunityId);
        govPersonInoutRecordDto.setCaId(caId);
        govPersonInoutRecordDto.setName(name);
        govPersonInoutRecordDto.setOpenTypeCd(openTypeCd);
        govPersonInoutRecordDto.setTel(tel);
        govPersonInoutRecordDto.setIdCard(idCard);
        govPersonInoutRecordDto.setRecordTypeCd(recordTypeCd);
        return getGovPersonInoutRecordBMOImpl.get(govPersonInoutRecordDto);
    }
}
