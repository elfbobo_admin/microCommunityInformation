package com.java110.gov.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govGuideSubscribe.GovGuideSubscribeDto;
import com.java110.gov.bmo.govGuideSubscribe.IDeleteGovGuideSubscribeBMO;
import com.java110.gov.bmo.govGuideSubscribe.IGetGovGuideSubscribeBMO;
import com.java110.gov.bmo.govGuideSubscribe.ISaveGovGuideSubscribeBMO;
import com.java110.gov.bmo.govGuideSubscribe.IUpdateGovGuideSubscribeBMO;
import com.java110.po.govGuideSubscribe.GovGuideSubscribePo;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govGuideSubscribe")
public class GovGuideSubscribeApi {

    @Autowired
    private ISaveGovGuideSubscribeBMO saveGovGuideSubscribeBMOImpl;
    @Autowired
    private IUpdateGovGuideSubscribeBMO updateGovGuideSubscribeBMOImpl;
    @Autowired
    private IDeleteGovGuideSubscribeBMO deleteGovGuideSubscribeBMOImpl;

    @Autowired
    private IGetGovGuideSubscribeBMO getGovGuideSubscribeBMOImpl;

    /**
     * 微信保存消息模板
     * @serviceCode /govGuideSubscribe/saveGovGuideSubscribe
     * @path /app/govGuideSubscribe/saveGovGuideSubscribe
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/saveGovGuideSubscribe", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovGuideSubscribe(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "wgId", "请求报文中未包含wgId");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "person", "请求报文中未包含person");
        Assert.hasKeyAndValue(reqJson, "link", "请求报文中未包含link");
        Assert.hasKeyAndValue(reqJson, "startTime", "请求报文中未包含startTime");
        Assert.hasKeyAndValue(reqJson, "endTime", "请求报文中未包含endTime");
        Assert.hasKeyAndValue(reqJson, "state", "请求报文中未包含state");


        GovGuideSubscribePo govGuideSubscribePo = BeanConvertUtil.covertBean(reqJson, GovGuideSubscribePo.class);
        return saveGovGuideSubscribeBMOImpl.save(govGuideSubscribePo);
    }

    /**
     * 微信修改消息模板
     * @serviceCode /govGuideSubscribe/updateGovGuideSubscribe
     * @path /app/govGuideSubscribe/updateGovGuideSubscribe
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/updateGovGuideSubscribe", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovGuideSubscribe(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "wgId", "请求报文中未包含wgId");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "person", "请求报文中未包含person");
        Assert.hasKeyAndValue(reqJson, "link", "请求报文中未包含link");
        Assert.hasKeyAndValue(reqJson, "startTime", "请求报文中未包含startTime");
        Assert.hasKeyAndValue(reqJson, "endTime", "请求报文中未包含endTime");
        Assert.hasKeyAndValue(reqJson, "state", "请求报文中未包含state");
        Assert.hasKeyAndValue(reqJson, "wgsId", "wgsId不能为空");


        GovGuideSubscribePo govGuideSubscribePo = BeanConvertUtil.covertBean(reqJson, GovGuideSubscribePo.class);
        return updateGovGuideSubscribeBMOImpl.update(govGuideSubscribePo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govGuideSubscribe/deleteGovGuideSubscribe
     * @path /app/govGuideSubscribe/deleteGovGuideSubscribe
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/deleteGovGuideSubscribe", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovGuideSubscribe(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "wgsId", "wgsId不能为空");
        GovGuideSubscribePo govGuideSubscribePo = BeanConvertUtil.covertBean(reqJson, GovGuideSubscribePo.class);
        return deleteGovGuideSubscribeBMOImpl.delete(govGuideSubscribePo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govGuideSubscribe/queryGovGuideSubscribe
     * @path /app/govGuideSubscribe/queryGovGuideSubscribe
     * @param caId 小区ID
     * @return
     */
    @RequestMapping(value = "/queryGovGuideSubscribe", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovGuideSubscribe(@RequestParam(value = "caId") String caId,
                                                         @RequestParam(value = "wgsId" , required = false) String wgsId,
                                                         @RequestParam(value = "guideName" , required = false) String guideName,
                                                         @RequestParam(value = "person" , required = false) String person,
                                                         @RequestParam(value = "link" , required = false) String link,
                                                         @RequestParam(value = "startTime" , required = false) String startTime,
                                                      @RequestParam(value = "endTime" , required = false) String endTime,
                                                      @RequestParam(value = "page") int page,
                                                      @RequestParam(value = "row") int row) {
        GovGuideSubscribeDto govGuideSubscribeDto = new GovGuideSubscribeDto();
        govGuideSubscribeDto.setPage(page);
        govGuideSubscribeDto.setRow(row);
        govGuideSubscribeDto.setGuideName(guideName);
        govGuideSubscribeDto.setWgsId(wgsId);
        govGuideSubscribeDto.setPerson(person);
        govGuideSubscribeDto.setLink(link);
        govGuideSubscribeDto.setStartTime(startTime);
        govGuideSubscribeDto.setEndTime(endTime);
        return getGovGuideSubscribeBMOImpl.get(govGuideSubscribeDto);
    }
}
