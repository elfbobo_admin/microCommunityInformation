package com.java110.gov.dao;


import com.java110.utils.exception.DAOException;


import java.util.List;
import java.util.Map;

/**
 * 民政服务宣传组件内部之间使用，没有给外围系统提供服务能力
 * 民政服务宣传服务接口类，要求全部以字符串传输，方便微服务化
 * 新建客户，修改客户，删除客户，查询客户等功能
 *
 * Created by wuxw on 2016/12/27.
 */
public interface IGovCiviladminServiceDao {


    /**
     * 保存 民政服务宣传信息
     * @param info
     * @throws DAOException DAO异常
     */
    void saveGovCiviladminInfo(Map info) throws DAOException;




    /**
     * 查询民政服务宣传信息（instance过程）
     * 根据bId 查询民政服务宣传信息
     * @param info bId 信息
     * @return 民政服务宣传信息
     * @throws DAOException DAO异常
     */
    List<Map> getGovCiviladminInfo(Map info) throws DAOException;



    /**
     * 修改民政服务宣传信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    void updateGovCiviladminInfo(Map info) throws DAOException;


    /**
     * 查询民政服务宣传总数
     *
     * @param info 民政服务宣传信息
     * @return 民政服务宣传数量
     */
    int queryGovCiviladminsCount(Map info);

}
