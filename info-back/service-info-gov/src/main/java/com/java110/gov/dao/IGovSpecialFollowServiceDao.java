package com.java110.gov.dao;


import com.java110.utils.exception.DAOException;


import java.util.List;
import java.util.Map;

/**
 * 特殊人员跟进记录组件内部之间使用，没有给外围系统提供服务能力
 * 特殊人员跟进记录服务接口类，要求全部以字符串传输，方便微服务化
 * 新建客户，修改客户，删除客户，查询客户等功能
 *
 * Created by wuxw on 2016/12/27.
 */
public interface IGovSpecialFollowServiceDao {


    /**
     * 保存 特殊人员跟进记录信息
     * @param info
     * @throws DAOException DAO异常
     */
    void saveGovSpecialFollowInfo(Map info) throws DAOException;




    /**
     * 查询特殊人员跟进记录信息（instance过程）
     * 根据bId 查询特殊人员跟进记录信息
     * @param info bId 信息
     * @return 特殊人员跟进记录信息
     * @throws DAOException DAO异常
     */
    List<Map> getGovSpecialFollowInfo(Map info) throws DAOException;



    /**
     * 修改特殊人员跟进记录信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    void updateGovSpecialFollowInfo(Map info) throws DAOException;


    /**
     * 查询特殊人员跟进记录总数
     *
     * @param info 特殊人员跟进记录信息
     * @return 特殊人员跟进记录数量
     */
    int queryGovSpecialFollowsCount(Map info);

}
