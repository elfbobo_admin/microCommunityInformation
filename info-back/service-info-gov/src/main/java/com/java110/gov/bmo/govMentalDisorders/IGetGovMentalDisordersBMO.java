package com.java110.gov.bmo.govMentalDisorders;
import org.springframework.http.ResponseEntity;
import com.java110.dto.govMentalDisorders.GovMentalDisordersDto;
public interface IGetGovMentalDisordersBMO {


    /**
     * 查询障碍者
     * add by wuxw
     * @param  govMentalDisordersDto
     * @return
     */
    ResponseEntity<String> get(GovMentalDisordersDto govMentalDisordersDto);


}
