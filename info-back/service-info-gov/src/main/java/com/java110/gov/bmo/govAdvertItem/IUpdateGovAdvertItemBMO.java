package com.java110.gov.bmo.govAdvertItem;
import com.java110.po.govAdvertItem.GovAdvertItemPo;
import org.springframework.http.ResponseEntity;

public interface IUpdateGovAdvertItemBMO {


    /**
     * 修改广告明细
     * add by wuxw
     * @param govAdvertItemPo
     * @return
     */
    ResponseEntity<String> update(GovAdvertItemPo govAdvertItemPo);


}
