package com.java110.gov.smo.impl;


import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.PageDto;
import com.java110.dto.govPersonInoutRecord.GovPersonInoutRecordDto;
import com.java110.gov.dao.IGovPersonInoutRecordServiceDao;
import com.java110.intf.gov.IGovPersonInoutRecordInnerServiceSMO;
import com.java110.po.govPersonInoutRecord.GovPersonInoutRecordPo;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 开门记录内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovPersonInoutRecordInnerServiceSMOImpl extends BaseServiceSMO implements IGovPersonInoutRecordInnerServiceSMO {

    @Autowired
    private IGovPersonInoutRecordServiceDao govPersonInoutRecordServiceDaoImpl;


    @Override
    public int saveGovPersonInoutRecord(@RequestBody GovPersonInoutRecordPo govPersonInoutRecordPo) {
        int saveFlag = 1;
        govPersonInoutRecordServiceDaoImpl.saveGovPersonInoutRecordInfo(BeanConvertUtil.beanCovertMap(govPersonInoutRecordPo));
        return saveFlag;
    }

     @Override
    public int updateGovPersonInoutRecord(@RequestBody  GovPersonInoutRecordPo govPersonInoutRecordPo) {
        int saveFlag = 1;
         govPersonInoutRecordServiceDaoImpl.updateGovPersonInoutRecordInfo(BeanConvertUtil.beanCovertMap(govPersonInoutRecordPo));
        return saveFlag;
    }

     @Override
    public int deleteGovPersonInoutRecord(@RequestBody  GovPersonInoutRecordPo govPersonInoutRecordPo) {
        int saveFlag = 1;
        govPersonInoutRecordPo.setStatusCd("1");
        govPersonInoutRecordServiceDaoImpl.updateGovPersonInoutRecordInfo(BeanConvertUtil.beanCovertMap(govPersonInoutRecordPo));
        return saveFlag;
    }

    @Override
    public List<GovPersonInoutRecordDto> queryGovPersonInoutRecords(@RequestBody  GovPersonInoutRecordDto govPersonInoutRecordDto) {

        //校验是否传了 分页信息

        int page = govPersonInoutRecordDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govPersonInoutRecordDto.setPage((page - 1) * govPersonInoutRecordDto.getRow());
        }

        List<GovPersonInoutRecordDto> govPersonInoutRecords = BeanConvertUtil.covertBeanList(govPersonInoutRecordServiceDaoImpl.getGovPersonInoutRecordInfo(BeanConvertUtil.beanCovertMap(govPersonInoutRecordDto)), GovPersonInoutRecordDto.class);

        return govPersonInoutRecords;
    }


    @Override
    public int queryGovPersonInoutRecordsCount(@RequestBody GovPersonInoutRecordDto govPersonInoutRecordDto) {
        return govPersonInoutRecordServiceDaoImpl.queryGovPersonInoutRecordsCount(BeanConvertUtil.beanCovertMap(govPersonInoutRecordDto));    }

    public IGovPersonInoutRecordServiceDao getGovPersonInoutRecordServiceDaoImpl() {
        return govPersonInoutRecordServiceDaoImpl;
    }

    public void setGovPersonInoutRecordServiceDaoImpl(IGovPersonInoutRecordServiceDao govPersonInoutRecordServiceDaoImpl) {
        this.govPersonInoutRecordServiceDaoImpl = govPersonInoutRecordServiceDaoImpl;
    }
}
