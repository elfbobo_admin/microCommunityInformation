package com.java110.gov.bmo.govPersonLabelRel.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.gov.bmo.govPersonLabelRel.IUpdateGovPersonLabelRelBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.gov.IGovPersonLabelRelInnerServiceSMO;
import com.java110.dto.govPersonLabelRel.GovPersonLabelRelDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.java110.po.govPersonLabelRel.GovPersonLabelRelPo;
import java.util.List;

@Service("updateGovPersonLabelRelBMOImpl")
public class UpdateGovPersonLabelRelBMOImpl implements IUpdateGovPersonLabelRelBMO {

    @Autowired
    private IGovPersonLabelRelInnerServiceSMO govPersonLabelRelInnerServiceSMOImpl;

    /**
     *
     *
     * @param govPersonLabelRelPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> update(GovPersonLabelRelPo govPersonLabelRelPo,String labels) {
        GovPersonLabelRelPo labelRelPo = new GovPersonLabelRelPo();
        labelRelPo.setGovPersonId(govPersonLabelRelPo.getGovPersonId());
        govPersonLabelRelInnerServiceSMOImpl.deleteGovPersonLabelRel(labelRelPo);
        int flag = 0;
        String[] labelArray = labels.split(",");
        for(String labelCd:labelArray){
            govPersonLabelRelPo.setLabelRelId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_labelRelId));
            govPersonLabelRelPo.setLabelCd(labelCd);
            flag = govPersonLabelRelInnerServiceSMOImpl.saveGovPersonLabelRel(govPersonLabelRelPo);
        }
        if (flag > 0) {
            return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }
            return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
