package com.java110.gov.bmo.govMeetingMemberRel;

import org.springframework.http.ResponseEntity;
import com.java110.po.govMeetingMemberRel.GovMeetingMemberRelPo;
public interface ISaveGovMeetingMemberRelBMO {


    /**
     * 添加会议与参会人关系
     * add by wuxw
     * @param govMeetingMemberRelPo
     * @return
     */
    ResponseEntity<String> save(GovMeetingMemberRelPo govMeetingMemberRelPo);


}
