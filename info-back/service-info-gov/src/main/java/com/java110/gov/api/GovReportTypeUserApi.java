package com.java110.gov.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govReportTypeUser.GovReportTypeUserDto;
import com.java110.gov.bmo.govReportTypeUser.IDeleteGovReportTypeUserBMO;
import com.java110.gov.bmo.govReportTypeUser.IGetGovReportTypeUserBMO;
import com.java110.gov.bmo.govReportTypeUser.ISaveGovReportTypeUserBMO;
import com.java110.gov.bmo.govReportTypeUser.IUpdateGovReportTypeUserBMO;
import com.java110.po.govReportTypeUser.GovReportTypeUserPo;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govReportTypeUser")
public class GovReportTypeUserApi {

    @Autowired
    private ISaveGovReportTypeUserBMO saveGovReportTypeUserBMOImpl;
    @Autowired
    private IUpdateGovReportTypeUserBMO updateGovReportTypeUserBMOImpl;
    @Autowired
    private IDeleteGovReportTypeUserBMO deleteGovReportTypeUserBMOImpl;

    @Autowired
    private IGetGovReportTypeUserBMO getGovReportTypeUserBMOImpl;

    /**
     * 微信保存消息模板
     * @serviceCode /govReportTypeUser/saveGovReportTypeUser
     * @path /app/govReportTypeUser/saveGovReportTypeUser
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/saveGovReportTypeUser", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovReportTypeUser(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "reportType", "请求报文中未包含reportType");
        Assert.hasKeyAndValue(reqJson, "staffId", "请求报文中未包含staffId");
        Assert.hasKeyAndValue(reqJson, "staffName", "请求报文中未包含staffName");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "state", "请求报文中未包含state");


        GovReportTypeUserPo govReportTypeUserPo = BeanConvertUtil.covertBean(reqJson, GovReportTypeUserPo.class);
        return saveGovReportTypeUserBMOImpl.save(govReportTypeUserPo);
    }

    /**
     * 微信修改消息模板
     * @serviceCode /govReportTypeUser/updateGovReportTypeUser
     * @path /app/govReportTypeUser/updateGovReportTypeUser
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/updateGovReportTypeUser", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovReportTypeUser(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "typeUserId", "请求报文中未包含typeUserId");
        Assert.hasKeyAndValue(reqJson, "reportType", "请求报文中未包含reportType");
        Assert.hasKeyAndValue(reqJson, "staffId", "请求报文中未包含staffId");
        Assert.hasKeyAndValue(reqJson, "staffName", "请求报文中未包含staffName");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "state", "请求报文中未包含state");
        Assert.hasKeyAndValue(reqJson, "typeUserId", "typeUserId不能为空");


        GovReportTypeUserPo govReportTypeUserPo = BeanConvertUtil.covertBean(reqJson, GovReportTypeUserPo.class);
        return updateGovReportTypeUserBMOImpl.update(govReportTypeUserPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govReportTypeUser/deleteGovReportTypeUser
     * @path /app/govReportTypeUser/deleteGovReportTypeUser
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/deleteGovReportTypeUser", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovReportTypeUser(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "typeUserId", "typeUserId不能为空");


        GovReportTypeUserPo govReportTypeUserPo = BeanConvertUtil.covertBean(reqJson, GovReportTypeUserPo.class);
        return deleteGovReportTypeUserBMOImpl.delete(govReportTypeUserPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govReportTypeUser/queryGovReportTypeUser
     * @path /app/govReportTypeUser/queryGovReportTypeUser
     * @param
     * @return
     */
    @RequestMapping(value = "/queryGovReportTypeUser", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovReportTypeUser(@RequestParam(value = "caId") String caId,
                                                         @RequestParam(value = "staffName",required = false) String staffName,
                                                         @RequestParam(value = "state",required = false) String state,
                                                         @RequestParam(value = "reportType",required = false) String reportType,
                                                      @RequestParam(value = "page") int page,
                                                      @RequestParam(value = "row") int row) {
        GovReportTypeUserDto govReportTypeUserDto = new GovReportTypeUserDto();
        govReportTypeUserDto.setPage(page);
        govReportTypeUserDto.setRow(row);
        govReportTypeUserDto.setCaId(caId);
        govReportTypeUserDto.setStaffName(staffName);
        govReportTypeUserDto.setState(state);
        govReportTypeUserDto.setReportType(reportType);
        return getGovReportTypeUserBMOImpl.get(govReportTypeUserDto);
    }
}
