package com.java110.gov.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govMeetingType.GovMeetingTypeDto;
import com.java110.po.govMeetingType.GovMeetingTypePo;
import com.java110.gov.bmo.govMeetingType.IDeleteGovMeetingTypeBMO;
import com.java110.gov.bmo.govMeetingType.IGetGovMeetingTypeBMO;
import com.java110.gov.bmo.govMeetingType.ISaveGovMeetingTypeBMO;
import com.java110.gov.bmo.govMeetingType.IUpdateGovMeetingTypeBMO;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govMeetingType")
public class GovMeetingTypeApi {

    @Autowired
    private ISaveGovMeetingTypeBMO saveGovMeetingTypeBMOImpl;
    @Autowired
    private IUpdateGovMeetingTypeBMO updateGovMeetingTypeBMOImpl;
    @Autowired
    private IDeleteGovMeetingTypeBMO deleteGovMeetingTypeBMOImpl;

    @Autowired
    private IGetGovMeetingTypeBMO getGovMeetingTypeBMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govMeetingType/saveGovMeetingType
     * @path /app/govMeetingType/saveGovMeetingType
     */
    @RequestMapping(value = "/saveGovMeetingType", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovMeetingType(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "meetingTypeName", "请求报文中未包含meetingTypeName");


        GovMeetingTypePo govMeetingTypePo = BeanConvertUtil.covertBean(reqJson, GovMeetingTypePo.class);
        return saveGovMeetingTypeBMOImpl.save(govMeetingTypePo);
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govMeetingType/updateGovMeetingType
     * @path /app/govMeetingType/updateGovMeetingType
     */
    @RequestMapping(value = "/updateGovMeetingType", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovMeetingType(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "typeId", "请求报文中未包含typeId");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "meetingTypeName", "请求报文中未包含meetingTypeName");


        GovMeetingTypePo govMeetingTypePo = BeanConvertUtil.covertBean(reqJson, GovMeetingTypePo.class);
        return updateGovMeetingTypeBMOImpl.update(govMeetingTypePo);
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govMeetingType/deleteGovMeetingType
     * @path /app/govMeetingType/deleteGovMeetingType
     */
    @RequestMapping(value = "/deleteGovMeetingType", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovMeetingType(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "caId", "caId不能为空");

        Assert.hasKeyAndValue(reqJson, "typeId", "typeId不能为空");


        GovMeetingTypePo govMeetingTypePo = BeanConvertUtil.covertBean(reqJson, GovMeetingTypePo.class);
        return deleteGovMeetingTypeBMOImpl.delete(govMeetingTypePo);
    }

    /**
     * 微信删除消息模板
     *
     * @param caId 小区ID
     * @return
     * @serviceCode /govMeetingType/queryGovMeetingType
     * @path /app/govMeetingType/queryGovMeetingType
     */
    @RequestMapping(value = "/queryGovMeetingType", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovMeetingType(@RequestParam(value = "caId") String caId,
                                                      @RequestParam(value = "page") int page,
                                                      @RequestParam(value = "row") int row) {
        GovMeetingTypeDto govMeetingTypeDto = new GovMeetingTypeDto();
        govMeetingTypeDto.setPage(page);
        govMeetingTypeDto.setRow(row);
        govMeetingTypeDto.setCaId(caId);
        return getGovMeetingTypeBMOImpl.get(govMeetingTypeDto);
    }
}
