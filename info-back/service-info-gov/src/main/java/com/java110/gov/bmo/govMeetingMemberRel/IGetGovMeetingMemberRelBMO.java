package com.java110.gov.bmo.govMeetingMemberRel;
import org.springframework.http.ResponseEntity;
import com.java110.dto.govMeetingMemberRel.GovMeetingMemberRelDto;
public interface IGetGovMeetingMemberRelBMO {


    /**
     * 查询会议与参会人关系
     * add by wuxw
     * @param  govMeetingMemberRelDto
     * @return
     */
    ResponseEntity<String> get(GovMeetingMemberRelDto govMeetingMemberRelDto);


}
