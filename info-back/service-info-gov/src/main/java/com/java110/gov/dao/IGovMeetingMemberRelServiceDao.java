package com.java110.gov.dao;


import com.java110.utils.exception.DAOException;


import java.util.List;
import java.util.Map;

/**
 * 会议与参会人关系组件内部之间使用，没有给外围系统提供服务能力
 * 会议与参会人关系服务接口类，要求全部以字符串传输，方便微服务化
 * 新建客户，修改客户，删除客户，查询客户等功能
 *
 * Created by wuxw on 2016/12/27.
 */
public interface IGovMeetingMemberRelServiceDao {


    /**
     * 保存 会议与参会人关系信息
     * @param info
     * @throws DAOException DAO异常
     */
    void saveGovMeetingMemberRelInfo(Map info) throws DAOException;




    /**
     * 查询会议与参会人关系信息（instance过程）
     * 根据bId 查询会议与参会人关系信息
     * @param info bId 信息
     * @return 会议与参会人关系信息
     * @throws DAOException DAO异常
     */
    List<Map> getGovMeetingMemberRelInfo(Map info) throws DAOException;



    /**
     * 修改会议与参会人关系信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    void updateGovMeetingMemberRelInfo(Map info) throws DAOException;


    /**
     * 查询会议与参会人关系总数
     *
     * @param info 会议与参会人关系信息
     * @return 会议与参会人关系数量
     */
    int queryGovMeetingMemberRelsCount(Map info);

}
