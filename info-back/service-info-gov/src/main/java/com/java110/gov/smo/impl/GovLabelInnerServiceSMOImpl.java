package com.java110.gov.smo.impl;


import com.java110.gov.dao.IGovLabelServiceDao;
import com.java110.intf.gov.IGovLabelInnerServiceSMO;
import com.java110.dto.govLabel.GovLabelDto;
import com.java110.po.govLabel.GovLabelPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 标签管理内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovLabelInnerServiceSMOImpl extends BaseServiceSMO implements IGovLabelInnerServiceSMO {

    @Autowired
    private IGovLabelServiceDao govLabelServiceDaoImpl;


    @Override
    public int saveGovLabel(@RequestBody  GovLabelPo govLabelPo) {
        int saveFlag = 1;
        govLabelServiceDaoImpl.saveGovLabelInfo(BeanConvertUtil.beanCovertMap(govLabelPo));
        return saveFlag;
    }

     @Override
    public int updateGovLabel(@RequestBody  GovLabelPo govLabelPo) {
        int saveFlag = 1;
         govLabelServiceDaoImpl.updateGovLabelInfo(BeanConvertUtil.beanCovertMap(govLabelPo));
        return saveFlag;
    }

     @Override
    public int deleteGovLabel(@RequestBody  GovLabelPo govLabelPo) {
        int saveFlag = 1;
        govLabelPo.setStatusCd("1");
        govLabelServiceDaoImpl.updateGovLabelInfo(BeanConvertUtil.beanCovertMap(govLabelPo));
        return saveFlag;
    }

    @Override
    public List<GovLabelDto> queryGovLabels(@RequestBody  GovLabelDto govLabelDto) {

        //校验是否传了 分页信息

        int page = govLabelDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govLabelDto.setPage((page - 1) * govLabelDto.getRow());
        }

        List<GovLabelDto> govLabels = BeanConvertUtil.covertBeanList(govLabelServiceDaoImpl.getGovLabelInfo(BeanConvertUtil.beanCovertMap(govLabelDto)), GovLabelDto.class);

        return govLabels;
    }


    @Override
    public int queryGovLabelsCount(@RequestBody GovLabelDto govLabelDto) {
        return govLabelServiceDaoImpl.queryGovLabelsCount(BeanConvertUtil.beanCovertMap(govLabelDto));    }

    public IGovLabelServiceDao getGovLabelServiceDaoImpl() {
        return govLabelServiceDaoImpl;
    }

    public void setGovLabelServiceDaoImpl(IGovLabelServiceDao govLabelServiceDaoImpl) {
        this.govLabelServiceDaoImpl = govLabelServiceDaoImpl;
    }
}
