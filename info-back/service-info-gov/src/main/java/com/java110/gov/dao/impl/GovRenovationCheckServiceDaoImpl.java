package com.java110.gov.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.gov.dao.IGovRenovationCheckServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 重点地区整治情况服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govRenovationCheckServiceDaoImpl")
//@Transactional
public class GovRenovationCheckServiceDaoImpl extends BaseServiceDao implements IGovRenovationCheckServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovRenovationCheckServiceDaoImpl.class);





    /**
     * 保存重点地区整治情况信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovRenovationCheckInfo(Map info) throws DAOException {
        logger.debug("保存重点地区整治情况信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govRenovationCheckServiceDaoImpl.saveGovRenovationCheckInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存重点地区整治情况信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询重点地区整治情况信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovRenovationCheckInfo(Map info) throws DAOException {
        logger.debug("查询重点地区整治情况信息 入参 info : {}",info);

        List<Map> businessGovRenovationCheckInfos = sqlSessionTemplate.selectList("govRenovationCheckServiceDaoImpl.getGovRenovationCheckInfo",info);

        return businessGovRenovationCheckInfos;
    }


    /**
     * 修改重点地区整治情况信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovRenovationCheckInfo(Map info) throws DAOException {
        logger.debug("修改重点地区整治情况信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govRenovationCheckServiceDaoImpl.updateGovRenovationCheckInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改重点地区整治情况信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询重点地区整治情况数量
     * @param info 重点地区整治情况信息
     * @return 重点地区整治情况数量
     */
    @Override
    public int queryGovRenovationChecksCount(Map info) {
        logger.debug("查询重点地区整治情况数据 入参 info : {}",info);

        List<Map> businessGovRenovationCheckInfos = sqlSessionTemplate.selectList("govRenovationCheckServiceDaoImpl.queryGovRenovationChecksCount", info);
        if (businessGovRenovationCheckInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovRenovationCheckInfos.get(0).get("count").toString());
    }


}
