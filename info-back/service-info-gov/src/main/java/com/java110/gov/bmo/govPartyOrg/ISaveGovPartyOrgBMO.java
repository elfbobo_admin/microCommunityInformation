package com.java110.gov.bmo.govPartyOrg;

import com.java110.po.govPartyOrg.GovPartyOrgPo;
import org.springframework.http.ResponseEntity;
public interface ISaveGovPartyOrgBMO {


    /**
     * 添加党组织
     * add by wuxw
     * @param govPartyOrgPo
     * @return
     */
    ResponseEntity<String> save(GovPartyOrgPo govPartyOrgPo);


}
