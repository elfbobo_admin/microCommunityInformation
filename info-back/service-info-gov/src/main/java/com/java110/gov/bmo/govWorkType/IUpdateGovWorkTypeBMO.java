package com.java110.gov.bmo.govWorkType;
import com.java110.po.govWorkType.GovWorkTypePo;
import org.springframework.http.ResponseEntity;

public interface IUpdateGovWorkTypeBMO {


    /**
     * 修改党内职务
     * add by wuxw
     * @param govWorkTypePo
     * @return
     */
    ResponseEntity<String> update(GovWorkTypePo govWorkTypePo);


}
