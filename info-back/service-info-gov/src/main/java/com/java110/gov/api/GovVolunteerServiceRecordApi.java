package com.java110.gov.api;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govVolunteerServiceRecord.GovVolunteerServiceRecordDto;
import com.java110.po.govVolunteerServiceRecord.GovVolunteerServiceRecordPo;
import com.java110.gov.bmo.govVolunteerServiceRecord.IDeleteGovVolunteerServiceRecordBMO;
import com.java110.gov.bmo.govVolunteerServiceRecord.IGetGovVolunteerServiceRecordBMO;
import com.java110.gov.bmo.govVolunteerServiceRecord.ISaveGovVolunteerServiceRecordBMO;
import com.java110.gov.bmo.govVolunteerServiceRecord.IUpdateGovVolunteerServiceRecordBMO;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govVolunteerServiceRecord")
public class GovVolunteerServiceRecordApi {

    @Autowired
    private ISaveGovVolunteerServiceRecordBMO saveGovVolunteerServiceRecordBMOImpl;
    @Autowired
    private IUpdateGovVolunteerServiceRecordBMO updateGovVolunteerServiceRecordBMOImpl;
    @Autowired
    private IDeleteGovVolunteerServiceRecordBMO deleteGovVolunteerServiceRecordBMOImpl;

    @Autowired
    private IGetGovVolunteerServiceRecordBMO getGovVolunteerServiceRecordBMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govVolunteerServiceRecord/saveGovVolunteerServiceRecord
     * @path /app/govVolunteerServiceRecord/saveGovVolunteerServiceRecord
     */
    @RequestMapping(value = "/saveGovVolunteerServiceRecord", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovVolunteerServiceRecord(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "context", "请求报文中未包含context");
        Assert.hasKeyAndValue(reqJson, "title", "请求报文中未包含title");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        JSONArray oldPersons = reqJson.getJSONArray("oldPersons");
        if(oldPersons == null || oldPersons.size() < 1){
            throw new IllegalArgumentException("请添加志愿者服务对象");
        }
        JSONArray volunteers = reqJson.getJSONArray("volunteers");
        if(volunteers == null || volunteers.size() < 1){
            throw new IllegalArgumentException("请添加志愿者");
        }
        GovVolunteerServiceRecordPo govVolunteerServiceRecordPo = BeanConvertUtil.covertBean(reqJson, GovVolunteerServiceRecordPo.class);
        return saveGovVolunteerServiceRecordBMOImpl.save(govVolunteerServiceRecordPo,oldPersons,volunteers);
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govVolunteerServiceRecord/updateGovVolunteerServiceRecord
     * @path /app/govVolunteerServiceRecord/updateGovVolunteerServiceRecord
     */
    @RequestMapping(value = "/updateGovVolunteerServiceRecord", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovVolunteerServiceRecord(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "serviceRecordId", "请求报文中未包含serviceRecordId");
        Assert.hasKeyAndValue(reqJson, "context", "请求报文中未包含context");
        Assert.hasKeyAndValue(reqJson, "title", "请求报文中未包含title");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");

        GovVolunteerServiceRecordPo govVolunteerServiceRecordPo = BeanConvertUtil.covertBean(reqJson, GovVolunteerServiceRecordPo.class);
        return updateGovVolunteerServiceRecordBMOImpl.update(govVolunteerServiceRecordPo);
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govVolunteerServiceRecord/deleteGovVolunteerServiceRecord
     * @path /app/govVolunteerServiceRecord/deleteGovVolunteerServiceRecord
     */
    @RequestMapping(value = "/deleteGovVolunteerServiceRecord", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovVolunteerServiceRecord(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "caId", "caId不能为空");
        GovVolunteerServiceRecordPo govVolunteerServiceRecordPo = BeanConvertUtil.covertBean(reqJson, GovVolunteerServiceRecordPo.class);
        return deleteGovVolunteerServiceRecordBMOImpl.delete(govVolunteerServiceRecordPo);
    }

    /**
     * 微信删除消息模板
     *
     * @param caId 小区ID
     * @return
     * @serviceCode /govVolunteerServiceRecord/queryGovVolunteerServiceRecord
     * @path /app/govVolunteerServiceRecord/queryGovVolunteerServiceRecord
     */
    @RequestMapping(value = "/queryGovVolunteerServiceRecord", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovVolunteerServiceRecord(@RequestParam(value = "caId") String caId,
                                                                 @RequestParam(value = "title" , required = false) String title,
                                                                 @RequestParam(value = "page") int page,
                                                                 @RequestParam(value = "row") int row) {
        GovVolunteerServiceRecordDto govVolunteerServiceRecordDto = new GovVolunteerServiceRecordDto();
        govVolunteerServiceRecordDto.setPage(page);
        govVolunteerServiceRecordDto.setRow(row);
        govVolunteerServiceRecordDto.setCaId(caId);
        govVolunteerServiceRecordDto.setTitle(title);
        return getGovVolunteerServiceRecordBMOImpl.get(govVolunteerServiceRecordDto);
    }
}
