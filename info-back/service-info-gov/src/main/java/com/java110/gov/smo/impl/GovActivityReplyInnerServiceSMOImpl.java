package com.java110.gov.smo.impl;


import com.java110.gov.dao.IGovActivityReplyServiceDao;
import com.java110.intf.gov.IGovActivityReplyInnerServiceSMO;
import com.java110.dto.govActivityReply.GovActivityReplyDto;
import com.java110.po.govActivityReply.GovActivityReplyPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 活动评论内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovActivityReplyInnerServiceSMOImpl extends BaseServiceSMO implements IGovActivityReplyInnerServiceSMO {

    @Autowired
    private IGovActivityReplyServiceDao govActivityReplyServiceDaoImpl;


    @Override
    public int saveGovActivityReply(@RequestBody  GovActivityReplyPo govActivityReplyPo) {
        int saveFlag = 1;
        govActivityReplyServiceDaoImpl.saveGovActivityReplyInfo(BeanConvertUtil.beanCovertMap(govActivityReplyPo));
        return saveFlag;
    }

     @Override
    public int updateGovActivityReply(@RequestBody  GovActivityReplyPo govActivityReplyPo) {
        int saveFlag = 1;
         govActivityReplyServiceDaoImpl.updateGovActivityReplyInfo(BeanConvertUtil.beanCovertMap(govActivityReplyPo));
        return saveFlag;
    }

     @Override
    public int deleteGovActivityReply(@RequestBody  GovActivityReplyPo govActivityReplyPo) {
        int saveFlag = 1;
        govActivityReplyPo.setStatusCd("1");
        govActivityReplyServiceDaoImpl.updateGovActivityReplyInfo(BeanConvertUtil.beanCovertMap(govActivityReplyPo));
        return saveFlag;
    }

    @Override
    public List<GovActivityReplyDto> queryGovActivityReplys(@RequestBody  GovActivityReplyDto govActivityReplyDto) {

        //校验是否传了 分页信息

        int page = govActivityReplyDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govActivityReplyDto.setPage((page - 1) * govActivityReplyDto.getRow());
        }

        List<GovActivityReplyDto> govActivityReplys = BeanConvertUtil.covertBeanList(govActivityReplyServiceDaoImpl.getGovActivityReplyInfo(BeanConvertUtil.beanCovertMap(govActivityReplyDto)), GovActivityReplyDto.class);

        return govActivityReplys;
    }


    @Override
    public int queryGovActivityReplysCount(@RequestBody GovActivityReplyDto govActivityReplyDto) {
        return govActivityReplyServiceDaoImpl.queryGovActivityReplysCount(BeanConvertUtil.beanCovertMap(govActivityReplyDto));    }

    public IGovActivityReplyServiceDao getGovActivityReplyServiceDaoImpl() {
        return govActivityReplyServiceDaoImpl;
    }

    public void setGovActivityReplyServiceDaoImpl(IGovActivityReplyServiceDao govActivityReplyServiceDaoImpl) {
        this.govActivityReplyServiceDaoImpl = govActivityReplyServiceDaoImpl;
    }
}
