package com.java110.gov.bmo.govGuideSubscribe.impl;

import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.gov.bmo.govGuideSubscribe.ISaveGovGuideSubscribeBMO;
import com.java110.intf.gov.IGovGuideSubscribeInnerServiceSMO;
import com.java110.po.govGuideSubscribe.GovGuideSubscribePo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("saveGovGuideSubscribeBMOImpl")
public class SaveGovGuideSubscribeBMOImpl implements ISaveGovGuideSubscribeBMO {

    @Autowired
    private IGovGuideSubscribeInnerServiceSMO govGuideSubscribeInnerServiceSMOImpl;

    /**
     * 添加小区信息
     *
     * @param govGuideSubscribePo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> save(GovGuideSubscribePo govGuideSubscribePo) {

        govGuideSubscribePo.setWgsId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_wgsId));
        int flag = govGuideSubscribeInnerServiceSMOImpl.saveGovGuideSubscribe(govGuideSubscribePo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
