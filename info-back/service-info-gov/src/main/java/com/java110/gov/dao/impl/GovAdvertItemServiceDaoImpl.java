package com.java110.gov.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.gov.dao.IGovAdvertItemServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 广告明细服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govAdvertItemServiceDaoImpl")
//@Transactional
public class GovAdvertItemServiceDaoImpl extends BaseServiceDao implements IGovAdvertItemServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovAdvertItemServiceDaoImpl.class);





    /**
     * 保存广告明细信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovAdvertItemInfo(Map info) throws DAOException {
        logger.debug("保存广告明细信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govAdvertItemServiceDaoImpl.saveGovAdvertItemInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存广告明细信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询广告明细信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovAdvertItemInfo(Map info) throws DAOException {
        logger.debug("查询广告明细信息 入参 info : {}",info);

        List<Map> businessGovAdvertItemInfos = sqlSessionTemplate.selectList("govAdvertItemServiceDaoImpl.getGovAdvertItemInfo",info);

        return businessGovAdvertItemInfos;
    }


    /**
     * 修改广告明细信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovAdvertItemInfo(Map info) throws DAOException {
        logger.debug("修改广告明细信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govAdvertItemServiceDaoImpl.updateGovAdvertItemInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改广告明细信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询广告明细数量
     * @param info 广告明细信息
     * @return 广告明细数量
     */
    @Override
    public int queryGovAdvertItemsCount(Map info) {
        logger.debug("查询广告明细数据 入参 info : {}",info);

        List<Map> businessGovAdvertItemInfos = sqlSessionTemplate.selectList("govAdvertItemServiceDaoImpl.queryGovAdvertItemsCount", info);
        if (businessGovAdvertItemInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovAdvertItemInfos.get(0).get("count").toString());
    }


}
