package com.java110.gov.bmo.govPetitionLetter;
import org.springframework.http.ResponseEntity;
import com.java110.dto.govPetitionLetter.GovPetitionLetterDto;
public interface IGetGovPetitionLetterBMO {


    /**
     * 查询信访管理
     * add by wuxw
     * @param  govPetitionLetterDto
     * @return
     */
    ResponseEntity<String> get(GovPetitionLetterDto govPetitionLetterDto);


}
