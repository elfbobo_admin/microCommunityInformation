package com.java110.gov.bmo.govWorkGuide.impl;

import com.java110.dto.govWorkGuide.GovWorkGuideDto;
import com.java110.gov.bmo.govWorkGuide.IGetGovWorkGuideBMO;
import com.java110.intf.gov.IGovWorkGuideInnerServiceSMO;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("getGovWorkGuideBMOImpl")
public class GetGovWorkGuideBMOImpl implements IGetGovWorkGuideBMO {

    @Autowired
    private IGovWorkGuideInnerServiceSMO govWorkGuideInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govWorkGuideDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovWorkGuideDto govWorkGuideDto) {


        int count = govWorkGuideInnerServiceSMOImpl.queryGovWorkGuidesCount(govWorkGuideDto);

        List<GovWorkGuideDto> govWorkGuideDtos = null;
        if (count > 0) {
            govWorkGuideDtos = govWorkGuideInnerServiceSMOImpl.queryGovWorkGuides(govWorkGuideDto);
        } else {
            govWorkGuideDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govWorkGuideDto.getRow()), count, govWorkGuideDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
