package com.java110.gov.smo.impl;


import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.PageDto;
import com.java110.dto.govReportSetting.GovReportSettingDto;
import com.java110.gov.dao.IGovReportSettingServiceDao;
import com.java110.intf.gov.IGovReportSettingInnerServiceSMO;
import com.java110.po.govReportSetting.GovReportSettingPo;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 报事设置内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovReportSettingInnerServiceSMOImpl extends BaseServiceSMO implements IGovReportSettingInnerServiceSMO {

    @Autowired
    private IGovReportSettingServiceDao govReportSettingServiceDaoImpl;


    @Override
    public int saveGovReportSetting(@RequestBody GovReportSettingPo govReportSettingPo) {
        int saveFlag = 1;
        govReportSettingServiceDaoImpl.saveGovReportSettingInfo(BeanConvertUtil.beanCovertMap(govReportSettingPo));
        return saveFlag;
    }

    @Override
    public int updateGovReportSetting(@RequestBody GovReportSettingPo govReportSettingPo) {
        int saveFlag = 1;
        govReportSettingServiceDaoImpl.updateGovReportSettingInfo(BeanConvertUtil.beanCovertMap(govReportSettingPo));
        return saveFlag;
    }

    @Override
    public int deleteGovReportSetting(@RequestBody GovReportSettingPo govReportSettingPo) {
        int saveFlag = 1;
        govReportSettingPo.setStatusCd("1");
        govReportSettingServiceDaoImpl.updateGovReportSettingInfo(BeanConvertUtil.beanCovertMap(govReportSettingPo));
        return saveFlag;
    }

    @Override
    public List<GovReportSettingDto> queryGovReportSettings(@RequestBody GovReportSettingDto govReportSettingDto) {

        //校验是否传了 分页信息

        int page = govReportSettingDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govReportSettingDto.setPage((page - 1) * govReportSettingDto.getRow());
        }

        List<GovReportSettingDto> govReportSettings = BeanConvertUtil.covertBeanList(govReportSettingServiceDaoImpl.getGovReportSettingInfo(BeanConvertUtil.beanCovertMap(govReportSettingDto)), GovReportSettingDto.class);

        return govReportSettings;
    }


    @Override
    public int queryGovReportSettingsCount(@RequestBody GovReportSettingDto govReportSettingDto) {
        return govReportSettingServiceDaoImpl.queryGovReportSettingsCount(BeanConvertUtil.beanCovertMap(govReportSettingDto));
    }

    public IGovReportSettingServiceDao getGovReportSettingServiceDaoImpl() {
        return govReportSettingServiceDaoImpl;
    }

    public void setGovReportSettingServiceDaoImpl(IGovReportSettingServiceDao govReportSettingServiceDaoImpl) {
        this.govReportSettingServiceDaoImpl = govReportSettingServiceDaoImpl;
    }
}
