package com.java110.gov.bmo.govMeetingList;
import com.alibaba.fastjson.JSONArray;
import org.springframework.http.ResponseEntity;
import com.java110.po.govMeetingList.GovMeetingListPo;

public interface IUpdateGovMeetingListBMO {


    /**
     * 修改会议列表
     * add by wuxw
     * @param govMeetingListPo
     * @return
     */
    ResponseEntity<String> update(GovMeetingListPo govMeetingListPo, JSONArray govMembers);

    /**
     * 修改会议列表
     * add by wuxw
     * @param govMeetingListPo
     * @return
     */
    ResponseEntity<String> recordGovMeetingList(GovMeetingListPo govMeetingListPo,JSONArray meetingImgs);


}
