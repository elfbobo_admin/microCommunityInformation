package com.java110.gov.bmo.govCiviladmin;
import com.java110.dto.govCiviladmin.GovCiviladminDto;
import org.springframework.http.ResponseEntity;
public interface IGetGovCiviladminBMO {


    /**
     * 查询民政服务宣传
     * add by wuxw
     * @param  govCiviladminDto
     * @return
     */
    ResponseEntity<String> get(GovCiviladminDto govCiviladminDto);


}
