package com.java110.gov.bmo.govPersonLabelRel;
import org.springframework.http.ResponseEntity;
import com.java110.dto.govPersonLabelRel.GovPersonLabelRelDto;
public interface IGetGovPersonLabelRelBMO {


    /**
     * 查询标签与人口关系
     * add by wuxw
     * @param  govPersonLabelRelDto
     * @return
     */
    ResponseEntity<String> get(GovPersonLabelRelDto govPersonLabelRelDto);


}
