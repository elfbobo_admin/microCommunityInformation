package com.java110.gov.bmo.govDrug.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.gov.bmo.govDrug.IUpdateGovDrugBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.gov.IGovDrugInnerServiceSMO;
import com.java110.dto.govDrug.GovDrugDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.java110.po.govDrug.GovDrugPo;
import java.util.List;

@Service("updateGovDrugBMOImpl")
public class UpdateGovDrugBMOImpl implements IUpdateGovDrugBMO {

    @Autowired
    private IGovDrugInnerServiceSMO govDrugInnerServiceSMOImpl;

    /**
     *
     *
     * @param govDrugPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> update(GovDrugPo govDrugPo) {

        int flag = govDrugInnerServiceSMOImpl.updateGovDrug(govDrugPo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
