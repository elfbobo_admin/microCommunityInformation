package com.java110.gov.dao;


import com.java110.utils.exception.DAOException;

import java.util.List;
import java.util.Map;

/**
 * 车辆进出记录组件内部之间使用，没有给外围系统提供服务能力
 * 车辆进出记录服务接口类，要求全部以字符串传输，方便微服务化
 * 新建客户，修改客户，删除客户，查询客户等功能
 *
 * Created by wuxw on 2016/12/27.
 */
public interface IGovCarInoutServiceDao {


    /**
     * 保存 车辆进出记录信息
     * @param info
     * @throws DAOException DAO异常
     */
    void saveGovCarInoutInfo(Map info) throws DAOException;




    /**
     * 查询车辆进出记录信息（instance过程）
     * 根据bId 查询车辆进出记录信息
     * @param info bId 信息
     * @return 车辆进出记录信息
     * @throws DAOException DAO异常
     */
    List<Map> getGovCarInoutInfo(Map info) throws DAOException;



    /**
     * 修改车辆进出记录信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    void updateGovCarInoutInfo(Map info) throws DAOException;


    /**
     * 查询车辆进出记录总数
     *
     * @param info 车辆进出记录信息
     * @return 车辆进出记录数量
     */
    int queryGovCarInoutsCount(Map info);

}
