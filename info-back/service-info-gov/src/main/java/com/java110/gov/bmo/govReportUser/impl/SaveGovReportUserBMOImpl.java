package com.java110.gov.bmo.govReportUser.impl;


import com.alibaba.fastjson.JSONObject;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.dto.govReportSetting.GovReportSettingDto;
import com.java110.dto.govReportUser.GovReportUserDto;
import com.java110.dto.reportPool.ReportPoolDto;
import com.java110.dto.user.UserDto;
import com.java110.gov.bmo.govReportUser.ISaveGovReportUserBMO;
import com.java110.intf.cust.IUserInnerServiceSMO;
import com.java110.intf.gov.IGovReportSettingInnerServiceSMO;
import com.java110.intf.gov.IGovReportUserInnerServiceSMO;
import com.java110.intf.gov.IReportPoolInnerServiceSMO;
import com.java110.po.govReportUser.GovReportUserPo;
import com.java110.po.reportPool.ReportPoolPo;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.utils.util.DateUtil;
import com.java110.vo.ResultVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;


@Service("saveGovReportUserBMOImpl")
public class SaveGovReportUserBMOImpl implements ISaveGovReportUserBMO {

    private static Logger logger = LoggerFactory.getLogger(SaveGovReportUserBMOImpl.class);
    @Autowired
    private IGovReportUserInnerServiceSMO govReportUserInnerServiceSMOImpl;
    @Autowired
    private IUserInnerServiceSMO userInnerServiceSMOImpl;
    @Autowired
    private IReportPoolInnerServiceSMO reportPoolInnerServiceSMOImpl;
    @Autowired
    private IGovReportSettingInnerServiceSMO govReportSettingInnerServiceSMOImpl;
    //派单
    public static final String ACTION_DISPATCH = "DISPATCH";

    //转单
    public static final String ACTION_TRANSFER = "TRANSFER";

    //退单
    public static final String ACTION_BACK = "BACK";

    /**
     * 添加小区信息
     *
     * @param govReportUserPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> save(GovReportUserPo govReportUserPo) {

        govReportUserPo.setRuId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_ruId));
        int flag = govReportUserInnerServiceSMOImpl.saveGovReportUser(govReportUserPo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

    @Override
    @Java110Transactional
    public ResponseEntity<String> poolDispatch(JSONObject reqJson) {
        ResponseEntity<String> responseEntity = null;

        UserDto userDto = new UserDto();
        userDto.setUserId(reqJson.getString("userId"));
        List<UserDto> userDtos = userInnerServiceSMOImpl.getUsers(userDto);

        Assert.listOnlyOne(userDtos, "用户不存在");

        reqJson.put("userName", userDtos.get(0).getUserName());

        String action = reqJson.getString("action");
        switch (action) {
            case ACTION_DISPATCH:
                responseEntity = dispacthRepair(reqJson);
                break;
            case ACTION_TRANSFER:
                responseEntity = transferRepair(reqJson);
                break;
            case ACTION_BACK:
                responseEntity = backRepair(reqJson);
                break;
        }
        return responseEntity;
    }

    /**
     * 派单处理
     *
     * @param reqJson
     */
    private ResponseEntity<String> dispacthRepair(JSONObject reqJson) {
        //获取报修id
        ResponseEntity<String> responseEntity = null;
        String reportId = reqJson.getString("reportId");
        ReportPoolDto reportPoolDto = new ReportPoolDto();
        reportPoolDto.setReportId(reportId);
        List<ReportPoolDto> reportPoolDtos = reportPoolInnerServiceSMOImpl.queryReportPools(reportPoolDto);
        if (reportPoolDtos == null || reportPoolDtos.size() < 1) {
            responseEntity = ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "数据错误！");
            return responseEntity;
        } else {
            //获取状态
            String state = reportPoolDtos.get(0).getState();
            if (state.equals("1000")) {   //1000表示未派单
                String userId = reqJson.getString("userId");
                String userName = reqJson.getString("userName");

                String ruId = GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_ruId);
                // 自己的单子状态修改为
                GovReportUserPo govReportUserPo = new GovReportUserPo();
                govReportUserPo.setRuId(ruId);
                govReportUserPo.setStartTime( DateUtil.getNow(DateUtil.DATE_FORMATE_STRING_A));
                govReportUserPo.setEndTime(DateUtil.getNow(DateUtil.DATE_FORMATE_STRING_A));
                govReportUserPo.setState( GovReportUserDto.STATE_DISPATCH);
                govReportUserPo.setReportId(reqJson.getString("reportId"));
                govReportUserPo.setStaffId(userId);
                govReportUserPo.setStaffName(userName);
                freshPreStaff(reqJson, govReportUserPo);
                govReportUserPo.setReportEvent(GovReportUserDto.POOL_EVENT_AUDIT_USER);
                govReportUserPo.setContext(reqJson.getString("context"));
                govReportUserPo.setCaId(reqJson.getString("caId"));
                int flag = govReportUserInnerServiceSMOImpl.saveGovReportUser(govReportUserPo);
                if (flag < 1) {
                    throw new IllegalArgumentException("保存失败");
                }
                //处理人信息
                govReportUserPo= new GovReportUserPo();
                govReportUserPo.setRuId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_ruId));
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(new Date());
                calendar.add(Calendar.SECOND, 30);
                govReportUserPo.setStartTime(DateUtil.getFormatTimeString(calendar.getTime(), DateUtil.DATE_FORMATE_STRING_A));
                govReportUserPo.setState(GovReportUserDto.STATE_DOING);
                govReportUserPo.setReportId(reqJson.getString("reportId"));
                govReportUserPo.setStaffId(reqJson.getString("staffId"));
                govReportUserPo.setStaffName(reqJson.getString("staffName"));
                govReportUserPo.setPreStaffId(userId);
                govReportUserPo.setPreStaffName(userName);
                govReportUserPo.setPreRuId(ruId);
                govReportUserPo.setReportEvent(GovReportUserDto.POOL_EVENT_AUDIT_USER);
                govReportUserPo.setContext("");
                govReportUserPo.setCaId(reqJson.getString("caId"));
                flag = govReportUserInnerServiceSMOImpl.saveGovReportUser(govReportUserPo);
                if (flag < 1) {
                    throw new IllegalArgumentException("保存失败");
                }

                //将订单状态
                modifyBusinessServPoolDispatch(reqJson, ReportPoolDto.STATE_TAKING);
                responseEntity = ResultVo.createResponseEntity(ResultVo.CODE_OK, ResultVo.MSG_OK);
            } else if (state.equals("1100")) {   //1100表示接单
                responseEntity = ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "该订单处于接单状态，无法进行派单！");
            } else {
                responseEntity = ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "状态异常！");
            }
        }

        return responseEntity;
    }

    /**
     * 转单
     *
     * @param reqJson
     */
    private ResponseEntity<String> transferRepair(JSONObject reqJson) {
        String userId = reqJson.getString("userId");
        String userName = reqJson.getString("userName");

        GovReportUserDto govReportUserDto = new GovReportUserDto();
        govReportUserDto.setReportId(reqJson.getString("reportId"));
        govReportUserDto.setCaId(reqJson.getString("caId"));
        govReportUserDto.setState(GovReportUserDto.STATE_DOING);
        govReportUserDto.setStaffId(userId);
        List<GovReportUserDto> govReportUserDtoList = govReportUserInnerServiceSMOImpl.queryGovReportUsers(govReportUserDto);
        Assert.listOnlyOne(govReportUserDtoList, "当前用户没有需要处理订单");
        //插入派单者的信息
        GovReportUserPo govReportUserPo = new GovReportUserPo();
        govReportUserPo.setRuId(govReportUserDtoList.get(0).getRuId());
        govReportUserPo.setEndTime(DateUtil.getNow(DateUtil.DATE_FORMATE_STRING_A));
        govReportUserPo.setState(GovReportUserDto.STATE_TRANSFER);
        govReportUserPo.setContext(reqJson.getString("context"));
        govReportUserPo.setCaId(reqJson.getString("caId"));
        int flag = govReportUserInnerServiceSMOImpl.updateGovReportUser(govReportUserPo);
        if (flag < 1) {
            throw new IllegalArgumentException("修改失败");
        }
        //处理人信息
        govReportUserPo = new GovReportUserPo();
        govReportUserPo.setRuId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_ruId));
        govReportUserPo.setState(GovReportUserDto.STATE_DOING);
        govReportUserPo.setReportId(reqJson.getString("reportId"));
        govReportUserPo.setStaffId(reqJson.getString("staffId"));
        govReportUserPo.setStaffName(reqJson.getString("staffName"));
        govReportUserPo.setPreStaffId(userId);
        govReportUserPo.setStartTime(DateUtil.getNow(DateUtil.DATE_FORMATE_STRING_A));
        govReportUserPo.setPreStaffName(userName);
        govReportUserPo.setPreRuId(govReportUserDtoList.get(0).getRuId());
        govReportUserPo.setReportEvent(GovReportUserDto.POOL_EVENT_AUDIT_USER);
        govReportUserPo.setContext("");
        govReportUserPo.setCaId(reqJson.getString("caId"));

        flag = govReportUserInnerServiceSMOImpl.saveGovReportUser(govReportUserPo);
        if (flag < 1) {
            throw new IllegalArgumentException("保存失败");
        }
        modifyBusinessServPoolDispatch(reqJson, ReportPoolDto.STATE_TRANSFER);

        ResponseEntity<String> responseEntity = ResultVo.createResponseEntity(ResultVo.CODE_OK, ResultVo.MSG_OK);

        return responseEntity;
    }

    private ResponseEntity<String> backRepair(JSONObject reqJson) {

        //查询订单状态
        ReportPoolDto reportPoolDto = new ReportPoolDto();
        reportPoolDto.setReportId(reqJson.getString("reportId"));
        reportPoolDto.setCaId(reqJson.getString("caId"));
        List<ReportPoolDto> reportPoolDtoList = reportPoolInnerServiceSMOImpl.queryReportPools(reportPoolDto);
        Assert.listOnlyOne(reportPoolDtoList, "当前用户没有需要处理订单或存在多条");


        String userId = reqJson.getString("userId");
        String userName = reqJson.getString("userName");

        GovReportUserDto govReportUserDto = new GovReportUserDto();
        govReportUserDto.setReportId(reqJson.getString("reportId"));
        govReportUserDto.setCaId(reqJson.getString("caId"));
        govReportUserDto.setState(GovReportUserDto.STATE_DOING);
        govReportUserDto.setStaffId(userId);
        List<GovReportUserDto> reportUserDtoList = govReportUserInnerServiceSMOImpl.queryGovReportUsers(govReportUserDto);
        Assert.listOnlyOne(reportUserDtoList, "当前用户没有需要处理订单");

        govReportUserDto = new GovReportUserDto();
        govReportUserDto.setReportId(reqJson.getString("reportId"));
        govReportUserDto.setStaffId(reqJson.getString("staffId"));
        govReportUserDto.setCaId(reqJson.getString("caId"));
        govReportUserDto.setRuId(reportUserDtoList.get(0).getPreRuId());
        govReportUserDto.setStates(new String[]{GovReportUserDto.STATE_TRANSFER, GovReportUserDto.STATE_CLOSE,GovReportUserDto.STATE_BACK});
        reportUserDtoList = govReportUserInnerServiceSMOImpl.queryGovReportUsers(govReportUserDto);
        Assert.listOnlyOne(reportUserDtoList, "非常抱歉当前不能退单");

        //把自己改成退单
        GovReportUserPo govReportUserPo = new GovReportUserPo();
        govReportUserPo.setRuId(reportUserDtoList.get(0).getRuId());
        govReportUserPo.setEndTime(DateUtil.getNow(DateUtil.DATE_FORMATE_STRING_A));
        govReportUserPo.setState(GovReportUserDto.STATE_BACK);
        govReportUserPo.setContext(reqJson.getString("context"));
        govReportUserPo.setCaId(reqJson.getString("caId"));
        int flag = govReportUserInnerServiceSMOImpl.updateGovReportUser(govReportUserPo);
        if (flag < 1) {
            throw new IllegalArgumentException("把自己改成退单处理失败");
        }
        //处理人信息
        govReportUserPo = new GovReportUserPo();
        govReportUserPo.setRuId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_ruId));
        govReportUserPo.setState(GovReportUserDto.STATE_DOING);
        govReportUserPo.setReportId(reqJson.getString("reportId"));
        govReportUserPo.setStaffId(reqJson.getString("staffId"));
        govReportUserPo.setStaffName(reqJson.getString("staffName"));
        govReportUserPo.setStartTime(DateUtil.getNow(DateUtil.DATE_FORMATE_STRING_A));


        govReportUserPo.setPreStaffId(reportUserDtoList.get(0).getPreStaffId());
        govReportUserPo.setPreStaffName(reportUserDtoList.get(0).getPreStaffName());
        govReportUserPo.setPreRuId(reportUserDtoList.get(0).getRuId());
        govReportUserPo.setReportEvent(GovReportUserDto.POOL_EVENT_AUDIT_USER);
        govReportUserPo.setContext("");
        govReportUserPo.setCaId(reqJson.getString("caId"));
        if (govReportUserInnerServiceSMOImpl.saveGovReportUser(govReportUserPo) < 1) {
            throw new IllegalArgumentException("保存失败");
        }
        modifyBusinessServPoolDispatch(reqJson, ReportPoolDto.STATE_BACK);

        return ResultVo.success();
    }

    private void modifyBusinessServPoolDispatch(JSONObject reqJson, String state) {

        //查询报修单
        ReportPoolDto reportPoolDto = new ReportPoolDto();
        reportPoolDto.setReportId(reqJson.getString("poolId"));
        reportPoolDto.setCaId(reqJson.getString("caId"));
        List<ReportPoolDto> reportPoolDtoList = reportPoolInnerServiceSMOImpl.queryReportPools(reportPoolDto);

        logger.debug("查询报修单结果：" + JSONObject.toJSONString(reportPoolDtoList.get(0)));


        JSONObject businessPool = new JSONObject();
        businessPool.putAll( BeanConvertUtil.beanCovertMap(reportPoolDtoList.get(0)));
        businessPool.put("state", state);

        ReportPoolPo reportPoolPo = BeanConvertUtil.covertBean(businessPool, ReportPoolPo.class);

        if (reportPoolInnerServiceSMOImpl.updateReportPool(reportPoolPo) < 1) {
            throw new IllegalArgumentException("修改工单池状态失败");
        }
    }

    /**
     * 刷入上一处理人
     *
     * @param reqJson
     * @param govReportUserPo
     */
    private void freshPreStaff(JSONObject reqJson, GovReportUserPo govReportUserPo) {

        GovReportUserDto govReportUserDto = new GovReportUserDto();
        govReportUserDto.setCaId(reqJson.getString("caId"));
        govReportUserDto.setReportId(reqJson.getString("reportId"));
        //housekeepingServPoolUserDto.setPoolEvent(HousekeepingServPoolUserDto.POOL_EVENT_START_USER);
        List<GovReportUserDto> govReportUserDtoList = govReportUserInnerServiceSMOImpl.queryGovReportUsers(govReportUserDto);
        if (govReportUserDtoList == null || govReportUserDtoList.size() < 1) {
            govReportUserPo.setPreStaffId("-1");
            govReportUserPo.setPreStaffName("-1");
            govReportUserPo.setPreRuId("-1");
        } else {
            int pos = govReportUserDtoList.size() - 1;
            govReportUserPo.setPreStaffId(govReportUserDtoList.get(pos).getStaffId());
            govReportUserPo.setPreStaffName(govReportUserDtoList.get(pos).getStaffName());
            govReportUserPo.setPreRuId(govReportUserDtoList.get(pos).getRuId());
        }
    }

    @Override
    @Java110Transactional
    public ResponseEntity<String> poolFinish(JSONObject reqJson) {
        String userId = reqJson.getString("staffId");
        UserDto userDto = new UserDto();
        userDto.setUserId(userId);
        List<UserDto> userDtos = userInnerServiceSMOImpl.getUsers(userDto);
        Assert.listOnlyOne(userDtos, "用户不存在");
        reqJson.put("userName", userDtos.get(0).getUserName());
        String userName = reqJson.getString("userName");
        //获取商品集合
        //判断当前用户是否有需要处理的订单
        GovReportUserDto govReportUserDto = new GovReportUserDto();
        govReportUserDto.setReportId(reqJson.getString("reportId"));
        govReportUserDto.setCaId(reqJson.getString("caId"));
        govReportUserDto.setState(GovReportUserDto.STATE_DOING);
        govReportUserDto.setStaffId(userId);
        List<GovReportUserDto> govReportUserDtoList = govReportUserInnerServiceSMOImpl.queryGovReportUsers(govReportUserDto);
        if (govReportUserDtoList == null || govReportUserDtoList.size() != 1) {
            return ResultVo.error("当前用户没有需要处理订单");
        }

        ReportPoolDto reportPoolDto = new ReportPoolDto();
        reportPoolDto.setReportId(govReportUserDtoList.get(0).getReportId());
        reportPoolDto.setCaId(govReportUserDtoList.get(0).getCaId());
        List<ReportPoolDto> reportPoolDtoList = reportPoolInnerServiceSMOImpl.queryReportPools(reportPoolDto);
        Assert.listOnlyOne(reportPoolDtoList, "订单不存在");

        // 1.0 关闭自己订单
        GovReportUserPo govReportUserPo = new GovReportUserPo();
        govReportUserPo.setRuId(govReportUserDtoList.get(0).getRuId());
        govReportUserPo.setEndTime(DateUtil.getNow(DateUtil.DATE_FORMATE_STRING_A));
        govReportUserPo.setState(GovReportUserDto.STATE_CLOSE);
        govReportUserPo.setContext(reqJson.getString("context"));
        govReportUserPo.setCaId(reqJson.getString("caId"));
        int flag = govReportUserInnerServiceSMOImpl.updateGovReportUser(govReportUserPo);
        if (flag < 1) {
            throw new IllegalArgumentException("关闭自己订单失败");
        }
        ReportPoolPo reportPoolPo = new ReportPoolPo();
        reportPoolPo.setCaId(reportPoolDtoList.get(0).getCaId());
        reportPoolPo.setReportId(reportPoolDtoList.get(0).getReportId());
        reportPoolPo.setState(ReportPoolDto.STATE_APPRAISE);
        flag = reportPoolInnerServiceSMOImpl.updateReportPool(reportPoolPo);
        if (flag < 1) {
            throw new IllegalArgumentException("更新订单失败");
        }

        //判断当前用户是否有需要处理的订单
        govReportUserDto = new GovReportUserDto();
        govReportUserDto.setReportId(reqJson.getString("reportId"));
        govReportUserDto.setCaId(reqJson.getString("caId"));
        govReportUserDto.setState(GovReportUserDto.STATE_SUBMIT);
        govReportUserDto.setReportEvent(GovReportUserDto.POOL_EVENT_START_USER);
        List<GovReportUserDto> govReportUserDtos = govReportUserInnerServiceSMOImpl.queryGovReportUsers(govReportUserDto);
        if (govReportUserDtos == null || govReportUserDtos.size() != 1) {
            return ResultVo.error("没有查到提交者信息");
        }

        //处理人信息
        govReportUserPo = new GovReportUserPo();
        govReportUserPo.setRuId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_ruId));
        govReportUserPo.setState(GovReportUserDto.STATE_EVALUATE);
        govReportUserPo.setReportId(reqJson.getString("reportId"));
        govReportUserPo.setStaffId(govReportUserDtos.get(0).getStaffId());
        govReportUserPo.setStaffName(govReportUserDtos.get(0).getStaffName());
        govReportUserPo.setStartTime(DateUtil.getNow(DateUtil.DATE_FORMATE_STRING_A));
        govReportUserPo.setPreStaffId(reqJson.getString("staffId"));
        govReportUserPo.setPreStaffName(reqJson.getString("userName"));
        govReportUserPo.setPreRuId(govReportUserDtoList.get(0).getRuId());
        govReportUserPo.setReportEvent(GovReportUserDto.POOL_EVENT_AUDIT_USER);
        govReportUserPo.setContext("");
        govReportUserPo.setCaId(reqJson.getString("caId"));
        flag = govReportUserInnerServiceSMOImpl.saveGovReportUser(govReportUserPo);
        if (flag < 1) {
            throw new IllegalArgumentException("保存待评价订单失败");
        }
        return ResultVo.success();
    }
    @Override
    @Java110Transactional
    public ResponseEntity<String> poolFinishHui(JSONObject reqJson) {
        String userId = reqJson.getString("staffId");
        UserDto userDto = new UserDto();
        userDto.setUserId(userId);
        List<UserDto> userDtos = userInnerServiceSMOImpl.getUsers(userDto);
        Assert.listOnlyOne(userDtos, "用户不存在");
        reqJson.put("userName", userDtos.get(0).getUserName());
        String userName = reqJson.getString("userName");
        //获取商品集合
        //判断当前用户是否有需要处理的订单
        GovReportUserDto govReportUserDto = new GovReportUserDto();
        govReportUserDto.setReportId(reqJson.getString("reportId"));
        govReportUserDto.setCaId(reqJson.getString("caId"));
        govReportUserDto.setState(GovReportUserDto.STATE_EVALUATE);
        govReportUserDto.setStaffId(userId);
        List<GovReportUserDto> govReportUserDtoList = govReportUserInnerServiceSMOImpl.queryGovReportUsers(govReportUserDto);
        if (govReportUserDtoList == null || govReportUserDtoList.size() != 1) {
            return ResultVo.error("当前用户没有需要处理订单");
        }

        ReportPoolDto reportPoolDto = new ReportPoolDto();
        reportPoolDto.setReportId(govReportUserDtoList.get(0).getReportId());
        reportPoolDto.setCaId(govReportUserDtoList.get(0).getCaId());
        List<ReportPoolDto> reportPoolDtoList = reportPoolInnerServiceSMOImpl.queryReportPools(reportPoolDto);
        Assert.listOnlyOne(reportPoolDtoList, "订单不存在");

        // 1.0 关闭自己订单
        GovReportUserPo govReportUserPo = new GovReportUserPo();
        govReportUserPo.setRuId(govReportUserDtoList.get(0).getRuId());
        govReportUserPo.setEndTime(DateUtil.getNow(DateUtil.DATE_FORMATE_STRING_A));
        govReportUserPo.setState(GovReportUserDto.STATE_FINISH);
        govReportUserPo.setContext(reqJson.getString("context"));
        govReportUserPo.setCaId(reqJson.getString("caId"));
        int flag = govReportUserInnerServiceSMOImpl.updateGovReportUser(govReportUserPo);
        if (flag < 1) {
            throw new IllegalArgumentException("关闭自己订单失败");
        }
        //刷新工单状态为待回复 需要验证工单类型 001不回访 003回访
        String state = ReportPoolDto.STATE_COMPLATE;
//        String userState = GovReportUserDto.STATE_DOING_FINISH_VISIT;
        GovReportSettingDto govReportSettingDto = new GovReportSettingDto();
        govReportSettingDto.setCaId(reportPoolDtoList.get(0).getCaId());
        govReportSettingDto.setReportType(reportPoolDtoList.get(0).getReportType());
        List<GovReportSettingDto> govReportSettingDtos = govReportSettingInnerServiceSMOImpl.queryGovReportSettings(govReportSettingDto);
        if(govReportSettingDtos != null && govReportSettingDtos.size() > 0 ){
            if("003".equals(govReportSettingDtos.get(0).getReturnVisitFlag())){
                state = ReportPoolDto.STATE_RETURN_VISIT;
            }
        }
        ReportPoolPo reportPoolPo = new ReportPoolPo();
        reportPoolPo.setCaId(reportPoolDtoList.get(0).getCaId());
        reportPoolPo.setReportId(reportPoolDtoList.get(0).getReportId());
        reportPoolPo.setState(state);
        flag = reportPoolInnerServiceSMOImpl.updateReportPool(reportPoolPo);
        if (flag < 1) {
            throw new IllegalArgumentException("更新订单失败");
        }

        //判断当前用户是否有需要处理的订单
//        govReportUserDto = new GovReportUserDto();
//        govReportUserDto.setReportId(reqJson.getString("reportId"));
//        govReportUserDto.setCaId(reqJson.getString("caId"));
//        govReportUserDto.setState(GovReportUserDto.STATE_SUBMIT);
//        govReportUserDto.setReportEvent(GovReportUserDto.POOL_EVENT_START_USER);
//        List<GovReportUserDto> govReportUserDtos = govReportUserInnerServiceSMOImpl.queryGovReportUsers(govReportUserDto);
//        if (govReportUserDtos == null || govReportUserDtos.size() != 1) {
//            return ResultVo.error("没有查到提交者信息");
//        }

        //处理人信息
//        govReportUserPo = new GovReportUserPo();
//        govReportUserPo.setRuId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_ruId));
//        govReportUserPo.setState(GovReportUserDto.STATE_DOING_FINISH_VISIT);
//        govReportUserPo.setReportId(reqJson.getString("reportId"));
//        govReportUserPo.setStaffId(govReportUserDtos.get(0).getStaffId());
//        govReportUserPo.setStaffName(govReportUserDtos.get(0).getStaffName());
//        govReportUserPo.setStartTime(DateUtil.getNow(DateUtil.DATE_FORMATE_STRING_A));
//        govReportUserPo.setPreStaffId(reqJson.getString("staffId"));
//        govReportUserPo.setPreStaffName(reqJson.getString("userName"));
//        govReportUserPo.setPreRuId(govReportUserDtoList.get(0).getRuId());
//        govReportUserPo.setReportEvent(GovReportUserDto.POOL_EVENT_AUDIT_USER);
//        govReportUserPo.setContext("");
//        govReportUserPo.setCaId(reqJson.getString("caId"));
//        flag = govReportUserInnerServiceSMOImpl.saveGovReportUser(govReportUserPo);
//        if (flag < 1) {
//            throw new IllegalArgumentException("保存待评价订单失败");
//        }
        return ResultVo.success();
    }
}
