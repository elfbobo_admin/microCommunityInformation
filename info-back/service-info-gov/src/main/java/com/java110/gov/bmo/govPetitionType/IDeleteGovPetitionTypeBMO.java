package com.java110.gov.bmo.govPetitionType;
import org.springframework.http.ResponseEntity;
import com.java110.po.govPetitionType.GovPetitionTypePo;

public interface IDeleteGovPetitionTypeBMO {


    /**
     * 修改信访类型表
     * add by wuxw
     * @param govPetitionTypePo
     * @return
     */
    ResponseEntity<String> delete(GovPetitionTypePo govPetitionTypePo);


}
