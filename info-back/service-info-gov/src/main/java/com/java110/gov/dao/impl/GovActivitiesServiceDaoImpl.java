package com.java110.gov.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.gov.dao.IGovActivitiesServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 公告管理服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govActivitiesServiceDaoImpl")
//@Transactional
public class GovActivitiesServiceDaoImpl extends BaseServiceDao implements IGovActivitiesServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovActivitiesServiceDaoImpl.class);





    /**
     * 保存公告管理信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovActivitiesInfo(Map info) throws DAOException {
        logger.debug("保存公告管理信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govActivitiesServiceDaoImpl.saveGovActivitiesInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存公告管理信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询公告管理信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovActivitiesInfo(Map info) throws DAOException {
        logger.debug("查询公告管理信息 入参 info : {}",info);

        List<Map> businessGovActivitiesInfos = sqlSessionTemplate.selectList("govActivitiesServiceDaoImpl.getGovActivitiesInfo",info);

        return businessGovActivitiesInfos;
    }


    /**
     * 修改公告管理信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovActivitiesInfo(Map info) throws DAOException {
        logger.debug("修改公告管理信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govActivitiesServiceDaoImpl.updateGovActivitiesInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改公告管理信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询公告管理数量
     * @param info 公告管理信息
     * @return 公告管理数量
     */
    @Override
    public int queryGovActivitiessCount(Map info) {
        logger.debug("查询公告管理数据 入参 info : {}",info);

        List<Map> businessGovActivitiesInfos = sqlSessionTemplate.selectList("govActivitiesServiceDaoImpl.queryGovActivitiessCount", info);
        if (businessGovActivitiesInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovActivitiesInfos.get(0).get("count").toString());
    }


}
