package com.java110.gov.smo.impl;


import com.java110.gov.dao.IGovReportUserServiceDao;
import com.java110.dto.govReportUser.GovReportUserDto;
import com.java110.intf.gov.IGovReportUserInnerServiceSMO;
import com.java110.po.govReportUser.GovReportUserPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 报事类型人员内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovReportUserInnerServiceSMOImpl extends BaseServiceSMO implements IGovReportUserInnerServiceSMO {

    @Autowired
    private IGovReportUserServiceDao govReportUserServiceDaoImpl;


    @Override
    public int saveGovReportUser(@RequestBody GovReportUserPo govReportUserPo) {
        int saveFlag = 1;
        govReportUserServiceDaoImpl.saveGovReportUserInfo(BeanConvertUtil.beanCovertMap(govReportUserPo));
        return saveFlag;
    }

     @Override
    public int updateGovReportUser(@RequestBody  GovReportUserPo govReportUserPo) {
        int saveFlag = 1;
         govReportUserServiceDaoImpl.updateGovReportUserInfo(BeanConvertUtil.beanCovertMap(govReportUserPo));
        return saveFlag;
    }

     @Override
    public int deleteGovReportUser(@RequestBody  GovReportUserPo govReportUserPo) {
        int saveFlag = 1;
        govReportUserPo.setStatusCd("1");
        govReportUserServiceDaoImpl.updateGovReportUserInfo(BeanConvertUtil.beanCovertMap(govReportUserPo));
        return saveFlag;
    }

    @Override
    public List<GovReportUserDto> queryGovReportUsers(@RequestBody  GovReportUserDto govReportUserDto) {

        //校验是否传了 分页信息

        int page = govReportUserDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govReportUserDto.setPage((page - 1) * govReportUserDto.getRow());
        }

        List<GovReportUserDto> govReportUsers = BeanConvertUtil.covertBeanList(govReportUserServiceDaoImpl.getGovReportUserInfo(BeanConvertUtil.beanCovertMap(govReportUserDto)), GovReportUserDto.class);

        return govReportUsers;
    }


    @Override
    public int queryGovReportUsersCount(@RequestBody GovReportUserDto govReportUserDto) {
        return govReportUserServiceDaoImpl.queryGovReportUsersCount(BeanConvertUtil.beanCovertMap(govReportUserDto));    }

    public IGovReportUserServiceDao getGovReportUserServiceDaoImpl() {
        return govReportUserServiceDaoImpl;
    }

    public void setGovReportUserServiceDaoImpl(IGovReportUserServiceDao govReportUserServiceDaoImpl) {
        this.govReportUserServiceDaoImpl = govReportUserServiceDaoImpl;
    }
}
