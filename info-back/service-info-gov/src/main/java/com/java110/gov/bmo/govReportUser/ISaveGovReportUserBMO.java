package com.java110.gov.bmo.govReportUser;

import com.alibaba.fastjson.JSONObject;
import com.java110.po.govReportUser.GovReportUserPo;
import org.springframework.http.ResponseEntity;
public interface ISaveGovReportUserBMO {


    /**
     * 添加报事类型人员
     * add by wuxw
     * @param govReportUserPo
     * @return
     */
    ResponseEntity<String> save(GovReportUserPo govReportUserPo);


    /**
     * 工单处理
     * @param reqJson
     * @return
     */
    ResponseEntity<String> poolDispatch(JSONObject reqJson);
    /**
     * 工单处理
     * @param reqJson
     * @return
     */
    ResponseEntity<String> poolFinish(JSONObject reqJson);
    /**
     * 工单处理 处理待评价
     * @param reqJson
     * @return
     */
    ResponseEntity<String> poolFinishHui(JSONObject reqJson);
}
