package com.java110.gov.smo.impl;


import com.java110.gov.dao.IGovActivityLikesServiceDao;
import com.java110.intf.gov.IGovActivityLikesInnerServiceSMO;
import com.java110.dto.govActivityLikes.GovActivityLikesDto;
import com.java110.po.govActivityLikes.GovActivityLikesPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 活动点赞内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovActivityLikesInnerServiceSMOImpl extends BaseServiceSMO implements IGovActivityLikesInnerServiceSMO {

    @Autowired
    private IGovActivityLikesServiceDao govActivityLikesServiceDaoImpl;


    @Override
    public int saveGovActivityLikes(@RequestBody  GovActivityLikesPo govActivityLikesPo) {
        int saveFlag = 1;
        govActivityLikesServiceDaoImpl.saveGovActivityLikesInfo(BeanConvertUtil.beanCovertMap(govActivityLikesPo));
        return saveFlag;
    }

     @Override
    public int updateGovActivityLikes(@RequestBody  GovActivityLikesPo govActivityLikesPo) {
        int saveFlag = 1;
         govActivityLikesServiceDaoImpl.updateGovActivityLikesInfo(BeanConvertUtil.beanCovertMap(govActivityLikesPo));
        return saveFlag;
    }

     @Override
    public int deleteGovActivityLikes(@RequestBody  GovActivityLikesPo govActivityLikesPo) {
        int saveFlag = 1;
        govActivityLikesPo.setStatusCd("1");
        govActivityLikesServiceDaoImpl.updateGovActivityLikesInfo(BeanConvertUtil.beanCovertMap(govActivityLikesPo));
        return saveFlag;
    }

    @Override
    public List<GovActivityLikesDto> queryGovActivityLikess(@RequestBody  GovActivityLikesDto govActivityLikesDto) {

        //校验是否传了 分页信息

        int page = govActivityLikesDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govActivityLikesDto.setPage((page - 1) * govActivityLikesDto.getRow());
        }

        List<GovActivityLikesDto> govActivityLikess = BeanConvertUtil.covertBeanList(govActivityLikesServiceDaoImpl.getGovActivityLikesInfo(BeanConvertUtil.beanCovertMap(govActivityLikesDto)), GovActivityLikesDto.class);

        return govActivityLikess;
    }


    @Override
    public int queryGovActivityLikessCount(@RequestBody GovActivityLikesDto govActivityLikesDto) {
        return govActivityLikesServiceDaoImpl.queryGovActivityLikessCount(BeanConvertUtil.beanCovertMap(govActivityLikesDto));    }

    public IGovActivityLikesServiceDao getGovActivityLikesServiceDaoImpl() {
        return govActivityLikesServiceDaoImpl;
    }

    public void setGovActivityLikesServiceDaoImpl(IGovActivityLikesServiceDao govActivityLikesServiceDaoImpl) {
        this.govActivityLikesServiceDaoImpl = govActivityLikesServiceDaoImpl;
    }
}
