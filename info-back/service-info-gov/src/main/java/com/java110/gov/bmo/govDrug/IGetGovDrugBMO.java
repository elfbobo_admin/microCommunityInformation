package com.java110.gov.bmo.govDrug;
import org.springframework.http.ResponseEntity;
import com.java110.dto.govDrug.GovDrugDto;
public interface IGetGovDrugBMO {


    /**
     * 查询吸毒者
     * add by wuxw
     * @param  govDrugDto
     * @return
     */
    ResponseEntity<String> get(GovDrugDto govDrugDto);


}
