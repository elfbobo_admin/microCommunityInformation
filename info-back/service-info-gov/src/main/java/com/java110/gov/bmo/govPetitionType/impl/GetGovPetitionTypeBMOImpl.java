package com.java110.gov.bmo.govPetitionType.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.gov.bmo.govPetitionType.IGetGovPetitionTypeBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.gov.IGovPetitionTypeInnerServiceSMO;
import com.java110.dto.govPetitionType.GovPetitionTypeDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import org.springframework.http.HttpStatus;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service("getGovPetitionTypeBMOImpl")
public class GetGovPetitionTypeBMOImpl implements IGetGovPetitionTypeBMO {

    @Autowired
    private IGovPetitionTypeInnerServiceSMO govPetitionTypeInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govPetitionTypeDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovPetitionTypeDto govPetitionTypeDto) {


        int count = govPetitionTypeInnerServiceSMOImpl.queryGovPetitionTypesCount(govPetitionTypeDto);

        List<GovPetitionTypeDto> govPetitionTypeDtos = null;
        if (count > 0) {
            govPetitionTypeDtos = govPetitionTypeInnerServiceSMOImpl.queryGovPetitionTypes(govPetitionTypeDto);
        } else {
            govPetitionTypeDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govPetitionTypeDto.getRow()), count, govPetitionTypeDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
