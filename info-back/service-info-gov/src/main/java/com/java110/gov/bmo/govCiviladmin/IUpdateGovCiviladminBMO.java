package com.java110.gov.bmo.govCiviladmin;
import com.java110.po.govCiviladmin.GovCiviladminPo;
import org.springframework.http.ResponseEntity;

public interface IUpdateGovCiviladminBMO {


    /**
     * 修改民政服务宣传
     * add by wuxw
     * @param govCiviladminPo
     * @return
     */
    ResponseEntity<String> update(GovCiviladminPo govCiviladminPo);


}
