package com.java110.gov.bmo.govCase;
import org.springframework.http.ResponseEntity;
import com.java110.dto.govCase.GovCaseDto;
public interface IGetGovCaseBMO {


    /**
     * 查询命案基本信息
     * add by wuxw
     * @param  govCaseDto
     * @return
     */
    ResponseEntity<String> get(GovCaseDto govCaseDto);


}
