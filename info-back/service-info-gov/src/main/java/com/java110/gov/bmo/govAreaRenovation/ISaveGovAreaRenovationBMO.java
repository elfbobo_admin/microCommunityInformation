package com.java110.gov.bmo.govAreaRenovation;

import org.springframework.http.ResponseEntity;
import com.java110.po.govAreaRenovation.GovAreaRenovationPo;
public interface ISaveGovAreaRenovationBMO {


    /**
     * 添加重点地区排查整治
     * add by wuxw
     * @param govAreaRenovationPo
     * @return
     */
    ResponseEntity<String> save(GovAreaRenovationPo govAreaRenovationPo);


}
