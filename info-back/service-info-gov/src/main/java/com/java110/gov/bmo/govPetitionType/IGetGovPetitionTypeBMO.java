package com.java110.gov.bmo.govPetitionType;
import org.springframework.http.ResponseEntity;
import com.java110.dto.govPetitionType.GovPetitionTypeDto;
public interface IGetGovPetitionTypeBMO {


    /**
     * 查询信访类型表
     * add by wuxw
     * @param  govPetitionTypeDto
     * @return
     */
    ResponseEntity<String> get(GovPetitionTypeDto govPetitionTypeDto);


}
