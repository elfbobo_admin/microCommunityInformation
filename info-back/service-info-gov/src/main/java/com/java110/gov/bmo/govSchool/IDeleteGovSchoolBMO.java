package com.java110.gov.bmo.govSchool;
import org.springframework.http.ResponseEntity;
import com.java110.po.govSchool.GovSchoolPo;

public interface IDeleteGovSchoolBMO {


    /**
     * 修改学校
     * add by wuxw
     * @param govSchoolPo
     * @return
     */
    ResponseEntity<String> delete(GovSchoolPo govSchoolPo);


}
