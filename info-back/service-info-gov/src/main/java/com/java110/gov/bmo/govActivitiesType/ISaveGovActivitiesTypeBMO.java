package com.java110.gov.bmo.govActivitiesType;

import com.java110.po.govActivitiesType.GovActivitiesTypePo;
import org.springframework.http.ResponseEntity;
public interface ISaveGovActivitiesTypeBMO {


    /**
     * 添加公告类型
     * add by wuxw
     * @param govActivitiesTypePo
     * @return
     */
    ResponseEntity<String> save(GovActivitiesTypePo govActivitiesTypePo);


}
