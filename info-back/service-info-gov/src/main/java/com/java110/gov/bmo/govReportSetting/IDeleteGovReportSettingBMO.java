package com.java110.gov.bmo.govReportSetting;
import com.java110.po.govReportSetting.GovReportSettingPo;
import org.springframework.http.ResponseEntity;

public interface IDeleteGovReportSettingBMO {


    /**
     * 修改报事设置
     * add by wuxw
     * @param govReportSettingPo
     * @return
     */
    ResponseEntity<String> delete(GovReportSettingPo govReportSettingPo);


}
