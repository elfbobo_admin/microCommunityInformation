package com.java110.gov.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govActivityPerson.GovActivityPersonDto;
import com.java110.gov.bmo.govActivityPerson.IDeleteGovActivityPersonBMO;
import com.java110.gov.bmo.govActivityPerson.IGetGovActivityPersonBMO;
import com.java110.gov.bmo.govActivityPerson.ISaveGovActivityPersonBMO;
import com.java110.gov.bmo.govActivityPerson.IUpdateGovActivityPersonBMO;
import com.java110.po.govActivityPerson.GovActivityPersonPo;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(value = "/govActivityPerson")
public class GovActivityPersonApi {

    @Autowired
    private ISaveGovActivityPersonBMO saveGovActivityPersonBMOImpl;
    @Autowired
    private IUpdateGovActivityPersonBMO updateGovActivityPersonBMOImpl;
    @Autowired
    private IDeleteGovActivityPersonBMO deleteGovActivityPersonBMOImpl;

    @Autowired
    private IGetGovActivityPersonBMO getGovActivityPersonBMOImpl;

    /**
     * 微信保存消息模板
     * @serviceCode /govActivityPerson/saveGovActivityPerson
     * @path /app/govActivityPerson/saveGovActivityPerson
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/saveGovActivityPerson", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovActivityPerson(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "actId", "请求报文中未包含actId");
        Assert.hasKeyAndValue(reqJson, "personName", "请求报文中未包含personName");
        Assert.hasKeyAndValue(reqJson, "personLink", "请求报文中未包含personLink");
        Assert.hasKeyAndValue(reqJson, "personAge", "请求报文中未包含personAge");
        Assert.hasKeyAndValue(reqJson, "personAddress", "请求报文中未包含personAddress");


        GovActivityPersonPo govActivityPersonPo = BeanConvertUtil.covertBean(reqJson, GovActivityPersonPo.class);
        return saveGovActivityPersonBMOImpl.save(govActivityPersonPo);
    }

    /**
     * 微信修改消息模板
     * @serviceCode /govActivityPerson/updateGovActivityPerson
     * @path /app/govActivityPerson/updateGovActivityPerson
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/updateGovActivityPerson", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovActivityPerson(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "actId", "请求报文中未包含actId");
        Assert.hasKeyAndValue(reqJson, "personName", "请求报文中未包含personName");
        Assert.hasKeyAndValue(reqJson, "personLink", "请求报文中未包含personLink");
        Assert.hasKeyAndValue(reqJson, "personAge", "请求报文中未包含personAge");
        Assert.hasKeyAndValue(reqJson, "personAddress", "请求报文中未包含personAddress");
        Assert.hasKeyAndValue(reqJson, "actPerId", "actPerId不能为空");


        GovActivityPersonPo govActivityPersonPo = BeanConvertUtil.covertBean(reqJson, GovActivityPersonPo.class);
        return updateGovActivityPersonBMOImpl.update(govActivityPersonPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govActivityPerson/deleteGovActivityPerson
     * @path /app/govActivityPerson/deleteGovActivityPerson
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/deleteGovActivityPerson", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovActivityPerson(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "actPerId", "actPerId不能为空");


        GovActivityPersonPo govActivityPersonPo = BeanConvertUtil.covertBean(reqJson, GovActivityPersonPo.class);
        return deleteGovActivityPersonBMOImpl.delete(govActivityPersonPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govActivityPerson/queryGovActivityPerson
     * @path /app/govActivityPerson/queryGovActivityPerson
     * @param caId 小区ID
     * @return
     */
    @RequestMapping(value = "/queryGovActivityPerson", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovActivityPerson(@RequestParam(value = "caId") String caId,
                                                         @RequestParam(value = "personName" , required = false) String personName,
                                                         @RequestParam(value = "personLink" , required = false) String personLink,
                                                         @RequestParam(value = "page") int page,
                                                         @RequestParam(value = "row") int row) {
        GovActivityPersonDto govActivityPersonDto = new GovActivityPersonDto();
        govActivityPersonDto.setPage(page);
        govActivityPersonDto.setRow(row);
        govActivityPersonDto.setPersonName(personName);
        govActivityPersonDto.setPersonLink(personLink);
        return getGovActivityPersonBMOImpl.get(govActivityPersonDto);
    }
}
