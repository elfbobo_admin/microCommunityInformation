package com.java110.gov.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.gov.dao.IGovMeetingMemberRelServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 会议与参会人关系服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govMeetingMemberRelServiceDaoImpl")
//@Transactional
public class GovMeetingMemberRelServiceDaoImpl extends BaseServiceDao implements IGovMeetingMemberRelServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovMeetingMemberRelServiceDaoImpl.class);





    /**
     * 保存会议与参会人关系信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovMeetingMemberRelInfo(Map info) throws DAOException {
        logger.debug("保存会议与参会人关系信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govMeetingMemberRelServiceDaoImpl.saveGovMeetingMemberRelInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存会议与参会人关系信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询会议与参会人关系信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovMeetingMemberRelInfo(Map info) throws DAOException {
        logger.debug("查询会议与参会人关系信息 入参 info : {}",info);

        List<Map> businessGovMeetingMemberRelInfos = sqlSessionTemplate.selectList("govMeetingMemberRelServiceDaoImpl.getGovMeetingMemberRelInfo",info);

        return businessGovMeetingMemberRelInfos;
    }


    /**
     * 修改会议与参会人关系信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovMeetingMemberRelInfo(Map info) throws DAOException {
        logger.debug("修改会议与参会人关系信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govMeetingMemberRelServiceDaoImpl.updateGovMeetingMemberRelInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改会议与参会人关系信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询会议与参会人关系数量
     * @param info 会议与参会人关系信息
     * @return 会议与参会人关系数量
     */
    @Override
    public int queryGovMeetingMemberRelsCount(Map info) {
        logger.debug("查询会议与参会人关系数据 入参 info : {}",info);

        List<Map> businessGovMeetingMemberRelInfos = sqlSessionTemplate.selectList("govMeetingMemberRelServiceDaoImpl.queryGovMeetingMemberRelsCount", info);
        if (businessGovMeetingMemberRelInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovMeetingMemberRelInfos.get(0).get("count").toString());
    }


}
