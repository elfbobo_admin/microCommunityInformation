package com.java110.gov.bmo.govSchool.impl;

import com.java110.dto.cityArea.CityAreaDto;
import com.java110.dto.govSchool.GovSchoolDto;
import com.java110.gov.bmo.govSchool.IGetGovSchoolBMO;
import com.java110.intf.assets.ICityAreaInnerServiceSMO;
import com.java110.intf.gov.IGovSchoolInnerServiceSMO;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("getGovSchoolBMOImpl")
public class GetGovSchoolBMOImpl implements IGetGovSchoolBMO {

    @Autowired
    private IGovSchoolInnerServiceSMO govSchoolInnerServiceSMOImpl;

    @Autowired
    private ICityAreaInnerServiceSMO cityAreaInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govSchoolDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovSchoolDto govSchoolDto) {


        int count = govSchoolInnerServiceSMOImpl.queryGovSchoolsCount(govSchoolDto);

        List<GovSchoolDto> govSchoolDtos = null;
        if (count > 0) {
            govSchoolDtos = govSchoolInnerServiceSMOImpl.queryGovSchools(govSchoolDto);
            reFreshAreaCode(govSchoolDtos);
        } else {
            govSchoolDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govSchoolDto.getRow()), count, govSchoolDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

    /**
     * @param govSchoolDtos
     */
    private void reFreshAreaCode(List<GovSchoolDto> govSchoolDtos) {
        for (GovSchoolDto schoolDto : govSchoolDtos) {
            CityAreaDto cityAreaDto = new CityAreaDto();
            cityAreaDto.setAreaCode(schoolDto.getAreaCode());
            List<CityAreaDto> cityAreaDtos = cityAreaInnerServiceSMOImpl.queryCityAreas(cityAreaDto);
                if (cityAreaDtos != null && cityAreaDtos.size() > 0) {
                    schoolDto.setAreaName(cityAreaDtos.get(0).getAreaName());
                }
        }
    }
}
