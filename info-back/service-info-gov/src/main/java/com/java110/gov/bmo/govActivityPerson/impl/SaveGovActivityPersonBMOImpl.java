package com.java110.gov.bmo.govActivityPerson.impl;

import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.gov.bmo.govActivityPerson.ISaveGovActivityPersonBMO;
import com.java110.intf.gov.IGovActivityPersonInnerServiceSMO;
import com.java110.po.govActivityPerson.GovActivityPersonPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("saveGovActivityPersonBMOImpl")
public class SaveGovActivityPersonBMOImpl implements ISaveGovActivityPersonBMO {

    @Autowired
    private IGovActivityPersonInnerServiceSMO govActivityPersonInnerServiceSMOImpl;

    /**
     * 添加小区信息
     *
     * @param govActivityPersonPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> save(GovActivityPersonPo govActivityPersonPo) {

        govActivityPersonPo.setActPerId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_actPerId));
        int flag = govActivityPersonInnerServiceSMOImpl.saveGovActivityPerson(govActivityPersonPo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
