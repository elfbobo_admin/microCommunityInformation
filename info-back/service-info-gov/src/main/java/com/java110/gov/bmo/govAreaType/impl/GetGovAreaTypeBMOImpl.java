package com.java110.gov.bmo.govAreaType.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.gov.bmo.govAreaType.IGetGovAreaTypeBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.gov.IGovAreaTypeInnerServiceSMO;
import com.java110.dto.govAreaType.GovAreaTypeDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import org.springframework.http.HttpStatus;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service("getGovAreaTypeBMOImpl")
public class GetGovAreaTypeBMOImpl implements IGetGovAreaTypeBMO {

    @Autowired
    private IGovAreaTypeInnerServiceSMO govAreaTypeInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govAreaTypeDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovAreaTypeDto govAreaTypeDto) {


        int count = govAreaTypeInnerServiceSMOImpl.queryGovAreaTypesCount(govAreaTypeDto);

        List<GovAreaTypeDto> govAreaTypeDtos = null;
        if (count > 0) {
            govAreaTypeDtos = govAreaTypeInnerServiceSMOImpl.queryGovAreaTypes(govAreaTypeDto);
        } else {
            govAreaTypeDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govAreaTypeDto.getRow()), count, govAreaTypeDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
