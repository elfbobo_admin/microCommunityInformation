package com.java110.gov.bmo.govCarInout.impl;

import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.gov.bmo.govCarInout.ISaveGovCarInoutBMO;
import com.java110.intf.gov.IGovCarInoutInnerServiceSMO;
import com.java110.po.govCarInout.GovCarInoutPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("saveGovCarInoutBMOImpl")
public class SaveGovCarInoutBMOImpl implements ISaveGovCarInoutBMO {

    @Autowired
    private IGovCarInoutInnerServiceSMO govCarInoutInnerServiceSMOImpl;

    /**
     * 添加小区信息
     *
     * @param govCarInoutPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> save(GovCarInoutPo govCarInoutPo) {

        govCarInoutPo.setInoutId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_inoutId));
        int flag = govCarInoutInnerServiceSMOImpl.saveGovCarInout(govCarInoutPo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
