package com.java110.gov.bmo.govActivity;
import com.java110.dto.govActivity.GovActivityDto;
import org.springframework.http.ResponseEntity;
public interface IGetGovActivityBMO {


    /**
     * 查询活动
     * add by wuxw
     * @param  govActivityDto
     * @return
     */
    ResponseEntity<String> get(GovActivityDto govActivityDto);


}
