package com.java110.gov.bmo.govSpecialFollow;
import org.springframework.http.ResponseEntity;
import com.java110.po.govSpecialFollow.GovSpecialFollowPo;

public interface IDeleteGovSpecialFollowBMO {


    /**
     * 修改特殊人员跟进记录
     * add by wuxw
     * @param govSpecialFollowPo
     * @return
     */
    ResponseEntity<String> delete(GovSpecialFollowPo govSpecialFollowPo);


}
