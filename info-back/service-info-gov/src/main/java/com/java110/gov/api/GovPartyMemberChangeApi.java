package com.java110.gov.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govPartyMemberChange.GovPartyMemberChangeDto;
import com.java110.gov.bmo.govPartyMemberChange.IDeleteGovPartyMemberChangeBMO;
import com.java110.gov.bmo.govPartyMemberChange.IGetGovPartyMemberChangeBMO;
import com.java110.gov.bmo.govPartyMemberChange.ISaveGovPartyMemberChangeBMO;
import com.java110.gov.bmo.govPartyMemberChange.IUpdateGovPartyMemberChangeBMO;
import com.java110.po.govPartyMemberChange.GovPartyMemberChangePo;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govPartyMemberChange")
public class GovPartyMemberChangeApi {

    @Autowired
    private ISaveGovPartyMemberChangeBMO saveGovPartyMemberChangeBMOImpl;
    @Autowired
    private IUpdateGovPartyMemberChangeBMO updateGovPartyMemberChangeBMOImpl;
    @Autowired
    private IDeleteGovPartyMemberChangeBMO deleteGovPartyMemberChangeBMOImpl;

    @Autowired
    private IGetGovPartyMemberChangeBMO getGovPartyMemberChangeBMOImpl;

    /**
     * 微信保存消息模板
     * @serviceCode /govPartyMemberChange/saveGovPartyMemberChange
     * @path /app/govPartyMemberChange/saveGovPartyMemberChange
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/saveGovPartyMemberChange", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovPartyMemberChange(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "govMemberId", "请求报文中未包含govMemberId");
        Assert.hasKeyAndValue(reqJson, "personName", "请求报文中未包含personName");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "srcOrgId", "请求报文中未包含srcOrgId");
        Assert.hasKeyAndValue(reqJson, "srcOrgName", "请求报文中未包含srcOrgName");
        Assert.hasKeyAndValue(reqJson, "targetOrgId", "请求报文中未包含targetOrgId");
        Assert.hasKeyAndValue(reqJson, "targetOrgName", "请求报文中未包含targetOrgName");
        Assert.hasKeyAndValue(reqJson, "chagneType", "请求报文中未包含chagneType");
        Assert.hasKeyAndValue(reqJson, "state", "请求报文中未包含state");


        GovPartyMemberChangePo govPartyMemberChangePo = BeanConvertUtil.covertBean(reqJson, GovPartyMemberChangePo.class);
        return saveGovPartyMemberChangeBMOImpl.save(govPartyMemberChangePo);
    }

    /**
     * 微信修改消息模板
     * @serviceCode /govPartyMemberChange/updateGovPartyMemberChange
     * @path /app/govPartyMemberChange/updateGovPartyMemberChange
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/updateGovPartyMemberChange", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovPartyMemberChange(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "govMemberId", "请求报文中未包含govMemberId");
        Assert.hasKeyAndValue(reqJson, "personName", "请求报文中未包含personName");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "srcOrgId", "请求报文中未包含srcOrgId");
        Assert.hasKeyAndValue(reqJson, "srcOrgName", "请求报文中未包含srcOrgName");
        Assert.hasKeyAndValue(reqJson, "targetOrgId", "请求报文中未包含targetOrgId");
        Assert.hasKeyAndValue(reqJson, "targetOrgName", "请求报文中未包含targetOrgName");
        Assert.hasKeyAndValue(reqJson, "chagneType", "请求报文中未包含chagneType");
        Assert.hasKeyAndValue(reqJson, "state", "请求报文中未包含state");
        Assert.hasKeyAndValue(reqJson, "govChangeId", "govChangeId不能为空");


        GovPartyMemberChangePo govPartyMemberChangePo = BeanConvertUtil.covertBean(reqJson, GovPartyMemberChangePo.class);
        return updateGovPartyMemberChangeBMOImpl.update(govPartyMemberChangePo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govPartyMemberChange/deleteGovPartyMemberChange
     * @path /app/govPartyMemberChange/deleteGovPartyMemberChange
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/deleteGovPartyMemberChange", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovPartyMemberChange(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "govChangeId", "govChangeId不能为空");


        GovPartyMemberChangePo govPartyMemberChangePo = BeanConvertUtil.covertBean(reqJson, GovPartyMemberChangePo.class);
        return deleteGovPartyMemberChangeBMOImpl.delete(govPartyMemberChangePo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govPartyMemberChange/queryGovPartyMemberChange
     * @path /app/govPartyMemberChange/queryGovPartyMemberChange
     * @param
     * @return
     */
    @RequestMapping(value = "/queryGovPartyMemberChange", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovPartyMemberChange(@RequestParam(value = "caId" , required = false) String caId,
                                                      @RequestParam(value = "govMemberId" , required = false) String govMemberId,
                                                      @RequestParam(value = "chagneType" , required = false) String chagneType,
                                                      @RequestParam(value = "state" , required = false) String state,
                                                      @RequestParam(value = "page") int page,
                                                      @RequestParam(value = "row") int row) {
        GovPartyMemberChangeDto govPartyMemberChangeDto = new GovPartyMemberChangeDto();
        govPartyMemberChangeDto.setPage(page);
        govPartyMemberChangeDto.setRow(row);
        govPartyMemberChangeDto.setCaId(caId);
        govPartyMemberChangeDto.setGovMemberId(govMemberId);
        govPartyMemberChangeDto.setChagneType(chagneType);
        govPartyMemberChangeDto.setState(state);
        return getGovPartyMemberChangeBMOImpl.get(govPartyMemberChangeDto);
    }
}
