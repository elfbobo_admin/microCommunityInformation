package com.java110.gov.smo.impl;


import com.java110.gov.dao.IGovRoadProtectionCaseServiceDao;
import com.java110.intf.gov.IGovRoadProtectionCaseInnerServiceSMO;
import com.java110.dto.govRoadProtectionCase.GovRoadProtectionCaseDto;
import com.java110.po.govRoadProtectionCase.GovRoadProtectionCasePo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 路案事件内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovRoadProtectionCaseInnerServiceSMOImpl extends BaseServiceSMO implements IGovRoadProtectionCaseInnerServiceSMO {

    @Autowired
    private IGovRoadProtectionCaseServiceDao govRoadProtectionCaseServiceDaoImpl;


    @Override
    public int saveGovRoadProtectionCase(@RequestBody  GovRoadProtectionCasePo govRoadProtectionCasePo) {
        int saveFlag = 1;
        govRoadProtectionCaseServiceDaoImpl.saveGovRoadProtectionCaseInfo(BeanConvertUtil.beanCovertMap(govRoadProtectionCasePo));
        return saveFlag;
    }

     @Override
    public int updateGovRoadProtectionCase(@RequestBody  GovRoadProtectionCasePo govRoadProtectionCasePo) {
        int saveFlag = 1;
         govRoadProtectionCaseServiceDaoImpl.updateGovRoadProtectionCaseInfo(BeanConvertUtil.beanCovertMap(govRoadProtectionCasePo));
        return saveFlag;
    }

     @Override
    public int deleteGovRoadProtectionCase(@RequestBody  GovRoadProtectionCasePo govRoadProtectionCasePo) {
        int saveFlag = 1;
        govRoadProtectionCasePo.setStatusCd("1");
        govRoadProtectionCaseServiceDaoImpl.updateGovRoadProtectionCaseInfo(BeanConvertUtil.beanCovertMap(govRoadProtectionCasePo));
        return saveFlag;
    }

    @Override
    public List<GovRoadProtectionCaseDto> queryGovRoadProtectionCases(@RequestBody  GovRoadProtectionCaseDto govRoadProtectionCaseDto) {

        //校验是否传了 分页信息

        int page = govRoadProtectionCaseDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govRoadProtectionCaseDto.setPage((page - 1) * govRoadProtectionCaseDto.getRow());
        }

        List<GovRoadProtectionCaseDto> govRoadProtectionCases = BeanConvertUtil.covertBeanList(govRoadProtectionCaseServiceDaoImpl.getGovRoadProtectionCaseInfo(BeanConvertUtil.beanCovertMap(govRoadProtectionCaseDto)), GovRoadProtectionCaseDto.class);

        return govRoadProtectionCases;
    }


    @Override
    public int queryGovRoadProtectionCasesCount(@RequestBody GovRoadProtectionCaseDto govRoadProtectionCaseDto) {
        return govRoadProtectionCaseServiceDaoImpl.queryGovRoadProtectionCasesCount(BeanConvertUtil.beanCovertMap(govRoadProtectionCaseDto));    }

    public IGovRoadProtectionCaseServiceDao getGovRoadProtectionCaseServiceDaoImpl() {
        return govRoadProtectionCaseServiceDaoImpl;
    }

    public void setGovRoadProtectionCaseServiceDaoImpl(IGovRoadProtectionCaseServiceDao govRoadProtectionCaseServiceDaoImpl) {
        this.govRoadProtectionCaseServiceDaoImpl = govRoadProtectionCaseServiceDaoImpl;
    }
}
