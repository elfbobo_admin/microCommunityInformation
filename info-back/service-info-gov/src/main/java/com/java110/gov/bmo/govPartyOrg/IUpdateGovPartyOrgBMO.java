package com.java110.gov.bmo.govPartyOrg;
import com.java110.po.govPartyOrg.GovPartyOrgPo;
import org.springframework.http.ResponseEntity;

public interface IUpdateGovPartyOrgBMO {


    /**
     * 修改党组织
     * add by wuxw
     * @param govPartyOrgPo
     * @return
     */
    ResponseEntity<String> update(GovPartyOrgPo govPartyOrgPo);


}
