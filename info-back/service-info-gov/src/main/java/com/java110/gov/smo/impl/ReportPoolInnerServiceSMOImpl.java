package com.java110.gov.smo.impl;


import com.java110.gov.dao.IReportPoolServiceDao;
import com.java110.dto.reportPool.ReportPoolDto;
import com.java110.intf.gov.IReportPoolInnerServiceSMO;
import com.java110.po.reportPool.ReportPoolPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 报事管理内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class ReportPoolInnerServiceSMOImpl extends BaseServiceSMO implements IReportPoolInnerServiceSMO {

    @Autowired
    private IReportPoolServiceDao reportPoolServiceDaoImpl;


    @Override
    public int saveReportPool(@RequestBody ReportPoolPo reportPoolPo) {
        int saveFlag = 1;
        reportPoolServiceDaoImpl.saveReportPoolInfo(BeanConvertUtil.beanCovertMap(reportPoolPo));
        return saveFlag;
    }

     @Override
    public int updateReportPool(@RequestBody  ReportPoolPo reportPoolPo) {
        int saveFlag = 1;
         reportPoolServiceDaoImpl.updateReportPoolInfo(BeanConvertUtil.beanCovertMap(reportPoolPo));
        return saveFlag;
    }

     @Override
    public int deleteReportPool(@RequestBody  ReportPoolPo reportPoolPo) {
        int saveFlag = 1;
        reportPoolPo.setStatusCd("1");
        reportPoolServiceDaoImpl.updateReportPoolInfo(BeanConvertUtil.beanCovertMap(reportPoolPo));
        return saveFlag;
    }

    @Override
    public List<ReportPoolDto> queryReportPools(@RequestBody  ReportPoolDto reportPoolDto) {

        //校验是否传了 分页信息

        int page = reportPoolDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            reportPoolDto.setPage((page - 1) * reportPoolDto.getRow());
        }

        List<ReportPoolDto> reportPools = BeanConvertUtil.covertBeanList(reportPoolServiceDaoImpl.getReportPoolInfo(BeanConvertUtil.beanCovertMap(reportPoolDto)), ReportPoolDto.class);

        return reportPools;
    }


    @Override
    public int queryReportPoolsCount(@RequestBody ReportPoolDto reportPoolDto) {
        return reportPoolServiceDaoImpl.queryReportPoolsCount(BeanConvertUtil.beanCovertMap(reportPoolDto));    }

    @Override
    public List<ReportPoolDto> quseryReportStaffs(@RequestBody  ReportPoolDto reportPoolDto) {

        //校验是否传了 分页信息

        int page = reportPoolDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            reportPoolDto.setPage((page - 1) * reportPoolDto.getRow());
        }

        List<ReportPoolDto> reportPools = BeanConvertUtil.covertBeanList(reportPoolServiceDaoImpl.quseryReportStaffs(BeanConvertUtil.beanCovertMap(reportPoolDto)), ReportPoolDto.class);

        return reportPools;
    }


    @Override
    public int quseryReportStaffCount(@RequestBody ReportPoolDto reportPoolDto) {
        return reportPoolServiceDaoImpl.quseryReportStaffCount(BeanConvertUtil.beanCovertMap(reportPoolDto));    }
    @Override
    public List<ReportPoolDto> queryStaffFinishReports(@RequestBody  ReportPoolDto reportPoolDto) {

        //校验是否传了 分页信息

        int page = reportPoolDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            reportPoolDto.setPage((page - 1) * reportPoolDto.getRow());
        }

        List<ReportPoolDto> reportPools = BeanConvertUtil.covertBeanList(reportPoolServiceDaoImpl.queryStaffFinishReports(BeanConvertUtil.beanCovertMap(reportPoolDto)), ReportPoolDto.class);

        return reportPools;
    }


    @Override
    public int queryStaffFinishReportCount(@RequestBody ReportPoolDto reportPoolDto) {
        return reportPoolServiceDaoImpl.queryStaffFinishReportCount(BeanConvertUtil.beanCovertMap(reportPoolDto));    }
    @Override
    public List<ReportPoolDto> quseryReportFinishStaffs(@RequestBody  ReportPoolDto reportPoolDto) {

        //校验是否传了 分页信息

        int page = reportPoolDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            reportPoolDto.setPage((page - 1) * reportPoolDto.getRow());
        }

        List<ReportPoolDto> reportPools = BeanConvertUtil.covertBeanList(reportPoolServiceDaoImpl.quseryReportFinishStaffs(BeanConvertUtil.beanCovertMap(reportPoolDto)), ReportPoolDto.class);

        return reportPools;
    }


    @Override
    public int quseryReportFinishStaffCount(@RequestBody ReportPoolDto reportPoolDto) {
        return reportPoolServiceDaoImpl.quseryReportFinishStaffCount(BeanConvertUtil.beanCovertMap(reportPoolDto));    }

    public IReportPoolServiceDao getReportPoolServiceDaoImpl() {
        return reportPoolServiceDaoImpl;
    }

    public void setReportPoolServiceDaoImpl(IReportPoolServiceDao reportPoolServiceDaoImpl) {
        this.reportPoolServiceDaoImpl = reportPoolServiceDaoImpl;
    }
}
