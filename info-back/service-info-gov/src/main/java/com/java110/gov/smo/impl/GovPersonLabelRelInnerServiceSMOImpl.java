package com.java110.gov.smo.impl;


import com.java110.gov.dao.IGovPersonLabelRelServiceDao;
import com.java110.intf.gov.IGovPersonLabelRelInnerServiceSMO;
import com.java110.dto.govPersonLabelRel.GovPersonLabelRelDto;
import com.java110.po.govPersonLabelRel.GovPersonLabelRelPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 标签与人口关系内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovPersonLabelRelInnerServiceSMOImpl extends BaseServiceSMO implements IGovPersonLabelRelInnerServiceSMO {

    @Autowired
    private IGovPersonLabelRelServiceDao govPersonLabelRelServiceDaoImpl;


    @Override
    public int saveGovPersonLabelRel(@RequestBody  GovPersonLabelRelPo govPersonLabelRelPo) {
        int saveFlag = 1;
        govPersonLabelRelServiceDaoImpl.saveGovPersonLabelRelInfo(BeanConvertUtil.beanCovertMap(govPersonLabelRelPo));
        return saveFlag;
    }

     @Override
    public int updateGovPersonLabelRel(@RequestBody  GovPersonLabelRelPo govPersonLabelRelPo) {
        int saveFlag = 1;
         govPersonLabelRelServiceDaoImpl.updateGovPersonLabelRelInfo(BeanConvertUtil.beanCovertMap(govPersonLabelRelPo));
        return saveFlag;
    }

     @Override
    public int deleteGovPersonLabelRel(@RequestBody  GovPersonLabelRelPo govPersonLabelRelPo) {
        int saveFlag = 1;
        govPersonLabelRelPo.setStatusCd("1");
        govPersonLabelRelServiceDaoImpl.updateGovPersonLabelRelInfo(BeanConvertUtil.beanCovertMap(govPersonLabelRelPo));
        return saveFlag;
    }

    @Override
    public List<GovPersonLabelRelDto> queryGovPersonLabelRels(@RequestBody  GovPersonLabelRelDto govPersonLabelRelDto) {

        //校验是否传了 分页信息

        int page = govPersonLabelRelDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govPersonLabelRelDto.setPage((page - 1) * govPersonLabelRelDto.getRow());
        }

        List<GovPersonLabelRelDto> govPersonLabelRels = BeanConvertUtil.covertBeanList(govPersonLabelRelServiceDaoImpl.getGovPersonLabelRelInfo(BeanConvertUtil.beanCovertMap(govPersonLabelRelDto)), GovPersonLabelRelDto.class);

        return govPersonLabelRels;
    }


    @Override
    public int queryGovPersonLabelRelsCount(@RequestBody GovPersonLabelRelDto govPersonLabelRelDto) {
        return govPersonLabelRelServiceDaoImpl.queryGovPersonLabelRelsCount(BeanConvertUtil.beanCovertMap(govPersonLabelRelDto));    }

    public IGovPersonLabelRelServiceDao getGovPersonLabelRelServiceDaoImpl() {
        return govPersonLabelRelServiceDaoImpl;
    }

    public void setGovPersonLabelRelServiceDaoImpl(IGovPersonLabelRelServiceDao govPersonLabelRelServiceDaoImpl) {
        this.govPersonLabelRelServiceDaoImpl = govPersonLabelRelServiceDaoImpl;
    }
}
