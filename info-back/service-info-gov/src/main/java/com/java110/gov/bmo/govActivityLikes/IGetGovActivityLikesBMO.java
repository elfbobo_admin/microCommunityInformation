package com.java110.gov.bmo.govActivityLikes;
import org.springframework.http.ResponseEntity;
import com.java110.dto.govActivityLikes.GovActivityLikesDto;
public interface IGetGovActivityLikesBMO {


    /**
     * 查询活动点赞
     * add by wuxw
     * @param  govActivityLikesDto
     * @return
     */
    ResponseEntity<String> get(GovActivityLikesDto govActivityLikesDto);


}
