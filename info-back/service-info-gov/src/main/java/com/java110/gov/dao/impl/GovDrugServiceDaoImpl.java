package com.java110.gov.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.gov.dao.IGovDrugServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 吸毒者服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govDrugServiceDaoImpl")
//@Transactional
public class GovDrugServiceDaoImpl extends BaseServiceDao implements IGovDrugServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovDrugServiceDaoImpl.class);





    /**
     * 保存吸毒者信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovDrugInfo(Map info) throws DAOException {
        logger.debug("保存吸毒者信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govDrugServiceDaoImpl.saveGovDrugInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存吸毒者信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询吸毒者信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovDrugInfo(Map info) throws DAOException {
        logger.debug("查询吸毒者信息 入参 info : {}",info);

        List<Map> businessGovDrugInfos = sqlSessionTemplate.selectList("govDrugServiceDaoImpl.getGovDrugInfo",info);

        return businessGovDrugInfos;
    }


    /**
     * 修改吸毒者信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovDrugInfo(Map info) throws DAOException {
        logger.debug("修改吸毒者信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govDrugServiceDaoImpl.updateGovDrugInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改吸毒者信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询吸毒者数量
     * @param info 吸毒者信息
     * @return 吸毒者数量
     */
    @Override
    public int queryGovDrugsCount(Map info) {
        logger.debug("查询吸毒者数据 入参 info : {}",info);

        List<Map> businessGovDrugInfos = sqlSessionTemplate.selectList("govDrugServiceDaoImpl.queryGovDrugsCount", info);
        if (businessGovDrugInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovDrugInfos.get(0).get("count").toString());
    }


}
