package com.java110.gov.bmo.govCiviladmin.impl;

import com.java110.dto.govCiviladmin.GovCiviladminDto;
import com.java110.gov.bmo.govCiviladmin.IGetGovCiviladminBMO;
import com.java110.intf.gov.IGovCiviladminInnerServiceSMO;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("getGovCiviladminBMOImpl")
public class GetGovCiviladminBMOImpl implements IGetGovCiviladminBMO {

    @Autowired
    private IGovCiviladminInnerServiceSMO govCiviladminInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govCiviladminDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovCiviladminDto govCiviladminDto) {


        int count = govCiviladminInnerServiceSMOImpl.queryGovCiviladminsCount(govCiviladminDto);

        List<GovCiviladminDto> govCiviladminDtos = null;
        if (count > 0) {
            govCiviladminDtos = govCiviladminInnerServiceSMOImpl.queryGovCiviladmins(govCiviladminDto);
        } else {
            govCiviladminDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govCiviladminDto.getRow()), count, govCiviladminDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
