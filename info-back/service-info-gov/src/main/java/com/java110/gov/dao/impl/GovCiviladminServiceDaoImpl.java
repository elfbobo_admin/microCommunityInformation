package com.java110.gov.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.gov.dao.IGovCiviladminServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 民政服务宣传服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govCiviladminServiceDaoImpl")
//@Transactional
public class GovCiviladminServiceDaoImpl extends BaseServiceDao implements IGovCiviladminServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovCiviladminServiceDaoImpl.class);





    /**
     * 保存民政服务宣传信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovCiviladminInfo(Map info) throws DAOException {
        logger.debug("保存民政服务宣传信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govCiviladminServiceDaoImpl.saveGovCiviladminInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存民政服务宣传信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询民政服务宣传信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovCiviladminInfo(Map info) throws DAOException {
        logger.debug("查询民政服务宣传信息 入参 info : {}",info);

        List<Map> businessGovCiviladminInfos = sqlSessionTemplate.selectList("govCiviladminServiceDaoImpl.getGovCiviladminInfo",info);

        return businessGovCiviladminInfos;
    }


    /**
     * 修改民政服务宣传信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovCiviladminInfo(Map info) throws DAOException {
        logger.debug("修改民政服务宣传信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govCiviladminServiceDaoImpl.updateGovCiviladminInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改民政服务宣传信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询民政服务宣传数量
     * @param info 民政服务宣传信息
     * @return 民政服务宣传数量
     */
    @Override
    public int queryGovCiviladminsCount(Map info) {
        logger.debug("查询民政服务宣传数据 入参 info : {}",info);

        List<Map> businessGovCiviladminInfos = sqlSessionTemplate.selectList("govCiviladminServiceDaoImpl.queryGovCiviladminsCount", info);
        if (businessGovCiviladminInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovCiviladminInfos.get(0).get("count").toString());
    }


}
