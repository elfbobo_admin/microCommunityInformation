package com.java110.gov.smo.impl;


import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.PageDto;
import com.java110.dto.govCarInout.GovCarInoutDto;
import com.java110.gov.dao.IGovCarInoutServiceDao;
import com.java110.intf.gov.IGovCarInoutInnerServiceSMO;
import com.java110.po.govCarInout.GovCarInoutPo;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 车辆进出记录内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovCarInoutInnerServiceSMOImpl extends BaseServiceSMO implements IGovCarInoutInnerServiceSMO {

    @Autowired
    private IGovCarInoutServiceDao govCarInoutServiceDaoImpl;


    @Override
    public int saveGovCarInout(@RequestBody GovCarInoutPo govCarInoutPo) {
        int saveFlag = 1;
        govCarInoutServiceDaoImpl.saveGovCarInoutInfo(BeanConvertUtil.beanCovertMap(govCarInoutPo));
        return saveFlag;
    }

     @Override
    public int updateGovCarInout(@RequestBody  GovCarInoutPo govCarInoutPo) {
        int saveFlag = 1;
         govCarInoutServiceDaoImpl.updateGovCarInoutInfo(BeanConvertUtil.beanCovertMap(govCarInoutPo));
        return saveFlag;
    }

     @Override
    public int deleteGovCarInout(@RequestBody  GovCarInoutPo govCarInoutPo) {
        int saveFlag = 1;
        govCarInoutPo.setStatusCd("1");
        govCarInoutServiceDaoImpl.updateGovCarInoutInfo(BeanConvertUtil.beanCovertMap(govCarInoutPo));
        return saveFlag;
    }

    @Override
    public List<GovCarInoutDto> queryGovCarInouts(@RequestBody  GovCarInoutDto govCarInoutDto) {

        //校验是否传了 分页信息

        int page = govCarInoutDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govCarInoutDto.setPage((page - 1) * govCarInoutDto.getRow());
        }

        List<GovCarInoutDto> govCarInouts = BeanConvertUtil.covertBeanList(govCarInoutServiceDaoImpl.getGovCarInoutInfo(BeanConvertUtil.beanCovertMap(govCarInoutDto)), GovCarInoutDto.class);

        return govCarInouts;
    }


    @Override
    public int queryGovCarInoutsCount(@RequestBody GovCarInoutDto govCarInoutDto) {
        return govCarInoutServiceDaoImpl.queryGovCarInoutsCount(BeanConvertUtil.beanCovertMap(govCarInoutDto));    }

    public IGovCarInoutServiceDao getGovCarInoutServiceDaoImpl() {
        return govCarInoutServiceDaoImpl;
    }

    public void setGovCarInoutServiceDaoImpl(IGovCarInoutServiceDao govCarInoutServiceDaoImpl) {
        this.govCarInoutServiceDaoImpl = govCarInoutServiceDaoImpl;
    }
}
