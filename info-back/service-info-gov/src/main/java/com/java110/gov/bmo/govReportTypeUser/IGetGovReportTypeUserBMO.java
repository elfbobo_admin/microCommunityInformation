package com.java110.gov.bmo.govReportTypeUser;
import com.java110.dto.govReportTypeUser.GovReportTypeUserDto;
import org.springframework.http.ResponseEntity;
public interface IGetGovReportTypeUserBMO {


    /**
     * 查询报事类型人员
     * add by wuxw
     * @param  govReportTypeUserDto
     * @return
     */
    ResponseEntity<String> get(GovReportTypeUserDto govReportTypeUserDto);


}
