package com.java110.gov.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.perGovActivityRel.PerGovActivityRelDto;
import com.java110.po.perGovActivityRel.PerGovActivityRelPo;
import com.java110.gov.bmo.perGovActivityRel.IDeletePerGovActivityRelBMO;
import com.java110.gov.bmo.perGovActivityRel.IGetPerGovActivityRelBMO;
import com.java110.gov.bmo.perGovActivityRel.ISavePerGovActivityRelBMO;
import com.java110.gov.bmo.perGovActivityRel.IUpdatePerGovActivityRelBMO;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/perGovActivityRel")
public class PerGovActivityRelApi {

    @Autowired
    private ISavePerGovActivityRelBMO savePerGovActivityRelBMOImpl;
    @Autowired
    private IUpdatePerGovActivityRelBMO updatePerGovActivityRelBMOImpl;
    @Autowired
    private IDeletePerGovActivityRelBMO deletePerGovActivityRelBMOImpl;

    @Autowired
    private IGetPerGovActivityRelBMO getPerGovActivityRelBMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /perGovActivityRel/savePerGovActivityRel
     * @path /app/perGovActivityRel/savePerGovActivityRel
     */
    @RequestMapping(value = "/savePerGovActivityRel", method = RequestMethod.POST)
    public ResponseEntity<String> savePerGovActivityRel(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "actRelId", "请求报文中未包含actRelId");
        Assert.hasKeyAndValue(reqJson, "govPersonId", "请求报文中未包含govPersonId");
        Assert.hasKeyAndValue(reqJson, "perActivitiesId", "请求报文中未包含perActivitiesId");
        Assert.hasKeyAndValue(reqJson, "state", "请求报文中未包含state");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");


        PerGovActivityRelPo perGovActivityRelPo = BeanConvertUtil.covertBean(reqJson, PerGovActivityRelPo.class);
        return savePerGovActivityRelBMOImpl.save(perGovActivityRelPo);
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /perGovActivityRel/updatePerGovActivityRel
     * @path /app/perGovActivityRel/updatePerGovActivityRel
     */
    @RequestMapping(value = "/updatePerGovActivityRel", method = RequestMethod.POST)
    public ResponseEntity<String> updatePerGovActivityRel(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "actRelId", "请求报文中未包含actRelId");
        Assert.hasKeyAndValue(reqJson, "govPersonId", "请求报文中未包含govPersonId");
        Assert.hasKeyAndValue(reqJson, "perActivitiesId", "请求报文中未包含perActivitiesId");
        Assert.hasKeyAndValue(reqJson, "state", "请求报文中未包含state");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "actRelId", "actRelId不能为空");
        PerGovActivityRelPo perGovActivityRelPo = BeanConvertUtil.covertBean(reqJson, PerGovActivityRelPo.class);
        return updatePerGovActivityRelBMOImpl.update(perGovActivityRelPo);
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /perGovActivityRel/deletePerGovActivityRel
     * @path /app/perGovActivityRel/deletePerGovActivityRel
     */
    @RequestMapping(value = "/deletePerGovActivityRel", method = RequestMethod.POST)
    public ResponseEntity<String> deletePerGovActivityRel(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "caId", "caId不能为空");

        Assert.hasKeyAndValue(reqJson, "actRelId", "actRelId不能为空");


        PerGovActivityRelPo perGovActivityRelPo = BeanConvertUtil.covertBean(reqJson, PerGovActivityRelPo.class);
        return deletePerGovActivityRelBMOImpl.delete(perGovActivityRelPo);
    }

    /**
     * 微信删除消息模板
     *
     * @param caId 小区ID
     * @return
     * @serviceCode /perGovActivityRel/queryPerGovActivityRel
     * @path /app/perGovActivityRel/queryPerGovActivityRel
     */
    @RequestMapping(value = "/queryPerGovActivityRel", method = RequestMethod.GET)
    public ResponseEntity<String> queryPerGovActivityRel(@RequestParam(value = "caId") String caId,
                                                         @RequestParam(value = "perActivitiesId", required = false) String perActivitiesId,
                                                         @RequestParam(value = "page") int page,
                                                         @RequestParam(value = "row") int row) {
        PerGovActivityRelDto perGovActivityRelDto = new PerGovActivityRelDto();
        perGovActivityRelDto.setPage(page);
        perGovActivityRelDto.setRow(row);
        perGovActivityRelDto.setCaId(caId);
        perGovActivityRelDto.setPerActivitiesId(perActivitiesId);
        return getPerGovActivityRelBMOImpl.get(perGovActivityRelDto);
    }
}
