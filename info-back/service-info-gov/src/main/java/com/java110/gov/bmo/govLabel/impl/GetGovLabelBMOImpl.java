package com.java110.gov.bmo.govLabel.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.gov.bmo.govLabel.IGetGovLabelBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.gov.IGovLabelInnerServiceSMO;
import com.java110.dto.govLabel.GovLabelDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import org.springframework.http.HttpStatus;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service("getGovLabelBMOImpl")
public class GetGovLabelBMOImpl implements IGetGovLabelBMO {

    @Autowired
    private IGovLabelInnerServiceSMO govLabelInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govLabelDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovLabelDto govLabelDto) {


        int count = govLabelInnerServiceSMOImpl.queryGovLabelsCount(govLabelDto);

        List<GovLabelDto> govLabelDtos = null;
        if (count > 0) {
            govLabelDtos = govLabelInnerServiceSMOImpl.queryGovLabels(govLabelDto);
        } else {
            govLabelDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govLabelDto.getRow()), count, govLabelDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
