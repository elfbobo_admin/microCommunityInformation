package com.java110.gov.bmo.govHelpPolicy;
import org.springframework.http.ResponseEntity;
import com.java110.po.govHelpPolicy.GovHelpPolicyPo;

public interface IDeleteGovHelpPolicyBMO {


    /**
     * 修改帮扶政策
     * add by wuxw
     * @param govHelpPolicyPo
     * @return
     */
    ResponseEntity<String> delete(GovHelpPolicyPo govHelpPolicyPo);


}
