package com.java110.gov.bmo.govPetitionLetter;
import org.springframework.http.ResponseEntity;
import com.java110.po.govPetitionLetter.GovPetitionLetterPo;

public interface IDeleteGovPetitionLetterBMO {


    /**
     * 修改信访管理
     * add by wuxw
     * @param govPetitionLetterPo
     * @return
     */
    ResponseEntity<String> delete(GovPetitionLetterPo govPetitionLetterPo);


}
