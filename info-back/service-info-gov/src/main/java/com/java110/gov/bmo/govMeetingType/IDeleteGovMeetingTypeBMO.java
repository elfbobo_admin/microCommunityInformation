package com.java110.gov.bmo.govMeetingType;
import org.springframework.http.ResponseEntity;
import com.java110.po.govMeetingType.GovMeetingTypePo;

public interface IDeleteGovMeetingTypeBMO {


    /**
     * 修改会议类型
     * add by wuxw
     * @param govMeetingTypePo
     * @return
     */
    ResponseEntity<String> delete(GovMeetingTypePo govMeetingTypePo);


}
