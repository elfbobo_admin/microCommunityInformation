package com.java110.gov.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.gov.dao.IGovMeetingTypeServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 会议类型服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govMeetingTypeServiceDaoImpl")
//@Transactional
public class GovMeetingTypeServiceDaoImpl extends BaseServiceDao implements IGovMeetingTypeServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovMeetingTypeServiceDaoImpl.class);





    /**
     * 保存会议类型信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovMeetingTypeInfo(Map info) throws DAOException {
        logger.debug("保存会议类型信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govMeetingTypeServiceDaoImpl.saveGovMeetingTypeInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存会议类型信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询会议类型信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovMeetingTypeInfo(Map info) throws DAOException {
        logger.debug("查询会议类型信息 入参 info : {}",info);

        List<Map> businessGovMeetingTypeInfos = sqlSessionTemplate.selectList("govMeetingTypeServiceDaoImpl.getGovMeetingTypeInfo",info);

        return businessGovMeetingTypeInfos;
    }


    /**
     * 修改会议类型信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovMeetingTypeInfo(Map info) throws DAOException {
        logger.debug("修改会议类型信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govMeetingTypeServiceDaoImpl.updateGovMeetingTypeInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改会议类型信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询会议类型数量
     * @param info 会议类型信息
     * @return 会议类型数量
     */
    @Override
    public int queryGovMeetingTypesCount(Map info) {
        logger.debug("查询会议类型数据 入参 info : {}",info);

        List<Map> businessGovMeetingTypeInfos = sqlSessionTemplate.selectList("govMeetingTypeServiceDaoImpl.queryGovMeetingTypesCount", info);
        if (businessGovMeetingTypeInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovMeetingTypeInfos.get(0).get("count").toString());
    }


}
