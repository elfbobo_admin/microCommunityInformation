package com.java110.gov.bmo.perGovActivities.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.gov.bmo.perGovActivities.IGetPerGovActivitiesBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.gov.IPerGovActivitiesInnerServiceSMO;
import com.java110.dto.perGovActivities.PerGovActivitiesDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import org.springframework.http.HttpStatus;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service("getPerGovActivitiesBMOImpl")
public class GetPerGovActivitiesBMOImpl implements IGetPerGovActivitiesBMO {

    @Autowired
    private IPerGovActivitiesInnerServiceSMO perGovActivitiesInnerServiceSMOImpl;

    /**
     *
     *
     * @param  perGovActivitiesDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(PerGovActivitiesDto perGovActivitiesDto) {


        int count = perGovActivitiesInnerServiceSMOImpl.queryPerGovActivitiessCount(perGovActivitiesDto);

        List<PerGovActivitiesDto> perGovActivitiesDtos = null;
        if (count > 0) {
            perGovActivitiesDtos = perGovActivitiesInnerServiceSMOImpl.queryPerGovActivitiess(perGovActivitiesDto);
        } else {
            perGovActivitiesDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) perGovActivitiesDto.getRow()), count, perGovActivitiesDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
