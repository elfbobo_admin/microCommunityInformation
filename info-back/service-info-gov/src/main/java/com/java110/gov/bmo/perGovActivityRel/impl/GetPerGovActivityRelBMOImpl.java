package com.java110.gov.bmo.perGovActivityRel.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.gov.bmo.perGovActivityRel.IGetPerGovActivityRelBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.gov.IPerGovActivityRelInnerServiceSMO;
import com.java110.dto.perGovActivityRel.PerGovActivityRelDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import org.springframework.http.HttpStatus;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service("getPerGovActivityRelBMOImpl")
public class GetPerGovActivityRelBMOImpl implements IGetPerGovActivityRelBMO {

    @Autowired
    private IPerGovActivityRelInnerServiceSMO perGovActivityRelInnerServiceSMOImpl;

    /**
     *
     *
     * @param  perGovActivityRelDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(PerGovActivityRelDto perGovActivityRelDto) {


        int count = perGovActivityRelInnerServiceSMOImpl.queryPerGovActivityRelsCount(perGovActivityRelDto);

        List<PerGovActivityRelDto> perGovActivityRelDtos = null;
        if (count > 0) {
            perGovActivityRelDtos = perGovActivityRelInnerServiceSMOImpl.queryPerGovActivityRels(perGovActivityRelDto);
        } else {
            perGovActivityRelDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) perGovActivityRelDto.getRow()), count, perGovActivityRelDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
