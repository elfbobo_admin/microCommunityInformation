package com.java110.gov.bmo.govVolunteerPersonRel;
import org.springframework.http.ResponseEntity;
import com.java110.dto.govVolunteerPersonRel.GovVolunteerPersonRelDto;
public interface IGetGovVolunteerPersonRelBMO {


    /**
     * 查询服务记录人员关系表
     * add by wuxw
     * @param  govVolunteerPersonRelDto
     * @return
     */
    ResponseEntity<String> get(GovVolunteerPersonRelDto govVolunteerPersonRelDto);


}
