package com.java110.gov.bmo.govActivities.impl;

import com.java110.dto.fileRel.FileRelDto;
import com.java110.gov.bmo.govActivities.IGetGovActivitiesBMO;
import com.java110.intf.assets.IFileRelInnerServiceSMO;
import com.java110.intf.gov.IGovActivitiesInnerServiceSMO;
import com.java110.utils.util.Assert;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.dto.govActivities.GovActivitiesDto;

import java.util.ArrayList;
import java.util.List;

@Service("getGovActivitiesBMOImpl")
public class GetGovActivitiesBMOImpl implements IGetGovActivitiesBMO {

    @Autowired
    private IGovActivitiesInnerServiceSMO govActivitiesInnerServiceSMOImpl;
    @Autowired
    private IFileRelInnerServiceSMO fileRelInnerServiceSMOImpl;
    /**
     *
     *
     * @param  govActivitiesDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovActivitiesDto govActivitiesDto) {


        int count = govActivitiesInnerServiceSMOImpl.queryGovActivitiessCount(govActivitiesDto);

        List<GovActivitiesDto> govActivitiesDtos = null;
        if (count > 0) {
            govActivitiesDtos = govActivitiesInnerServiceSMOImpl.queryGovActivitiess(govActivitiesDto);
            reFreshHeaderImg(govActivitiesDtos);
        } else {
            govActivitiesDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govActivitiesDto.getRow()), count, govActivitiesDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

    private void reFreshHeaderImg(List<GovActivitiesDto> govActivitiesDtos){
            for(GovActivitiesDto govActivitiesDto: govActivitiesDtos){
               String imgId=  govActivitiesDto.getHeaderImg();
                FileRelDto fileRelDto = new FileRelDto();
                fileRelDto.setFileRelId(imgId);
                fileRelDto.setRelType(FileRelDto.REL_TYPE);
                List<FileRelDto> relDtos = fileRelInnerServiceSMOImpl.queryFileRels(fileRelDto);
                if(relDtos != null && relDtos.size() > 0){
                    govActivitiesDto.setHeaderImg(relDtos.get(0).getFileName());
                }
            }
    }
}
