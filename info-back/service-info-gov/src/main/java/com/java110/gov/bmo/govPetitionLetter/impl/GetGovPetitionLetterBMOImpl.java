package com.java110.gov.bmo.govPetitionLetter.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.gov.bmo.govPetitionLetter.IGetGovPetitionLetterBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.gov.IGovPetitionLetterInnerServiceSMO;
import com.java110.dto.govPetitionLetter.GovPetitionLetterDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import org.springframework.http.HttpStatus;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service("getGovPetitionLetterBMOImpl")
public class GetGovPetitionLetterBMOImpl implements IGetGovPetitionLetterBMO {

    @Autowired
    private IGovPetitionLetterInnerServiceSMO govPetitionLetterInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govPetitionLetterDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovPetitionLetterDto govPetitionLetterDto) {


        int count = govPetitionLetterInnerServiceSMOImpl.queryGovPetitionLettersCount(govPetitionLetterDto);

        List<GovPetitionLetterDto> govPetitionLetterDtos = null;
        if (count > 0) {
            govPetitionLetterDtos = govPetitionLetterInnerServiceSMOImpl.queryGovPetitionLetters(govPetitionLetterDto);
        } else {
            govPetitionLetterDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govPetitionLetterDto.getRow()), count, govPetitionLetterDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
