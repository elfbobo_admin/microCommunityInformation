package com.java110.gov.bmo.govPersonLabelRel;

import org.springframework.http.ResponseEntity;
import com.java110.po.govPersonLabelRel.GovPersonLabelRelPo;
public interface ISaveGovPersonLabelRelBMO {


    /**
     * 添加标签与人口关系
     * add by wuxw
     * @param govPersonLabelRelPo
     * @return
     */
    ResponseEntity<String> save(GovPersonLabelRelPo govPersonLabelRelPo,String labels);
    /**
     * 添加标签与人口关系
     * add by wuxw
     * @param govPersonLabelRelPo
     * @return
     */
    ResponseEntity<String> savePersonLabelRel(GovPersonLabelRelPo govPersonLabelRelPo,String lableCd,String lableName,String lableType);


}
