package com.java110.gov.bmo.govActivityPerson;
import com.java110.dto.govActivityPerson.GovActivityPersonDto;
import org.springframework.http.ResponseEntity;
public interface IGetGovActivityPersonBMO {


    /**
     * 查询活动报名人员
     * add by wuxw
     * @param  govActivityPersonDto
     * @return
     */
    ResponseEntity<String> get(GovActivityPersonDto govActivityPersonDto);


}
