package com.java110.gov.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govAreaRenovation.GovAreaRenovationDto;
import com.java110.po.govAreaRenovation.GovAreaRenovationPo;
import com.java110.gov.bmo.govAreaRenovation.IDeleteGovAreaRenovationBMO;
import com.java110.gov.bmo.govAreaRenovation.IGetGovAreaRenovationBMO;
import com.java110.gov.bmo.govAreaRenovation.ISaveGovAreaRenovationBMO;
import com.java110.gov.bmo.govAreaRenovation.IUpdateGovAreaRenovationBMO;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govAreaRenovation")
public class GovAreaRenovationApi {

    @Autowired
    private ISaveGovAreaRenovationBMO saveGovAreaRenovationBMOImpl;
    @Autowired
    private IUpdateGovAreaRenovationBMO updateGovAreaRenovationBMOImpl;
    @Autowired
    private IDeleteGovAreaRenovationBMO deleteGovAreaRenovationBMOImpl;

    @Autowired
    private IGetGovAreaRenovationBMO getGovAreaRenovationBMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govAreaRenovation/saveGovAreaRenovation
     * @path /app/govAreaRenovation/saveGovAreaRenovation
     */
    @RequestMapping(value = "/saveGovAreaRenovation", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovAreaRenovation(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue( reqJson, "caId", "请求报文中未包含caId" );
        Assert.hasKeyAndValue( reqJson, "typeId", "请求报文中未包含typeId" );
        Assert.hasKeyAndValue( reqJson, "securityKey", "请求报文中未包含securityKey" );
        Assert.hasKeyAndValue( reqJson, "securityProblem", "请求报文中未包含securityProblem" );
        Assert.hasKeyAndValue( reqJson, "securityRange", "请求报文中未包含securityRange" );
        Assert.hasKeyAndValue( reqJson, "leadCompany", "请求报文中未包含leadCompany" );
        Assert.hasKeyAndValue( reqJson, "leadParticipate", "请求报文中未包含leadParticipate" );
        Assert.hasKeyAndValue( reqJson, "name", "请求报文中未包含name" );
        Assert.hasKeyAndValue( reqJson, "tel", "请求报文中未包含tel" );
        Assert.hasKeyAndValue( reqJson, "startTime", "请求报文中未包含startTime" );
        Assert.hasKeyAndValue( reqJson, "endTime", "请求报文中未包含endTime" );
        Assert.hasKeyAndValue( reqJson, "crackCriminal", "请求报文中未包含crackCriminal" );
        Assert.hasKeyAndValue( reqJson, "crackSecurity", "请求报文中未包含crackSecurity" );


        GovAreaRenovationPo govAreaRenovationPo = BeanConvertUtil.covertBean( reqJson, GovAreaRenovationPo.class );
        govAreaRenovationPo.setDatasourceType( "999999" );
        return saveGovAreaRenovationBMOImpl.save( govAreaRenovationPo );
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govAreaRenovation/updateGovAreaRenovation
     * @path /app/govAreaRenovation/updateGovAreaRenovation
     */
    @RequestMapping(value = "/updateGovAreaRenovation", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovAreaRenovation(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue( reqJson, "govRenovationId", "请求报文中未包含govRenovationId" );
        Assert.hasKeyAndValue( reqJson, "caId", "请求报文中未包含caId" );
        Assert.hasKeyAndValue( reqJson, "typeId", "请求报文中未包含typeId" );
        Assert.hasKeyAndValue( reqJson, "securityKey", "请求报文中未包含securityKey" );
        Assert.hasKeyAndValue( reqJson, "securityProblem", "请求报文中未包含securityProblem" );
        Assert.hasKeyAndValue( reqJson, "securityRange", "请求报文中未包含securityRange" );
        Assert.hasKeyAndValue( reqJson, "leadCompany", "请求报文中未包含leadCompany" );
        Assert.hasKeyAndValue( reqJson, "leadParticipate", "请求报文中未包含leadParticipate" );
        Assert.hasKeyAndValue( reqJson, "name", "请求报文中未包含name" );
        Assert.hasKeyAndValue( reqJson, "tel", "请求报文中未包含tel" );
        Assert.hasKeyAndValue( reqJson, "startTime", "请求报文中未包含startTime" );
        Assert.hasKeyAndValue( reqJson, "endTime", "请求报文中未包含endTime" );
        Assert.hasKeyAndValue( reqJson, "crackCriminal", "请求报文中未包含crackCriminal" );
        Assert.hasKeyAndValue( reqJson, "crackSecurity", "请求报文中未包含crackSecurity" );


        GovAreaRenovationPo govAreaRenovationPo = BeanConvertUtil.covertBean( reqJson, GovAreaRenovationPo.class );
        return updateGovAreaRenovationBMOImpl.update( govAreaRenovationPo );
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govAreaRenovation/deleteGovAreaRenovation
     * @path /app/govAreaRenovation/deleteGovAreaRenovation
     */
    @RequestMapping(value = "/deleteGovAreaRenovation", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovAreaRenovation(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue( reqJson, "caId", "caId不能为空" );

        Assert.hasKeyAndValue( reqJson, "govRenovationId", "govRenovationId不能为空" );


        GovAreaRenovationPo govAreaRenovationPo = BeanConvertUtil.covertBean( reqJson, GovAreaRenovationPo.class );
        return deleteGovAreaRenovationBMOImpl.delete( govAreaRenovationPo );
    }

    /**
     * 微信删除消息模板
     *
     * @param caId 小区ID
     * @return
     * @serviceCode /govAreaRenovation/queryGovAreaRenovation
     * @path /app/govAreaRenovation/queryGovAreaRenovation
     */
    @RequestMapping(value = "/queryGovAreaRenovation", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovAreaRenovation(@RequestParam(value = "caId") String caId,
                                                         @RequestParam(value = "page") int page,
                                                         @RequestParam(value = "row") int row) {
        GovAreaRenovationDto govAreaRenovationDto = new GovAreaRenovationDto();
        govAreaRenovationDto.setPage( page );
        govAreaRenovationDto.setRow( row );
        govAreaRenovationDto.setCaId( caId );
        return getGovAreaRenovationBMOImpl.get( govAreaRenovationDto );
    }
}
