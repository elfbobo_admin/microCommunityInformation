package com.java110.gov.bmo.govActivityReply;
import org.springframework.http.ResponseEntity;
import com.java110.dto.govActivityReply.GovActivityReplyDto;
public interface IGetGovActivityReplyBMO {


    /**
     * 查询活动评论
     * add by wuxw
     * @param  govActivityReplyDto
     * @return
     */
    ResponseEntity<String> get(GovActivityReplyDto govActivityReplyDto);


}
