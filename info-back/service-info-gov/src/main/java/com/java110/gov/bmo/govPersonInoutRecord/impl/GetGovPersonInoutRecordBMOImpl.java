package com.java110.gov.bmo.govPersonInoutRecord.impl;

import com.java110.dto.govPersonInoutRecord.GovPersonInoutRecordDto;
import com.java110.gov.bmo.govPersonInoutRecord.IGetGovPersonInoutRecordBMO;
import com.java110.intf.gov.IGovPersonInoutRecordInnerServiceSMO;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("getGovPersonInoutRecordBMOImpl")
public class GetGovPersonInoutRecordBMOImpl implements IGetGovPersonInoutRecordBMO {

    @Autowired
    private IGovPersonInoutRecordInnerServiceSMO govPersonInoutRecordInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govPersonInoutRecordDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovPersonInoutRecordDto govPersonInoutRecordDto) {


        int count = govPersonInoutRecordInnerServiceSMOImpl.queryGovPersonInoutRecordsCount(govPersonInoutRecordDto);

        List<GovPersonInoutRecordDto> govPersonInoutRecordDtos = null;
        if (count > 0) {
            govPersonInoutRecordDtos = govPersonInoutRecordInnerServiceSMOImpl.queryGovPersonInoutRecords(govPersonInoutRecordDto);
        } else {
            govPersonInoutRecordDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govPersonInoutRecordDto.getRow()), count, govPersonInoutRecordDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
