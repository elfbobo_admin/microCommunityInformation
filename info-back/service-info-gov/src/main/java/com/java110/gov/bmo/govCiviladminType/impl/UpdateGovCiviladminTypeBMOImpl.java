package com.java110.gov.bmo.govCiviladminType.impl;

import com.java110.core.annotation.Java110Transactional;
import com.java110.gov.bmo.govCiviladminType.IUpdateGovCiviladminTypeBMO;
import com.java110.intf.gov.IGovCiviladminTypeInnerServiceSMO;
import com.java110.po.govCiviladminType.GovCiviladminTypePo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("updateGovCiviladminTypeBMOImpl")
public class UpdateGovCiviladminTypeBMOImpl implements IUpdateGovCiviladminTypeBMO {

    @Autowired
    private IGovCiviladminTypeInnerServiceSMO govCiviladminTypeInnerServiceSMOImpl;

    /**
     *
     *
     * @param govCiviladminTypePo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> update(GovCiviladminTypePo govCiviladminTypePo) {

        int flag = govCiviladminTypeInnerServiceSMOImpl.updateGovCiviladminType(govCiviladminTypePo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
