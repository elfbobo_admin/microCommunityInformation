package com.java110.gov.smo.impl;


import com.java110.gov.dao.IPerGovActivitiesServiceDao;
import com.java110.intf.gov.IPerGovActivitiesInnerServiceSMO;
import com.java110.dto.perGovActivities.PerGovActivitiesDto;
import com.java110.po.perGovActivities.PerGovActivitiesPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 生日记录内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class PerGovActivitiesInnerServiceSMOImpl extends BaseServiceSMO implements IPerGovActivitiesInnerServiceSMO {

    @Autowired
    private IPerGovActivitiesServiceDao perGovActivitiesServiceDaoImpl;


    @Override
    public int savePerGovActivities(@RequestBody  PerGovActivitiesPo perGovActivitiesPo) {
        int saveFlag = 1;
        perGovActivitiesServiceDaoImpl.savePerGovActivitiesInfo(BeanConvertUtil.beanCovertMap(perGovActivitiesPo));
        return saveFlag;
    }

     @Override
    public int updatePerGovActivities(@RequestBody  PerGovActivitiesPo perGovActivitiesPo) {
        int saveFlag = 1;
         perGovActivitiesServiceDaoImpl.updatePerGovActivitiesInfo(BeanConvertUtil.beanCovertMap(perGovActivitiesPo));
        return saveFlag;
    }

     @Override
    public int deletePerGovActivities(@RequestBody  PerGovActivitiesPo perGovActivitiesPo) {
        int saveFlag = 1;
        perGovActivitiesPo.setStatusCd("1");
        perGovActivitiesServiceDaoImpl.updatePerGovActivitiesInfo(BeanConvertUtil.beanCovertMap(perGovActivitiesPo));
        return saveFlag;
    }

    @Override
    public List<PerGovActivitiesDto> queryPerGovActivitiess(@RequestBody  PerGovActivitiesDto perGovActivitiesDto) {

        //校验是否传了 分页信息

        int page = perGovActivitiesDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            perGovActivitiesDto.setPage((page - 1) * perGovActivitiesDto.getRow());
        }

        List<PerGovActivitiesDto> perGovActivitiess = BeanConvertUtil.covertBeanList(perGovActivitiesServiceDaoImpl.getPerGovActivitiesInfo(BeanConvertUtil.beanCovertMap(perGovActivitiesDto)), PerGovActivitiesDto.class);

        return perGovActivitiess;
    }


    @Override
    public int queryPerGovActivitiessCount(@RequestBody PerGovActivitiesDto perGovActivitiesDto) {
        return perGovActivitiesServiceDaoImpl.queryPerGovActivitiessCount(BeanConvertUtil.beanCovertMap(perGovActivitiesDto));    }

    public IPerGovActivitiesServiceDao getPerGovActivitiesServiceDaoImpl() {
        return perGovActivitiesServiceDaoImpl;
    }

    public void setPerGovActivitiesServiceDaoImpl(IPerGovActivitiesServiceDao perGovActivitiesServiceDaoImpl) {
        this.perGovActivitiesServiceDaoImpl = perGovActivitiesServiceDaoImpl;
    }
}
