package com.java110.gov.bmo.govActivities;
import com.java110.po.govActivities.GovActivitiesPo;
import org.springframework.http.ResponseEntity;

public interface IDeleteGovActivitiesBMO {


    /**
     * 修改公告管理
     * add by wuxw
     * @param govActivitiesPo
     * @return
     */
    ResponseEntity<String> delete(GovActivitiesPo govActivitiesPo);


}
