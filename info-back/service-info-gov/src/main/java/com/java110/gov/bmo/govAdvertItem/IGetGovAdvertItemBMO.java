package com.java110.gov.bmo.govAdvertItem;
import com.java110.dto.govAdvertItem.GovAdvertItemDto;
import org.springframework.http.ResponseEntity;
public interface IGetGovAdvertItemBMO {


    /**
     * 查询广告明细
     * add by wuxw
     * @param  govAdvertItemDto
     * @return
     */
    ResponseEntity<String> get(GovAdvertItemDto govAdvertItemDto);


}
