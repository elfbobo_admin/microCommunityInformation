function _loadGovPerson() {

  let param = {
    params: {
      caId: vc.getCurrentCommunity().caId,
      state: 'finish',
      page: 1,
      row: 1
    }
  }
  vc.http.apiGet(
    '/govPerson/queryLargeGovPerson',
    param,
    function (json, res) {
      //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
      let _json = JSON.parse(json);
      console.log(_json);
      if (_json.code == 0) {
        let _data = _json.data;
        console.log(_data);
        initChart(_data);
        return;
      }
    },
    function (errInfo, error) {
      console.log('请求失败处理');

      vc.toast(errInfo);

    });
}

function initChart(_data) {
  var dom = document.getElementById("aleftboxtmidd");
  var myChart = echarts.init(dom);
  option = {
    color: ['teal', '#e5ffc7', '#508097', '#4d72d9', 'gold', 'limegreen', 'mediumturquoise', 'orchid'],
    backgroundColor: 'rgba(1,202,217,.2)',
    grid: {
      left: 5,
      right: 50,
      top: 20,
      bottom: 0,
      containLabel: true
    },
    //   legend: {
    //       x : 'center',
    //       y : '70%',
    //       textStyle:{
    //         fontSize: 10,
    //         color:'rgba(255,255,255,.7)'
    //       }
    //   },
    calculable: true,
    series: [
      {
        name: '面积模式',
        type: 'pie',
        radius: [5, 45],
        center: ['50%', '55%'],
        roseType: 'area',
        data: [
          { value: _data.gridCoun, name: '(' + _data.gridCoun + ')网格人员' },
          { value: _data.oldCoun, name: '(' + _data.oldCoun + ')老年人' },
          { value: _data.relCoun, name: '(' + _data.relCoun + ')刑满释放' },
          { value: _data.drugsCoun, name: '(' + _data.drugsCoun + ')吸毒人员' },
          { value: _data.correCoun, name: '社区矫正(' + _data.correCoun + ')' },
          { value: _data.mentalCoun, name: '精神障碍(' + _data.mentalCoun + ')' },
          { value: _data.aidssCoun, name: '艾滋病(' + _data.aidssCoun + ')' },
          { value: _data.volunCoun, name: '志愿者(' + _data.volunCoun + ')' }
        ]
      }
    ]
  };
  myChart.setOption(option);
}


_loadGovPerson();