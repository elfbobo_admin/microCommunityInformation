(function(vc,vm){

    vc.extends({
        data:{
            deleteGovHelpPolicyListInfo:{

            }
        },
         _initMethod:function(){

         },
         _initEvent:function(){
             vc.on('deleteGovHelpPolicyList','openDeleteGovHelpPolicyListModal',function(_params){

                vc.component.deleteGovHelpPolicyListInfo = _params;
                $('#deleteGovHelpPolicyListModel').modal('show');

            });
        },
        methods:{
            deleteGovHelpPolicyList:function(){
                vc.component.deleteGovHelpPolicyListInfo.caId=vc.getCurrentCommunity().caId;
                vc.http.apiPost(
                    '/govHelpPolicyList/deleteGovHelpPolicyList',
                    JSON.stringify(vc.component.deleteGovHelpPolicyListInfo),
                    {
                        emulateJSON:true
                     },
                     function(json,res){
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovHelpPolicyListModel').modal('hide');
                            vc.emit('govHelpPolicyListManage','listGovHelpPolicyList',{});
                            return ;
                        }
                        vc.message(_json.msg);
                     },
                     function(errInfo,error){
                        console.log('请求失败处理');
                        vc.message(json);

                     });
            },
            closeDeleteGovHelpPolicyListModel:function(){
                $('#deleteGovHelpPolicyListModel').modal('hide');
            }
        }
    });

})(window.vc,window.vc.component);
