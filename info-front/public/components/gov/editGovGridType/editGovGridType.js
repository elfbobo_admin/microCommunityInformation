(function (vc, vm) {

    vc.extends({
        data: {
            editGovGridTypeInfo: {
                govTypeId: '',
                govTypeId: '',
                caId: '',
                typeName: '',
                ramark: '',
                statusCd: '',
                datasourceType: '',

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('editGovGridType', 'openEditGovGridTypeModal', function (_params) {
                vc.component.refreshEditGovGridTypeInfo();
                $('#editGovGridTypeModel').modal('show');
                vc.copyObject(_params, vc.component.editGovGridTypeInfo);
                vc.component.editGovGridTypeInfo.communityId = vc.getCurrentCommunity().communityId;
            });
        },
        methods: {
            editGovGridTypeValidate: function () {
                return vc.validate.validate({
                    editGovGridTypeInfo: vc.component.editGovGridTypeInfo
                }, {
                    'editGovGridTypeInfo.govTypeId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "责任类型ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "责任类型ID不能超过30"
                        },
                    ],
                    'editGovGridTypeInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "区域ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "区域ID不能超过30"
                        },
                    ],
                    'editGovGridTypeInfo.typeName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "网格名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "网格名称不能超过64"
                        },
                    ],
                    'editGovGridTypeInfo.ramark': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "备注不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "备注不能超过1024"
                        },
                    ]

                });
            },
            editGovGridType: function () {
                if (!vc.component.editGovGridTypeValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }

                vc.http.apiPost(
                    '/govGridType/updateGovGridType',
                    JSON.stringify(vc.component.editGovGridTypeInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovGridTypeModel').modal('hide');
                            vc.emit('govGridTypeManage', 'listGovGridType', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            refreshEditGovGridTypeInfo: function () {
                vc.component.editGovGridTypeInfo = {
                    govTypeId: '',
                    govTypeId: '',
                    caId: '',
                    typeName: '',
                    ramark: '',
                    statusCd: '',
                    datasourceType: '',

                }
            }
        }
    });

})(window.vc, window.vc.component);
