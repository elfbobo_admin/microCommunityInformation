(function (vc, vm) {

    vc.extends({
        data: {
            editGovProtectionCaseInfo: {
                caseId: '',
                objType: '',
                objId: '',
                objName: '',
                caId: '',
                govCommunityId: '',
                caseName: '',
                caseCode: '',
                areaCode: '',
                areaName: '',
                happenedTime: '',
                happenedPlace: '',
                caseType: '',
                idType: '',
                idCard: '',
                personName: '',
                isSolved: '',
                personNum: '',
                fleeingNum: '',
                arrestsNum: '',
                detectionContent: '',
                caseContent: '',
            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('editGovProtectionCase', 'openEditGovProtectionCaseModal', function (_params) {
                vc.component.refreshEditGovProtectionCaseInfo();
                $('#editGovProtectionCaseModel').modal('show');
                vc.copyObject(_params, vc.component.editGovProtectionCaseInfo);
                vc.component.editGovProtectionCaseInfo.caId = vc.getCurrentCommunity().caId;
            });
        },
        methods: {
            editGovProtectionCaseValidate: function () {
                return vc.validate.validate({
                    editGovProtectionCaseInfo: vc.component.editGovProtectionCaseInfo
                }, {
                    
                    'editGovProtectionCaseInfo.objType': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "对象类型（1道路；2校园周边）不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "2",
                            errInfo: "对象类型（1道路；2校园周边）不能超过2"
                        },
                    ],
                    'editGovProtectionCaseInfo.objId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "对象Id不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "对象Id不能超过30"
                        },
                    ],
                    'editGovProtectionCaseInfo.objName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "对象名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "60",
                            errInfo: "对象名称不能超过60"
                        },
                    ],
                    'editGovProtectionCaseInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "区域ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "区域ID不能超过30"
                        },
                    ],
                    'editGovProtectionCaseInfo.caseCode': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "案（事）件编号不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "100",
                            errInfo: "案（事）件编号不能超过100"
                        },
                    ],
                    'editGovProtectionCaseInfo.caseName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "案（事）件名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "100",
                            errInfo: "案（事）件名称不能超过100"
                        },
                    ],
                    'editGovProtectionCaseInfo.areaCode': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "发案地不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "8",
                            errInfo: "发案地不能超过8"
                        },
                    ],
                    'editGovProtectionCaseInfo.happenedTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "发生日期不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "发生日期不能超过时间类型"
                        },
                    ],
                    'editGovProtectionCaseInfo.happenedPlace': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "发生地点不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "200",
                            errInfo: "发生地点不能超过200"
                        },
                    ],
                    'editGovProtectionCaseInfo.caseType': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "案件性质不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "案件性质不能超过12"
                        },
                    ],
                    'editGovProtectionCaseInfo.idType': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "主犯（嫌疑人）证件类型不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "主犯（嫌疑人）证件类型不能超过12"
                        },
                    ],
                    'editGovProtectionCaseInfo.idCard': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "主犯（嫌疑人）证件号码不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "主犯（嫌疑人）证件号码不能超过64"
                        },
                    ],
                    'editGovProtectionCaseInfo.personName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "主犯（嫌疑人）姓名不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "主犯（嫌疑人）姓名不能超过64"
                        },
                    ],
                    'editGovProtectionCaseInfo.isSolved': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "是否破案不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "2",
                            errInfo: "是否破案不能超过2"
                        },
                    ],
                    'editGovProtectionCaseInfo.personNum': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "作案人数不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "3",
                            errInfo: "作案人数不能超过3"
                        },
                    ],
                    'editGovProtectionCaseInfo.fleeingNum': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "在逃人数不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "3",
                            errInfo: "在逃人数不能超过3"
                        },
                    ],
                    'editGovProtectionCaseInfo.arrestsNum': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "抓捕人数不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "3",
                            errInfo: "抓捕人数不能超过3"
                        },
                    ],
                    'editGovProtectionCaseInfo.detectionContent': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "案件侦破情况不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "长文本",
                            errInfo: "案件侦破情况不能超过长文本"
                        },
                    ],
                    'editGovProtectionCaseInfo.caseContent': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "案（事）件情况不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "长文本",
                            errInfo: "案（事）件情况不能超过长文本"
                        },
                    ]
                });
            },
            editGovProtectionCase: function () {
                if (!vc.component.editGovProtectionCaseValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }

                vc.http.apiPost(
                    '/govProtectionCase/updateGovProtectionCase',
                    JSON.stringify(vc.component.editGovProtectionCaseInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovProtectionCaseModel').modal('hide');
                            vc.emit('govProtectionCaseManage', 'listGovProtectionCase', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            refreshEditGovProtectionCaseInfo: function () {
                vc.component.editGovProtectionCaseInfo = {
                    caseId: '',
                    objType: '',
                    objId: '',
                    objName: '',
                    caId: '',
                    govCommunityId: '',
                    caseName: '',
                    caseCode: '',
                    areaCode: '',
                    areaName: '',
                    happenedTime: '',
                    happenedPlace: '',
                    caseType: '',
                    idType: '',
                    idCard: '',
                    personName: '',
                    isSolved: '',
                    personNum: '',
                    fleeingNum: '',
                    arrestsNum: '',
                    detectionContent: '',
                    caseContent: '',
                }
            }
        }
    });

})(window.vc, window.vc.component);
