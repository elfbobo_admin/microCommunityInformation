(function(vc){
    vc.extends({
        propTypes: {
           emitChooseGovRoadProtectionCase:vc.propTypes.string,
           emitLoadData:vc.propTypes.string
        },
        data:{
            chooseGovRoadProtectionCaseInfo:{
                govRoadProtectionCases:[],
                _currentGovRoadProtectionCaseName:'',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('chooseGovRoadProtectionCase','openChooseGovRoadProtectionCaseModel',function(_param){
                $('#chooseGovRoadProtectionCaseModel').modal('show');
                vc.component._refreshChooseGovRoadProtectionCaseInfo();
                vc.component._loadAllGovRoadProtectionCaseInfo(1,10,'');
            });
        },
        methods:{
            _loadAllGovRoadProtectionCaseInfo:function(_page,_row,_name){
                var param = {
                    params:{
                        page:_page,
                        row:_row,
                        caId:vc.getCurrentCommunity().caId,
                        name:_name
                    }
                };

                //发送get请求
               vc.http.apiGet('/govRoadProtectionCase/queryGovRoadProtectionCase',
                             param,
                             function(json){
                                var _govRoadProtectionCaseInfo = JSON.parse(json);
                                vc.component.chooseGovRoadProtectionCaseInfo.govRoadProtectionCases = _govRoadProtectionCaseInfo.govRoadProtectionCases;
                             },function(){
                                console.log('请求失败处理');
                             }
                           );
            },
            chooseGovRoadProtectionCase:function(_govRoadProtectionCase){
                if(_govRoadProtectionCase.hasOwnProperty('name')){
                     _govRoadProtectionCase.govRoadProtectionCaseName = _govRoadProtectionCase.name;
                }
                vc.emit($props.emitChooseGovRoadProtectionCase,'chooseGovRoadProtectionCase',_govRoadProtectionCase);
                vc.emit($props.emitLoadData,'listGovRoadProtectionCaseData',{
                    govRoadProtectionCaseId:_govRoadProtectionCase.govRoadProtectionCaseId
                });
                $('#chooseGovRoadProtectionCaseModel').modal('hide');
            },
            queryGovRoadProtectionCases:function(){
                vc.component._loadAllGovRoadProtectionCaseInfo(1,10,vc.component.chooseGovRoadProtectionCaseInfo._currentGovRoadProtectionCaseName);
            },
            _refreshChooseGovRoadProtectionCaseInfo:function(){
                vc.component.chooseGovRoadProtectionCaseInfo._currentGovRoadProtectionCaseName = "";
            }
        }

    });
})(window.vc);
