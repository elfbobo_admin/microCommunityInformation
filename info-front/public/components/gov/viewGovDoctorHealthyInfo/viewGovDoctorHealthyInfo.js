/**
    档案医生 组件
**/
(function(vc){

    vc.extends({
        propTypes: {
           callBackListener:vc.propTypes.string, //父组件名称
           callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            viewGovDoctorHealthyInfo:{
                index:0,
                flowComponent:'viewGovDoctorHealthyInfo',
                govDoctorId:'',
caId:'',
doctorNum:'',
idType:'',
idCard:'',
govPersonId:'',
personName:'',
personTel:'',
personSex:'',
birthday:'',
jobName:'',
titleName:'',
businessExpertise:'',
statusCd:'',
datasourceType:'',

            }
        },
        _initMethod:function(){
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovDoctorHealthyInfoData();
        },
        _initEvent:function(){
            vc.on('viewGovDoctorHealthyInfo','chooseGovDoctorHealthy',function(_app){
                vc.copyObject(_app, vc.component.viewGovDoctorHealthyInfo);
                vc.emit($props.callBackListener,$props.callBackFunction,vc.component.viewGovDoctorHealthyInfo);
            });

            vc.on('viewGovDoctorHealthyInfo', 'onIndex', function(_index){
                vc.component.viewGovDoctorHealthyInfo.index = _index;
            });

        },
        methods:{

            _openSelectGovDoctorHealthyInfoModel(){
                vc.emit('chooseGovDoctorHealthy','openChooseGovDoctorHealthyModel',{});
            },
            _openAddGovDoctorHealthyInfoModel(){
                vc.emit('addGovDoctorHealthy','openAddGovDoctorHealthyModal',{});
            },
            _loadGovDoctorHealthyInfoData:function(){

            }
        }
    });

})(window.vc);
