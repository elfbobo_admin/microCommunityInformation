(function(vc){
    vc.extends({
        propTypes: {
           emitChooseGovMeetingList:vc.propTypes.string,
           emitLoadData:vc.propTypes.string
        },
        data:{
            chooseGovMeetingListInfo:{
                govMeetingLists:[],
                _currentGovMeetingListName:'',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('chooseGovMeetingList','openChooseGovMeetingListModel',function(_param){
                $('#chooseGovMeetingListModel').modal('show');
                vc.component._refreshChooseGovMeetingListInfo();
                vc.component._loadAllGovMeetingListInfo(1,10,'');
            });
        },
        methods:{
            _loadAllGovMeetingListInfo:function(_page,_row,_name){
                var param = {
                    params:{
                        page:_page,
                        row:_row,
                        caId:vc.getCurrentCommunity().caId,
                        name:_name
                    }
                };

                //发送get请求
               vc.http.apiGet('/govMeetingList/queryGovMeetingList',
                             param,
                             function(json){
                                var _govMeetingListInfo = JSON.parse(json);
                                vc.component.chooseGovMeetingListInfo.govMeetingLists = _govMeetingListInfo.govMeetingLists;
                             },function(){
                                console.log('请求失败处理');
                             }
                           );
            },
            chooseGovMeetingList:function(_govMeetingList){
                if(_govMeetingList.hasOwnProperty('name')){
                     _govMeetingList.govMeetingListName = _govMeetingList.name;
                }
                vc.emit($props.emitChooseGovMeetingList,'chooseGovMeetingList',_govMeetingList);
                vc.emit($props.emitLoadData,'listGovMeetingListData',{
                    govMeetingListId:_govMeetingList.govMeetingListId
                });
                $('#chooseGovMeetingListModel').modal('hide');
            },
            queryGovMeetingLists:function(){
                vc.component._loadAllGovMeetingListInfo(1,10,vc.component.chooseGovMeetingListInfo._currentGovMeetingListName);
            },
            _refreshChooseGovMeetingListInfo:function(){
                vc.component.chooseGovMeetingListInfo._currentGovMeetingListName = "";
            }
        }

    });
})(window.vc);
