/**
    医疗分类 组件
**/
(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            viewGovMedicalClassifyInfo: {
                index: 0,
                flowComponent: 'viewGovMedicalClassifyInfo',
                medicalClassifyId: '',
                caId: '',
                classifyName: '',
                classifyType: '',
                seq: '',
                ramark: '',
                statusCd: '',

            }
        },
        _initMethod: function () {
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovMedicalClassifyInfoData();
        },
        _initEvent: function () {
            vc.on('viewGovMedicalClassifyInfo', 'chooseGovMedicalClassify', function (_app) {
                vc.copyObject(_app, vc.component.viewGovMedicalClassifyInfo);
                vc.emit($props.callBackListener, $props.callBackFunction, vc.component.viewGovMedicalClassifyInfo);
            });

            vc.on('viewGovMedicalClassifyInfo', 'onIndex', function (_index) {
                vc.component.viewGovMedicalClassifyInfo.index = _index;
            });

        },
        methods: {

            _openSelectGovMedicalClassifyInfoModel() {
                vc.emit('chooseGovMedicalClassify', 'openChooseGovMedicalClassifyModel', {});
            },
            _openAddGovMedicalClassifyInfoModel() {
                vc.emit('addGovMedicalClassify', 'openAddGovMedicalClassifyModal', {});
            },
            _loadGovMedicalClassifyInfoData: function () {

            }
        }
    });

})(window.vc);
