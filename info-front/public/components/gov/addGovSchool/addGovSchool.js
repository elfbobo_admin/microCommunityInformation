(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovSchoolInfo: {
                schoolId: '',
                caId: '',
                schoolName: '',
                schoolAddress: '',
                schoolType: '',
                areaCode: '',
                selectProv: '',
                selectCity: '',
                selectArea: '',
                studentNum: '',
                schoolmaster: '',
                masterTel: '',
                partSafetyLeader: '',
                partLeaderTel: '',
                safetyLeader: '',
                leaderTel: '',
                securityLeader: '',
                securityLeaderTel: '',
                securityPersonnelNum: '',
                areas: [],
                provs: [],
                citys: [],

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('addGovSchool', 'openAddGovSchoolModal', function () {
                vc.component._initArea('101', '0');
                $('#addGovSchoolModel').modal('show');
            });
        },
        methods: {
            addGovSchoolValidate() {
                return vc.validate.validate({
                    addGovSchoolInfo: vc.component.addGovSchoolInfo
                }, {
                    'addGovSchoolInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "区域ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "区域ID不能超过30"
                        },
                    ],
                    'addGovSchoolInfo.schoolName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "学校名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "60",
                            errInfo: "学校名称不能超过60"
                        },
                    ],
                    'addGovSchoolInfo.schoolAddress': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "学校地址不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "学校地址不能超过30"
                        },
                    ],
                    'addGovSchoolInfo.schoolType': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "学校办学类型不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "5",
                            errInfo: "学校办学类型不能超过5"
                        },
                    ],
                    'addGovSchoolInfo.areaCode': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "所属行政区不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "10",
                            errInfo: "所属行政区不能超过10"
                        },
                    ],
                    'addGovSchoolInfo.studentNum': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "在校学生数不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "6",
                            errInfo: "在校学生数不能超过6"
                        },
                    ],
                    'addGovSchoolInfo.schoolmaster': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "校长姓名不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "校长姓名不能超过30"
                        },
                    ],
                    'addGovSchoolInfo.masterTel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "校长联系方式不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "校长联系方式不能超过30"
                        },
                    ],
                    'addGovSchoolInfo.partSafetyLeader': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "分管安保负责人不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "分管安保负责人不能超过30"
                        },
                    ],
                    'addGovSchoolInfo.partLeaderTel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "分管安保负责人联系电话不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "分管安保负责人联系电话不能超过30"
                        },
                    ],
                    'addGovSchoolInfo.safetyLeader': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "安保负责人不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "安保负责人不能超过30"
                        },
                    ],
                    'addGovSchoolInfo.leaderTel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "安保负责人联系电话不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "安保负责人联系电话不能超过30"
                        },
                    ],
                    'addGovSchoolInfo.securityLeader': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "治安负责人不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "治安负责人不能超过30"
                        },
                    ],
                    'addGovSchoolInfo.securityLeaderTel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "治安负责人联系方式不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "治安负责人联系方式不能超过30"
                        },
                    ],
                    'addGovSchoolInfo.securityPersonnelNum': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "安全保卫人数不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "安全保卫人数不能超过30"
                        },
                    ],
                });
            },
            saveGovSchoolInfo: function () {
                vc.component.addGovSchoolInfo.caId = vc.getCurrentCommunity().caId;
                if (!vc.component.addGovSchoolValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovSchoolInfo);
                    $('#addGovSchoolModel').modal('hide');
                    return;
                }
                vc.http.apiPost(
                    '/govSchool/saveGovSchool',
                    JSON.stringify(vc.component.addGovSchoolInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovSchoolModel').modal('hide');
                            vc.component.clearAddGovSchoolInfo();
                            vc.emit('govSchoolManage', 'listGovSchool', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            getProv: function (_prov) {
                vc.component._initArea('202', _prov);
            },
            getCity: function (_city) {
                vc.component._initArea('303', _city);
            },
            getArea: function (_area) {
                vc.component.addGovSchoolInfo.areaCode = _area;
            },
            _initArea: function (_areaLevel, _parentAreaCode) { //加载区域
                var _param = {
                    params: {
                        areaLevel: _areaLevel,
                        parentAreaCode: _parentAreaCode
                    }
                };
                 //发送get请求
                vc.http.apiGet('/cityArea/getAreas',
                    _param,
                    function (json, res) {
                            var _tmpAreas = JSON.parse(json);
                            if (_areaLevel == '101') {
                                vc.component.addGovSchoolInfo.provs = _tmpAreas.data.areas;
                                vc.component.addGovSchoolInfo.citys = [];
                                vc.component.addGovSchoolInfo.areas = [];
                            } else if (_areaLevel == '202') {
                                vc.component.addGovSchoolInfo.citys = _tmpAreas.data.areas;
                                vc.component.addGovSchoolInfo.areas = [];
                            } else {
                                vc.component.addGovSchoolInfo.areas = _tmpAreas.data.areas;
                            }
                            return;
                        //vc.component.$emit('errorInfoEvent',json);
                    }, function (errInfo, error) {
                        console.log('请求失败处理', errInfo, error);
                        vc.toast("查询地区失败");
                    });
            },
            clearAddGovSchoolInfo: function () {
                vc.component.addGovSchoolInfo = {
                    schoolId: '',
                    caId: '',
                    schoolName: '',
                    schoolAddress: '',
                    schoolType: '',
                    areaCode: '',
                    selectProv: '',
                    selectCity: '',
                    selectArea: '',
                    studentNum: '',
                    schoolmaster: '',
                    masterTel: '',
                    partSafetyLeader: '',
                    partLeaderTel: '',
                    safetyLeader: '',
                    leaderTel: '',
                    securityLeader: '',
                    securityLeaderTel: '',
                    securityPersonnelNum: '',
                    areas: [],
                    provs: [],
                    citys: [],

                };
            }
        }
    });

})(window.vc);
