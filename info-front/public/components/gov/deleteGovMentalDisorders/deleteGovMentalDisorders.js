(function(vc,vm){

    vc.extends({
        data:{
            deleteGovMentalDisordersInfo:{

            }
        },
         _initMethod:function(){

         },
         _initEvent:function(){
             vc.on('deleteGovMentalDisorders','openDeleteGovMentalDisordersModal',function(_params){

                vc.component.deleteGovMentalDisordersInfo = _params;
                $('#deleteGovMentalDisordersModel').modal('show');

            });
        },
        methods:{
            deleteGovMentalDisorders:function(){
                vc.component.deleteGovMentalDisordersInfo.caId=vc.getCurrentCommunity().caId;
                vc.http.apiPost(
                    '/govMentalDisorders/deleteGovMentalDisorders',
                    JSON.stringify(vc.component.deleteGovMentalDisordersInfo),
                    {
                        emulateJSON:true
                     },
                     function(json,res){
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovMentalDisordersModel').modal('hide');
                            vc.emit('govMentalDisordersManage','listGovMentalDisorders',{});
                            return ;
                        }
                        vc.message(_json.msg);
                     },
                     function(errInfo,error){
                        console.log('请求失败处理');
                        vc.message(json);

                     });
            },
            closeDeleteGovMentalDisordersModel:function(){
                $('#deleteGovMentalDisordersModel').modal('hide');
            }
        }
    });

})(window.vc,window.vc.component);
