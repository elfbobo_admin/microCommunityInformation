(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovHomicideBasicInfo: {
                govHomicideId: '',
                caId: '',
                homicideNum: '',
                homicideName: '',
                happenTime: '',
                victimId: '',
                victimName: '',
                suspicionId: '',
                suspicionName: '',
                endTime: '',
                leadRamark: '',
                ramark: '',

            }
        },
        _initMethod: function () {
           
        },
        _initEvent: function () {
            vc.on('addGovHomicideBasic', 'openAddGovHomicideBasicModal', function () {
                vc.initDate('addHappenTime', function (_value) {
                    $that.addGovHomicideBasicInfo.happenTime = _value;
                });
                vc.initDate('addEndTime', function (_value) {
                    $that.addGovHomicideBasicInfo.endTime = _value;
                });
                $('#addGovHomicideBasicModel').modal('show');
            });
            vc.on('victimName','chooseGovPerson', function (_param) {
                $that.addGovHomicideBasicInfo.victimId = _param.govPersonId;
                $that.addGovHomicideBasicInfo.victimName = _param.personName;
            });
            vc.on('suspicionName','chooseGovPersonSuspicion', function (_param) {
                $that.addGovHomicideBasicInfo.suspicionId = _param.govPersonId;
                $that.addGovHomicideBasicInfo.suspicionName = _param.personName;
            });
        },
        methods: {
            addGovHomicideBasicValidate() {
                return vc.validate.validate({
                    addGovHomicideBasicInfo: vc.component.addGovHomicideBasicInfo
                }, {
                   
                    'addGovHomicideBasicInfo.homicideNum': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "命案编号不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "命案编号不能超过30"
                        },
                    ],
                    'addGovHomicideBasicInfo.homicideName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "命案名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "命案名称不能超过64"
                        },
                    ],
                    'addGovHomicideBasicInfo.happenTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "发生时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "发生时间不能超过时间类型"
                        },
                    ],
                    'addGovHomicideBasicInfo.victimId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "受害人ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "受害人ID不能超过30"
                        },
                    ],
                    'addGovHomicideBasicInfo.victimName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "受害人名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "102",
                            errInfo: "受害人名称不能超过102"
                        },
                    ],
                    'addGovHomicideBasicInfo.suspicionId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "嫌疑人ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "102",
                            errInfo: "嫌疑人ID不能超过102"
                        },
                    ],
                    'addGovHomicideBasicInfo.suspicionName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "嫌疑人名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "嫌疑人名称不能超过64"
                        },
                    ],
                    'addGovHomicideBasicInfo.endTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "结案日期不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "结案日期不能超过时间类型"
                        },
                    ],
                    'addGovHomicideBasicInfo.leadRamark': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "概要情况不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "概要情况不能超过1024"
                        },
                    ]

                });
            },
            saveGovHomicideBasicInfo: function () {
                if (!vc.component.addGovHomicideBasicValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }

                vc.component.addGovHomicideBasicInfo.caId = vc.getCurrentCommunity().caId;
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovHomicideBasicInfo);
                    $('#addGovHomicideBasicModel').modal('hide');
                    return;
                }
                vc.http.apiPost(
                    '/govHomicideBasic/saveGovHomicideBasic',
                    JSON.stringify(vc.component.addGovHomicideBasicInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovHomicideBasicModel').modal('hide');
                            vc.component.clearAddGovHomicideBasicInfo();
                            vc.emit('govHomicideBasicManage', 'listGovHomicideBasic', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            _closeAddGovHomicideBasic: function () {
                $that.clearAddGovHomicideBasicInfo();
                vc.emit('govHomicideBasicManage', 'listGovHomicideBasic', {});
            },
            _openAddChooseGovPersonModel: function () {
                let _caId = vc.getCurrentCommunity().caId;

                vc.emit('chooseGovPerson', 'openChooseGovPersonModel', _caId);
            },
            _openAddSuspicionChooseGovPersonModel: function () {
                let _caId = vc.getCurrentCommunity().caId;
  
                vc.emit('chooseGovPersonSuspicionName', 'openchooseGovPersonSuspicionModel', _caId);
            },
            clearAddGovHomicideBasicInfo: function () {
                vc.component.addGovHomicideBasicInfo = {
                    govHomicideId: '',
                    caId: '',
                    homicideNum: '',
                    homicideName: '',
                    happenTime: '',
                    victimId: '',
                    victimName: '',
                    suspicionId: '',
                    suspicionName: '',
                    endTime: '',
                    leadRamark: '',
                    ramark: '',

                };
            }
        }
    });

})(window.vc);
