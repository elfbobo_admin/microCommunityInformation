(function (vc, vm) {

    vc.extends({
        data: {
            editGovRenovationCheckInfo: {
                govCheckId: '',
                govRenovationId: '',
                caId: '',
                effectEvaluation: '',
                mainActivities: '',
                remediationEffect: '',
                remediationSummary: '',
                govAreaRenovations: []
            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('editGovRenovationCheck', 'openEditGovRenovationCheckModal', function (_params) {
                vc.component.refreshEditGovRenovationCheckInfo();
                $that._editGovAreaRenovations();
                $('#editGovRenovationCheckModel').modal('show');
                vc.copyObject(_params, vc.component.editGovRenovationCheckInfo);
                vc.component.editGovRenovationCheckInfo.caId = vc.getCurrentCommunity().caId;
            });
        },
        methods: {
            editGovRenovationCheckValidate: function () {
                return vc.validate.validate({
                    editGovRenovationCheckInfo: vc.component.editGovRenovationCheckInfo
                }, {
                    'editGovRenovationCheckInfo.govCheckId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "整治情况ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "整治情况ID不能超过30"
                        },
                    ],
                    'editGovRenovationCheckInfo.govRenovationId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "重点地区整治ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "重点地区整治ID不能超过30"
                        },
                    ],
                    'editGovRenovationCheckInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "区域ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "区域ID不能超过30"
                        },
                    ],
                    'editGovRenovationCheckInfo.effectEvaluation': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "效果评估不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "效果评估不能超过11"
                        },
                    ],
                    'editGovRenovationCheckInfo.mainActivities': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "主要活动不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "251",
                            errInfo: "主要活动不能超过251"
                        },
                    ],
                    'editGovRenovationCheckInfo.remediationEffect': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "整治效果不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "251",
                            errInfo: "整治效果不能超过251"
                        },
                    ],
                    'editGovRenovationCheckInfo.remediationSummary': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "整治总结不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "251",
                            errInfo: "整治总结不能超过251"
                        },
                    ]

                });
            },
            editGovRenovationCheck: function () {
                if (!vc.component.editGovRenovationCheckValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }

                vc.http.apiPost(
                    '/govRenovationCheck/updateGovRenovationCheck',
                    JSON.stringify(vc.component.editGovRenovationCheckInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovRenovationCheckModel').modal('hide');
                            vc.emit('govRenovationCheckManage', 'listGovRenovationCheck', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            _editGovAreaRenovations: function () {

                var param = {
                    params: {
                        page : 1,
                        row : 50,
                        caId : vc.getCurrentCommunity().caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govAreaRenovation/queryGovAreaRenovation',
                    param,
                    function (json, res) {
                        var _govAreaRenovationManageInfo = JSON.parse(json);
                        vc.component.editGovRenovationCheckInfo.govAreaRenovations = _govAreaRenovationManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            refreshEditGovRenovationCheckInfo: function () {
                vc.component.editGovRenovationCheckInfo = {
                    govCheckId: '',
                    govRenovationId: '',
                    caId: '',
                    effectEvaluation: '',
                    mainActivities: '',
                    remediationEffect: '',
                    remediationSummary: '',
                    govAreaRenovations: []

                }
            }
        }
    });

})(window.vc, window.vc.component);
