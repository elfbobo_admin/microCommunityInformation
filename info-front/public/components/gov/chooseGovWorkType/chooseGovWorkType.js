(function(vc){
    vc.extends({
        propTypes: {
           emitChooseGovWorkType:vc.propTypes.string,
           emitLoadData:vc.propTypes.string
        },
        data:{
            chooseGovWorkTypeInfo:{
                govWorkTypes:[],
                _currentGovWorkTypeName:'',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('chooseGovWorkType','openChooseGovWorkTypeModel',function(_param){
                $('#chooseGovWorkTypeModel').modal('show');
                vc.component._refreshChooseGovWorkTypeInfo();
                vc.component._loadAllGovWorkTypeInfo(1,10,'');
            });
        },
        methods:{
            _loadAllGovWorkTypeInfo:function(_page,_row,_name){
                var param = {
                    params:{
                        page:_page,
                        row:_row,
                        communityId:vc.getCurrentCommunity().communityId,
                        name:_name
                    }
                };

                //发送get请求
               vc.http.apiGet('govWorkType.listGovWorkTypes',
                             param,
                             function(json){
                                var _govWorkTypeInfo = JSON.parse(json);
                                vc.component.chooseGovWorkTypeInfo.govWorkTypes = _govWorkTypeInfo.govWorkTypes;
                             },function(){
                                console.log('请求失败处理');
                             }
                           );
            },
            chooseGovWorkType:function(_govWorkType){
                if(_govWorkType.hasOwnProperty('name')){
                     _govWorkType.govWorkTypeName = _govWorkType.name;
                }
                vc.emit($props.emitChooseGovWorkType,'chooseGovWorkType',_govWorkType);
                vc.emit($props.emitLoadData,'listGovWorkTypeData',{
                    govWorkTypeId:_govWorkType.govWorkTypeId
                });
                $('#chooseGovWorkTypeModel').modal('hide');
            },
            queryGovWorkTypes:function(){
                vc.component._loadAllGovWorkTypeInfo(1,10,vc.component.chooseGovWorkTypeInfo._currentGovWorkTypeName);
            },
            _refreshChooseGovWorkTypeInfo:function(){
                vc.component.chooseGovWorkTypeInfo._currentGovWorkTypeName = "";
            }
        }

    });
})(window.vc);
