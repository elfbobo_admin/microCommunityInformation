(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovParkingAreaInfo: {
                paId: '',
                govCommunityId: '',
                num: '',
                parkingCount: '',
                typeCd: '',
                remark: '',
                govCommunitys:[]

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('addGovParkingArea', 'openAddGovParkingAreaModal', function () {
                $that._listAddGovCommunitys();
                $('#addGovParkingAreaModel').modal('show');
            });
        },
        methods: {
            addGovParkingAreaValidate() {
                return vc.validate.validate({
                    addGovParkingAreaInfo: vc.component.addGovParkingAreaInfo
                }, {
                    'addGovParkingAreaInfo.govCommunityId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "小区不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "200",
                            errInfo: "小区超长"
                        },
                    ],
                    'addGovParkingAreaInfo.num': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "停车场编号不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "100",
                            errInfo: "停车场编号超长"
                        },
                    ],
                    'addGovParkingAreaInfo.parkingCount': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "车位数量不能为空"
                        },
                        {
                            limit: "num",
                            param: "100",
                            errInfo: "车位数量不是有效数字"
                        },
                    ],
                    'addGovParkingAreaInfo.typeCd': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "停车场类型不能为空"
                        },
                        {
                            limit: "num",
                            param: "100",
                            errInfo: "停车场类型不是有效数字"
                        },
                    ],
                    'addGovParkingAreaInfo.remark': [
                        {
                            limit: "maxLength",
                            param: "200",
                            errInfo: "备注内容不能超过200"
                        },
                    ],




                });
            },
            saveGovParkingAreaInfo: function () {
                if (!vc.component.addGovParkingAreaValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }

                vc.component.addGovParkingAreaInfo.caId = vc.getCurrentCommunity().caId;
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovParkingAreaInfo);
                    $('#addGovParkingAreaModel').modal('hide');
                    return;
                }

                vc.http.apiPost(
                    '/parkingArea/saveParkingArea',
                    JSON.stringify(vc.component.addGovParkingAreaInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovParkingAreaModel').modal('hide');
                            vc.component.clearAddGovParkingAreaInfo();
                            vc.emit('govParkingAreaManage', 'listGovParkingArea', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            _listAddGovCommunitys: function () {

                let param = {
                    params: {
                        page: 1,
                        row: 50,
                        caId: vc.getCurrentCommunity().caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govCommunity/queryGovCommunity',
                    param,
                    function (json, res) {
                        var _govCommunityManageInfo = JSON.parse(json);
                        vc.component.addGovParkingAreaInfo.govCommunitys = _govCommunityManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            clearAddGovParkingAreaInfo: function () {
                vc.component.addGovParkingAreaInfo = {
                    govCommunityId: '',
                    num: '',
                    parkingCount: '',
                    typeCd: '',
                    remark: '',

                };
            }
        }
    });

})(window.vc);
