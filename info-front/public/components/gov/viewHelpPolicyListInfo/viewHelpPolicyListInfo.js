(function (vc) {
    vc.extends({
        data: {
            viewHelpPolicyListInfo: {
                orgId: '',
                govPersonId: '',
                govHelpPolicyLists: []
            }
        },
        _initMethod: function () {
        },
        _initEvent: function () {
            vc.on('helpPolicyListInfo', 'switch', function (_param) {
                vc.component._refreshviewHelpPolicyListInfo();
                console.log(_param);
                vc.component._loadHelpPolicyListInfo(_param.govPersonId);
            });
        },
        methods: {
            _loadHelpPolicyListInfo: function (_personId) {
                if (!_personId) {
                    return;
                }

                var str = [];
               var params = {
                    page: 1,
                    row: 10,
                    caId: vc.getCurrentCommunity().caId                }
                str = _personId.split("&");
                if(str[1] == '6009'){
                    params['poorPersonId'] = str[0];
                }else if(str[1] == '6010'){
                    params['cadrePersonId'] = str[0];
                }
                var param = {
                     params
                    };
                //发送get请求
                vc.http.apiGet('/govHelpPolicyList/queryGovHelpPolicyList',
                    param,
                    function (json) {
                        var _labelInfo = JSON.parse(json);
                        $that.viewHelpPolicyListInfo.govHelpPolicyLists = _labelInfo.data;
                    }, function () {
                        console.log('请求失败处理');
                    }
                );
            },
            _refreshviewHelpPolicyListInfo: function () {
                $that.viewHelpPolicyListInfo = {
                    orgId: '',
                    govPersonId: '',
                    govHelpPolicyLists: []
                }
            }
        }

    });
})(window.vc);
