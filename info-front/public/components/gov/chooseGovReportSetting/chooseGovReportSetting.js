(function(vc){
    vc.extends({
        propTypes: {
           emitChooseGovReportSetting:vc.propTypes.string,
           emitLoadData:vc.propTypes.string
        },
        data:{
            chooseGovReportSettingInfo:{
                govReportSettings:[],
                _currentGovReportSettingName:'',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('chooseGovReportSetting','openChooseGovReportSettingModel',function(_param){
                $('#chooseGovReportSettingModel').modal('show');
                vc.component._refreshChooseGovReportSettingInfo();
                vc.component._loadAllGovReportSettingInfo(1,10,'');
            });
        },
        methods:{
            _loadAllGovReportSettingInfo:function(_page,_row,_name){
                var param = {
                    params:{
                        page:_page,
                        row:_row,
                        communityId:vc.getCurrentCommunity().communityId,
                        name:_name
                    }
                };

                //发送get请求
               vc.http.apiGet('govReportSetting.listGovReportSettings',
                             param,
                             function(json){
                                var _govReportSettingInfo = JSON.parse(json);
                                vc.component.chooseGovReportSettingInfo.govReportSettings = _govReportSettingInfo.govReportSettings;
                             },function(){
                                console.log('请求失败处理');
                             }
                           );
            },
            chooseGovReportSetting:function(_govReportSetting){
                if(_govReportSetting.hasOwnProperty('name')){
                     _govReportSetting.govReportSettingName = _govReportSetting.name;
                }
                vc.emit($props.emitChooseGovReportSetting,'chooseGovReportSetting',_govReportSetting);
                vc.emit($props.emitLoadData,'listGovReportSettingData',{
                    govReportSettingId:_govReportSetting.govReportSettingId
                });
                $('#chooseGovReportSettingModel').modal('hide');
            },
            queryGovReportSettings:function(){
                vc.component._loadAllGovReportSettingInfo(1,10,vc.component.chooseGovReportSettingInfo._currentGovReportSettingName);
            },
            _refreshChooseGovReportSettingInfo:function(){
                vc.component.chooseGovReportSettingInfo._currentGovReportSettingName = "";
            }
        }

    });
})(window.vc);
