(function(vc,vm){

    vc.extends({
        data:{
            deleteGovDrugInfo:{

            }
        },
         _initMethod:function(){

         },
         _initEvent:function(){
             vc.on('deleteGovDrug','openDeleteGovDrugModal',function(_params){

                vc.component.deleteGovDrugInfo = _params;
                $('#deleteGovDrugModel').modal('show');

            });
        },
        methods:{
            deleteGovDrug:function(){
                vc.component.deleteGovDrugInfo.caId=vc.getCurrentCommunity().caId;
                vc.http.apiPost(
                    '/govDrug/deleteGovDrug',
                    JSON.stringify(vc.component.deleteGovDrugInfo),
                    {
                        emulateJSON:true
                     },
                     function(json,res){
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovDrugModel').modal('hide');
                            vc.emit('govDrugManage','listGovDrug',{});
                            return ;
                        }
                        vc.message(_json.msg);
                     },
                     function(errInfo,error){
                        console.log('请求失败处理');
                        vc.message(json);

                     });
            },
            closeDeleteGovDrugModel:function(){
                $('#deleteGovDrugModel').modal('hide');
            }
        }
    });

})(window.vc,window.vc.component);
