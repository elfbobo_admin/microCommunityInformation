(function(vc){

    vc.extends({
        propTypes: {
               callBackListener:vc.propTypes.string, //父组件名称
               callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            addGovGuideInfo:{
                wgId:'',
                caId:'',
                guideType:'',
                guideName:'',
                flow:'',
                data:'',
                person:'',
                link:'',
                state:'',
                subscribe:'',

            }
        },
         _initMethod:function(){
            $that._initAddGovGuideInfoFlow();
         },
         _initEvent:function(){
            vc.on('addGovGuide','openAddGovGuideModal',function(){
                $('#addGovGuideModel').modal('show');
            });
        },
        methods:{
            addGovGuideValidate(){
                return vc.validate.validate({
                    addGovGuideInfo:vc.component.addGovGuideInfo
                },{
'addGovGuideInfo.guideType':[
{
                            limit:"required",
                            param:"",
                            errInfo:"事项类型不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"30",
                            errInfo:"事项类型ID超长"
                        },
                    ],
'addGovGuideInfo.guideName':[
{
                            limit:"required",
                            param:"",
                            errInfo:"事项名称不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"64",
                            errInfo:"事项名称超长"
                        },
                    ],
'addGovGuideInfo.flow':[
{
                            limit:"required",
                            param:"",
                            errInfo:"办事流程不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"4000",
                            errInfo:"办事流程超长"
                        },
                    ],
'addGovGuideInfo.data':[
{
                            limit:"required",
                            param:"",
                            errInfo:"办事材料不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"4000",
                            errInfo:"办事材料超长"
                        },
                    ],
'addGovGuideInfo.person':[
{
                            limit:"required",
                            param:"",
                            errInfo:"咨询人不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"64",
                            errInfo:"咨询人名称太长"
                        },
                    ],
'addGovGuideInfo.link':[
{
                            limit:"required",
                            param:"",
                            errInfo:"咨询电话不能为空"
                        },
                        {
                            limit: "phone",
                            param: "",
                            errInfo: "咨询电话太长"
                        },
                    ],
'addGovGuideInfo.state':[
{
                            limit:"required",
                            param:"",
                            errInfo:"对外公示不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"30",
                            errInfo:"对外公示ID超长"
                        },
                    ],
'addGovGuideInfo.subscribe':[
{
                            limit:"required",
                            param:"",
                            errInfo:"是否开启预约不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"30",
                            errInfo:"上级组织ID超长"
                        },
                    ],




                });
            },
            saveGovGuideInfo:function(){
                if(!vc.component.addGovGuideValidate()){
                    vc.toast(vc.validate.errInfo);

                    return ;
                }
                vc.component.addGovGuideInfo.caId = vc.getCurrentCommunity().caId;
                //不提交数据将数据 回调给侦听处理
                if(vc.notNull($props.callBackListener)){
                    vc.emit($props.callBackListener,$props.callBackFunction,vc.component.addGovGuideInfo);
                    $('#addGovGuideModel').modal('hide');
                    return ;
                }

                vc.http.apiPost(
                    '/govWorkGuide/saveGovWorkGuide',
                    JSON.stringify(vc.component.addGovGuideInfo),
                    {
                        emulateJSON:true
                     },
                     function(json,res){
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovGuideModel').modal('hide');
                            vc.component.clearAddGovGuideInfo();
                            vc.emit('govGuideManage','listGovGuide',{});

                            return ;
                        }
                        vc.message(_json.msg);

                     },
                     function(errInfo,error){
                        console.log('请求失败处理');

                        vc.message(errInfo);

                     });
            },
            clearAddGovGuideInfo:function(){
                vc.component.addGovGuideInfo = {
                                            caId:'',
                                            guideType:'',
                                            guideName:'',
                                            flow:'',
                                            data:'',
                                            person:'',
                                            link:'',
                                            state:'',
                                            subscribe:'',

                                        };
            },
            _initAddGovGuideInfoFlow: function () {
                let $summernote = $('.addGovGuideInfoFlow').summernote({
                    lang: 'zh-CN',
                    height: 300,
                    placeholder: '必填，请输入办事流程',
                    callbacks: {
                        onImageUpload: function (files, editor, $editable) {
                            $that.sendAddFile($summernote, files);
                        },
                        onChange: function (contexts, $editable) {
                            $that.addGovGuideInfo.flow = contexts;
                        }
                    },
                    toolbar: [
                        ['style', ['style']],
                        ['font', ['bold', 'italic', 'underline', 'clear']],
                        ['fontname', ['fontname']],
                        ['color', ['color']],
                        ['para', ['ul', 'ol', 'paragraph']],
                        ['height', ['height']],
                        ['table', ['table']],
                        ['insert', ['link', 'picture']],
                        ['view', ['fullscreen', 'codeview']],
                        ['help', ['help']]
                    ],
                });
            },
        }
    });

})(window.vc);
