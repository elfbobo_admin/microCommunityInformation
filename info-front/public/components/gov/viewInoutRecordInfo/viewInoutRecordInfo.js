(function (vc) {
    vc.extends({
        data: {
            viewInoutRecordInfo: {
                orgId: '',
                govPersonId: '',
                govInouts: []
            }
        },
        _initMethod: function () {
        },
        _initEvent: function () {
            vc.on('inoutRecordInfo', 'switch', function (_param) {
                vc.component._refreshviewGovInoutRecordInfo();
                console.log(_param);
                vc.component._loadInoutRecordInfo(_param.govPersonId);
            });
        },
        methods: {
            _loadInoutRecordInfo: function (_personId) {
                var param = {
                    params: {
                        page: 1,
                        row: 10,
                        caId: vc.getCurrentCommunity().caId,
                        tel: _personId
                    }
                };
                //发送get请求
                vc.http.apiGet('/govPersonInoutRecord/queryGovPersonInoutRecord',
                    param,
                    function (json) {
                        var _labelInfo = JSON.parse(json);
                        $that.viewInoutRecordInfo.govInouts = _labelInfo.data;
                    }, function () {
                        console.log('请求失败处理');
                    }
                );
            },
            _refreshviewGovInoutRecordInfo: function () {
                $that.viewInoutRecordInfo = {
                    orgId: '',
                    govPersonId: '',
                    govInouts: []
                }
            }
        }

    });
})(window.vc);
