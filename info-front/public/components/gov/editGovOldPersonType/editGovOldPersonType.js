(function (vc, vm) {

    vc.extends({
        data: {
            editGovOldPersonTypeInfo: {
                typeId: '',
                caId: '',
                typeName: '',
                ramark:''
            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('editGovOldPersonType', 'openEditGovOldPersonTypeModal', function (_params) {
                vc.component.refreshEditGovOldPersonTypeInfo();
                $('#editGovOldPersonTypeModel').modal('show');
                vc.copyObject(_params, vc.component.editGovOldPersonTypeInfo);
                vc.component.editGovOldPersonTypeInfo.caId = vc.getCurrentCommunity().caId;
            });
        },
        methods: {
            editGovOldPersonTypeValidate: function () {
                return vc.validate.validate({
                    editGovOldPersonTypeInfo: vc.component.editGovOldPersonTypeInfo
                }, {
                    'editGovOldPersonTypeInfo.typeId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "老人类型ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "老人类型ID不能超过30"
                        },
                    ],
                    'editGovOldPersonTypeInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "区域ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "区域ID不能超过30"
                        },
                    ],
                    'editGovOldPersonTypeInfo.typeName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "类型名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "类型名称不能超过64"
                        },
                    ],
                    'editGovOldPersonTypeInfo.ramark': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "备注不能为空"
                        }
                    ]
                });
            },
            editGovOldPersonType: function () {
                if (!vc.component.editGovOldPersonTypeValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }

                vc.http.apiPost(
                    '/govOldPersonType/updateGovOldPersonType',
                    JSON.stringify(vc.component.editGovOldPersonTypeInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovOldPersonTypeModel').modal('hide');
                            vc.emit('govOldPersonTypeManage', 'listGovOldPersonType', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            refreshEditGovOldPersonTypeInfo: function () {
                vc.component.editGovOldPersonTypeInfo = {
                    typeId: '',
                    caId: '',
                    typeName: '',
                    ramark:''
                }
            }
        }
    });

})(window.vc, window.vc.component);
