/**
    信访类型表 组件
**/
(function(vc){

    vc.extends({
        propTypes: {
           callBackListener:vc.propTypes.string, //父组件名称
           callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            viewGovPetitionTypeInfo:{
                index:0,
                flowComponent:'viewGovPetitionTypeInfo',
                typeId:'',
typeName:'',
typeDesc:'',
seq:'',
caId:'',
statusCd:'',

            }
        },
        _initMethod:function(){
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovPetitionTypeInfoData();
        },
        _initEvent:function(){
            vc.on('viewGovPetitionTypeInfo','chooseGovPetitionType',function(_app){
                vc.copyObject(_app, vc.component.viewGovPetitionTypeInfo);
                vc.emit($props.callBackListener,$props.callBackFunction,vc.component.viewGovPetitionTypeInfo);
            });

            vc.on('viewGovPetitionTypeInfo', 'onIndex', function(_index){
                vc.component.viewGovPetitionTypeInfo.index = _index;
            });

        },
        methods:{

            _openSelectGovPetitionTypeInfoModel(){
                vc.emit('chooseGovPetitionType','openChooseGovPetitionTypeModel',{});
            },
            _openAddGovPetitionTypeInfoModel(){
                vc.emit('addGovPetitionType','openAddGovPetitionTypeModal',{});
            },
            _loadGovPetitionTypeInfoData:function(){

            }
        }
    });

})(window.vc);
