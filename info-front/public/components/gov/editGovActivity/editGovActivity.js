(function(vc,vm){

    vc.extends({
        data:{
            editGovActivityInfo:{
                actId:'',
                typeId:'',
                caId:'',
                actName:'',
                actTime:'',
                actAddress:'',
                personCount:'',
                context:'',
                contactName:'',
                contactLink:'',

            }
        },
         _initMethod:function(){
            vc.component._initEditActTime();
         },
         _initEvent:function(){
             vc.on('editGovActivity','openEditGovActivityModal',function(_params){
                vc.component.refreshEditGovActivityInfo();
                $('#editGovActivityModel').modal('show');
                vc.copyObject(_params, vc.component.editGovActivityInfo );
                vc.component.editGovActivityInfo.caId = vc.getCurrentCommunity().caId;
            });
        },
        methods:{
            editGovActivityValidate:function(){
                        return vc.validate.validate({
                            editGovActivityInfo:vc.component.editGovActivityInfo
                        },{
                            'editGovActivityInfo.caId':[
{
                            limit:"required",
                            param:"",
                            errInfo:"所属区域不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"30",
                            errInfo:"所属区域超长"
                        },
                    ],
'editGovActivityInfo.actName':[
{
                            limit:"required",
                            param:"",
                            errInfo:"活动名称不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"64",
                            errInfo:"活动名称超长"
                        },
                    ],
'editGovActivityInfo.actTime':[
{
                            limit:"required",
                            param:"",
                            errInfo:"活动时间不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"128",
                            errInfo:"活动时间超长"
                        },
                    ],
'editGovActivityInfo.actAddress':[
{
                            limit:"required",
                            param:"",
                            errInfo:"活动地点不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"512",
                            errInfo:"活动地点超长"
                        },
                    ],
'editGovActivityInfo.personCount':[
{
                            limit:"required",
                            param:"",
                            errInfo:"人数不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"20",
                            errInfo:"人数超长"
                        },
                    ],
'editGovActivityInfo.context':[
{
                            limit:"required",
                            param:"",
                            errInfo:"活动内容不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"2048",
                            errInfo:"活动内容太长"
                        },
                    ],
'editGovActivityInfo.contactName':[
{
                            limit:"required",
                            param:"",
                            errInfo:"联系人不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"64",
                            errInfo:"联系人名称太长"
                        },
                    ],
'editGovActivityInfo.contactLink':[
{
                            limit:"required",
                            param:"",
                            errInfo:"联系电话不能为空"
                        },
                        {
                            limit: "phone",
                            param: "",
                            errInfo: "联系电话太长"
                        },
                    ],
'editGovActivityInfo.actId':[
{
                            limit:"required",
                            param:"",
                            errInfo:"活动ID不能为空"
                        }]

                        });
             },
            editGovActivity:function(){
                if(!vc.component.editGovActivityValidate()){
                    vc.toast(vc.validate.errInfo);
                    return ;
                }

                vc.http.apiPost(
                    '/govActivity/updateGovActivity',
                    JSON.stringify(vc.component.editGovActivityInfo),
                    {
                        emulateJSON:true
                     },
                     function(json,res){
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovActivityModel').modal('hide');
                             vc.emit('govActivityManage','listGovActivity',{});
                            return ;
                        }
                        vc.message(_json.msg);
                     },
                     function(errInfo,error){
                        console.log('请求失败处理');

                        vc.message(errInfo);
                     });
            },
            _initEditActTime: function () {
                $('.editActTime').datetimepicker({
                    language: 'zh-CN',
                    fontAwesome: 'fa',
                    format: 'yyyy-mm-dd hh:ii:ss',
                    initTime: true,
                    initialDate: new Date(),
                    autoClose: 1,
                    todayBtn: true
                });
                $('.editActTime').datetimepicker()
                    .on('changeDate', function (ev) {
                        var value = $(".editActTime").val();
                        vc.component.editGovActivityInfo.actTime = value;
                    });
            },
            refreshEditGovActivityInfo:function(){
                vc.component.editGovActivityInfo= {
                  actId:'',
                    caId:'',
                    typeId:'',
                    actName:'',
                    actTime:'',
                    actAddress:'',
                    personCount:'',
                    context:'',
                    contactName:'',
                    contactLink:'',

                }
            }
        }
    });

})(window.vc,window.vc.component);
