(function(vc,vm){

    vc.extends({
        data:{
            deleteGovPersonDieRecordInfo:{

            }
        },
         _initMethod:function(){

         },
         _initEvent:function(){
             vc.on('deleteGovPersonDieRecord','openDeleteGovPersonDieRecordModal',function(_params){

                vc.component.deleteGovPersonDieRecordInfo = _params;
                $('#deleteGovPersonDieRecordModel').modal('show');

            });
        },
        methods:{
            deleteGovPersonDieRecord:function(){
                vc.component.deleteGovPersonDieRecordInfo.caId=vc.getCurrentCommunity().caId;
                vc.http.apiPost(
                    '/govPersonDieRecord/deleteGovPersonDieRecord',
                    JSON.stringify(vc.component.deleteGovPersonDieRecordInfo),
                    {
                        emulateJSON:true
                     },
                     function(json,res){
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovPersonDieRecordModel').modal('hide');
                            vc.emit('govPersonDieRecordManage','listGovPersonDieRecord',{});
                            return ;
                        }
                        vc.message(_json.msg);
                     },
                     function(errInfo,error){
                        console.log('请求失败处理');
                        vc.message(json);

                     });
            },
            closeDeleteGovPersonDieRecordModel:function(){
                $('#deleteGovPersonDieRecordModel').modal('hide');
            }
        }
    });

})(window.vc,window.vc.component);
