(function (vc, vm) {

    vc.extends({
        data: {
            editGovHealthTermInfo: {
                termId: '',
                termName: '',
                caId: '',
                seq: ''
            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('editGovHealthTerm', 'openEditGovHealthTermModal', function (_params) {
                vc.component.refreshEditGovHealthTermInfo();
                $('#editGovHealthTermModel').modal('show');
                vc.copyObject(_params, vc.component.editGovHealthTermInfo);
                vc.component.editGovHealthTermInfo.caId = vc.getCurrentCommunity().caId;
            });
        },
        methods: {
            editGovHealthTermValidate: function () {
                return vc.validate.validate({
                    editGovHealthTermInfo: vc.component.editGovHealthTermInfo
                }, {
                    'editGovHealthTermInfo.termId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "体检项ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "体检项ID不能超过30"
                        },
                    ],
                    'editGovHealthTermInfo.termName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "体检项不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "体检项不能超过30"
                        },
                    ],
                    'editGovHealthTermInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "区域ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "区域ID不能超过30"
                        },
                    ],
                    'editGovHealthTermInfo.seq': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "显示顺序不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "显示顺序不能超过11"
                        },
                    ]

                });
            },
            editGovHealthTerm: function () {
                if (!vc.component.editGovHealthTermValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }

                vc.http.apiPost(
                    '/govHealthTerm/updateGovHealthTerm',
                    JSON.stringify(vc.component.editGovHealthTermInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovHealthTermModel').modal('hide');
                            vc.emit('govHealthTermManage', 'listGovHealthTerm', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            refreshEditGovHealthTermInfo: function () {
                vc.component.editGovHealthTermInfo = {
                    termId: '',
                    termName: '',
                    caId: '',
                    seq: ''
                }
            }
        }
    });

})(window.vc, window.vc.component);
