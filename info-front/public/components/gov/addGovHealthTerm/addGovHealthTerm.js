(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovHealthTermInfo: {
                termId: '',
                termName: '',
                caId: '',
                seq: ''
            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('addGovHealthTerm', 'openAddGovHealthTermModal', function () {
                $('#addGovHealthTermModel').modal('show');
            });
        },
        methods: {
            addGovHealthTermValidate() {
                return vc.validate.validate({
                    addGovHealthTermInfo: vc.component.addGovHealthTermInfo
                }, {
                    'addGovHealthTermInfo.termName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "体检项不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "体检项不能超过30"
                        },
                    ],
                    'addGovHealthTermInfo.seq': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "显示顺序不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "显示顺序不能超过11"
                        },
                    ]




                });
            },
            saveGovHealthTermInfo: function () {
                if (!vc.component.addGovHealthTermValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }

                vc.component.addGovHealthTermInfo.caId = vc.getCurrentCommunity().caId;
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovHealthTermInfo);
                    $('#addGovHealthTermModel').modal('hide');
                    return;
                }
                vc.http.apiPost(
                    '/govHealthTerm/saveGovHealthTerm',
                    JSON.stringify(vc.component.addGovHealthTermInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovHealthTermModel').modal('hide');
                            vc.component.clearAddGovHealthTermInfo();
                            vc.emit('govHealthTermManage', 'listGovHealthTerm', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            clearAddGovHealthTermInfo: function () {
                vc.component.addGovHealthTermInfo = {
                    termId: '',
                    termName: '',
                    caId: '',
                    seq: ''
                };
            }
        }
    });

})(window.vc);
