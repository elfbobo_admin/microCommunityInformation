(function(vc,vm){

    vc.extends({
        data:{
            deleteGovActivityInfo:{

            }
        },
         _initMethod:function(){

         },
         _initEvent:function(){
             vc.on('deleteGovActivity','openDeleteGovActivityModal',function(_params){

                vc.component.deleteGovActivityInfo = _params;
                $('#deleteGovActivityModel').modal('show');

            });
        },
        methods:{
            deleteGovActivity:function(){
                //vc.component.deleteGovActivityInfo.communityId=vc.getCurrentCommunity().communityId;
                vc.http.apiPost(
                    '/govActivity/deleteGovActivity',
                    JSON.stringify(vc.component.deleteGovActivityInfo),
                    {
                        emulateJSON:true
                     },
                     function(json,res){
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovActivityModel').modal('hide');
                            vc.emit('govActivityManage','listGovActivity',{});
                            return ;
                        }
                        vc.message(_json.msg);
                     },
                     function(errInfo,error){
                        console.log('请求失败处理');
                        vc.message(json);

                     });
            },
            closeDeleteGovActivityModel:function(){
                $('#deleteGovActivityModel').modal('hide');
            }
        }
    });

})(window.vc,window.vc.component);
