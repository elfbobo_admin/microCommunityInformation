(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovHelpPolicyInfo: {
                govHelpId: '',
                caId: '',
                helpName: '',
                helpTime: '',
                helpBrief: ''
            }
        },
        _initMethod: function () {
            vc.initDateTime('helpTimes', function (_value) {
                $that.addGovHelpPolicyInfo.helpTime = _value;
            });
        },
        _initEvent: function () {
            vc.on('addGovHelpPolicy', 'openAddGovHelpPolicyModal', function () {
                $('#addGovHelpPolicyModel').modal('show');

                $that._initNoticeInfo();
            });
        },
        methods: {
            addGovHelpPolicyValidate() {
                return vc.validate.validate({
                    addGovHelpPolicyInfo: vc.component.addGovHelpPolicyInfo
                }, {
                    'addGovHelpPolicyInfo.helpName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "政策名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "政策名称不能超过64"
                        },
                    ],
                    'addGovHelpPolicyInfo.helpTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "开始时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "开始时间不能超过时间类型"
                        },
                    ],
                    'addGovHelpPolicyInfo.helpBrief': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "政策内容不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "政策内容不能超过1024"
                        },
                    ]



                });
            },
            saveGovHelpPolicyInfo: function () {
                if (!vc.component.addGovHelpPolicyValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }

                vc.component.addGovHelpPolicyInfo.caId = vc.getCurrentCommunity().caId;
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovHelpPolicyInfo);
                    $('#addGovHelpPolicyModel').modal('hide');
                    return;
                }
                vc.http.apiPost(
                    '/govHelpPolicy/saveGovHelpPolicy',
                    JSON.stringify(vc.component.addGovHelpPolicyInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovHelpPolicyModel').modal('hide');
                            vc.component.clearAddGovHelpPolicyInfo();
                            vc.emit('govHelpPolicyManage', 'listGovHelpPolicy', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            _initNoticeInfo: function () {


                var $summernote = $('.summernote').summernote({
                    lang: 'zh-CN',
                    height: 300,
                    placeholder: '必填，请输入政策内容',
                    callbacks: {
                        onImageUpload: function (files, editor, $editable) {
                            $that.sendAddFile($summernote, files[0]);
                        },
                        onChange: function (contents, $editable) {
                            vc.component.addGovHelpPolicyInfo.helpBrief = contents;
                        }
                    },
                    toolbar: [
                        ['style', ['style']],
                        ['font', ['bold', 'italic', 'underline', 'clear']],
                        ['fontname', ['fontname']],
                        ['color', ['color']],
                        ['para', ['ul', 'ol', 'paragraph']],
                        ['height', ['height']],
                        ['table', ['table']],
                        ['insert', ['link', 'picture']],
                        ['view', ['fullscreen', 'codeview']],
                        ['help', ['help']]
                    ],
                });

            },
            sendAddFile: function ($summernote, files) {
                console.log('上传图片', files);

                var param = new FormData();
                param.append("uploadFile", files);
                vc.http.uploadImage(
                    param,
                    {
                        emulateJSON: true,
                        //添加请求头
                        headers: {
                            "Content-Type": "multipart/form-data"
                        }
                    },
                    function (json, res) {
                        if (res.status == 200) {
                            var data = JSON.parse(json);
                            $summernote.summernote('insertImage', data.data);
                            return;
                        }
                        vc.toast(json);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');
                        vc.toast(errInfo);
                    });

            },
            closeAddGovHelpPolicy: function () {
                vc.emit('govHelpPolicyManage', 'listGovHelpPolicy', {});

            },
            clearAddGovHelpPolicyInfo: function () {
                vc.component.addGovHelpPolicyInfo = {
                    govHelpId: '',
                    caId: '',
                    helpName: '',
                    helpTime: '',
                    helpBrief: ''
                };
            }
        }
    });

})(window.vc);
