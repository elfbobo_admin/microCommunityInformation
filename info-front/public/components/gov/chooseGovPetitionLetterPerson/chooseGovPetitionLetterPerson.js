(function (vc) {
    vc.extends({
        propTypes: {
            emitChooseGovPetitionPerson: vc.propTypes.string,
            emitLoadData: vc.propTypes.string
        },
        data: {
            chooseGovPetitionPersonInfo: {
                govPersons: [],
                _currentGovPetitionName: '',
            }
        },
        _initMethod: function () {
        },
        _initEvent: function () {
            vc.on('chooseGovPetitionPerson', 'openChooseGovPetitionPersonModel', function (_param) {
                $('#chooseGovPetitionPersonModel').modal('show');
                vc.component._refreshChooseGovPetitionInfo();
                vc.component._loadPersonInfo(1, 10, '');
            });
        },
        methods: {
            _loadPersonInfo: function (_page, _row, _name) {
                var param = {
                    params: {
                        page: _page,
                        row: _row,
                        caId: vc.getCurrentCommunity().caId,
                        labelCd : "6008",
                        personName: _name
                    }
                };
                //发送get请求
                vc.http.apiGet('/govPerson/queryLabelGovPerson',
                    param,
                    function (json) {
                        var _govPersonInfo = JSON.parse(json);
                        vc.component.chooseGovPetitionPersonInfo.govPersons = _govPersonInfo.data;
                    }, function () {
                        console.log('请求失败处理');
                    }
                );
            },
            chooseGovPetitionPerson: function (_govPerson) {
                if (_govPerson.hasOwnProperty('personName')) {
                    _govPerson.personName = _govPerson.personName;
                    _govPerson.idCard = _govPerson.idCard;
                    _govPerson.personTel = _govPerson.personTel;
                    _govPerson.petitionPersonId = _govPerson.petitionPersonId;
                }
                vc.emit($props.emitChooseGovPetitionPerson, 'chooseGovPetitionPerson', _govPerson);
                vc.emit($props.emitLoadData, 'listGovPetitionPersonData', {
                    govPetitionPersonId: _govPerson.govPetitionPersonId
                });
                $('#chooseGovPetitionPersonModel').modal('hide');
            },
            queryGovPetition: function () {
                vc.component._loadPersonInfo(1, 10, vc.component.chooseGovPetitionPersonInfo._currentGovPetitionName);
            },
            _refreshChooseGovPetitionInfo: function () {
                vc.component.chooseGovPetitionPersonInfo._currentGovPetitionName = "";
            }
        }

    });
})(window.vc);
