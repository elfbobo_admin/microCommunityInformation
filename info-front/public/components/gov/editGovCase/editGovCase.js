(function (vc, vm) {

    vc.extends({
        data: {
            editGovCaseInfo: {
                caseId: '',
                caseId: '',
                govVictimId: '',
                victimName: '',
                caseName: '',
                caId: '',
                caseNum: '',
                happenTime: '',
                govSuspectId: '',
                suspectName: '',
                settleTime: '',
                briefCondition: '',
                ramark: '',
                statusCd: '',

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('editGovCase', 'openEditGovCaseModal', function (_params) {
                vc.component.refreshEditGovCaseInfo();
                $('#editGovCaseModel').modal('show');
                vc.copyObject(_params, vc.component.editGovCaseInfo);
                vc.component.editGovCaseInfo.caId = vc.getCurrentCommunity().caId;
            });
        },
        methods: {
            editGovCaseValidate: function () {
                return vc.validate.validate({
                    editGovCaseInfo: vc.component.editGovCaseInfo
                }, {
                    'editGovCaseInfo.caseId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "案件ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "案件ID不能超过30"
                        },
                    ],
                    'editGovCaseInfo.govVictimId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "受害人ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "受害人ID不能超过30"
                        },
                    ],
                    'editGovCaseInfo.victimName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "受害人名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "受害人名称不能超过64"
                        },
                    ],
                    'editGovCaseInfo.caseName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "案件名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "案件名称不能超过64"
                        },
                    ],
                    'editGovCaseInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "区域ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "区域ID不能超过30"
                        },
                    ],
                    'editGovCaseInfo.caseNum': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "案件编号不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "32",
                            errInfo: "案件编号不能超过32"
                        },
                    ],
                    'editGovCaseInfo.happenTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "发生日期不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "发生日期不能超过时间类型"
                        },
                    ],
                    'editGovCaseInfo.govSuspectId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "嫌疑人ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "嫌疑人ID不能超过30"
                        },
                    ],
                    'editGovCaseInfo.suspectName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "嫌疑人名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "嫌疑人名称不能超过64"
                        },
                    ],
                    'editGovCaseInfo.settleTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "结案日期不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "结案日期不能超过时间类型"
                        },
                    ],
                    'editGovCaseInfo.briefCondition': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "简要情况不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "简要情况不能超过1024"
                        },
                    ],
                    'editGovCaseInfo.ramark': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "备注不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "备注不能超过1024"
                        },
                    ],
                    'editGovCaseInfo.statusCd': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "数据状态不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "2",
                            errInfo: "数据状态不能超过2"
                        },
                    ],
                    'editGovCaseInfo.caseId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "命案基本信息ID不能为空"
                        }]

                });
            },
            editGovCase: function () {
                if (!vc.component.editGovCaseValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }

                vc.http.apiPost(
                    '/govCase/updateGovCase',
                    JSON.stringify(vc.component.editGovCaseInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovCaseModel').modal('hide');
                            vc.emit('govCaseManage', 'listGovCase', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            refreshEditGovCaseInfo: function () {
                vc.component.editGovCaseInfo = {
                    caseId: '',
                    caseId: '',
                    govVictimId: '',
                    victimName: '',
                    caseName: '',
                    caId: '',
                    caseNum: '',
                    happenTime: '',
                    govSuspectId: '',
                    suspectName: '',
                    settleTime: '',
                    briefCondition: '',
                    ramark: '',
                    statusCd: '',

                }
            }
        }
    });

})(window.vc, window.vc.component);
