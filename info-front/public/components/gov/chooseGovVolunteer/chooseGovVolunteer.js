(function(vc){
    vc.extends({
        propTypes: {
           emitChooseGovVolunteer:vc.propTypes.string,
           emitLoadData:vc.propTypes.string
        },
        data:{
            chooseGovVolunteerInfo:{
                govVolunteers:[],
                _currentGovVolunteerName:'',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('chooseGovVolunteer','openChooseGovVolunteerModel',function(_param){
                $('#chooseGovVolunteerModel').modal('show');
                vc.component._refreshChooseGovVolunteerInfo();
                vc.component._loadAllGovVolunteerInfo(1,10,'');
            });
        },
        methods:{
            _loadAllGovVolunteerInfo:function(_page,_row,_name){
                var param = {
                    params:{
                        page:_page,
                        row:_row,
                        caId: vc.getCurrentCommunity().caId,
                        isShow: 'Y',
                        name:_name
                    }
                };

                //发送get请求
               vc.http.apiGet('/govVolunteer/queryGovVolunteer',
                             param,
                             function(json){
                                var _govVolunteerInfo = JSON.parse(json);
                                vc.component.chooseGovVolunteerInfo.govVolunteers = _govVolunteerInfo.data;
                             },function(){
                                console.log('请求失败处理');
                             }
                           );
            },
            chooseGovVolunteer:function(_govVolunteer){
                if(_govVolunteer.hasOwnProperty('name')){
                     _govVolunteer.govVolunteerName = _govVolunteer.name;
                }
                vc.emit($props.emitChooseGovVolunteer,'chooseGovVolunteer',_govVolunteer);
                vc.emit($props.emitLoadData,'listGovVolunteerData',{
                    govVolunteerId:_govVolunteer.govVolunteerId
                });
                $('#chooseGovVolunteerModel').modal('hide');
            },
            queryGovVolunteers:function(){
                vc.component._loadAllGovVolunteerInfo(1,10,vc.component.chooseGovVolunteerInfo._currentGovVolunteerName);
            },
            _refreshChooseGovVolunteerInfo:function(){
                vc.component.chooseGovVolunteerInfo._currentGovVolunteerName = "";
            }
        }

    });
})(window.vc);
