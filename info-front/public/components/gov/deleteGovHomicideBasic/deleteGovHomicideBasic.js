(function (vc, vm) {

    vc.extends({
        data: {
            deleteGovHomicideBasicInfo: {

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('deleteGovHomicideBasic', 'openDeleteGovHomicideBasicModal', function (_params) {

                vc.component.deleteGovHomicideBasicInfo = _params;
                $('#deleteGovHomicideBasicModel').modal('show');

            });
        },
        methods: {
            deleteGovHomicideBasic: function () {
                vc.component.deleteGovHomicideBasicInfo.caId = vc.getCurrentCommunity().caId;
                vc.http.apiPost(
                    '/govHomicideBasic/deleteGovHomicideBasic',
                    JSON.stringify(vc.component.deleteGovHomicideBasicInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovHomicideBasicModel').modal('hide');
                            vc.emit('govHomicideBasicManage', 'listGovHomicideBasic', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');
                        vc.message(json);

                    });
            },
            closeDeleteGovHomicideBasicModel: function () {
                $('#deleteGovHomicideBasicModel').modal('hide');
            }
        }
    });

})(window.vc, window.vc.component);
