/**
    报名管理 组件
**/
(function(vc){

    vc.extends({
        propTypes: {
           callBackListener:vc.propTypes.string, //父组件名称
           callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            viewGovActivityPersonInfo:{
                index:0,
                flowComponent:'viewGovActivityPersonInfo',
                caId:'',
actId:'',
personName:'',
personLink:'',
personAge:'',
personAddress:'',
ramark:'',

            }
        },
        _initMethod:function(){
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovActivityPersonInfoData();
        },
        _initEvent:function(){
            vc.on('viewGovActivityPersonInfo','chooseGovActivityPerson',function(_app){
                vc.copyObject(_app, vc.component.viewGovActivityPersonInfo);
                vc.emit($props.callBackListener,$props.callBackFunction,vc.component.viewGovActivityPersonInfo);
            });

            vc.on('viewGovActivityPersonInfo', 'onIndex', function(_index){
                vc.component.viewGovActivityPersonInfo.index = _index;
            });

        },
        methods:{

            _openSelectGovActivityPersonInfoModel(){
                vc.emit('chooseGovActivityPerson','openChooseGovActivityPersonModel',{});
            },
            _openAddGovActivityPersonInfoModel(){
                vc.emit('addGovActivityPerson','openAddGovActivityPersonModal',{});
            },
            _loadGovActivityPersonInfoData:function(){

            }
        }
    });

})(window.vc);
