(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovMedicalClassifyInfo: {
                medicalClassifyId: '',
                caId: '',
                classifyName: '',
                classifyType: 'C',
                seq: '',
                ramark: '',
            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('addGovMedicalClassify', 'openAddGovMedicalClassifyModal', function () {
                $('#addGovMedicalClassifyModel').modal('show');
            });
        },
        methods: {
            addGovMedicalClassifyValidate() {
                return vc.validate.validate({
                    addGovMedicalClassifyInfo: vc.component.addGovMedicalClassifyInfo
                }, {
                    'addGovMedicalClassifyInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "区域ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "区域ID不能超过30"
                        },
                    ],
                    'addGovMedicalClassifyInfo.classifyName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "分类名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "分类名称不能超过30"
                        },
                    ],
                    'addGovMedicalClassifyInfo.seq': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "列顺序不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "3",
                            errInfo: "列顺序不能超过3"
                        },
                    ],
                    'addGovMedicalClassifyInfo.ramark': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "备注不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "备注不能超过1024"
                        },
                    ]

                });
            },
            saveGovMedicalClassifyInfo: function () {
                vc.component.addGovMedicalClassifyInfo.caId = vc.getCurrentCommunity().caId;
                if (!vc.component.addGovMedicalClassifyValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovMedicalClassifyInfo);
                    $('#addGovMedicalClassifyModel').modal('hide');
                    return;
                }
                vc.http.apiPost(
                    '/govMedicalClassify/saveGovMedicalClassify',
                    JSON.stringify(vc.component.addGovMedicalClassifyInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovMedicalClassifyModel').modal('hide');
                            vc.component.clearAddGovMedicalClassifyInfo();
                            vc.emit('govMedicalClassifyManage', 'listGovMedicalClassify', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            clearAddGovMedicalClassifyInfo: function () {
                vc.component.addGovMedicalClassifyInfo = {
                    medicalClassifyId: '',
                    caId: '',
                    classifyName: '',
                    classifyType: 'C',
                    seq: '',
                    ramark: '',

                };
            }
        }
    });

})(window.vc);
