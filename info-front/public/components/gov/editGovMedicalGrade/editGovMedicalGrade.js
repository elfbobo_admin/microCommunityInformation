(function (vc, vm) {

    vc.extends({
        data: {
            editGovMedicalGradeInfo: {
                medicalGradeId: '',
                medicalClassifyId: '',
                classifyName: '',
                caId: '',
                gradeName: '',
                gradeType: 'C',
                seq: '',
                ramark: '',
            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('editGovMedicalGrade', 'openEditGovMedicalGradeModal', function (_params) {
                vc.component.refreshEditGovMedicalGradeInfo();
                $('#editGovMedicalGradeModel').modal('show');
                vc.copyObject(_params, vc.component.editGovMedicalGradeInfo);
                vc.component.editGovMedicalGradeInfo.caId = vc.getCurrentCommunity().caId;
            });
            vc.on('viewGovMedicalClassifyInfo', 'chooseGovMedicalClassify', function (_param) {
                $that.editGovMedicalGradeInfo.medicalClassifyId = _param.medicalClassifyId;
                $that.editGovMedicalGradeInfo.classifyName = _param.classifyName;
            });
        },
        methods: {
            editGovMedicalGradeValidate: function () {
                return vc.validate.validate({
                    editGovMedicalGradeInfo: vc.component.editGovMedicalGradeInfo
                }, {
                    'editGovMedicalGradeInfo.medicalGradeId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "id不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "id不能超过30"
                        },
                    ],
                    'editGovMedicalGradeInfo.medicalClassifyId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "分类id不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "分类id不能超过30"
                        },
                    ],
                    'editGovMedicalGradeInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "区域ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "区域ID不能超过30"
                        },
                    ],
                    'editGovMedicalGradeInfo.gradeName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "分级名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "分级名称不能超过30"
                        },
                    ],
                    'editGovMedicalGradeInfo.gradeType': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "类型不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "类型不能超过12"
                        },
                    ],
                    'editGovMedicalGradeInfo.seq': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "列顺序不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "3",
                            errInfo: "列顺序不能超过3"
                        },
                    ]
                });
            },
            editGovMedicalGrade: function () {
                if (!vc.component.editGovMedicalGradeValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }
                vc.http.apiPost(
                    '/govMedicalGrade/updateGovMedicalGrade',
                    JSON.stringify(vc.component.editGovMedicalGradeInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovMedicalGradeModel').modal('hide');
                            vc.emit('govMedicalGradeManage', 'listGovMedicalGrade', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            _openEditChooseGovClassifyModel: function () {
                let _caId = vc.getCurrentCommunity().caId;
                if(!_caId){
                    vc.toast("请先选择区域！");
                    return;
                }
                vc.emit( 'chooseGovMedicalClassify', 'openChooseGovMedicalClassifyModel', _caId);
            },
            _closeEditGovClassify: function () {
                $that.refreshEditGovMedicalGradeInfo();
                vc.emit('govMedicalGradeManage', 'listGovMedicalGrade', {});
            },
            refreshEditGovMedicalGradeInfo: function () {
                vc.component.editGovMedicalGradeInfo = {
                    medicalGradeId: '',
                    medicalClassifyId: '',
                    classifyName: '',
                    caId: '',
                    gradeName: '',
                    gradeType: 'C',
                    seq: '',
                    ramark: '',
                }
            }
        }
    });

})(window.vc, window.vc.component);
