(function (vc, vm) {

    vc.extends({
        data: {
            editGovServFieldInfo: {
                servId: '',
                caId: '',
                name: '',
                isShow: '',
                ramark: '',

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('editGovServField', 'openEditGovServFieldModal', function (_params) {
                vc.component.refreshEditGovServFieldInfo();
                $('#editGovServFieldModel').modal('show');
                vc.copyObject(_params, vc.component.editGovServFieldInfo);
                vc.component.editGovServFieldInfo.caId = vc.getCurrentCommunity().caId;
            });
        },
        methods: {
            editGovServFieldValidate: function () {
                return vc.validate.validate({
                    editGovServFieldInfo: vc.component.editGovServFieldInfo
                }, {
                    'editGovServFieldInfo.servId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "服务领域ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "服务领域ID不能超过30"
                        },
                    ],
                    'editGovServFieldInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "区域ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "区域ID不能超过30"
                        },
                    ],
                    'editGovServFieldInfo.name': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "领域名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "领域名称不能超过64"
                        },
                    ],
                    'editGovServFieldInfo.isShow': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "是否显示不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "是否显示不能超过64"
                        },
                    ]

                });
            },
            editGovServField: function () {
                if (!vc.component.editGovServFieldValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }

                vc.http.apiPost(
                    '/govServField/updateGovServField',
                    JSON.stringify(vc.component.editGovServFieldInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovServFieldModel').modal('hide');
                            vc.emit('govServFieldManage', 'listGovServField', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            refreshEditGovServFieldInfo: function () {
                vc.component.editGovServFieldInfo = {
                    servId: '',
                    caId: '',
                    name: '',
                    isShow: '',
                    ramark: '',

                }
            }
        }
    });

})(window.vc, window.vc.component);
