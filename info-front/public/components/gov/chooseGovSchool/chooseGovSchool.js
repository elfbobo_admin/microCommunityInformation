(function(vc){
    vc.extends({
        propTypes: {
           emitChooseGovSchool:vc.propTypes.string,
           emitLoadData:vc.propTypes.string
        },
        data:{
            chooseGovSchoolInfo:{
                govSchools:[],
                _currentGovSchoolName:'',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('chooseGovSchool','openChooseGovSchoolModel',function(_param){
                $('#chooseGovSchoolModel').modal('show');
                vc.component._refreshChooseGovSchoolInfo();
                vc.component._loadAllGovSchoolInfo(1,10,'');
            });
        },
        methods:{
            _loadAllGovSchoolInfo:function(_page,_row,_name){
                var param = {
                    params:{
                        page:_page,
                        row:_row,
                        caId:vc.getCurrentCommunity().caId,
                        name:_name
                    }
                };

                //发送get请求
               vc.http.apiGet('/govSchool/queryGovSchool',
                             param,
                             function(json){
                                var _govSchoolInfo = JSON.parse(json);
                                vc.component.chooseGovSchoolInfo.govSchools = _govSchoolInfo.govSchools;
                             },function(){
                                console.log('请求失败处理');
                             }
                           );
            },
            chooseGovSchool:function(_govSchool){
                if(_govSchool.hasOwnProperty('name')){
                     _govSchool.govSchoolName = _govSchool.name;
                }
                vc.emit($props.emitChooseGovSchool,'chooseGovSchool',_govSchool);
                vc.emit($props.emitLoadData,'listGovSchoolData',{
                    govSchoolId:_govSchool.govSchoolId
                });
                $('#chooseGovSchoolModel').modal('hide');
            },
            queryGovSchools:function(){
                vc.component._loadAllGovSchoolInfo(1,10,vc.component.chooseGovSchoolInfo._currentGovSchoolName);
            },
            _refreshChooseGovSchoolInfo:function(){
                vc.component.chooseGovSchoolInfo._currentGovSchoolName = "";
            }
        }

    });
})(window.vc);
