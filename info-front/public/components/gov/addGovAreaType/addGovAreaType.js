(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovAreaTypeInfo: {
                typeId: '',
                caId: '',
                name: '',
                isShow: '',
                ramark: '',

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('addGovAreaType', 'openAddGovAreaTypeModal', function () {
                $('#addGovAreaTypeModel').modal('show');
            });
        },
        methods: {
            addGovAreaTypeValidate() {
                return vc.validate.validate({
                    addGovAreaTypeInfo: vc.component.addGovAreaTypeInfo
                }, {
                    'addGovAreaTypeInfo.name': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "区域类型名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "区域类型名称不能超过64"
                        },
                    ],
                    'addGovAreaTypeInfo.isShow': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "是否显示不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "是否显示不能超过64"
                        },
                    ],
                    'addGovAreaTypeInfo.ramark': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "备注不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "备注不能超过1024"
                        },
                    ],




                });
            },
            saveGovAreaTypeInfo: function () {
                if (!vc.component.addGovAreaTypeValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }

                vc.component.addGovAreaTypeInfo.caId = vc.getCurrentCommunity().caId;
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovAreaTypeInfo);
                    $('#addGovAreaTypeModel').modal('hide');
                    return;
                }
                vc.http.apiPost(
                    '/govAreaType/saveGovAreaType',
                    JSON.stringify(vc.component.addGovAreaTypeInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovAreaTypeModel').modal('hide');
                            vc.component.clearAddGovAreaTypeInfo();
                            vc.emit('govAreaTypeManage', 'listGovAreaType', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            clearAddGovAreaTypeInfo: function () {
                vc.component.addGovAreaTypeInfo = {
                    typeId: '',
                    caId: '',
                    name: '',
                    isShow: '',
                    ramark: '',

                };
            }
        }
    });

})(window.vc);
