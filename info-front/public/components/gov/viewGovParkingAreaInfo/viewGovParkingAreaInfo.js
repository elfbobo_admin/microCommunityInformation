/**
    停车场 组件
**/
(function(vc){

    vc.extends({
        propTypes: {
           callBackListener:vc.propTypes.string, //父组件名称
           callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            viewGovParkingAreaInfo:{
                index:0,
                flowComponent:'viewGovParkingAreaInfo',
                govCommunityId:'',
num:'',
parkingCount:'',
typeCd:'',
remark:'',

            }
        },
        _initMethod:function(){
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovParkingAreaInfoData();
        },
        _initEvent:function(){
            vc.on('viewGovParkingAreaInfo','chooseGovParkingArea',function(_app){
                vc.copyObject(_app, vc.component.viewGovParkingAreaInfo);
                vc.emit($props.callBackListener,$props.callBackFunction,vc.component.viewGovParkingAreaInfo);
            });

            vc.on('viewGovParkingAreaInfo', 'onIndex', function(_index){
                vc.component.viewGovParkingAreaInfo.index = _index;
            });

        },
        methods:{

            _openSelectGovParkingAreaInfoModel(){
                vc.emit('chooseGovParkingArea','openChooseGovParkingAreaModel',{});
            },
            _openAddGovParkingAreaInfoModel(){
                vc.emit('addGovParkingArea','openAddGovParkingAreaModal',{});
            },
            _loadGovParkingAreaInfoData:function(){

            }
        }
    });

})(window.vc);
