(function (vc, vm) {

    vc.extends({
        data: {
            editReportPoolInfo: {
                reportId: '',
                caId: '',
                reportType: '',
                reportName: '',
                tel: '',
                context: '',
                appointmentTime: '',

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('editReportPool', 'openEditReportPoolModal', function (_params) {
                vc.component.refreshEditReportPoolInfo();
                $('#editReportPoolModel').modal('show');
                vc.copyObject(_params, vc.component.editReportPoolInfo);
                vc.component.editReportPoolInfo.communityId = vc.getCurrentCommunity().communityId;
            });
        },
        methods: {
            editReportPoolValidate: function () {
                return vc.validate.validate({
                    editReportPoolInfo: vc.component.editReportPoolInfo
                }, {
                    'editReportPoolInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "所属区域不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "所属区域超长"
                        },
                    ],
                    'editReportPoolInfo.reportType': [
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "所报事类型超长"
                        },
                    ],
                    'editReportPoolInfo.reportName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "报事姓名不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "户号超长"
                        },
                    ],
                    'editReportPoolInfo.tel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "手机号不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "13",
                            errInfo: "手机号超长"
                        },
                    ],
                    'editReportPoolInfo.context': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "报事内容不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "2000",
                            errInfo: "报事内容超长"
                        },
                    ],
                    'editReportPoolInfo.appointmentTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "预约时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "备注太长"
                        },
                    ],
                    'editReportPoolInfo.reportId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "报事管理ID不能为空"
                        }]

                });
            },
            editReportPool: function () {
                if (!vc.component.editReportPoolValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }

                vc.http.apiPost(
                    '/reportPool/updateReportPool',
                    JSON.stringify(vc.component.editReportPoolInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editReportPoolModel').modal('hide');
                            vc.emit('reportPoolManage', 'listReportPool', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            refreshEditReportPoolInfo: function () {
                vc.component.editReportPoolInfo = {
                    reportId: '',
                    caId: '',
                    reportType: '',
                    reportName: '',
                    tel: '',
                    context: '',
                    appointmentTime: '',

                }
            }
        }
    });

})(window.vc, window.vc.component);
