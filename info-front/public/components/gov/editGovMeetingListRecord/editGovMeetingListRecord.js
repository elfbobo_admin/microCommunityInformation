(function (vc, vm) {

    vc.extends({
        data: {
            editGovMeetingListRecordInfo: {
                govMeetingId: '',
                caId: '',
                typeId: '',
                meetingTypeName: '',
                orgName: '',
                orgId: '',
                govMemberId: '',
                memberName: '',
                meetingName: '',
                meetingTime: '',
                meetingAddress: '',
                meetingBrief: '',
                meetingPoints: '',
                meetingContent: '',
                meetingImgs: [],
                govMembers: [],
                govMeetingTypes: [],
                govPartyOrgs: []
            }
        },
        _initMethod: function () {
            $that._listEditGovMeetingTypes();
            $that._listEditGovPartyOrgs();
        },
        _initEvent: function () {
            vc.on('editGovMeetingListRecord', 'openeditGovMeetingListRecordModal', function (_params) {
                vc.component.refresheditGovMeetingListRecordInfo();
                $('#editGovMeetingListRecordModel').modal('show');
                console.log(_params);
                vc.copyObject(_params, vc.component.editGovMeetingListRecordInfo);
                vc.component.editGovMeetingListRecordInfo.govMembers = _params.govMeetingMemberRelDtos;
               
                vc.component.editGovMeetingListRecordInfo.meetingImgs = _params.meetingImgs;
                vc.component.editGovMeetingListRecordInfo.caId = vc.getCurrentCommunity().caId;

                vc.emit('viewEditGovPartyMemberRecord', 'openviewEditGovPartyMemberRecordModel', $that.editGovMeetingListRecordInfo.govMembers);
      
                vc.emit('editGovMeetingListRecordCover','uploadImage', 'notifyPhotos',vc.component.editGovMeetingListRecordInfo.meetingImgs);
            });
            vc.on("editGovMeetingListRecord", "notifyUploadCoverImage", function (_param) {
                if(_param.length > 0){
                    vc.component.editGovMeetingListRecordInfo.meetingImgs=_param;
                }else{
                    vc.component.editGovMeetingListRecordInfo.meetingImgs = [];
                }
                
            });
        },
        methods: {
            editGovMeetingListRecordValidate: function () {
                return vc.validate.validate({
                    editGovMeetingListRecordInfo: vc.component.editGovMeetingListRecordInfo
                }, {
                    'editGovMeetingListRecordInfo.govMeetingId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "会议ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "会议ID不能超过30"
                        },
                    ],
                    'editGovMeetingListRecordInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "区域ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "区域ID不能超过30"
                        },
                    ],
                    'editGovMeetingListRecordInfo.typeId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "会议类型不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "会议类型不能超过30"
                        },
                    ],
                    'editGovMeetingListRecordInfo.orgId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "组织部门不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "组织部门不能超过30"
                        },
                    ],
                    'editGovMeetingListRecordInfo.govMemberId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "主持人不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "主持人不能超过30"
                        },
                    ],
                    'editGovMeetingListRecordInfo.meetingName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "会议名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "会议名称不能超过64"
                        },
                    ],
                    'editGovMeetingListRecordInfo.meetingTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "会议时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "会议时间不能超过时间类型"
                        },
                    ],
                    'editGovMeetingListRecordInfo.meetingAddress': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "会议地点不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "512",
                            errInfo: "会议地点不能超过512"
                        },
                    ],
                    'editGovMeetingListRecordInfo.meetingBrief': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "会议简介不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "会议简介不能超过30"
                        },
                    ],
                    'editGovMeetingListRecordInfo.meetingPoints': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "会议要点不能为空"
                        }
                    ],
                    'editGovMeetingListRecordInfo.meetingContent': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "主要内容不能为空"
                        }
                    ],

                });
            },
            editGovMeetingListRecord: function () {
                if (!vc.component.editGovMeetingListRecordValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }
                if (!$that.editGovMeetingListRecordInfo.meetingImgs.length) {
                    vc.toast("请选择会议照片");
                    return;
                }
                vc.http.apiPost(
                    '/govMeetingList/recordGovMeetingList',
                    JSON.stringify(vc.component.editGovMeetingListRecordInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            vc.component.refresheditGovMeetingListRecordInfo();
                            vc.emit('govMeetingListRecord', 'listGovMeetingList', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            _listEditGovMeetingTypes: function () {
                var param = {
                    params: {
                        caId: vc.getCurrentCommunity().caId,
                        page: 1,
                        row: 50
                    }
                };

                //发送get请求
                vc.http.apiGet('/govMeetingType/queryGovMeetingType',
                    param,
                    function (json, res) {
                        var _govMeetingTypeManageInfo = JSON.parse(json);
                        vc.component.editGovMeetingListRecordInfo.govMeetingTypes = _govMeetingTypeManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _listEditGovPartyOrgs: function () {

                var param = {
                    params: {
                        caId: vc.getCurrentCommunity().caId,
                        page: 1,
                        row: 50
                    }
                };

                //发送get请求
                vc.http.apiGet('/govPartyOrg/queryGovPartyOrg',
                    param,
                    function (json, res) {
                        var _govPartyOrgManageInfo = JSON.parse(json);
                        vc.component.editGovMeetingListRecordInfo.govPartyOrgs = _govPartyOrgManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openEditChooseGovPartyMember: function (_editGovMeetingListRecordInfo) {
                let _caId = vc.getCurrentCommunity().caId;
                if (!_caId) {
                    vc.toast("请先选择区域！");
                    return;
                }
                vc.emit('chooseGovPartyMember', 'openChooseGovPartyMemberModel', _editGovMeetingListRecordInfo);
            },
            _setEditviewGovPartyMember: function (_orgId) {
                let _caId = vc.getCurrentCommunity().caId;
                if (!_caId) {
                    vc.toast("请先选择区域！");
                    return;
                }
                vc.emit('viewEditGovPartyMember', 'selectPartyOrgs', _orgId);
            },
            _cleareditGovMeetingListRecordModel: function () {
                $that.refresheditGovMeetingListRecordInfo();
                vc.emit('govMeetingListRecord', 'listGovMeetingList', {});
            },
            refresheditGovMeetingListRecordInfo: function () {
                let _govMembers = vc.component.editGovMeetingListRecordInfo.govMembers;
                let _govMeetingTypes = vc.component.editGovMeetingListRecordInfo.govMeetingTypes;
                let _govPartyOrgs = vc.component.editGovMeetingListRecordInfo.govPartyOrgs;
                vc.component.editGovMeetingListRecordInfo = {
                    govMeetingId: '',
                    caId: '',
                    typeId: '',
                    meetingTypeName: '',
                    orgName: '',
                    orgId: '',
                    govMemberId: '',
                    memberName: '',
                    meetingName: '',
                    meetingTime: '',
                    meetingAddress: '',
                    meetingBrief: '',
                    meetingPoints: '',
                    meetingContent: '',
                    meetingImgs: [],
                    govMembers: _govMembers,
                    govMeetingTypes: _govMeetingTypes,
                    govPartyOrgs: _govPartyOrgs
                }
            }
        }
    });

})(window.vc, window.vc.component);
