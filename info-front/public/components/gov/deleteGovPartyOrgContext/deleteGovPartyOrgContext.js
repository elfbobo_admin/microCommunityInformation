(function (vc, vm) {

    vc.extends({
        data: {
            deleteGovPartyOrgContextInfo: {

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('deleteGovPartyOrgContext', 'openDeleteGovPartyOrgContextModal', function (_params) {

                vc.component.deleteGovPartyOrgContextInfo = _params;
                $('#deleteGovPartyOrgContextModel').modal('show');

            });
        },
        methods: {
            deleteGovPartyOrgContext: function () {
                vc.http.apiPost(
                    '/govPartyOrgContext/deleteGovPartyOrgContext',
                    JSON.stringify(vc.component.deleteGovPartyOrgContextInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovPartyOrgContextModel').modal('hide');
                            vc.emit('govPartyOrgContextManage', 'listGovPartyOrgContext', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');
                        vc.message(json);

                    });
            },
            closeDeleteGovPartyOrgContextModel: function () {
                $('#deleteGovPartyOrgContextModel').modal('hide');
            }
        }
    });

})(window.vc, window.vc.component);
