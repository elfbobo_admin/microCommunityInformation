(function (vc, vm) {

    vc.extends({
        data: {
            editGovMedicalClassifyInfo: {
                medicalClassifyId: '',
                caId: '',
                classifyName: '',
                classifyType: '',
                seq: '',
                ramark: '',
            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('editGovMedicalClassify', 'openEditGovMedicalClassifyModal', function (_params) {
                vc.component.refreshEditGovMedicalClassifyInfo();
                $('#editGovMedicalClassifyModel').modal('show');
                vc.copyObject(_params, vc.component.editGovMedicalClassifyInfo);
                vc.component.editGovMedicalClassifyInfo.caId = vc.getCurrentCommunity().caId;
            });
        },
        methods: {
            editGovMedicalClassifyValidate: function () {
                return vc.validate.validate({
                    editGovMedicalClassifyInfo: vc.component.editGovMedicalClassifyInfo
                }, {
                    
                    'editGovMedicalClassifyInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "区域ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "区域ID不能超过30"
                        },
                    ],
                    'editGovMedicalClassifyInfo.classifyName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "分类名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "分类名称不能超过30"
                        },
                    ],
                    'editGovMedicalClassifyInfo.seq': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "列顺序不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "3",
                            errInfo: "列顺序不能超过3"
                        },
                    ],
                    'editGovMedicalClassifyInfo.ramark': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "备注不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "备注不能超过1024"
                        },
                    ]

                });
            },
            editGovMedicalClassify: function () {
                if (!vc.component.editGovMedicalClassifyValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }

                vc.http.apiPost(
                    '/govMedicalClassify/updateGovMedicalClassify',
                    JSON.stringify(vc.component.editGovMedicalClassifyInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovMedicalClassifyModel').modal('hide');
                            vc.emit('govMedicalClassifyManage', 'listGovMedicalClassify', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            refreshEditGovMedicalClassifyInfo: function () {
                vc.component.editGovMedicalClassifyInfo = {
                    medicalClassifyId: '',
                    caId: '',
                    classifyName: '',
                    classifyType: '',
                    seq: '',
                    ramark: '',
                }
            }
        }
    });

})(window.vc, window.vc.component);
