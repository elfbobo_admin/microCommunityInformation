(function (vc) {
    vc.extends({
        data: {
            viewRoomInfo: {
                orgId: '',
                govPersonId: '',
                rooms: []
            }
        },
        _initMethod: function () {
        },
        _initEvent: function () {
            vc.on('roomInfo', 'switch', function (_param) {
                vc.component._refreshviewGovRoomMemberInfo();
                console.log(_param);
                vc.component._loadRoomInfo(_param.govPersonId);
            });
        },
        methods: {
            _loadRoomInfo: function (_personId) {
                var param = {
                    params: {
                        page: 1,
                        row: 10,
                        caId: vc.getCurrentCommunity().caId,
                        govPersonId: _personId
                    }
                };
                //发送get请求
                vc.http.apiGet('/govRoom/getPersonOwner',
                    param,
                    function (json) {
                        var _labelInfo = JSON.parse(json);
                        $that.viewRoomInfo.rooms = _labelInfo.data;
                    }, function () {
                        console.log('请求失败处理');
                    }
                );
            },
            _refreshviewGovRoomMemberInfo: function () {
                $that.viewRoomInfo = {
                    orgId: '',
                    govPersonId: '',
                    rooms: []
                }
            }
        }

    });
})(window.vc);
