(function(vc){
    vc.extends({
        propTypes: {
           emitChooseGovActivityType:vc.propTypes.string,
           emitLoadData:vc.propTypes.string
        },
        data:{
            chooseGovActivityTypeInfo:{
                govActivityTypes:[],
                _currentGovActivityTypeName:'',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('chooseGovActivityType','openChooseGovActivityTypeModel',function(_param){
                $('#chooseGovActivityTypeModel').modal('show');
                vc.component._refreshChooseGovActivityTypeInfo();
                vc.component._loadAllGovActivityTypeInfo(1,10,'');
            });
        },
        methods:{
            _loadAllGovActivityTypeInfo:function(_page,_row,_name){
                var param = {
                    params:{
                        page:_page,
                        row:_row,
                        communityId:vc.getCurrentCommunity().communityId,
                        name:_name
                    }
                };

                //发送get请求
               vc.http.apiGet('govActivityType.listGovActivityTypes',
                             param,
                             function(json){
                                var _govActivityTypeInfo = JSON.parse(json);
                                vc.component.chooseGovActivityTypeInfo.govActivityTypes = _govActivityTypeInfo.govActivityTypes;
                             },function(){
                                console.log('请求失败处理');
                             }
                           );
            },
            chooseGovActivityType:function(_govActivityType){
                if(_govActivityType.hasOwnProperty('name')){
                     _govActivityType.govActivityTypeName = _govActivityType.name;
                }
                vc.emit($props.emitChooseGovActivityType,'chooseGovActivityType',_govActivityType);
                vc.emit($props.emitLoadData,'listGovActivityTypeData',{
                    govActivityTypeId:_govActivityType.govActivityTypeId
                });
                $('#chooseGovActivityTypeModel').modal('hide');
            },
            queryGovActivityTypes:function(){
                vc.component._loadAllGovActivityTypeInfo(1,10,vc.component.chooseGovActivityTypeInfo._currentGovActivityTypeName);
            },
            _refreshChooseGovActivityTypeInfo:function(){
                vc.component.chooseGovActivityTypeInfo._currentGovActivityTypeName = "";
            }
        }

    });
})(window.vc);
