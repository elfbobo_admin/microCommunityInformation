(function (vc, vm) {

    vc.extends({
        data: {
            editGovCompanyInfo: {
                govCompanyId: '',
                caId: '',
                companyName: '',
                companyType: '',
                idCard: '',
                artificialPerson: '',
                companyAddress: '',
                registerTime: '',
                personName: '',
                personIdCard: '',
                personTel: '',
                ramark: '',
                govCommunityAreas: [],
                govCommunityId:'',
                govCommunitys:[]
            }
        },
        _initMethod: function () {
            vc.initDateTime('editRegisterTime', function (_value) {
                $that.editGovCompanyInfo.registerTime = _value;
            });

            $that._listEditCompanyGovCommunitys();
        },
        _initEvent: function () {
            vc.on('editGovCompany', 'openEditGovCompanyModal', function (_params) {
                vc.component.refreshEditGovCompanyInfo();
                $('#editGovCompanyModel').modal('show');
                vc.copyObject(_params, vc.component.editGovCompanyInfo);
            });
        },
        methods: {
            editGovCompanyValidate: function () {
                return vc.validate.validate({
                    editGovCompanyInfo: vc.component.editGovCompanyInfo
                }, {
                    'editGovCompanyInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "所属区域不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "所属区域超长"
                        },
                    ],
                    'editGovCompanyInfo.companyName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "公司名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "128",
                            errInfo: "公司名称超长"
                        },
                    ],
                    'editGovCompanyInfo.companyType': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "公司类型不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "公司类型超长"
                        },
                    ],
                    'editGovCompanyInfo.idCard': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "证件号码不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "证件号码超长"
                        },
                    ],
                    'editGovCompanyInfo.artificialPerson': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "法人不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "法人超长"
                        },
                    ],
                    'editGovCompanyInfo.companyAddress': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "地址不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "512",
                            errInfo: "地址超长"
                        },
                    ],
                    'editGovCompanyInfo.registerTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "注册时间不能为空"
                        },
                        {
                            limit: "dateTime",
                            param: "",
                            errInfo: "不是有效的时间格式"
                        },
                    ],
                    'editGovCompanyInfo.personName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "负责人不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "负责人太长"
                        },
                    ],
                    'editGovCompanyInfo.personIdCard': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "负责人身份证不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "20",
                            errInfo: "负责人身份证超长"
                        },
                    ],
                    'editGovCompanyInfo.personTel': [
                        {
                            limit: "phone",
                            param: "",
                            errInfo: "电话格式错误"
                        },
                    ],
                    'editGovCompanyInfo.ramark': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "备注不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "备注太长"
                        },
                    ],
                    'editGovCompanyInfo.govCompanyId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "公司组织ID不能为空"
                        }]

                });
            },
            editGovCompany: function () {
                if (!vc.component.editGovCompanyValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }
                if(!vc.component.isCardNoAdd(vc.component.addGovOwnerInfo.personIdCard)){
                    vc.toast("身份证格式有误，请检查身份证信息");

                    return;
                }
                vc.component.editGovCompanyInfo.caId = vc.getCurrentCommunity().caId;
                vc.http.apiPost(
                    '/govCompany/updateGovCompany',
                    JSON.stringify(vc.component.editGovCompanyInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovCompanyModel').modal('hide');
                            vc.emit('govCompanyManage', 'listGovCompany', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            isCardNoAdd: function (card) {
                //身份证号码为15位或者18位，15位时全为数字，18位前17位为数字，最后一位是校验位，可能为数字或字符X
                var reg = /(^\d{15}$)|(^\d{17}(\d|X)$)/;
                if (reg.test(card) === false) {
                    return false;
                }
                return true;
            },
            _listEditCompantGovCommunityAreas: function () {

                let param = {
                    params: {
                        page: 1,
                        row: 50
                    }
                };
                //发送get请求
                vc.http.apiGet('/govCommunityArea/queryGovCommunityArea',
                    param,
                    function (json, res) {
                        var _govCommunityAreaManageInfo = JSON.parse(json);
                        vc.component.editGovCompanyInfo.total = _govCommunityAreaManageInfo.total;
                        vc.component.editGovCompanyInfo.records = _govCommunityAreaManageInfo.records;
                        vc.component.editGovCompanyInfo.govCommunityAreas = _govCommunityAreaManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _listEditCompanyGovCommunitys: function () {

                let param = {
                    params: {
                        page: 1,
                        row: 50,
                        caId: vc.getCurrentCommunity().caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govCommunity/queryGovCommunity',
                    param,
                    function (json, res) {
                        var _govCommunityManageInfo = JSON.parse(json);
                        vc.component.editGovCompanyInfo.total = _govCommunityManageInfo.total;
                        vc.component.editGovCompanyInfo.records = _govCommunityManageInfo.records;
                        vc.component.editGovCompanyInfo.govCommunitys = _govCommunityManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            refreshEditGovCompanyInfo: function () {
                let _govCommunityAreas = $that.editGovCompanyInfo.govCommunityAreas;
                let _govCommunitys = $that.editGovCompanyInfo.govCommunitys;
                vc.component.editGovCompanyInfo = {
                    govCompanyId: '',
                    caId: '',
                    companyName: '',
                    companyType: '',
                    idCard: '',
                    artificialPerson: '',
                    companyAddress: '',
                    registerTime: '',
                    personName: '',
                    personIdCard: '',
                    personTel: '',
                    ramark: '',
                    govCommunityAreas: _govCommunityAreas,
                    govCommunityId:'',
                    govCommunitys: _govCommunitys
                }
            }
        }
    });

})(window.vc, window.vc.component);
