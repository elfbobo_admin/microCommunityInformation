(function(vc){
    vc.extends({
        propTypes: {
           emitChooseGovHelpPolicyList:vc.propTypes.string,
           emitLoadData:vc.propTypes.string
        },
        data:{
            chooseGovHelpPolicyListInfo:{
                govHelpPolicyLists:[],
                _currentGovHelpPolicyListName:'',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('chooseGovHelpPolicyList','openChooseGovHelpPolicyListModel',function(_param){
                $('#chooseGovHelpPolicyListModel').modal('show');
                vc.component._refreshChooseGovHelpPolicyListInfo();
                vc.component._loadAllGovHelpPolicyListInfo(1,10,'');
            });
        },
        methods:{
            _loadAllGovHelpPolicyListInfo:function(_page,_row,_name){
                var param = {
                    params:{
                        page:_page,
                        row:_row,
                        caId:vc.getCurrentCommunity().caId,
                        name:_name
                    }
                };

                //发送get请求
               vc.http.apiGet('/govHelpPolicyList/queryGovHelpPolicyList',
                             param,
                             function(json){
                                var _govHelpPolicyListInfo = JSON.parse(json);
                                vc.component.chooseGovHelpPolicyListInfo.govHelpPolicyLists = _govHelpPolicyListInfo.govHelpPolicyLists;
                             },function(){
                                console.log('请求失败处理');
                             }
                           );
            },
            chooseGovHelpPolicyList:function(_govHelpPolicyList){
                if(_govHelpPolicyList.hasOwnProperty('name')){
                     _govHelpPolicyList.govHelpPolicyListName = _govHelpPolicyList.name;
                }
                vc.emit($props.emitChooseGovHelpPolicyList,'chooseGovHelpPolicyList',_govHelpPolicyList);
                vc.emit($props.emitLoadData,'listGovHelpPolicyListData',{
                    govHelpPolicyListId:_govHelpPolicyList.govHelpPolicyListId
                });
                $('#chooseGovHelpPolicyListModel').modal('hide');
            },
            queryGovHelpPolicyLists:function(){
                vc.component._loadAllGovHelpPolicyListInfo(1,10,vc.component.chooseGovHelpPolicyListInfo._currentGovHelpPolicyListName);
            },
            _refreshChooseGovHelpPolicyListInfo:function(){
                vc.component.chooseGovHelpPolicyListInfo._currentGovHelpPolicyListName = "";
            }
        }

    });
})(window.vc);
