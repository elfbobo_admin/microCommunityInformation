/**
    公告管理 组件
**/
(function(vc){

    vc.extends({
        propTypes: {
           callBackListener:vc.propTypes.string, //父组件名称
           callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            viewGovActivitiesInfo:{
                index:0,
                flowComponent:'viewGovActivitiesInfo',
                activitiesId:'',
title:'',
typeCd:'',
headerImg:'',
context:'',
startTime:'',
endTime:'',
state:'',

            }
        },
        _initMethod:function(){
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovActivitiesInfoData();
        },
        _initEvent:function(){
            vc.on('viewGovActivitiesInfo','chooseGovActivities',function(_app){
                vc.copyObject(_app, vc.component.viewGovActivitiesInfo);
                vc.emit($props.callBackListener,$props.callBackFunction,vc.component.viewGovActivitiesInfo);
            });

            vc.on('viewGovActivitiesInfo', 'onIndex', function(_index){
                vc.component.viewGovActivitiesInfo.index = _index;
            });

        },
        methods:{

            _openSelectGovActivitiesInfoModel(){
                vc.emit('chooseGovActivities','openChooseGovActivitiesModel',{});
            },
            _openAddGovActivitiesInfoModel(){
                vc.emit('addGovActivities','openAddGovActivitiesModal',{});
            },
            _loadGovActivitiesInfoData:function(){

            }
        }
    });

})(window.vc);
