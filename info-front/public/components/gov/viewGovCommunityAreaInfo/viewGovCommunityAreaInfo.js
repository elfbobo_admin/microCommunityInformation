/**
    区域管理 组件
**/
(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            viewGovCommunityAreaInfo: {
                index: 0,
                flowComponent: 'viewGovCommunityAreaInfo',
                caCode: '',
                caName: '',
                caSpace: '',
                areaCode: '',
                caAddress: '',
                person: '',
                personLink: '',
                remark: '',

            }
        },
        _initMethod: function () {
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovCommunityAreaInfoData();
        },
        _initEvent: function () {
            vc.on('viewGovCommunityAreaInfo', 'chooseGovCommunityArea', function (_app) {
                vc.copyObject(_app, vc.component.viewGovCommunityAreaInfo);
                vc.emit($props.callBackListener, $props.callBackFunction, vc.component.viewGovCommunityAreaInfo);
            });

            vc.on('viewGovCommunityAreaInfo', 'onIndex', function (_index) {
                vc.component.viewGovCommunityAreaInfo.index = _index;
            });

        },
        methods: {

            _openSelectGovCommunityAreaInfoModel() {
                vc.emit('chooseGovCommunityArea', 'openChooseGovCommunityAreaModel', {});
            },
            _openAddGovCommunityAreaInfoModel() {
                vc.emit('addGovCommunityArea', 'openAddGovCommunityAreaModal', {});
            },
            _loadGovCommunityAreaInfoData: function () {

            }
        }
    });

})(window.vc);
