/**
    党关系管理 组件
**/
(function(vc){

    vc.extends({
        propTypes: {
           callBackListener:vc.propTypes.string, //父组件名称
           callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            viewGovPartyMemberChangeInfo:{
                index:0,
                flowComponent:'viewGovPartyMemberChangeInfo',
                caId:'',
govMemberId:'',
personName:'',
srcOrgId:'',
srcOrgName:'',
targetOrgId:'',
targetOrgName:'',
chagneType:'',
ramark:'',

            }
        },
        _initMethod:function(){
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovPartyMemberChangeInfoData();
        },
        _initEvent:function(){
            vc.on('viewGovPartyMemberChangeInfo','chooseGovPartyMemberChange',function(_app){
                vc.copyObject(_app, vc.component.viewGovPartyMemberChangeInfo);
                vc.emit($props.callBackListener,$props.callBackFunction,vc.component.viewGovPartyMemberChangeInfo);
            });

            vc.on('viewGovPartyMemberChangeInfo', 'onIndex', function(_index){
                vc.component.viewGovPartyMemberChangeInfo.index = _index;
            });

        },
        methods:{

            _openSelectGovPartyMemberChangeInfoModel(){
                vc.emit('chooseGovPartyMemberChange','openChooseGovPartyMemberChangeModel',{});
            },
            _openAddGovPartyMemberChangeInfoModel(){
                vc.emit('addGovPartyMemberChange','openAddGovPartyMemberChangeModal',{});
            },
            _loadGovPartyMemberChangeInfoData:function(){

            }
        }
    });

})(window.vc);
