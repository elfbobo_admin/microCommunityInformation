/**
    人口管理 组件
**/
(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            viewGovPersonInfo: {
                index: 0,
                flowComponent: 'viewGovPersonInfo',
                caId: '',
                personType: '',
                idType: '',
                idCard: '',
                personName: '',
                personTel: '',
                personSex: '',
                prePersonName: '',
                birthday: '',
                nation: '',
                nativePlace: '',
                politicalOutlook: '',
                maritalStatus: '',
                religiousBelief: '',
                ramark: '',

            }
        },
        _initMethod: function () {
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovPersonInfoData();
        },
        _initEvent: function () {
            vc.on('viewGovPersonInfo', 'chooseGovPerson', function (_app) {
                vc.copyObject(_app, vc.component.viewGovPersonInfo);
                vc.emit($props.callBackListener, $props.callBackFunction, vc.component.viewGovPersonInfo);
            });

            vc.on('viewGovPersonInfo', 'onIndex', function (_index) {
                vc.component.viewGovPersonInfo.index = _index;
            });

        },
        methods: {

            _openSelectGovPersonInfoModel() {
                vc.emit('chooseGovPerson', 'openChooseGovPersonModel', {});
            },
            _openAddGovPersonInfoModel() {
                vc.emit('addGovPerson', 'openAddGovPersonModal', {});
            },
            _loadGovPersonInfoData: function () {

            }
        }
    });

})(window.vc);
