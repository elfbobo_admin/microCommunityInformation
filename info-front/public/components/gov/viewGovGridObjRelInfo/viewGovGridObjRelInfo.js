/**
    网格对象关系 组件
**/
(function(vc){

    vc.extends({
        propTypes: {
           callBackListener:vc.propTypes.string, //父组件名称
           callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            viewGovGridObjRelInfo:{
                index:0,
                flowComponent:'viewGovGridObjRelInfo',
                govGridObjId:'',
govGridId:'',
objTypeCd:'',
objTypeName:'',
caId:'',
objId:'',
startTime:'',
endTime:'',
workStatus:'',
ramark:'',
statusCd:'',

            }
        },
        _initMethod:function(){
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovGridObjRelInfoData();
        },
        _initEvent:function(){
            vc.on('viewGovGridObjRelInfo','chooseGovGridObjRel',function(_app){
                vc.copyObject(_app, vc.component.viewGovGridObjRelInfo);
                vc.emit($props.callBackListener,$props.callBackFunction,vc.component.viewGovGridObjRelInfo);
            });

            vc.on('viewGovGridObjRelInfo', 'onIndex', function(_index){
                vc.component.viewGovGridObjRelInfo.index = _index;
            });

        },
        methods:{

            _openSelectGovGridObjRelInfoModel(){
                vc.emit('chooseGovGridObjRel','openChooseGovGridObjRelModel',{});
            },
            _openAddGovGridObjRelInfoModel(){
                vc.emit('addGovGridObjRel','openAddGovGridObjRelModal',{});
            },
            _loadGovGridObjRelInfoData:function(){

            }
        }
    });

})(window.vc);
