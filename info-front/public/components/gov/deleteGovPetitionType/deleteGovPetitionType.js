(function(vc,vm){

    vc.extends({
        data:{
            deleteGovPetitionTypeInfo:{

            }
        },
         _initMethod:function(){

         },
         _initEvent:function(){
             vc.on('deleteGovPetitionType','openDeleteGovPetitionTypeModal',function(_params){

                vc.component.deleteGovPetitionTypeInfo = _params;
                $('#deleteGovPetitionTypeModel').modal('show');

            });
        },
        methods:{
            deleteGovPetitionType:function(){
                vc.component.deleteGovPetitionTypeInfo.caId=vc.getCurrentCommunity().caId;
                vc.http.apiPost(
                    '/govPetitionType/deleteGovPetitionType',
                    JSON.stringify(vc.component.deleteGovPetitionTypeInfo),
                    {
                        emulateJSON:true
                     },
                     function(json,res){
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovPetitionTypeModel').modal('hide');
                            vc.emit('govPetitionTypeManage','listGovPetitionType',{});
                            return ;
                        }
                        vc.message(_json.msg);
                     },
                     function(errInfo,error){
                        console.log('请求失败处理');
                        vc.message(json);

                     });
            },
            closeDeleteGovPetitionTypeModel:function(){
                $('#deleteGovPetitionTypeModel').modal('hide');
            }
        }
    });

})(window.vc,window.vc.component);
