(function (vc, vm) {

    vc.extends({
        data: {
            deleteGovBuildingTypeInfo: {

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('deleteGovBuildingType', 'openDeleteGovBuildingTypeModal', function (_params) {

                vc.component.deleteGovBuildingTypeInfo = _params;
                $('#deleteGovBuildingTypeModel').modal('show');

            });
        },
        methods: {
            deleteGovBuildingType: function () {
                //vc.component.deleteGovBuildingTypeInfo.communityId = vc.getCurrentCommunity().communityId;
                vc.http.apiPost(
                    '/govBuildingType/deleteGovBuildingType',
                    JSON.stringify(vc.component.deleteGovBuildingTypeInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovBuildingTypeModel').modal('hide');
                            vc.emit('govBuildingTypeManage', 'listGovBuildingType', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');
                        vc.message(json);

                    });
            },
            closeDeleteGovBuildingTypeModel: function () {
                $('#deleteGovBuildingTypeModel').modal('hide');
            }
        }
    });

})(window.vc, window.vc.component);
