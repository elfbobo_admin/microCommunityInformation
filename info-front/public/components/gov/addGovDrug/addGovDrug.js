(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovDrugInfo: {
                drugId: '',
                name: '',
                caId: '',
                address: '',
                tel: '',
                drugStartTime: '',
                drugEndTime: '',
                drugReason: '',
                emergencyPerson: '',
                emergencyTel: '',
                ramark: '',
                statusCd: '',

            }
        },
        _initMethod: function () {
            vc.initDateTime('addStartTime', function (_value) {
                $that.addGovDrugInfo.drugStartTime = _value;
            });
            vc.initDateTime('addEndTime', function (_value) {
                $that.addGovDrugInfo.drugEndTime = _value;
            });
        },
        _initEvent: function () {
            vc.on('addGovDrug', 'openAddGovDrugModal', function () {
                $('#addGovDrugModel').modal('show');
            });
        },
        methods: {
            addGovDrugValidate() {
                return vc.validate.validate({
                    addGovDrugInfo: vc.component.addGovDrugInfo
                }, {
                    'addGovDrugInfo.name': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "吸毒者名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "吸毒者名称不能超过64"
                        },
                    ],
                    'addGovDrugInfo.address': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "住址不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "512",
                            errInfo: "住址不能超过512"
                        },
                    ],
                    'addGovDrugInfo.tel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "手机号不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "手机号不能超过11"
                        },
                    ],
                    'addGovDrugInfo.drugStartTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "吸毒开始时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "吸毒开始时间不能超过时间类型"
                        },
                    ],
                    'addGovDrugInfo.drugEndTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "吸毒结束时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "吸毒结束时间不能超过时间类型"
                        },
                    ],
                    'addGovDrugInfo.drugReason': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "吸毒原因不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "128",
                            errInfo: "吸毒原因不能超过128"
                        },
                    ],
                    'addGovDrugInfo.emergencyPerson': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "紧急联系人不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "紧急联系人不能超过64"
                        },
                    ],
                    'addGovDrugInfo.emergencyTel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "紧急联系电话不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "紧急联系电话不能超过11"
                        },
                    ],
                });
            },
            saveGovDrugInfo: function () {
                if (!vc.component.addGovDrugValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }

                vc.component.addGovDrugInfo.caId = vc.getCurrentCommunity().caId;
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovDrugInfo);
                    $('#addGovDrugModel').modal('hide');
                    return;
                }
                vc.http.apiPost(
                    '/govDrug/saveGovDrug',
                    JSON.stringify(vc.component.addGovDrugInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovDrugModel').modal('hide');
                            vc.component.clearAddGovDrugInfo();
                            vc.emit('govDrugManage', 'listGovDrug', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            clearAddGovDrugInfo: function () {
                vc.component.addGovDrugInfo = {
                    drugId: '',
                    name: '',
                    caId: '',
                    address: '',
                    tel: '',
                    drugStartTime: '',
                    drugEndTime: '',
                    drugReason: '',
                    emergencyPerson: '',
                    emergencyTel: '',
                    ramark: '',
                    statusCd: '',

                };
            }
        }
    });

})(window.vc);
