(function (vc, vm) {

    vc.extends({
        data: {
            editGovGridInfo: {
                govGridId: '',
                govPersonId: '',
                govTypeId: '',
                caId: '',
                personName: '',
                personTel: '',
                ramark: '',
                govGridTypes: [],
                idCard: '',
                personSex: '',
                nation:'',
                maritalStatus:''
            }
        },
        _initMethod: function () {
            $that._deitGovGridTypes();
        },
        _initEvent: function () {
            vc.on('editGovGrid', 'openEditGovGridModal', function (_params) {
                vc.component.refreshEditGovGridInfo();
                $('#editGovGridModel').modal('show');
                vc.copyObject(_params, vc.component.editGovGridInfo);
                vc.component.editGovGridInfo.communityId = vc.getCurrentCommunity().communityId;
            });
        },
        methods: {
            editGovGridValidate: function () {
                return vc.validate.validate({
                    editGovGridInfo: vc.component.editGovGridInfo
                }, {

                    'editGovGridInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "区域ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "区域ID不能超过30"
                        },
                    ],
                    'editGovGridInfo.personName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "姓名不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "姓名不能超过64"
                        },
                    ],
                    'editGovGridInfo.personTel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "电话不能为空"
                        },
                        {
                            limit: "phone",
                            param: "",
                            errInfo: "电话格式错误"
                        },
                    ],
                    'editGovGridInfo.idCard': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "身份证不能为空"
                        }
                    ],
                    'editGovGridInfo.personSex': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "性别不能为空"
                        }
                    ],
                    'editGovGridInfo.nation': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "民族不能为空"
                        }
                    ],
                    'editGovGridInfo.maritalStatus': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "婚姻状态不能为空"
                        }
                    ],
                    'editGovGridInfo.ramark': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "备注不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "备注不能超过1024"
                        },
                    ]

                });
            },
            editGovGrid: function () {
                if (!vc.component.editGovGridValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }

                vc.http.apiPost(
                    '/govGrid/updateGovGrid',
                    JSON.stringify(vc.component.editGovGridInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovGridModel').modal('hide');
                            vc.emit('govGridManage', 'listGovGrid', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            _deitGovGridTypes: function () {
                var param = {
                    params: {
                        page: 1,
                        row: 50,
                        caId: vc.getCurrentCommunity().caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govGridType/queryGovGridType',
                    param,
                    function (json, res) {
                        var _govGridTypeManageInfo = JSON.parse(json);
                        vc.component.editGovGridInfo.govGridTypes = _govGridTypeManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            refreshEditGovGridInfo: function () {
                let _govGridTypes = vc.component.editGovGridInfo.govGridTypes;
                vc.component.editGovGridInfo = {
                    govGridId: '',
                    govPersonId: '',
                    govTypeId: '',
                    caId: '',
                    personName: '',
                    personTel: '',
                    ramark: '',
                    govGridTypes: _govGridTypes,
                    idCard: '',
                    personSex: '',
                    nation: '',
                    maritalStatus: ''

                }
            }
        }
    });

})(window.vc, window.vc.component);
