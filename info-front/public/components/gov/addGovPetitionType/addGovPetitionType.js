(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovPetitionTypeInfo: {
                typeId: '',
                typeName: '',
                typeDesc: '',
                seq: '',
                caId: '',
                statusCd: '',

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('addGovPetitionType', 'openAddGovPetitionTypeModal', function () {
                $('#addGovPetitionTypeModel').modal('show');
            });
        },
        methods: {
            addGovPetitionTypeValidate() {
                return vc.validate.validate({
                    addGovPetitionTypeInfo: vc.component.addGovPetitionTypeInfo
                }, {
                    'addGovPetitionTypeInfo.typeName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "类型名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "100",
                            errInfo: "类型名称不能超过100"
                        },
                    ],
                    'addGovPetitionTypeInfo.typeDesc': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "类型描述不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "500",
                            errInfo: "类型描述不能超过500"
                        },
                    ],
                    'addGovPetitionTypeInfo.seq': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "顺序不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "顺序不能超过11"
                        },
                    ],
                });
            },
            saveGovPetitionTypeInfo: function () {
                if (!vc.component.addGovPetitionTypeValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }

                vc.component.addGovPetitionTypeInfo.caId = vc.getCurrentCommunity().caId;
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovPetitionTypeInfo);
                    $('#addGovPetitionTypeModel').modal('hide');
                    return;
                }
                vc.http.apiPost(
                    '/govPetitionType/saveGovPetitionType',
                    JSON.stringify(vc.component.addGovPetitionTypeInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovPetitionTypeModel').modal('hide');
                            vc.component.clearAddGovPetitionTypeInfo();
                            vc.emit('govPetitionTypeManage', 'listGovPetitionType', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            clearAddGovPetitionTypeInfo: function () {
                vc.component.addGovPetitionTypeInfo = {
                    typeId: '',
                    typeName: '',
                    typeDesc: '',
                    seq: '',
                    caId: '',
                    statusCd: '',

                };
            }
        }
    });

})(window.vc);
