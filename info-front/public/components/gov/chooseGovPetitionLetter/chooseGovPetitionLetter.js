(function(vc){
    vc.extends({
        propTypes: {
           emitChooseGovPetitionLetter:vc.propTypes.string,
           emitLoadData:vc.propTypes.string
        },
        data:{
            chooseGovPetitionLetterInfo:{
                govPetitionLetters:[],
                _currentGovPetitionLetterName:'',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('chooseGovPetitionLetter','openChooseGovPetitionLetterModel',function(_param){
                $('#chooseGovPetitionLetterModel').modal('show');
                vc.component._refreshChooseGovPetitionLetterInfo();
                vc.component._loadAllGovPetitionLetterInfo(1,10,'');
            });
        },
        methods:{
            _loadAllGovPetitionLetterInfo:function(_page,_row,_name){
                var param = {
                    params:{
                        page:_page,
                        row:_row,
                        caId:vc.getCurrentCommunity().caId,
                        name:_name
                    }
                };

                //发送get请求
               vc.http.apiGet('/govPetitionLetter/queryGovPetitionLetter',
                             param,
                             function(json){
                                var _govPetitionLetterInfo = JSON.parse(json);
                                vc.component.chooseGovPetitionLetterInfo.govPetitionLetters = _govPetitionLetterInfo.govPetitionLetters;
                             },function(){
                                console.log('请求失败处理');
                             }
                           );
            },
            chooseGovPetitionLetter:function(_govPetitionLetter){
                if(_govPetitionLetter.hasOwnProperty('name')){
                     _govPetitionLetter.govPetitionLetterName = _govPetitionLetter.name;
                }
                vc.emit($props.emitChooseGovPetitionLetter,'chooseGovPetitionLetter',_govPetitionLetter);
                vc.emit($props.emitLoadData,'listGovPetitionLetterData',{
                    govPetitionLetterId:_govPetitionLetter.govPetitionLetterId
                });
                $('#chooseGovPetitionLetterModel').modal('hide');
            },
            queryGovPetitionLetters:function(){
                vc.component._loadAllGovPetitionLetterInfo(1,10,vc.component.chooseGovPetitionLetterInfo._currentGovPetitionLetterName);
            },
            _refreshChooseGovPetitionLetterInfo:function(){
                vc.component.chooseGovPetitionLetterInfo._currentGovPetitionLetterName = "";
            }
        }

    });
})(window.vc);
