/**
    服务记录表 组件
**/
(function(vc){

    vc.extends({
        propTypes: {
           callBackListener:vc.propTypes.string, //父组件名称
           callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            viewGovVolunteerServiceRecordInfo:{
                index:0,
                flowComponent:'viewGovVolunteerServiceRecordInfo',
                serviceRecordId:'',
title:'',
typeCd:'',
img:'',
context:'',
caId:'',
startTime:'',
endTime:'',
statusCd:'',

            }
        },
        _initMethod:function(){
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovVolunteerServiceRecordInfoData();
        },
        _initEvent:function(){
            vc.on('viewGovVolunteerServiceRecordInfo','chooseGovVolunteerServiceRecord',function(_app){
                vc.copyObject(_app, vc.component.viewGovVolunteerServiceRecordInfo);
                vc.emit($props.callBackListener,$props.callBackFunction,vc.component.viewGovVolunteerServiceRecordInfo);
            });

            vc.on('viewGovVolunteerServiceRecordInfo', 'onIndex', function(_index){
                vc.component.viewGovVolunteerServiceRecordInfo.index = _index;
            });

        },
        methods:{

            _openSelectGovVolunteerServiceRecordInfoModel(){
                vc.emit('chooseGovVolunteerServiceRecord','openChooseGovVolunteerServiceRecordModel',{});
            },
            _openAddGovVolunteerServiceRecordInfoModel(){
                vc.emit('addGovVolunteerServiceRecord','openAddGovVolunteerServiceRecordModal',{});
            },
            _loadGovVolunteerServiceRecordInfoData:function(){

            }
        }
    });

})(window.vc);
