(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovReportTypeUserInfo: {
                typeUserId: '',
                reportType: '',
                staffId: '',
                staffName: '',
                caId: '',
                ramark: '',

            }
        },
        _initMethod: function () {
            vc.component.addGovReportTypeUserInfo.caId = vc.getCurrentCommunity().caId;
        },
        _initEvent: function () {
            vc.on('addGovReportTypeUser', 'openAddGovReportTypeUserModal', function () {
                $('#addGovReportTypeUserModel').modal('show');
            });
        },
        methods: {
            addGovReportTypeUserValidate() {
                return vc.validate.validate({
                    addGovReportTypeUserInfo: vc.component.addGovReportTypeUserInfo
                }, {
                    'addGovReportTypeUserInfo.reportType': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "报事类型不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "200",
                            errInfo: "报事类型名称超长"
                        },
                    ],
                    'addGovReportTypeUserInfo.staffId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "员工ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "员工ID超长"
                        },
                    ],
                    'addGovReportTypeUserInfo.staffName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "员工名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "员工名称超长"
                        },
                    ],
                    'addGovReportTypeUserInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "社区ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "社区ID超长"
                        },
                    ],
                    'addGovReportTypeUserInfo.ramark': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "备注不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "500",
                            errInfo: "备注太长"
                        },
                    ],




                });
            },
            saveGovReportTypeUserInfo: function () {
                if (!vc.component.addGovReportTypeUserValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }

                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovReportTypeUserInfo);
                    $('#addGovReportTypeUserModel').modal('hide');
                    return;
                }

                vc.http.apiPost(
                    'govReportTypeUser.saveGovReportTypeUser',
                    JSON.stringify(vc.component.addGovReportTypeUserInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovReportTypeUserModel').modal('hide');
                            vc.component.clearAddGovReportTypeUserInfo();
                            vc.emit('govReportTypeUserManage', 'listGovReportTypeUser', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            clearAddGovReportTypeUserInfo: function () {
                vc.component.addGovReportTypeUserInfo = {
                    reportType: '',
                    staffId: '',
                    staffName: '',
                    caId: '',
                    ramark: '',

                };
            }
        }
    });

})(window.vc);
