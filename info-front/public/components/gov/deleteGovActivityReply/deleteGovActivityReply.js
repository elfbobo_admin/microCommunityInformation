(function(vc,vm){

    vc.extends({
        data:{
            deleteGovActivityReplyInfo:{

            }
        },
         _initMethod:function(){

         },
         _initEvent:function(){
             vc.on('deleteGovActivityReply','openDeleteGovActivityReplyModal',function(_params){

                vc.component.deleteGovActivityReplyInfo = _params;
                $('#deleteGovActivityReplyModel').modal('show');

            });
        },
        methods:{
            deleteGovActivityReply:function(){
                vc.component.deleteGovActivityReplyInfo.caId=vc.getCurrentCommunity().caId;
                vc.http.apiPost(
                    '/govActivityReply/deleteGovActivityReply',
                    JSON.stringify(vc.component.deleteGovActivityReplyInfo),
                    {
                        emulateJSON:true
                     },
                     function(json,res){
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovActivityReplyModel').modal('hide');
                            vc.emit('govActivityReplyManage','listGovActivityReply',{});
                            return ;
                        }
                        vc.message(_json.msg);
                     },
                     function(errInfo,error){
                        console.log('请求失败处理');
                        vc.message(json);

                     });
            },
            closeDeleteGovActivityReplyModel:function(){
                $('#deleteGovActivityReplyModel').modal('hide');
            }
        }
    });

})(window.vc,window.vc.component);
