(function(vc){
    vc.extends({
        propTypes: {
           emitChooseGovActivityReply:vc.propTypes.string,
           emitLoadData:vc.propTypes.string
        },
        data:{
            chooseGovActivityReplyInfo:{
                govActivityReplys:[],
                _currentGovActivityReplyName:'',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('chooseGovActivityReply','openChooseGovActivityReplyModel',function(_param){
                $('#chooseGovActivityReplyModel').modal('show');
                vc.component._refreshChooseGovActivityReplyInfo();
                vc.component._loadAllGovActivityReplyInfo(1,10,'');
            });
        },
        methods:{
            _loadAllGovActivityReplyInfo:function(_page,_row,_name){
                var param = {
                    params:{
                        page:_page,
                        row:_row,
                        caId:vc.getCurrentCommunity().caId,
                        name:_name
                    }
                };

                //发送get请求
               vc.http.apiGet('/govActivityReply/queryGovActivityReply',
                             param,
                             function(json){
                                var _govActivityReplyInfo = JSON.parse(json);
                                vc.component.chooseGovActivityReplyInfo.govActivityReplys = _govActivityReplyInfo.govActivityReplys;
                             },function(){
                                console.log('请求失败处理');
                             }
                           );
            },
            chooseGovActivityReply:function(_govActivityReply){
                if(_govActivityReply.hasOwnProperty('name')){
                     _govActivityReply.govActivityReplyName = _govActivityReply.name;
                }
                vc.emit($props.emitChooseGovActivityReply,'chooseGovActivityReply',_govActivityReply);
                vc.emit($props.emitLoadData,'listGovActivityReplyData',{
                    govActivityReplyId:_govActivityReply.govActivityReplyId
                });
                $('#chooseGovActivityReplyModel').modal('hide');
            },
            queryGovActivityReplys:function(){
                vc.component._loadAllGovActivityReplyInfo(1,10,vc.component.chooseGovActivityReplyInfo._currentGovActivityReplyName);
            },
            _refreshChooseGovActivityReplyInfo:function(){
                vc.component.chooseGovActivityReplyInfo._currentGovActivityReplyName = "";
            }
        }

    });
})(window.vc);
