(function(vc){
    vc.extends({
        propTypes: {
           emitChooseGovHelpPolicy:vc.propTypes.string,
           emitLoadData:vc.propTypes.string
        },
        data:{
            chooseGovHelpPolicyInfo:{
                govHelpPolicys:[],
                _currentGovHelpPolicyName:'',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('chooseGovHelpPolicy','openChooseGovHelpPolicyModel',function(_param){
                $('#chooseGovHelpPolicyModel').modal('show');
                vc.component._refreshChooseGovHelpPolicyInfo();
                vc.component._loadAllGovHelpPolicyInfo(1,10,'');
            });
        },
        methods:{
            _loadAllGovHelpPolicyInfo:function(_page,_row,_name){
                var param = {
                    params:{
                        page:_page,
                        row:_row,
                        caId:vc.getCurrentCommunity().caId,
                        name:_name
                    }
                };

                //发送get请求
               vc.http.apiGet('/govHelpPolicy/queryGovHelpPolicy',
                             param,
                             function(json){
                                var _govHelpPolicyInfo = JSON.parse(json);
                                vc.component.chooseGovHelpPolicyInfo.govHelpPolicys = _govHelpPolicyInfo.govHelpPolicys;
                             },function(){
                                console.log('请求失败处理');
                             }
                           );
            },
            chooseGovHelpPolicy:function(_govHelpPolicy){
                if(_govHelpPolicy.hasOwnProperty('name')){
                     _govHelpPolicy.govHelpPolicyName = _govHelpPolicy.name;
                }
                vc.emit($props.emitChooseGovHelpPolicy,'chooseGovHelpPolicy',_govHelpPolicy);
                vc.emit($props.emitLoadData,'listGovHelpPolicyData',{
                    govHelpPolicyId:_govHelpPolicy.govHelpPolicyId
                });
                $('#chooseGovHelpPolicyModel').modal('hide');
            },
            queryGovHelpPolicys:function(){
                vc.component._loadAllGovHelpPolicyInfo(1,10,vc.component.chooseGovHelpPolicyInfo._currentGovHelpPolicyName);
            },
            _refreshChooseGovHelpPolicyInfo:function(){
                vc.component.chooseGovHelpPolicyInfo._currentGovHelpPolicyName = "";
            }
        }

    });
})(window.vc);
