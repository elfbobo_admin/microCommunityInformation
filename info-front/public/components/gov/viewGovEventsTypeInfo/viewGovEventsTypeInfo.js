/**
    事件类型 组件
**/
(function(vc){

    vc.extends({
        propTypes: {
           callBackListener:vc.propTypes.string, //父组件名称
           callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            viewGovEventsTypeInfo:{
                index:0,
                flowComponent:'viewGovEventsTypeInfo',
                typeId:'',
caId:'',
typeName:'',
statusCd:'',
ramark:'',

            }
        },
        _initMethod:function(){
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovEventsTypeInfoData();
        },
        _initEvent:function(){
            vc.on('viewGovEventsTypeInfo','chooseGovEventsType',function(_app){
                vc.copyObject(_app, vc.component.viewGovEventsTypeInfo);
                vc.emit($props.callBackListener,$props.callBackFunction,vc.component.viewGovEventsTypeInfo);
            });

            vc.on('viewGovEventsTypeInfo', 'onIndex', function(_index){
                vc.component.viewGovEventsTypeInfo.index = _index;
            });

        },
        methods:{

            _openSelectGovEventsTypeInfoModel(){
                vc.emit('chooseGovEventsType','openChooseGovEventsTypeModel',{});
            },
            _openAddGovEventsTypeInfoModel(){
                vc.emit('addGovEventsType','openAddGovEventsTypeModal',{});
            },
            _loadGovEventsTypeInfoData:function(){

            }
        }
    });

})(window.vc);
