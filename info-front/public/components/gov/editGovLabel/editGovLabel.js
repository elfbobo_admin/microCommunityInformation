(function (vc, vm) {

    vc.extends({
        data: {
            editGovLabelInfo: {
                govLabelId: '',
                govLabelId: '',
                caId: '',
                labelName: '',
                labelCd: '',
                labelType: '',
                ramark: '',
                statusCd: '',

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('editGovLabel', 'openEditGovLabelModal', function (_params) {
                vc.component.refreshEditGovLabelInfo();
                $('#editGovLabelModel').modal('show');
                vc.copyObject(_params, vc.component.editGovLabelInfo);
                vc.component.editGovLabelInfo.caId = vc.getCurrentCommunity().caId;
            });
        },
        methods: {
            editGovLabelValidate: function () {
                return vc.validate.validate({
                    editGovLabelInfo: vc.component.editGovLabelInfo
                }, {
                    'editGovLabelInfo.labelName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "标签名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "标签名称不能超过30"
                        },
                    ],
                    'editGovLabelInfo.labelCd': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "标签值不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "标签值不能超过30"
                        },
                    ],
                    'editGovLabelInfo.labelType': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "标签类型不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "标签类型不能超过12"
                        },
                    ],
                    'editGovLabelInfo.govLabelId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "标签ID不能为空"
                        }]

                });
            },
            editGovLabel: function () {
                if (!vc.component.editGovLabelValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }

                vc.http.apiPost(
                    '/govLabel/updateGovLabel',
                    JSON.stringify(vc.component.editGovLabelInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovLabelModel').modal('hide');
                            vc.emit('govLabelManage', 'listGovLabel', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            refreshEditGovLabelInfo: function () {
                vc.component.editGovLabelInfo = {
                    govLabelId: '',
                    govLabelId: '',
                    caId: '',
                    labelName: '',
                    labelCd: '',
                    labelType: '',
                    ramark: '',
                    statusCd: '',

                }
            }
        }
    });

})(window.vc, window.vc.component);
