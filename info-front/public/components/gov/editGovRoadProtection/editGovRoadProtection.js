(function (vc, vm) {

    vc.extends({
        data: {
            editGovRoadProtectionInfo: {
                roadProtectionId: '',
                roadProtectionId: '',
                caId: '',
                govCommunityId: '',
                roadType: '',
                roadName: '',
                belongToDepartment: '',
                departmentTel: '',
                departmentAddress: '',
                leadingCadreName: '',
                leaderLink: '',
                manageDepartment: '',
                manageDepTel: '',
                manageDepAddress: '',
                branchLeaders: '',
                branchLeadersTel: '',
                safeHiddenGrade: '',
                safeTrouble: '',
                statusCd: '',

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('editGovRoadProtection', 'openEditGovRoadProtectionModal', function (_params) {
                vc.component.refreshEditGovRoadProtectionInfo();
                $('#editGovRoadProtectionModel').modal('show');
                vc.copyObject(_params, vc.component.editGovRoadProtectionInfo);
                vc.component.editGovRoadProtectionInfo.caId = vc.getCurrentCommunity().caId;
            });
        },
        methods: {
            editGovRoadProtectionValidate: function () {
                return vc.validate.validate({
                    editGovRoadProtectionInfo: vc.component.editGovRoadProtectionInfo
                }, {
                    'editGovRoadProtectionInfo.roadType': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "路线类型不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "路线类型不能超过30"
                        },
                    ],
                    'editGovRoadProtectionInfo.roadName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "100",
                            errInfo: "名称不能超过100"
                        },
                    ],
                    'editGovRoadProtectionInfo.belongToDepartment': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "隶属单位名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "100",
                            errInfo: "隶属单位名称不能超过100"
                        },
                    ],
                    'editGovRoadProtectionInfo.departmentTel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "隶属单位电话不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "隶属单位电话不能超过30"
                        },
                    ],
                    'editGovRoadProtectionInfo.departmentAddress': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "隶属单位地址不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "隶属单位地址不能超过30"
                        },
                    ],
                    'editGovRoadProtectionInfo.leadingCadreName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "隶属单位负责人姓名不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "隶属单位负责人姓名不能超过30"
                        },
                    ],
                    'editGovRoadProtectionInfo.leaderLink': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "隶属单位负责人电话不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "隶属单位负责人电话不能超过11"
                        },
                    ],
                    'editGovRoadProtectionInfo.manageDepartment': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "管辖单位名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "100",
                            errInfo: "管辖单位名称不能超过100"
                        },
                    ],
                    'editGovRoadProtectionInfo.manageDepTel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "管辖单位联系方式不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "管辖单位联系方式不能超过30"
                        },
                    ],
                    'editGovRoadProtectionInfo.manageDepAddress': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "管辖单位地址不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "100",
                            errInfo: "管辖单位地址不能超过100"
                        },
                    ],
                    'editGovRoadProtectionInfo.branchLeaders': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "分管治保负责人姓名不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "分管治保负责人姓名不能超过30"
                        },
                    ],
                    'editGovRoadProtectionInfo.branchLeadersTel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "分管治保负责人电话不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "分管治保负责人电话不能超过11"
                        },
                    ],
                    'editGovRoadProtectionInfo.safeHiddenGrade': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "治安隐患等级不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "5",
                            errInfo: "治安隐患等级不能超过5"
                        },
                    ],
                    'editGovRoadProtectionInfo.safeTrouble': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "治安隐患问题不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "长文本",
                            errInfo: "治安隐患问题不能超过长文本"
                        },
                    ],
                    'editGovRoadProtectionInfo.roadProtectionId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "护路护线ID不能为空"
                        }]

                });
            },
            editGovRoadProtection: function () {
                if (!vc.component.editGovRoadProtectionValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }

                vc.http.apiPost(
                    '/govRoadProtection/updateGovRoadProtection',
                    JSON.stringify(vc.component.editGovRoadProtectionInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovRoadProtectionModel').modal('hide');
                            vc.emit('govRoadProtectionManage', 'listGovRoadProtection', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            refreshEditGovRoadProtectionInfo: function () {
                vc.component.editGovRoadProtectionInfo = {
                    roadProtectionId: '',
                    roadProtectionId: '',
                    caId: '',
                    govCommunityId: '',
                    roadType: '',
                    roadName: '',
                    belongToDepartment: '',
                    departmentTel: '',
                    departmentAddress: '',
                    leadingCadreName: '',
                    leaderLink: '',
                    manageDepartment: '',
                    manageDepTel: '',
                    manageDepAddress: '',
                    branchLeaders: '',
                    branchLeadersTel: '',
                    safeHiddenGrade: '',
                    safeTrouble: '',
                    statusCd: '',

                }
            }
        }
    });

})(window.vc, window.vc.component);
