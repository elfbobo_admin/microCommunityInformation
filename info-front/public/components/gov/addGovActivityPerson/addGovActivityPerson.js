(function(vc){

    vc.extends({
        propTypes: {
               callBackListener:vc.propTypes.string, //父组件名称
               callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            addGovActivityPersonInfo:{
                actPerId:'',
                caId:'',
                actId:'',
                personName:'',
                personLink:'',
                personAge:'',
                personAddress:'',
                remark:''

            }
        },
         _initMethod:function(){
           
         },
         _initEvent:function(){
            vc.on('addGovActivityPerson','openAddGovActivityPersonModal',function(){
                $('#addGovActivityPersonModel').modal('show');
            });
        },
        methods:{
            addGovActivityPersonValidate(){
                return vc.validate.validate({
                    addGovActivityPersonInfo:vc.component.addGovActivityPersonInfo
                },{
                    'addGovActivityPersonInfo.caId':[
{
                            limit:"required",
                            param:"",
                            errInfo:"所属区域不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"30",
                            errInfo:"所属区域超长"
                        },
                    ],
'addGovActivityPersonInfo.actId':[
{
                            limit:"required",
                            param:"",
                            errInfo:"活动名称不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"30",
                            errInfo:"活动名称超长"
                        },
                    ],
'addGovActivityPersonInfo.personName':[
{
                            limit:"required",
                            param:"",
                            errInfo:"人员名称不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"64",
                            errInfo:"人员名称超长"
                        },
                    ],
'addGovActivityPersonInfo.personLink':[
{
                            limit:"required",
                            param:"",
                            errInfo:"人员电话不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"11",
                            errInfo:"请填写人员电话"
                        },
                    ],
'addGovActivityPersonInfo.personAge':[
{
                            limit:"required",
                            param:"",
                            errInfo:"人员年龄不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"11",
                            errInfo:"请填写人员年龄"
                        },
                    ],
'addGovActivityPersonInfo.personAddress':[
{
                            limit:"required",
                            param:"",
                            errInfo:"住址不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"512",
                            errInfo:"住址超长"
                        },
                    ]
                });
            },
            saveGovActivityPersonInfo:function(){
                vc.component.addGovActivityPersonInfo.caId = vc.getCurrentCommunity().caId;
                if(!vc.component.addGovActivityPersonValidate()){
                    vc.toast(vc.validate.errInfo);

                    return ;
                }
                //不提交数据将数据 回调给侦听处理
                if(vc.notNull($props.callBackListener)){
                    vc.emit($props.callBackListener,$props.callBackFunction,vc.component.addGovActivityPersonInfo);
                    $('#addGovActivityPersonModel').modal('hide');
                    return ;
                }

                vc.http.apiPost(
                    '/govActivityPerson/saveGovActivityPerson',
                    JSON.stringify(vc.component.addGovActivityPersonInfo),
                    {
                        emulateJSON:true
                     },
                     function(json,res){
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovActivityPersonModel').modal('hide');
                            vc.component.clearAddGovActivityPersonInfo();
                            vc.emit('govActivityPersonManage','listGovActivityPerson',{});

                            return ;
                        }
                        vc.message(_json.msg);

                     },
                     function(errInfo,error){
                        console.log('请求失败处理');

                        vc.message(errInfo);

                     });
            },
 
            clearAddGovActivityPersonInfo:function(){
                vc.component.addGovActivityPersonInfo = {
                                            caId:'',
                                            actId:'',
                                            personName:'',
                                            personLink:'',
                                            personAge:'',
                                            personAddress:'',
                                            remark:''
                                        };
            }
        }
    });

})(window.vc);
