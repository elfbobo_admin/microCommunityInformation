(function (vc, vm) {

    vc.extends({
        data: {
            editGovDoctorHealthyInfo: {
                govDoctorId: '',
                caId: '',
                relId: '',
                doctorNum: '',
                idType: '',
                idCard: '',
                govPersonId: '',
                personName: '',
                personTel: '',
                personSex: '',
                birthday: '',
                jobName: '',
                titleName: '',
                businessExpertise: '',
                groupId: '',
                name: '',
                labelCd: '',
                titleNames: [],
                jobNames: [],
                govMedicalGroups: []
            }
        },
        _initMethod: function () {
            $that._listEditGovMedicalGroups();
            vc.initDate('editBirthday', function (_value) {
                $that.editGovDoctorHealthyInfo.birthday = _value;
            });
            vc.getDict('gov_doctor_healthy', "title_name", function (_data) {
                vc.component.editGovDoctorHealthyInfo.titleNames = _data;
            });
            vc.getDict('gov_doctor_healthy', "job_name", function (_data) {
                vc.component.editGovDoctorHealthyInfo.jobNames = _data;
            });
        },
        _initEvent: function () {
            vc.on('editGovDoctorHealthy', 'openEditGovDoctorHealthyModal', function (_params) {
                vc.component.refreshEditGovDoctorHealthyInfo();
                $('#editGovDoctorHealthyModel').modal('show');
                vc.copyObject(_params, vc.component.editGovDoctorHealthyInfo);
                vc.component.editGovDoctorHealthyInfo.caId = vc.getCurrentCommunity().caId;
            });
        },
        methods: {
            editGovDoctorHealthyValidate: function () {
                return vc.validate.validate({
                    editGovDoctorHealthyInfo: vc.component.editGovDoctorHealthyInfo
                }, {
                    'editGovDoctorHealthyInfo.govDoctorId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "医生ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "医生ID不能超过30"
                        },
                    ],
                    'editGovDoctorHealthyInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "区域ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "区域ID不能超过30"
                        },
                    ],
                    'editGovDoctorHealthyInfo.doctorNum': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "工号不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "工号不能超过12"
                        },
                    ],
                    'editGovDoctorHealthyInfo.idType': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "证件类型不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "证件类型不能超过12"
                        },
                    ],
                    'editGovDoctorHealthyInfo.idCard': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "证件号码不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "证件号码不能超过64"
                        },
                    ],
                    'editGovDoctorHealthyInfo.govPersonId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "人口ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "人口ID不能超过30"
                        },
                    ],
                    'editGovDoctorHealthyInfo.personName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "姓名不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "姓名不能超过64"
                        },
                    ],
                    'editGovDoctorHealthyInfo.personTel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "电话不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "电话不能超过11"
                        },
                    ],
                    'editGovDoctorHealthyInfo.personSex': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "性别不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "性别不能超过11"
                        },
                    ],
                    'editGovDoctorHealthyInfo.birthday': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "生日不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "生日不能超过时间类型"
                        },
                    ],
                    'editGovDoctorHealthyInfo.jobName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "职务不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "职务不能超过12"
                        },
                    ],
                    'editGovDoctorHealthyInfo.titleName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "职称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "职称不能超过12"
                        },
                    ],
                    'editGovDoctorHealthyInfo.businessExpertise': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "业务专长不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "业务专长不能超过1024"
                        },
                    ],
                    'editGovDoctorHealthyInfo.groupId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "所属团队不能为空"
                        }
                    ],
                    'editGovDoctorHealthyInfo.labelCd': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "角色不能为空"
                        }
                    ]

                });
            },
            editGovDoctorHealthy: function () {
                if (!vc.component.editGovDoctorHealthyValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }
                if(!vc.component.isCardNoEdit(vc.component.editGovDoctorHealthyInfo.idCard)){
                    vc.toast("身份证格式有误，请检查身份证信息");

                    return;
                }
                vc.http.apiPost(
                    '/govDoctorHealthy/updateGovDoctorHealthy',
                    JSON.stringify(vc.component.editGovDoctorHealthyInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovDoctorHealthyModel').modal('hide');
                            vc.emit('govDoctorHealthyManage', 'listGovDoctorHealthy', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            isCardNoEdit: function (card) {
                //身份证号码为15位或者18位，15位时全为数字，18位前17位为数字，最后一位是校验位，可能为数字或字符X
                var reg = /(^\d{15}$)|(^\d{17}(\d|X)$)/;
                if (reg.test(card) === false) {
                    return false;
                }
                return true;
            },
            _listEditGovMedicalGroups: function () {

                var param = {
                    params: {
                        page : 1,
                        row: 50,
                        caId : vc.getCurrentCommunity().caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govMedicalGroup/queryGovMedicalGroup',
                    param,
                    function (json, res) {
                        var _govMedicalGroupManageInfo = JSON.parse(json);
                        vc.component.editGovDoctorHealthyInfo.govMedicalGroups = _govMedicalGroupManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _setEditGroupName: function(_groupId){
                $that.editGovDoctorHealthyInfo.govMedicalGroups.forEach(element => {
                    if(element.groupId = _groupId){
                        $that.editGovDoctorHealthyInfo.name = element.name;
                    }
                });
            },
            refreshEditGovDoctorHealthyInfo: function () {
                let _titleNames = vc.component.editGovDoctorHealthyInfo.titleNames;
                let _jobNames = vc.component.editGovDoctorHealthyInfo.jobNames;
                let _govMedicalGroups = vc.component.editGovDoctorHealthyInfo.govMedicalGroups;
                vc.component.editGovDoctorHealthyInfo = {
                    govDoctorId: '',
                    caId: '',
                    relId: '',
                    doctorNum: '',
                    idType: '',
                    idCard: '',
                    govPersonId: '',
                    personName: '',
                    personTel: '',
                    personSex: '',
                    birthday: '',
                    jobName: '',
                    titleName: '',
                    businessExpertise: '',
                    groupId: '',
                    name: '',
                    labelCd: '',
                    titleNames: _titleNames,
                    jobNames: _jobNames,
                    govMedicalGroups: _govMedicalGroups

                }
            }
        }
    });

})(window.vc, window.vc.component);
