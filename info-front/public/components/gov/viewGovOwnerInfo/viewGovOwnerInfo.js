/**
    户籍管理 组件
**/
(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            viewGovOwnerInfo: {
                index: 0,
                flowComponent: 'viewGovOwnerInfo',
                caId: '',
                roomId: '',
                roomNum: '',
                ownerNum: '',
                ownerName: '',
                ownerType: '',
                idCard: '',
                ownerTel: '',
                ownerAddress: '',
                ramark: '',

            }
        },
        _initMethod: function () {
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovOwnerInfoData();
        },
        _initEvent: function () {
            vc.on('viewGovOwnerInfo', 'chooseGovOwner', function (_app) {
                vc.copyObject(_app, vc.component.viewGovOwnerInfo);
                vc.emit($props.callBackListener, $props.callBackFunction, vc.component.viewGovOwnerInfo);
            });

            vc.on('viewGovOwnerInfo', 'onIndex', function (_index) {
                vc.component.viewGovOwnerInfo.index = _index;
            });

        },
        methods: {

            _openSelectGovOwnerInfoModel() {
                vc.emit('chooseGovOwner', 'openChooseGovOwnerModel', {});
            },
            _openAddGovOwnerInfoModel() {
                vc.emit('addGovOwner', 'openAddGovOwnerModal', {});
            },
            _loadGovOwnerInfoData: function () {

            }
        }
    });

})(window.vc);
