(function (vc, vm) {

    vc.extends({
        data: {
            editGovMedicalGroupInfo: {
                groupId: '',
                caId: '',
                hospitalId: '',
                companyName: '',
                name: '',
                seq: '',
                ramark: '',
            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('editGovMedicalGroup', 'openEditGovMedicalGroupModal', function (_params) {
                vc.component.refreshEditGovMedicalGroupInfo();
                $('#editGovMedicalGroupModel').modal('show');
                vc.copyObject(_params, vc.component.editGovMedicalGroupInfo);
                vc.component.editGovMedicalGroupInfo.caId = vc.getCurrentCommunity().caId;
            });
            vc.on('viewGovCompanyInfo', 'chooseGovCompany', function (_param) {
                $that.editGovMedicalGroupInfo.hospitalId = _param.govCompanyId;
                $that.editGovMedicalGroupInfo.companyName = _param.companyName;
            });
        },
        methods: {
            editGovMedicalGroupValidate: function () {
                return vc.validate.validate({
                    editGovMedicalGroupInfo: vc.component.editGovMedicalGroupInfo
                }, {
                    'editGovMedicalGroupInfo.hospitalId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "医院ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "医院ID不能超过30"
                        },
                    ],
                    'editGovMedicalGroupInfo.name': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "团队名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "团队名称不能超过64"
                        },
                    ],
                    'editGovMedicalGroupInfo.seq': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "列顺序不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "3",
                            errInfo: "列顺序不能超过3"
                        },
                    ],
                    'editGovMedicalGroupInfo.ramark': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "备注不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "备注不能超过1024"
                        },
                    ]
                });
            },
            editGovMedicalGroup: function () {
                if (!vc.component.editGovMedicalGroupValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }

                vc.http.apiPost(
                    '/govMedicalGroup/updateGovMedicalGroup',
                    JSON.stringify(vc.component.editGovMedicalGroupInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovMedicalGroupModel').modal('hide');
                            vc.emit('govMedicalGroupManage', 'listGovMedicalGroup', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            _openEditChooseGovCompanyModel: function () {
                let _caId = vc.getCurrentCommunity().caId;
                if(!_caId){
                    vc.toast("请先选择区域！");
                    return;
                }
                vc.emit( 'chooseGovCompany', 'openChooseGovCompanyModel', _caId);
            },
            _closeEditGovMedicalGroup: function () {
                $that.refreshEditGovMedicalGroupInfo();
                vc.emit('govMedicalGroupManage', 'listGovMedicalGroup', {});
            },
            refreshEditGovMedicalGroupInfo: function () {
                vc.component.editGovMedicalGroupInfo = {
                    groupId: '',
                    caId: '',
                    hospitalId: '',
                    companyName: '',
                    name: '',
                    seq: '',
                    ramark: '',
                }
            }
        }
    });

})(window.vc, window.vc.component);
