(function(vc,vm){

    vc.extends({
        data:{
            deleteGovReportSettingInfo:{

            }
        },
         _initMethod:function(){

         },
         _initEvent:function(){
             vc.on('deleteGovReportSetting','openDeleteGovReportSettingModal',function(_params){

                vc.component.deleteGovReportSettingInfo = _params;
                $('#deleteGovReportSettingModel').modal('show');

            });
        },
        methods:{
            deleteGovReportSetting:function(){
                
                vc.http.apiPost(
                    '/govReportSetting/deleteGovReportSetting',
                    JSON.stringify(vc.component.deleteGovReportSettingInfo),
                    {
                        emulateJSON:true
                     },
                     function(json,res){
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovReportSettingModel').modal('hide');
                            vc.emit('govReportSettingManage','listGovReportSetting',{});
                            return ;
                        }
                        vc.message(_json.msg);
                     },
                     function(errInfo,error){
                        console.log('请求失败处理');
                        vc.message(json);

                     });
            },
            closeDeleteGovReportSettingModel:function(){
                $('#deleteGovReportSettingModel').modal('hide');
            }
        }
    });

})(window.vc,window.vc.component);
