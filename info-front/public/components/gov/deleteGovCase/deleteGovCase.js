(function (vc, vm) {

    vc.extends({
        data: {
            deleteGovCaseInfo: {

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('deleteGovCase', 'openDeleteGovCaseModal', function (_params) {

                vc.component.deleteGovCaseInfo = _params;
                $('#deleteGovCaseModel').modal('show');

            });
        },
        methods: {
            deleteGovCase: function () {
                vc.component.deleteGovCaseInfo.caId = vc.getCurrentCommunity().caId;
                vc.http.apiPost(
                    '/govCase/deleteGovCase',
                    JSON.stringify(vc.component.deleteGovCaseInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovCaseModel').modal('hide');
                            vc.emit('govCaseManage', 'listGovCase', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');
                        vc.message(json);

                    });
            },
            closeDeleteGovCaseModel: function () {
                $('#deleteGovCaseModel').modal('hide');
            }
        }
    });

})(window.vc, window.vc.component);
