(function (vc) {
    vc.extends({
        data: {
            viewCarInfo: {
                orgId: '',
                govPersonId: '',
                cars: []
            }
        },
        _initMethod: function () {
        },
        _initEvent: function () {
            vc.on('carInfo', 'switch', function (_param) {
                vc.component._refreshviewGovCarsInfo();
                vc.component._loadCarsInfo(_param.govPersonId);
            });
        },
        methods: {
            _loadCarsInfo: function (_personId) {
                var param = {
                    params: {
                        page: 1,
                        row: 10,
                        caId: vc.getCurrentCommunity().caId,
                        govPersonId: _personId
                    }
                };
                //发送get请求
                vc.http.apiGet('/car/queryCar',
                    param,
                    function (json) {
                        var _labelInfo = JSON.parse(json);
                        $that.viewCarInfo.cars = _labelInfo.data;
                    }, function () {
                        console.log('请求失败处理');
                    }
                );
            },
            _refreshviewGovCarsInfo: function () {
                $that.viewCarInfo = {
                    orgId: '',
                    govPersonId: '',
                    cars: []
                }
            }
        }

    });
})(window.vc);
