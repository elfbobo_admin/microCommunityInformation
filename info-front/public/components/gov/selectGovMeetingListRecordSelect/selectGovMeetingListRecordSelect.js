(function (vc, vm) {

    vc.extends({
        data: {
            selectGovMeetingListRecordSelectInfo: {
                govMeetingId: '',
                caId: '',
                typeId: '',
                meetingTypeName: '',
                orgName: '',
                orgId: '',
                govMemberId: '',
                memberName: '',
                meetingName: '',
                meetingTime: '',
                meetingAddress: '',
                meetingBrief: '',
                meetingPoints: '',
                meetingContent: '',
                meetingImgs: [],
                govMembers: [],
                govMeetingTypes: [],
                govPartyOrgs: []
            }
        },
        _initMethod: function () {
           
        },
        _initEvent: function () {
            vc.on('selectGovMeetingListRecordSelect', 'openselectGovMeetingListRecordSelectModal', function (_params) {
                vc.component.refreshselectGovMeetingListRecordSelectInfo();
                $('#selectGovMeetingListRecordSelectModel').modal('show');
                console.log(_params);
                vc.copyObject(_params, vc.component.selectGovMeetingListRecordSelectInfo);
                vc.component.selectGovMeetingListRecordSelectInfo.govMembers = _params.govMeetingMemberRelDtos;
               
                vc.component.selectGovMeetingListRecordSelectInfo.meetingImgs = _params.meetingImgs;
                vc.component.selectGovMeetingListRecordSelectInfo.caId = vc.getCurrentCommunity().caId;

                vc.emit('viewEditGovPartyMemberRecord', 'openviewEditGovPartyMemberRecordModel', $that.selectGovMeetingListRecordSelectInfo.govMembers);
      
                vc.emit('selectGovMeetingListRecordSelectCover','uploadImage', 'notifyPhotos',vc.component.selectGovMeetingListRecordSelectInfo.meetingImgs);
            });
            vc.on("selectGovMeetingListRecordSelect", "notifyUploadCoverImage", function (_param) {
                if(_param.length > 0){
                    vc.component.selectGovMeetingListRecordSelectInfo.meetingImgs=_param;
                }else{
                    vc.component.selectGovMeetingListRecordSelectInfo.meetingImgs = [];
                }
                
            });
        },
        methods: {
            _clearselectGovMeetingListRecordSelectModel: function () {
                $that.refreshselectGovMeetingListRecordSelectInfo();
                vc.emit('govMeetingListRecord', 'listGovMeetingList', {});
            },
            refreshselectGovMeetingListRecordSelectInfo: function () {
                let _govMembers = vc.component.selectGovMeetingListRecordSelectInfo.govMembers;
                let _govMeetingTypes = vc.component.selectGovMeetingListRecordSelectInfo.govMeetingTypes;
                let _govPartyOrgs = vc.component.selectGovMeetingListRecordSelectInfo.govPartyOrgs;
                vc.component.selectGovMeetingListRecordSelectInfo = {
                    govMeetingId: '',
                    caId: '',
                    typeId: '',
                    meetingTypeName: '',
                    orgName: '',
                    orgId: '',
                    govMemberId: '',
                    memberName: '',
                    meetingName: '',
                    meetingTime: '',
                    meetingAddress: '',
                    meetingBrief: '',
                    meetingPoints: '',
                    meetingContent: '',
                    meetingImgs: [],
                    govMembers: _govMembers,
                    govMeetingTypes: _govMeetingTypes,
                    govPartyOrgs: _govPartyOrgs
                }
            }
        }
    });

})(window.vc, window.vc.component);
