/**
    护路护线 组件
**/
(function(vc){

    vc.extends({
        propTypes: {
           callBackListener:vc.propTypes.string, //父组件名称
           callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            viewGovRoadProtectionInfo:{
                index:0,
                flowComponent:'viewGovRoadProtectionInfo',
                roadProtectionId:'',
caId:'',
govCommunityId:'',
roadType:'',
roadName:'',
belongToDepartment:'',
departmentTel:'',
departmentAddress:'',
leadingCadreName:'',
leaderLink:'',
manageDepartment:'',
manageDepTel:'',
manageDepAddress:'',
branchLeaders:'',
branchLeadersTel:'',
safeHiddenGrade:'',
safeTrouble:'',
statusCd:'',

            }
        },
        _initMethod:function(){
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovRoadProtectionInfoData();
        },
        _initEvent:function(){
            vc.on('viewGovRoadProtectionInfo','chooseGovRoadProtection',function(_app){
                vc.copyObject(_app, vc.component.viewGovRoadProtectionInfo);
                vc.emit($props.callBackListener,$props.callBackFunction,vc.component.viewGovRoadProtectionInfo);
            });

            vc.on('viewGovRoadProtectionInfo', 'onIndex', function(_index){
                vc.component.viewGovRoadProtectionInfo.index = _index;
            });

        },
        methods:{

            _openSelectGovRoadProtectionInfoModel(){
                vc.emit('chooseGovRoadProtection','openChooseGovRoadProtectionModel',{});
            },
            _openAddGovRoadProtectionInfoModel(){
                vc.emit('addGovRoadProtection','openAddGovRoadProtectionModal',{});
            },
            _loadGovRoadProtectionInfoData:function(){

            }
        }
    });

})(window.vc);
