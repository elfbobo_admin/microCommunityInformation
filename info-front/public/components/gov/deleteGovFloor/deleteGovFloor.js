(function (vc, vm) {

    vc.extends({
        data: {
            deleteGovFloorInfo: {

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('deleteGovFloor', 'openDeleteGovFloorModal', function (_params) {

                vc.component.deleteGovFloorInfo = _params;
                $('#deleteGovFloorModel').modal('show');

            });
        },
        methods: {
            deleteGovFloor: function () {
                //vc.component.deleteGovFloorInfo.communityId = vc.getCurrentCommunity().communityId;
                vc.http.apiPost(
                    '/govFloor/deleteGovFloor',
                    JSON.stringify(vc.component.deleteGovFloorInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovFloorModel').modal('hide');
                            vc.emit('govFloorManage', 'listGovFloor', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');
                        vc.message(json);

                    });
            },
            closeDeleteGovFloorModel: function () {
                $('#deleteGovFloorModel').modal('hide');
            }
        }
    });

})(window.vc, window.vc.component);
