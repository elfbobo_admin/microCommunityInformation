/**
    体检项 组件
**/
(function(vc){

    vc.extends({
        propTypes: {
           callBackListener:vc.propTypes.string, //父组件名称
           callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            viewGovHealthTermInfo:{
                index:0,
                flowComponent:'viewGovHealthTermInfo',
                termId:'',
termName:'',
caId:'',
seq:'',
statusCd:'',

            }
        },
        _initMethod:function(){
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovHealthTermInfoData();
        },
        _initEvent:function(){
            vc.on('viewGovHealthTermInfo','chooseGovHealthTerm',function(_app){
                vc.copyObject(_app, vc.component.viewGovHealthTermInfo);
                vc.emit($props.callBackListener,$props.callBackFunction,vc.component.viewGovHealthTermInfo);
            });

            vc.on('viewGovHealthTermInfo', 'onIndex', function(_index){
                vc.component.viewGovHealthTermInfo.index = _index;
            });

        },
        methods:{

            _openSelectGovHealthTermInfoModel(){
                vc.emit('chooseGovHealthTerm','openChooseGovHealthTermModel',{});
            },
            _openAddGovHealthTermInfoModel(){
                vc.emit('addGovHealthTerm','openAddGovHealthTermModal',{});
            },
            _loadGovHealthTermInfoData:function(){

            }
        }
    });

})(window.vc);
