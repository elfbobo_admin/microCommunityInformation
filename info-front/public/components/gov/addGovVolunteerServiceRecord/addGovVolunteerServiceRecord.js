(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovVolunteerServiceRecordInfo: {
                serviceRecordId: '',
                title: '',
                typeCd: '',
                img: '',
                context: '',
                caId: '',
                startTime: '',
                endTime: '',
                volunteerNAme: '',
                statusCd: '',
                oldPersons:[],
            }
        },
        _initMethod: function () {
            $that._initContentInfo();
        },
        _initEvent: function () {
            vc.on('addGovVolunteerServiceRecord', 'openAddGovVolunteerServiceRecordModal', function () {
                $('#addGovVolunteerServiceRecordModel').modal('show');
            });
            vc.on('viewGovOldBirthPerson', 'page_event', function (_param) {
                $that.addGovVolunteerServiceRecordInfo.oldPersons = _param;
            });
            vc.on('openChooseGovVolunteer', 'chooseGovVolunteer', function (_param) {
                let _volunteer = {
                    volunteerId: _param.volunteerId,
                };
                vc.component.addGovVolunteerServiceRecordInfo.volunteerNAme = _param.name;
                vc.component.addGovVolunteerServiceRecordInfo.volunteers = [];
                vc.component.addGovVolunteerServiceRecordInfo.volunteers.push(_volunteer);
            });
        },
        methods: {
            addGovVolunteerServiceRecordValidate() {
                return vc.validate.validate({
                    addGovVolunteerServiceRecordInfo: vc.component.addGovVolunteerServiceRecordInfo
                }, {
                    'addGovVolunteerServiceRecordInfo.title': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "记录标题不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "200",
                            errInfo: "记录标题不能超过200"
                        },
                    ],
                    'addGovVolunteerServiceRecordInfo.context': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "活动内容不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "长文本",
                            errInfo: "活动内容不能超过长文本"
                        },
                    ],
                    'addGovVolunteerServiceRecordInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "区域ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "区域ID不能超过30"
                        },
                    ],
                    'addGovVolunteerServiceRecordInfo.startTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "开始时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "开始时间不能超过时间类型"
                        },
                    ],
                    'addGovVolunteerServiceRecordInfo.endTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "结束时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "结束时间不能超过时间类型"
                        },
                    ],
                });
            },
            saveGovVolunteerServiceRecordInfo: function () {
                vc.component.addGovVolunteerServiceRecordInfo.caId = vc.getCurrentCommunity().caId;
                if (!vc.component.addGovVolunteerServiceRecordValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovVolunteerServiceRecordInfo);
                    $('#addGovVolunteerServiceRecordModel').modal('hide');
                    return;
                }
                vc.http.apiPost(
                    '/govVolunteerServiceRecord/saveGovVolunteerServiceRecord',
                    JSON.stringify(vc.component.addGovVolunteerServiceRecordInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovVolunteerServiceRecordModel').modal('hide');
                            vc.component.clearAddGovVolunteerServiceRecordInfo();
                            vc.emit('govVolunteerServiceRecordManage', 'listGovVolunteerServiceRecord', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            _initContentInfo: function () {
                vc.initDateTime('addStartTime', function (_value) {
                    $that.addGovVolunteerServiceRecordInfo.startTime = _value;
                });
                vc.initDateTime('addEndTime', function (_value) {
                    $that.addGovVolunteerServiceRecordInfo.endTime = _value;
                });
                var $summernote = $('.summernote').summernote({
                    lang: 'zh-CN',
                    height: 300,
                    placeholder: '必填，请输入公告内容',
                    callbacks: {
                        onImageUpload: function (files, editor, $editable) {
                            $that.sendAddFile($summernote,files[0]);
                        },
                        onChange: function (contents, $editable) {
                            vc.component.addGovVolunteerServiceRecordInfo.context = contents;
                        }
                    },
                    toolbar: [
                        ['style', ['style']],
                        ['font', ['bold', 'italic', 'underline', 'clear']],
                        ['fontname', ['fontname']],
                        ['color', ['color']],
                        ['para', ['ul', 'ol', 'paragraph']],
                        ['height', ['height']],
                        ['table', ['table']],
                        ['insert', ['link', 'picture']],
                        ['view', ['fullscreen', 'codeview']],
                        ['help', ['help']]
                    ],
                });
            },
            sendAddFile: function ($summernote,files) {
                console.log('上传图片', files);

                var param = new FormData();
                param.append("uploadFile", files);
                vc.http.uploadImage(
                    param,
                    {
                        emulateJSON: true,
                        //添加请求头
                        headers: {
                            "Content-Type": "multipart/form-data"
                        }
                    },
                    function (json, res) {
                        if (res.status == 200) {
                            var data = JSON.parse(json);
                            $summernote.summernote('insertImage', data.data);
                            return;
                        }
                        vc.toast(json);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');
                        vc.toast(errInfo);
                    });

            },
            _openAddChooseGovVolunteerModel: function () {
                vc.emit( 'chooseGovVolunteer', 'openChooseGovVolunteerModel', {});
            },
            _clearAddGovMeetingListModel: function () {
                $that.clearAddGovVolunteerServiceRecordInfo();
                vc.emit('govVolunteerServiceRecordManage', 'listGovVolunteerServiceRecord', {});
            },
            clearAddGovVolunteerServiceRecordInfo: function () {
                vc.component.addGovVolunteerServiceRecordInfo = {
                    serviceRecordId: '',
                    title: '',
                    typeCd: '',
                    img: '',
                    context: '',
                    caId: '',
                    startTime: '',
                    endTime: '',
                    statusCd: '',
                    oldPersons:[],

                };
            }
        }
    });

})(window.vc);
