(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovCommunityarCorrectionInfo: {
                correctionId: '',
                name: '',
                caId: '',
                address: '',
                tel: '',
                correctionStartTime: '',
                correctionEndTime: '',
                correctionReason: '',
                ramark: '',
                statusCd: '',

            }
        },
        _initMethod: function () {
            vc.initDateTime('addStartTime', function (_value) {
                $that.addGovCommunityarCorrectionInfo.correctionStartTime = _value;
            });
            vc.initDateTime('addEndTime', function (_value) {
                $that.addGovCommunityarCorrectionInfo.correctionEndTime = _value;
            });
        },
        _initEvent: function () {
            vc.on('addGovCommunityarCorrection', 'openAddGovCommunityarCorrectionModal', function () {
                $('#addGovCommunityarCorrectionModel').modal('show');
            });
        },
        methods: {
            addGovCommunityarCorrectionValidate() {
                return vc.validate.validate({
                    addGovCommunityarCorrectionInfo: vc.component.addGovCommunityarCorrectionInfo
                }, {
                    
                    'addGovCommunityarCorrectionInfo.name': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "矫正者名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "矫正者名称不能超过64"
                        },
                    ],
                   
                    'addGovCommunityarCorrectionInfo.address': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "住址不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "512",
                            errInfo: "住址不能超过512"
                        },
                    ],
                    'addGovCommunityarCorrectionInfo.tel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "手机号不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "手机号不能超过11"
                        },
                    ],
                    'addGovCommunityarCorrectionInfo.correctionStartTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "矫正开始时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "矫正开始时间不能超过时间类型"
                        },
                    ],
                    'addGovCommunityarCorrectionInfo.correctionEndTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "矫正结束时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "矫正结束时间不能超过时间类型"
                        },
                    ],
                    'addGovCommunityarCorrectionInfo.correctionReason': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "矫正原因不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "128",
                            errInfo: "矫正原因不能超过128"
                        },
                    ],
                });
            },
            saveGovCommunityarCorrectionInfo: function () {
                if (!vc.component.addGovCommunityarCorrectionValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }

                vc.component.addGovCommunityarCorrectionInfo.caId = vc.getCurrentCommunity().caId;
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovCommunityarCorrectionInfo);
                    $('#addGovCommunityarCorrectionModel').modal('hide');
                    return;
                }
                vc.http.apiPost(
                    '/govCommunityarCorrection/saveGovCommunityarCorrection',
                    JSON.stringify(vc.component.addGovCommunityarCorrectionInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovCommunityarCorrectionModel').modal('hide');
                            vc.component.clearAddGovCommunityarCorrectionInfo();
                            vc.emit('govCommunityarCorrectionManage', 'listGovCommunityarCorrection', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            clearAddGovCommunityarCorrectionInfo: function () {
                vc.component.addGovCommunityarCorrectionInfo = {
                    correctionId: '',
                    name: '',
                    caId: '',
                    address: '',
                    tel: '',
                    correctionStartTime: '',
                    correctionEndTime: '',
                    correctionReason: '',
                    ramark: '',
                    statusCd: '',

                };
            }
        }
    });

})(window.vc);
