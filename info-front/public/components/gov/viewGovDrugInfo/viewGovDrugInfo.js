/**
    吸毒者 组件
**/
(function(vc){

    vc.extends({
        propTypes: {
           callBackListener:vc.propTypes.string, //父组件名称
           callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            viewGovDrugInfo:{
                index:0,
                flowComponent:'viewGovDrugInfo',
                drugId:'',
name:'',
caId:'',
address:'',
tel:'',
drugStartTime:'',
drugEndTime:'',
drugReason:'',
emergencyPerson:'',
emergencyTel:'',
ramark:'',
statusCd:'',

            }
        },
        _initMethod:function(){
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovDrugInfoData();
        },
        _initEvent:function(){
            vc.on('viewGovDrugInfo','chooseGovDrug',function(_app){
                vc.copyObject(_app, vc.component.viewGovDrugInfo);
                vc.emit($props.callBackListener,$props.callBackFunction,vc.component.viewGovDrugInfo);
            });

            vc.on('viewGovDrugInfo', 'onIndex', function(_index){
                vc.component.viewGovDrugInfo.index = _index;
            });

        },
        methods:{

            _openSelectGovDrugInfoModel(){
                vc.emit('chooseGovDrug','openChooseGovDrugModel',{});
            },
            _openAddGovDrugInfoModel(){
                vc.emit('addGovDrug','openAddGovDrugModal',{});
            },
            _loadGovDrugInfoData:function(){

            }
        }
    });

})(window.vc);
