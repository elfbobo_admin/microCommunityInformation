(function (vc, vm) {

    vc.extends({
        data: {
            deleteGovVolunteerInfo: {

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('deleteGovVolunteer', 'openDeleteGovVolunteerModal', function (_params) {

                vc.component.deleteGovVolunteerInfo = _params;
                $('#deleteGovVolunteerModel').modal('show');

            });
        },
        methods: {
            deleteGovVolunteer: function () {
                vc.component.deleteGovVolunteerInfo.caId = vc.getCurrentCommunity().caId;
                vc.http.apiPost(
                    '/govVolunteer/deleteGovVolunteer',
                    JSON.stringify(vc.component.deleteGovVolunteerInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovVolunteerModel').modal('hide');
                            vc.emit('govVolunteerManage', 'listGovVolunteer', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');
                        vc.message(json);

                    });
            },
            closeDeleteGovVolunteerModel: function () {
                $('#deleteGovVolunteerModel').modal('hide');
            }
        }
    });

})(window.vc, window.vc.component);
