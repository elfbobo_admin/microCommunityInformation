(function (vc, vm) {

    vc.extends({
        data: {
            editGovCarInfo: {
                carId: '',
                govCommunityId: '',
                paId: '',
                carNum: '',
                carBrand: '',
                carColor: '',
                carType: '',
                startTime: '',
                endTime: '',
                remark: '',

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('editGovCar', 'openEditGovCarModal', function (_params) {
                vc.component.refreshEditGovCarInfo();
                $('#editGovCarModel').modal('show');
                vc.copyObject(_params, vc.component.editGovCarInfo);
                vc.component.editGovCarInfo.caId = vc.getCurrentCommunity().caId;
                $that._listEditGovCommunitys();
                $that._listEditGovParkingAreas();
            });
        },
        methods: {
            editGovCarValidate: function () {
                return vc.validate.validate({
                    editGovCarInfo: vc.component.editGovCarInfo
                }, {
                    'editGovCarInfo.govCommunityId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "小区不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "200",
                            errInfo: "小区超长"
                        },
                    ],
                    'editGovCarInfo.paId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "停车场不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "100",
                            errInfo: "停车场超长"
                        },
                    ],
                    'editGovCarInfo.carNum': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "车牌号不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "车牌号超过12位"
                        },
                    ],
                    'editGovCarInfo.carBrand': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "车辆品牌不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "50",
                            errInfo: "车辆品牌超过50位"
                        },
                    ],
                    'editGovCarInfo.carColor': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "车辆颜色不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "车辆颜色超过12位"
                        },
                    ],
                    'editGovCarInfo.carType': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "车辆类型不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "100",
                            errInfo: "车辆类型超长"
                        },
                    ],
                    'editGovCarInfo.startTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "起租时间不能为空"
                        },
                        {
                            limit: "datetime",
                            param: "",
                            errInfo: "起租时间格式错误"
                        },
                    ],
                    'editGovCarInfo.endTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "截租时间不能为空"
                        },
                        {
                            limit: "datetime",
                            param: "",
                            errInfo: "截租时间格式错误"
                        },
                    ],
                    'editGovCarInfo.remark': [
                        {
                            limit: "maxLength",
                            param: "200",
                            errInfo: "备注内容不能超过200"
                        },
                    ],
                    'editGovCarInfo.carId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "车辆编号不能为空"
                        }]

                });
            },
            editGovCar: function () {
                if (!vc.component.editGovCarValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }

                vc.http.apiPost(
                    '/car/updateCar',
                    JSON.stringify(vc.component.editGovCarInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovCarModel').modal('hide');
                            vc.emit('govCarManage', 'listGovCar', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            _listEditGovCommunitys: function () {

                let param = {
                    params: {
                        page: 1,
                        row: 50,
                        caId: vc.getCurrentCommunity().caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govCommunity/queryGovCommunity',
                    param,
                    function (json, res) {
                        var _govCommunityManageInfo = JSON.parse(json);
                        vc.component.editGovCarInfo.govCommunitys = _govCommunityManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },

            _listEditGovParkingAreas: function (_page, _rows) {

                vc.component.govParkingAreaManageInfo.conditions.page = _page;
                vc.component.govParkingAreaManageInfo.conditions.row = _rows;
                var param = {
                    params: vc.component.govParkingAreaManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/parkingArea/queryParkingArea',
                    param,
                    function (json, res) {
                        var _govParkingAreaManageInfo = JSON.parse(json);
                        let govParkingAreas = _govParkingAreaManageInfo.data;
                        $that.editGovCarInfo.govParkingAreas = govParkingAreas;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            refreshEditGovCarInfo: function () {
                vc.component.editGovCarInfo = {
                    carId: '',
                    govCommunityId: '',
                    paId: '',
                    carNum: '',
                    carBrand: '',
                    carColor: '',
                    carType: '',
                    startTime: '',
                    endTime: '',
                    remark: '',

                }
            }
        }
    });

})(window.vc, window.vc.component);
