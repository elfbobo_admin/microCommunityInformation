(function (vc, vm) {

    vc.extends({
        data: {
            editGovVolunteerServInfo: {
                servId: '',
                volunteerId: '',
                caId: '',
                name: '',
                ramark: '',

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('editGovVolunteerServ', 'openEditGovVolunteerServModal', function (_params) {
                vc.component.refreshEditGovVolunteerServInfo();
                $('#editGovVolunteerServModel').modal('show');
                vc.copyObject(_params, vc.component.editGovVolunteerServInfo);
                vc.component.editGovVolunteerServInfo.caId = vc.getCurrentCommunity().caId;
            });
        },
        methods: {
            editGovVolunteerServValidate: function () {
                return vc.validate.validate({
                    editGovVolunteerServInfo: vc.component.editGovVolunteerServInfo
                }, {
                    'editGovVolunteerServInfo.servId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "服务领域ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "服务领域ID不能超过30"
                        },
                    ],
                    'editGovVolunteerServInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "区域ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "区域ID不能超过30"
                        },
                    ],
                    'editGovVolunteerServInfo.name': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "领域名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "领域名称不能超过64"
                        },
                    ],
                    'editGovVolunteerServInfo.ramark': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "备注不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "备注不能超过1024"
                        },
                    ]

                });
            },
            editGovVolunteerServ: function () {
                if (!vc.component.editGovVolunteerServValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }

                vc.http.apiPost(
                    '/govVolunteerServ/updateGovVolunteerServ',
                    JSON.stringify(vc.component.editGovVolunteerServInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovVolunteerServModel').modal('hide');
                            vc.emit('govVolunteerServManage', 'listGovVolunteerServ', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            refreshEditGovVolunteerServInfo: function () {
                vc.component.editGovVolunteerServInfo = {
                    servId: '',
                    servId: '',
                    volunteerId: '',
                    caId: '',
                    name: '',
                    ramark: '',

                }
            }
        }
    });

})(window.vc, window.vc.component);
