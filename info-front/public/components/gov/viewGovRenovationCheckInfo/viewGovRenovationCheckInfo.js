/**
    排查整治情况 组件
**/
(function(vc){

    vc.extends({
        propTypes: {
           callBackListener:vc.propTypes.string, //父组件名称
           callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            viewGovRenovationCheckInfo:{
                index:0,
                flowComponent:'viewGovRenovationCheckInfo',
                govCheckId:'',
govRenovationId:'',
caId:'',
effectEvaluation:'',
mainActivities:'',
remediationEffect:'',
remediationSummary:'',

            }
        },
        _initMethod:function(){
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovRenovationCheckInfoData();
        },
        _initEvent:function(){
            vc.on('viewGovRenovationCheckInfo','chooseGovRenovationCheck',function(_app){
                vc.copyObject(_app, vc.component.viewGovRenovationCheckInfo);
                vc.emit($props.callBackListener,$props.callBackFunction,vc.component.viewGovRenovationCheckInfo);
            });

            vc.on('viewGovRenovationCheckInfo', 'onIndex', function(_index){
                vc.component.viewGovRenovationCheckInfo.index = _index;
            });

        },
        methods:{

            _openSelectGovRenovationCheckInfoModel(){
                vc.emit('chooseGovRenovationCheck','openChooseGovRenovationCheckModel',{});
            },
            _openAddGovRenovationCheckInfoModel(){
                vc.emit('addGovRenovationCheck','openAddGovRenovationCheckModal',{});
            },
            _loadGovRenovationCheckInfoData:function(){

            }
        }
    });

})(window.vc);
