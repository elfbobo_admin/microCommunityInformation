(function (vc, vm) {

    vc.extends({
        data: {
            editGovDrugInfo: {
                drugId: '',
                name: '',
                caId: '',
                address: '',
                tel: '',
                drugStartTime: '',
                drugEndTime: '',
                drugReason: '',
                emergencyPerson: '',
                emergencyTel: '',
                ramark: '',
                statusCd: '',

            }
        },
        _initMethod: function () {
            $('.editStartTime').datetimepicker({
                language: 'zh-CN',
                fontAwesome: 'fa',
                format: 'yyyy-mm-dd hh:ii:ss',
                initTime: true,
                initialDate: new Date(),
                autoClose: 1,
                todayBtn: true

            });
            $('.editStartTime').datetimepicker()
                .on('changeDate', function (ev) {
                    var value = $(".editStartTime").val();
                    vc.component.editGovDrugInfo.drugStartTime = value;
                });
            $('.editEndTime').datetimepicker({
                language: 'zh-CN',
                fontAwesome: 'fa',
                format: 'yyyy-mm-dd hh:ii:ss',
                initTime: true,
                initialDate: new Date(),
                autoClose: 1,
                todayBtn: true
            });
            $('.editEndTime').datetimepicker()
                .on('changeDate', function (ev) {
                    var value = $(".editEndTime").val();
                    vc.component.editGovDrugInfo.drugEndTime = value;
                });
        },
        _initEvent: function () {
            vc.on('editGovDrug', 'openEditGovDrugModal', function (_params) {
                vc.component.refreshEditGovDrugInfo();
                $('#editGovDrugModel').modal('show');
                vc.copyObject(_params, vc.component.editGovDrugInfo);
                vc.component.editGovDrugInfo.caId = vc.getCurrentCommunity().caId;
            });
        },
        methods: {
            editGovDrugValidate: function () {
                return vc.validate.validate({
                    editGovDrugInfo: vc.component.editGovDrugInfo
                }, {
                    'editGovDrugInfo.name': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "吸毒者名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "吸毒者名称不能超过64"
                        },
                    ],
                    'editGovDrugInfo.address': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "住址不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "512",
                            errInfo: "住址不能超过512"
                        },
                    ],
                    'editGovDrugInfo.tel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "手机号不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "手机号不能超过11"
                        },
                    ],
                    'editGovDrugInfo.drugStartTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "吸毒开始时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "吸毒开始时间不能超过时间类型"
                        },
                    ],
                    'editGovDrugInfo.drugEndTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "吸毒结束时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "吸毒结束时间不能超过时间类型"
                        },
                    ],
                    'editGovDrugInfo.drugReason': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "吸毒原因不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "128",
                            errInfo: "吸毒原因不能超过128"
                        },
                    ],
                    'editGovDrugInfo.emergencyPerson': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "紧急联系人不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "紧急联系人不能超过64"
                        },
                    ],
                    'editGovDrugInfo.emergencyTel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "紧急联系电话不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "紧急联系电话不能超过11"
                        },
                    ],
                    'editGovDrugInfo.drugId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "吸毒者ID不能为空"
                        }]

                });
            },
            editGovDrug: function () {
                if (!vc.component.editGovDrugValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }

                vc.http.apiPost(
                    '/govDrug/updateGovDrug',
                    JSON.stringify(vc.component.editGovDrugInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovDrugModel').modal('hide');
                            vc.emit('govDrugManage', 'listGovDrug', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            refreshEditGovDrugInfo: function () {
                vc.component.editGovDrugInfo = {
                    drugId: '',
                    drugId: '',
                    name: '',
                    caId: '',
                    address: '',
                    tel: '',
                    drugStartTime: '',
                    drugEndTime: '',
                    drugReason: '',
                    emergencyPerson: '',
                    emergencyTel: '',
                    ramark: '',
                    statusCd: '',

                }
            }
        }
    });

})(window.vc, window.vc.component);
