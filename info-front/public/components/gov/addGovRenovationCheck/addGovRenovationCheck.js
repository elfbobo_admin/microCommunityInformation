(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovRenovationCheckInfo: {
                govCheckId: '',
                govRenovationId: '',
                caId: '',
                effectEvaluation: '',
                mainActivities: '',
                remediationEffect: '',
                remediationSummary: '',
                govAreaRenovations: []
            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('addGovRenovationCheck', 'openAddGovRenovationCheckModal', function () {
                $that._addGovAreaRenovations();
                $('#addGovRenovationCheckModel').modal('show');
            });
        },
        methods: {
            addGovRenovationCheckValidate() {
                return vc.validate.validate({
                    addGovRenovationCheckInfo: vc.component.addGovRenovationCheckInfo
                }, {
                   
                    'addGovRenovationCheckInfo.effectEvaluation': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "效果评估不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "效果评估不能超过11"
                        },
                    ],
                    'addGovRenovationCheckInfo.mainActivities': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "主要活动不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "251",
                            errInfo: "主要活动不能超过251"
                        },
                    ],
                    'addGovRenovationCheckInfo.remediationEffect': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "整治效果不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "251",
                            errInfo: "整治效果不能超过251"
                        },
                    ],
                    'addGovRenovationCheckInfo.remediationSummary': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "整治总结不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "251",
                            errInfo: "整治总结不能超过251"
                        },
                    ],




                });
            },
            saveGovRenovationCheckInfo: function () {
                if (!vc.component.addGovRenovationCheckValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }

                vc.component.addGovRenovationCheckInfo.caId = vc.getCurrentCommunity().caId;
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovRenovationCheckInfo);
                    $('#addGovRenovationCheckModel').modal('hide');
                    return;
                }
                vc.http.apiPost(
                    '/govRenovationCheck/saveGovRenovationCheck',
                    JSON.stringify(vc.component.addGovRenovationCheckInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovRenovationCheckModel').modal('hide');
                            vc.component.clearAddGovRenovationCheckInfo();
                            vc.emit('govRenovationCheckManage', 'listGovRenovationCheck', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            _addGovAreaRenovations: function () {

                var param = {
                    params: {
                        page : 1,
                        row : 50,
                        caId : vc.getCurrentCommunity().caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govAreaRenovation/queryGovAreaRenovation',
                    param,
                    function (json, res) {
                        var _govAreaRenovationManageInfo = JSON.parse(json);
                        vc.component.addGovRenovationCheckInfo.govAreaRenovations = _govAreaRenovationManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            clearAddGovRenovationCheckInfo: function () {
                vc.component.addGovRenovationCheckInfo = {
                    govCheckId: '',
                    govRenovationId: '',
                    caId: '',
                    effectEvaluation: '',
                    mainActivities: '',
                    remediationEffect: '',
                    remediationSummary: '',
                    govAreaRenovations: []
                };
            }
        }
    });

})(window.vc);
