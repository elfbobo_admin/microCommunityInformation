(function(vc){
    vc.extends({
        propTypes: {
           emitChooseGovGridType:vc.propTypes.string,
           emitLoadData:vc.propTypes.string
        },
        data:{
            chooseGovGridTypeInfo:{
                govGridTypes:[],
                _currentGovGridTypeName:'',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('chooseGovGridType','openChooseGovGridTypeModel',function(_param){
                $('#chooseGovGridTypeModel').modal('show');
                vc.component._refreshChooseGovGridTypeInfo();
                vc.component._loadAllGovGridTypeInfo(1,10,'');
            });
        },
        methods:{
            _loadAllGovGridTypeInfo:function(_page,_row,_name){
                var param = {
                    params:{
                        page:_page,
                        row:_row,
                        communityId:vc.getCurrentCommunity().communityId,
                        name:_name
                    }
                };

                //发送get请求
               vc.http.apiGet('govGridType.listGovGridTypes',
                             param,
                             function(json){
                                var _govGridTypeInfo = JSON.parse(json);
                                vc.component.chooseGovGridTypeInfo.govGridTypes = _govGridTypeInfo.govGridTypes;
                             },function(){
                                console.log('请求失败处理');
                             }
                           );
            },
            chooseGovGridType:function(_govGridType){
                if(_govGridType.hasOwnProperty('name')){
                     _govGridType.govGridTypeName = _govGridType.name;
                }
                vc.emit($props.emitChooseGovGridType,'chooseGovGridType',_govGridType);
                vc.emit($props.emitLoadData,'listGovGridTypeData',{
                    govGridTypeId:_govGridType.govGridTypeId
                });
                $('#chooseGovGridTypeModel').modal('hide');
            },
            queryGovGridTypes:function(){
                vc.component._loadAllGovGridTypeInfo(1,10,vc.component.chooseGovGridTypeInfo._currentGovGridTypeName);
            },
            _refreshChooseGovGridTypeInfo:function(){
                vc.component.chooseGovGridTypeInfo._currentGovGridTypeName = "";
            }
        }

    });
})(window.vc);
