(function(vc){
    vc.extends({
        propTypes: {
           emitChooseGovVolunteerServiceRecord:vc.propTypes.string,
           emitLoadData:vc.propTypes.string
        },
        data:{
            chooseGovVolunteerServiceRecordInfo:{
                govVolunteerServiceRecords:[],
                _currentGovVolunteerServiceRecordName:'',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('chooseGovVolunteerServiceRecord','openChooseGovVolunteerServiceRecordModel',function(_param){
                $('#chooseGovVolunteerServiceRecordModel').modal('show');
                vc.component._refreshChooseGovVolunteerServiceRecordInfo();
                vc.component._loadAllGovVolunteerServiceRecordInfo(1,10,'');
            });
        },
        methods:{
            _loadAllGovVolunteerServiceRecordInfo:function(_page,_row,_name){
                var param = {
                    params:{
                        page:_page,
                        row:_row,
                        caId:vc.getCurrentCommunity().caId,
                        name:_name
                    }
                };

                //发送get请求
               vc.http.apiGet('/govVolunteerServiceRecord/queryGovVolunteerServiceRecord',
                             param,
                             function(json){
                                var _govVolunteerServiceRecordInfo = JSON.parse(json);
                                vc.component.chooseGovVolunteerServiceRecordInfo.govVolunteerServiceRecords = _govVolunteerServiceRecordInfo.govVolunteerServiceRecords;
                             },function(){
                                console.log('请求失败处理');
                             }
                           );
            },
            chooseGovVolunteerServiceRecord:function(_govVolunteerServiceRecord){
                if(_govVolunteerServiceRecord.hasOwnProperty('name')){
                     _govVolunteerServiceRecord.govVolunteerServiceRecordName = _govVolunteerServiceRecord.name;
                }
                vc.emit($props.emitChooseGovVolunteerServiceRecord,'chooseGovVolunteerServiceRecord',_govVolunteerServiceRecord);
                vc.emit($props.emitLoadData,'listGovVolunteerServiceRecordData',{
                    govVolunteerServiceRecordId:_govVolunteerServiceRecord.govVolunteerServiceRecordId
                });
                $('#chooseGovVolunteerServiceRecordModel').modal('hide');
            },
            queryGovVolunteerServiceRecords:function(){
                vc.component._loadAllGovVolunteerServiceRecordInfo(1,10,vc.component.chooseGovVolunteerServiceRecordInfo._currentGovVolunteerServiceRecordName);
            },
            _refreshChooseGovVolunteerServiceRecordInfo:function(){
                vc.component.chooseGovVolunteerServiceRecordInfo._currentGovVolunteerServiceRecordName = "";
            }
        }

    });
})(window.vc);
