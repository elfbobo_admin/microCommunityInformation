/**
    体检项目 组件
**/
(function(vc){

    vc.extends({
        propTypes: {
           callBackListener:vc.propTypes.string, //父组件名称
           callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            viewGovHealthInfo:{
                index:0,
                flowComponent:'viewGovHealthInfo',
                healthId:'',
caId:'',
name:'',
ramark:'',

            }
        },
        _initMethod:function(){
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovHealthInfoData();
        },
        _initEvent:function(){
            vc.on('viewGovHealthInfo','chooseGovHealth',function(_app){
                vc.copyObject(_app, vc.component.viewGovHealthInfo);
                vc.emit($props.callBackListener,$props.callBackFunction,vc.component.viewGovHealthInfo);
            });

            vc.on('viewGovHealthInfo', 'onIndex', function(_index){
                vc.component.viewGovHealthInfo.index = _index;
            });

        },
        methods:{

            _openSelectGovHealthInfoModel(){
                vc.emit('chooseGovHealth','openChooseGovHealthModel',{});
            },
            _openAddGovHealthInfoModel(){
                vc.emit('addGovHealth','openAddGovHealthModal',{});
            },
            _loadGovHealthInfoData:function(){

            }
        }
    });

})(window.vc);
