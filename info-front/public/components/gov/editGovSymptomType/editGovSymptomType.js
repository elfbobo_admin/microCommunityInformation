(function (vc, vm) {

    vc.extends({
        data: {
            editGovSymptomTypeInfo: {
                symptomId: '',
                caId: '',
                hospitalId: '',
                name: '',
                seq: '',
                remark: '',

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('editGovSymptomType', 'openEditGovSymptomTypeModal', function (_params) {
                vc.component.refreshEditGovSymptomTypeInfo();
                $('#editGovSymptomTypeModel').modal('show');
                vc.copyObject(_params, vc.component.editGovSymptomTypeInfo);
                vc.component.editGovSymptomTypeInfo.caId = vc.getCurrentCommunity().caId;
            });
        },
        methods: {
            editGovSymptomTypeValidate: function () {
                return vc.validate.validate({
                    editGovSymptomTypeInfo: vc.component.editGovSymptomTypeInfo
                }, {
                    
                    'editGovSymptomTypeInfo.name': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "症状名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "症状名称不能超过64"
                        },
                    ],
                    'editGovSymptomTypeInfo.seq': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "顺序不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "3",
                            errInfo: "顺序不能超过3"
                        },
                    ],
                    'editGovSymptomTypeInfo.remark': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "备注不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "备注不能超过1024"
                        },
                    ]

                });
            },
            editGovSymptomType: function () {
                if (!vc.component.editGovSymptomTypeValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }

                vc.http.apiPost(
                    '/govSymptomType/updateGovSymptomType',
                    JSON.stringify(vc.component.editGovSymptomTypeInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovSymptomTypeModel').modal('hide');
                            vc.emit('govSymptomTypeManage', 'listGovSymptomType', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            refreshEditGovSymptomTypeInfo: function () {
                vc.component.editGovSymptomTypeInfo = {
                    symptomId: '',
                    caId: '',
                    hospitalId: '',
                    name: '',
                    seq: '',
                    remark: '',

                }
            }
        }
    });

})(window.vc, window.vc.component);
