(function (vc, vm) {

    vc.extends({
        data: {
            deleteGovPartyMemberChangeInfo: {

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('deleteGovPartyMemberChange', 'openDeleteGovPartyMemberChangeModal', function (_params) {

                vc.component.deleteGovPartyMemberChangeInfo = _params;
                $('#deleteGovPartyMemberChangeModel').modal('show');

            });
        },
        methods: {
            deleteGovPartyMemberChange: function () {
                vc.http.apiPost(
                    '/govPartyMemberChange/deleteGovPartyMemberChange',
                    JSON.stringify(vc.component.deleteGovPartyMemberChangeInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovPartyMemberChangeModel').modal('hide');
                            vc.emit('govPartyMemberChangeManage', 'listGovPartyMemberChange', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');
                        vc.message(json);

                    });
            },
            closeDeleteGovPartyMemberChangeModel: function () {
                $('#deleteGovPartyMemberChangeModel').modal('hide');
            }
        }
    });

})(window.vc, window.vc.component);
