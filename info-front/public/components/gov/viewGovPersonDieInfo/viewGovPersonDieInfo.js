/**
    死亡登记 组件
**/
(function(vc){

    vc.extends({
        propTypes: {
           callBackListener:vc.propTypes.string, //父组件名称
           callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            viewGovPersonDieInfo:{
                index:0,
                flowComponent:'viewGovPersonDieInfo',
                dieId:'',
govPersonId:'',
caId:'',
govCommunityId:'',
personType:'',
dieType:'',
diePlace:'',
imgProve:'',
dieTime:'',
startTime:'',
endTime:'',
funeralPlace:'',
contactPerson:'',
contactTel:'',
ramark:'',
statusCd:'',

            }
        },
        _initMethod:function(){
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovPersonDieInfoData();
        },
        _initEvent:function(){
            vc.on('viewGovPersonDieInfo','chooseGovPersonDie',function(_app){
                vc.copyObject(_app, vc.component.viewGovPersonDieInfo);
                vc.emit($props.callBackListener,$props.callBackFunction,vc.component.viewGovPersonDieInfo);
            });

            vc.on('viewGovPersonDieInfo', 'onIndex', function(_index){
                vc.component.viewGovPersonDieInfo.index = _index;
            });

        },
        methods:{

            _openSelectGovPersonDieInfoModel(){
                vc.emit('chooseGovPersonDie','openChooseGovPersonDieModel',{});
            },
            _openAddGovPersonDieInfoModel(){
                vc.emit('addGovPersonDie','openAddGovPersonDieModal',{});
            },
            _loadGovPersonDieInfoData:function(){

            }
        }
    });

})(window.vc);
