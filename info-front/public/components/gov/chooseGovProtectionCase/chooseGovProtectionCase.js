(function(vc){
    vc.extends({
        propTypes: {
           emitChooseGovProtectionCase:vc.propTypes.string,
           emitLoadData:vc.propTypes.string
        },
        data:{
            chooseGovProtectionCaseInfo:{
                govProtectionCases:[],
                _currentGovProtectionCaseName:'',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('chooseGovProtectionCase','openChooseGovProtectionCaseModel',function(_param){
                $('#chooseGovProtectionCaseModel').modal('show');
                vc.component._refreshChooseGovProtectionCaseInfo();
                vc.component._loadAllGovProtectionCaseInfo(1,10,'');
            });
        },
        methods:{
            _loadAllGovProtectionCaseInfo:function(_page,_row,_name){
                var param = {
                    params:{
                        page:_page,
                        row:_row,
                        caId:vc.getCurrentCommunity().caId,
                        name:_name
                    }
                };

                //发送get请求
               vc.http.apiGet('/govProtectionCase/queryGovProtectionCase',
                             param,
                             function(json){
                                var _govProtectionCaseInfo = JSON.parse(json);
                                vc.component.chooseGovProtectionCaseInfo.govProtectionCases = _govProtectionCaseInfo.govProtectionCases;
                             },function(){
                                console.log('请求失败处理');
                             }
                           );
            },
            chooseGovProtectionCase:function(_govProtectionCase){
                if(_govProtectionCase.hasOwnProperty('name')){
                     _govProtectionCase.govProtectionCaseName = _govProtectionCase.name;
                }
                vc.emit($props.emitChooseGovProtectionCase,'chooseGovProtectionCase',_govProtectionCase);
                vc.emit($props.emitLoadData,'listGovProtectionCaseData',{
                    govProtectionCaseId:_govProtectionCase.govProtectionCaseId
                });
                $('#chooseGovProtectionCaseModel').modal('hide');
            },
            queryGovProtectionCases:function(){
                vc.component._loadAllGovProtectionCaseInfo(1,10,vc.component.chooseGovProtectionCaseInfo._currentGovProtectionCaseName);
            },
            _refreshChooseGovProtectionCaseInfo:function(){
                vc.component.chooseGovProtectionCaseInfo._currentGovProtectionCaseName = "";
            }
        }

    });
})(window.vc);
