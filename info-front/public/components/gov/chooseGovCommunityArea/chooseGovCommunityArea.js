(function (vc) {
    vc.extends({
        propTypes: {
            emitChooseGovCommunityArea: vc.propTypes.string,
            emitLoadData: vc.propTypes.string
        },
        data: {
            chooseGovCommunityAreaInfo: {
                govCommunityAreas: [],
                _currentGovCommunityAreaName: '',
            }
        },
        _initMethod: function () {
        },
        _initEvent: function () {
            vc.on('chooseGovCommunityArea', 'openChooseGovCommunityAreaModel', function (_param) {
                $('#chooseGovCommunityAreaModel').modal('show');
                vc.component._refreshChooseGovCommunityAreaInfo();
                vc.component._loadAllGovCommunityAreaInfo(1, 10, '');
            });
        },
        methods: {
            _loadAllGovCommunityAreaInfo: function (_page, _row, _name) {
                var param = {
                    params: {
                        page: _page,
                        row: _row,
                        communityId: vc.getCurrentCommunity().communityId,
                        name: _name
                    }
                };

                //发送get请求
                vc.http.apiGet('govCommunityArea.listGovCommunityAreas',
                    param,
                    function (json) {
                        var _govCommunityAreaInfo = JSON.parse(json);
                        vc.component.chooseGovCommunityAreaInfo.govCommunityAreas = _govCommunityAreaInfo.govCommunityAreas;
                    }, function () {
                        console.log('请求失败处理');
                    }
                );
            },
            chooseGovCommunityArea: function (_govCommunityArea) {
                if (_govCommunityArea.hasOwnProperty('name')) {
                    _govCommunityArea.govCommunityAreaName = _govCommunityArea.name;
                }
                vc.emit($props.emitChooseGovCommunityArea, 'chooseGovCommunityArea', _govCommunityArea);
                vc.emit($props.emitLoadData, 'listGovCommunityAreaData', {
                    govCommunityAreaId: _govCommunityArea.govCommunityAreaId
                });
                $('#chooseGovCommunityAreaModel').modal('hide');
            },
            queryGovCommunityAreas: function () {
                vc.component._loadAllGovCommunityAreaInfo(1, 10, vc.component.chooseGovCommunityAreaInfo._currentGovCommunityAreaName);
            },
            _refreshChooseGovCommunityAreaInfo: function () {
                vc.component.chooseGovCommunityAreaInfo._currentGovCommunityAreaName = "";
            }
        }

    });
})(window.vc);
