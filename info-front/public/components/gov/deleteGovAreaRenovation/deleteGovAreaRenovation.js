(function (vc, vm) {

    vc.extends({
        data: {
            deleteGovAreaRenovationInfo: {

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('deleteGovAreaRenovation', 'openDeleteGovAreaRenovationModal', function (_params) {

                vc.component.deleteGovAreaRenovationInfo = _params;
                $('#deleteGovAreaRenovationModel').modal('show');

            });
        },
        methods: {
            deleteGovAreaRenovation: function () {
                vc.component.deleteGovAreaRenovationInfo.caId = vc.getCurrentCommunity().caId;
                vc.http.apiPost(
                    '/govAreaRenovation/deleteGovAreaRenovation',
                    JSON.stringify(vc.component.deleteGovAreaRenovationInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovAreaRenovationModel').modal('hide');
                            vc.emit('govAreaRenovationManage', 'listGovAreaRenovation', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');
                        vc.message(json);

                    });
            },
            closeDeleteGovAreaRenovationModel: function () {
                $('#deleteGovAreaRenovationModel').modal('hide');
            }
        }
    });

})(window.vc, window.vc.component);
