(function (vc) {
    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addAdvertInfo: {
                caId: '',
                advertId: '',
                adName: '',
                adTypeCd: '10000',
                seq: '',
                startTime: '',
                state: '2000',
                endTime: '',
                floorId: '',
                advertType: '',
                pageUrl: '',
                photos: [],
                viewType: '',
                vedioName: ''
            }
        },
        _initMethod: function () {
        },
        _initEvent: function () {
            vc.on('addAdvert', 'openAddAdvertModal', function () {
                vc.component._initAddAdvertDateInfo();
                $('#addAdvertModel').modal('show');
            });
            vc.on("addAdvert", "notifyUploadImage", function (_param) {
                console.log("_param",_param);
                vc.component.addAdvertInfo.photos = _param;
            });
            vc.on("addAdvert", "notifyUploadVedio", function (_param) {
                vc.component.addAdvertInfo.vedioName = _param.realFileName;
            });
        },
        methods: {
            _initAddAdvertDateInfo: function () {
                vc.component.addAdvertInfo.startTime = vc.dateTimeFormat(new Date().getTime());
                $('.addAdvertStartTime').datetimepicker({
                    language: 'zh-CN',
                    fontAwesome: 'fa',
                    format: 'yyyy-mm-dd hh:ii:ss',
                    initTime: true,
                    initialDate: new Date(),
                    autoClose: 1,
                    todayBtn: true
                });
                $('.addAdvertStartTime').datetimepicker()
                    .on('changeDate', function (ev) {
                        var value = $(".addAdvertStartTime").val();
                        vc.component.addAdvertInfo.startTime = value;
                    });
                $('.addAdvertEndTime').datetimepicker({
                    language: 'zh-CN',
                    fontAwesome: 'fa',
                    format: 'yyyy-mm-dd hh:ii:ss',
                    initTime: true,
                    initialDate: new Date(),
                    autoClose: 1,
                    todayBtn: true
                });
                $('.addAdvertEndTime').datetimepicker()
                    .on('changeDate', function (ev) {
                        var value = $(".addAdvertEndTime").val();
                        vc.component.addAdvertInfo.endTime = value;
                    });
            },
            addAdvertValidate: function () {
                return vc.validate.validate({
                    addAdvertInfo: vc.component.addAdvertInfo
                }, {
                    'addAdvertInfo.adName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "广告名称不能为空"
                        },
                        {
                            limit: "maxin",
                            param: "1,200",
                            errInfo: "广告名称不能超过200位"
                        },
                    ],
                    'addAdvertInfo.adTypeCd': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "广告类型不能为空"
                        },
                        {
                            limit: "num",
                            param: "",
                            errInfo: "广告类型不能为空"
                        },
                    ],
                    'addAdvertInfo.advertType': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "发布类型不能为空"
                        }
                    ],
                    'addAdvertInfo.seq': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "播放顺序不能为空"
                        },
                        {
                            limit: "num",
                            param: "",
                            errInfo: "播放顺序不是有效的数字"
                        },
                    ],
                    'addAdvertInfo.startTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "投放时间不能为空"
                        },
                        {
                            limit: "dateTime",
                            param: "",
                            errInfo: "不是有效的时间格式"
                        },
                    ],
                    'addAdvertInfo.endTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "结束时间不能为空"
                        },
                        {
                            limit: "dateTime",
                            param: "",
                            errInfo: "不是有效的时间格式"
                        },
                    ],
                });
            },
            saveAdvertInfo: function () {
                if (!vc.component.addAdvertValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }
                if (vc.component.addAdvertInfo.viewType == '8888') {
                    vc.component.addAdvertInfo.vedioName = '';
                } else {
                    vc.component.addAdvertInfo.photos = [];
                }
                if (vc.component.addAdvertInfo.viewType == '8888' && vc.component.addAdvertInfo.photos.length < 1) {
                    vc.toast('请上传图片');
                    return;
                } else if (vc.component.addAdvertInfo.viewType == '9999' && vc.component.addAdvertInfo.vedioName == '') {
                    vc.toast('请上传视频');
                    return;
                }
                vc.component.addAdvertInfo.caId = vc.getCurrentCommunity().caId;
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addAdvertInfo);
                    $('#addAdvertModel').modal('hide');
                    return;
                }
                vc.http.apiPost('/govAdvert/saveGovAdvert',
                    JSON.stringify(vc.component.addAdvertInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        if (res.status == 200) {
                            //关闭model
                            $('#addAdvertModel').modal('hide');
                            vc.component.clearAddAdvertInfo();
                            vc.emit('advertManage', 'listAdvert', {});
                            return;
                        }
                        vc.toast(json);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');
                        vc.toast(errInfo);
                    });

                    
            },
            clearAddAdvertInfo: function () {
                vc.emit('addAdvert', 'uploadImage', 'clearImage', {});
                vc.emit('addAdvert', 'uploadVedio', 'clearVedio', {});
                vc.component._initAddAdvertDateInfo();
                vc.component.addAdvertInfo = {
                    caId: '',
                    advertId: '',
                    adName: '',
                    adTypeCd: '10000',
                    state: '2000',
                    seq: '',
                    startTime: '',
                    endTime: '',
                    advertType: '',
                    pageUrl: '',
                    photos: [],
                    viewType: '',
                    vedioName: ''
                };
            }
        }
    });
})(window.vc);
