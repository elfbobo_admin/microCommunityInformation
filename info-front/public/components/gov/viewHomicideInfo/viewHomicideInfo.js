(function (vc) {
    vc.extends({
        data: {
            viewHomicideInfo: {
                orgId: '',
                govPersonId: '',
                govHomicideBasics: []
            }
        },
        _initMethod: function () {
        },
        _initEvent: function () {
            vc.on('homicideInfo', 'switch', function (_param) {
                vc.component._refreshviewHomicideInfo();
                console.log(_param);
                vc.component._loadHomicideInfo(_param.govPersonId);
            });
        },
        methods: {
            _loadHomicideInfo: function (_personId) {
                if (!_personId) {
                    return;
                }

                var str = [];
               var params = {
                    page: 1,
                    row: 10,
                    caId: vc.getCurrentCommunity().caId                }
                str = _personId.split("&");
                if(str[1] == '6006'){
                    params['victimId'] = str[0];
                }else if(str[1] == '6007'){
                    params['suspicionId'] = str[0];
                }
                var param = {
                     params
                    };
                //发送get请求
                vc.http.apiGet('/govHomicideBasic/queryGovHomicideBasic',
                    param,
                    function (json) {
                        var _labelInfo = JSON.parse(json);
                        $that.viewHomicideInfo.govHomicideBasics = _labelInfo.data;
                    }, function () {
                        console.log('请求失败处理');
                    }
                );
            },
            _refreshviewHomicideInfo: function () {
                $that.viewHomicideInfo = {
                    orgId: '',
                    govPersonId: '',
                    govHomicideBasics: []
                }
            }
        }

    });
})(window.vc);
