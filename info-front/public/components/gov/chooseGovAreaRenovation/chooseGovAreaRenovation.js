(function (vc) {
    vc.extends({
        propTypes: {
            emitChooseGovAreaRenovation: vc.propTypes.string,
            emitLoadData: vc.propTypes.string
        },
        data: {
            chooseGovAreaRenovationInfo: {
                govAreaRenovations: [],
                _currentGovAreaRenovationName: '',
            }
        },
        _initMethod: function () {
        },
        _initEvent: function () {
            vc.on('chooseGovAreaRenovation', 'openChooseGovAreaRenovationModel', function (_param) {
                $('#chooseGovAreaRenovationModel').modal('show');
                vc.component._refreshChooseGovAreaRenovationInfo();
                vc.component._loadAllGovAreaRenovationInfo(1, 10, '');
            });
        },
        methods: {
            _loadAllGovAreaRenovationInfo: function (_page, _row, _name) {
                var param = {
                    params: {
                        page: _page,
                        row: _row,
                        caId: vc.getCurrentCommunity().caId,
                        name: _name
                    }
                };

                //发送get请求
                vc.http.apiGet('/govAreaRenovation/queryGovAreaRenovation',
                    param,
                    function (json) {
                        var _govAreaRenovationInfo = JSON.parse(json);
                        vc.component.chooseGovAreaRenovationInfo.govAreaRenovations = _govAreaRenovationInfo.govAreaRenovations;
                    }, function () {
                        console.log('请求失败处理');
                    }
                );
            },
            chooseGovAreaRenovation: function (_govAreaRenovation) {
                if (_govAreaRenovation.hasOwnProperty('name')) {
                    _govAreaRenovation.govAreaRenovationName = _govAreaRenovation.name;
                }
                vc.emit($props.emitChooseGovAreaRenovation, 'chooseGovAreaRenovation', _govAreaRenovation);
                vc.emit($props.emitLoadData, 'listGovAreaRenovationData', {
                    govAreaRenovationId: _govAreaRenovation.govAreaRenovationId
                });
                $('#chooseGovAreaRenovationModel').modal('hide');
            },
            queryGovAreaRenovations: function () {
                vc.component._loadAllGovAreaRenovationInfo(1, 10, vc.component.chooseGovAreaRenovationInfo._currentGovAreaRenovationName);
            },
            _refreshChooseGovAreaRenovationInfo: function () {
                vc.component.chooseGovAreaRenovationInfo._currentGovAreaRenovationName = "";
            }
        }

    });
})(window.vc);
