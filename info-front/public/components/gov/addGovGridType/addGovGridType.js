(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovGridTypeInfo: {
                govTypeId: '',
                caId: '',
                typeName: '',
                ramark: ''
            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('addGovGridType', 'openAddGovGridTypeModal', function () {
                $('#addGovGridTypeModel').modal('show');
            });
        },
        methods: {
            addGovGridTypeValidate() {
                return vc.validate.validate({
                    addGovGridTypeInfo: vc.component.addGovGridTypeInfo
                }, {
                    'addGovGridTypeInfo.typeName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "责任名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "责任名称不能超过64"
                        },
                    ]




                });
            },
            saveGovGridTypeInfo: function () {
                if (!vc.component.addGovGridTypeValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }

                vc.component.addGovGridTypeInfo.caId = vc.getCurrentCommunity().caId;
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovGridTypeInfo);
                    $('#addGovGridTypeModel').modal('hide');
                    return;
                }

                vc.http.apiPost(
                    '/govGridType/saveGovGridType',
                    JSON.stringify(vc.component.addGovGridTypeInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovGridTypeModel').modal('hide');
                            vc.component.clearAddGovGridTypeInfo();
                            vc.emit('govGridTypeManage', 'listGovGridType', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            clearAddGovGridTypeInfo: function () {
                vc.component.addGovGridTypeInfo = {
                    govTypeId: '',
                    caId: '',
                    typeName: '',
                    ramark: '',
                    statusCd: '',
                    datasourceType: '',

                };
            }
        }
    });

})(window.vc);
