(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovGuideTitleInfo: {
                titleId: '0000',
                titleType: '',
                title: '',
                seq: '',
                healthId: '',
                termId: '',
                govHealthTerms: [],
                titleValues: []
            }
        },
        _initMethod: function () {
            $that._listAddGovHealthTerms();
        },
        _initEvent: function () {
            vc.on('addGovGuideTitle', 'openAddGovGuideTitleModal', function (_param) {
                vc.copyObject(_param, $that.addGovGuideTitleInfo);
                $('#addGovGuideTitleModel').modal('show');
                console.log($that.addGovGuideTitleInfo);
            });
        },
        methods: {
            addGovGuideTitleValidate() {
                return vc.validate.validate({
                    addGovGuideTitleInfo: vc.component.addGovGuideTitleInfo
                }, {
                    'addGovGuideTitleInfo.titleType': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "题目类型不能为空"
                        },
                        {
                            limit: "num",
                            param: "",
                            errInfo: "题目类型格式错误"
                        },
                    ],
                    'addGovGuideTitleInfo.termId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "体检项不能为空"
                        },
                        {
                            limit: "num",
                            param: "",
                            errInfo: "体检项格式错误"
                        },
                    ],
                    'addGovGuideTitleInfo.title': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "问卷题目不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "256",
                            errInfo: "问卷题目太长"
                        },
                    ],
                    'addGovGuideTitleInfo.seq': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "顺序不能为空"
                        },
                        {
                            limit: "num",
                            param: "",
                            errInfo: "顺序必须是数字"
                        },
                    ]
                });
            },
            saveGovGuideTitleInfo: function () {
                if (!vc.component.addGovGuideTitleValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }
                vc.component.addGovGuideTitleInfo.caId = vc.getCurrentCommunity().caId;
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovGuideTitleInfo);
                    $('#addGovGuideTitleModel').modal('hide');
                    return;
                }
                vc.http.apiPost(
                    '/govHealthTitle/saveGovHealthTitle',
                    JSON.stringify(vc.component.addGovGuideTitleInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovGuideTitleModel').modal('hide');
                            vc.component.clearAddGovGuideTitleInfo();
                            vc.emit('govGuideTitleManage', 'listGovGuideTitle', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            _listAddGovHealthTerms: function () {
                var param = {
                    params: {
                        page: 1 ,
                        row: 50 ,
                        caId: vc.getCurrentCommunity().caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govHealthTerm/queryGovHealthTerm',
                    param,
                    function (json, res) {
                        var _govHealthTermManageInfo = JSON.parse(json);
                        vc.component.addGovGuideTitleInfo.govHealthTerms = _govHealthTermManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            clearAddGovGuideTitleInfo: function () {
                vc.component.addGovGuideTitleInfo = {
                    titleId: '',
                    titleType: '',
                    title: '',
                    seq: '',
                    termId: '',
                    healthId: '',
                    govHealthTerms: [],
                    titleValues: []
                };
            },
            _changeAddTitleType: function () {

                let _titleType = $that.addGovGuideTitleInfo.titleType;

                if (_titleType == '3003') {
                    $that.addGovGuideTitleInfo.titleValues = [];
                    return;
                }

                $that.addGovGuideTitleInfo.titleValues = [
                    {
                        value: '',
                        seq: 1
                    }
                ];
            },
            _addTitleValue: function () {
                $that.addGovGuideTitleInfo.titleValues.push(
                    {
                        value: '',
                        seq: $that.addGovGuideTitleInfo.titleValues.length + 1
                    }
                );
            },
            _deleteTitleValue: function (_seq) {
                console.log(_seq);

                let _newTitleValues = [];
                let _tmpTitleValues = $that.addGovGuideTitleInfo.titleValues;
                _tmpTitleValues.forEach(item => {
                    if (item.seq != _seq) {
                        _newTitleValues.push({
                            value: item.value,
                            seq: _newTitleValues.length + 1
                        })
                    }
                });

                $that.addGovGuideTitleInfo.titleValues = _newTitleValues;
            }
        }
    });

})(window.vc);
