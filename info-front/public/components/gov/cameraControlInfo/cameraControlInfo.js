/**
 入驻小区
 **/
(function (vc) {
    vc.extends({
        data: {
            cameraControlInfo: {
                machineCode: '',
                machineVersion: '',
                machineName: '',
                machineTypeCd: '',
                machineTypeName: '',
                machineIp: '',
                machineMac: '',
                machineId: ''
            }
        },
        _initMethod: function () {
        },
        _initEvent: function () {
            vc.on('cameraControlInfo', 'notify', function (_data) {
                $that.refreshCameraControlInfo();
                vc.copyObject(_data._machine, $that.cameraControlInfo);
            });
            vc.on('chooseMachine', 'chooseMachine', function (_data) {
                $that._saveMachineStaff(_data);
            });
        },
        methods: {
            refreshCameraControlInfo: function () {
                vc.component.cameraControlInfo = {
                    machineCode: '',
                    machineVersion: '',
                    machineName: '',
                    machineTypeCd: '',
                    machineTypeName: '',
                    machineIp: '',
                    machineMac: '',
                    machineId: ''
                }
            },
            _clearMachine() {
                let params = {

                }
                vc.http.apiPost(
                    '/machine/clearMachine',
                    JSON.stringify(params),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            vc.message(_json.msg);
                            vc.emit('cameraControlInfo', 'page_event', _data);
                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            _saveMachineStaff(_data) {
                let params = {
                    machineId: _data.machineId,
                    caId: _data.caId,
                    isShow: 'Y'
                }
                vc.http.apiPost(
                    '/machineStaffShow/saveMachineStaffShow',
                    JSON.stringify(params),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            vc.emit('cameraControlInfo', 'page_event', {});
                            vc.message(_json.msg);
                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            _opendMachine() {
                vc.emit('chooseMachine', 'openChooseMachineModel', {});

            }
        }
    });
})(window.vc);