(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovDoctorHealthyInfo: {
                govDoctorId: '',
                caId: '',
                doctorNum: '',
                idType: '1',
                idCard: '',
                govPersonId: '',
                personName: '',
                personTel: '',
                personSex: '',
                birthday: '',
                jobName: '',
                titleName: '',
                businessExpertise: '',
                groupId: '',
                name: '',
                labelCd: '',
                titleNames: [],
                jobNames: [],
                govMedicalGroups: []
            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('addGovDoctorHealthy', 'openAddGovDoctorHealthyModal', function () {
                $that._listAddGovMedicalGroups();
                vc.initDate('addBirthday', function (_value) {
                    $that.addGovDoctorHealthyInfo.birthday = _value;
                });
                vc.getDict('gov_doctor_healthy', "title_name", function (_data) {
                    vc.component.addGovDoctorHealthyInfo.titleNames = _data;
                });
                vc.getDict('gov_doctor_healthy', "job_name", function (_data) {
                    vc.component.addGovDoctorHealthyInfo.jobNames = _data;
                });
                $('#addGovDoctorHealthyModel').modal('show');
            });
        },
        methods: {
            addGovDoctorHealthyValidate() {
                return vc.validate.validate({
                    addGovDoctorHealthyInfo: vc.component.addGovDoctorHealthyInfo
                }, {
                    'addGovDoctorHealthyInfo.doctorNum': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "工号不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "工号不能超过12"
                        },
                    ],
                    'addGovDoctorHealthyInfo.idType': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "证件类型不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "证件类型不能超过12"
                        },
                    ],
                    'addGovDoctorHealthyInfo.idCard': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "证件号码不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "证件号码不能超过64"
                        },
                    ],
                    'addGovDoctorHealthyInfo.personName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "姓名不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "姓名不能超过64"
                        },
                    ],
                    'addGovDoctorHealthyInfo.personTel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "电话不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "电话不能超过11"
                        },
                    ],
                    'addGovDoctorHealthyInfo.personSex': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "性别不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "性别不能超过11"
                        },
                    ],
                    'addGovDoctorHealthyInfo.birthday': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "生日不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "生日不能超过时间类型"
                        },
                    ],
                    'addGovDoctorHealthyInfo.jobName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "职务不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "职务不能超过12"
                        },
                    ],
                    'addGovDoctorHealthyInfo.titleName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "职称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "职称不能超过12"
                        },
                    ],
                    'addGovDoctorHealthyInfo.businessExpertise': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "业务专长不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "业务专长不能超过1024"
                        },
                    ],
                    'addGovDoctorHealthyInfo.groupId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "所属团队不能为空"
                        }
                    ],
                    'addGovDoctorHealthyInfo.labelCd': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "医生角色不能为空"
                        }
                    ],




                });
            },
            saveGovDoctorHealthyInfo: function () {
                if (!vc.component.addGovDoctorHealthyValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }
                if(!vc.component.isCardNoAdd(vc.component.addGovDoctorHealthyInfo.idCard)){
                    vc.toast("身份证格式有误，请检查身份证信息");

                    return;
                }
                vc.component.addGovDoctorHealthyInfo.caId = vc.getCurrentCommunity().caId;
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovDoctorHealthyInfo);
                    $('#addGovDoctorHealthyModel').modal('hide');
                    return;
                }
                vc.http.apiPost(
                    '/govDoctorHealthy/saveGovDoctorHealthy',
                    JSON.stringify(vc.component.addGovDoctorHealthyInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovDoctorHealthyModel').modal('hide');
                            vc.component.clearAddGovDoctorHealthyInfo();
                            vc.emit('govDoctorHealthyManage', 'listGovDoctorHealthy', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            isCardNoAdd: function (card) {
                //身份证号码为15位或者18位，15位时全为数字，18位前17位为数字，最后一位是校验位，可能为数字或字符X
                var reg = /(^\d{15}$)|(^\d{17}(\d|X)$)/;
                if (reg.test(card) === false) {
                    return false;
                }
                return true;
            },
            _listAddGovMedicalGroups: function () {

                var param = {
                    params: {
                        page : 1,
                        row: 50,
                        caId : vc.getCurrentCommunity().caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govMedicalGroup/queryGovMedicalGroup',
                    param,
                    function (json, res) {
                        var _govMedicalGroupManageInfo = JSON.parse(json);
                        vc.component.addGovDoctorHealthyInfo.govMedicalGroups = _govMedicalGroupManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _setAddGroupName: function(_groupId){
                $that.addGovDoctorHealthyInfo.govMedicalGroups.forEach(element => {
                    if(element.groupId = _groupId){
                        $that.addGovDoctorHealthyInfo.name = element.name;
                    }
                });
            },
            clearAddGovDoctorHealthyInfo: function () {
                vc.component.addGovDoctorHealthyInfo = {
                    govDoctorId: '',
                    caId: '',
                    doctorNum: '',
                    idType: '1',
                    idCard: '',
                    govPersonId: '',
                    personName: '',
                    personTel: '',
                    personSex: '',
                    birthday: '',
                    jobName: '',
                    titleName: '',
                    businessExpertise: '',
                    statusCd: '',
                    datasourceType: '',
                    groupId: '',
                    name: '',
                    labelCd: '',
                    titleNames: [],
                    jobNames: [],
                    govMedicalGroups: []

                };
            }
        }
    });

})(window.vc);
