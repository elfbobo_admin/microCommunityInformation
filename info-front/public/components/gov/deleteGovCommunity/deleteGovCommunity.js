(function (vc, vm) {

    vc.extends({
        data: {
            deleteGovCommunityInfo: {

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('deleteGovCommunity', 'openDeleteGovCommunityModal', function (_params) {

                vc.component.deleteGovCommunityInfo = _params;
                $('#deleteGovCommunityModel').modal('show');

            });
        },
        methods: {
            deleteGovCommunity: function () {
                //vc.component.deleteGovCommunityInfo.communityId = vc.getCurrentCommunity().communityId;
                vc.http.apiPost(
                    '/govCommunity/deleteGovCommunity',
                    JSON.stringify(vc.component.deleteGovCommunityInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovCommunityModel').modal('hide');
                            vc.emit('govCommunityManage', 'listGovCommunity', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');
                        vc.message(json);

                    });
            },
            closeDeleteGovCommunityModel: function () {
                $('#deleteGovCommunityModel').modal('hide');
            }
        }
    });

})(window.vc, window.vc.component);
