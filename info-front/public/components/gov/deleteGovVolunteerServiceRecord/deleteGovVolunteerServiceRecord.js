(function(vc,vm){

    vc.extends({
        data:{
            deleteGovVolunteerServiceRecordInfo:{

            }
        },
         _initMethod:function(){

         },
         _initEvent:function(){
             vc.on('deleteGovVolunteerServiceRecord','openDeleteGovVolunteerServiceRecordModal',function(_params){

                vc.component.deleteGovVolunteerServiceRecordInfo = _params;
                $('#deleteGovVolunteerServiceRecordModel').modal('show');

            });
        },
        methods:{
            deleteGovVolunteerServiceRecord:function(){
                vc.component.deleteGovVolunteerServiceRecordInfo.caId=vc.getCurrentCommunity().caId;
                vc.http.apiPost(
                    '/govVolunteerServiceRecord/deleteGovVolunteerServiceRecord',
                    JSON.stringify(vc.component.deleteGovVolunteerServiceRecordInfo),
                    {
                        emulateJSON:true
                     },
                     function(json,res){
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovVolunteerServiceRecordModel').modal('hide');
                            vc.emit('govVolunteerServiceRecordManage','listGovVolunteerServiceRecord',{});
                            return ;
                        }
                        vc.message(_json.msg);
                     },
                     function(errInfo,error){
                        console.log('请求失败处理');
                        vc.message(json);

                     });
            },
            closeDeleteGovVolunteerServiceRecordModel:function(){
                $('#deleteGovVolunteerServiceRecordModel').modal('hide');
            }
        }
    });

})(window.vc,window.vc.component);
