(function (vc, vm) {

    vc.extends({
        data: {
            editGovHomicideBasicInfo: {
                govHomicideId: '',
                caId: '',
                homicideNum: '',
                homicideName: '',
                happenTime: '',
                victimId: '',
                victimName: '',
                suspicionId: '',
                suspicionName: '',
                endTime: '',
                leadRamark: '',
                ramark: '',

            }
        },
        _initMethod: function () {

            vc.initDate('editHappenTime', function (_value) {
                $that.editGovHomicideBasicInfo.happenTime = _value;
            });
            vc.initDate('editEndTime', function (_value) {
                $that.editGovHomicideBasicInfo.endTime = _value;
            });
        },
        _initEvent: function () {
            vc.on('editGovHomicideBasic', 'openEditGovHomicideBasicModal', function (_params) {
                vc.component.refreshEditGovHomicideBasicInfo();
                $('#editGovHomicideBasicModel').modal('show');
                vc.copyObject(_params, vc.component.editGovHomicideBasicInfo);
                vc.component.editGovHomicideBasicInfo.caId = vc.getCurrentCommunity().caId;
            });
            vc.on('victimName','chooseGovPerson', function (_param) {
                $that.editGovHomicideBasicInfo.victimId = _param.govPersonId;
                $that.editGovHomicideBasicInfo.victimName = _param.personName;
            });
            vc.on('suspicionName','chooseGovPersonSuspicion', function (_param) {
                $that.editGovHomicideBasicInfo.suspicionId = _param.govPersonId;
                $that.editGovHomicideBasicInfo.suspicionName = _param.personName;
            });
        },
        methods: {
            editGovHomicideBasicValidate: function () {
                return vc.validate.validate({
                    editGovHomicideBasicInfo: vc.component.editGovHomicideBasicInfo
                }, {
                    'editGovHomicideBasicInfo.govHomicideId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "命案ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "命案ID不能超过30"
                        },
                    ],
                    'editGovHomicideBasicInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "区域ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "区域ID不能超过30"
                        },
                    ],
                    'editGovHomicideBasicInfo.homicideNum': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "命案编号不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "命案编号不能超过30"
                        },
                    ],
                    'editGovHomicideBasicInfo.homicideName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "命案名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "命案名称不能超过64"
                        },
                    ],
                    'editGovHomicideBasicInfo.happenTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "发生时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "发生时间不能超过时间类型"
                        },
                    ],
                    'editGovHomicideBasicInfo.victimId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "受害人ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "受害人ID不能超过30"
                        },
                    ],
                    'editGovHomicideBasicInfo.victimName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "受害人名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "102",
                            errInfo: "受害人名称不能超过102"
                        },
                    ],
                    'editGovHomicideBasicInfo.suspicionId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "嫌疑人ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "102",
                            errInfo: "嫌疑人ID不能超过102"
                        },
                    ],
                    'editGovHomicideBasicInfo.suspicionName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "嫌疑人名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "嫌疑人名称不能超过64"
                        },
                    ],
                    'editGovHomicideBasicInfo.endTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "结案日期不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "结案日期不能超过时间类型"
                        },
                    ],
                    'editGovHomicideBasicInfo.leadRamark': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "概要情况不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "概要情况不能超过1024"
                        },
                    ]

                });
            },
            editGovHomicideBasic: function () {
                if (!vc.component.editGovHomicideBasicValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }

                vc.http.apiPost(
                    '/govHomicideBasic/updateGovHomicideBasic',
                    JSON.stringify(vc.component.editGovHomicideBasicInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovHomicideBasicModel').modal('hide');
                            vc.emit('govHomicideBasicManage', 'listGovHomicideBasic', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            _closeEditGovHomicideBasic: function () {
                $that.refreshEditGovHomicideBasicInfo();
                vc.emit('govHomicideBasicManage', 'listGovHomicideBasic', {});
            },
            _openEditChooseGovPersonModel: function () {
                let _caId = vc.getCurrentCommunity().caId;

                vc.emit('chooseGovPerson', 'openChooseGovPersonModel', _caId);
            },
            _openEditSuspicionChooseGovPersonModel: function () {
                let _caId = vc.getCurrentCommunity().caId;
  
                vc.emit('chooseGovPersonSuspicionName', 'openchooseGovPersonSuspicionModel', _caId);
            },
            refreshEditGovHomicideBasicInfo: function () {
                vc.component.editGovHomicideBasicInfo = {
                    govHomicideId: '',
                    caId: '',
                    homicideNum: '',
                    homicideName: '',
                    happenTime: '',
                    victimId: '',
                    victimName: '',
                    suspicionId: '',
                    suspicionName: '',
                    endTime: '',
                    leadRamark: '',
                    ramark: '',

                }
            }
        }
    });

})(window.vc, window.vc.component);
