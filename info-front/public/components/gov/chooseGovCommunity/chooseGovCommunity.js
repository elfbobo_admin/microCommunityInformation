(function (vc) {
    vc.extends({
        propTypes: {
            emitChooseGovCommunity: vc.propTypes.string,
            emitLoadData: vc.propTypes.string
        },
        data: {
            chooseGovCommunityInfo: {
                govCommunitys: [],
                _currentGovCommunityName: '',
            }
        },
        _initMethod: function () {
        },
        _initEvent: function () {
            vc.on('chooseGovCommunity', 'openChooseGovCommunityModel', function (_param) {
                $('#chooseGovCommunityModel').modal('show');
                vc.component._refreshChooseGovCommunityInfo();
                vc.component._loadAllGovCommunityInfo(1, 10, '');
            });
        },
        methods: {
            _loadAllGovCommunityInfo: function (_page, _row, _name) {
                var param = {
                    params: {
                        page: _page,
                        row: _row,
                        communityId: vc.getCurrentCommunity().communityId,
                        name: _name
                    }
                };

                //发送get请求
                vc.http.apiGet('govCommunity.listGovCommunitys',
                    param,
                    function (json) {
                        var _govCommunityInfo = JSON.parse(json);
                        vc.component.chooseGovCommunityInfo.govCommunitys = _govCommunityInfo.govCommunitys;
                    }, function () {
                        console.log('请求失败处理');
                    }
                );
            },
            chooseGovCommunity: function (_govCommunity) {
                if (_govCommunity.hasOwnProperty('name')) {
                    _govCommunity.govCommunityName = _govCommunity.name;
                }
                vc.emit($props.emitChooseGovCommunity, 'chooseGovCommunity', _govCommunity);
                vc.emit($props.emitLoadData, 'listGovCommunityData', {
                    govCommunityId: _govCommunity.govCommunityId
                });
                $('#chooseGovCommunityModel').modal('hide');
            },
            queryGovCommunitys: function () {
                vc.component._loadAllGovCommunityInfo(1, 10, vc.component.chooseGovCommunityInfo._currentGovCommunityName);
            },
            _refreshChooseGovCommunityInfo: function () {
                vc.component.chooseGovCommunityInfo._currentGovCommunityName = "";
            }
        }

    });
})(window.vc);
