(function(vc,vm){

    vc.extends({
        data:{
            editGovActivityTypeInfo:{
                typeId:'',
caId:'',
typeName:'',
remark:'',

            }
        },
         _initMethod:function(){

         },
         _initEvent:function(){
             vc.on('editGovActivityType','openEditGovActivityTypeModal',function(_params){
                vc.component.refreshEditGovActivityTypeInfo();
                $('#editGovActivityTypeModel').modal('show');
                vc.copyObject(_params, vc.component.editGovActivityTypeInfo );
                vc.component.editGovActivityTypeInfo.caId = vc.getCurrentCommunity().caId;
            });
        },
        methods:{
            editGovActivityTypeValidate:function(){
                        return vc.validate.validate({
                            editGovActivityTypeInfo:vc.component.editGovActivityTypeInfo
                        },{
                            'editGovActivityTypeInfo.caId':[
{
                            limit:"required",
                            param:"",
                            errInfo:"所属区域不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"30",
                            errInfo:"所属区域超长"
                        },
                    ],
'editGovActivityTypeInfo.typeName':[
{
                            limit:"required",
                            param:"",
                            errInfo:"类型名称不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"200",
                            errInfo:"类型名称超长"
                        },
                    ],
'editGovActivityTypeInfo.remark':[
{
                            limit:"required",
                            param:"",
                            errInfo:"备注不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"500",
                            errInfo:"备注太长"
                        },
                    ],
'editGovActivityTypeInfo.typeId':[
{
                            limit:"required",
                            param:"",
                            errInfo:"活动类型ID不能为空"
                        }]

                        });
             },
            editGovActivityType:function(){
                if(!vc.component.editGovActivityTypeValidate()){
                    vc.toast(vc.validate.errInfo);
                    return ;
                }

                vc.http.apiPost(
                    '/govActivityType/updateGovActivityType',
                    JSON.stringify(vc.component.editGovActivityTypeInfo),
                    {
                        emulateJSON:true
                     },
                     function(json,res){
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovActivityTypeModel').modal('hide');
                             vc.emit('govActivityTypeManage','listGovActivityType',{});
                            return ;
                        }
                        vc.message(_json.msg);
                     },
                     function(errInfo,error){
                        console.log('请求失败处理');

                        vc.message(errInfo);
                     });
            },
            refreshEditGovActivityTypeInfo:function(){
                vc.component.editGovActivityTypeInfo= {
                  typeId:'',
caId:'',
typeName:'',
remark:'',

                }
            }
        }
    });

})(window.vc,window.vc.component);
