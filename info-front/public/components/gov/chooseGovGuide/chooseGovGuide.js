(function(vc){
    vc.extends({
        propTypes: {
           emitChooseGovGuide:vc.propTypes.string,
           emitLoadData:vc.propTypes.string
        },
        data:{
            chooseGovGuideInfo:{
                govGuides:[],
                _currentGovGuideName:'',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('chooseGovGuide','openChooseGovGuideModel',function(_param){
                $('#chooseGovGuideModel').modal('show');
                vc.component._refreshChooseGovGuideInfo();
                vc.component._loadAllGovGuideInfo(1,10,'');
            });
        },
        methods:{
            _loadAllGovGuideInfo:function(_page,_row,_name){
                var param = {
                    params:{
                        page:_page,
                        row:_row,
                        communityId:vc.getCurrentCommunity().communityId,
                        name:_name
                    }
                };

                //发送get请求
               vc.http.apiGet('govGuide.listGovGuides',
                             param,
                             function(json){
                                var _govGuideInfo = JSON.parse(json);
                                vc.component.chooseGovGuideInfo.govGuides = _govGuideInfo.govGuides;
                             },function(){
                                console.log('请求失败处理');
                             }
                           );
            },
            chooseGovGuide:function(_govGuide){
                if(_govGuide.hasOwnProperty('name')){
                     _govGuide.govGuideName = _govGuide.name;
                }
                vc.emit($props.emitChooseGovGuide,'chooseGovGuide',_govGuide);
                vc.emit($props.emitLoadData,'listGovGuideData',{
                    govGuideId:_govGuide.govGuideId
                });
                $('#chooseGovGuideModel').modal('hide');
            },
            queryGovGuides:function(){
                vc.component._loadAllGovGuideInfo(1,10,vc.component.chooseGovGuideInfo._currentGovGuideName);
            },
            _refreshChooseGovGuideInfo:function(){
                vc.component.chooseGovGuideInfo._currentGovGuideName = "";
            }
        }

    });
})(window.vc);
