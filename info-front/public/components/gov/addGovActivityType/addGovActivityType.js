(function(vc){

    vc.extends({
        propTypes: {
               callBackListener:vc.propTypes.string, //父组件名称
               callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            addGovActivityTypeInfo:{
                typeId:'',
                caId:'',
                typeName:'',
                remark:''

            }
        },
         _initMethod:function(){
         },
         _initEvent:function(){
            vc.on('addGovActivityType','openAddGovActivityTypeModal',function(){
                $('#addGovActivityTypeModel').modal('show');
            });
        },
        methods:{
            addGovActivityTypeValidate(){
                return vc.validate.validate({
                    addGovActivityTypeInfo:vc.component.addGovActivityTypeInfo
                },{
                    'addGovActivityTypeInfo.caId':[
{
                            limit:"required",
                            param:"",
                            errInfo:"所属区域不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"30",
                            errInfo:"所属区域超长"
                        },
                    ],
'addGovActivityTypeInfo.typeName':[
{
                            limit:"required",
                            param:"",
                            errInfo:"类型名称不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"200",
                            errInfo:"类型名称超长"
                        },
                    ],
'addGovActivityTypeInfo.remark':[
{
                            limit:"required",
                            param:"",
                            errInfo:"备注不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"500",
                            errInfo:"备注太长"
                        },
                    ],




                });
            },
            saveGovActivityTypeInfo:function(){
                vc.component.addGovActivityTypeInfo.caId = vc.getCurrentCommunity().caId;
                if(!vc.component.addGovActivityTypeValidate()){
                    vc.toast(vc.validate.errInfo);

                    return ;
                }
                //不提交数据将数据 回调给侦听处理
                if(vc.notNull($props.callBackListener)){
                    vc.emit($props.callBackListener,$props.callBackFunction,vc.component.addGovActivityTypeInfo);
                    $('#addGovActivityTypeModel').modal('hide');
                    return ;
                }

                vc.http.apiPost(
                    '/govActivityType/saveGovActivityType',
                    JSON.stringify(vc.component.addGovActivityTypeInfo),
                    {
                        emulateJSON:true
                     },
                     function(json,res){
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovActivityTypeModel').modal('hide');
                            vc.component.clearAddGovActivityTypeInfo();
                            vc.emit('govActivityTypeManage','listGovActivityType',{});

                            return ;
                        }
                        vc.message(_json.msg);

                     },
                     function(errInfo,error){
                        console.log('请求失败处理');

                        vc.message(errInfo);

                     });
            },
            clearAddGovActivityTypeInfo:function(){
                vc.component.addGovActivityTypeInfo = {
                                            caId:'',
                                            typeName:'',
                                            remark:''
                                        };
            }
        }
    });

})(window.vc);
