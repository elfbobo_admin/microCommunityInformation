/**
    命案基本信息 组件
**/
(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            viewGovCaseInfo: {
                index: 0,
                flowComponent: 'viewGovCaseInfo',
                caseId: '',
                govVictimId: '',
                victimName: '',
                caseName: '',
                caId: '',
                caseNum: '',
                happenTime: '',
                govSuspectId: '',
                suspectName: '',
                settleTime: '',
                briefCondition: '',
                ramark: '',
                statusCd: '',

            }
        },
        _initMethod: function () {
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovCaseInfoData();
        },
        _initEvent: function () {
            vc.on('viewGovCaseInfo', 'chooseGovCase', function (_app) {
                vc.copyObject(_app, vc.component.viewGovCaseInfo);
                vc.emit($props.callBackListener, $props.callBackFunction, vc.component.viewGovCaseInfo);
            });

            vc.on('viewGovCaseInfo', 'onIndex', function (_index) {
                vc.component.viewGovCaseInfo.index = _index;
            });

        },
        methods: {

            _openSelectGovCaseInfoModel() {
                vc.emit('chooseGovCase', 'openChooseGovCaseModel', {});
            },
            _openAddGovCaseInfoModel() {
                vc.emit('addGovCase', 'openAddGovCaseModal', {});
            },
            _loadGovCaseInfoData: function () {

            }
        }
    });

})(window.vc);
