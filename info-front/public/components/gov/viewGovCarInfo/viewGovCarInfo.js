/**
    车辆 组件
**/
(function(vc){

    vc.extends({
        propTypes: {
           callBackListener:vc.propTypes.string, //父组件名称
           callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            viewGovCarInfo:{
                index:0,
                flowComponent:'viewGovCarInfo',
                govCommunityId:'',
paId:'',
carNum:'',
carBrand:'',
carColor:'',
carType:'',
startTime:'',
endTime:'',
remark:'',

            }
        },
        _initMethod:function(){
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovCarInfoData();
        },
        _initEvent:function(){
            vc.on('viewGovCarInfo','chooseGovCar',function(_app){
                vc.copyObject(_app, vc.component.viewGovCarInfo);
                vc.emit($props.callBackListener,$props.callBackFunction,vc.component.viewGovCarInfo);
            });

            vc.on('viewGovCarInfo', 'onIndex', function(_index){
                vc.component.viewGovCarInfo.index = _index;
            });

        },
        methods:{

            _openSelectGovCarInfoModel(){
                vc.emit('chooseGovCar','openChooseGovCarModel',{});
            },
            _openAddGovCarInfoModel(){
                vc.emit('addGovCar','openAddGovCarModal',{});
            },
            _loadGovCarInfoData:function(){

            }
        }
    });

})(window.vc);
