(function (vc, vm) {

    vc.extends({
        data: {
            editGovHealthInfo: {
                healthId: '',
                caId: '',
                name: '',
                ramark: '',

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('editGovHealth', 'openEditGovHealthModal', function (_params) {
                vc.component.refreshEditGovHealthInfo();
                $('#editGovHealthModel').modal('show');
                vc.copyObject(_params, vc.component.editGovHealthInfo);
                vc.component.editGovHealthInfo.caId = vc.getCurrentCommunity().caId;
            });
        },
        methods: {
            editGovHealthValidate: function () {
                return vc.validate.validate({
                    editGovHealthInfo: vc.component.editGovHealthInfo
                }, {
                    'editGovHealthInfo.healthId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "体检项ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "体检项ID不能超过30"
                        },
                    ],
                    'editGovHealthInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "区域ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "区域ID不能超过30"
                        },
                    ],
                    'editGovHealthInfo.name': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "项目名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "项目名称不能超过64"
                        },
                    ]
                    

                });
            },
            editGovHealth: function () {
                if (!vc.component.editGovHealthValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }

                vc.http.apiPost(
                    '/govHealth/updateGovHealth',
                    JSON.stringify(vc.component.editGovHealthInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovHealthModel').modal('hide');
                            vc.emit('govHealthManage', 'listGovHealth', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            refreshEditGovHealthInfo: function () {
                vc.component.editGovHealthInfo = {
                    healthId: '',
                    caId: '',
                    name: '',
                    ramark: '',

                }
            }
        }
    });

})(window.vc, window.vc.component);
