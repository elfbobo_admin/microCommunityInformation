(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovMedicalGradeInfo: {
                medicalGradeId: '',
                medicalClassifyId: '',
                classifyName: '',
                caId: '',
                gradeName: '',
                gradeType: 'C',
                seq: '',
                ramark: '',
            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('addGovMedicalGrade', 'openAddGovMedicalGradeModal', function () {
                $('#addGovMedicalGradeModel').modal('show');
            });
            vc.on('viewGovMedicalClassifyInfo', 'chooseGovMedicalClassify', function (_param) {
                $that.addGovMedicalGradeInfo.medicalClassifyId = _param.medicalClassifyId;
                $that.addGovMedicalGradeInfo.classifyName = _param.classifyName;
            });
        },
        methods: {
            addGovMedicalGradeValidate() {
                return vc.validate.validate({
                    addGovMedicalGradeInfo: vc.component.addGovMedicalGradeInfo
                }, {
                   
                    'addGovMedicalGradeInfo.medicalClassifyId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "分类id不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "分类id不能超过30"
                        },
                    ],
                    'addGovMedicalGradeInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "区域ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "区域ID不能超过30"
                        },
                    ],
                    'addGovMedicalGradeInfo.gradeName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "分级名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "分级名称不能超过30"
                        },
                    ],
                    'addGovMedicalGradeInfo.seq': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "列顺序不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "3",
                            errInfo: "列顺序不能超过3"
                        },
                    ]
                });
            },
            saveGovMedicalGradeInfo: function () {
                vc.component.addGovMedicalGradeInfo.caId = vc.getCurrentCommunity().caId;
                if (!vc.component.addGovMedicalGradeValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovMedicalGradeInfo);
                    $('#addGovMedicalGradeModel').modal('hide');
                    return;
                }
                vc.http.apiPost(
                    '/govMedicalGrade/saveGovMedicalGrade',
                    JSON.stringify(vc.component.addGovMedicalGradeInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovMedicalGradeModel').modal('hide');
                            vc.component.clearAddGovMedicalGradeInfo();
                            vc.emit('govMedicalGradeManage', 'listGovMedicalGrade', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            _openAddChooseGovClassifyModel: function () {
                let _caId = vc.getCurrentCommunity().caId;
                if(!_caId){
                    vc.toast("请先选择区域！");
                    return;
                }
                vc.emit( 'chooseGovMedicalClassify', 'openChooseGovMedicalClassifyModel', _caId);
            },
            _closeAddGovClassify: function () {
                $that.clearAddGovMedicalGradeInfo();
                vc.emit('govMedicalGradeManage', 'listGovMedicalGrade', {});
            },
            clearAddGovMedicalGradeInfo: function () {
                vc.component.addGovMedicalGradeInfo = {
                    medicalGradeId: '',
                    medicalClassifyId: '',
                    caId: '',
                    gradeName: '',
                    gradeType: 'C',
                    seq: '',
                    ramark: '',
                    

                };
            }
        }
    });

})(window.vc);
