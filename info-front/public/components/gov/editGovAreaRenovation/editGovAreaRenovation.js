(function (vc, vm) {

    vc.extends({
        data: {
            editGovAreaRenovationInfo: {
                govRenovationId: '',
                caId: '',
                typeId: '',
                govAreaTypes: [],
                securityKey: '',
                securityProblem: '',
                securityRange: '',
                leadCompany: '',
                leadParticipate: '',
                name: '',
                tel: '',
                startTime: '',
                endTime: '',
                crackCriminal: '',
                crackSecurity: '',
                leadRamark: '',

            }
        },
        _initMethod: function () {
            $that._listEditGovAreaTypes();
        },
        _initEvent: function () {
            vc.on('editGovAreaRenovation', 'openEditGovAreaRenovationModal', function (_params) {
                vc.component.refreshEditGovAreaRenovationInfo();
                $('#editGovAreaRenovationModel').modal('show');
                vc.initDate('editStartTime', function (_value) {
                    $that.editGovAreaRenovationInfo.startTime = _value;
                });
                vc.initDate('editEndTime', function (_value) {
                    $that.editGovAreaRenovationInfo.endTime = _value;
                });
                vc.copyObject(_params, vc.component.editGovAreaRenovationInfo);
                vc.component.editGovAreaRenovationInfo.caId = vc.getCurrentCommunity().caId;
            });
        },
        methods: {
            editGovAreaRenovationValidate: function () {
                return vc.validate.validate({
                    editGovAreaRenovationInfo: vc.component.editGovAreaRenovationInfo
                }, {
                    'editGovAreaRenovationInfo.govRenovationId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "重点地区整治ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "重点地区整治ID不能超过30"
                        },
                    ],
                    'editGovAreaRenovationInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "区域ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "区域ID不能超过30"
                        },
                    ],
                    'editGovAreaRenovationInfo.typeId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "涉及区域类型ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "涉及区域类型ID不能超过30"
                        },
                    ],
                    'editGovAreaRenovationInfo.securityKey': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "治安重点地区不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "治安重点地区不能超过64"
                        },
                    ],
                    'editGovAreaRenovationInfo.securityProblem': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "治安突出问题不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "治安突出问题不能超过64"
                        },
                    ],
                    'editGovAreaRenovationInfo.securityRange': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "涉及区域范围不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "涉及区域范围不能超过64"
                        },
                    ],
                    'editGovAreaRenovationInfo.leadCompany': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "整治牵头单位不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "102",
                            errInfo: "整治牵头单位不能超过102"
                        },
                    ],
                    'editGovAreaRenovationInfo.leadParticipate': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "整治参与单位不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "102",
                            errInfo: "整治参与单位不能超过102"
                        },
                    ],
                    'editGovAreaRenovationInfo.name': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "负责人姓名不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "负责人姓名不能超过64"
                        },
                    ],
                    'editGovAreaRenovationInfo.tel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "负责人联系方式不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "负责人联系方式不能超过11"
                        },
                    ],
                    'editGovAreaRenovationInfo.startTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "整改开始时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "整改开始时间不能超过时间类型"
                        },
                    ],
                    'editGovAreaRenovationInfo.endTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "整改结束时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "整改结束时间不能超过时间类型"
                        },
                    ],
                    'editGovAreaRenovationInfo.crackCriminal': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "破获刑事案件数不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "破获刑事案件数不能超过11"
                        },
                    ],
                    'editGovAreaRenovationInfo.crackSecurity': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "查处治安案件数不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "查处治安案件数不能超过11"
                        },
                    ],
                    'editGovAreaRenovationInfo.leadRamark': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "整治情况不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "整治情况不能超过1024"
                        },
                    ],
                    'editGovAreaRenovationInfo.govRenovationId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "重点地区排查整治ID不能为空"
                        }]

                });
            },
            editGovAreaRenovation: function () {
                if (!vc.component.editGovAreaRenovationValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }

                vc.http.apiPost(
                    '/govAreaRenovation/updateGovAreaRenovation',
                    JSON.stringify(vc.component.editGovAreaRenovationInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovAreaRenovationModel').modal('hide');
                            vc.emit('govAreaRenovationManage', 'listGovAreaRenovation', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            _listEditGovAreaTypes: function () {
                var param = {
                    params: {
                        caId:vc.getCurrentCommunity().caId,
                        page: 1,
                        row: 50
                    }
                };
                //发送get请求
                vc.http.apiGet('/govAreaType/queryGovAreaType',
                    param,
                    function (json, res) {
                        var _govAreaTypeManageInfo = JSON.parse(json);
                        vc.component.editGovAreaRenovationInfo.govAreaTypes = _govAreaTypeManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            refreshEditGovAreaRenovationInfo: function () {
                let _govAreaTypes =  vc.component.editGovAreaRenovationInfo.govAreaTypes;
                vc.component.editGovAreaRenovationInfo = {
                    govRenovationId: '',
                    caId: '',
                    typeId: '',
                    govAreaTypes: _govAreaTypes,
                    securityKey: '',
                    securityProblem: '',
                    securityRange: '',
                    leadCompany: '',
                    leadParticipate: '',
                    name: '',
                    tel: '',
                    startTime: '',
                    endTime: '',
                    crackCriminal: '',
                    crackSecurity: '',
                    leadRamark: '',

                }
            }
        }
    });

})(window.vc, window.vc.component);
