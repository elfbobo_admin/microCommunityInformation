(function (vc) {
    vc.extends({
        data: {
            viewMeetingInfo: {
                orgId: '',
                govPersonId: '',
                govMeetingLists: []
            }
        },
        _initMethod: function () {
        },
        _initEvent: function () {
            vc.on('meetingInfo', 'switch', function (_param) {
                vc.component._refreshviewMeetingInfo();
                console.log(_param);
                vc.component._loadMeetingInfo(_param.govPersonId);
            });
        },
        methods: {
            _loadMeetingInfo: function (_personId) {
                var param = {
                    params: {
                        page: 1,
                        row: 10,
                        caId: vc.getCurrentCommunity().caId,
                        govMemberId: _personId
                    }
                };
                //发送get请求
                vc.http.apiGet('/govMeetingList/queryPersonIdMeetingList',
                    param,
                    function (json) {
                        var _labelInfo = JSON.parse(json);
                        $that.viewMeetingInfo.govMeetingLists = _labelInfo.data;
                    }, function () {
                        console.log('请求失败处理');
                    }
                );
            },
            _refreshviewMeetingInfo: function () {
                $that.viewMeetingInfo = {
                    orgId: '',
                    govPersonId: '',
                    labels: []
                }
            }
        }

    });
})(window.vc);
