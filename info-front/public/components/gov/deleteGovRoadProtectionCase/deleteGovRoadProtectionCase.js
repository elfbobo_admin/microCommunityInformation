(function(vc,vm){

    vc.extends({
        data:{
            deleteGovRoadProtectionCaseInfo:{

            }
        },
         _initMethod:function(){

         },
         _initEvent:function(){
             vc.on('deleteGovRoadProtectionCase','openDeleteGovRoadProtectionCaseModal',function(_params){

                vc.component.deleteGovRoadProtectionCaseInfo = _params;
                $('#deleteGovRoadProtectionCaseModel').modal('show');

            });
        },
        methods:{
            deleteGovRoadProtectionCase:function(){
                vc.component.deleteGovRoadProtectionCaseInfo.caId=vc.getCurrentCommunity().caId;
                vc.http.apiPost(
                    '/govRoadProtectionCase/deleteGovRoadProtectionCase',
                    JSON.stringify(vc.component.deleteGovRoadProtectionCaseInfo),
                    {
                        emulateJSON:true
                     },
                     function(json,res){
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovRoadProtectionCaseModel').modal('hide');
                            vc.emit('govRoadProtectionCaseManage','listGovRoadProtectionCase',{});
                            return ;
                        }
                        vc.message(_json.msg);
                     },
                     function(errInfo,error){
                        console.log('请求失败处理');
                        vc.message(json);

                     });
            },
            closeDeleteGovRoadProtectionCaseModel:function(){
                $('#deleteGovRoadProtectionCaseModel').modal('hide');
            }
        }
    });

})(window.vc,window.vc.component);
