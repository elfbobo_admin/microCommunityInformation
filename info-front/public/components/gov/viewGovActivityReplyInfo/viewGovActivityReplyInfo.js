/**
    活动评论 组件
**/
(function(vc){

    vc.extends({
        propTypes: {
           callBackListener:vc.propTypes.string, //父组件名称
           callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            viewGovActivityReplyInfo:{
                index:0,
                flowComponent:'viewGovActivityReplyInfo',
                replyId:'',
actId:'',
parentReplyId:'',
context:'',
userId:'',
userName:'',
preUserName:'',
sessionId:'',
preAuthorId:'',
statusCd:'',

            }
        },
        _initMethod:function(){
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovActivityReplyInfoData();
        },
        _initEvent:function(){
            vc.on('viewGovActivityReplyInfo','chooseGovActivityReply',function(_app){
                vc.copyObject(_app, vc.component.viewGovActivityReplyInfo);
                vc.emit($props.callBackListener,$props.callBackFunction,vc.component.viewGovActivityReplyInfo);
            });

            vc.on('viewGovActivityReplyInfo', 'onIndex', function(_index){
                vc.component.viewGovActivityReplyInfo.index = _index;
            });

        },
        methods:{

            _openSelectGovActivityReplyInfoModel(){
                vc.emit('chooseGovActivityReply','openChooseGovActivityReplyModel',{});
            },
            _openAddGovActivityReplyInfoModel(){
                vc.emit('addGovActivityReply','openAddGovActivityReplyModal',{});
            },
            _loadGovActivityReplyInfoData:function(){

            }
        }
    });

})(window.vc);
