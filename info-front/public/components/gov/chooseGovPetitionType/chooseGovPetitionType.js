(function(vc){
    vc.extends({
        propTypes: {
           emitChooseGovPetitionType:vc.propTypes.string,
           emitLoadData:vc.propTypes.string
        },
        data:{
            chooseGovPetitionTypeInfo:{
                govPetitionTypes:[],
                _currentGovPetitionTypeName:'',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('chooseGovPetitionType','openChooseGovPetitionTypeModel',function(_param){
                $('#chooseGovPetitionTypeModel').modal('show');
                vc.component._refreshChooseGovPetitionTypeInfo();
                vc.component._loadAllGovPetitionTypeInfo(1,10,'');
            });
        },
        methods:{
            _loadAllGovPetitionTypeInfo:function(_page,_row,_name){
                var param = {
                    params:{
                        page:_page,
                        row:_row,
                        caId:vc.getCurrentCommunity().caId,
                        name:_name
                    }
                };

                //发送get请求
               vc.http.apiGet('/govPetitionType/queryGovPetitionType',
                             param,
                             function(json){
                                var _govPetitionTypeInfo = JSON.parse(json);
                                vc.component.chooseGovPetitionTypeInfo.govPetitionTypes = _govPetitionTypeInfo.govPetitionTypes;
                             },function(){
                                console.log('请求失败处理');
                             }
                           );
            },
            chooseGovPetitionType:function(_govPetitionType){
                if(_govPetitionType.hasOwnProperty('name')){
                     _govPetitionType.govPetitionTypeName = _govPetitionType.name;
                }
                vc.emit($props.emitChooseGovPetitionType,'chooseGovPetitionType',_govPetitionType);
                vc.emit($props.emitLoadData,'listGovPetitionTypeData',{
                    govPetitionTypeId:_govPetitionType.govPetitionTypeId
                });
                $('#chooseGovPetitionTypeModel').modal('hide');
            },
            queryGovPetitionTypes:function(){
                vc.component._loadAllGovPetitionTypeInfo(1,10,vc.component.chooseGovPetitionTypeInfo._currentGovPetitionTypeName);
            },
            _refreshChooseGovPetitionTypeInfo:function(){
                vc.component.chooseGovPetitionTypeInfo._currentGovPetitionTypeName = "";
            }
        }

    });
})(window.vc);
