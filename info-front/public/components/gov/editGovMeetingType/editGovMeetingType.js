(function (vc, vm) {

    vc.extends({
        data: {
            editGovMeetingTypeInfo: {
                typeId: '',
                caId: '',
                meetingTypeName: '',
                ramark: ''
            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('editGovMeetingType', 'openEditGovMeetingTypeModal', function (_params) {
                vc.component.refreshEditGovMeetingTypeInfo();
                $('#editGovMeetingTypeModel').modal('show');
                vc.copyObject(_params, vc.component.editGovMeetingTypeInfo);
                vc.component.editGovMeetingTypeInfo.caId = vc.getCurrentCommunity().caId;
            });
        },
        methods: {
            editGovMeetingTypeValidate: function () {
                return vc.validate.validate({
                    editGovMeetingTypeInfo: vc.component.editGovMeetingTypeInfo
                }, {
                    'editGovMeetingTypeInfo.typeId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "会议类型ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "会议类型ID不能超过30"
                        },
                    ],
                    'editGovMeetingTypeInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "区域ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "区域ID不能超过30"
                        },
                    ],
                    'editGovMeetingTypeInfo.meetingTypeName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "类型名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "类型名称不能超过64"
                        },
                    ]

                });
            },
            editGovMeetingType: function () {
                if (!vc.component.editGovMeetingTypeValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }

                vc.http.apiPost(
                    '/govMeetingType/updateGovMeetingType',
                    JSON.stringify(vc.component.editGovMeetingTypeInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovMeetingTypeModel').modal('hide');
                            vc.emit('govMeetingTypeManage', 'listGovMeetingType', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            refreshEditGovMeetingTypeInfo: function () {
                vc.component.editGovMeetingTypeInfo = {
                    typeId: '',
                    caId: '',
                    meetingTypeName: '',
                    ramark: ''
                }
            }
        }
    });

})(window.vc, window.vc.component);
