(function(vc){
    vc.extends({
        propTypes: {
           emitChooseGovOldPerson:vc.propTypes.string,
           emitLoadData:vc.propTypes.string
        },
        data:{
            chooseGovOldPersonInfo:{
                govOldPersons:[],
                _currentGovOldPersonName:'',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('chooseGovOldPerson','openChooseGovOldPersonModel',function(_param){
                $('#chooseGovOldPersonModel').modal('show');
                vc.component._refreshChooseGovOldPersonInfo();
                vc.component._loadAllGovOldPersonInfo(1,10,'');
            });
        },
        methods:{
            _loadAllGovOldPersonInfo:function(_page,_row,_name){
                var param = {
                    params:{
                        page:_page,
                        row:_row,
                        caId: vc.getCurrentCommunity().caId,
                        personName:_name,
                        birthday:vc.dateFormat(new Date().getTime())
                    }
                };

                //发送get请求
               vc.http.apiGet('/govOldPerson/queryGovOldPerson',
                             param,
                             function(json){
                                var _govOldPersonInfo = JSON.parse(json);
                                vc.component.chooseGovOldPersonInfo.govOldPersons = _govOldPersonInfo.data;
                             },function(){
                                console.log('请求失败处理');
                             }
                           );
            },
            chooseGovOldPerson:function(_govOldPerson){
                if(_govOldPerson.hasOwnProperty('name')){
                     _govOldPerson.govOldPersonName = _govOldPerson.name;
                }
                vc.emit($props.emitChooseGovOldPerson,'chooseGovOldPerson',_govOldPerson);
                vc.emit($props.emitLoadData,'listGovOldPersonData',{
                    govOldPersonId:_govOldPerson.govOldPersonId
                });
                $('#chooseGovOldPersonModel').modal('hide');
            },
            queryGovOldPersons:function(){
                vc.component._loadAllGovOldPersonInfo(1,10,vc.component.chooseGovOldPersonInfo._currentGovOldPersonName);
            },
            _refreshChooseGovOldPersonInfo:function(){
                vc.component.chooseGovOldPersonInfo._currentGovOldPersonName = "";
            }
        }

    });
})(window.vc);
