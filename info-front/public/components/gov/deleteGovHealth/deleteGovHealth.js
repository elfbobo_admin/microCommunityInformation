(function(vc,vm){

    vc.extends({
        data:{
            deleteGovHealthInfo:{

            }
        },
         _initMethod:function(){

         },
         _initEvent:function(){
             vc.on('deleteGovHealth','openDeleteGovHealthModal',function(_params){

                vc.component.deleteGovHealthInfo = _params;
                $('#deleteGovHealthModel').modal('show');

            });
        },
        methods:{
            deleteGovHealth:function(){
                vc.component.deleteGovHealthInfo.caId=vc.getCurrentCommunity().caId;
                vc.http.apiPost(
                    '/govHealth/deleteGovHealth',
                    JSON.stringify(vc.component.deleteGovHealthInfo),
                    {
                        emulateJSON:true
                     },
                     function(json,res){
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovHealthModel').modal('hide');
                            vc.emit('govHealthManage','listGovHealth',{});
                            return ;
                        }
                        vc.message(_json.msg);
                     },
                     function(errInfo,error){
                        console.log('请求失败处理');
                        vc.message(json);

                     });
            },
            closeDeleteGovHealthModel:function(){
                $('#deleteGovHealthModel').modal('hide');
            }
        }
    });

})(window.vc,window.vc.component);
