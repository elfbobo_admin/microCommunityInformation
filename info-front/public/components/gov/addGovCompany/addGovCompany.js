(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovCompanyInfo: {
                govCompanyId: '',
                caId: '',
                companyName: '',
                companyType: '',
                idCard: '',
                artificialPerson: '',
                companyAddress: '',
                registerTime: '',
                personName: '',
                personIdCard: '',
                personTel: '',
                ramark: '',
                govCommunityAreas:[],
                govCommunityId:'',
                govCommunitys:[]

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('addGovCompany', 'openAddGovCompanyModal', function (_param) {
                vc.initDateTime('addRegisterTime', function (_value) {
                    $that.addGovCompanyInfo.registerTime = _value;
                });
                $that._listAddCompantGovCommunityAreas();
                $that._listAddCompanyGovCommunitys();
                $('#addGovCompanyModel').modal('show');
                $that.addGovCompanyInfo.companyType = _param._companyType;
            });
        },
        methods: {
            addGovCompanyValidate() {
                return vc.validate.validate({
                    addGovCompanyInfo: vc.component.addGovCompanyInfo
                }, {
                    'addGovCompanyInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "所属区域不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "所属区域超长"
                        },
                    ],
                    'addGovCompanyInfo.companyName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "公司名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "128",
                            errInfo: "公司名称超长"
                        },
                    ],
                    'addGovCompanyInfo.companyType': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "公司类型不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "公司类型超长"
                        },
                    ],
                    'addGovCompanyInfo.idCard': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "证件号码不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "证件号码超长"
                        },
                    ],
                    'addGovCompanyInfo.artificialPerson': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "法人不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "法人超长"
                        },
                    ],
                    'addGovCompanyInfo.companyAddress': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "地址不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "512",
                            errInfo: "地址超长"
                        },
                    ],
                    'addGovCompanyInfo.registerTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "注册时间不能为空"
                        },
                        {
                            limit: "dateTime",
                            param: "",
                            errInfo: "不是有效的时间格式"
                        },
                    ],
                    'addGovCompanyInfo.personName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "负责人不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "负责人太长"
                        },
                    ],
                    'addGovCompanyInfo.personIdCard': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "负责人身份证不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "20",
                            errInfo: "负责人身份证超长"
                        },
                    ],
                    'addGovCompanyInfo.personTel': [
                        {
                            limit: "phone",
                            param: "",
                            errInfo: "电话格式错误"
                        },
                    ],
                    'addGovCompanyInfo.ramark': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "备注不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "备注太长"
                        },
                    ],




                });
            },
            saveGovCompanyInfo: function () {
                vc.component.addGovCompanyInfo.caId = vc.getCurrentCommunity().caId;
                if (!vc.component.addGovCompanyValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }
                if(!vc.component.isCardNoAdd(vc.component.addGovCompanyInfo.personIdCard)){
                    vc.toast("身份证格式有误，请检查身份证信息");

                    return;
                }
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovCompanyInfo);
                    $('#addGovCompanyModel').modal('hide');
                    return;
                }
                vc.http.apiPost(
                    '/govCompany/saveGovCompany',
                    JSON.stringify(vc.component.addGovCompanyInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovCompanyModel').modal('hide');
                            vc.component.clearAddGovCompanyInfo();
                            vc.emit('govCompanyManage', 'listGovCompany', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            isCardNoAdd: function (card) {
                //身份证号码为15位或者18位，15位时全为数字，18位前17位为数字，最后一位是校验位，可能为数字或字符X
                var reg = /(^\d{15}$)|(^\d{17}(\d|X)$)/;
                if (reg.test(card) === false) {
                    return false;
                }
                return true;
            },
            
            _listAddCompantGovCommunityAreas: function () {

                let param = {
                    params: {
                        page: 1,
                        row: 50
                    }
                };
                //发送get请求
                vc.http.apiGet('/govCommunityArea/queryGovCommunityArea',
                    param,
                    function (json, res) {
                        var _govCommunityAreaManageInfo = JSON.parse(json);
                        vc.component.addGovCompanyInfo.total = _govCommunityAreaManageInfo.total;
                        vc.component.addGovCompanyInfo.records = _govCommunityAreaManageInfo.records;
                        vc.component.addGovCompanyInfo.govCommunityAreas = _govCommunityAreaManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
             _listAddCompanyGovCommunitys: function () {

                let param = {
                    params: {
                        page: 1,
                        row: 50,
                        caId: vc.getCurrentCommunity().caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govCommunity/queryGovCommunity',
                    param,
                    function (json, res) {
                        var _govCommunityManageInfo = JSON.parse(json);
                        vc.component.addGovCompanyInfo.total = _govCommunityManageInfo.total;
                        vc.component.addGovCompanyInfo.records = _govCommunityManageInfo.records;
                        vc.component.addGovCompanyInfo.govCommunitys = _govCommunityManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            clearAddGovCompanyInfo: function () {
                vc.component.addGovCompanyInfo = {
                    caId: '',
                    companyName: '',
                    companyType: '',
                    idCard: '',
                    artificialPerson: '',
                    companyAddress: '',
                    registerTime: '',
                    personName: '',
                    personIdCard: '',
                    personTel: '',
                    ramark: '',
                    govCommunityAreas:[],
                    govCommunityId:'',
                    govCommunitys:[]
                };
            }
        }
    });

})(window.vc);
