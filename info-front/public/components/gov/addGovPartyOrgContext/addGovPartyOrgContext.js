(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovPartyOrgContextInfo: {
                orgId: '',
                orgId: '',
                context: '',

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('addGovPartyOrgContext', 'openAddGovPartyOrgContextModal', function () {
                $('#addGovPartyOrgContextModel').modal('show');
            });
        },
        methods: {
            addGovPartyOrgContextValidate() {
                return vc.validate.validate({
                    addGovPartyOrgContextInfo: vc.component.addGovPartyOrgContextInfo
                }, {
                    'addGovPartyOrgContextInfo.orgId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "组织名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "组织名称超长"
                        },
                    ],
                    'addGovPartyOrgContextInfo.context': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "简介不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "简介超长"
                        },
                    ],




                });
            },
            saveGovPartyOrgContextInfo: function () {
                if (!vc.component.addGovPartyOrgContextValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }

                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovPartyOrgContextInfo);
                    $('#addGovPartyOrgContextModel').modal('hide');
                    return;
                }

                vc.http.apiPost(
                    '/govPartyOrgContext/saveGovPartyOrgContext',
                    JSON.stringify(vc.component.addGovPartyOrgContextInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovPartyOrgContextModel').modal('hide');
                            vc.component.clearAddGovPartyOrgContextInfo();
                            vc.emit('govPartyOrgContextManage', 'listGovPartyOrgContext', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            clearAddGovPartyOrgContextInfo: function () {
                vc.component.addGovPartyOrgContextInfo = {
                    orgId: '',
                    context: '',

                };
            }
        }
    });

})(window.vc);
