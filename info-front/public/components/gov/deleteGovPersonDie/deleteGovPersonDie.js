(function(vc,vm){

    vc.extends({
        data:{
            deleteGovPersonDieInfo:{

            }
        },
         _initMethod:function(){

         },
         _initEvent:function(){
             vc.on('deleteGovPersonDie','openDeleteGovPersonDieModal',function(_params){

                vc.component.deleteGovPersonDieInfo = _params;
                $('#deleteGovPersonDieModel').modal('show');

            });
        },
        methods:{
            deleteGovPersonDie:function(){
                vc.component.deleteGovPersonDieInfo.caId=vc.getCurrentCommunity().caId;
                vc.http.apiPost(
                    '/govPersonDie/deleteGovPersonDie',
                    JSON.stringify(vc.component.deleteGovPersonDieInfo),
                    {
                        emulateJSON:true
                     },
                     function(json,res){
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovPersonDieModel').modal('hide');
                            vc.emit('govPersonDieManage','listGovPersonDie',{});
                            return ;
                        }
                        vc.message(_json.msg);
                     },
                     function(errInfo,error){
                        console.log('请求失败处理');
                        vc.message(json);

                     });
            },
            closeDeleteGovPersonDieModel:function(){
                $('#deleteGovPersonDieModel').modal('hide');
            }
        }
    });

})(window.vc,window.vc.component);
