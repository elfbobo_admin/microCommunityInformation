(function(vc){
    vc.extends({
        propTypes: {
           emitChooseGovAids:vc.propTypes.string,
           emitLoadData:vc.propTypes.string
        },
        data:{
            chooseGovAidsInfo:{
                govAidss:[],
                _currentGovAidsName:'',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('chooseGovAids','openChooseGovAidsModel',function(_param){
                $('#chooseGovAidsModel').modal('show');
                vc.component._refreshChooseGovAidsInfo();
                vc.component._loadAllGovAidsInfo(1,10,'');
            });
        },
        methods:{
            _loadAllGovAidsInfo:function(_page,_row,_name){
                var param = {
                    params:{
                        page:_page,
                        row:_row,
                        caId:vc.getCurrentCommunity().caId,
                        name:_name
                    }
                };

                //发送get请求
               vc.http.apiGet('/govAids/queryGovAids',
                             param,
                             function(json){
                                var _govAidsInfo = JSON.parse(json);
                                vc.component.chooseGovAidsInfo.govAidss = _govAidsInfo.govAidss;
                             },function(){
                                console.log('请求失败处理');
                             }
                           );
            },
            chooseGovAids:function(_govAids){
                if(_govAids.hasOwnProperty('name')){
                     _govAids.govAidsName = _govAids.name;
                }
                vc.emit($props.emitChooseGovAids,'chooseGovAids',_govAids);
                vc.emit($props.emitLoadData,'listGovAidsData',{
                    govAidsId:_govAids.govAidsId
                });
                $('#chooseGovAidsModel').modal('hide');
            },
            queryGovAidss:function(){
                vc.component._loadAllGovAidsInfo(1,10,vc.component.chooseGovAidsInfo._currentGovAidsName);
            },
            _refreshChooseGovAidsInfo:function(){
                vc.component.chooseGovAidsInfo._currentGovAidsName = "";
            }
        }

    });
})(window.vc);
