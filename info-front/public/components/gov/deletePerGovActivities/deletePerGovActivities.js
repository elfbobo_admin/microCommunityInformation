(function(vc,vm){

    vc.extends({
        data:{
            deletePerGovActivitiesInfo:{

            }
        },
         _initMethod:function(){

         },
         _initEvent:function(){
             vc.on('deletePerGovActivities','openDeletePerGovActivitiesModal',function(_params){

                vc.component.deletePerGovActivitiesInfo = _params;
                $('#deletePerGovActivitiesModel').modal('show');

            });
        },
        methods:{
            deletePerGovActivities:function(){
                vc.component.deletePerGovActivitiesInfo.caId=vc.getCurrentCommunity().caId;
                vc.http.apiPost(
                    '/perGovActivities/deletePerGovActivities',
                    JSON.stringify(vc.component.deletePerGovActivitiesInfo),
                    {
                        emulateJSON:true
                     },
                     function(json,res){
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deletePerGovActivitiesModel').modal('hide');
                            vc.emit('perGovActivitiesManage','listPerGovActivities',{});
                            return ;
                        }
                        vc.message(_json.msg);
                     },
                     function(errInfo,error){
                        console.log('请求失败处理');
                        vc.message(json);

                     });
            },
            closeDeletePerGovActivitiesModel:function(){
                $('#deletePerGovActivitiesModel').modal('hide');
            }
        }
    });

})(window.vc,window.vc.component);
