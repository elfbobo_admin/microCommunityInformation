(function (vc) {
    vc.extends({
        propTypes: {
            emitChooseGovCompany: vc.propTypes.string,
            emitLoadData: vc.propTypes.string
        },
        data: {
            chooseGovCompanyInfo: {
                total: 0,
                records: 1,
                govCompanys: [],
                _currentGovCompanyName: '',
            }
        },
        _initMethod: function () {
        },
        _initEvent: function () {
            vc.on('chooseGovCompany', 'openChooseGovCompanyModel', function (_param) {
                $('#chooseGovCompanyModel').modal('show');
                vc.component._refreshChooseGovCompanyInfo();
                vc.component._loadAllGovCompanyInfo(1, 10, '');
            });
        },
        methods: {
            _loadAllGovCompanyInfo: function (_page, _row, _name) {
                var param = {
                    params: {
                        page: _page,
                        row: _row,
                        caId: vc.getCurrentCommunity().caId,
                        companyName: _name
                    }
                };
                //发送get请求
                vc.http.apiGet('/govCompany/queryGovCompany',
                    param,
                    function (json) {
                        var _govCompanyInfo = JSON.parse(json);
                        vc.component.chooseGovCompanyInfo.govCompanys = _govCompanyInfo.data;
                        vc.component.chooseGovCompanyInfo.total = _govCompanyInfo.total;
                        vc.component.chooseGovCompanyInfo.records = _govCompanyInfo.records;
                        vc.emit('pagination', 'init', {
                            total: vc.component.chooseGovCompanyInfo.records,
                            currentPage: _page
                        });
                    }, function () {
                        console.log('请求失败处理');
                    }
                );
            },
            chooseGovCompany: function (_govCompany) {
                vc.emit($props.emitChooseGovCompany, 'chooseGovCompany', _govCompany);
                $('#chooseGovCompanyModel').modal('hide');
            },
            queryGovCompanys: function () {
                vc.component._loadAllGovCompanyInfo(1, 10, vc.component.chooseGovCompanyInfo._currentGovCompanyName);
            },
            _refreshChooseGovCompanyInfo: function () {
                vc.component.chooseGovCompanyInfo._currentGovCompanyName = "";
            }
        }

    });
})(window.vc);
