(function (vc, vm) {

    vc.extends({
        data: {
            editGovAidsInfo: {
                aidsId: '',
                name: '',
                caId: '',
                address: '',
                tel: '',
                aidsStartTime: '',
                aidsReason: '',
                emergencyPerson: '',
                emergencyTel: '',
                statusCd: '',

            }
        },
        _initMethod: function () {
            $('.editStartTime').datetimepicker({
                language: 'zh-CN',
                fontAwesome: 'fa',
                format: 'yyyy-mm-dd hh:ii:ss',
                initTime: true,
                initialDate: new Date(),
                autoClose: 1,
                todayBtn: true

            });
            $('.editStartTime').datetimepicker()
                .on('changeDate', function (ev) {
                    var value = $(".editStartTime").val();
                    vc.component.editGovAidsInfo.aidsStartTime = value;
                });
        },
        _initEvent: function () {
            vc.on('editGovAids', 'openEditGovAidsModal', function (_params) {
                vc.component.refreshEditGovAidsInfo();
                $('#editGovAidsModel').modal('show');
                vc.copyObject(_params, vc.component.editGovAidsInfo);
                vc.component.editGovAidsInfo.caId = vc.getCurrentCommunity().caId;
            });
        },
        methods: {
            editGovAidsValidate: function () {
                return vc.validate.validate({
                    editGovAidsInfo: vc.component.editGovAidsInfo
                }, {
                    'editGovAidsInfo.name': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "艾滋病者名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "艾滋病者名称不能超过64"
                        },
                    ],
                    'editGovAidsInfo.address': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "住址不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "512",
                            errInfo: "住址不能超过512"
                        },
                    ],
                    'editGovAidsInfo.tel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "手机号不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "手机号不能超过11"
                        },
                    ],
                    'editGovAidsInfo.aidsStartTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "开始时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "开始时间不能超过时间类型"
                        },
                    ],
                    'editGovAidsInfo.aidsReason': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "感染原因不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "128",
                            errInfo: "感染原因不能超过128"
                        },
                    ],
                    'editGovAidsInfo.emergencyPerson': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "紧急联系人不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "紧急联系人不能超过64"
                        },
                    ],
                    'editGovAidsInfo.emergencyTel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "紧急联系电话不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "紧急联系电话不能超过11"
                        },
                    ],
                    'editGovAidsInfo.aidsId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "艾滋病者ID不能为空"
                        }]

                });
            },
            editGovAids: function () {
                if (!vc.component.editGovAidsValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }

                vc.http.apiPost(
                    '/govAids/updateGovAids',
                    JSON.stringify(vc.component.editGovAidsInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovAidsModel').modal('hide');
                            vc.emit('govAidsManage', 'listGovAids', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            refreshEditGovAidsInfo: function () {
                vc.component.editGovAidsInfo = {
                    aidsId: '',
                    name: '',
                    caId: '',
                    address: '',
                    tel: '',
                    aidsStartTime: '',
                    aidsReason: '',
                    emergencyPerson: '',
                    emergencyTel: '',
                    statusCd: '',

                }
            }
        }
    });

})(window.vc, window.vc.component);
