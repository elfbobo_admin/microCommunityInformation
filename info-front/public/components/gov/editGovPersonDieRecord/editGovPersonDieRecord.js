(function (vc, vm) {

    vc.extends({
        data: {
            editGovPersonDieRecordInfo: {
                dieRecordId: '',
                dieRecordId: '',
                dieId: '',
                govPersonId: '',
                title: '',
                typeCd: '',
                context: '',
                activityTime: '',
                caId: '',
                statusCd: '',

            }
        },
        _initMethod: function () {
            $that._initEditContextInfo();
        },
        _initEvent: function () {
            vc.on('editGovPersonDieRecord', 'openEditGovPersonDieRecordModal', function (_params) {
                vc.component.refreshEditGovPersonDieRecordInfo();
                $('#editGovPersonDieRecordModel').modal('show');
                vc.copyObject(_params, vc.component.editGovPersonDieRecordInfo);
                vc.component.editGovPersonDieRecordInfo.caId = vc.getCurrentCommunity().caId;
                $(".eidtSummernote").summernote('code', vc.component.editGovPersonDieRecordInfo.context);
            });
        },
        methods: {
            editGovPersonDieRecordValidate: function () {
                return vc.validate.validate({
                    editGovPersonDieRecordInfo: vc.component.editGovPersonDieRecordInfo
                }, {
                    'editGovPersonDieRecordInfo.title': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "记录标题不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "200",
                            errInfo: "记录标题不能超过200"
                        },
                    ],
                    'editGovPersonDieRecordInfo.context': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "记录内容不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "长文本",
                            errInfo: "记录内容不能超过长文本"
                        },
                    ],
                    'editGovPersonDieRecordInfo.activityTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "参加时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "参加时间不能超过时间类型"
                        },
                    ]
                });
            },
            editGovPersonDieRecord: function () {
                if (!vc.component.editGovPersonDieRecordValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }

                vc.http.apiPost(
                    '/govPersonDieRecord/updateGovPersonDieRecord',
                    JSON.stringify(vc.component.editGovPersonDieRecordInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovPersonDieRecordModel').modal('hide');
                            vc.emit('govPersonDieRecordManage', 'listGovPersonDieRecord', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            _initEditContextInfo: function () {
                $('.editActivityTime').datetimepicker({
                    language: 'zh-CN',
                    fontAwesome: 'fa',
                    format: 'yyyy-mm-dd hh:ii:ss',
                    initTime: true,
                    initialDate: new Date(),
                    autoClose: 1,
                    todayBtn: true
                });
                var $summernote = $('.eidtSummernote').summernote({
                    lang: 'zh-CN',
                    height: 300,
                    placeholder: '必填，请输入公告内容',
                    callbacks: {
                        onImageUpload: function (files, editor, $editable) {
                            $that.sendEditFile($summernote, files[0]);
                        },
                        onChange: function (contents, $editable) {
                            vc.component.editGovPersonDieRecordInfo.context = contents;
                        }
                    },
                    toolbar: [
                        ['style', ['style']],
                        ['font', ['bold', 'italic', 'underline', 'clear']],
                        ['fontname', ['fontname']],
                        ['color', ['color']],
                        ['para', ['ul', 'ol', 'paragraph']],
                        ['height', ['height']],
                        ['table', ['table']],
                        ['insert', ['link', 'picture']],
                        ['view', ['fullscreen', 'codeview']],
                        ['help', ['help']]
                    ],
                });
            },
            sendEditFile: function ($summernote, files) {
                console.log('上传图片', files);

                var param = new FormData();
                param.append("uploadFile", files);
                vc.http.uploadImage(
                    param,
                    {
                        emulateJSON: true,
                        //添加请求头
                        headers: {
                            "Content-Type": "multipart/form-data"
                        }
                    },
                    function (json, res) {
                        if (res.status == 200) {
                            var data = JSON.parse(json);
                            $summernote.summernote('insertImage', data.data);
                            return;
                        }
                        vc.toast(json);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');
                        vc.toast(errInfo);
                    });

            },
            refreshEditGovPersonDieRecordInfo: function () {
                vc.component.editGovPersonDieRecordInfo = {
                    dieRecordId: '',
                    dieRecordId: '',
                    dieId: '',
                    govPersonId: '',
                    title: '',
                    typeCd: '',
                    context: '',
                    activityTime: '',
                    caId: '',
                    statusCd: '',

                }
            }
        }
    });

})(window.vc, window.vc.component);
