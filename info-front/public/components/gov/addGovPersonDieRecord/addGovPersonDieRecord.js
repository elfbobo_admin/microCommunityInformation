(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovPersonDieRecordInfo: {
                dieRecordId: '',
                dieId: '',
                govPersonId: '',
                title: '',
                typeCd: '',
                context: '',
                activityTime: '',
                caId: '',
                statusCd: '',
            }
        },
        _initMethod: function () {
            ////dieId govPersonId caId
            vc.component.addGovPersonDieRecordInfo.govPersonId = vc.getParam('govPersonId');
            vc.component.addGovPersonDieRecordInfo.dieId = vc.getParam('dieId');
            vc.component.addGovPersonDieRecordInfo.caId = vc.getParam('caId');
            $that._initContentInfo();
        },
        _initEvent: function () {
            vc.on('addGovPersonDieRecord', 'openAddGovPersonDieRecordModal', function () {
                $('#addGovPersonDieRecordModel').modal('show');
            });
        },
        methods: {
            addGovPersonDieRecordValidate() {
                return vc.validate.validate({
                    addGovPersonDieRecordInfo: vc.component.addGovPersonDieRecordInfo
                }, {
                    'addGovPersonDieRecordInfo.dieId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "记录ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "记录ID不能超过30"
                        },
                    ],
                    'addGovPersonDieRecordInfo.govPersonId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "人口ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "人口ID不能超过30"
                        },
                    ],
                    'addGovPersonDieRecordInfo.title': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "记录标题不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "200",
                            errInfo: "记录标题不能超过200"
                        },
                    ],
                   
                    'addGovPersonDieRecordInfo.context': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "记录内容不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "长文本",
                            errInfo: "记录内容不能超过长文本"
                        },
                    ],
                    'addGovPersonDieRecordInfo.activityTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "参加时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "参加时间不能超过时间类型"
                        },
                    ],
                    'addGovPersonDieRecordInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "区域ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "区域ID不能超过30"
                        },
                    ],
                });
            },
            saveGovPersonDieRecordInfo: function () {
                if (!vc.component.addGovPersonDieRecordValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }

                vc.component.addGovPersonDieRecordInfo.caId = vc.getCurrentCommunity().caId;
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovPersonDieRecordInfo);
                    $('#addGovPersonDieRecordModel').modal('hide');
                    return;
                }
                vc.http.apiPost(
                    '/govPersonDieRecord/saveGovPersonDieRecord',
                    JSON.stringify(vc.component.addGovPersonDieRecordInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovPersonDieRecordModel').modal('hide');
                            vc.component.clearAddGovPersonDieRecordInfo();
                            vc.emit('govPersonDieRecordManage', 'listGovPersonDieRecord', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            _initContentInfo: function () {
                vc.initDateTime('addActivityTime', function (_value) {
                    $that.addGovPersonDieRecordInfo.activityTime = _value;
                });
                var $summernote = $('.summernote').summernote({
                    lang: 'zh-CN',
                    height: 300,
                    placeholder: '必填，请输入公告内容',
                    callbacks: {
                        onImageUpload: function (files, editor, $editable) {
                            $that.sendAddFile($summernote,files[0]);
                        },
                        onChange: function (contents, $editable) {
                            vc.component.addGovPersonDieRecordInfo.context = contents;
                        }
                    },
                    toolbar: [
                        ['style', ['style']],
                        ['font', ['bold', 'italic', 'underline', 'clear']],
                        ['fontname', ['fontname']],
                        ['color', ['color']],
                        ['para', ['ul', 'ol', 'paragraph']],
                        ['height', ['height']],
                        ['table', ['table']],
                        ['insert', ['link', 'picture']],
                        ['view', ['fullscreen', 'codeview']],
                        ['help', ['help']]
                    ],
                });
            },
            sendAddFile: function ($summernote,files) {
                console.log('上传图片', files);

                var param = new FormData();
                param.append("uploadFile", files);
                vc.http.uploadImage(
                    param,
                    {
                        emulateJSON: true,
                        //添加请求头
                        headers: {
                            "Content-Type": "multipart/form-data"
                        }
                    },
                    function (json, res) {
                        if (res.status == 200) {
                            var data = JSON.parse(json);
                            $summernote.summernote('insertImage', data.data);
                            return;
                        }
                        vc.toast(json);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');
                        vc.toast(errInfo);
                    });

            },
            clearAddGovPersonDieRecordInfo: function () {
                vc.component.addGovPersonDieRecordInfo = {
                    dieRecordId: '',
                    dieId: '',
                    govPersonId: '',
                    title: '',
                    typeCd: '',
                    context: '',
                    activityTime: '',
                    caId: '',
                    statusCd: '',

                };
            }
        }
    });

})(window.vc);
