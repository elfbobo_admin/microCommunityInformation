(function(vc){
    vc.extends({
        propTypes: {
           emitChooseGovDoctorHealthy:vc.propTypes.string,
           emitLoadData:vc.propTypes.string
        },
        data:{
            chooseGovDoctorHealthyInfo:{
                govDoctorHealthys:[],
                _currentGovDoctorHealthyName:'',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('chooseGovDoctorHealthy','openChooseGovDoctorHealthyModel',function(_param){
                $('#chooseGovDoctorHealthyModel').modal('show');
                vc.component._refreshChooseGovDoctorHealthyInfo();
                vc.component._loadAllGovDoctorHealthyInfo(1,10,'');
            });
        },
        methods:{
            _loadAllGovDoctorHealthyInfo:function(_page,_row,_name){
                var param = {
                    params:{
                        page:_page,
                        row:_row,
                        caId:vc.getCurrentCommunity().caId,
                        name:_name
                    }
                };

                //发送get请求
               vc.http.apiGet('/govDoctorHealthy/queryGovDoctorHealthy',
                             param,
                             function(json){
                                var _govDoctorHealthyInfo = JSON.parse(json);
                                vc.component.chooseGovDoctorHealthyInfo.govDoctorHealthys = _govDoctorHealthyInfo.govDoctorHealthys;
                             },function(){
                                console.log('请求失败处理');
                             }
                           );
            },
            chooseGovDoctorHealthy:function(_govDoctorHealthy){
                if(_govDoctorHealthy.hasOwnProperty('name')){
                     _govDoctorHealthy.govDoctorHealthyName = _govDoctorHealthy.name;
                }
                vc.emit($props.emitChooseGovDoctorHealthy,'chooseGovDoctorHealthy',_govDoctorHealthy);
                vc.emit($props.emitLoadData,'listGovDoctorHealthyData',{
                    govDoctorHealthyId:_govDoctorHealthy.govDoctorHealthyId
                });
                $('#chooseGovDoctorHealthyModel').modal('hide');
            },
            queryGovDoctorHealthys:function(){
                vc.component._loadAllGovDoctorHealthyInfo(1,10,vc.component.chooseGovDoctorHealthyInfo._currentGovDoctorHealthyName);
            },
            _refreshChooseGovDoctorHealthyInfo:function(){
                vc.component.chooseGovDoctorHealthyInfo._currentGovDoctorHealthyName = "";
            }
        }

    });
})(window.vc);
