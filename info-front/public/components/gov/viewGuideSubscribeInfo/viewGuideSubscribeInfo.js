/**
    预约报名 组件
**/
(function(vc){

    vc.extends({
        propTypes: {
           callBackListener:vc.propTypes.string, //父组件名称
           callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            viewGuideSubscribeInfo:{
                index:0,
                flowComponent:'viewGuideSubscribeInfo',
                caId:'',
wgId:'',
person:'',
link:'',
startTime:'',
endTime:'',
state:'',

            }
        },
        _initMethod:function(){
            //根据请求参数查询 查询 业主信息
            vc.component._loadGuideSubscribeInfoData();
        },
        _initEvent:function(){
            vc.on('viewGuideSubscribeInfo','chooseGuideSubscribe',function(_app){
                vc.copyObject(_app, vc.component.viewGuideSubscribeInfo);
                vc.emit($props.callBackListener,$props.callBackFunction,vc.component.viewGuideSubscribeInfo);
            });

            vc.on('viewGuideSubscribeInfo', 'onIndex', function(_index){
                vc.component.viewGuideSubscribeInfo.index = _index;
            });

        },
        methods:{

            _openSelectGuideSubscribeInfoModel(){
                vc.emit('chooseGuideSubscribe','openChooseGuideSubscribeModel',{});
            },
            _openAddGuideSubscribeInfoModel(){
                vc.emit('addGuideSubscribe','openAddGuideSubscribeModal',{});
            },
            _loadGuideSubscribeInfoData:function(){

            }
        }
    });

})(window.vc);
