(function (vc, vm) {

    vc.extends({
        data: {
            editGovReportTypeUserInfo: {
                typeUserId: '',
                reportType: '',
                staffId: '',
                staffName: '',
                caId: '',
                ramark: '',

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('editGovReportTypeUser', 'openEditGovReportTypeUserModal', function (_params) {
                vc.component.refreshEditGovReportTypeUserInfo();
                $('#editGovReportTypeUserModel').modal('show');
                vc.copyObject(_params, vc.component.editGovReportTypeUserInfo);
                vc.component.editGovReportTypeUserInfo.communityId = vc.getCurrentCommunity().communityId;
            });
        },
        methods: {
            editGovReportTypeUserValidate: function () {
                return vc.validate.validate({
                    editGovReportTypeUserInfo: vc.component.editGovReportTypeUserInfo
                }, {
                    'editGovReportTypeUserInfo.reportType': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "报事类型不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "200",
                            errInfo: "报事类型名称超长"
                        },
                    ],
                    'editGovReportTypeUserInfo.staffId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "员工ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "员工ID超长"
                        },
                    ],
                    'editGovReportTypeUserInfo.staffName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "员工名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "员工名称超长"
                        },
                    ],
                    'editGovReportTypeUserInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "社区ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "社区ID超长"
                        },
                    ],
                    'editGovReportTypeUserInfo.ramark': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "备注不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "500",
                            errInfo: "备注太长"
                        },
                    ],
                    'editGovReportTypeUserInfo.typeUserId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "报事类型人员ID不能为空"
                        }]

                });
            },
            editGovReportTypeUser: function () {
                if (!vc.component.editGovReportTypeUserValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }

                vc.http.apiPost(
                    'govReportTypeUser.updateGovReportTypeUser',
                    JSON.stringify(vc.component.editGovReportTypeUserInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovReportTypeUserModel').modal('hide');
                            vc.emit('govReportTypeUserManage', 'listGovReportTypeUser', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            refreshEditGovReportTypeUserInfo: function () {
                vc.component.editGovReportTypeUserInfo = {
                    typeUserId: '',
                    reportType: '',
                    staffId: '',
                    staffName: '',
                    caId: '',
                    ramark: '',

                }
            }
        }
    });

})(window.vc, window.vc.component);
