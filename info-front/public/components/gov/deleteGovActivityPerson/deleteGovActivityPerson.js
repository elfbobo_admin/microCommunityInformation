(function(vc,vm){

    vc.extends({
        data:{
            deleteGovActivityPersonInfo:{

            }
        },
         _initMethod:function(){

         },
         _initEvent:function(){
             vc.on('deleteGovActivityPerson','openDeleteGovActivityPersonModal',function(_params){

                vc.component.deleteGovActivityPersonInfo = _params;
                $('#deleteGovActivityPersonModel').modal('show');

            });
        },
        methods:{
            deleteGovActivityPerson:function(){
                //vc.component.deleteGovActivityPersonInfo.communityId=vc.getCurrentCommunity().communityId;
                vc.http.apiPost(
                    '/govActivityPerson/deleteGovActivityPerson',
                    JSON.stringify(vc.component.deleteGovActivityPersonInfo),
                    {
                        emulateJSON:true
                     },
                     function(json,res){
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovActivityPersonModel').modal('hide');
                            vc.emit('govActivityPersonManage','listGovActivityPerson',{});
                            return ;
                        }
                        vc.message(_json.msg);
                     },
                     function(errInfo,error){
                        console.log('请求失败处理');
                        vc.message(json);

                     });
            },
            closeDeleteGovActivityPersonModel:function(){
                $('#deleteGovActivityPersonModel').modal('hide');
            }
        }
    });

})(window.vc,window.vc.component);
