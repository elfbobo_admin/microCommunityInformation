(function(vc,vm){

    vc.extends({
        data:{
            deleteGovReleasePrisonInfo:{

            }
        },
         _initMethod:function(){

         },
         _initEvent:function(){
             vc.on('deleteGovReleasePrison','openDeleteGovReleasePrisonModal',function(_params){

                vc.component.deleteGovReleasePrisonInfo = _params;
                $('#deleteGovReleasePrisonModel').modal('show');

            });
        },
        methods:{
            deleteGovReleasePrison:function(){
                vc.component.deleteGovReleasePrisonInfo.caId=vc.getCurrentCommunity().caId;
                vc.http.apiPost(
                    '/govReleasePrison/deleteGovReleasePrison',
                    JSON.stringify(vc.component.deleteGovReleasePrisonInfo),
                    {
                        emulateJSON:true
                     },
                     function(json,res){
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovReleasePrisonModel').modal('hide');
                            vc.emit('govReleasePrisonManage','listGovReleasePrison',{});
                            return ;
                        }
                        vc.message(_json.msg);
                     },
                     function(errInfo,error){
                        console.log('请求失败处理');
                        vc.message(json);

                     });
            },
            closeDeleteGovReleasePrisonModel:function(){
                $('#deleteGovReleasePrisonModel').modal('hide');
            }
        }
    });

})(window.vc,window.vc.component);
