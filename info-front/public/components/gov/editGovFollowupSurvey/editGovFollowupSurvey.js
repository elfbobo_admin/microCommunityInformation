(function (vc, vm) {

    vc.extends({
        data: {
            editGovFollowupSurveyInfo: {
                surveyId: '',
                surveyId: '',
                surveyTime: '',
                govPersonId: '',
                govPersonName: '',
                surveyWay: '',
                symptoms: '',
                symptomsSel: [],
                caId: '',
                lifeStyleGuide: '',
                drugCompliance: '',
                adrs: '',
                surveyType: '',
                medication: '',
                referralReason: '',
                referralDepartment: '',
                surveyAdvice: '',
                surveyConclusion: '',
                nextSurveyTime: '',
                surveyDoctor: '',
                surveyDoctorId: '',
                remark: '',
            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('editGovFollowupSurvey', 'openEditGovFollowupSurveyModal', function (_params) {
                vc.component.refreshEditGovFollowupSurveyInfo();
                $('#editGovFollowupSurveyModel').modal('show');
                vc.copyObject(_params, vc.component.editGovFollowupSurveyInfo);
                $that.editGovFollowupSurveyInfo.symptomsSel = $that.editGovFollowupSurveyInfo.symptoms.split(',');
                vc.component.editGovFollowupSurveyInfo.caId = vc.getCurrentCommunity().caId;
            });
            vc.on('openChooseGovPerson', 'chooseGovPerson', function (_param) {
                $that.editGovFollowupSurveyInfo.govPersonId = _param.govPersonId;
                $that.editGovFollowupSurveyInfo.govPersonName = _param.personName;
            });
        },
        methods: {
            editGovFollowupSurveyValidate: function () {
                return vc.validate.validate({
                    editGovFollowupSurveyInfo: vc.component.editGovFollowupSurveyInfo
                }, {
                    'editGovFollowupSurveyInfo.surveyTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "随访时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "随访时间不能超过时间类型"
                        },
                    ],
                    'editGovFollowupSurveyInfo.govPersonId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "人口ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "人口ID不能超过30"
                        },
                    ],
                    'editGovFollowupSurveyInfo.surveyWay': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "随访方式：1001门诊不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "4",
                            errInfo: "随访方式：1001门诊不能超过4"
                        },
                    ],
                    'editGovFollowupSurveyInfo.symptoms': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "症状不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "500",
                            errInfo: "症状不能超过500"
                        },
                    ],
                    'editGovFollowupSurveyInfo.lifeStyleGuide': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "生活方式指导不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "2000",
                            errInfo: "生活方式指导不能超过2000"
                        },
                    ],
                    'editGovFollowupSurveyInfo.drugCompliance': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "服药依从性1001规律不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "服药依从性1001规律不能超过12"
                        },
                    ],
                    'editGovFollowupSurveyInfo.adrs': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "药物不良反应不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "2",
                            errInfo: "药物不良反应不能超过2"
                        },
                    ],
                    'editGovFollowupSurveyInfo.surveyType': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "随访分类：1001控制满意不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "4",
                            errInfo: "随访分类：1001控制满意不能超过4"
                        },
                    ],
                    'editGovFollowupSurveyInfo.medication': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "用药情况不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "2000",
                            errInfo: "用药情况不能超过2000"
                        },
                    ],
                    'editGovFollowupSurveyInfo.referralReason': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "转诊原因不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "200",
                            errInfo: "转诊原因不能超过200"
                        },
                    ],
                    'editGovFollowupSurveyInfo.referralDepartment': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "机构及科别不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "200",
                            errInfo: "机构及科别不能超过200"
                        },
                    ],
                    'editGovFollowupSurveyInfo.surveyAdvice': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "随访建议不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "2000",
                            errInfo: "随访建议不能超过2000"
                        },
                    ],
                    'editGovFollowupSurveyInfo.surveyConclusion': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "随访结论不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "2000",
                            errInfo: "随访结论不能超过2000"
                        },
                    ],
                    'editGovFollowupSurveyInfo.nextSurveyTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "下次随访时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "下次随访时间不能超过时间类型"
                        },
                    ],
                    'editGovFollowupSurveyInfo.surveyDoctor': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "随访医生不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "100",
                            errInfo: "随访医生不能超过100"
                        },
                    ],
                    'editGovFollowupSurveyInfo.remark': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "备注不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "备注不能超过1024"
                        },
                    ]

                });
            },
            editGovFollowupSurvey: function () {
                $that.editGovFollowupSurveyInfo.symptoms = vc.component.editGovFollowupSurveyInfo.symptomsSel.toString();
                if (!vc.component.editGovFollowupSurveyValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }

                vc.http.apiPost(
                    '/govFollowupSurvey/updateGovFollowupSurvey',
                    JSON.stringify(vc.component.editGovFollowupSurveyInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovFollowupSurveyModel').modal('hide');
                            vc.emit('govFollowupSurveyManage', 'listGovFollowupSurvey', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            _closeEditGovFollowupSurvey: function () {
                $that.refreshEditGovFollowupSurveyInfo();
                vc.emit('govFollowupSurveyManage', 'listGovFollowupSurvey', {});
            },
            refreshEditGovFollowupSurveyInfo: function () {
                vc.component.editGovFollowupSurveyInfo = {
                    surveyId: '',
                    surveyId: '',
                    surveyTime: '',
                    govPersonId: '',
                    govPersonName: '',
                    surveyWay: '',
                    symptoms: '',
                    symptomsSel: [],
                    caId: '',
                    lifeStyleGuide: '',
                    drugCompliance: '',
                    adrs: '',
                    surveyType: '',
                    medication: '',
                    referralReason: '',
                    referralDepartment: '',
                    surveyAdvice: '',
                    surveyConclusion: '',
                    nextSurveyTime: '',
                    surveyDoctor: '',
                    surveyDoctorId: '',
                    remark: '',
                }
            }
        }
    });

})(window.vc, window.vc.component);
