(function(vc){
    vc.extends({
        propTypes: {
           emitChooseGovReportTypeUser:vc.propTypes.string,
           emitLoadData:vc.propTypes.string
        },
        data:{
            chooseGovReportTypeUserInfo:{
                govReportTypeUsers:[],
                _currentGovReportTypeUserName:'',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('chooseGovReportTypeUser','openChooseGovReportTypeUserModel',function(_param){
                $('#chooseGovReportTypeUserModel').modal('show');
                vc.component._refreshChooseGovReportTypeUserInfo();
                vc.component._loadAllGovReportTypeUserInfo(1,10,'');
            });
        },
        methods:{
            _loadAllGovReportTypeUserInfo:function(_page,_row,_name){
                var param = {
                    params:{
                        page:_page,
                        row:_row,
                        communityId:vc.getCurrentCommunity().communityId,
                        name:_name
                    }
                };

                //发送get请求
               vc.http.apiGet('govReportTypeUser.listGovReportTypeUsers',
                             param,
                             function(json){
                                var _govReportTypeUserInfo = JSON.parse(json);
                                vc.component.chooseGovReportTypeUserInfo.govReportTypeUsers = _govReportTypeUserInfo.govReportTypeUsers;
                             },function(){
                                console.log('请求失败处理');
                             }
                           );
            },
            chooseGovReportTypeUser:function(_govReportTypeUser){
                if(_govReportTypeUser.hasOwnProperty('name')){
                     _govReportTypeUser.govReportTypeUserName = _govReportTypeUser.name;
                }
                vc.emit($props.emitChooseGovReportTypeUser,'chooseGovReportTypeUser',_govReportTypeUser);
                vc.emit($props.emitLoadData,'listGovReportTypeUserData',{
                    govReportTypeUserId:_govReportTypeUser.govReportTypeUserId
                });
                $('#chooseGovReportTypeUserModel').modal('hide');
            },
            queryGovReportTypeUsers:function(){
                vc.component._loadAllGovReportTypeUserInfo(1,10,vc.component.chooseGovReportTypeUserInfo._currentGovReportTypeUserName);
            },
            _refreshChooseGovReportTypeUserInfo:function(){
                vc.component.chooseGovReportTypeUserInfo._currentGovReportTypeUserName = "";
            }
        }

    });
})(window.vc);
