(function (vc, vm) {

    vc.extends({
        data: {
            editGovOldPersonInfo: {
                oldId: '',
                caId: '',
                govCommunityId: '',
                govPersonId: '',
                personName: '',
                personTel: '',
                personAge: '',
                contactPerson: '',
                contactTel: '',
                servPerson: '',
                servTel: '',
                birthday: '',
                ramark: '',
                idCard: '',
                personSex: '',
                nation: '',
                maritalStatus: 'Y',
                timeAmount: '0',
                typeId: '',
                govCommunitys: [],
                govOldPersonTypes: [],
                nationData : [
                    {id:1 ,name:'汉族'},
                    {id:2 ,name:'蒙古族'},
                    {id:3 ,name:'回族'},
                    {id:4 ,name:'藏族'},
                    {id:5 ,name:'维吾尔族'},
                    {id:6 ,name:'苗族'},
                    {id:7 ,name:'彝族'},
                    {id:8 ,name:'壮族'},
                    {id:9 ,name:'布依族'},
                    {id:10,name:'朝鲜族'},
                    {id:11,name:'满族'},
                    {id:12,name:'侗族'},
                    {id:13,name:'瑶族'},
                    {id:14,name:'白族'},
                    {id:15,name:'土家族'},
                    {id:16,name:'哈尼族'},
                    {id:17,name:'哈萨克族'},
                    {id:18,name:'傣族'},
                    {id:19,name:'黎族'},
                    {id:20,name:'傈僳族'},
                    {id:21,name:'佤族'},
                    {id:22,name:'畲族'},
                    {id:23,name:'高山族'},
                    {id:24,name:'拉祜族'},
                    {id:25,name:'水族'},
                    {id:26,name:'东乡族'},
                    {id:27,name:'纳西族'},
                    {id:28,name:'景颇族'},
                    {id:29,name:'柯尔克孜族'},
                    {id:30,name:'土族'},
                    {id:31,name:'达翰尔族'},
                    {id:32,name:'么佬族'},
                    {id:33,name:'羌族'},
                    {id:34,name:'布朗族'},
                    {id:35,name:'撒拉族'},
                    {id:36,name:'毛南族'},
                    {id:37,name:'仡佬族'},
                    {id:38,name:'锡伯族'},
                    {id:39,name:'阿昌族'},
                    {id:40,name:'普米族'},
                    {id:41,name:'塔吉克族'},
                    {id:42,name:'怒族'},
                    {id:43,name:'乌孜别克族'},
                    {id:44,name:'俄罗斯族'},
                    {id:45,name:'鄂温克族'},
                    {id:46,name:'德昂族'},
                    {id:47,name:'保安族'},
                    {id:48,name:'裕固族'},
                    {id:49,name:'京族'},
                    {id:50,name:'塔塔尔族'},
                    {id:51,name:'独龙族'},
                    {id:52,name:'鄂伦春族'},
                    {id:53,name:'赫哲族'},
                    {id:54,name:'门巴族'},
                    {id:55,name:'珞巴族'},
                    {id:56,name:'基诺族'},
            
            ]
            }
        },
        _initMethod: function () {
            vc.initDate('editBirthday', function (_value) {
                $that.editGovOldPersonInfo.birthday = _value;
            });
            $that._editGovCommunitys(vc.getCurrentCommunity().caId);
            $that._editGovOldPersonTypes();
        },
        _initEvent: function () {
            vc.on('editGovOldPerson', 'openEditGovOldPersonModal', function (_params) {
                vc.component.refreshEditGovOldPersonInfo();
                $('#editGovOldPersonModel').modal('show');
                vc.copyObject(_params, vc.component.editGovOldPersonInfo);
                vc.component.editGovOldPersonInfo.caId = vc.getCurrentCommunity().caId;
            });
        },
        methods: {
            editGovOldPersonValidate: function () {
                return vc.validate.validate({
                    editGovOldPersonInfo: vc.component.editGovOldPersonInfo
                }, {

                    'editGovOldPersonInfo.govCommunityId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "小区ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "小区ID不能超过30"
                        },
                    ],
                    'editGovOldPersonInfo.personName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "姓名不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "姓名不能超过64"
                        },
                    ],
                    'editGovOldPersonInfo.personTel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "电话不能为空"
                        },
                        {
                            limit: "phone",
                            param: "",
                            errInfo: "电话不能超过11"
                        },
                    ],
                    'editGovOldPersonInfo.personAge': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "年龄不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "年龄不能超过11"
                        },
                    ],
                    'editGovOldPersonInfo.contactPerson': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "家人联系人不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "家人联系人不能超过64"
                        },
                    ],
                    'editGovOldPersonInfo.contactTel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "家人联系电话不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "家人联系电话不能超过11"
                        },
                    ],
                    'editGovOldPersonInfo.servPerson': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "服务人不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "服务人不能超过64"
                        },
                    ],
                    'editGovOldPersonInfo.servTel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "服务人电话不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "服务人电话不能超过11"
                        },
                    ],
                    'editGovOldPersonInfo.idCard': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "身份证号码不能为空"
                        }
                    ],
                    'editGovOldPersonInfo.personSex': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "性别不能为空"
                        }
                    ],
                    'editGovOldPersonInfo.nation': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "民族不能为空"
                        }
                    ],
                    'editGovOldPersonInfo.timeAmount': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "初始时间不能为空"
                        },
                        {
                            limit: "num",
                            param: "",
                            errInfo: "初始时间格式错误"
                        },
                    ]


                });
            },
            _editGovCommunitys: function (_caId) {

                let param = {
                    params: {
                        page: 1,
                        row: 50,
                        caId: _caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govCommunity/queryGovCommunity',
                    param,
                    function (json, res) {
                        var _govCommunityManageInfo = JSON.parse(json);
                        vc.component.editGovOldPersonInfo.total = _govCommunityManageInfo.total;
                        vc.component.editGovOldPersonInfo.records = _govCommunityManageInfo.records;
                        vc.component.editGovOldPersonInfo.govCommunitys = _govCommunityManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _editGovOldPersonTypes: function () {

                let param = {
                    params: {
                        page: 1,
                        row: 50,
                        caId: vc.getCurrentCommunity().caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govOldPersonType/queryGovOldPersonType',
                    param,
                    function (json, res) {
                        var _govOldPersonTypeManageInfo = JSON.parse(json);
                        vc.component.editGovOldPersonInfo.total = _govOldPersonTypeManageInfo.total;
                        vc.component.editGovOldPersonInfo.records = _govOldPersonTypeManageInfo.records;
                        vc.component.editGovOldPersonInfo.govOldPersonTypes = _govOldPersonTypeManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            isCardNoEdit: function (card) {
                //身份证号码为15位或者18位，15位时全为数字，18位前17位为数字，最后一位是校验位，可能为数字或字符X
                var reg = /(^\d{15}$)|(^\d{17}(\d|X)$)/;
                if (reg.test(card) === false) {
                    return false;
                }
                return true;
            },
            editGovOldPerson: function () {
                if (!vc.component.editGovOldPersonValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }
                if(!vc.component.isCardNoEdit(vc.component.editGovOldPersonInfo.idCard)){
                    vc.toast("身份证格式有误，请检查身份证信息");

                    return;
                }
                vc.http.apiPost(
                    '/govOldPerson/updateGovOldPerson',
                    JSON.stringify(vc.component.editGovOldPersonInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovOldPersonModel').modal('hide');
                            vc.emit('govOldPersonManage', 'listGovOldPerson', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            refreshEditGovOldPersonInfo: function () {
                let _govCommunitys = vc.component.editGovOldPersonInfo.govCommunitys;
                let _govOldPersonTypes = vc.component.editGovOldPersonInfo.govOldPersonTypes;
                vc.component.editGovOldPersonInfo = {
                    oldId: '',
                    caId: '',
                    govCommunityId: '',
                    govPersonId: '',
                    personName: '',
                    personTel: '',
                    personAge: '',
                    contactPerson: '',
                    contactTel: '',
                    servPerson: '',
                    servTel: '',
                    ramark: '',
                    idCard: '',
                    personSex: '',
                    nation: '',
                    maritalStatus: 'Y',
                    timeAmount: '0',
                    typeId: '',
                    govCommunitys: _govCommunitys,
                    govOldPersonTypes: _govOldPersonTypes
                }
            }
        }
    });

})(window.vc, window.vc.component);
