(function (vc, vm) {

    vc.extends({
        data: {
            editGovHelpPolicyListInfo: {
                govHelpListId: '',
                listName: '',
                caId: '',
                govHelpId: '',
                helpName: '',
                poorPersonId: '',
                poorPersonName: '',
                cadrePersonId: '',
                cadrePersonName: '',
                startTime: '',
                endTime: '',
                listBrief: '',
                govHelpPolicys: []
            }
        },
        _initMethod: function () {
            vc.initDateTime('startTimes', function (_value) {
                $that.editGovHelpPolicyListInfo.startTime = _value;
            });
            vc.initDateTime('endTimes', function (_value) {
                $that.editGovHelpPolicyListInfo.endTime = _value;
            });
            $that._initEditNoticeInfo();
            $that._listEditGovHelpPolicys();
        },
        _initEvent: function () {
            vc.on('editGovHelpPolicyList', 'openEditGovHelpPolicyListModal', function (_params) {
                vc.component.refreshEditGovHelpPolicyListInfo();
                $('#editGovHelpPolicyListModel').modal('show');
                vc.copyObject(_params, vc.component.editGovHelpPolicyListInfo);
                vc.component.editGovHelpPolicyListInfo.caId = vc.getCurrentCommunity().caId;
                $(".eidtSummernote").summernote('code', vc.component.editGovHelpPolicyListInfo.listBrief);
            });
        },
        methods: {
            editGovHelpPolicyListValidate: function () {
                return vc.validate.validate({
                    editGovHelpPolicyListInfo: vc.component.editGovHelpPolicyListInfo
                }, {
                    'editGovHelpPolicyListInfo.govHelpListId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "帮扶记录ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "帮扶记录ID不能超过30"
                        },
                    ],
                    'editGovHelpPolicyListInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "区域ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "区域ID不能超过30"
                        },
                    ],
                    'editGovHelpPolicyListInfo.govHelpId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "政策ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "政策ID不能超过64"
                        },
                    ],
                    'editGovHelpPolicyListInfo.helpName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "政策名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "政策名称不能超过64"
                        },
                    ],
                    'editGovHelpPolicyListInfo.poorPersonId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "贫困人员ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "贫困人员ID不能超过30"
                        },
                    ],
                    'editGovHelpPolicyListInfo.poorPersonName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "贫困人员不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "贫困人员不能超过64"
                        },
                    ],
                    'editGovHelpPolicyListInfo.cadrePersonId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "扶贫干部ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "扶贫干部ID不能超过30"
                        },
                    ],
                    'editGovHelpPolicyListInfo.cadrePersonName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "扶贫干部不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "扶贫干部不能超过64"
                        },
                    ],
                    'editGovHelpPolicyListInfo.startTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "帮扶开始时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "帮扶开始时间不能超过时间类型"
                        },
                    ],
                    'editGovHelpPolicyListInfo.endTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "帮扶结束时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "帮扶结束时间不能超过时间类型"
                        },
                    ],
                    'editGovHelpPolicyListInfo.listBrief': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "帮扶内容不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "帮扶内容不能超过1024"
                        },
                    ],
                    'editGovHelpPolicyListInfo.listName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "记录名称不能为空"
                        }]

                });
            },
            editGovHelpPolicyList: function () {
                if (!vc.component.editGovHelpPolicyListValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }

                vc.http.apiPost(
                    '/govHelpPolicyList/updateGovHelpPolicyList',
                    JSON.stringify(vc.component.editGovHelpPolicyListInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovHelpPolicyListModel').modal('hide');
                            vc.emit('govHelpPolicyListManage', 'listGovHelpPolicyList', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            _listEditGovHelpPolicys: function () {
                var param = {
                    params: {
                        caId: vc.getCurrentCommunity().caId,
                        page: 1,
                        row: 50
                    }
                };

                //发送get请求
                vc.http.apiGet('/govHelpPolicy/queryGovHelpPolicy',
                    param,
                    function (json, res) {
                        var _govHelpPolicyManageInfo = JSON.parse(json);
                        vc.component.editGovHelpPolicyListInfo.govHelpPolicys = _govHelpPolicyManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _initEditNoticeInfo: function () {

                var $summernote = $('.eidtSummernote').summernote({
                    lang: 'zh-CN',
                    height: 300,
                    placeholder: '必填，请输入政策内容',
                    callbacks: {
                        onImageUpload: function (files, editor, $editable) {
                            $that.sendAddFile($summernote, files[0]);
                        },
                        onChange: function (contents, $editable) {
                            vc.component.editGovHelpPolicyListInfo.listBrief = contents;
                        }
                    },
                    toolbar: [
                        ['style', ['style']],
                        ['font', ['bold', 'italic', 'underline', 'clear']],
                        ['fontname', ['fontname']],
                        ['color', ['color']],
                        ['para', ['ul', 'ol', 'paragraph']],
                        ['height', ['height']],
                        ['table', ['table']],
                        ['insert', ['link', 'picture']],
                        ['view', ['fullscreen', 'codeview']],
                        ['help', ['help']]
                    ],
                });

            },
            sendAddFile: function ($summernote, files) {
                console.log('上传图片', files);

                var param = new FormData();
                param.append("uploadFile", files);
                vc.http.uploadImage(
                    param,
                    {
                        emulateJSON: true,
                        //添加请求头
                        headers: {
                            "Content-Type": "multipart/form-data"
                        }
                    },
                    function (json, res) {
                        if (res.status == 200) {
                            var data = JSON.parse(json);
                            $summernote.summernote('insertImage', data.data);
                            return;
                        }
                        vc.toast(json);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');
                        vc.toast(errInfo);
                    });

            },
            _openEditchooseGovPersonPolicyModel: function () {
                vc.component.editGovHelpPolicyListInfo.caId = vc.getCurrentCommunity().caId;
                var param = {
                    editGovHelpPolicyListInfo: $that.editGovHelpPolicyListInfo,
                    labelCd: '6009'
                }
                vc.emit('chooseGovPersonHelpPolicy', 'openchooseGovPersonHelpPolicyModel', param);
            },
            _openEditchooseGovPersonHelpModel: function () {
                vc.component.editGovHelpPolicyListInfo.caId = vc.getCurrentCommunity().caId;
                var param = {
                    editGovHelpPolicyListInfo: $that.editGovHelpPolicyListInfo,
                    labelCd: '6010'
                }
                vc.emit('chooseGovPersonHelpPolicy', 'openchooseGovPersonHelpPolicyModel', param);
            },
            setEditHelpName: function(_govHelpId){
                $that.editGovHelpPolicyListInfo.govHelpPolicys.forEach(element => {
                    if(element.govHelpId == _govHelpId){
                        $that.editGovHelpPolicyListInfo.helpName = element.helpName;
                    }
                });
            },
            closeEdiGovHelpPolicyList: function () {
                $that.refreshEditGovHelpPolicyListInfo();
                vc.emit('govHelpPolicyListManage', 'listGovHelpPolicyList', {});

            },
            refreshEditGovHelpPolicyListInfo: function () {
                let _govHelpPolicys = vc.component.editGovHelpPolicyListInfo.govHelpPolicys;
                vc.component.editGovHelpPolicyListInfo = {
                    govHelpListId: '',
                    listName: '',
                    caId: '',
                    govHelpId: '',
                    helpName: '',
                    poorPersonId: '',
                    poorPersonName: '',
                    cadrePersonId: '',
                    cadrePersonName: '',
                    startTime: '',
                    endTime: '',
                    listBrief: '',
                    govHelpPolicys: _govHelpPolicys
                }
            }
        }
    });

})(window.vc, window.vc.component);
