(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovRoomInfo: {
                govRoomId: '',
                caId: '',
                govCommunityId: '',
                govFloorId: '',
                roomNum: '',
                roomType: '',
                roomArea: '',
                layer: '',
                roomAddress: '',
                roomRight: '',
                ownerId: '9999',
                ownerName: '',
                ownerTel: '',
                isConservati: '',
                isSettle: '',
                ramark: '',
                govCommunityAreas: [],
                govCommunitys: [],
                govFloors: []

            }
        },
        _initMethod: function () {

         
        },
        _initEvent: function () {
            vc.on('addGovRoom', 'openAddGovRoomModal', function () {
                vc.component.addGovRoomInfo.caId = vc.getCurrentCommunity().caId;
                $that._listAddGovCommunitys(vc.getCurrentCommunity().caId);
                $('#addGovRoomModel').modal('show');
            });
            vc.on('openChooseGovOwner', 'chooseGovOwner', function (_param) {
                console.log(_param);
                $that.addGovRoomInfo.ownerId = _param.govOwnerId;
                $that.addGovRoomInfo.ownerName = _param.ownerName;
                $that.addGovRoomInfo.ownerTel = _param.ownerTel;
            });
        },
        methods: {
            addGovRoomValidate() {
                return vc.validate.validate({
                    addGovRoomInfo: vc.component.addGovRoomInfo
                }, {
                    'addGovRoomInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "所属区域不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "所属区域超长"
                        },
                    ],
                    'addGovRoomInfo.govCommunityId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "所属小区不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "所属小区超长"
                        },
                    ],
                    'addGovRoomInfo.govFloorId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "所属建筑物不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "所属建筑物超长"
                        },
                    ],
                    'addGovRoomInfo.roomNum': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "房屋编号不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "房屋编号超长"
                        },
                    ],
                    'addGovRoomInfo.roomType': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "区域地址不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "6",
                            errInfo: "区域地址超长"
                        },
                    ],
                    'addGovRoomInfo.roomArea': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "房屋面积不能为空"
                        },
                        {
                            limit: "num",
                            param: "",
                            errInfo: "房屋面积格式错误"
                        },
                    ],
                    'addGovRoomInfo.layer': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "楼层不能为空"
                        },
                        {
                            limit: "num",
                            param: "",
                            errInfo: "楼层格式错误"
                        },
                    ],
                    'addGovRoomInfo.roomAddress': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "房屋地址不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "251",
                            errInfo: "房屋地址太长"
                        },
                    ],
                    'addGovRoomInfo.roomRight': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "房屋产权不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "房屋产权超长"
                        },
                    ],
                    'addGovRoomInfo.ownerId': [
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "房主ID超长"
                        },
                    ],
                    'addGovRoomInfo.ownerName': [
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "房主超长"
                        },
                    ],
                    'addGovRoomInfo.isConservati': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "是否常住不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "3",
                            errInfo: "是否常住超长了"
                        },
                    ],
                    'addGovRoomInfo.isSettle': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "是否落户不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "3",
                            errInfo: "是否落户超长了"
                        },
                    ],
                    'addGovRoomInfo.ramark': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "备注不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "备注太长"
                        },
                    ],




                });
            },
            saveGovRoomInfo: function () {
                if (!vc.component.addGovRoomValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }

                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovRoomInfo);
                    $('#addGovRoomModel').modal('hide');
                    return;
                }

                vc.http.apiPost(
                    '/govRoom/saveGovRoom',
                    JSON.stringify(vc.component.addGovRoomInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovRoomModel').modal('hide');
                            vc.component.clearAddGovRoomInfo();
                            vc.emit('govRoomManage', 'listGovRoom', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            _listAddGovCommunitys: function (_caId) {

                let param = {
                    params: {
                        page: 1,
                        row: 50,
                        caId: _caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govCommunity/queryGovCommunity',
                    param,
                    function (json, res) {
                        var _govCommunityManageInfo = JSON.parse(json);
                        vc.component.addGovRoomInfo.total = _govCommunityManageInfo.total;
                        vc.component.addGovRoomInfo.records = _govCommunityManageInfo.records;
                        vc.component.addGovRoomInfo.govCommunitys = _govCommunityManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _listAddGovFloors: function (_caId,_GovCommunityId) {

                let param = {
                    params: {
                        page: 1,
                        row: 50,
                        caId: _caId,
                        govCommunityId: _GovCommunityId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govFloor/queryGovFloor',
                    param,
                    function (json, res) {
                        var _govFloorManageInfo = JSON.parse(json);
                        vc.component.addGovRoomInfo.total = _govFloorManageInfo.total;
                        vc.component.addGovRoomInfo.records = _govFloorManageInfo.records;
                        vc.component.addGovRoomInfo.govFloors = _govFloorManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _setOwner:function(_ownerId){
                vc.component.addGovRoomInfo.ownerId=_ownerId;
            },
            _openAddChooseGovOwnerModel:function(){
                vc.emit('chooseGovOwner', 'openChooseGovOwnerModel', {});
            },
            _closeAddGovOwnerModel: function () {
                $that.clearAddGovRoomInfo();
                vc.emit('govRoomManage', 'listGovRoom', {});
            },
            clearAddGovRoomInfo: function () {
                vc.component.addGovRoomInfo = {
                    caId: '',
                    govCommunityId: '',
                    govFloorId: '',
                    roomNum: '',
                    roomType: '',
                    roomArea: '',
                    layer: '',
                    roomAddress: '',
                    roomRight: '',
                    ownerId: '',
                    ownerName: '',
                    ownerTel: '',
                    isConservati: '',
                    isSettle: '',
                    ramark: '',
                    govCommunityAreas: [],
                    govCommunitys: [],
                    govFloors: []

                };
            }
        }
    });

})(window.vc);
