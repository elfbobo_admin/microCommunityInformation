(function(vc){
    let DEFAULT_PAGE = 1;
    let DEFAULT_ROW = 10;
    vc.extends({
        data:{
            govCommunityAreaInfo: {
                govCommunityAreas: [],
                errorInfo: '',
                caName: '',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('chooseEnterShop','openChooseEnterShopModel',function(_param){
                $('#chooseEnterShopModel').modal('show');
                $that.govCommunityAreaInfo.caName = '';
                $that._chooseCommunityAreas(DEFAULT_PAGE, DEFAULT_ROW);
            });
            vc.on('chooseEnterShop','paginationPlus', 'page_event', function (_currentPage) {
                vc.component._chooseCommunityAreas(_currentPage, DEFAULT_ROW);
            });
        },
        methods:{
            _chooseCommunityAreas: function (_page, _rows) {

                var param = {
                    params: {
                        page: _page,
                        row: _rows,
                        caName: $that.govCommunityAreaInfo.caName
                    }
                };

                //发送get请求
                vc.http.apiGet('/govCommunityArea/queryGovCommunityAreaStaff',
                    param,
                    function (json, res) {
                        var _govCommunityAreaInfo = JSON.parse(json);
                        vc.component.govCommunityAreaInfo.total = _govCommunityAreaInfo.total;
                        vc.component.govCommunityAreaInfo.records = _govCommunityAreaInfo.records;
                        vc.component.govCommunityAreaInfo.govCommunityAreas = _govCommunityAreaInfo.data;
                        vc.emit('chooseEnterShop','paginationPlus', 'init', {
                            total: vc.component.govCommunityAreaInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _chooseCurrentShop: function (_govCommunityArea) {
                vc.setCurrentCommunity(_govCommunityArea);
                //vm.govCommunityAreaInfo._currentShop = _currentShop;
                //中心加载当前页
                location.reload();
            },
            _queryEnterShop: function () {
                $that._chooseCommunityAreas(DEFAULT_PAGE, DEFAULT_ROW)
            }
        }

    });
})(window.vc);
