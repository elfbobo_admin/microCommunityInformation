(function (vc) {

    vc.extends({
        data: {
            producerToolInfo: {
                topic: '',
                communitySecure: '',
                data: '',
                dataOut: ''
            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
          
        },
        methods: {
            producerToolValidate() {
                return vc.validate.validate({
                    producerToolInfo: vc.component.producerToolInfo
                }, {
                    'producerToolInfo.topic': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "topic名称不能为空"
                        }
                    ],
                    'producerToolInfo.communitySecure': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "秘钥不能为空"
                        },
                    ],
                    'producerToolInfo.data': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "数据包不能为空"
                        },
                    ]

                });
            },
            sendProducerToolInfo: function () {
                if (!vc.component.producerToolValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }

                vc.http.apiPost(
                    '/kafka/producer',
                    JSON.stringify(vc.component.producerToolInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        if (res.status == 200) {
                            //关闭model
                            vc.component.producerToolInfo.dataOut=json.data;
                            return;
                        }
                        vc.toast(json);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.toast(errInfo);

                    });
            },
            clearproducerToolInfo: function () {
                vc.component.producerToolInfo = {
                    topic: '',
                    communitySecure: '',
                    data: '',
                    dataOut:''
                };
            }
        }
    });

})(window.vc);
