(function (vc) {
    vc.extends({
        data: {
            finishReportPoolInfo: {
                reportId: '',
                ruId: '',
                context: '',
                fees: [],
                beforeRepairPhotos: [],
                afterRepairPhotos: [],
            }
        },
        _initMethod: function () {
            $that.finishReportPoolInfo.reportId = vc.getParam('reportId');
            $that.finishReportPoolInfo.ruId = vc.getParam('ruId');
        },
        _initEvent: function () {
            
        },
        methods: {
            finishReportPoolValidate() {
                return vc.validate.validate({
                    finishReportPoolInfo: vc.component.finishReportPoolInfo
                }, {
                    'finishReportPoolInfo.reportId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "报修单不能为空"
                        }
                    ],
                    'finishReportPoolInfo.context': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "处理意见不能为空"
                        }
                    ]
                });
            },
            _finishReportPoolInfo: function () {
                if (!vc.component.finishReportPoolValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }

                let _fees = [];
                $that.finishReportPoolInfo.fees.forEach(item =>{
                    if(item.state == '1001' && !item.cartId){
                        _fees.push(item);
                    }
                })

                let _data = {
                    caId:vc.getCurrentCommunity().caId,
                    reportId:vc.component.finishReportPoolInfo.reportId,
                    context:vc.component.finishReportPoolInfo.context
                }

                vc.http.apiPost(
                    '/govReportUser/poolFinish',
                    JSON.stringify(_data),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            $that._back();
                            return;
                        }
                        vc.toast(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');
                        vc.toast(errInfo);
                    }
                );
            },
            // 返回
            _back: function () {
                vc.goBack();
            },
            clearFinishRepairInfo: function () {
                vc.component.finishReportPoolInfo = {
                    reportId: '',
                    ruId: '',
                    repairType: '',
                    context: '',
                    feeFlag: '200',
                    repairObjType: '',
                    publicArea: '',
                    repairChannel: '',
                    maintenanceTypes: [],
                    maintenanceType: '',
                    totalPrice: 0,
                    choosedGoodsList: []
                };
            }
        }
    });
})(window.vc);
