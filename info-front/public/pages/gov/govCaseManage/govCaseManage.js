/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govCaseManageInfo: {
                govCases: [],
                total: 0,
                records: 1,
                moreCondition: false,
                caId: '',
                conditions: {
                    caseId: '',
                    govVictimId: '',
                    victimName: '',
                    caseName: '',
                    caId: '',
                    caseNum: '',
                    happenTime: '',
                    govSuspectId: '',
                    suspectName: '',
                    settleTime: '',
                    statusCd: '',

                }
            }
        },
        _initMethod: function () {
            vc.component._listGovCases(DEFAULT_PAGE, DEFAULT_ROWS);
        },
        _initEvent: function () {

            vc.on('govCaseManage', 'listGovCase', function (_param) {
                vc.component._listGovCases(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovCases(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovCases: function (_page, _rows) {

                vc.component.govCaseManageInfo.conditions.page = _page;
                vc.component.govCaseManageInfo.conditions.row = _rows;
                vc.component.govCaseManageInfo.conditions.caId = vc.getCurrentCommunity().caId;
                var param = {
                    params: vc.component.govCaseManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govCase/queryGovCase',
                    param,
                    function (json, res) {
                        var _govCaseManageInfo = JSON.parse(json);
                        vc.component.govCaseManageInfo.total = _govCaseManageInfo.total;
                        vc.component.govCaseManageInfo.records = _govCaseManageInfo.records;
                        vc.component.govCaseManageInfo.govCases = _govCaseManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govCaseManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddGovCaseModal: function () {
                vc.emit('addGovCase', 'openAddGovCaseModal', {});
            },
            _openEditGovCaseModel: function (_govCase) {
                vc.emit('editGovCase', 'openEditGovCaseModal', _govCase);
            },
            _openDeleteGovCaseModel: function (_govCase) {
                vc.emit('deleteGovCase', 'openDeleteGovCaseModal', _govCase);
            },
            _queryGovCaseMethod: function () {
                vc.component._listGovCases(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition: function () {
                if (vc.component.govCaseManageInfo.moreCondition) {
                    vc.component.govCaseManageInfo.moreCondition = false;
                } else {
                    vc.component.govCaseManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
