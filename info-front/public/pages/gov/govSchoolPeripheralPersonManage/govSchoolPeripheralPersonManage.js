/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govSchoolPeripheralPersonManageInfo: {
                govSchoolPeripheralPersons: [],
                govSchools:[],
                extentInjurys:[],
                total: 0,
                records: 1,
                componentShow: 'govSchoolPeripheralPersonManage',
                moreCondition: false,
                schPersonId: '',
                conditions: {
                    schoolId: '',
                    govPersonId: '',
                    schoolName: '',
                    personName: '',
                    personTel: '',
                    caId: '',
                    extentInjury: '',
                    isAttention: '',
                }
            }
        },
        _initMethod: function () {
            vc.component._listGovSchoolPeripheralPersons(DEFAULT_PAGE, DEFAULT_ROWS);
            vc.component._listGovSchools();
            vc.getDict('gov_school_peripheral_person', "extent_injury", function (_data) {
                vc.component.govSchoolPeripheralPersonManageInfo.extentInjurys = _data;
            });
        },
        _initEvent: function () {
            vc.on('govSchoolPeripheralPersonManage', 'listGovSchoolPeripheralPerson', function (_param) {
                $that.govSchoolPeripheralPersonManageInfo.componentShow ='govSchoolPeripheralPersonManage';
                vc.component._listGovSchoolPeripheralPersons(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovSchoolPeripheralPersons(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovSchoolPeripheralPersons: function (_page, _rows) {

                vc.component.govSchoolPeripheralPersonManageInfo.conditions.page = _page;
                vc.component.govSchoolPeripheralPersonManageInfo.conditions.row = _rows;
                vc.component.govSchoolPeripheralPersonManageInfo.conditions.caId = vc.getCurrentCommunity().caId;
                var param = {
                    params: vc.component.govSchoolPeripheralPersonManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govSchoolPeripheralPerson/queryGovSchoolPeripheralPerson',
                    param,
                    function (json, res) {
                        var _govSchoolPeripheralPersonManageInfo = JSON.parse(json);
                        vc.component.govSchoolPeripheralPersonManageInfo.total = _govSchoolPeripheralPersonManageInfo.total;
                        vc.component.govSchoolPeripheralPersonManageInfo.records = _govSchoolPeripheralPersonManageInfo.records;
                        vc.component.govSchoolPeripheralPersonManageInfo.govSchoolPeripheralPersons = _govSchoolPeripheralPersonManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govSchoolPeripheralPersonManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _listGovSchools: function () {
                var param = {
                    params:{
                        caId : vc.getCurrentCommunity().caId,
                        page:1,
                        row :50
                    }
                };
                //发送get请求
                vc.http.apiGet('/govSchool/queryGovSchool',
                    param,
                    function (json, res) {
                        var _govSchoolManageInfo = JSON.parse(json);
                        vc.component.govSchoolPeripheralPersonManageInfo.govSchools = _govSchoolManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _getSchoolExtentInjurys: function (_value) {
                let _retutest = '';
                $that.govSchoolPeripheralPersonManageInfo.extentInjurys.forEach(_item => {
                    if (_item.statusCd == _value) {
                        _retutest = _item.name;
                    }
                });
                return _retutest;
            },
            _openAddGovSchoolPeripheralPersonModal: function () {
                $that.govSchoolPeripheralPersonManageInfo.componentShow = 'addGovSchoolPeripheralPerson';
            },
            _openEditGovSchoolPeripheralPersonModel: function (_govSchoolPeripheralPerson) {
                vc.emit('editGovSchoolPeripheralPerson', 'openEditGovSchoolPeripheralPersonModal', _govSchoolPeripheralPerson);
                $that.govSchoolPeripheralPersonManageInfo.componentShow = 'editGovSchoolPeripheralPerson';
                
            },
            _openDeleteGovSchoolPeripheralPersonModel: function (_govSchoolPeripheralPerson) {
                vc.emit('deleteGovSchoolPeripheralPerson', 'openDeleteGovSchoolPeripheralPersonModal', _govSchoolPeripheralPerson);
            },
            _queryGovSchoolPeripheralPersonMethod: function () {
                vc.component._listGovSchoolPeripheralPersons(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition: function () {
                if (vc.component.govSchoolPeripheralPersonManageInfo.moreCondition) {
                    vc.component.govSchoolPeripheralPersonManageInfo.moreCondition = false;
                } else {
                    vc.component.govSchoolPeripheralPersonManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
