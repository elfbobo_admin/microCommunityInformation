/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govRoomManageInfo: {
                govRooms: [],
                total: 0,
                records: 1,
                moreCondition: false,
                govRoomId: '',
                govCommunityAreas: [],
                govCommunitys: [],
                govFloors: [],
                componentShow: 'govRoomManage',
                conditions: {
                    caId: vc.getCurrentCommunity().caId,
                    govCommunityId: '',
                    govFloorId: '',
                    roomNum: '',
                    roomType: '',
                    roomRight: '',
                    ownerName: '',
                    ownerTel: '',

                }
            }
        },
        _initMethod: function () {
            vc.component._listGovRooms(DEFAULT_PAGE, DEFAULT_ROWS);
            $that._listGovCommunitys(vc.getCurrentCommunity().caId);
        },
        _initEvent: function () {

            vc.on('govRoomManage', 'listGovRoom', function (_param) {
                $that.govRoomManageInfo.componentShow ='govRoomManage';
                vc.component._listGovRooms(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovRooms(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovRooms: function (_page, _rows) {

                vc.component.govRoomManageInfo.conditions.page = _page;
                vc.component.govRoomManageInfo.conditions.row = _rows;
                var param = {
                    params: vc.component.govRoomManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govRoom/queryGovRoom',
                    param,
                    function (json, res) {
                        var _govRoomManageInfo = JSON.parse(json);
                        vc.component.govRoomManageInfo.total = _govRoomManageInfo.total;
                        vc.component.govRoomManageInfo.records = _govRoomManageInfo.records;
                        vc.component.govRoomManageInfo.govRooms = _govRoomManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govRoomManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _listGovCommunitys: function (_caId) {

                let param = {
                    params: {
                        page: 1,
                        row: 50,
                        caId: _caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govCommunity/queryGovCommunity',
                    param,
                    function (json, res) {
                        var _govCommunityManageInfo = JSON.parse(json);
                        vc.component.govRoomManageInfo.total = _govCommunityManageInfo.total;
                        vc.component.govRoomManageInfo.records = _govCommunityManageInfo.records;
                        vc.component.govRoomManageInfo.govCommunitys = _govCommunityManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _listGovFloors: function (_caId,_GovCommunityId) {

                let param = {
                    params: {
                        page: 1,
                        row: 50,
                        caId: _caId,
                        govCommunityId: _GovCommunityId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govFloor/queryGovFloor',
                    param,
                    function (json, res) {
                        var _govFloorManageInfo = JSON.parse(json);
                        vc.component.govRoomManageInfo.total = _govFloorManageInfo.total;
                        vc.component.govRoomManageInfo.records = _govFloorManageInfo.records;
                        vc.component.govRoomManageInfo.govFloors = _govFloorManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddGovRoomModal: function () {
                $that.govRoomManageInfo.componentShow = 'addGovRoom';
               
                vc.emit('addGovRoom', 'openAddGovRoomModal', {});
            },
            _openEditGovRoomModel: function (_govRoom) {
                $that.govRoomManageInfo.componentShow = 'editGovRoom';
                vc.emit('editGovRoom', 'openEditGovRoomModal', _govRoom);
            },
            _openDeleteGovRoomModel: function (_govRoom) {
                vc.emit('deleteGovRoom', 'openDeleteGovRoomModal', _govRoom);
            },
            _queryGovRoomMethod: function () {
                vc.component._listGovRooms(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition: function () {
                if (vc.component.govRoomManageInfo.moreCondition) {
                    vc.component.govRoomManageInfo.moreCondition = false;
                } else {
                    vc.component.govRoomManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
