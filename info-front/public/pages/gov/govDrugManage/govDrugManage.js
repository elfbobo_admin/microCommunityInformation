/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govDrugManageInfo: {
                govDrugs: [],
                total: 0,
                records: 1,
                moreCondition: false,
                drugId: '',
                conditions: {
                    drugId: '',
                    name: '',
                    caId: '',
                    address: '',
                    tel: '',
                    drugStartTime: '',
                    drugEndTime: '',
                    drugReason: '',
                    emergencyPerson: '',
                    emergencyTel: '',
                    statusCd: '',

                }
            }
        },
        _initMethod: function () {
            vc.component._listGovDrugs(DEFAULT_PAGE, DEFAULT_ROWS);
        },
        _initEvent: function () {

            vc.on('govDrugManage', 'listGovDrug', function (_param) {
                vc.component._listGovDrugs(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovDrugs(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovDrugs: function (_page, _rows) {

                vc.component.govDrugManageInfo.conditions.page = _page;
                vc.component.govDrugManageInfo.conditions.row = _rows;
                vc.component.govDrugManageInfo.conditions.caId = vc.getCurrentCommunity().caId;
                var param = {
                    params: vc.component.govDrugManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govDrug/queryGovDrug',
                    param,
                    function (json, res) {
                        var _govDrugManageInfo = JSON.parse(json);
                        vc.component.govDrugManageInfo.total = _govDrugManageInfo.total;
                        vc.component.govDrugManageInfo.records = _govDrugManageInfo.records;
                        vc.component.govDrugManageInfo.govDrugs = _govDrugManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govDrugManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddGovDrugModal: function () {
                vc.emit('addGovDrug', 'openAddGovDrugModal', {});
            },
            _openEditGovDrugModel: function (_govDrug) {
                vc.emit('editGovDrug', 'openEditGovDrugModal', _govDrug);
            },
            _openDeleteGovDrugModel: function (_govDrug) {
                vc.emit('deleteGovDrug', 'openDeleteGovDrugModal', _govDrug);
            },
            _queryGovDrugMethod: function () {
                vc.component._listGovDrugs(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _openGovSpecialFolloModel: function (personInfo) {
                vc.jumpToPage('/admin.html#/pages/gov/govSpecialFollowManage?pId=' + personInfo.drugId +'&pName='+ personInfo.name);
            },
            _moreCondition: function () {
                if (vc.component.govDrugManageInfo.moreCondition) {
                    vc.component.govDrugManageInfo.moreCondition = false;
                } else {
                    vc.component.govDrugManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
