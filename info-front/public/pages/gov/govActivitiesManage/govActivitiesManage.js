/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govActivitiesManageInfo: {
                govActivitiess: [],
                total: 0,
                records: 1,
                moreCondition: false,
                activitiesId: '',
                componentShow:'activitiesList',
                govActivitiesTypes:[],
                conditions: {
                    title: '',
                    typeCd: '',
                    caId : vc.getCurrentCommunity().caId
                }
            }
        },
        _initMethod: function () {
            vc.component._listGovActivitiess(DEFAULT_PAGE, DEFAULT_ROWS);
            $that._queryGovActivitiesTypes(1,50);
        },
        _initEvent: function () {
          
            vc.on('govActivitiesManage', 'listGovActivities', function (_param) {
                vc.component.govActivitiesManageInfo.componentShow = 'activitiesList';
                vc.component._listGovActivitiess(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovActivitiess(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovActivitiess: function (_page, _rows) {

                vc.component.govActivitiesManageInfo.conditions.page = _page;
                vc.component.govActivitiesManageInfo.conditions.row = _rows;
                var param = {
                    params: vc.component.govActivitiesManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govActivities/queryGovActivities',
                    param,
                    function (json, res) {
                        var _govActivitiesManageInfo = JSON.parse(json);
                        vc.component.govActivitiesManageInfo.total = _govActivitiesManageInfo.total;
                        vc.component.govActivitiesManageInfo.records = _govActivitiesManageInfo.records;
                        vc.component.govActivitiesManageInfo.govActivitiess = _govActivitiesManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govActivitiesManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _queryGovActivitiesTypes: function (_page, _rows) {

                var param = {
                    params: {
                       page : _page,
                        row : _rows,
                        caId : vc.getCurrentCommunity().caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govActivitiesType/queryGovActivitiesType',
                    param,
                    function (json, res) {
                        var _govActivitiesTypeManageInfo = JSON.parse(json);
                        vc.component.govActivitiesManageInfo.govActivitiesTypes = _govActivitiesTypeManageInfo.data;

                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddGovActivitiesModal: function () {
                vc.component.govActivitiesManageInfo.componentShow = 'addGovActivities';
                vc.emit('addGovActivities', 'openAddGovActivitiesModal', {});

            },
            _openEditGovActivitiesModel: function (_govActivities) {
                vc.component.govActivitiesManageInfo.componentShow = 'editGovActivities';
                vc.emit('editGovActivities', 'openEditGovActivitiesModal', _govActivities);
            },
            _openDeleteGovActivitiesModel: function (_govActivities) {
                vc.emit('deleteGovActivities', 'openDeleteGovActivitiesModal', _govActivities);
            },
            _queryGovActivitiesMethod: function () {
                vc.component._listGovActivitiess(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition: function () {
                if (vc.component.govActivitiesManageInfo.moreCondition) {
                    vc.component.govActivitiesManageInfo.moreCondition = false;
                } else {
                    vc.component.govActivitiesManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
