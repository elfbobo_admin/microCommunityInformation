/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govMedicalGradeManageInfo: {
                govMedicalGrades: [],
                total: 0,
                records: 1,
                moreCondition: false,
                caId: '',
                componentShow: 'govMedicalGradeManage',
                conditions: {
                    medicalGradeId: '',
                    medicalClassifyId: '',
                    caId: '',
                    gradeName: '',
                    classifyName: '',
                    gradeType: '',
                }
            }
        },
        _initMethod: function () {
            vc.component._listGovMedicalGrades(DEFAULT_PAGE, DEFAULT_ROWS);
        },
        _initEvent: function () {

            vc.on('govMedicalGradeManage', 'listGovMedicalGrade', function (_param) {
                $that.govMedicalGradeManageInfo.componentShow ='govMedicalGradeManage';
                vc.component._listGovMedicalGrades(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovMedicalGrades(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovMedicalGrades: function (_page, _rows) {

                vc.component.govMedicalGradeManageInfo.conditions.page = _page;
                vc.component.govMedicalGradeManageInfo.conditions.row = _rows;
                vc.component.govMedicalGradeManageInfo.conditions.caId = vc.getCurrentCommunity().caId;
                var param = {
                    params: vc.component.govMedicalGradeManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govMedicalGrade/queryGovMedicalGrade',
                    param,
                    function (json, res) {
                        var _govMedicalGradeManageInfo = JSON.parse(json);
                        vc.component.govMedicalGradeManageInfo.total = _govMedicalGradeManageInfo.total;
                        vc.component.govMedicalGradeManageInfo.records = _govMedicalGradeManageInfo.records;
                        vc.component.govMedicalGradeManageInfo.govMedicalGrades = _govMedicalGradeManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govMedicalGradeManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddGovMedicalGradeModal: function () {
                $that.govMedicalGradeManageInfo.componentShow = 'addGovMedicalGrade';
            },
            _openEditGovMedicalGradeModel: function (_govMedicalGrade) {
                $that.govMedicalGradeManageInfo.componentShow = 'editGovMedicalGrade';
                vc.emit('editGovMedicalGrade', 'openEditGovMedicalGradeModal', _govMedicalGrade);
            },
            _openDeleteGovMedicalGradeModel: function (_govMedicalGrade) {
                vc.emit('deleteGovMedicalGrade', 'openDeleteGovMedicalGradeModal', _govMedicalGrade);
            },
            _queryGovMedicalGradeMethod: function () {
                vc.component._listGovMedicalGrades(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition: function () {
                if (vc.component.govMedicalGradeManageInfo.moreCondition) {
                    vc.component.govMedicalGradeManageInfo.moreCondition = false;
                } else {
                    vc.component.govMedicalGradeManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
