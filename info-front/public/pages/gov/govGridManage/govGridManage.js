/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govGridManageInfo: {
                govGrids: [],
                total: 0,
                records: 1,
                moreCondition: false,
                personName: '',
                govGridTypes: [],
                conditions: {
                    govTypeId: '',
                    personName: '',
                    personTel: '',
                    caId: vc.getCurrentCommunity().caId
                }
            }
        },
        _initMethod: function () {
            vc.component._listGovGrids(DEFAULT_PAGE, DEFAULT_ROWS);
            $that._listGovGridTypes();
        },
        _initEvent: function () {

            vc.on('govGridManage', 'listGovGrid', function (_param) {
                vc.component._listGovGrids(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovGrids(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovGrids: function (_page, _rows) {

                vc.component.govGridManageInfo.conditions.page = _page;
                vc.component.govGridManageInfo.conditions.row = _rows;
                var param = {
                    params: vc.component.govGridManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govGrid/queryGovGrid',
                    param,
                    function (json, res) {
                        var _govGridManageInfo = JSON.parse(json);
                        vc.component.govGridManageInfo.total = _govGridManageInfo.total;
                        vc.component.govGridManageInfo.records = _govGridManageInfo.records;
                        vc.component.govGridManageInfo.govGrids = _govGridManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govGridManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _listGovGridTypes: function () {
                var param = {
                    params: {
                        page: 1,
                        row: 50,
                        caId: vc.getCurrentCommunity().caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govGridType/queryGovGridType',
                    param,
                    function (json, res) {
                        var _govGridTypeManageInfo = JSON.parse(json);
                        vc.component.govGridManageInfo.govGridTypes = _govGridTypeManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddGovGridModal: function () {
                vc.emit('addGovGrid', 'openAddGovGridModal', {});
            },
            _openEditGovGridModel: function (_govGrid) {
                vc.emit('editGovGrid', 'openEditGovGridModal', _govGrid);
            },
            _openDeleteGovGridModel: function (_govGrid) {
                vc.emit('deleteGovGrid', 'openDeleteGovGridModal', _govGrid);
            },
            _queryGovGridMethod: function () {
                vc.component._listGovGrids(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _openGovGridObjRelModel: function (_govGrid) {
                vc.jumpToPage('/admin.html#/pages/gov/govGridObjRelManage?govGridId='+_govGrid.govGridId+'&typeName='+_govGrid.typeName+'&personName='+_govGrid.personName)
            },
            _moreCondition: function () {
                if (vc.component.govGridManageInfo.moreCondition) {
                    vc.component.govGridManageInfo.moreCondition = false;
                } else {
                    vc.component.govGridManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
