/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govCommunityAreaManageInfo: {
                govCommunityAreas: [],
                total: 0,
                records: 1,
                moreCondition: false,
                caId: '',
                conditions: {
                    caName: '',
                    areaCode: '',
                    person: '',
                    personLink: '',

                }
            }
        },
        _initMethod: function () {
            vc.component._listGovCommunityAreas(DEFAULT_PAGE, DEFAULT_ROWS);
        },
        _initEvent: function () {

            vc.on('govCommunityAreaManage', 'listGovCommunityArea', function (_param) {
                vc.component._listGovCommunityAreas(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovCommunityAreas(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovCommunityAreas: function (_page, _rows) {

                vc.component.govCommunityAreaManageInfo.conditions.page = _page;
                vc.component.govCommunityAreaManageInfo.conditions.row = _rows;
                var param = {
                    params: vc.component.govCommunityAreaManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govCommunityArea/queryGovCommunityArea',
                    param,
                    function (json, res) {
                        var _govCommunityAreaManageInfo = JSON.parse(json);
                        vc.component.govCommunityAreaManageInfo.total = _govCommunityAreaManageInfo.total;
                        vc.component.govCommunityAreaManageInfo.records = _govCommunityAreaManageInfo.records;
                        vc.component.govCommunityAreaManageInfo.govCommunityAreas = _govCommunityAreaManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govCommunityAreaManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddGovCommunityAreaModal: function () {
                vc.emit('addGovCommunityArea', 'openAddGovCommunityAreaModal', {});
            },
            _openEditGovCommunityAreaModel: function (_govCommunityArea) {
                vc.emit('editGovCommunityArea', 'openEditGovCommunityAreaModal', _govCommunityArea);
            },
            _openDeleteGovCommunityAreaModel: function (_govCommunityArea) {
                vc.emit('deleteGovCommunityArea', 'openDeleteGovCommunityAreaModal', _govCommunityArea);
            },
            _queryGovCommunityAreaMethod: function () {
                vc.component._listGovCommunityAreas(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition: function () {
                if (vc.component.govCommunityAreaManageInfo.moreCondition) {
                    vc.component.govCommunityAreaManageInfo.moreCondition = false;
                } else {
                    vc.component.govCommunityAreaManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
