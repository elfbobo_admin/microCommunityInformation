/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govVolunteerManageInfo: {
                govVolunteers: [],
                total: 0,
                records: 1,
                moreCondition: false,
                caId: '',
                goodAtSkills: [],
                freeTimes: [],
                politicalOutlooks: [],
                edus: [],
                goodAtSkillss: [],
                volunteerServs: [],
                conditions: {
                    name: '',
                    tel: '',
                    volunteerId: '',
                    state: ''

                }
            }
        },
        _initMethod: function () {

            vc.getDict('gov_volunteer', "political_outlook", function (_data) {
                vc.component.govVolunteerManageInfo.politicalOutlooks = _data;
            });
            vc.getDict('gov_volunteer', "edu", function (_data) {
                vc.component.govVolunteerManageInfo.edus = _data;
            });
            vc.getDict('gov_volunteer', "good_at_skills", function (_data) {
                vc.component.govVolunteerManageInfo.goodAtSkillss = _data;
            });
            vc.getDict('gov_volunteer', "free_time", function (_data) {
                vc.component.govVolunteerManageInfo.freeTimes = _data;
            });
            vc.component._listGovVolunteers(DEFAULT_PAGE, DEFAULT_ROWS);
            $that._listGovServFields();

        },
        _initEvent: function () {

            vc.on('govVolunteerManage', 'listGovVolunteer', function (_param) {
                vc.component._listGovVolunteers(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovVolunteers(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovVolunteers: function (_page, _rows) {

                vc.component.govVolunteerManageInfo.conditions.page = _page;
                vc.component.govVolunteerManageInfo.conditions.row = _rows;
                vc.component.govVolunteerManageInfo.conditions.caId = vc.getCurrentCommunity().caId;
                var param = {
                    params: vc.component.govVolunteerManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govVolunteer/queryGovVolunteer',
                    param,
                    function (json, res) {
                        var _govVolunteerManageInfo = JSON.parse(json);
                        vc.component.govVolunteerManageInfo.total = _govVolunteerManageInfo.total;
                        vc.component.govVolunteerManageInfo.records = _govVolunteerManageInfo.records;
                        vc.component.govVolunteerManageInfo.govVolunteers = _govVolunteerManageInfo.data;

                        vc.emit('pagination', 'init', {
                            total: vc.component.govVolunteerManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _listGovServFields: function () {
                var param = {
                    params: {
                        page: 1,
                        row: 50,
                        caId: vc.getCurrentCommunity().caId,
                        isShow: 'Y'
                    }
                };

                //发送get请求
                vc.http.apiGet('/govServField/queryGovServField',
                    param,
                    function (json, res) {
                        var _govServFieldManageInfo = JSON.parse(json);
                        vc.component.govVolunteerManageInfo.volunteerServs = _govServFieldManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddGovVolunteerModal: function () {
                vc.emit('addGovVolunteer', 'openAddGovVolunteerModal', {});
            },
            _openEditGovVolunteerModel: function (_govVolunteer) {
                vc.emit('editGovVolunteer', 'openEditGovVolunteerModal', _govVolunteer);
            },
            _openDeleteGovVolunteerModel: function (_govVolunteer) {
                vc.emit('deleteGovVolunteer', 'openDeleteGovVolunteerModal', _govVolunteer);
            },
            _openViewGovVolunteerModel: function (_govVolunteer) {

                vc.jumpToPage('/admin.html#/pages/common/viewGovVolunteerInfo?volunteerId=' + _govVolunteer.volunteerId);
            },
            _queryGovVolunteerMethod: function () {
                vc.component._listGovVolunteers(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _againReviewGovVolunteerModel: function (_govVolunteer) {

                var params = {
                    volunteerId: _govVolunteer.volunteerId,
                    state: '10001'
                }

                vc.http.apiPost(
                    '/govVolunteer/reviewGovVolunteer',
                    JSON.stringify(params),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            vc.emit('govVolunteerManage', 'listGovVolunteer', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            _getspecEduName: function (_edu) {
                let _retutest = '';
                $that.govVolunteerManageInfo.edus.forEach(_item => {
                    if (_item.statusCd == _edu) {
                        _retutest = _item.name + ',' + _retutest;
                    }
                });
                return _retutest.substr(0, _retutest.length - 1);
            },
            _getspecGoodName: function (_str) {
                let _retutest = '';
                let _tempGoodAtSkills = JSON.parse(_str);
                $that.govVolunteerManageInfo.goodAtSkillss.forEach(_item => {
                    _tempGoodAtSkills.forEach(_tempItem => {
                        if (_item.statusCd == _tempItem) {
                            _retutest = _item.name + ',' + _retutest;
                        }
                    })
                });
                return _retutest.substr(0, _retutest.length - 1);
            },
            _getspecFreeName: function (_str) {
                let _retutest = '';
                let _tempFreeTime = JSON.parse(_str);
                $that.govVolunteerManageInfo.freeTimes.forEach(_item => {
                    _tempFreeTime.forEach(_tempItem => {
                        if (_item.statusCd == _tempItem) {
                            _retutest = _item.name + ',' + _retutest;
                        }
                    })
                });
                return _retutest.substr(0, _retutest.length - 1);
            },
            _getspecServName: function (_str) {
                let _retutest = '';
                let _tempServ = JSON.parse(_str);
                $that.govVolunteerManageInfo.volunteerServs.forEach(_item => {
                    _tempServ.forEach(_tempItem => {
                        if (_item.servId == _tempItem) {
                            _retutest = _item.name + ',' + _retutest;
                        }
                    })
                });
                return _retutest.substr(0, _retutest.length - 1);
            },
            _moreCondition: function () {
                if (vc.component.govVolunteerManageInfo.moreCondition) {
                    vc.component.govVolunteerManageInfo.moreCondition = false;
                } else {
                    vc.component.govVolunteerManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
