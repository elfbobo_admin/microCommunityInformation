/**
    入驻小区
**/
(function(vc){
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data:{
            govCommunityarCorrectionManageInfo:{
                govCommunityarCorrections:[],
                total:0,
                records:1,
                moreCondition:false,
                correctionId:'',
                conditions:{
                    correctionId:'',
                    name:'',
                    caId:'',
                    address:'',
                    tel:'',
                    correctionStartTime:'',
                    correctionEndTime:'',
                    correctionReason:'',
                    statusCd:'',

                }
            }
        },
        _initMethod:function(){
            vc.component._listGovCommunityarCorrections(DEFAULT_PAGE, DEFAULT_ROWS);
        },
        _initEvent:function(){
            
            vc.on('govCommunityarCorrectionManage','listGovCommunityarCorrection',function(_param){
                  vc.component._listGovCommunityarCorrections(DEFAULT_PAGE, DEFAULT_ROWS);
            });
             vc.on('pagination','page_event',function(_currentPage){
                vc.component._listGovCommunityarCorrections(_currentPage,DEFAULT_ROWS);
            });
        },
        methods:{
            _listGovCommunityarCorrections:function(_page, _rows){

                vc.component.govCommunityarCorrectionManageInfo.conditions.page = _page;
                vc.component.govCommunityarCorrectionManageInfo.conditions.row = _rows;
                vc.component.govCommunityarCorrectionManageInfo.conditions.caId = vc.getCurrentCommunity().caId;
                var param = {
                    params:vc.component.govCommunityarCorrectionManageInfo.conditions
               };

               //发送get请求
               vc.http.apiGet('/govCommunityarCorrection/queryGovCommunityarCorrection',
                             param,
                             function(json,res){
                                var _govCommunityarCorrectionManageInfo=JSON.parse(json);
                                vc.component.govCommunityarCorrectionManageInfo.total = _govCommunityarCorrectionManageInfo.total;
                                vc.component.govCommunityarCorrectionManageInfo.records = _govCommunityarCorrectionManageInfo.records;
                                vc.component.govCommunityarCorrectionManageInfo.govCommunityarCorrections = _govCommunityarCorrectionManageInfo.data;
                                vc.emit('pagination','init',{
                                     total:vc.component.govCommunityarCorrectionManageInfo.records,
                                     currentPage:_page
                                 });
                             },function(errInfo,error){
                                console.log('请求失败处理');
                             }
                           );
            },
            _openAddGovCommunityarCorrectionModal:function(){
                vc.emit('addGovCommunityarCorrection','openAddGovCommunityarCorrectionModal',{});
            },
            _openEditGovCommunityarCorrectionModel:function(_govCommunityarCorrection){
                vc.emit('editGovCommunityarCorrection','openEditGovCommunityarCorrectionModal',_govCommunityarCorrection);
            },
            _openDeleteGovCommunityarCorrectionModel:function(_govCommunityarCorrection){
                vc.emit('deleteGovCommunityarCorrection','openDeleteGovCommunityarCorrectionModal',_govCommunityarCorrection);
            },
            _queryGovCommunityarCorrectionMethod:function(){
                vc.component._listGovCommunityarCorrections(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _openGovSpecialFolloModel: function (personInfo) {
                vc.jumpToPage('/admin.html#/pages/gov/govSpecialFollowManage?pId=' + personInfo.correctionId +'&pName='+ personInfo.name);
            },
            _moreCondition:function(){
                if(vc.component.govCommunityarCorrectionManageInfo.moreCondition){
                    vc.component.govCommunityarCorrectionManageInfo.moreCondition = false;
                }else{
                    vc.component.govCommunityarCorrectionManageInfo.moreCondition = true;
                }
            }

             
        }
    });
})(window.vc);
