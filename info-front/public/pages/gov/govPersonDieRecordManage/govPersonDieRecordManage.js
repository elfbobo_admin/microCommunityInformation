/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govPersonDieRecordManageInfo: {
                govPersonDieRecords: [],
                total: 0,
                records: 1,
                moreCondition: false,
                dieRecordId: '',
                conditions: {
                    dieRecordId: '',
                    dieId: '',
                    govPersonId: '',
                    context: '',
                    activityTime: '',
                    title: '',
                    caId: '',
                    statusCd: '',
                }
            }
        },
        _initMethod: function () {
            vc.initDate('qActivityTime', function (_value) {
                $that.govPersonDieRecordManageInfo.conditions.activityTime = _value;
            });
            vc.component._listGovPersonDieRecords(DEFAULT_PAGE, DEFAULT_ROWS);
        },
        _initEvent: function () {
            vc.on('govPersonDieRecordManage', 'listGovPersonDieRecord', function (_param) {
                vc.component._listGovPersonDieRecords(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovPersonDieRecords(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovPersonDieRecords: function (_page, _rows) {
                vc.component.govPersonDieRecordManageInfo.conditions.page = _page;
                vc.component.govPersonDieRecordManageInfo.conditions.row = _rows;
                vc.component.govPersonDieRecordManageInfo.conditions.caId = vc.getCurrentCommunity().caId;
                var param = {
                    params: vc.component.govPersonDieRecordManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govPersonDieRecord/queryGovPersonDieRecord',
                    param,
                    function (json, res) {
                        var _govPersonDieRecordManageInfo = JSON.parse(json);
                        vc.component.govPersonDieRecordManageInfo.total = _govPersonDieRecordManageInfo.total;
                        vc.component.govPersonDieRecordManageInfo.records = _govPersonDieRecordManageInfo.records;
                        vc.component.govPersonDieRecordManageInfo.govPersonDieRecords = _govPersonDieRecordManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govPersonDieRecordManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddGovPersonDieRecordModal: function () {
                vc.emit('addGovPersonDieRecord', 'openAddGovPersonDieRecordModal', {});
            },
            _openEditGovPersonDieRecordModel: function (_govPersonDieRecord) {
                vc.emit('editGovPersonDieRecord', 'openEditGovPersonDieRecordModal', _govPersonDieRecord);
            },
            _openDeleteGovPersonDieRecordModel: function (_govPersonDieRecord) {
                vc.emit('deleteGovPersonDieRecord', 'openDeleteGovPersonDieRecordModal', _govPersonDieRecord);
            },
            _queryGovPersonDieRecordMethod: function () {
                vc.component._listGovPersonDieRecords(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition: function () {
                if (vc.component.govPersonDieRecordManageInfo.moreCondition) {
                    vc.component.govPersonDieRecordManageInfo.moreCondition = false;
                } else {
                    vc.component.govPersonDieRecordManageInfo.moreCondition = true;
                }
            }
        }
    });
})(window.vc);
