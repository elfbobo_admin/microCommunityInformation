/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govInoutRecordInfo: {
                govCommunitys:[],
                govInouts: [],
                total: 0,
                records: 1,
                govCommunityAreas:[],
                govBuildingTypes:[],
                conditions: {
                    caId: vc.getCurrentCommunity().caId,
                    govCommunityId: '',
                    name: '',
                    tel: '',
                    idCard: '',
                }
            }
        },
        _initMethod: function () {
            vc.component._listgovInouts(DEFAULT_PAGE, DEFAULT_ROWS);
            $that._listGovCommunityAreas();
            $that._listGovCommunitys();
        },
        _initEvent: function () {

            vc.on('govFloorManage', 'listGovFloor', function (_param) {
                vc.component._listgovInouts(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listgovInouts(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listgovInouts: function (_page, _rows) {
                vc.component.govInoutRecordInfo.conditions.page = _page;
                vc.component.govInoutRecordInfo.conditions.row = _rows;
                var param = {
                    params: vc.component.govInoutRecordInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govPersonInoutRecord/queryGovPersonInoutRecord',
                    param,
                    function (json, res) {
                        var _govInoutRecordInfo = JSON.parse(json);
                        vc.component.govInoutRecordInfo.total = _govInoutRecordInfo.total;
                        vc.component.govInoutRecordInfo.records = _govInoutRecordInfo.records;
                        vc.component.govInoutRecordInfo.govInouts = _govInoutRecordInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govInoutRecordInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _listGovCommunityAreas: function () {
                let param = {
                    params: {
                        page: 1,
                        row: 50,
                        caId: vc.getCurrentCommunity().caId
                    }
                };
                //发送get请求
                vc.http.apiGet('/govCommunityArea/queryGovCommunityArea',
                    param,
                    function (json, res) {
                        var _govCommunityAreaManageInfo = JSON.parse(json);
                        vc.component.govInoutRecordInfo.total = _govCommunityAreaManageInfo.total;
                        vc.component.govInoutRecordInfo.records = _govCommunityAreaManageInfo.records;
                        vc.component.govInoutRecordInfo.govCommunityAreas = _govCommunityAreaManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _listGovCommunitys: function (_caId) {

                let param = {
                    params: {
                        page: 1,
                        row: 50,
                        caId: _caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govCommunity/queryGovCommunity',
                    param,
                    function (json, res) {
                        var _govCommunityManageInfo = JSON.parse(json);
                        vc.component.govInoutRecordInfo.total = _govCommunityManageInfo.total;
                        vc.component.govInoutRecordInfo.records = _govCommunityManageInfo.records;
                        vc.component.govInoutRecordInfo.govCommunitys = _govCommunityManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _queryGovFloorMethod: function () {
                vc.component._listgovInouts(DEFAULT_PAGE, DEFAULT_ROWS);

            }
        }
    });
})(window.vc);
