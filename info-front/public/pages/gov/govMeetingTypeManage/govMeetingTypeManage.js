/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govMeetingTypeManageInfo: {
                govMeetingTypes: [],
                total: 0,
                records: 1,
                moreCondition: false,
                caId: '',
                conditions: {
                    caId: ''
                }
            }
        },
        _initMethod: function () {
            vc.component._listGovMeetingTypes(DEFAULT_PAGE, DEFAULT_ROWS);
        },
        _initEvent: function () {

            vc.on('govMeetingTypeManage', 'listGovMeetingType', function (_param) {
                vc.component._listGovMeetingTypes(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovMeetingTypes(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovMeetingTypes: function (_page, _rows) {

                vc.component.govMeetingTypeManageInfo.conditions.page = _page;
                vc.component.govMeetingTypeManageInfo.conditions.row = _rows;
                vc.component.govMeetingTypeManageInfo.conditions.caId = vc.getCurrentCommunity().caId;
                var param = {
                    params: vc.component.govMeetingTypeManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govMeetingType/queryGovMeetingType',
                    param,
                    function (json, res) {
                        var _govMeetingTypeManageInfo = JSON.parse(json);
                        vc.component.govMeetingTypeManageInfo.total = _govMeetingTypeManageInfo.total;
                        vc.component.govMeetingTypeManageInfo.records = _govMeetingTypeManageInfo.records;
                        vc.component.govMeetingTypeManageInfo.govMeetingTypes = _govMeetingTypeManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govMeetingTypeManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddGovMeetingTypeModal: function () {
                vc.emit('addGovMeetingType', 'openAddGovMeetingTypeModal', {});
            },
            _openEditGovMeetingTypeModel: function (_govMeetingType) {
                vc.emit('editGovMeetingType', 'openEditGovMeetingTypeModal', _govMeetingType);
            },
            _openDeleteGovMeetingTypeModel: function (_govMeetingType) {
                vc.emit('deleteGovMeetingType', 'openDeleteGovMeetingTypeModal', _govMeetingType);
            },
            _queryGovMeetingTypeMethod: function () {
                vc.component._listGovMeetingTypes(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition: function () {
                if (vc.component.govMeetingTypeManageInfo.moreCondition) {
                    vc.component.govMeetingTypeManageInfo.moreCondition = false;
                } else {
                    vc.component.govMeetingTypeManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
