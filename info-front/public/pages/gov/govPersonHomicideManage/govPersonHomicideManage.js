/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govPersonHomicideManageInfo: {
                govPersons: [],
                total: 0,
                records: 1,
                moreCondition: false,
                govPersonId: '',
                politicalOutlooks:[],
                isWeb: '',
                govCommunityAreas: [],
                conditions: {
                    caId: vc.getCurrentCommunity().caId,
                    idCardLike: '',
                    personNameLike: '',
                    personTelLike: '',

                }
            }
        },
        _initMethod: function () {
            vc.component._listGovPersons(DEFAULT_PAGE, DEFAULT_ROWS);
            vc.getDict('gov_volunteer', "political_outlook", function (_data) {
                vc.component.govPersonHomicideManageInfo.politicalOutlooks = _data;
            });
        },
        _initEvent: function () {

            vc.on('govPersonManage', 'listGovPerson', function (_param) {
                vc.component._listGovPersons(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovPersons(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovPersons: function (_page, _rows) {

                vc.component.govPersonHomicideManageInfo.conditions.page = _page;
                vc.component.govPersonHomicideManageInfo.conditions.row = _rows;
                var param = {
                    params: vc.component.govPersonHomicideManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govPerson/queryGovPersonHomicide',
                    param,
                    function (json, res) {
                        var _govPersonHomicideManageInfo = JSON.parse(json);
                        vc.component.govPersonHomicideManageInfo.total = _govPersonHomicideManageInfo.total;
                        vc.component.govPersonHomicideManageInfo.records = _govPersonHomicideManageInfo.records;
                        vc.component.govPersonHomicideManageInfo.govPersons = _govPersonHomicideManageInfo.data;
                        console.log(vc.component.govPersonHomicideManageInfo.govPersons);
                        vc.emit('pagination', 'init', {
                            total: vc.component.govPersonHomicideManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _getSpecName: function (_politicalOutlook) {
                let _retutest = '';
                $that.govPersonHomicideManageInfo.politicalOutlooks.forEach(_item => {
                    if (_item.statusCd == _politicalOutlook) {
                        _retutest = _item.name + ',' + _retutest;
                    }
                });
                return _retutest.substr(0, _retutest.length - 1);
            },
            _openViewGovPersons: function (_govPerson) {

                vc.jumpToPage('/admin.html#/gov/viewGovPersonHomicide?govPersonId='+_govPerson.govPersonId+'&govOwnerId='+_govPerson.govOwnerId+'&govHomicideId='+_govPerson.govHomicideId);
            },
            _queryGovPersonMethod: function () {
                vc.component._listGovPersons(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition: function () {
                if (vc.component.govPersonHomicideManageInfo.moreCondition) {
                    vc.component.govPersonHomicideManageInfo.moreCondition = false;
                } else {
                    vc.component.govPersonHomicideManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
