/**
    入驻小区
**/
(function(vc){
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data:{
            govMentalDisordersManageInfo:{
                govMentalDisorderss:[],
                total:0,
                records:1,
                moreCondition:false,
                disordersId:'',
                conditions:{
                    disordersId:'',
                    name:'',
                    caId:'',
                    address:'',
                    tel:'',
                    disordersStartTime:'',
                    disordersReason:'',
                    emergencyPerson:'',
                    emergencyTel:'',
                    statusCd:'',

                }
            }
        },
        _initMethod:function(){
            vc.component._listGovMentalDisorderss(DEFAULT_PAGE, DEFAULT_ROWS);
        },
        _initEvent:function(){
            
            vc.on('govMentalDisordersManage','listGovMentalDisorders',function(_param){
                  vc.component._listGovMentalDisorderss(DEFAULT_PAGE, DEFAULT_ROWS);
            });
             vc.on('pagination','page_event',function(_currentPage){
                vc.component._listGovMentalDisorderss(_currentPage,DEFAULT_ROWS);
            });
        },
        methods:{
            _listGovMentalDisorderss:function(_page, _rows){

                vc.component.govMentalDisordersManageInfo.conditions.page = _page;
                vc.component.govMentalDisordersManageInfo.conditions.row = _rows;
                vc.component.govMentalDisordersManageInfo.conditions.caId = vc.getCurrentCommunity().caId;
                var param = {
                    params:vc.component.govMentalDisordersManageInfo.conditions
               };

               //发送get请求
               vc.http.apiGet('/govMentalDisorders/queryGovMentalDisorders',
                             param,
                             function(json,res){
                                var _govMentalDisordersManageInfo=JSON.parse(json);
                                vc.component.govMentalDisordersManageInfo.total = _govMentalDisordersManageInfo.total;
                                vc.component.govMentalDisordersManageInfo.records = _govMentalDisordersManageInfo.records;
                                vc.component.govMentalDisordersManageInfo.govMentalDisorderss = _govMentalDisordersManageInfo.data;
                                vc.emit('pagination','init',{
                                     total:vc.component.govMentalDisordersManageInfo.records,
                                     currentPage:_page
                                 });
                             },function(errInfo,error){
                                console.log('请求失败处理');
                             }
                           );
            },
            _openAddGovMentalDisordersModal:function(){
                vc.emit('addGovMentalDisorders','openAddGovMentalDisordersModal',{});
            },
            _openEditGovMentalDisordersModel:function(_govMentalDisorders){
                vc.emit('editGovMentalDisorders','openEditGovMentalDisordersModal',_govMentalDisorders);
            },
            _openDeleteGovMentalDisordersModel:function(_govMentalDisorders){
                vc.emit('deleteGovMentalDisorders','openDeleteGovMentalDisordersModal',_govMentalDisorders);
            },
            _queryGovMentalDisordersMethod:function(){
                vc.component._listGovMentalDisorderss(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _openGovSpecialFolloModel: function (personInfo) {
                vc.jumpToPage('/admin.html#/pages/gov/govSpecialFollowManage?pId=' + personInfo.disordersId +'&pName='+ personInfo.name);
            },
            _moreCondition:function(){
                if(vc.component.govMentalDisordersManageInfo.moreCondition){
                    vc.component.govMentalDisordersManageInfo.moreCondition = false;
                }else{
                    vc.component.govMentalDisordersManageInfo.moreCondition = true;
                }
            }

             
        }
    });
})(window.vc);
