/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govWorkTypeManageInfo: {
                govWorkTypes: [],
                total: 0,
                records: 1,
                moreCondition: false,
                govTypeId: '',
                conditions: {

                }
            }
        },
        _initMethod: function () {
            vc.component._listGovWorkTypes(DEFAULT_PAGE, DEFAULT_ROWS);
        },
        _initEvent: function () {

            vc.on('govWorkTypeManage', 'listGovWorkType', function (_param) {
                vc.component._listGovWorkTypes(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovWorkTypes(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovWorkTypes: function (_page, _rows) {

                vc.component.govWorkTypeManageInfo.conditions.page = _page;
                vc.component.govWorkTypeManageInfo.conditions.row = _rows;
                var param = {
                    params: vc.component.govWorkTypeManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govWorkType/queryGovWorkType',
                    param,
                    function (json, res) {
                        var _govWorkTypeManageInfo = JSON.parse(json);
                        vc.component.govWorkTypeManageInfo.total = _govWorkTypeManageInfo.total;
                        vc.component.govWorkTypeManageInfo.records = _govWorkTypeManageInfo.records;
                        vc.component.govWorkTypeManageInfo.govWorkTypes = _govWorkTypeManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govWorkTypeManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddGovWorkTypeModal: function () {
                vc.emit('addGovWorkType', 'openAddGovWorkTypeModal', {});
            },
            _openEditGovWorkTypeModel: function (_govWorkType) {
                vc.emit('editGovWorkType', 'openEditGovWorkTypeModal', _govWorkType);
            },
            _openDeleteGovWorkTypeModel: function (_govWorkType) {
                vc.emit('deleteGovWorkType', 'openDeleteGovWorkTypeModal', _govWorkType);
            },
            _queryGovWorkTypeMethod: function () {
                vc.component._listGovWorkTypes(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition: function () {
                if (vc.component.govWorkTypeManageInfo.moreCondition) {
                    vc.component.govWorkTypeManageInfo.moreCondition = false;
                } else {
                    vc.component.govWorkTypeManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
