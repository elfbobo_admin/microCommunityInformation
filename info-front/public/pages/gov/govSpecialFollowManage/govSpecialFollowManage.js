/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govSpecialFollowManageInfo: {
                govSpecialFollows: [],
                total: 0,
                records: 1,
                moreCondition: false,
                followId: '',
                specialPersonId: '',
                name: '',
                conditions: {
                    followId: '',
                    specialPersonId: '',
                    name: '',
                    caId: '',
                    specialStartTime: '',
                    specialReason: '',
                    isWork: '',
                    statusCd: '',

                }
            }
        },
        _initMethod: function () {
            $that.govSpecialFollowManageInfo.conditions.specialPersonId = vc.getParam('pId');
            $that.govSpecialFollowManageInfo.specialPersonId = vc.getParam('pId');
            $that.govSpecialFollowManageInfo.name = vc.getParam('pName');
            vc.component._listGovSpecialFollows(DEFAULT_PAGE, DEFAULT_ROWS);
        },
        _initEvent: function () {

            vc.on('govSpecialFollowManage', 'listGovSpecialFollow', function (_param) {
                vc.component._listGovSpecialFollows(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovSpecialFollows(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovSpecialFollows: function (_page, _rows) {

                vc.component.govSpecialFollowManageInfo.conditions.page = _page;
                vc.component.govSpecialFollowManageInfo.conditions.row = _rows;
                vc.component.govSpecialFollowManageInfo.conditions.caId = vc.getCurrentCommunity().caId;
                var param = {
                    params: vc.component.govSpecialFollowManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govSpecialFollow/queryGovSpecialFollow',
                    param,
                    function (json, res) {
                        var _govSpecialFollowManageInfo = JSON.parse(json);
                        vc.component.govSpecialFollowManageInfo.total = _govSpecialFollowManageInfo.total;
                        vc.component.govSpecialFollowManageInfo.records = _govSpecialFollowManageInfo.records;
                        vc.component.govSpecialFollowManageInfo.govSpecialFollows = _govSpecialFollowManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govSpecialFollowManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddGovSpecialFollowModal: function () {
                vc.emit('addGovSpecialFollow', 'openAddGovSpecialFollowModal',{specialPersonId: $that.govSpecialFollowManageInfo.specialPersonId,name:$that.govSpecialFollowManageInfo.name});
            },
            _openEditGovSpecialFollowModel: function (_govSpecialFollow) {
                vc.emit('editGovSpecialFollow', 'openEditGovSpecialFollowModal', _govSpecialFollow);
            },
            _openDeleteGovSpecialFollowModel: function (_govSpecialFollow) {
                vc.emit('deleteGovSpecialFollow', 'openDeleteGovSpecialFollowModal', _govSpecialFollow);
            },
            _queryGovSpecialFollowMethod: function () {
                vc.component._listGovSpecialFollows(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _goBack:function(){
                vc.goBack();
            },
            _moreCondition: function () {
                if (vc.component.govSpecialFollowManageInfo.moreCondition) {
                    vc.component.govSpecialFollowManageInfo.moreCondition = false;
                } else {
                    vc.component.govSpecialFollowManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
