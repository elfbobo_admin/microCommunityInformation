/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govFollowupSurveyManageInfo: {
                govFollowupSurveys: [],
                govSymptomTypes: [],
                total: 0,
                records: 1,
                moreCondition: false,
                caId: '',
                componentShow: 'govPersonDieManage',
                conditions: {
                    surveyTime: '',
                    govPersonName: '',
                    govPersonId: '',
                    surveyWay: '',
                    symptoms: '',
                    caId: '',
                    lifeStyleGuide: '',
                    drugCompliance: '',
                    adrs: '',
                    surveyType: '',
                    medication: '',
                    referralReason: '',
                    referralDepartment: '',
                    surveyAdvice: '',
                    surveyConclusion: '',
                    nextSurveyTime: '',
                    surveyDoctor: '',
                }
            }
        },
        _initMethod: function () {
            vc.initDate('qsurveyTime', function (_value) {
                $that.govFollowupSurveyManageInfo.conditions.surveyTime = _value;
            });
            vc.component._listGovFollowupSurveys(DEFAULT_PAGE, DEFAULT_ROWS);
            vc.component._listGovSymptomTypes();
        },
        _initEvent: function () {

            vc.on('govFollowupSurveyManage', 'listGovFollowupSurvey', function (_param) {
                vc.component._listGovFollowupSurveys(DEFAULT_PAGE, DEFAULT_ROWS);
                $that.govFollowupSurveyManageInfo.componentShow ='govPersonDieManage';
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovFollowupSurveys(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {

            _listGovSymptomTypes: function () {
                var param = {
                    params: {
                        page:1,
                        row:50,
                        caId : vc.getCurrentCommunity().caId
                    }
                };
                //发送get请求
                vc.http.apiGet('/govSymptomType/queryGovSymptomType',
                    param,
                    function (json, res) {
                        var _govSymptomTypeManageInfo = JSON.parse(json);
                        vc.component.govFollowupSurveyManageInfo.govSymptomTypes = _govSymptomTypeManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },

            _listGovFollowupSurveys: function (_page, _rows) {

                vc.component.govFollowupSurveyManageInfo.conditions.page = _page;
                vc.component.govFollowupSurveyManageInfo.conditions.row = _rows;
                vc.component.govFollowupSurveyManageInfo.conditions.caId = vc.getCurrentCommunity().caId;
                var param = {
                    params: vc.component.govFollowupSurveyManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govFollowupSurvey/queryGovFollowupSurvey',
                    param,
                    function (json, res) {
                        var _govFollowupSurveyManageInfo = JSON.parse(json);
                        vc.component.govFollowupSurveyManageInfo.total = _govFollowupSurveyManageInfo.total;
                        vc.component.govFollowupSurveyManageInfo.records = _govFollowupSurveyManageInfo.records;
                        vc.component.govFollowupSurveyManageInfo.govFollowupSurveys = _govFollowupSurveyManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govFollowupSurveyManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _getSurveyWay: function (_way) {
                let _retutest = '';
                let sways = [{value:'1001',name:'门诊'},
                             {value:'2002',name:'家庭'},
                             {value:'3003',name:'电话'}
                ];
                sways.forEach(_item => {
                    if (_item.value == _way) {
                        _retutest = _item.name ;
                    }
                });
                return _retutest;
            },
            _getDrugCompliance: function (_drug) {
                let _retutest = '';
                let sways = [{value:'1001',name:'规律'},
                             {value:'2002',name:'间断'},
                             {value:'3003',name:'不服药'}
                ];
                sways.forEach(_item => {
                    if (_item.value == _drug) {
                        _retutest = _item.name ;
                    }
                });
                return _retutest;
            },
            _getSurveyType: function (_drug) {
                let _retutest = '';
                let sways = [{value:'1001',name:'控制满意'},
                             {value:'2002',name:'控制不满意'},
                             {value:'3003',name:'不良反应'},
                             {value:'3003',name:'并发症'}
                ];
                sways.forEach(_item => {
                    if (_item.value == _drug) {
                        _retutest = _item.name ;
                    }
                });
                return _retutest;
            },
            _openAddGovFollowupSurveyModal: function () {
                $that.govFollowupSurveyManageInfo.componentShow = 'addGovFollowupSurvey';
            },
            _openEditGovFollowupSurveyModel: function (_govFollowupSurvey) {
                $that.govFollowupSurveyManageInfo.componentShow = 'editGovFollowupSurvey';
                vc.emit('editGovFollowupSurvey', 'openEditGovFollowupSurveyModal', _govFollowupSurvey);
            },
            _openDeleteGovFollowupSurveyModel: function (_govFollowupSurvey) {
                vc.emit('deleteGovFollowupSurvey', 'openDeleteGovFollowupSurveyModal', _govFollowupSurvey);
            },
            _queryGovFollowupSurveyMethod: function () {
                vc.component._listGovFollowupSurveys(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition: function () {
                if (vc.component.govFollowupSurveyManageInfo.moreCondition) {
                    vc.component.govFollowupSurveyManageInfo.moreCondition = false;
                } else {
                    vc.component.govFollowupSurveyManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
