/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govRoadProtectionCaseManageInfo: {
                govRoadProtectionCases: [],
                govRoadProtections: [],
                idTypes:[],
                caseTypes:[],
                total: 0,
                records: 1,
                moreCondition: false,
                roadCaseId: '',
                conditions: {
                    roadCaseId: '',
                    roadProtectionId: '',
                    caId: '',
                    govCommunityId: '',
                    caseName: '',
                    objType: '',
                    objName: '',
                    caseCode: '',
                    happenedTime: '',
                    happenedPlace: '',
                    idType: '',
                    idCard: '',
                    personName: '',
                    isSolved: '',
                    personNum: '',
                    fleeingNum: '',
                    arrestsNum: '',
                    detectionContent: '',
                    caseContent: '',
                    statusCd: '',
                }
            }
        },
        _initMethod: function () {
            vc.component._listGovRoadProtectionCases(DEFAULT_PAGE, DEFAULT_ROWS);
            vc.component._listGovRoadProtections();
            vc.getDict('gov_road_protection_case', "case_type", function (_data) {
                vc.component.govRoadProtectionCaseManageInfo.caseTypes = _data;
            });
            vc.getDict('gov_road_protection_case', "id_type", function (_data) {
                vc.component.govRoadProtectionCaseManageInfo.idTypes = _data;
            });
            vc.initDate('qHappenedTime', function (_value) {
                $that.govRoadProtectionCaseManageInfo.conditions.happenedTime = _value;
            });
        },
        _initEvent: function () {
            vc.on('govRoadProtectionCaseManage', 'listGovRoadProtectionCase', function (_param) {
                vc.component._listGovRoadProtectionCases(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovRoadProtectionCases(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovRoadProtections: function () {
                var param = {
                    params: {
                        caId : vc.getCurrentCommunity().caId,
                        page :1,
                        row :50
                    }
                };
                //发送get请求
                vc.http.apiGet('/govRoadProtection/queryGovRoadProtection',
                    param,
                    function (json, res) {
                        var _govRoadProtectionManageInfo = JSON.parse(json);
                        vc.component.govRoadProtectionCaseManageInfo.govRoadProtections = _govRoadProtectionManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _getRoadName: function (_value) {
                let _retutest = '';
                $that.govRoadProtectionCaseManageInfo.govRoadProtections.forEach(_item => {
                    if (_item.roadProtectionId == _value) {
                        _retutest = _item.roadName;
                    }
                });
                return _retutest;
            },
            _getIdType: function (_value) { 
                let _retutest = '';
                $that.govRoadProtectionCaseManageInfo.idTypes.forEach(_item => {
                    if (_item.statusCd == _value) {
                        _retutest = _item.name;
                    }
                });
                return _retutest;
            },
            _getCaseType: function (_value) {
                let _retutest = '';
                $that.govRoadProtectionCaseManageInfo.caseTypes.forEach(_item => {
                    if (_item.statusCd == _value) {
                        _retutest = _item.name;
                    }
                });
                return _retutest;
            },
            _listGovRoadProtectionCases: function (_page, _rows) {
                vc.component.govRoadProtectionCaseManageInfo.conditions.page = _page;
                vc.component.govRoadProtectionCaseManageInfo.conditions.row = _rows;
                vc.component.govRoadProtectionCaseManageInfo.conditions.caId = vc.getCurrentCommunity().caId;
                var param = {
                    params: vc.component.govRoadProtectionCaseManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govRoadProtectionCase/queryGovRoadProtectionCase',
                    param,
                    function (json, res) {
                        var _govRoadProtectionCaseManageInfo = JSON.parse(json);
                        vc.component.govRoadProtectionCaseManageInfo.total = _govRoadProtectionCaseManageInfo.total;
                        vc.component.govRoadProtectionCaseManageInfo.records = _govRoadProtectionCaseManageInfo.records;
                        vc.component.govRoadProtectionCaseManageInfo.govRoadProtectionCases = _govRoadProtectionCaseManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govRoadProtectionCaseManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddGovRoadProtectionCaseModal: function () {
                vc.emit('addGovRoadProtectionCase', 'openAddGovRoadProtectionCaseModal', {});
            },
            _openEditGovRoadProtectionCaseModel: function (_govRoadProtectionCase) {
                vc.emit('editGovRoadProtectionCase', 'openEditGovRoadProtectionCaseModal', _govRoadProtectionCase);
            },
            _openDeleteGovRoadProtectionCaseModel: function (_govRoadProtectionCase) {
                vc.emit('deleteGovRoadProtectionCase', 'openDeleteGovRoadProtectionCaseModal', _govRoadProtectionCase);
            },
            _queryGovRoadProtectionCaseMethod: function () {
                vc.component._listGovRoadProtectionCases(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition: function () {
                if (vc.component.govRoadProtectionCaseManageInfo.moreCondition) {
                    vc.component.govRoadProtectionCaseManageInfo.moreCondition = false;
                } else {
                    vc.component.govRoadProtectionCaseManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
