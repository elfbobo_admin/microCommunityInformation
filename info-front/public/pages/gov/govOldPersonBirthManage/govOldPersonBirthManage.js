/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govOldPersonManageInfo: {
                govOldPersons: [],
                total: 0,
                records: 1,
                moreCondition: false,
                caId: '',
                govCommunitys:[],
                govOldPersonTypes:[],
                conditions: {
                    caId: vc.getCurrentCommunity().caId,
                    govCommunityId: '',
                    personName: '',
                    personTel: '',
                    birthday: '',
                    typeId: ''

                }
            }
        },
        _initMethod: function () {
            vc.component.govOldPersonManageInfo.conditions.birthday = vc.dateFormat(new Date().getTime());
            vc.initDate('qBirthday', function (_value) {
                $that.govOldPersonManageInfo.conditions.birthday = _value;
            });
            vc.component._listGovOldPersons(DEFAULT_PAGE, DEFAULT_ROWS);
            $that._listGovCommunitys(vc.getCurrentCommunity().caId);
            $that._getGovOldPersonTypes();
        },
        _initEvent: function () {
            vc.on('govOldPersonManage', 'listGovOldPerson', function (_param) {
                vc.component._listGovOldPersons(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovOldPersons(_currentPage, DEFAULT_ROWS);
            });
            vc.on('chooseGovHealth', 'chooseGovHealth', function (_currentPage) {
                console.log(_currentPage);
                let _govPersonId = _currentPage.govPersonId;
                let _healthId = _currentPage.healthId;
                vc.jumpToPage('/admin.html#/pages/gov/reportInfoAnswerManage?govPersonId='+_govPersonId+'&healthId='+_healthId);
            });
        },
        methods: {
            _listGovOldPersons: function (_page, _rows) {

                vc.component.govOldPersonManageInfo.conditions.page = _page;
                vc.component.govOldPersonManageInfo.conditions.row = _rows;
                var param = {
                    params: vc.component.govOldPersonManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govOldPerson/queryGovOldPerson',
                    param,
                    function (json, res) {
                        var _govOldPersonManageInfo = JSON.parse(json);
                        vc.component.govOldPersonManageInfo.total = _govOldPersonManageInfo.total;
                        vc.component.govOldPersonManageInfo.records = _govOldPersonManageInfo.records;
                        vc.component.govOldPersonManageInfo.govOldPersons = _govOldPersonManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govOldPersonManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _getGovOldPersonTypes: function () {
                let param = {
                    params: {
                        page: 1,
                        row: 50,
                        caId: vc.getCurrentCommunity().caId
                    }
                };
                //发送get请求
                vc.http.apiGet('/govOldPersonType/queryGovOldPersonType',
                    param,
                    function (json, res) {
                        var _govOldPersonTypeManageInfo = JSON.parse(json);
                        vc.component.govOldPersonManageInfo.total = _govOldPersonTypeManageInfo.total;
                        vc.component.govOldPersonManageInfo.records = _govOldPersonTypeManageInfo.records;
                        vc.component.govOldPersonManageInfo.govOldPersonTypes = _govOldPersonTypeManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _toUserProfile: function (_govOldPerson) {
                // vc.emit('chooseGovHealth', 'openChooseGovHealthModel', {_govOldPerson});
                vc.jumpToPage('/admin.html#/pages/gov/userProfile?govPersonId='+_govOldPerson.govPersonId);
            },
            _openViewGovOldPersonModel:function(_govOldPerson){
                vc.jumpToPage('/admin.html#/pages/common/viewGovOldPersonInfo?govPersonId='+_govOldPerson.govPersonId+'&oldId='+_govOldPerson.oldId);
            },
            _listGovCommunitys: function (_caId) {

                let param = {
                    params: {
                        page: 1,
                        row: 50,
                        caId: _caId
                    }
                };
                //发送get请求
                vc.http.apiGet('/govCommunity/queryGovCommunity',
                    param,
                    function (json, res) {
                        var _govCommunityManageInfo = JSON.parse(json);
                        vc.component.govOldPersonManageInfo.total = _govCommunityManageInfo.total;
                        vc.component.govOldPersonManageInfo.records = _govCommunityManageInfo.records;
                        vc.component.govOldPersonManageInfo.govCommunitys = _govCommunityManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openGovOldPersonBirth: function () {
                vc.jumpToPage("/admin.html#/pages/gov/perGovActivitiesManage");// + vc.objToGetParam(JSON.parse(json))
            },
            _openEditGovOldPersonModel: function (_govOldPerson) {
                vc.emit('editGovOldPerson', 'openEditGovOldPersonModal', _govOldPerson);
            },
            _openDeleteGovOldPersonModel: function (_govOldPerson) {
                vc.emit('deleteGovOldPerson', 'openDeleteGovOldPersonModal', _govOldPerson);
            },
            _queryGovOldPersonMethod: function () {
                vc.component._listGovOldPersons(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition: function () {
                if (vc.component.govOldPersonManageInfo.moreCondition) {
                    vc.component.govOldPersonManageInfo.moreCondition = false;
                } else {
                    vc.component.govOldPersonManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
