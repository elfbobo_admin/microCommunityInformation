/**
    入驻小区
**/
(function(vc){
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data:{
            govActivityTypeManageInfo:{
                govActivityTypes:[],
                total:0,
                records:1,
                moreCondition:false,
                typeId:'',
                conditions:{
                    caId:'',
                    typeName:'',

                }
            }
        },
        _initMethod:function(){
            vc.component._listGovActivityTypes(DEFAULT_PAGE, DEFAULT_ROWS);
        },
        _initEvent:function(){
            
            vc.on('govActivityTypeManage','listGovActivityType',function(_param){
                  vc.component._listGovActivityTypes(DEFAULT_PAGE, DEFAULT_ROWS);
            });
             vc.on('pagination','page_event',function(_currentPage){
                vc.component._listGovActivityTypes(_currentPage,DEFAULT_ROWS);
            });
        },
        methods:{
            _listGovActivityTypes:function(_page, _rows){
                vc.component.govActivityTypeManageInfo.conditions.caId = vc.getCurrentCommunity().caId;
                vc.component.govActivityTypeManageInfo.conditions.page = _page;
                vc.component.govActivityTypeManageInfo.conditions.row = _rows;
                var param = {
                    params:vc.component.govActivityTypeManageInfo.conditions
               };

               //发送get请求
               vc.http.apiGet('/govActivityType/queryGovActivityType',
                             param,
                             function(json,res){
                                var _govActivityTypeManageInfo=JSON.parse(json);
                                vc.component.govActivityTypeManageInfo.total = _govActivityTypeManageInfo.total;
                                vc.component.govActivityTypeManageInfo.records = _govActivityTypeManageInfo.records;
                                vc.component.govActivityTypeManageInfo.govActivityTypes = _govActivityTypeManageInfo.data;
                                vc.emit('pagination','init',{
                                     total:vc.component.govActivityTypeManageInfo.records,
                                     currentPage:_page
                                 });
                             },function(errInfo,error){
                                console.log('请求失败处理');
                             }
                           );
            },
            _listAddGovCommunityAreas: function () {
                let param = {
                    params: {
                        page: 1,
                        row: 50
                    }
                };
                //发送get请求
                vc.http.apiGet('/govCommunityArea/queryGovCommunityArea',
                    param,
                    function (json, res) {
                        var _govCommunityAreaManageInfo = JSON.parse(json);
                        vc.component.addGovCommunityInfo.govCommunityAreas = _govCommunityAreaManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddGovActivityTypeModal:function(){
                vc.emit('addGovActivityType','openAddGovActivityTypeModal',{});
            },
            _openEditGovActivityTypeModel:function(_govActivityType){
                vc.emit('editGovActivityType','openEditGovActivityTypeModal',_govActivityType);
            },
            _openDeleteGovActivityTypeModel:function(_govActivityType){
                vc.emit('deleteGovActivityType','openDeleteGovActivityTypeModal',_govActivityType);
            },
            _queryGovActivityTypeMethod:function(){
                vc.component._listGovActivityTypes(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition:function(){
                if(vc.component.govActivityTypeManageInfo.moreCondition){
                    vc.component.govActivityTypeManageInfo.moreCondition = false;
                }else{
                    vc.component.govActivityTypeManageInfo.moreCondition = true;
                }
            }

             
        }
    });
})(window.vc);
