/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govReportTypeUserManageInfo: {
                govReportTypeUsers: [],
                total: 0,
                records: 1,
                moreCondition: false,
                typeUserId: '',
                reportType: '',
                reportTypeName: '',
                conditions: {
                    staffName: '',
                    state: '',
                    reportType: '',
                    caId: vc.getCurrentCommunity().caId

                }
            }
        },
        _initMethod: function () {
            $that.govReportTypeUserManageInfo.reportType=vc.getParam('reportType');
            $that.govReportTypeUserManageInfo.conditions.reportType=vc.getParam('reportType');
            $that.govReportTypeUserManageInfo.reportTypeName=vc.getParam('reportTypeName');
            vc.component._listGovReportTypeUsers(DEFAULT_PAGE, DEFAULT_ROWS);
        },
        _initEvent: function () {

            vc.on('govReportTypeUserManage', 'listGovReportTypeUser', function (_param) {
                vc.component._listGovReportTypeUsers(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovReportTypeUsers(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovReportTypeUsers: function (_page, _rows) {

                vc.component.govReportTypeUserManageInfo.conditions.page = _page;
                vc.component.govReportTypeUserManageInfo.conditions.row = _rows;
                var param = {
                    params: vc.component.govReportTypeUserManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govReportTypeUser/queryGovReportTypeUser',
                    param,
                    function (json, res) {
                        var _govReportTypeUserManageInfo = JSON.parse(json);
                        vc.component.govReportTypeUserManageInfo.total = _govReportTypeUserManageInfo.total;
                        vc.component.govReportTypeUserManageInfo.records = _govReportTypeUserManageInfo.records;
                        vc.component.govReportTypeUserManageInfo.govReportTypeUsers = _govReportTypeUserManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govReportTypeUserManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddRepairTypeUserModal: function () {
               
                vc.emit('selectStaff', 'openStaff', {
                    call: function (_staff) {
                        $that.saveGovReportTypeUserInfo(_staff);
                    }
                });
            },
            saveGovReportTypeUserInfo: function (_staff) {

                console.log(_staff);
                let param = {
                    caId: vc.getCurrentCommunity().caId,
                    staffId: _staff.staffId,
                    staffName: _staff.staffName,
                    reportType: $that.govReportTypeUserManageInfo.reportType,
                    state:'9999'
                }

                vc.http.apiPost(
                    '/govReportTypeUser/saveGovReportTypeUser',
                    JSON.stringify(param),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#selectStaffModel').modal('hide');
                            vc.component._listGovReportTypeUsers(DEFAULT_PAGE, DEFAULT_ROWS);

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            _openAddGovReportTypeUserModal: function () {
                vc.emit('addGovReportTypeUser', 'openAddGovReportTypeUserModal', {});
            },
            _openEditGovReportTypeUserModel: function (_govReportTypeUser) {
                vc.emit('editGovReportTypeUser', 'openEditGovReportTypeUserModal', _govReportTypeUser);
            },
            _openDeleteGovReportTypeUserModel: function (_govReportTypeUser) {
                vc.emit('deleteGovReportTypeUser', 'openDeleteGovReportTypeUserModal', _govReportTypeUser);
            },
            _queryGovReportTypeUserMethod: function () {
                vc.component._listGovReportTypeUsers(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _goBack:function(){            vc.goBack();},
            _moreCondition: function () {
                if (vc.component.govReportTypeUserManageInfo.moreCondition) {
                    vc.component.govReportTypeUserManageInfo.moreCondition = false;
                } else {
                    vc.component.govReportTypeUserManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
