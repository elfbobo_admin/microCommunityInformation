/**
    入驻小区
**/
(function(vc){
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data:{
            govVolunteerServManageInfo:{
                govVolunteerServs:[],
                total:0,
                records:1,
                moreCondition:false,
                caId:'',
                conditions:{
                    
                }
            }
        },
        _initMethod:function(){
            vc.component._listGovVolunteerServs(DEFAULT_PAGE, DEFAULT_ROWS);
        },
        _initEvent:function(){
            
            vc.on('govVolunteerServManage','listGovVolunteerServ',function(_param){
                  vc.component._listGovVolunteerServs(DEFAULT_PAGE, DEFAULT_ROWS);
            });
             vc.on('pagination','page_event',function(_currentPage){
                vc.component._listGovVolunteerServs(_currentPage,DEFAULT_ROWS);
            });
        },
        methods:{
            _listGovVolunteerServs:function(_page, _rows){

                vc.component.govVolunteerServManageInfo.conditions.page = _page;
                vc.component.govVolunteerServManageInfo.conditions.row = _rows;
                var param = {
                    params:vc.component.govVolunteerServManageInfo.conditions
               };

               //发送get请求
               vc.http.apiGet('/govVolunteerServ/queryGovVolunteerServ',
                             param,
                             function(json,res){
                                var _govVolunteerServManageInfo=JSON.parse(json);
                                vc.component.govVolunteerServManageInfo.total = _govVolunteerServManageInfo.total;
                                vc.component.govVolunteerServManageInfo.records = _govVolunteerServManageInfo.records;
                                vc.component.govVolunteerServManageInfo.govVolunteerServs = _govVolunteerServManageInfo.data;
                                vc.emit('pagination','init',{
                                     total:vc.component.govVolunteerServManageInfo.records,
                                     currentPage:_page
                                 });
                             },function(errInfo,error){
                                console.log('请求失败处理');
                             }
                           );
            },
            _openAddGovVolunteerServModal:function(){
                vc.emit('addGovVolunteerServ','openAddGovVolunteerServModal',{});
            },
            _openEditGovVolunteerServModel:function(_govVolunteerServ){
                vc.emit('editGovVolunteerServ','openEditGovVolunteerServModal',_govVolunteerServ);
            },
            _openDeleteGovVolunteerServModel:function(_govVolunteerServ){
                vc.emit('deleteGovVolunteerServ','openDeleteGovVolunteerServModal',_govVolunteerServ);
            },
            _queryGovVolunteerServMethod:function(){
                vc.component._listGovVolunteerServs(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition:function(){
                if(vc.component.govVolunteerServManageInfo.moreCondition){
                    vc.component.govVolunteerServManageInfo.moreCondition = false;
                }else{
                    vc.component.govVolunteerServManageInfo.moreCondition = true;
                }
            }

             
        }
    });
})(window.vc);
