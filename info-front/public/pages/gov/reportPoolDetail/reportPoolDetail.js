(function (vc) {
    vc.extends({
        data: {
            reportPoolDetailInfo: {
                reportId: '',
                caId:'',
                reportName: '',
                reportType: '',
                reportTypeName: '',
                tel: '',
                context: '',
                stateName: '',
                appointmentTime: '',
                repairChannel: '',
                createUserId:'',
                createName:'',
                visitType: '',
                visitContext: '',
                userId: '',
                userName: '',
                reportPoolUsers: []
              
            }
        },
        _initMethod: function () {
            let reportId = vc.getParam('reportId');
            if (!vc.notNull(reportId)) {
                vc.toast('非法操作');
                vc.jumpToPage('/admin.html#/pages/gov/reportPoolManage');
                return;
            }
            $that.reportPoolDetailInfo.reportId = reportId;
            $that._listRepairPools()
        },
        _initEvent: function () {
        },
        methods: {
            _listRepairPools: function () {
                var param = {
                    params: {
                        page: 1,
                        row: 1,
                        caId: vc.getCurrentCommunity().caId,
                        reportId: $that.reportPoolDetailInfo.reportId
                    }
                };
                //发送get请求
                vc.http.apiGet('/reportPool/queryReportPool',
                    param,
                    function (json, res) {
                        var _reportPoolPoolManageInfo = JSON.parse(json);
                        let _reportPools = _reportPoolPoolManageInfo.data;
                        if (_reportPools.length < 1) {
                            vc.toast("数据异常");
                            vc.jumpToPage('/admin.html#/pages/gov/reportPoolPoolManage');
                            return;
                        }
                        vc.copyObject(_reportPools[0], $that.reportPoolDetailInfo);
                      
                        //查询处理轨迹
                        $that._loadRepairUser();
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _loadRepairUser: function () {
                var param = {
                    params: {
                        page: 1,
                        row: 100,
                        caId: vc.getCurrentCommunity().caId,
                        reportId: $that.reportPoolDetailInfo.reportId
                    }
                };
                //发送get请求
                vc.http.apiGet('/govReportUser/queryGovReportUser',
                    param,
                    function (json, res) {
                        var _reportPoolPoolManageInfo = JSON.parse(json);
                        let _reportPools = _reportPoolPoolManageInfo.data;
                        $that.reportPoolDetailInfo.reportPoolUsers = _reportPools;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _goBack: function () {
                vc.goBack()
            },
            openFile: function (_photo) {
                vc.emit('viewImage', 'showImage', {
                    url: _photo.url
                });
            }
        }
    });
})(window.vc);
