/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govHelpPolicyListManageInfo: {
                govHelpPolicyLists: [],
                total: 0,
                records: 1,
                componentShow: 'govHelpPolicyListManage',
                moreCondition: false,
                caId: '',
                conditions: {
                    caId: '',
                    govHelpId: '',
                    poorPersonNameLike: '',
                    cadrePersonNameLike: ''

                }
            }
        },
        _initMethod: function () {
            vc.component._listGovHelpPolicyLists(DEFAULT_PAGE, DEFAULT_ROWS);
        },
        _initEvent: function () {

            vc.on('govHelpPolicyListManage', 'listGovHelpPolicyList', function (_param) {
                $that.govHelpPolicyListManageInfo.componentShow ='govHelpPolicyListManage';
                vc.component._listGovHelpPolicyLists(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovHelpPolicyLists(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovHelpPolicyLists: function (_page, _rows) {

                vc.component.govHelpPolicyListManageInfo.conditions.page = _page;
                vc.component.govHelpPolicyListManageInfo.conditions.row = _rows;
                vc.component.govHelpPolicyListManageInfo.conditions.caId = vc.getCurrentCommunity().caId;
                var param = {
                    params: vc.component.govHelpPolicyListManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govHelpPolicyList/queryGovHelpPolicyList',
                    param,
                    function (json, res) {
                        var _govHelpPolicyListManageInfo = JSON.parse(json);
                        vc.component.govHelpPolicyListManageInfo.total = _govHelpPolicyListManageInfo.total;
                        vc.component.govHelpPolicyListManageInfo.records = _govHelpPolicyListManageInfo.records;
                        vc.component.govHelpPolicyListManageInfo.govHelpPolicyLists = _govHelpPolicyListManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govHelpPolicyListManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddGovHelpPolicyListModal: function () {
                $that.govHelpPolicyListManageInfo.componentShow = 'addGovHelpPolicyList';
                vc.emit('addGovHelpPolicyList', 'openAddGovHelpPolicyListModal', {});
            },
            _openEditGovHelpPolicyListModel: function (_govHelpPolicyList) {
                $that.govHelpPolicyListManageInfo.componentShow = 'editGovHelpPolicyList';
                vc.emit('editGovHelpPolicyList', 'openEditGovHelpPolicyListModal', _govHelpPolicyList);
            },
            _openDeleteGovHelpPolicyListModel: function (_govHelpPolicyList) {
                vc.emit('deleteGovHelpPolicyList', 'openDeleteGovHelpPolicyListModal', _govHelpPolicyList);
            },
            _queryGovHelpPolicyListMethod: function () {
                vc.component._listGovHelpPolicyLists(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition: function () {
                if (vc.component.govHelpPolicyListManageInfo.moreCondition) {
                    vc.component.govHelpPolicyListManageInfo.moreCondition = false;
                } else {
                    vc.component.govHelpPolicyListManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
