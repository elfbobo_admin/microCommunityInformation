/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            userQuestionAnswerManageInfo: {
                questionAnswerTitles: [],
                total: 0,
                records: 1,
                moreCondition: false,
                questionAnswerValue: [],
                healthId: '',
                caId: vc.getCurrentCommunity().caId,
                personId: '',
                doctor: '',
                doctorRemark: ''
            }
        },
        _initMethod: function () {
            $that.userQuestionAnswerManageInfo.healthId = vc.getParam('healthId');
            $that.userQuestionAnswerManageInfo.personId = vc.getParam('personId');
            vc.component._listQuestionAnswerTitles(DEFAULT_PAGE, DEFAULT_ROWS);
            console.log($that.userQuestionAnswerManageInfo);
        },
        _initEvent: function () {
        },
        methods: {
            _listQuestionAnswerTitles: function (_page, _rows) {
                let _that = $that.userQuestionAnswerManageInfo;
                var param = {
                    params: {
                        page: 1,
                        row: 100,
                        healthId: _that.healthId,
                        caId: vc.getCurrentCommunity().caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govHealthTitle/getTermInfo',
                    param,
                    function (json, res) {
                        _that.questionAnswerTitles = [];
                        let _userQuestionAnswerManageInfo = JSON.parse(json);
                        _that.questionAnswerTitles = _userQuestionAnswerManageInfo.data;
                        $that._listQuestionAnswerValues(DEFAULT_PAGE, DEFAULT_ROWS);
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _listQuestionAnswerValues: function (_page, _rows) {
                let _that = $that.userQuestionAnswerManageInfo;
                var param = {
                    params: {
                        page: 1,
                        row: 100,
                        healthId: _that.healthId,
                        healthPersonId: _that.healthPersonId,
                        personId: _that.personId,
                        caId: vc.getCurrentCommunity().caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govHealthAnswerValue/queryGovHealthAnswerValue',
                    param,
                    function (json, res) {
                        _that.questionAnswerValue = [];
                        let _userQuestionAnswerManageInfo = JSON.parse(json);
                        _that.questionAnswerValue = _userQuestionAnswerManageInfo.data;
                        _that.doctor = _that.questionAnswerValue[0].doctor;
                        _that.doctorRemark = _that.questionAnswerValue[0].doctorRemark;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _getTitleValue(_titleId, _valueId, _titleType) {
                let _retuText = '';
                $that.userQuestionAnswerManageInfo.questionAnswerValue.forEach(item => {
                    if (_titleType != '3003') {
                        if (item.titleId == _titleId && item.valueId == _valueId) {
                            _retuText = item.valueContent
                        }
                    } else {
                        if (item.titleId == _titleId) {
                            _retuText = item.valueContent
                        }
                    }
                });

                return _retuText;
            },
            _goBack: function () {
                vc.goBack();
            }
        }
    });
})(window.vc);
