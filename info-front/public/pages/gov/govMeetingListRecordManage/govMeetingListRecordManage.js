/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govMeetingListRecordInfo: {
                govMeetingLists: [],
                total: 0,
                records: 1,
                moreCondition: false,
                caId: '',
                componentShow: 'govMeetingListRecord',
                conditions: {
                    meetingName: '',
                    meetingTime: ''
                }
            }
        },
        _initMethod: function () {
            vc.component._listGovMeetingLists(DEFAULT_PAGE, DEFAULT_ROWS);
            vc.initDateTime('meetingTime', function (_value) {
                $that.govMeetingListRecordInfo.conditions.meetingTime = _value;
            });
        },
        _initEvent: function () {

            vc.on('govMeetingListRecord', 'listGovMeetingList', function (_param) {
                $that.govMeetingListRecordInfo.componentShow ='govMeetingListRecord';
                vc.component._listGovMeetingLists(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovMeetingLists(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovMeetingLists: function (_page, _rows) {

                vc.component.govMeetingListRecordInfo.conditions.page = _page;
                vc.component.govMeetingListRecordInfo.conditions.row = _rows;
                vc.component.govMeetingListRecordInfo.conditions.caId = vc.getCurrentCommunity().caId;
                var param = {
                    params: vc.component.govMeetingListRecordInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govMeetingList/queryGovMeetingList',
                    param,
                    function (json, res) {
                        var _govMeetingListRecordInfo = JSON.parse(json);
                        vc.component.govMeetingListRecordInfo.total = _govMeetingListRecordInfo.total;
                        vc.component.govMeetingListRecordInfo.records = _govMeetingListRecordInfo.records;
                        vc.component.govMeetingListRecordInfo.govMeetingLists = _govMeetingListRecordInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govMeetingListRecordInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddGovMeetingListModal: function () {
                $that.govMeetingListRecordInfo.componentShow = 'addGovMeetingList';
                //vc.emit('addGovMeetingList', 'openAddGovMeetingListModal', {});
            },
            _openRecordGovMeetingListModel: function (_govMeetingList) {
                $that.govMeetingListRecordInfo.componentShow = 'editGovMeetingList';
                vc.emit('editGovMeetingListRecord', 'openeditGovMeetingListRecordModal', _govMeetingList);
            },
            _selectRecordGovMeetingListModel: function (_govMeetingList) {
                $that.govMeetingListRecordInfo.componentShow = 'selectGovMeetingListRecordSelect';
                vc.emit('selectGovMeetingListRecordSelect', 'openselectGovMeetingListRecordSelectModal', _govMeetingList);
            },
            _openDeleteGovMeetingListModel: function (_govMeetingList) {

                vc.emit('deleteGovMeetingList', 'openDeleteGovMeetingListModal', _govMeetingList);
            },
            _queryGovMeetingListMethod: function () {
                vc.component._listGovMeetingLists(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition: function () {
                if (vc.component.govMeetingListRecordInfo.moreCondition) {
                    vc.component.govMeetingListRecordInfo.moreCondition = false;
                } else {
                    vc.component.govMeetingListRecordInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
