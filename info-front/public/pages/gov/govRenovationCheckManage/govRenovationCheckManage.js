/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govRenovationCheckManageInfo: {
                govRenovationChecks: [],
                total: 0,
                records: 1,
                moreCondition: false,
                caId: '',
                conditions: {
                    govRenovationId: '',

                }
            }
        },
        _initMethod: function () {
            vc.component._listGovRenovationChecks(DEFAULT_PAGE, DEFAULT_ROWS);
        },
        _initEvent: function () {

            vc.on('govRenovationCheckManage', 'listGovRenovationCheck', function (_param) {
                vc.component._listGovRenovationChecks(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovRenovationChecks(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovRenovationChecks: function (_page, _rows) {

                vc.component.govRenovationCheckManageInfo.conditions.page = _page;
                vc.component.govRenovationCheckManageInfo.conditions.row = _rows;
                vc.component.govRenovationCheckManageInfo.conditions.caId = vc.getCurrentCommunity().caId;
                var param = {
                    params: vc.component.govRenovationCheckManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govRenovationCheck/queryGovRenovationCheck',
                    param,
                    function (json, res) {
                        var _govRenovationCheckManageInfo = JSON.parse(json);
                        vc.component.govRenovationCheckManageInfo.total = _govRenovationCheckManageInfo.total;
                        vc.component.govRenovationCheckManageInfo.records = _govRenovationCheckManageInfo.records;
                        vc.component.govRenovationCheckManageInfo.govRenovationChecks = _govRenovationCheckManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govRenovationCheckManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddGovRenovationCheckModal: function () {
                vc.emit('addGovRenovationCheck', 'openAddGovRenovationCheckModal', {});
            },
            _openEditGovRenovationCheckModel: function (_govRenovationCheck) {
                vc.emit('editGovRenovationCheck', 'openEditGovRenovationCheckModal', _govRenovationCheck);
            },
            _openDeleteGovRenovationCheckModel: function (_govRenovationCheck) {
                vc.emit('deleteGovRenovationCheck', 'openDeleteGovRenovationCheckModal', _govRenovationCheck);
            },
            _queryGovRenovationCheckMethod: function () {
                vc.component._listGovRenovationChecks(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition: function () {
                if (vc.component.govRenovationCheckManageInfo.moreCondition) {
                    vc.component.govRenovationCheckManageInfo.moreCondition = false;
                } else {
                    vc.component.govRenovationCheckManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
