/**
    老年人 组件
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            viewGovOldPersonInfo: {
                oldId: '',
                caId: '',
                govCommunityId: '',
                personName: '',
                personTel: '',
                personAge: '',
                contactPerson: '',
                contactTel: '',
                servPerson: '',
                servTel: '',
                ramark: '',
                govPersonId:''
            },
            govPersonsAnswers: [],
            govOldAccountDetail: {
                acctId:'',
                acctName: '',
                amount: '',
                createTime: '',
                govOldAccountDetails: [],
                conditions: {
                    detailType: '',
                    orderId: ''
                }
            }



        },
        _initMethod: function () {
            let _oldId = vc.getParam("oldId");
            let _govPersonId = vc.getParam("govPersonId");
            $that.viewGovOldPersonInfo.oldId = _oldId;
            $that.viewGovOldPersonInfo.govPersonId = _govPersonId;
            //根据请求参数查询 查询 业主信息
            vc.component._listGovOldPersons(_govPersonId);
            $that._listGovPersonsAnswer(DEFAULT_PAGE,DEFAULT_ROWS,_govPersonId);
            $that._queryGovOldAccountDetail(DEFAULT_PAGE,DEFAULT_ROWS,_oldId);
        },
        _initEvent: function () {
            vc.on('viewGovOldPersonInfo', 'chooseGovOldPerson', function (_app) {
                vc.copyObject(_app, vc.component.viewGovOldPersonInfo);
            });

            vc.on('viewGovOldPersonInfo', 'onIndex', function (_index) {
                vc.component.viewGovOldPersonInfo.index = _index;
            });
            vc.on('govPersons','paginationPlus', 'page_event', function (_currentPage) {
                vc.component._listGovPersonsAnswer(_currentPage,DEFAULT_ROWS,$that.viewGovOldPersonInfo.govPersonId);
            });
            vc.on('account','paginationPlus', 'page_event', function (_currentPage) {
             
                vc.component._queryGovOldAccountDetail(_currentPage,DEFAULT_ROWS,$that.viewGovOldPersonInfo.oldId);
            });
        },
        methods: {

            _openSelectGovOldPersonInfoModel() {
                vc.emit('chooseGovOldPerson', 'openChooseGovOldPersonModel', {});
            },
            _openAddGovOldPersonInfoModel() {
                vc.emit('addGovOldPerson', 'openAddGovOldPersonModal', {});
            },
            _toViewAnswerTitle: function (_govPersonsAnswer) {
                vc.jumpToPage('/admin.html#/pages/gov/reportInfoAnswerValueManage?personId=' + _govPersonsAnswer.personId + '&healthId=' + _govPersonsAnswer.healthId);
            },
            _listGovPersonsAnswer: function (_page,_row,_govPersonId) {

                var param = {
                    params: {
                        personId: _govPersonId,
                        caId: vc.getCurrentCommunity().caId,
                        page: _page,
                        row: _row
                    }
                };

                //发送get请求
                vc.http.apiGet('/govHealthAnswer/queryGovHealthAnswer',
                    param,
                    function (json, res) {
                        vc.component.govPersonsAnswers = [];
                        var _govOldPersonManageInfo = JSON.parse(json);
                        vc.component.viewGovOldPersonInfo.total = _govOldPersonManageInfo.total;
                        vc.component.viewGovOldPersonInfo.records = _govOldPersonManageInfo.records;
                        vc.component.govPersonsAnswers = _govOldPersonManageInfo.data;
                        vc.emit('govPersons', 'paginationPlus', 'init', {
                            total: _govOldPersonManageInfo.records,
                            dataCount: _govOldPersonManageInfo.total,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _listGovOldPersons: function (_govPersonId) {
                var param = {
                    params: {
                        govPersonId: _govPersonId,
                        caId: vc.getCurrentCommunity().caId,
                        page: DEFAULT_PAGE,
                        row: 10
                    }
                };

                //发送get请求
                vc.http.apiGet('/govOldPerson/queryGovOldPerson',
                    param,
                    function (json, res) {
                        var _govOldPersonManageInfo = JSON.parse(json);
                        vc.component.viewGovOldPersonInfo = _govOldPersonManageInfo.data[0];

                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _goBack: function () {
                vc.goBack();
            },
            _queryGovOldAccountDetail: function (_page,_row,_oldId) {
              
                var param = {
                    params: {
                        oldId: _oldId,
                        caId: vc.getCurrentCommunity().caId,
                        page: _page,
                        row: _row,
                        detailType: $that.govOldAccountDetail.conditions.detailType,
                        orderId: $that.govOldAccountDetail.conditions.orderId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govOldPerson/queryGovOldAccountDetail',
                    param,
                    function (json, res) {
                        vc.component.govOldAccountDetail.govOldAccountDetails = [];
                        var _govOldPersonManageInfo = JSON.parse(json);
                        console.log(_govOldPersonManageInfo)
                        vc.copyObject(_govOldPersonManageInfo.data[0], vc.component.govOldAccountDetail);
                        vc.component.govOldAccountDetail.govOldAccountDetails = _govOldPersonManageInfo.data[0].accountDetailDtos.data;
                        vc.emit('account', 'paginationPlus', 'init', {
                            total: _govOldPersonManageInfo.data[0].accountDetailDtos.records,
                            dataCount: _govOldPersonManageInfo.data[0].accountDetailDtos.total,
                            currentPage: DEFAULT_PAGE
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _queryAccountDetailMethod: function () {
                _queryGovOldAccountDetail(DEFAULT_PAGE,DEFAULT_ROWS,$that.viewGovOldPersonInfo.oldId);
            }
        }
    });

})(window.vc);
