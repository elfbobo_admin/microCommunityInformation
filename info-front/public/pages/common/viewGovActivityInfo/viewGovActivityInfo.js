/**
    活动管理 组件
**/
(function (vc) {

    vc.extends({

        data: {
            viewGovActivityInfo: {
                index: 0,
                caId: '',
                actId: '',
                actName: '',
                actTime: '',
                actAddress: '',
                personCount: '',
                context: '',
                contactName: '',
                contactLink: ''
            },
            govActivityInfo: {
                comments:[],
                commentCount: 0
            }
        },
        _initMethod: function () {
            let _actId = vc.getParam("actId");
            vc.component.viewGovActivityInfo.actId = _actId;
            vc.component._loadGovActivityInfoData();
            vc.component._listRelys();
        },
        _initEvent: function () {
            vc.on('viewGovActivityInfo', 'chooseGovActivity', function (_app) {
                console.log(_app);
                vc.copyObject(_app, vc.component.viewGovActivityInfo);
                vc.emit($props.callBackListener, $props.callBackFunction, vc.component.viewGovActivityInfo);
            });

            vc.on('viewGovActivityInfo', 'onIndex', function (_index) {
                vc.component.viewGovActivityInfo.index = _index;
            });

        },
        methods: {

            _openSelectGovActivityInfoModel() {
                vc.emit('chooseGovActivity', 'openChooseGovActivityModel', {});
            },
            _openAddGovActivityInfoModel() {
                vc.emit('addGovActivity', 'openAddGovActivityModal', {});
            },
            //活动信息
            _loadGovActivityInfoData: function () {
                var param = {
                    params:{
                        caId : vc.getCurrentCommunity().caId,
                        page : 1,
                        row : 1,
                        actId : vc.component.viewGovActivityInfo.actId
                    }
               };
               //发送get请求
               vc.http.apiGet('/govActivity/queryGovActivity',
                             param,
                             function(json,res){
                                var _govActivityManageInfo=JSON.parse(json);
                                vc.component.viewGovActivityInfo = _govActivityManageInfo.data[0];
                             },function(errInfo,error){
                                console.log('请求失败处理');
                             }
                           );
            } ,
            _listRelys: function (_page, _rows) {
                var param = {
                    params: {
                        actId : vc.component.viewGovActivityInfo.actId,
                        page: 1,
                        row: 100,
                        isTree: 1
                    }
                };

                //发送get请求
                vc.http.apiGet('/govActivityReply/queryGovActivityReply',
                    param,
                    function (json, res) {
                        let _replyInfo = JSON.parse(json);
                        $that.govActivityInfo.comments = _replyInfo.data;
                        $that.govActivityInfo.commentCount = _replyInfo.total;

                        console.log( $that.govActivityInfo.comments);
                        console.log( $that.govActivityInfo.commentCount);
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },

            _goBack: function () {
                console.log("我在这里");
                vc.goBack();
            }
        }
    });

})(window.vc);
