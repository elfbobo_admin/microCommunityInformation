## HC社区政务，主要包括 党建管理 投票管理 居民管理 区域管理 业主 物业征信管理 消防 维修基金使用监管 验房存档功能和政务发布等，欢迎各位朋友们参与加入

## 外系统对接协议

[HC社区政务对接协议](info-doc/readme.md)

## 视频介绍

https://www.bilibili.com/video/BV11f4y1J7NA/

## 演示地址

http://gov.homecommunity.cn/

账号密码 admin/admin

## 安装部署

百度云盘链接：https://pan.baidu.com/s/1WjgmeTrKUyUpmMJ-dp6ZsQ 
提取码：y214

下载一键安装部署脚本执行
./hcGovInstall.sh 你的私网Ip

## 运行效果

1.在浏览器输入 http://localhost:3002/ 如下图

![image](info-doc/img/login.png)
![image](info-doc/img/10001.png)
![image](info-doc/img/10002.png)
![image](info-doc/img/10003.png)
![image](info-doc/img/10004.png)
![image](info-doc/img/10005.png)
![image](info-doc/img/10006.png)
![image](info-doc/img/10007.png)
![image](info-doc/img/10008.png)